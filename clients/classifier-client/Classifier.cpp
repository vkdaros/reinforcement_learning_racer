/***************************************************************************

    file                 : Classifier.cpp
    created              : Sun Sep 23 13:22:31 BRT 2012
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <iomanip>

#include <cmath>

#include "Classifier.h"
#include "StretchData.h"

void Classifier::init(float *angles) {
	// set angles from -90 to 90 degrees with 10 degree step.
    for (int i = 0; i < TRACK_SENSORS_NUM; i++) {
        angles[i] = -90 + i * 10;
    }

    this->lastDistance = 0;
    this->lap = 0;
}

void Classifier::onShutdown() {
    string file_name = string(this->trackName) + ".raw";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    list<StretchData>::iterator i;
    for (i = this->stretches.begin(); i != this->stretches.end(); i++) {
        i->print(outputStream);
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Classifier::onRestart() {
    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File track_data.out closed." << endl;
    }
}

CarControl Classifier::wDrive(CarState cs) {
    float accel;
    float brake;
    int gear;
    float steer;
    float clutch;

    accel = getAcceleration(cs);
    brake = getBraking(cs);
    gear = selectGear(cs);
    steer = getSteer(cs);
    clutch = 0;

    updateLap(cs);
    registerData(cs, steer);

    CarControl cc(accel, brake, gear, steer, clutch);

    // Updating lastDistance only after everything else is done.
    this->lastDistance = cs.getDistFromStart();

    return cc;
}

int Classifier::selectGear(CarState cs) {
    return 1;
}

float Classifier::getAcceleration(CarState cs) {
    return 0.25;
}

float Classifier::getBraking(CarState cs) {
    float brake;

    if (cs.getTrack(9) <= 50 && cs.getSpeedX() > 20) {
        brake = 0.3;
    } else {
        brake = 0;
    }

    return brake;
}

float Classifier::getSteer(CarState cs) {
    float targetAngle;
    float steer;

    /* TODO: Improve steering policy to turn harder. */
    /*       Or at least check if it is ok.          */

    // Keep on the midle of the track.
    targetAngle = (cs.getAngle() - cs.getTrackPos());
    steer = targetAngle / 0.366519; // steerLock magic number
    steer *= 3.0;
    if (steer < -1) {
        steer = -1;
    } else if (steer > 1) {
        steer = 1;
    }

    return steer;
}

void Classifier::registerData(CarState cs, float steer) {
    // If the race hasn't started, there is noting to do.
    if (this->lap <= 0) {
        return;
    }

    static int stretchCounter = 0;
    static float lastRecordedDistance = 0;
    float distanceRaced = cs.getDistRaced();
    float deltaDistance = distanceRaced - lastRecordedDistance;

    if (this->lastDistance > distanceRaced) {
        // A new lap has just started.
        stretchCounter = 0;

        // Force to record the beginning of the first stretch.
        deltaDistance = DISTANCE_STEP + 0.01;
    }
    if (deltaDistance >= DISTANCE_STEP) {
        float angle = cs.getAngle();
        float steerAngle = steer * 0.366519;
        float lapTime = cs.getCurLapTime();
        float distFromStart = cs.getDistFromStart();
        float distRaced = cs.getDistRaced();
        float speedX = cs.getSpeedX();
        float speedY = cs.getSpeedY();
        float speedZ = cs.getSpeedZ();
        float frontRighttWheelSpin = cs.getWheelSpinVel(0);
        float frontLeftWheelSpin = cs.getWheelSpinVel(1);
        float rearRightWheelSpin = cs.getWheelSpinVel(2);
        float rearLeftWheelSpin = cs.getWheelSpinVel(3);
        float z = cs.getZ();
        float trackWidth = cs.getTrack(0) + cs.getTrack(TRACK_SENSORS_NUM - 1);

        StretchData stretch(stretchCounter,
                            angle,
                            steerAngle,
                            lapTime,
                            distFromStart,
                            distRaced,
                            speedX,
                            speedY,
                            speedZ,
                            frontRighttWheelSpin,
                            frontLeftWheelSpin,
                            rearRightWheelSpin,
                            rearLeftWheelSpin,
                            z,
                            trackWidth);
        this->stretches.push_back(stretch);

        lastRecordedDistance = distanceRaced;
        stretchCounter++;
    }
}

void Classifier::updateLap(CarState cs) {
    if (cs.getDistFromStart() < this->lastDistance) {
        this->lap++;
    }
}
