/***************************************************************************

    file                 : Classifier.h
    created              : Sun Sep 23 13:22:31 BRT 2012
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CLASSIFIER_H_
#define CLASSIFIER_H_

#include <fstream>
#include <list>

#include "BaseDriver.h"
#include "CarState.h"
#include "CarControl.h"
#include "WrapperBaseDriver.h"
#include "StretchData.h"

#define DISTANCE_STEP 5 // Magic number to data work better in visualizer.
#define PI 3.14159265359

using namespace std;

class Classifier : public WrapperBaseDriver {
  public:
	// Constructor.
	Classifier(){};

	// Initialization of the desired angles for the rangefinders.
	virtual void init(float *angles);

	// Called at the end of the race, before being unloaded.
	virtual void onShutdown();

	// Called when the race is restarted.
	virtual void onRestart();

    // Change the effectors values in order to drive the car.
    virtual CarControl wDrive(CarState cs);

  private:
    // File where the sensor readings will be written
    ofstream outputStream;

    // Distance from start measured in the last tick.
    float lastDistance;

    // Lap counter. If equals to zero, then the first lap hasn't started yet.
    int lap;

    // List of scanned data of track stretches.
    list<StretchData> stretches;

    // Select to gear up or down according to RPM.
    int selectGear(CarState cs);

    // Returns acceleartion based on the distance to the next bend.
    float getAcceleration(CarState cs);

    // Returns braking based on the distance to the next bend.
    float getBraking(CarState cs);

    // Returns braking based on the distance to the next bend.
    float getSteer(CarState cs);

    // Writes on output file relevant current state data.
    void registerData(CarState cs, float steer);

    // Responsible for updating lap counter.
    void updateLap(CarState cs);
};
#endif /* CLASSIFIER_H_ */
