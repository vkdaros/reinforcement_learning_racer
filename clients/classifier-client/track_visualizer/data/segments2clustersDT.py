#!/usr/bin/python

# Create clusters according a Decision Tree.

import os
import sys
import math

error_message = """
ERROR: no input file provided. At least one segment file must be provided.
    Usage: segments2clustersDT.py <FILE1> [FILE2 FILE3 ...]
"""

SHORT_STRAIGHT = 0
MODERATE_STRAIGHT = 1
LONG_STRAIGHT = 2
RIGHT_HAIRPIN = 3
LEFT_HAIRPIN = 4
SHORT_SHARP_RIGHT = 5
SHORT_SHARP_LEFT = 6
LONG_SHARP_RIGHT = 7
LONG_SHARP_LEFT = 8
SHORT_EASY_RIGHT = 9
SHORT_EASY_LEFT = 10
LONG_EASY_RIGHT = 11
LONG_EASY_LEFT = 12


def get_file_names():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()
    return sys.argv[1:]


def is_straight(x, y):
    return (2500 * x - y <= 50)


def is_hairpin(x, y):
    return ( ((x - 3)**2 / 1.25**2) + ((y - 0)**2 / 350**2) <= 1 )


def is_sharp_turn(x, y):
    return ( ((x - 3)**2 / 2.25**2) + ((y - 0)**2 / 1200**2) <= 1 )


def is_to_right(angle):
    return angle < 0.0


def decision_tree(dist, angle):
    x = math.fabs(angle)
    y = dist

    if is_straight(x, y):
        if y <= 50:
            return SHORT_STRAIGHT
        if y <= 200:
            return MODERATE_STRAIGHT
        return LONG_STRAIGHT

    if is_hairpin(x, y):
        if is_to_right(angle):
            return RIGHT_HAIRPIN
        return LEFT_HAIRPIN

    if is_sharp_turn(x, y):
        if y <= 100:
            if is_to_right(angle):
                return SHORT_SHARP_RIGHT
            return SHORT_SHARP_LEFT
        else:
            if is_to_right(angle):
                return LONG_SHARP_RIGHT
            return LONG_SHARP_LEFT

    if y <= 100:
        if is_to_right(angle):
            return SHORT_EASY_RIGHT
        return SHORT_EASY_LEFT
    else:
        if is_to_right(angle):
            return LONG_EASY_RIGHT
        return LONG_EASY_LEFT


def apply_cluster(file_name):
    file = open(file_name, "r")
    lines = file.readlines()
    file.close()

    file = open(os.path.splitext(file_name)[0] + ".cluster", "w")
    for line in lines:
        dist, angle = [float(v) for v in line.strip().split(',')]
        cluster = decision_tree(dist, angle)
        file.write("%f,%f,cluster%d\n" % (dist, angle, cluster))
    file.close()


def main():
    file_names = get_file_names()

    for name in file_names:
        apply_cluster(name)


main()
