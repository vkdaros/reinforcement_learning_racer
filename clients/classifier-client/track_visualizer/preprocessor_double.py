#!/usr/bin/python

import sys
import math

# CONSTANTS
TRACK = 1.9               # track = distance between wheels in the same axis.
WHEELBASE = 4.5           # wheelbase = distance between
E = (math.pi / 180) * 0.5 # steering dead angle.
FRONT_WHEEL_RADIOUS = 0.3306
REAR_WHEEL_RADIOUS = 0.3250

error_message = """
ERROR: no input file provided
    Usage: preprocesser.py <FILE>
    FILE: track file in CSV (semicolon ";" as separator). Values are
    angle; steerAngle; lapTime; distFromStart; distRaced; speedX; speedY; speedZ;
    frontRighttWheelSpin; frontLeftWheelSpin; rearRightWheelSpin; rearLeftWheelSpin; z
"""

def get_file_name():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()

    return sys.argv[1]


def parse_data(file_name):
    keys = [ "angle", "steerAngle", "lapTime", "distFromStart", "distRaced", \
    "speedX", "speedY", "speedZ", \
    "frontRighttWheelSpin", "frontLeftWheelSpin", \
    "rearRightWheelSpin", "rearLeftWheelSpin", "z"]

    file = open(file_name, "r")
    # data = list of dictionaries.
    data = []
    data_raw = [[float(val.strip()) for val in line.split(';')] for line in file.readlines()]
    for d in data_raw:
        data.append(dict(zip(keys,d)))
    file.close()

    return data

# IT DOES NOT WORK AS EXPECTED!
def munoz_method(data, last_data):
    w_right = data["frontRighttWheelSpin"]
    w_left = data["frontLeftWheelSpin"]
    steer = data["steerAngle"]

    if w_right < 0:
        w_right = 0
    if w_left < 0:
        w_left = 0

    if w_right == w_left or steer == 0:
        # It is not a turn.
        return (data["distRaced"] - last_data["distRaced"]), 0
    elif w_right < w_left:
        # Right turn
        w_in = w_right
        w_out = w_left
        turn_direction = -1
    else:
        # Left turn
        w_in = w_left
        w_out = w_right
        turn_direction = 1

    if w_in == 0.0:
        w_in +=0.000001
    # Inner radius of the turn.
    r_in = TRACK / ((w_out / w_in) - 1)
    
    # Curvature angle of the turn.
    delta_t = data["lapTime"] - last_data["lapTime"]
    alpha = turn_direction * FRONT_WHEEL_RADIOUS * w_in * delta_t / r_in

    return (r_in + (TRACK / 2)), alpha


def jazar_method(data, last_data):
    # Using the same variables names showed in the book.
    a = WHEELBASE / 2; # center of gravity
    l = WHEELBASE
    delta = data["steerAngle"]

    w_right = data["frontRighttWheelSpin"]
    w_left = data["frontLeftWheelSpin"]

    if w_right == w_left or delta == 0:
        # It is not a turn.
        return (data["distRaced"] - last_data["distRaced"]), 0
    elif delta < 0:
        # Right turn
        w_out = w_left
        turn_direction = -1
    else:
        # Left turn
        w_out = w_right
        turn_direction = 1

    # Radius of the turn.
    cot_delta = 1 / math.tan(delta)
    R = math.sqrt(a * a + l * l * cot_delta * cot_delta)
    
    # Curvature angle of the turn.
    delta_t = data["lapTime"] - last_data["lapTime"]
    alpha = turn_direction * FRONT_WHEEL_RADIOUS * w_out * delta_t / (R + TRACK / 2)

    return R, alpha


def simple_method(data, last_data):
    w_right = data["frontRighttWheelSpin"]
    w_left = data["frontLeftWheelSpin"]
    steer = data["steerAngle"]

    if w_right == w_left or steer == 0:
        # It is not a turn.
        return (data["distRaced"] - last_data["distRaced"]), 0
    elif steer < 0:
        # Right turn
        w_out = w_left
        turn_direction = -1
    else:
        # Left turn
        w_out = w_right
        turn_direction = 1

    r = math.fabs(WHEELBASE / math.sin(steer))
    delta_t = data["lapTime"] - last_data["lapTime"]
    alpha = turn_direction * FRONT_WHEEL_RADIOUS * w_out * delta_t / (r + TRACK / 2)

    return r, alpha


def get_turn_radius_and_angle(method, data, last_data):
    error_message = """
ERROR: in get_turn_radius_and_angle(): Wrong 'method' argument: %s
       There is no such method.""" % (method)
    if method == "Simple":
        return simple_method(data, last_data)
    elif method == "Jazar":
        return jazar_method(data, last_data)
    elif method == "Munoz":
        return munoz_method(data, last_data)
    else:
       print(error_message)


def process(data):
    angle_sum = [0, 0]
    new_data = []

    last_d = data[0]
    for d in data[1:]:
        radii = [0, 0]
        angles = [0, 0]

        radii[0], angles[0] = get_turn_radius_and_angle("Jazar", d, last_d)
        radii[1], angles[1] = get_turn_radius_and_angle("Simple", d, last_d)

        angle_sum[0] += angles[0]
        angle_sum[1] += angles[1]

        dist = d["distRaced"] - last_d["distRaced"]
        new_data.append([dist, angles[0], dist, angles[1]])

        last_d = d

    correction_factor = [-2 * math.pi / angle_sum[0], \
                         -2 * math.pi / angle_sum[1]]
    for d in new_data:
        d[1] *= correction_factor[0]
        d[3] *= correction_factor[1]

    return new_data


def main():
    file_name = get_file_name()
    data = parse_data(file_name)

    processed_data = process(data)
    for d in processed_data:
        print("%f %f %f %f" % (d[0], d[1], d[2], d[3]))


main()
