#!/usr/bin/python2

import os
import sys
import string
import math
import pygame
from pygame.locals import *

# CONSTANTS
SCREEN_SIZE = [1024, 768]
TRACK_SURFACE_SIZE = [2000, 2000]
INITIAL_POINT = (TRACK_SURFACE_SIZE[0] / 2, TRACK_SURFACE_SIZE[1] / 2)
TRACK_WIDTH = 20
SEGMENTATION_EPSILON = 0.0
SEGMENTATION_MIN_DISTANCE = 0
PRINT_SEGMENTS = False

BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255,255,0)
MAGENTA = (255,0,255)
CYAN = (0,255,255)
WHITE = (255, 255, 255)

FOREST_GREEN = (34, 139, 34)
YELLOW_GREEN = (153, 204, 50)
AQUAMARINE = (112, 219, 147)
ROYALL_BLUE1 = (72, 118, 255)
SLATE_BLUE = (106, 90, 205)
LIME_GREEN = (50, 205, 50)
ORANGE = (255, 165, 0)
DARK_ORCHID = (153, 50, 204)

error_message = """
ERROR: no input file provided
    Usage: track_visualizer.py <FILE> [SCALE_FACTOR] [WIDTH HEIGHT]
    FILE: track file in which each line has two numbers distance and angle.
    SCALE_FACTOR: the greater the factor, the larger the zoom.
    WIDTH HEIGHT: screen size.
"""

help_message = """
INTERACTION:
    Arrow keys: move image;
    + - keys: to scale image;
    q a keys: to increase decrease segmentation angle;
    w s keys: to increase decrease segmentation distance.
    p key:    to print segments over the track.
    x key:    to create a .xy points file.
    c key:    to create a .segments file with accumulated distances and angles of each segment.
    v key:    to create a screenshot .png file.
    ESC BACKSPACE: quit
"""

def get_file_name():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()

    return sys.argv[1]


def get_scale_factor():
    scale_factor = 1
    if len(sys.argv) >= 3:
        scale_factor = float(sys.argv[2])

    return scale_factor


def get_screen_size():
    width = 1024
    height = 768
    if len(sys.argv) >= 5:
        width = int(sys.argv[3])
        height = int(sys.argv[4])

    return [width, height]


def get_data(file_name):
    file = open(file_name, "r")
    data = [[float(val) for val in line.split()] for line in file.readlines()]
    file.close()

    # data = [[dist, angle], [dist, angle], ... ]
    return data


def get_points(data, scale_factor):
    last_point = INITIAL_POINT
    angle = math.pi # First segment will point upward.

    points = [last_point]
    for d in data:
        angle += d[1]
        distance = d[0]
        x = math.sin(angle) * distance / (1 / scale_factor)
        y = math.cos(angle) * distance / (1 / scale_factor)
        new_point = (last_point[0] + x, last_point[1] + y)
        points.append(new_point)
        last_point = new_point

    return points


# Calculate right and left edges of the track anc returns these two lists.
def get_edge_points(data, center_points, scale_factor):
    right_points = []
    left_points = []
    angle = math.pi # First segment will point upward.

    for i in range(len(data)):
        gama = angle - math.pi / 2 + data[i][1] / 2

        x = math.sin(gama) * TRACK_WIDTH / (1 / scale_factor)
        y = math.cos(gama) * TRACK_WIDTH / (1 / scale_factor)

        new_right = (center_points[i][0] + x, center_points[i][1] + y)
        new_left = (center_points[i][0] - x, center_points[i][1] - y)

        right_points.append(new_right)
        left_points.append(new_left)

        angle += data[i][1]

    i += 1
    last_right = (center_points[i][0] + x, center_points[i][1] + y)
    last_left = (center_points[i][0] - x, center_points[i][1] - y)

    right_points.append(last_right)
    left_points.append(last_left)

    return right_points, left_points


def draw_crosshair(surface):
    width = surface.get_width()
    height = surface.get_height()

    size = 0.05 # Percentage with respect to half of the screen.

    up = height * (1 - size) / 2
    down = height * (1 + size) / 2
    left = width * (1 - size) / 2
    right = width * (1 + size) / 2

    pygame.draw.line(surface, GREEN, (left, up), (right, down))
    pygame.draw.line(surface, GREEN, (left, down), (right, up))


def draw_points(clear, surface, points, color):
    if clear:
        surface.fill((255, 255, 255, 20))

    last_point = points[0]
    for point in points[1:]:
        if math.isnan(point[0]) or math.isnan(point[1]):
            print("NAN found!")
            continue
        pygame.draw.line(surface, color, last_point, point)
        last_point = point


def draw_segments(clear, surface, data, right_points, left_points):
    if clear:
        surface.fill((255, 255, 255, 20))

    if not PRINT_SEGMENTS:
        return

    #colors = [YELLOW_GREEN, AQUAMARINE, ROYALL_BLUE1, LIME_GREEN, SLATE_BLUE,\
    #          ORANGE, DARK_ORCHID]
    colors = [GREEN, BLUE, YELLOW, MAGENTA, CYAN, FOREST_GREEN, ORANGE, RED]
    color = 0

    accumulated_distance = data[0][0]

    size = 2 # COLOCAR COMO CONSTANTE
    i = size
    while i < len(right_points):
        left_rev = left_points[i - size: i]
        left_rev.reverse()
        pygame.draw.polygon(surface, colors[color], \
                            right_points[i - size: i] + left_rev)
        if math.fabs(data[i - 2][1] - data[i - 1][1]) > SEGMENTATION_EPSILON and \
           accumulated_distance > SEGMENTATION_MIN_DISTANCE:

            color = (color + 1) % len(colors)
            accumulated_distance = data[i - 1][0]
        else:
            accumulated_distance += data[i - 1][0]

        i += size-1


def draw_all(surface, data, points, right_points, left_points):
    draw_segments(True, surface, data, right_points, left_points)
    draw_points(False, surface, points, WHITE)
    draw_points(False, surface, right_points, RED)
    draw_points(False, surface, left_points, BLUE)


def handle_events(offset):
    global SEGMENTATION_EPSILON
    global SEGMENTATION_MIN_DISTANCE
    global PRINT_SEGMENTS

    running = True
    scale = 1
    step = 10    # The amount of pixels offset will move.
    factor = 0.1 # Proportion by which scale will increase/decrease.
    segmentation_changed = False
    should_export_points = False
    should_export_segments = False
    should_export_screenshot = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == KEYUP:
            if event.key == K_ESCAPE or event.key == K_BACKSPACE:
                running = False

        if event.type == KEYDOWN:
            if event.key == K_RIGHT:
                offset[0] -= step
            elif event.key == K_LEFT:
                offset[0] += step
            if event.key == K_UP:
                offset[1] += step
            elif event.key == K_DOWN:
                offset[1] -= step
            if event.key == K_p:
                PRINT_SEGMENTS = not PRINT_SEGMENTS
                segmentation_changed = True
            if event.key == K_x:
                should_export_points = True
            if event.key == K_c:
                should_export_segments = True
            if event.key == K_v:
                should_export_screenshot = True
            if event.key == K_q:
                SEGMENTATION_EPSILON += 0.001
                segmentation_changed = True
                print("SEGMENTATION_EPSILON: %f" % SEGMENTATION_EPSILON)
            elif event.key == K_a:
                SEGMENTATION_EPSILON -= 0.001
                segmentation_changed = True
                print("SEGMENTATION_EPSILON: %f" % SEGMENTATION_EPSILON)
            if event.key == K_w:
                SEGMENTATION_MIN_DISTANCE += 1
                segmentation_changed = True
                print("SEGMENTATION_MIN_DISTANCE: %f" % SEGMENTATION_MIN_DISTANCE)
            elif event.key == K_s:
                SEGMENTATION_MIN_DISTANCE -= 1
                segmentation_changed = True
                print("SEGMENTATION_MIN_DISTANCE: %f" % SEGMENTATION_MIN_DISTANCE)
            if event.unicode == "+":
                scale += factor
                offset[0] -= (SCREEN_SIZE[0] / 2 - offset[0]) * factor
                offset[1] -= (SCREEN_SIZE[1] / 2 - offset[1]) * factor
            elif event.key == K_MINUS or event.key == K_KP_MINUS:
                scale -= factor
                offset[0] += (SCREEN_SIZE[0] / 2 - offset[0]) * factor
                offset[1] += (SCREEN_SIZE[1] / 2 - offset[1]) * factor

    return running, scale, offset, segmentation_changed, should_export_points, should_export_segments, should_export_screenshot


def scale_surface(surface, scale):
    new_size = [int(x * scale) for x in surface.get_size()]
    surface = pygame.transform.scale(surface, new_size)
    TRACK_SURFACE_SIZE[0] = new_size[0]
    TRACK_SURFACE_SIZE[1] = new_size[1]

    return surface


def scale_points(points, scale):
    scaled_points = [[(val * scale) for val in tuple] for tuple in points]

    return scaled_points


def export_segments(data, n):
    segments = []

    accumulated_distance = data[0][0]
    accumulated_angle = data[0][1]

    size = 2 # COLOCAR COMO CONSTANTE
    i = size
    while i < n:
        if math.fabs(data[i - 2][1] - data[i - 1][1]) > SEGMENTATION_EPSILON and \
           accumulated_distance > SEGMENTATION_MIN_DISTANCE:

            segments.append([accumulated_distance, accumulated_angle])
            accumulated_distance = data[i - 1][0]
            accumulated_angle = data[i - 1][1]
        else:
            accumulated_distance += data[i - 1][0]
            accumulated_angle += data[i - 1][1]

        i += size-1
    
    file_name = get_file_name()
    file_name = os.path.splitext(file_name)[0] + ".segments"

    file = open(file_name, "w")
    print("Exporting segments to file: " + file_name)
    for segment in segments:
        file.write("%f, %f\n" % (segment[0], segment[1]))
    file.close()
    print("Exportation finished.\n")


def export_points(points):
    file_name = get_file_name()
    file_name = os.path.splitext(file_name)[0] + ".xy"

    file = open(file_name, "w")
    print("Exporting points to file: " + file_name)
    for point in points:
        file.write("%f %f\n" % point)
    file.close()
    print("Exportation finished.\n")


def export_screenshot(surface):
    file_name = get_file_name()
    file_name = os.path.splitext(file_name)[0] + ".png"

    print("Exporting screenshot to file: " + file_name)
    pygame.image.save(surface, file_name)
    print("Exportation finished.\n")


def main():
    global SCREEN_SIZE

    file_name = get_file_name()
    data = get_data(file_name)
    scale_factor = get_scale_factor()
    SCREEN_SIZE = get_screen_size()
    points = get_points(data, scale_factor)
    right_points, left_points = get_edge_points(data, points, scale_factor)

    print(help_message)

    pygame.init()
    pygame.key.set_repeat(100, 10)

    window = pygame.display.set_mode(SCREEN_SIZE)
    track_surface = pygame.Surface(TRACK_SURFACE_SIZE, pygame.SRCALPHA, 32)

    draw_all(track_surface, data, points, right_points, left_points)

    offset = [-INITIAL_POINT[0] + SCREEN_SIZE[0] / 2, \
              -INITIAL_POINT[1] + SCREEN_SIZE[1] / 2]
    running = True
    while running:
        running, scale, offset, segmentation_changed, should_export_points, \
        should_export_segments, should_export_screenshot = handle_events(offset)

        window.fill(BLACK)
        #draw_crosshair(window)

        redraw = False
        if scale != 1:
            track_surface = scale_surface(track_surface, scale)
            points = scale_points(points, scale)
            right_points = scale_points(right_points, scale)
            left_points = scale_points(left_points, scale)
            redraw = True

        if segmentation_changed:
            redraw = True

        if redraw:
            draw_all(track_surface, data, points, right_points, left_points)

        if should_export_points:
            export_points(points)

        if should_export_segments:
            export_segments(data, len(right_points))

        window.blit(track_surface, offset)

        if should_export_screenshot:
            export_screenshot(window)

        pygame.display.update()

    pygame.quit()


main()
