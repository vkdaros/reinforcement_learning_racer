/***************************************************************************

    file                 : Slotcar.cpp
    created              : Tue Jul 16 17:42:53 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <cmath>
#include <utility>

#include "Slotcar.h"
#include "StretchData.h"

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! TODO: stretch <-> segment !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

// Returns -1 if x is negative, 1 when x is positive and 0 for zero.
template <typename T> int sign(T x) {
    return (T(0) < x) - (x < T(0));
}

// Gear changing constants.
const int Slotcar::maxRpmAtGear[6] = {0, 7000, 8000, 8000, 8500, 9000};
const int Slotcar::minRpmAtGear[7] = {0, 0, 3500, 4000, 4000, 4500, 4500};

void Slotcar::init(float *angles) {
	// set angles from -90 to 90 degrees with 10 degree step.
    for (int i = 0; i < TRACK_SENSORS_NUM; i++) {
        angles[i] = -90 + i * 10;
    }

    this->lastDistance = 0;
    this->lap = 0;
    this->currentSegment = 0;
}

void Slotcar::onShutdown() {
    return;

    string file_name = string(this->trackName) + ".raw";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    list<StretchData>::iterator i;
    for (i = this->stretches.begin(); i != this->stretches.end(); i++) {
        i->print(outputStream);
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::onRestart() {
    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File track_data.out closed." << endl;
    }
}

CarControl Slotcar::wDrive(CarState cs) {
    float accel;
    float brake;
    int gear;
    float steer;
    float clutch;

    updateLap(cs);
    updeteCurrentSegment(cs);

    if (this->lap <= 1) {
        accel = getSlowAcceleration(cs);
        brake = getSlowBraking(cs);
        gear = selectGear(cs);
        steer = getSteer(cs);
        clutch = 0;
        
        /*accel = 0.25;
        //brake = getSlowBraking(cs);
        brake = getBraking(cs);
        gear = 1;
        steer = getSteer(cs);
        clutch = 0;*/
        registerData(cs, steer);
    } else {
        accel = getAcceleration(cs);
        brake = getBraking(cs);
        gear = selectGear(cs);
        steer = getSteer(cs);
        clutch = 0;
    }
    if (this->lap == 2 && this->segmentsType.empty()) {
        processData();
        //printSegments();
    }

    CarControl cc(accel, brake, gear, steer, clutch);

    // Updating lastDistance only after everything else is done.
    this->lastDistance = cs.getDistFromStart();

    return cc;
}

int Slotcar::selectGear(CarState cs) {
    int gear = cs.getGear();
    int rpm  = cs.getRpm();

    if (gear < 1) {
        gear = 1;
    } else if (gear < 6 && rpm >= Slotcar::maxRpmAtGear[gear]) {
        gear++;
    } else if (gear > 1 && rpm <= Slotcar::minRpmAtGear[gear]) {
        gear--;
    }

    return gear;
}

float Slotcar::getAcceleration(CarState cs) {
    float acceleration = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead > 20 && speed < 60) {
        acceleration = 0.5;
    } else if (distanceAhead < 80 && speed >= 180) {
        acceleration = 0;
    } else if (distanceAhead < 150 && speed >= 200) {
        acceleration = 0;
    } else if (distanceAhead >= 100) {
        acceleration = 1;
    } else if (distanceAhead >= 50) {
        acceleration = 0.8;
    } else if (distanceAhead <= 20 && speed > 50) {
        acceleration = 0;
    } else {
        acceleration = 0.25;
    }

    return acceleration;
}

float Slotcar::getSlowAcceleration(CarState cs) {
    float acceleration = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead > 20 && speed < 60) {
        acceleration = 0.25;
    } else if (distanceAhead < 80 && speed >= 180) {
        acceleration = 0;
    } else if (distanceAhead < 150 && speed >= 200) {
        acceleration = 0;
    } else if (distanceAhead >= 150) {
        acceleration = 0.6;
    } else if (distanceAhead >= 50) {
        acceleration = 0.45;
    } else if (distanceAhead <= 20 && speed > 50) {
        acceleration = 0;
    } else {
        acceleration = 0.15;
    }

    return acceleration;
}

float Slotcar::getBraking(CarState cs) {
    float brake = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead <= 100 && speed > 100) {
        brake = 1.0;
    } else if (distanceAhead < 150 && speed >= 220) {
        brake = 1.0;
    } else if (distanceAhead < 80 && speed >= 180) {
        brake = 1.0;
    } else if (distanceAhead < 50 && speed > 80) {
        brake = 0.5;
    } else if (distanceAhead < 50 && speed > 60) {
        brake = 0.2;
    }

    //return brake;
    return ABS(cs, brake);
}

float Slotcar::getSlowBraking(CarState cs) {
    float brake = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead <= 100 && speed > 100) {
        brake = 1.0;
    } else if (distanceAhead < 50 && speed > 80) {
        brake = 0.5;
    } else if (distanceAhead < 50 && speed > 60) {
        brake = 0.2;
    }

    //return brake;
    return ABS(cs, brake);
}



float Slotcar::ABS(CarState cs, float brake) {
    // Car speed in m/s.
    float speed = cs.getSpeedX() / 3.6;

    float frontWheelsVel;
    float rearWheelsVel;
    float wheelsVel;
    float slip;
    static bool haveLocked = false;

    if (brake == 0) {
        haveLocked = false;
        return brake;
    }

    frontWheelsVel = (cs.getWheelSpinVel(0) + cs.getWheelSpinVel(1)) * FRONT_WHEEL_RADIOUS;
    rearWheelsVel = (cs.getWheelSpinVel(2) + cs.getWheelSpinVel(3)) * REAR_WHEEL_RADIOUS;
    wheelsVel = (frontWheelsVel + rearWheelsVel) / 4;
    slip = speed - wheelsVel;
    
    if (slip > MAX_SLIP) {
        if (!haveLocked) {
            haveLocked = true;
        } else {
            brake = 0;
            haveLocked = false;
        }
    }

    return brake;
}

float Slotcar::getSteer(CarState cs) {
    float targetAngle;
    float steer;

    /* TODO: Improve steering policy to turn harder. */
    /*       Or at least check if it is ok.          */

    // Keep on the midle of the track.
    targetAngle = (cs.getAngle() - cs.getTrackPos());
    steer = targetAngle / 0.366519; // steerLock magic number
    steer *= 3.0;
    if (steer < -1) {
        steer = -1;
    } else if (steer > 1) {
        steer = 1;
    }

    return steer;
}

void Slotcar::registerData(CarState cs, float steer) {
    // If the race hasn't started, there is noting to do.
    if (this->lap <= 0) {
        return;
    }

    // TODO: Usar distFromStart em vez de distRaced!
    static int stretchCounter = 0;
    static float lastRecordedDistance = 0;
    float distanceRaced = cs.getDistRaced();
    float deltaDistance = distanceRaced - lastRecordedDistance;

    if (this->lastDistance > distanceRaced) {
        // A new lap has just started.
        stretchCounter = 0;

        // Force to record the beginning of the first stretch.
        deltaDistance = DISTANCE_STEP + 0.01;
    }
    if (deltaDistance >= DISTANCE_STEP) {
        float angle = cs.getAngle();
        float steerAngle = steer * 0.366519;
        float lapTime = cs.getCurLapTime();
        float distFromStart = cs.getDistFromStart();
        float distRaced = cs.getDistRaced();
        float speedX = cs.getSpeedX();
        float speedY = cs.getSpeedY();
        float speedZ = cs.getSpeedZ();
        float frontRightWheelSpin = cs.getWheelSpinVel(0);
        float frontLeftWheelSpin = cs.getWheelSpinVel(1);
        float rearRightWheelSpin = cs.getWheelSpinVel(2);
        float rearLeftWheelSpin = cs.getWheelSpinVel(3);
        float z = cs.getZ();
        float trackWidth = cs.getTrack(0) + cs.getTrack(TRACK_SENSORS_NUM - 1);

        StretchData stretch(stretchCounter,
                            angle,
                            steerAngle,
                            lapTime,
                            distFromStart,
                            distRaced,
                            speedX,
                            speedY,
                            speedZ,
                            frontRightWheelSpin,
                            frontLeftWheelSpin,
                            rearRightWheelSpin,
                            rearLeftWheelSpin,
                            z,
                            trackWidth);
        this->stretches.push_back(stretch);

        lastRecordedDistance = distanceRaced;
        stretchCounter++;
    }
}

void Slotcar::updateLap(CarState cs) {
    if (cs.getDistFromStart() < this->lastDistance && cs.getSpeedX() > 0) {
        cout << "Lap " << setw(3) << this->lap << " - time: " << fixed << setw(13) << setprecision(3) << cs.getLastLapTime() << endl;
        this->lap++;
        //cout << "Starting lap: " << this->lap << endl;
    }
}

void Slotcar::updeteCurrentSegment(CarState cs) {
    static int lastLap = 0;

    if (this->segmentsEnd.size() <= 0) {
        return;
    }
    if (cs.getDistFromStart() > this->segmentsEnd[this->currentSegment]) {
        this->currentSegment++;
    } else if (this->lap > lastLap) {
        this->currentSegment = 0;
        lastLap = this->lap;
    }
/* For DEBUG:
    else return;
    cout << ">>> Segment: " << setw(2) << this->currentSegment
         << "; dist: " << setprecision(2) << fixed << setw(7)
         << cs.getDistFromStart() << "; ends: " << setw(7)
         << this->segmentsEnd[this->currentSegment] << endl;
*/
}

// Populates angles, distance vectors.
void Slotcar::preprocessData(vector<float>& angles, vector<float>& distances) {
    list<StretchData>::iterator data;
    list<StretchData>::iterator lastData;
    float angleSum = 0;

    for (data = this->stretches.begin(), lastData = data, data++;
         data != this->stretches.end();
         lastData = data, data++) {

        float angle = getTurnAngle(&(*data), &(*lastData));
        float distance = data->getDistRaced() - lastData->getDistRaced();

        angles.push_back(angle);
        distances.push_back(distance);

        angleSum += angle;
    }

    // Adjusts angles such that the sum 2*PI (a complete turn).
    float correctionFactor = abs(2 * M_PI / angleSum);
    for (unsigned int i = 0; i < angles.size(); i++) {
        angles[i] *= correctionFactor;
    }
}

void Slotcar::meanFilter(vector<float>& angles, vector<float>& filteredAngles) {
    for (unsigned int i = 0; i < FILTER_WINDOW; i++) {
        filteredAngles.push_back(angles[i]);
    }
    for (unsigned int i = FILTER_WINDOW; i < angles.size() - FILTER_WINDOW; i++) {
        float mean = 0;
        for (unsigned int j = i - FILTER_WINDOW; j <= i + FILTER_WINDOW; j++) {
            mean += angles[j];
        }
        filteredAngles.push_back(mean / (1 + 2 * FILTER_WINDOW));
    }
    for (unsigned int i = angles.size() - FILTER_WINDOW; i < angles.size(); i++) {
        filteredAngles.push_back(angles[i]);
    }
}

void Slotcar::flatFilter(vector<float>& angles, vector<float>& distances,
                         vector<float>& flatAngles,
                         vector<float>& flatDistances) {
    list<StretchData>::iterator dataS = this->stretches.begin();
    float greaterDiff = 0;
    float currentAngle = 0;
    float lastAngle = 0;
    vector<int> dfs;
    while (dataS != this->stretches.end()) {
        dfs.push_back(dataS->getDistFromStart());

        lastAngle = currentAngle;
        currentAngle = dataS->getAngle();
        if (currentAngle - lastAngle > greaterDiff) {
            greaterDiff = currentAngle - lastAngle;
        }
        dataS++;
    }
    float angleThreshold = greaterDiff * ANGLE_THRESHOLD_FRACTION;
    cout << "AngleThreshold: " << setprecision(8) << angleThreshold << endl;

    flatDistances.push_back(0);
    flatAngles.push_back(0);
    float baseAngle = 0;

    unsigned int end = angles.size();
    for (unsigned int i = 2; i < end - 2; i++) {
        if (abs(angles[i] - baseAngle) >= angleThreshold) {
            float BD = dfs[i - 1];
            float BA = baseAngle;

            float topBackDist = dfs[i];
            float topBackAngle = angles[i];

            while (i < end - 2 && (!isPeak(angles, i) ||
                                   sign(angles[i] - angles[i - 1]) ==
                                   sign(angles[i + 2] - angles[i + 1]))) {
                i++;
            }
            topBackAngle = angles[i];
            if (abs(topBackAngle) < angleThreshold) {
                topBackAngle = 0.0;
            }
            baseAngle = topBackAngle;

            flatDistances.push_back(BD);
            flatAngles.push_back(BA);

            flatDistances.push_back(topBackDist);
            flatAngles.push_back(topBackAngle);
        }
    }
    flatAngles.push_back(baseAngle);
    flatDistances.push_back(dfs[end - 1]);

    // Pos-fix
    unsigned int part = 0;
    bool isBase = true;
    part = flatAngles.size() - 1;
    baseAngle = flatAngles[part];
    isBase = true;
    for (unsigned int i = end - 1; i > 0 && part > 2; i--) {
        if (dfs[i] < flatDistances[part - 1]) {
            part--;
            isBase = false;
            if (abs(flatAngles[part] - flatAngles[part - 1]) < 0.00001) {
                isBase = true;
                baseAngle = flatAngles[part];
            }
        }
        if (abs(angles[i] - baseAngle) >= angleThreshold && isBase && flatAngles[part] == 0) {
            flatDistances[part - 1] = dfs[i + 1];
            flatDistances[part - 2] = dfs[i];
        }
    }
}

bool Slotcar::isPeak(vector<float>& angles, int i) {
    return ((angles[i] > 0 &&
             angles[i] > angles[i - 1] &&
             angles[i] > angles[i + 1]) ||
            (angles[i] < 0 &&
             angles[i] < angles[i - 1] &&
             angles[i] < angles[i + 1]));
}

void Slotcar::processData() {
    // TODO: It is possible to decrease the number of function calls.

    list<StretchData>::iterator data;
    list<StretchData>::iterator lastData;

    vector<float> angles;
    vector<float> distances;
    vector<float> filteredAngles;
    vector<float> flatAngles;
    vector<float> flatDistances;

    preprocessData(angles, distances);
    meanFilter(angles, filteredAngles);
    flatFilter(filteredAngles, distances, flatAngles, flatDistances);

    // To be used with the visualizer.
    writeSegmentsFile(angles, distances);
    writeSignalFile(angles, ".signal");
    writeSignalFile(filteredAngles, ".filteredSignal");
    writeFlatSignalFile(flatAngles, flatDistances, ".flatSignal");

    // ATENTION HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    angles.swap(filteredAngles);

    string file_name = string(this->trackName) + ".cluster";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name
             << " file." << endl;
    }

    // NEW SEGMENTATION
    vector<int> cuts;
    cuts.push_back(0);
    for (unsigned int i = WINDOW_SIZE; i < angles.size() - WINDOW_SIZE; i++) {
        float meanBack = mean(i - WINDOW_SIZE, i, angles);
        //stdBack = standardDeviation(i - WINDOW_SIZE, i, meanBack, angles);
        float stdBack = variance(i - WINDOW_SIZE, i, meanBack, angles);
        
        float meanFront = mean(i, i + WINDOW_SIZE, angles);
        //stdFront = standardDeviation(i, i + WINDOW_SIZE, meanFront, angles);
        float stdFront = variance(i, i + WINDOW_SIZE, meanFront, angles);

        if (pow(meanBack - meanFront, 2) > 9 * max(stdBack, stdFront)) {
            cuts.push_back(i - WINDOW_SIZE);
        }
    }
    unsigned int i;
    float totalDistance = 0;
    float accumulatedDistance;
    float accumulatedAngle;
    for (i = 1; i < cuts.size(); i++) {
        accumulatedDistance = 0;
        accumulatedAngle = 0;
        for (int j = cuts[i - 1]; j < cuts[i]; j++) {
            totalDistance += distances[j];
            accumulatedDistance += distances[j];
            accumulatedAngle += angles[j];
        }
        int type = classifySegment(accumulatedAngle, accumulatedDistance);
        this->segmentsType.push_back(type);
        this->segmentsEnd.push_back(totalDistance);
        // Cluster file to be used with the visualizer.
        outputStream << accumulatedDistance << "," << accumulatedAngle
                     << ",cluster" << type<<endl;
    }
    accumulatedDistance = 0;
    accumulatedAngle = 0;
    for (unsigned int j = cuts[i - 1]; j < distances.size(); j++) {
        totalDistance += distances[j];
        accumulatedDistance += distances[j];
        accumulatedAngle += angles[j];
    }
    int type = classifySegment(accumulatedAngle, accumulatedDistance);
    this->segmentsType.push_back(type);
    this->segmentsEnd.push_back(totalDistance + 20); // Trick!
    outputStream << (accumulatedDistance + 20) << "," << accumulatedAngle
                 << ",cluster" << type<<endl;

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

float Slotcar::getTurnAngle(StretchData *data, StretchData *lastData) {
    float rightWheelSpin = data->getFrontRightWheelSpin();
    float leftWheelSpin = data->getFrontLeftWheelSpin();
    float steer = data->getSteerAngle();

    float outsideWheelSpin;
    int turnDirection;

    if (rightWheelSpin == leftWheelSpin || steer == 0) {
        return 0;
    } else if (steer < 0) {
        // Right turn.
        turnDirection = RIGHT;
        outsideWheelSpin = leftWheelSpin;
    } else {
        // Left turn.
        turnDirection = LEFT;
        outsideWheelSpin = rightWheelSpin;
    }

    float radius = abs(WHEELBASE / sin(steer));
    float deltaT = data->getLapTime() - lastData->getLapTime();
    float distance = FRONT_WHEEL_RADIOUS * outsideWheelSpin * deltaT;
    float angle = turnDirection * distance / (radius + TRACK / 2);

    return angle;
}

int Slotcar::classifySegment(float angle, float distance) {
    float x = abs(angle);
    float y = distance;
    int type;

    if (isStraight(x, y)) {
        if (y <= 50) {
            type = SHORT_STRAIGHT;
        } else if (y <= 150) {
            type = MODERATE_STRAIGHT;
        } else {
            type = LONG_STRAIGHT;
        }
    } else if (isHairpin(x, y)) {
        if (isRightTurn(angle)) {
            type = HAIRPIN_RIGHT;
        } else {
            type = HAIRPIN_LEFT;
        }
    } else if (isSharpTurn(x, y)) {
        if (y <= 100) {
            if (isRightTurn(angle)) {
                type = SHORT_SHARP_RIGHT;
            } else {
                type = SHORT_SHARP_LEFT;
            }
        } else {
            if (isRightTurn(angle)) {
                type = LONG_SHARP_RIGHT;
            } else {
                type = LONG_SHARP_LEFT;
            }
        }
    } else if (y <= 100) {
        if (isRightTurn(angle)) {
            type = SHORT_EASY_RIGHT;
        } else {
            type = SHORT_EASY_LEFT;
        }
    } else {
        if (isRightTurn(angle)) {
            type = LONG_EASY_RIGHT;
        } else {
            type = LONG_EASY_LEFT;
        }
    }
    return type;
}

bool Slotcar::isStraight(float x, float y) {
    return (2500 * x - y <= 50);
}

bool Slotcar::isHairpin(float x, float y) {
    // 1.5625 = 1.25^2
    // 122500 = 350^2
    return (((x - 3) * (x - 3) / 1.5625) + (y * y / 122500) <= 1);
}

bool Slotcar::isSharpTurn(float x, float y) {
    // 5.0625 = 2.25^2
    // 1440000 = 1200^2
    return (((x - 3) * (x - 3) / 5.0625) + (y * y / 1440000) <= 1);
}

bool Slotcar::isRightTurn(float angle) {
    return angle < 0.0;
}

float Slotcar::mean(int first, int last, vector<float>& v) {
    float sum = 0;
    for (int i = first; i <= last; i++) {
        sum += v[i];
    }
    return sum / (last - first + 1);
}

float Slotcar::variance(int first, int last, float mean, vector<float>& v) {
    float sum = 0;
    for (int i = first; i <= last; i++) {
        float difference = v[i] - mean;
        sum += difference * difference;
    }
    return sum / (last - first + 1);
}

float Slotcar::standardDeviation(int first, int last, float mean,
                                 vector<float>& v) {
    return sqrt(variance(first, last, mean, v));
}

void Slotcar::printSegments() {
    string file_name = string(this->trackName) + ".classification";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    for (unsigned int i = 0; i < this->segmentsType.size(); i++) {
        string str;
        switch (this->segmentsType[i]) {
            case SHORT_STRAIGHT:
                str = "SHORT_STRAIGHT";
                break;
            case MODERATE_STRAIGHT:
                str = "MODERATE_STRAIGHT";
                break;
            case LONG_STRAIGHT:
                str = "LONG_STRAIGHT";
                break;
            case HAIRPIN_RIGHT:
                str = "HAIRPIN_RIGHT";
                break;
            case HAIRPIN_LEFT:
                str = "HAIRPIN_LEFT";
                break;
            case SHORT_SHARP_RIGHT:
                str = "SHORT_SHARP_RIGHT";
                break;
            case SHORT_SHARP_LEFT:
                str = "SHORT_SHARP_LEFT";
                break;
            case LONG_SHARP_RIGHT:
                str = "LONG_SHARP_RIGHT";
                break;
            case LONG_SHARP_LEFT:
                str = "LONG_SHARP_LEFT";
                break;
            case SHORT_EASY_RIGHT:
                str = "SHORT_EASY_RIGHT";
                break;
            case SHORT_EASY_LEFT:
                str = "SHORT_EASY_LEFT";
                break;
            case LONG_EASY_RIGHT:
                str = "LONG_EASY_RIGHT";
                break;
            case LONG_EASY_LEFT:
                str = "LONG_EASY_LEFT";
                break;
        }
        cout << setw(2) << i << ". end: " << setw(7) << setprecision(2) << fixed
             << this->segmentsEnd[i] << "; type: " << setw(2) << this->segmentsType[i]
             << " - " + str << endl;
        outputStream << setw(2) << i << ". end: " << setw(6) << setprecision(2)
                     << fixed << this->segmentsEnd[i] << "m; type: "
                     << setw(2) << this->segmentsType[i] << " - " + str << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeSegmentsFile(vector<float>& angles,
                                vector<float>& distances) {

    string file_name = string(this->trackName) + ".segments";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    outputStream << setiosflags(ios::fixed);
    unsigned int size = angles.size();
    for (unsigned int i = 0; i < size; i++) {
        outputStream << setprecision(5) << distances[i] << " "
                     << setprecision(5) << angles[i] << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeSignalFile(vector<float>& angles, string extension) {
    string file_name = string(this->trackName) + extension;
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    list<StretchData>::iterator data = this->stretches.begin();
    outputStream << setiosflags(ios::fixed);
    unsigned int size = angles.size();
    for (unsigned int i = 0; i < size; i++, data++) {

        outputStream << setprecision(5) << data->getDistFromStart() << " "
                     << setprecision(5) << angles[i] << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeFlatSignalFile(vector<float>& angles,
                                  vector<float>& distances, string extension) {
    string file_name = string(this->trackName) + extension;
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    outputStream << setiosflags(ios::fixed);
    unsigned int size = angles.size();
    for (unsigned int i = 0; i < size; i++) {

        outputStream << setprecision(6) << distances[i] << " "
                     << setprecision(6) << angles[i] << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

/*
    // 5 mean-filter.
    file_name = string(this->trackName) + ".filteredSignal";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    data = this->stretches.begin();
    outputStream << setiosflags(ios::fixed);
    for (unsigned int i = 4; i < size; i++, data++) {
        float mean = 0;
        for (unsigned int j = i - 4; j <= i; j++) {
            mean += angles[j];
        }
        mean /= 5;

        outputStream << setprecision(5) << data->getDistRaced() << " "
                     << setprecision(5) << mean << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
*/
/*
    // 10 mean-filter.
    file_name = string(this->trackName) + ".filtered2Signal";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    data = this->stretches.begin();
    outputStream << setiosflags(ios::fixed);
    for (unsigned int i = 9; i < size; i++, data++) {
        float mean = 0;
        for (unsigned int j = i - 9; j <= i; j++) {
            mean += angles[j];
        }
        mean /= 10;

        outputStream << setprecision(5) << data->getDistRaced() << " "
                     << setprecision(5) << mean << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
*/
