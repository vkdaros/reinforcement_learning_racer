/***************************************************************************

    file                 : Slotcar.h
    created              : Tue Jul 16 17:42:53 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SLOTCAR_H_
#define SLOTCAR_H_

#include <fstream>
#include <list>
#include <vector>

#include "BaseDriver.h"
#include "CarState.h"
#include "CarControl.h"
#include "WrapperBaseDriver.h"
#include "StretchData.h"

#define DISTANCE_STEP 5 // Magic number to data work better in visualizer.
#define PI 3.14159265359

#define MAX_SLIP   0.277 * 0.1
#define WINDOW_SIZE 5 // Size of sample in segmentation.
#define FILTER_WINDOW 1//3
#define FLAT_WINDOW 4
#define ANGLE_THRESHOLD_FRACTION 0.222

#define WHEELBASE 4.5 // Distance between centers of front and rear wheels.
#define TRACK     1.9 // Distance between centers of two wheels on same axle.
#define FRONT_WHEEL_RADIOUS 0.3306
#define REAR_WHEEL_RADIOUS  0.3250

#define RIGHT (-1)
#define LEFT   (1)

// Segment types for classification.
#define SHORT_STRAIGHT         0
#define MODERATE_STRAIGHT      1
#define LONG_STRAIGHT          2
#define HAIRPIN_RIGHT        (-3)
#define HAIRPIN_LEFT           3
#define SHORT_SHARP_RIGHT    (-4)
#define SHORT_SHARP_LEFT       4
#define LONG_SHARP_RIGHT     (-5)
#define LONG_SHARP_LEFT        5
#define SHORT_EASY_RIGHT     (-6)
#define SHORT_EASY_LEFT        6
#define LONG_EASY_RIGHT      (-7)
#define LONG_EASY_LEFT         7

using namespace std;

class Slotcar : public WrapperBaseDriver {
  public:
	// Constructor.
	Slotcar(){};

	// Initialization of the desired angles for the rangefinders.
	virtual void init(float *angles);

	// Called at the end of the race, before being unloaded.
	virtual void onShutdown();

	// Called when the race is restarted.
	virtual void onRestart();

    // Change the effectors values in order to drive the car.
    virtual CarControl wDrive(CarState cs);

  protected:
    // Gear changing constants.
    static const int maxRpmAtGear[6];
    static const int minRpmAtGear[7];

    // File where the sensor readings will be written
    ofstream outputStream;

    // Distance from start measured in the last tick.
    float lastDistance;

    // Lap counter. If equals to zero, then the first lap hasn't started yet.
    int lap;

    unsigned int currentSegment;

    // List of scanned data of track stretches.
    list<StretchData> stretches;

    // Vector of classified segments which compose the track.
    vector<int> segmentsType;

    // Distance from start of the end of each segment.
    vector<float> segmentsEnd;

    // Select to gear up or down according to RPM.
    int selectGear(CarState cs);

    // Returns acceleartion based on the distance to the next bend.
    virtual float getAcceleration(CarState cs);
    virtual float getSlowAcceleration(CarState cs);

    // Returns braking based on the distance to the next bend.
    virtual float getBraking(CarState cs);
    virtual float getSlowBraking(CarState cs);

    // Returns brake after pass through ABS filter.
    float ABS(CarState cs, float brake);

    // Returns braking based on the distance to the next bend.
    float getSteer(CarState cs);

    // Writes on output file relevant current state data.
    void registerData(CarState cs, float steer);

    // Responsible for updating lap counter.
    void updateLap(CarState cs);

    // Update in which segment the car is.
    void updeteCurrentSegment(CarState cs);

    // Process registered data and fill segments list.
    void preprocessData(vector<float>& angles, vector<float>& distances);
    void meanFilter(vector<float>& angles, vector<float>& filteredAngles);
    void flatFilter(vector<float>& angles, vector<float>& distances,
                    vector<float>& flatAngles, vector<float>& flatDistances);
    bool isPeak(vector<float>& angles, int i);
    void processData();
    float getTurnAngle(StretchData *data, StretchData *lastData);

    // Return the type of the segment according to turn angle and length.
    int classifySegment(float angle, float distance);
    bool isStraight(float x, float y);
    bool isHairpin(float x, float y);
    bool isSharpTurn(float x, float y);
    bool isRightTurn(float angle);

    float mean(int first, int last, vector<float>& v);
    float variance(int first, int last, float mean, vector<float>& v);
    float standardDeviation(int first, int last, float mean, vector<float>& v);

    // Just for debug.
    void printSegments();
    void writeSegmentsFile(vector<float>& angles, vector<float>& distances);
    void writeSignalFile(vector<float>& angles, string extension);
    void writeFlatSignalFile(vector<float>& angles, vector<float>& distances,
                             string extension);
};
#endif /* SLOTCAR_H_ */
