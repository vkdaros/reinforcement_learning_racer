/***************************************************************************

    file                 : SlotcarRL.cpp
    created              : Wed Jul 24 18:49:42 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cmath>
#include <cstdlib>
#include <ctime>

#include "SlotcarRL.h"
#include "CarState.h"

int Q[N_STATES][N_ACTIONS];

void SlotcarRL::init(float *angles) {
    Slotcar::init(angles);

    for (int i = 0; i < N_STATES; i++) {
        for (int j = 0; j < N_ACTIONS; j++) {
            Q[i][j] = 0;
        }
    }

    this->lastState = -1;
    this->lastAction = -1;
    srand(time(NULL));
}

/* TODO: o fast estah fazendo curva melhor na aalborg. */
float SlotcarRL::getAcceleration(CarState cs) {
    float acceleration = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead > 20 && speed < 60) {
        acceleration = 0.5;
    } else if (distanceAhead < 80 && speed >= 180) {
        acceleration = 0;
    } else if (distanceAhead < 150 && speed >= 200) {
        acceleration = 0;
    } else if (distanceAhead >= 100) {
        acceleration = 1;
    } else if (distanceAhead >= 50) {
        acceleration = 0.8;
    } else if (distanceAhead <= 20 && speed > 50) {
        acceleration = 0;
    } else {
        acceleration = 0.25;
    }

    return acceleration;
}

float SlotcarRL::getBraking(CarState cs) {
    float brake = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead <= 100 && speed > 100) {
        brake = 1.0;
    } else if (distanceAhead < 150 && speed >= 220) {
        brake = 1.0;
    } else if (distanceAhead < 80 && speed >= 180) {
        brake = 1.0;
    } else if (distanceAhead < 50 && speed > 80) {
        brake = 0.5;
    } else if (distanceAhead < 50 && speed > 60) {
        brake = 0.2;
    }

    //return brake;
    return ABS(cs, brake);
}

int SlotcarRL::getCurrentState(CarState cs) {
    static int lastLap = 1;
    int speedState;
    int nearEnd;
    int stretchType;
    int nextStretchType;

    if (this->lap > lastLap) {
        lastLap = this->lap;
        return FINAL_STATE;
    }

    float speed = cs.getSpeedX();
    if (speed <= 20) {
        // TODO: Punish speed <= 0;
        speedState = 0;
    } else if (speed <= 60) {
        speedState = 1;
    } else if (speed <= 120) {
        speedState = 2;
    } else {
        speedState = 3;
    }

    unsigned int s = this->currentSegment;
    float segmentLength;
    float position;
    if (s == 0) {
        segmentLength = this->segmentsEnd[s];
        position = cs.getDistFromStart();
    } else {
        segmentLength = this->segmentsEnd[s] - this->segmentsEnd[s - 1];
        position = cs.getDistFromStart() - this->segmentsEnd[s - 1];
    }
    float closeToEnd = segmentLength * 0.075;
    if (position >= closeToEnd) {
        nearEnd = 1;
    } else {
        nearEnd = 0;
    }

    // Consider only the turn intensity. Ignore left/right.
    stretchType = abs(this->segmentsType[s]);
    if (s + 1 >= this->segmentsType.size()) {
        nextStretchType = abs(this->segmentsType[0]);
    } else {
        nextStretchType = abs(this->segmentsType[s + 1]);
    }

    return 128 * speedState + 64 * nearEnd + 8 * stretchType + nextStretchType;
}

int SlotcarRL::chooseAction(int state) {
    int action;
    float r = ((double) rand() / (RAND_MAX));

    if (r < EPSILON) {
        action = rand() % N_ACTIONS;
    } else {
        action = chooseBestAction(state);
    }
    return action;
}

int SlotcarRL::chooseBestAction(int state) {
    int bestAction = 0;

    for (int action = 1; action < N_ACTIONS; action++) {
        if (Q[state][action] > Q[state][bestAction]) {
            bestAction = action;
        } else if (Q[state][action] == Q[state][bestAction]) {
            int r = rand() % 2;
            if (r == 0) {
                bestAction = action;
            }
        }
    }
    return bestAction;
}

int SlotcarRL::getReward(int state) {
    if (state == FINAL_STATE) {
        return 0;
    }
    return -1;
}

/*
    Server updates sensors (CarState cs);
    Server call cc = wDrive(cs);
    Server waits 10ms;
    If cc is not updated, use last cc.
*/

/*
drive()
    updateLap;
    choose accel, brake etc.;
    create new cc;
    return cc;
*/

/*
lap = episode
step = timeTick


*/

/*
Q-Learning
    for all states:
        for all actions:
            Q(s, a) = arbritraryValue

    for all episodes:
        s = initialState
        for all steps:
            a = ChooseAction(s)
            r, s' = apply(a)
            a'= bestActionFor(s') // a' that maximize Q(s', a')
            Q(s, a) += alpha * (r + gama * Q(s', a') - Q(s, a))
            s = s'
*/

/*
Q-Learning Plus
    for all states:
        for all actions:
            Q(s, a) = 0

    for each lap:
        s = getState;
        lastA = ChooseAction(s);

        for each drive() call:
            sAtual = getState;
            r = R(sAtual);
            nextA = bestActionFor(sAtual) // a' that maximize Q(s', a')
            if (sAtual is not terminal) {
                Q(sAntigo, lastA) += alpha * (r + gama * Q(sAtual, nextA) - Q(sAntigo, lastA))
            } else {
                Q(sAtigo, lastA) += alpha * r
            }
            sAntigo = sAtual
            lastA = nextA;
*/

CarControl SlotcarRL::wDrive(CarState cs) {
    float accel;
    float brake;
    int gear;
    float steer;
    float clutch;

    Slotcar::updateLap(cs);
    Slotcar::updeteCurrentSegment(cs);

    if (this->lap <= 0) {
        // No learning until race starts.
        accel = 0.25;
        brake = Slotcar::getSlowBraking(cs);
        gear = 1;
        steer = Slotcar::getSteer(cs);
        clutch = 0;
        Slotcar::registerData(cs, steer);
    } else if (this-> lap == 1) {
        /*accel = getAcceleration(cs);
        brake = getBraking(cs);
        gear = selectGear(cs);
        steer = getSteer(cs);
        clutch = 0;*/

        accel = 0.25;
        brake = Slotcar::getSlowBraking(cs);
        gear = 1;
        steer = Slotcar::getSteer(cs);
        clutch = 0;
        Slotcar::registerData(cs, steer);
    } else {
        if (this->lap == 2 && this->segmentsType.empty()) {
            Slotcar::processData();
            Slotcar::printSegments();
        }

        int state = getCurrentState(cs);
        int reward = getReward(state);
        //int action = chooseAction(state);
        int action = chooseBestAction(state);

        if (lastState != -1) {
            if (state != FINAL_STATE) {
                Q[this->lastState][this->lastAction] += ALPHA * (reward + GAMA *
                    Q[state][action] - Q[this->lastState][this->lastAction]);
            } else {
                Q[this->lastState][this->lastAction] += ALPHA * reward;
            }
        }
        this->lastState = state;
        this->lastAction = action;

        action = action / ((N_ACTIONS - 1) / 2) - 1;
        if (action >= 0) {
            accel = action;
            brake = 0;
        } else {
            accel = 0;
            brake = (-1) * action;
        }

        gear = Slotcar::selectGear(cs);
        steer = Slotcar::getSteer(cs);
        clutch = 0;
    }

    CarControl cc(accel, brake, gear, steer, clutch);

    // Updating lastDistance only after everything else is done.
    this->lastDistance = cs.getDistFromStart();

    return cc;
}
