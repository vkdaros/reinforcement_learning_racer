/***************************************************************************

    file                 : SlotcarRL.h
    created              : Wed Jul 24 18:49:42 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SLOTCAR_RL_H_
#define SLOTCAR_RL_H_

#include "CarState.h"
#include "CarControl.h"
#include "Slotcar.h"

#define N_STATES 512
#define N_ACTIONS 9
#define FINAL_STATE (-1)

#define EPSILON 0 // Greedy.
#define ALPHA   0.1
#define GAMA    1

using namespace std;

class SlotcarRL : public Slotcar {
  public:
	// Constructor.
	SlotcarRL(){};

	// Initialization of Q and R. Call Slotcar::init() to handle angles.
	virtual void init(float *angles);
    virtual CarControl wDrive(CarState cs);

  protected:
    // Returns acceleartion based on the distance to the next bend.
    virtual float getAcceleration(CarState cs);

    // Returns braking based on the distance to the next bend.
    virtual float getBraking(CarState cs);

  private:
    int lastState;
    int lastAction;
    
    int getCurrentState(CarState cs);
    int chooseAction(int state);
    int chooseBestAction(int state);
    int getReward(int state);
};
#endif /* SLOTCAR_RL_H_ */
