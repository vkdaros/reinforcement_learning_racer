/***************************************************************************

    file                 : StretchData.h
    created              : Wed Oct 3 15:06:57 BRT 2012
    copyright            : (C) 2012 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef STRETCHDATA_H_
#define STRETCHDATA_H_

#include <fstream>

using namespace std;

class StretchData {
  public:
    StretchData(int stretchNumber = 0, 
                float angle = 0,
                float steerAngle = 0,
                float lapTime = 0,
                float distFromStart = 0,
                float distRaced = 0,
                float speedX = 0,
                float speedY = 0,
                float speedZ = 0,
                float frontRightWheelSpin = 0,
                float frontLeftWheelSpin = 0,
                float rearRightWheelSpin = 0,
                float rearLeftWheelSpin = 0,
                float z = 0,
                float trackWidth = 0);

    int getStretchNumber();
    void setStretchNumber(int number);

    float getAngle();
    void setAngle(float angle);
    
    float getSteerAngle();
    void setSteerAngle(float steerAngle);

    float getLapTime();
    void setLapTime(float lapTime);

    float getDistFromStart();
    void setDistFromStart(float distFromStart);

    float getDistRaced();
    void setDistRaced(float distRaced);

    float getSpeedX();
    void setSpeedX(float speedX);

    float getSpeedY();
    void setSpeedY(float speedY);

    float getSpeedZ();
    void setSpeedZ(float speedZ);

    float getFrontRightWheelSpin();
    void setFrontRightWheelSpin(float frontRightWheelSpin);

    float getFrontLeftWheelSpin();
    void setFrontLeftWheelSpin(float frontLeftWheelSpin);

    float getRearRightWheelSpin();
    void setRearRightWheelSpin(float rearRightWheelSpin);

    float getRearLeftWheelSpin();
    void setRearLeftWheelSpin(float rearLeftWheelSpin);

    float getZ();
    void setZ(float z);

    float getTrackWidth();
    void setTrackWidith(float width);

    void print(ofstream& outputStream);

  private:
    // Index of the stretch starting from the beginning of the track.
    int stretchNumber;

    // Angle between last strectch direction and current stretch direction.
    // Range: (-PI, PI) (rad).
    float angle;

    float steerAngle;
    float lapTime;

    // Distance of the beginning of the stretch with respect to the beginning
    // of the track.
    float distFromStart;
    float distRaced;
    float speedX;
    float speedY;
    float speedZ;
    float frontRightWheelSpin;
    float frontLeftWheelSpin;
    float rearRightWheelSpin;
    float rearLeftWheelSpin;
    float z;

    // Width of the track in the stretch.
    float trackWidth;
};

#endif // STRETCHDATA_H_

