#!/usr/bin/python

import sys
from xml.dom import minidom

if len(sys.argv) < 3:
    print("ERROR: no xml file provided\n\tUsage: race_config.py <FILE> <TRACK_NAME>\n")
    sys.exit()

xml_file_name = sys.argv[1]
xml_file = open(xml_file_name, "r")
xml_str = xml_file.read()
xml_file.close()

dom = minidom.parseString(xml_str)

sections = dom.getElementsByTagName("section")

current_track_name = sys.argv[2]
current_track_category = "road"
current_race_mode = "results only"
current_race_laps = "1"

for section in sections:
    name = section.getAttribute("name")
    if name == "Tracks":
        attstrs = section.getElementsByTagName("attstr")
        for attstr in attstrs:
            att_name = attstr.getAttribute("name")
            if att_name == "name":
                attstr.setAttribute("val", current_track_name)
            elif att_name == "category":
                attstr.setAttribute("val", current_track_category)
    elif name == "Quick Race":
        attstrs = section.getElementsByTagName("attstr")
        for attstr in attstrs:
            att_name = attstr.getAttribute("name")
            if att_name == "display mode":
                attstr.setAttribute("val", current_race_mode)

        attnums = section.getElementsByTagName("attnum")
        for attnum in attnums:
            att_name = attnum.getAttribute("name")
            if att_name == "laps":
                attnum.setAttribute("val", current_race_laps)

xml_file = open(xml_file_name, "w")
xml_file.write(dom.toxml())
xml_file.close()
