#set term wxt size 1200,720
set term pngcairo size 1200,720
set output "signals.png"

set grid

set autoscale fix
set xtic auto
set ytic auto

set xlabel "Distance (m)"
set ylabel "Angle (rad)"

plot "unknown.signal"         with lines lw 2 lt rgb "#32CD32" title "Raw signal", \
     "unknown.filteredSignal" with lines lw 2 lc 1 title "Mean filter", \
     "unknown.flatSignal"     with lines lw 2 lc 3 title "Flat filter"

#replot
