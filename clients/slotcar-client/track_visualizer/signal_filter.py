#!/usr/bin/python

import os
import sys
import math
import numpy

error_message = """
ERROR: no input file provided.
    Usage: signal_filter.py <FILE1> [FILE2, FILE3, ..]
"""

WINDOW_SIZE = 4
FILTER_WINDOW_SIZE = 3


def get_file_names():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()

    return sys.argv[1:]


def get_data(file_name):
    file = open(file_name, "r")
    data = [[float(val) for val in line.split()] for line in file.readlines()]
    file.close()

    # data = [[dist, angle], [dist, angle], ... ]
    return data


def filter_data(data):
    filtered = []
    for i in range(WINDOW_SIZE):
        filtered.append([data[i][0], 0])
        #print(str(filtered[-1][0]) + " " + str(filtered[-1][1]))
    i = WINDOW_SIZE
    while i < len(data):
        angles = [d[1] for d in data[i - WINDOW_SIZE:i]]
        mean_back = numpy.average(angles)
        std_back = numpy.std(angles)

        if math.fabs(data[i][1]) > 3 * std_back:
            base_back = list(data[i - 1])
            peak_back = list(data[i])
            peak_front = list(data[i])
            #print("..... peak_front:  " + str(peak_front[0]) + " " + str(peak_front[1]))
            if i + 1 < len(data):
                base_front = list(data[i + 1])
            else:
                base_front = -1

            j = i + 1
            crossed_zero = False
            still_good = True
            while j < len(data) and still_good:
                still_good = False;
                same_signal = (data[j][1] * data[j - 1][1] >= 0)
                if (same_signal and not crossed_zero and math.fabs(data[j][1]) >= math.fabs(data[j - 1][1])) or \
                   (not same_signal and not crossed_zero) or \
                   (same_signal and crossed_zero and math.fabs(data[j][1]) >= math.fabs(data[j - 1][1])):
                    still_good = True
                    peak_back[1] = data[j][1]
                    peak_front = list(data[j])
                    #print("....+ peak_front:  " + str(peak_front[0]) + " " + str(peak_front[1]))
                    if j + 1 < len(data):
                        base_front = list(data[j + 1])
                    else:
                        base_front = -1
                    j += 1
                if not same_signal:
                    crossed_zero = True

            #print (str(i) + " " + str(j) + " " + str(len(data)) + " " + str(data[j][1]) + " " + str(data[i][1]))
            while j < len(data) and math.fabs(data[j][1] - data[i][1]) \
                  <= (2 / 3) * math.fabs(data[i][1]):
                peak_front[0] = data[j][0]
                #print("...++ peak_front:  " + str(peak_front[0]) + " " + str(peak_front[1]))
                if j + 1 < len(data):
                    base_front = list(data[j + 1])
                else:
                    base_front = -1
                j += 1

            crossed_zero = False
            still_good = True
            while j < len(data) and still_good:
                still_good = False;
                same_signal = (data[j][1] * data[j - 1][1] >= 0)
                increasing = (math.fabs(data[j][1]) >= math.fabs(data[j - 1][1]))
                decreasing = (math.fabs(data[j][1]) <= math.fabs(data[j - 1][1]))

                #print(str(j) + " " + str(data[j][1]) + " " + str(same_signal) + " " + str(increasing) + " " + str(decreasing))
                if (same_signal and not crossed_zero and decreasing) or \
                   (not same_signal and not crossed_zero) or \
                   (same_signal and crossed_zero and increasing):
                    still_good = True
                    peak_front[0] = data[j - 1][0]
                    #print("..+++ peak_front:  " + str(peak_front[0]) + " " + str(peak_front[1]))
                    base_front = list(data[j])
                    j += 1
                if not same_signal:
                    crossed_zero = True

            #print(">>> " + str(data[i][0]))
            if base_back[0] > filtered[-1][0]:
                #print("base_back: " + str(base_back[0]))
                filtered.append(base_back)
            if peak_back[0] != peak_front[0]:
                if peak_back[0] > filtered[-1][0]:
                    #print("peak_back: " + str(peak_back))
                    filtered.append(peak_back)
                #print("peak_front: " + str(peak_front[0]))
                filtered.append(peak_front)
            if base_front != -1:
                #print("base_front: " + str(base_front))
                filtered.append(base_front)
            i = j - 1
        i += 1

    return filtered


def write_file(file_name, segments):
    file = open(os.path.splitext(file_name)[0] + ".filteredNew", "w")
    for segment in segments:
        file.write("%f %f\n" % (segment[0], segment[1]))
    file.close()


def main():
    file_names = get_file_names()
    for file_name in file_names:
        data = get_data(file_name)
        filtered_data = filter_data(data)
        write_file(file_name, filtered_data)


main()
