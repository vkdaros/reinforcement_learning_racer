/***************************************************************************

    file                 : Slotcar.cpp
    created              : Tue Jul 16 17:42:53 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <utility>

#include <sys/stat.h>
#include <sys/types.h>

#include "Slotcar.h"
#include "StretchData.h"

#define PI 3.14159265359

vector<float> angles;
vector<float> distances;
vector<float> filteredAngles;
vector<float> flatAngles;
vector<float> accAngles;
vector<float> flatDistances;
list<float> lapTimes;

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! TODO: stretch <-> segment !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

// Labels used to describe states.
const char* STRETCH_TYPES[N_TYPES] = {
    "SHORT_STRAIGHT",
    "MEDIUM_STRAIGHT",
    "LONG_STRAIGHT",
    "HAIRPIN",
    "ELBOW",
    "SHORT_HARD",
    "LONG_HARD",
    "SHORT_MEDIUM",
    "LONG_MEDIUM",
    "SHORT_EASY",
    "LONG_EASY"};

// Returns -1 if x is negative, 1 when x is positive and 0 for zero.
template <typename T> int sign(T x) {
    return (T(0) < x) - (x < T(0));
}

// Gear changing constants.
const int Slotcar::maxRpmAtGear[6] = {0, 7000, 8000, 8000, 8500, 9000};
const int Slotcar::minRpmAtGear[7] = {0, 0, 3500, 4000, 4000, 4500, 4500};
const int Slotcar::SENSORS[TRACK_SENSORS_NUM] = {-90, -45, -30, -24, -19, -14,
                                                   -9, -6, -3, 0, 3, 6, 9, 14,
                                                   19, 24, 30, 45, 90};

// Stuck constants.
const int Slotcar::stuckTime = 20;
const float Slotcar::stuckAngle = 0.785398163; // (PI / 4)

void Slotcar::init(float *angles) {
	// set angles from -90 to 90 degrees with 10 degree step.
    for (int i = 0; i < TRACK_SENSORS_NUM; i++) {
        angles[i] = SENSORS[i];
    }

    this->lastDistance = 0;
    this->lastCurLapTime = 0;
    this->lap = 0;
    this->currentSegment = 0;
    this->recovering = false;
    this->trackLength = 0.0;
}

void Slotcar::onShutdown() {
    // To be used with the visualizer.
    writeSegmentsFile(angles, distances);
    writeSignalFile(angles, ".signal");
    writeSignalFile(filteredAngles, ".filteredSignal");
    writeFlatSignalFile(flatAngles, flatDistances, ".flatSignal");

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File track_data.out closed." << endl;
    }
    lapTimes.push_back(this->lastCurLapTime);
    writeLapTimes();
}

void Slotcar::onRestart() {
    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File track_data.out closed." << endl;
    }
    lapTimes.push_back(this->lastCurLapTime);
    writeLapTimes();
}

CarControl Slotcar::wDrive(CarState cs) {
    static bool first_alignment = true;

    float accel;
    float brake;
    int gear;
    float steer;
    float clutch;

    updateLap(cs);
    updateCurrentSegment(cs);

    // check if car is currently stuck
    if (fabs(cs.getAngle()) > Slotcar::stuckAngle) {
        // update stuck counter
        this->stuckCounter++;
    } else if (!this->recovering) {
        // if not stuck reset stuck counter
        this->stuckCounter = 0;
    }

    // If car is really stuck, apply recovering policy and stops learning.
    if (this->stuckCounter > Slotcar::stuckTime) {
        if (!this->recovering) {
            cout << "===> Recovering from crash." << endl;
        }
        this->recovering = true;
    }

    if (this->recovering) {
        accel = 0.7;
        brake = 0.0;
        clutch = 0;
        /* set gear and sterring command assuming car is
         * pointing in a direction out of track */

        // to bring car parallel to track axis
        steer = -cs.getAngle() / STEER_LOCK;
        steer = max<float>(min<float>(1.0, steer), -1.0);
        gear = -1; // gear R

        // If the car is pointing in the correct direction, keep with reverse
        // gear for a while and then revert gear and steer.
        if (cs.getAngle() * cs.getTrackPos() >= 0 &&
            !(fabs(cs.getTrackPos()) < 0.1 && cs.getSpeedX() < 0)) {
            gear = 1;
            accel = 0.8;
            steer *= -1;
        }

        // Calculate clutching
        //clutching(cs,clutch);

        if (fabs(cs.getAngle()) < 0.75 * Slotcar::stuckAngle &&
            fabs(cs.getTrackPos()) < 0.8) {

            this->recovering = false;
            cout << "===> Back on track!" << endl;
        }
    } else {
        // Before start, use reverse gear to align the car
        if (first_alignment) {
            accel = 0.5;
            brake = 0.0;
            gear = -1;
            steer = getReverseSteer(cs);
            clutch = 0;

            // If angle is lower than 5 degree and is less than 3 percent away
            // from the middle of the track, then it is aligned.
            if (fabs(cs.getAngle()) < 0.087266463 &&
                fabs(cs.getTrackPos()) < 0.03) {
                cout << "===> CAR ALIGNED! >>> angle: " << cs.getAngle()
                     << "; trackPos: " << cs.getTrackPos() << endl;
                first_alignment = false;
            }
        } else if (this->lap < 1) {
            // No registering before first lap starts.
            if (cs.getSpeedX() < 0) {
                accel = 0.0;
                brake = 0.3;
                gear = 0;
                steer = 0.0;
            } else {
                accel = 0.7;
                brake = Slotcar::getSlowBraking(cs);
                gear = 1;
                steer = Slotcar::getSteer(cs);
            }
            clutch = 0;
        } else if (this->lap == 1) {
            accel = getSlowAcceleration(cs);
            brake = getSlowBraking(cs);
            gear = selectGear(cs);
            steer = getSteer(cs);
            clutch = 0;

            registerData(cs, steer);
        } else {
            accel = getAcceleration(cs);
            brake = getBraking(cs);
            gear = selectGear(cs);
            steer = getSoftSteer(cs);
            clutch = 0;
        }
    }

    if (this->lap == 2 && segmentsType.empty()) {
        processData();
        //printSegments();
    }

    CarControl cc(accel, brake, gear, steer, clutch);

    // Updating lastDistance only after everything else is done.
    this->lastDistance = cs.getDistFromStart();
    this->lastCurLapTime = cs.getCurLapTime();

    return cc;
}

int Slotcar::selectGear(CarState &cs) {
    int gear = cs.getGear();
    int rpm  = cs.getRpm();

    if (gear < 1) {
        gear = 1;
    } else if (gear < 6 && rpm >= Slotcar::maxRpmAtGear[gear]) {
        gear++;
    } else if (gear > 1 && rpm <= Slotcar::minRpmAtGear[gear]) {
        gear--;
    }

    return gear;
}

float Slotcar::getAcceleration(CarState &cs) {
    float acceleration = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead > 20 && speed < 60) {
        acceleration = 0.5;
    } else if (distanceAhead < 80 && speed >= 180) {
        acceleration = 0;
    } else if (distanceAhead < 150 && speed >= 200) {
        acceleration = 0;
    } else if (distanceAhead >= 100) {
        acceleration = 1;
    } else if (distanceAhead >= 50) {
        acceleration = 0.8;
    } else if (distanceAhead <= 20 && speed > 50) {
        acceleration = 0;
    } else {
        acceleration = 0.25;
    }

    return acceleration;
}

float Slotcar::getSlowAcceleration(CarState &cs) {
    float acceleration = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead > 20 && speed < 60) {
        acceleration = 0.25;
    } else if (distanceAhead < 80 && speed >= 180) {
        acceleration = 0;
    } else if (distanceAhead < 150 && speed >= 200) {
        acceleration = 0;
    } else if (distanceAhead >= 150) {
        acceleration = 0.6;
    } else if (distanceAhead >= 50) {
        acceleration = 0.45;
    } else if (distanceAhead <= 20 && speed > 50) {
        acceleration = 0;
    } else {
        acceleration = 0.15;
    }

    return acceleration;
}

float Slotcar::getBraking(CarState &cs) {
    float brake = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead <= 100 && speed > 100) {
        brake = 1.0;
    } else if (distanceAhead < 150 && speed >= 220) {
        brake = 1.0;
    } else if (distanceAhead < 80 && speed >= 180) {
        brake = 1.0;
    } else if (distanceAhead < 50 && speed > 80) {
        brake = 0.5;
    } else if (distanceAhead < 50 && speed > 60) {
        brake = 0.2;
    }

    //return brake;
    return Slotcar::ABS(cs, brake);
}

float Slotcar::getSlowBraking(CarState &cs) {
    float brake = 0;
    float distanceAhead = cs.getTrack(9);
    float speed = cs.getSpeedX();

    if (distanceAhead <= 100 && speed > 100) {
        brake = 1.0;
    } else if (distanceAhead < 50 && speed > 80) {
        brake = 0.5;
    } else if (distanceAhead < 50 && speed > 60) {
        brake = 0.2;
    }

    //return brake;
    return Slotcar::ABS(cs, brake);
}

float Slotcar::ABS(CarState &cs, float brake) {
    // Car speed in m/s.
    float speed = cs.getSpeedX() / 3.6;

    float frontWheelsVel;
    float rearWheelsVel;
    float wheelsVel;
    float slip;
    static bool haveLocked = false;

    if (brake == 0) {
        haveLocked = false;
        return brake;
    }

    frontWheelsVel = (cs.getWheelSpinVel(0) + cs.getWheelSpinVel(1)) * FRONT_WHEEL_RADIUS;
    rearWheelsVel = (cs.getWheelSpinVel(2) + cs.getWheelSpinVel(3)) * REAR_WHEEL_RADIUS;
    wheelsVel = (frontWheelsVel + rearWheelsVel) / 4;
    slip = speed - wheelsVel;
    
    if (slip > MAX_SLIP) {
        if (!haveLocked) {
            haveLocked = true;
        } else {
            brake = 0;
            haveLocked = false;
        }
    }

    return brake;
}

float Slotcar::getSteer(CarState &cs) {
    float targetAngle;
    float steer;

    // Keep on the midle of the track.
    targetAngle = cs.getAngle() - cs.getTrackPos() * 0.5;
    steer = targetAngle / STEER_LOCK;

    // Turn hard. There is no problem when car is slow.
    steer *= 3.0;
    steer = max<float>(min<float>(1.0, steer), -1.0);

    return steer;
}

// Steer angle for using reverse gear.
float Slotcar::getReverseSteer(CarState &cs) {
    float targetAngle;
    float steer;

    // Keep on the midle of the track.
    targetAngle = -cs.getAngle() - cs.getTrackPos() * 0.5;
    steer = targetAngle / STEER_LOCK;

    // Turn hard. There is no problem when car is slow.
    steer *= 3.0;
    steer = max<float>(min<float>(1.0, steer), -1.0);

    return steer;
}


float Slotcar::getSoftSteer(CarState &cs) {
    float targetAngle;
    float steer;

    // Keep on the midle of the track.
    targetAngle = cs.getAngle() - cs.getTrackPos() * STEER_LOCK;
    steer = targetAngle / STEER_LOCK;
    steer = max<float>(min<float>(1.0, steer), -1.0);

    return steer;
}

void Slotcar::registerData(CarState &cs, float steer) {
    // If the race hasn't started, there is noting to do.
    if (this->lap <= 0) {
        return;
    }

    static int stretchCounter = 0;
    static float lastRecordedDistance = 0;
    float distanceRaced = cs.getDistFromStart();
    float deltaDistance = distanceRaced - lastRecordedDistance;

    if (this->lastDistance > distanceRaced) {
        // A new lap has just started.
        stretchCounter = 0;

        // Force to record the beginning of the first stretch.
        deltaDistance = DISTANCE_STEP + 0.01;
    }
    if (deltaDistance >= DISTANCE_STEP) {
        float angle = cs.getAngle();
        float steerAngle = steer * 0.366519;
        float lapTime = cs.getCurLapTime();
        float distFromStart = cs.getDistFromStart();
        float distRaced = cs.getDistRaced();
        float speedX = cs.getSpeedX();
        float speedY = cs.getSpeedY();
        float speedZ = cs.getSpeedZ();
        float frontRightWheelSpin = cs.getWheelSpinVel(0);
        float frontLeftWheelSpin = cs.getWheelSpinVel(1);
        float rearRightWheelSpin = cs.getWheelSpinVel(2);
        float rearLeftWheelSpin = cs.getWheelSpinVel(3);
        float z = cs.getZ();
        float trackWidth = cs.getTrack(0) + cs.getTrack(TRACK_SENSORS_NUM - 1);

        StretchData stretch(stretchCounter,
                            angle,
                            steerAngle,
                            lapTime,
                            distFromStart,
                            distRaced,
                            speedX,
                            speedY,
                            speedZ,
                            frontRightWheelSpin,
                            frontLeftWheelSpin,
                            rearRightWheelSpin,
                            rearLeftWheelSpin,
                            z,
                            trackWidth);
        this->stretches.push_back(stretch);

        lastRecordedDistance = distanceRaced;
        stretchCounter++;
    }
}

/* OBS: curLapTime is set to ZERO as soon as the nose of the car cross the
 *      finish line. By the other hand, distFromStart is set to ZERO only after
 *      the center of the car cross the line.
 *      In game lap counter respects distFromStart.
 */
void Slotcar::updateLap(CarState &cs) {
    static bool locked = false;

    // If the car is 100m from start or farther, lap increasement is unlocked.
    if (this->lastDistance > 100) {
        locked = false;
    }

    if (!locked && cs.getDistFromStart() < this->lastDistance &&
        cs.getDistFromStart() < 10) {

        cout << "Lap " << setw(3) << this->lap << " - time: " << fixed
             << setw(7) << setprecision(3) << cs.getLastLapTime() << endl;
        if (this->lap >= 1) {
            lapTimes.push_back(cs.getLastLapTime());
        }
        this->lap++;
        locked = true;

        // When the first lap finishes, register the length of the track.
        if (this->trackLength <= 0.0) {
            this->trackLength = this->lastDistance;
        }
    }
}

void Slotcar::updateCurrentSegment(CarState &cs) {
    if (this->segmentsEnd.size() <= 0) {
        return;
    }
    if ((this->currentSegment == 0 &&
         cs.getDistFromStart() > this->segmentsEnd[0] &&
         cs.getDistFromStart() < this->segmentsEnd[1])
        ||
        (this->currentSegment > 0 &&
         cs.getDistFromStart() > this->segmentsEnd[this->currentSegment])
        ||
        (this->currentSegment == this->segmentsEnd.size() - 1 &&
        cs.getDistFromStart() <= 15)) {

        this->currentSegment++;
        if (this->currentSegment >= this->segmentsEnd.size()) {
            this->currentSegment = 0;
        }
        /*
        cout << "  -> At segment: " << setw(3) << this->currentSegment
             << "; End: " << setw(8) << setprecision(2)
             << this->segmentsEnd[this->currentSegment]
             << "; Type: " << this->segmentsType[this->currentSegment] << endl;
        */
    }
}

// Populates angles, distance vectors.
void Slotcar::preprocessData(vector<float>& angles, vector<float>& distances) {
    list<StretchData>::iterator data;
    list<StretchData>::iterator lastData;
    float angleSum = 0;
    float posAngleSum = 0;
    float negAngleSum = 0;

    for (data = this->stretches.begin(), lastData = data, data++;
         data != this->stretches.end();
         lastData = data, data++) {

        float angle = getTurnAngle(&(*data), &(*lastData));
        //float distance = data->getDistRaced() - lastData->getDistRaced();
        float distance = data->getDistFromStart() - lastData->getDistFromStart();

        angles.push_back(angle);
        distances.push_back(distance);

        angleSum += angle;
        if (angle > 0) {
            posAngleSum += angle;
        }
    }
    negAngleSum = angleSum - posAngleSum;

    // If angleSum is closer to zero than to 2PI..
    if (fabs(angleSum) < PI) {
        // In this case there is a crossroad (as in Suzuka track), so the total
        // angle sum should be zero.
        float posFactor = (posAngleSum - (angleSum / 2)) / posAngleSum;
        float negFactor = (negAngleSum - (angleSum / 2)) / negAngleSum;
        for (unsigned int i = 0; i < angles.size(); i++) {
            if (angles[i] > 0) {
                angles[i] *= posFactor;
            } else {
                angles[i] *= negFactor;
            }
        }
        // FIX ME: magic number that works only for Suzuka (Wheel 2).
        float correctionFactor = 1.46;
        for (unsigned int i = 0; i < angles.size(); i++) {
            angles[i] *= correctionFactor;
        }
    } else {
        // Adjusts angles such that the sum is 2*PI (a complete turn).
        float correctionFactor = fabs(2 * PI / angleSum);
        for (unsigned int i = 0; i < angles.size(); i++) {
            angles[i] *= correctionFactor;
        }
    }
}

void Slotcar::meanFilter(vector<float>& angles, vector<float>& filteredAngles) {
    for (unsigned int i = 0; i < FILTER_WINDOW; i++) {
        filteredAngles.push_back(angles[i]);
    }
    for (unsigned int i = FILTER_WINDOW; i < angles.size() - FILTER_WINDOW; i++) {
        float mean = 0;
        for (unsigned int j = i - FILTER_WINDOW; j <= i + FILTER_WINDOW; j++) {
            mean += angles[j];
            // The weight of current point is higher.
            if (j == i) {
                mean += angles[j];
            }
        }
        filteredAngles.push_back(mean / (2 + 2 * FILTER_WINDOW));
    }
    for (unsigned int i = angles.size() - FILTER_WINDOW; i < angles.size(); i++) {
        filteredAngles.push_back(angles[i]);
    }
}

// TODO: flatFilter does NOT need 'distances' vector.
// TODO: flatDistances should be vector<int> once dfs is vector<int>
void Slotcar::flatFilter(vector<float>& angles, vector<float>& distances,
                         vector<float>& flatAngles,
                         vector<float>& flatDistances) {
    vector<int> dfs;
    float lastAngle = 0;

    // Used to calculate the standard deviation of the difference between each
    // pair of consecutive angles.
    int n = 0;
    float mean = 0;
    float m2 = 0;
    for (list<StretchData>::iterator data = this->stretches.begin();
         data != this->stretches.end(); data++) {

        dfs.push_back(data->getDistFromStart());

        float diff = fabs(data->getAngle() - lastAngle);
        lastAngle = data->getAngle();

        // Online Knuth, Welford standard deviation.
        // http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
        n++;
        float delta = diff - mean;
        mean += delta / n;
        m2 += delta * (diff - mean);
    }
    float variance = m2 / (n - 1);
    float std = sqrt(variance);

    float angleThreshold = ANGLE_THRESHOLD_SCALE * std;
    cout << "AngleThreshold: " << setprecision(8) << angleThreshold << "; std: "
         << std << endl;

    flatDistances.push_back(0);
    flatAngles.push_back(0);
    float baseAngle = 0.0;

    unsigned int end = angles.size();
    for (unsigned int i = 1; i < end - 2; i++) {

        if (isGap(angles[i], baseAngle, angleThreshold)) {
            float BD = dfs[i - 1]; // back distance
            float BA = baseAngle;  // back angle

            float topBackDist = dfs[i];
            float topBackAngle = angles[i];

            // Keep going until finds a peak which is neither in the middle of a
            // upward line nor in the middle of a downward line.
            while (i < end - 3 && (!isPeak(angles, i) ||
                                   (sign(angles[i] - angles[i - 1]) ==
                                   sign(angles[i + 2] - angles[i + 1])
                                   &&
                                   sign(angles[i] - angles[i - 1]) ==
                                   sign(angles[i + 3] - angles[i + 2])
                                   ))) {

                // Get close to the point where the signal cross the zero line.
                if (fabs(angles[i - 1] - angles[i]) > std &&
                    angles[i - 1] * angles[i] <= 0) {

                    BD = dfs[i - 1];
                    topBackDist = dfs[i];
                }
                i++;
            }

            // When angles has different signs, ajust dist to be in the middle.
            if (fabs(angles[i - 1] - angles[i]) > std * 0.3 &&
                angles[i - 1] * angles[i] <= 0) {

                BD = dfs[i - 1];
                topBackDist = dfs[i];
            }

            // Makes straights have exactly 0.0 as angle.
            topBackAngle = angles[i];
            if (fabs(topBackAngle) < STRAIGHT_THRESHOLD) {
                topBackAngle = 0.0;
            }

            // If the stretch is too short or de resulting gap is too small, no
            // changes are made.
            if (topBackDist - flatDistances.back() < MIN_STRETCH_LENGTH) {
                flatAngles.pop_back();

                if (fabs(topBackAngle - flatAngles.back()) < angleThreshold) {
                    flatDistances.pop_back();
                    flatDistances.pop_back();
                    flatAngles.pop_back();
                    baseAngle = flatAngles.back();
                } else {
                    flatAngles.push_back(topBackAngle);
                    baseAngle = topBackAngle;
                }
            } else if (fabs(baseAngle - topBackAngle) >= std) {
                flatDistances.push_back(BD);
                flatAngles.push_back(BA);

                flatDistances.push_back(topBackDist);
                flatAngles.push_back(topBackAngle);
                baseAngle = topBackAngle;
            }
        }
    }
    flatAngles.push_back(baseAngle);
    flatDistances.push_back(dfs[end]);
}

void Slotcar::accumulateAngles(vector<float>& angles, vector<float>& distances,
                               vector<float>& flatDistances,
                               vector<float>& accAngles) {
    float accAngle = 0.0;
    float accDist = 0.0;
    unsigned int f = 1;
    bool needPush = true;
    accAngles.push_back(accAngle);
    for (unsigned int i = 0; i < angles.size() &&
                             f < flatDistances.size(); i++) {
        // Round float to consider only 5 decimal places.
        float round_angle = roundf(angles[i] * 100000) / 100000;
        float round_dist = roundf(distances[i] * 100000) / 100000;

        accAngle += round_angle;
        accDist += round_dist;
        needPush = true;

        /*
        cout << setiosflags(ios::fixed);
        cout << i << ": a: "<<setprecision(5)<<round_angle<<" ("
             <<setprecision(8)<<angles[i]<<"); accA: "
             <<accAngle<<"; d: " <<round_dist<<" ("<<distances[i]<<"); accD: "
             <<accDist<<endl;
        */

        if (accDist >= flatDistances[f]) {
            //cout << "----> f: " << (f-1)/2 << "; i: " << i << "; A: " << accAngle << "; D: " << accDist << endl;
            accAngles.push_back(accAngle);
            accAngle = 0.0;
            f += 2;
            needPush = false;
        }
    }
    if (needPush) {
        cout << "----> f: " << (f-1)/2 << "; A: " << accAngle << "; D: " << accDist << endl;
        accAngles.push_back(accAngle);
    }
}

bool Slotcar::isPeak(vector<float>& angles, int i) {
    return ((angles[i] > 0 &&
             angles[i] > angles[i - 1] &&
             angles[i] > angles[i + 1]) ||
            (angles[i] < 0 &&
             angles[i] < angles[i - 1] &&
             angles[i] < angles[i + 1]));
}

void Slotcar::processData() {
    list<StretchData>::iterator data;
    list<StretchData>::iterator lastData;

    preprocessData(angles, distances);
    meanFilter(angles, filteredAngles);
    flatFilter(filteredAngles, distances, flatAngles, flatDistances);
    accumulateAngles(angles, distances, flatDistances, accAngles);

    // ATENTION HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    angles.swap(filteredAngles);

    string file_name = "./output/" + string(this->trackName) + ".cluster";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name
             << " file." << endl;
    }

    int last = accAngles.size() - 1;
    float length;
    float accAngle;

    // If the last and the first angle are the same, then they are in the same
    // segment.
    if (fabs(flatAngles[1] - flatAngles[flatAngles.size() - 1]) <
        STRAIGHT_THRESHOLD) {

        length = flatDistances[last] - flatDistances[last - 2] +
                 flatDistances[1];
        accAngle = accAngles[1] + accAngles[last];
        last--;
    } else {
        length = flatDistances[1];
        accAngle = accAngles[1];
    }
    float radius;
    if (accAngle == 0.0 || flatAngles[1] == 0.0) radius = -1;
    else radius = length / fabs(accAngle);

    int type = classifySegment(accAngle, radius, length);
    this->segmentsType.push_back(type);
    this->segmentsEnd.push_back(flatDistances[1]);
    this->segmentsLength.push_back(length);

    // Cluster file to be used with the visualizer.
    outputStream << flatDistances[1]
                 << "," << flatAngles[1] << ",cluster" << type << endl;

    // The same as before, but for the rest.
    flatDistances[flatDistances.size() - 1] -= 1.0;
    for (int i = 2; i <= last; i++) {
        int j = (2 * i) - 1; // Distances iterator

        length = flatDistances[j] - flatDistances[j - 2];
        accAngle = accAngles[i];
        if (accAngle == 0.0 || flatAngles[j] == 0.0) radius = -1;
        else radius = length / fabs(accAngle);

        type = classifySegment(accAngle, radius, length);
        this->segmentsType.push_back(type);
        this->segmentsEnd.push_back(flatDistances[j]);
        this->segmentsLength.push_back(length);

        // Cluster file to be used with the visualizer.
        outputStream << flatDistances[j]
                     << "," << flatAngles[j] << ",cluster" << type << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

float Slotcar::getTurnAngle(StretchData *data, StretchData *lastData) {
    float rightWheelSpin = data->getFrontRightWheelSpin();
    float leftWheelSpin = data->getFrontLeftWheelSpin();
    float steer = data->getSteerAngle();

    float outsideWheelSpin;
    int turnDirection;

    if (rightWheelSpin == leftWheelSpin || steer == 0) {
        return 0;
    } else if (steer < 0) {
        // Right turn.
        turnDirection = RIGHT;
        outsideWheelSpin = leftWheelSpin;
    } else {
        // Left turn.
        turnDirection = LEFT;
        outsideWheelSpin = rightWheelSpin;
    }

    float radius = fabs(WHEELBASE / sin(steer));
    float deltaT = data->getLapTime() - lastData->getLapTime();
    float distance = FRONT_WHEEL_RADIUS * outsideWheelSpin * deltaT;
    float angle = turnDirection * distance / (radius + TRACK / 2);

    return angle;
}

int Slotcar::classifySegment(float angle, float radius, float length) {
    //cout << "angle: " << angle << "; radius: " << radius
    //     << "; length: " << length;
    int type = 1000;
    float alpha = fabs(angle);

    if (radius == -1) { // STRAIGHTS
        if (length <= 55) {
            type = SHORT_STRAIGHT;
        } else if (length <= 200) {
            type = MEDIUM_STRAIGHT;
        } else {
            type = LONG_STRAIGHT;
        }
    } else { // TURNS
        if (length <= 91.0) {
            if (length <= 60 && alpha <= 0.75) {
                type = SHORT_EASY;
            } else if (alpha <= 1.396) {    //  80 degrees.
                type = SHORT_MEDIUM;
            } else {
                type = SHORT_HARD;
            }
        } else {
            if (radius <= 112.0 && alpha > 1.369) {           // 80 degrees.
                type = LONG_HARD;
            } else if (radius <= 210.0 && alpha > 0.89) {    // 50 degrees.
                type = LONG_MEDIUM;
            } else {
                type = LONG_EASY;
            }
        }
        if (radius <= 57.0) {
            if (alpha >= 1.919) {           // 110 degrees.
                type = HAIRPIN;
            } else if (alpha >= 0.873) {    //  50 degrees.
                type = ELBOW;
            }
        }
        if (isRightTurn(angle)) type *= -1;
    }
    if (type == 1000) {
        cout << "[ERROR] Slotcar::classifySegment: could not classify segment "
             << "r: " << radius << " l: " << length << " a: " << alpha << endl;
        type = SHORT_STRAIGHT;
    }
    //cout << " -> type: " << type << endl;
    return type;
}

bool Slotcar::isRightTurn(float angle) {
    return angle < 0.0;
}

bool Slotcar::isGap(float a, float b, float angleThreshold) {
    // It is a gap when a and b are far from each other or
    // when a is in a turn and b is in a straight or
    // when a is in a straight and b is in a turn.
    return (
        // If the difference is too small, return false.
        (fabs(a - b) > angleThreshold / ANGLE_THRESHOLD_SCALE) &&

        // If both a and b are in the same straight zone, then there is no gap.
        !(fabs(a) < STRAIGHT_THRESHOLD && fabs(b) < STRAIGHT_THRESHOLD) &&
        (
            fabs(a - b) >= angleThreshold ||
            (fabs(a) >= STRAIGHT_THRESHOLD && fabs(b) < STRAIGHT_THRESHOLD) ||
            (fabs(a) < STRAIGHT_THRESHOLD && fabs(b) >= STRAIGHT_THRESHOLD) ||
            a * b < 0
        )
    );
}

float Slotcar::mean(int first, int last, vector<float>& v) {
    float sum = 0;
    for (int i = first; i <= last; i++) {
        sum += v[i];
    }
    return sum / (last - first + 1);
}

float Slotcar::variance(int first, int last, float mean, vector<float>& v) {
    float sum = 0;
    for (int i = first; i <= last; i++) {
        float difference = v[i] - mean;
        sum += difference * difference;
    }
    return sum / (last - first + 1);
}

float Slotcar::standardDeviation(int first, int last, float mean,
                                 vector<float>& v) {
    return sqrt(variance(first, last, mean, v));
}

void Slotcar::printSegments() {
    string file_name = "./output/" +string(this->trackName) + ".classification";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    for (unsigned int i = 0; i < this->segmentsType.size(); i++) {
        string str = STRETCH_TYPES[abs(this->segmentsType[i])];

        cout << setw(2) << i << ". end: " << setw(7) << setprecision(2) << fixed
             << this->segmentsEnd[i] << "; type: " << setw(2)
             << this->segmentsType[i] << " - " + str << endl;
        outputStream << setw(2) << i << ". end: " << setw(7) << setprecision(2)
                     << fixed << this->segmentsEnd[i] << "; type: "
                     << setw(2) << this->segmentsType[i] << " - " + str << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeSegmentsFile(vector<float>& angles,
                                vector<float>& distances) {

    // Create output directory. If it already exists, no problem.
    mkdir("./output", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    string file_name = "./output/" +string(this->trackName) + ".segments";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    outputStream << setiosflags(ios::fixed);
    unsigned int size = angles.size();
    for (unsigned int i = 0; i < size; i++) {
        outputStream << setprecision(5) << distances[i] << " "
                     << setprecision(5) << angles[i] << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeSignalFile(vector<float>& angles, string extension) {
    string file_name = "./output/" +string(this->trackName) + extension;
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    list<StretchData>::iterator data = this->stretches.begin();
    outputStream << setiosflags(ios::fixed);
    unsigned int size = angles.size();
    for (unsigned int i = 0; i < size; i++, data++) {

        outputStream << setprecision(5) << data->getDistFromStart() << " "
                     << setprecision(5) << angles[i] << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeFlatSignalFile(vector<float>& angles,
                                  vector<float>& distances, string extension) {
    string file_name = "./output/" +string(this->trackName) + extension;
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    outputStream << setiosflags(ios::fixed);
    unsigned int size = angles.size();
    for (unsigned int i = 0; i < size; i++) {

        outputStream << setprecision(6) << distances[i] << " "
                     << setprecision(6) << angles[i] << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

void Slotcar::writeLapTimes() {
    string file_name = "./output/" +string(this->trackName) + ".lapTimes";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    outputStream << setiosflags(ios::fixed);
    int i = 1;
    for (list<float>::iterator lap = lapTimes.begin();
         lap != lapTimes.end(); lap++, i++) {

        outputStream << i << " " << setprecision(3) << *lap << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}
