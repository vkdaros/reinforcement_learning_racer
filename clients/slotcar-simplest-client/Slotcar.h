/***************************************************************************

    file                 : Slotcar.h
    created              : Tue Jul 16 17:42:53 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SLOTCAR_H_
#define SLOTCAR_H_

#include <fstream>
#include <list>
#include <vector>
#include <string>

#include "BaseDriver.h"
#include "CarState.h"
#include "CarControl.h"
#include "WrapperBaseDriver.h"
#include "StretchData.h"

#define DISTANCE_STEP 5 // Magic number to data work better in visualizer.
#define PI 3.14159265359

#define MAX_SLIP    (0.277 * 0.1)
#define WINDOW_SIZE 5 // Size of sample in segmentation.
#define FILTER_WINDOW 1
#define FLAT_WINDOW 4

#define ANGLE_THRESHOLD_SCALE 4
#define STRAIGHT_THRESHOLD 0.008
#define MIN_STRETCH_LENGTH 15.0

#define WHEELBASE 4.5 // Distance between centers of front and rear wheels.
#define TRACK     1.9 // Distance between centers of two wheels on same axle.
#define FRONT_WHEEL_RADIUS 0.3306
#define REAR_WHEEL_RADIUS  0.3250
#define STEER_LOCK 0.366519

#define RIGHT (-1)
#define LEFT   (1)

// Segment types for classification.
#define N_TYPES 11
#define SHORT_STRAIGHT         0
#define MEDIUM_STRAIGHT        1
#define LONG_STRAIGHT          2
#define HAIRPIN_RIGHT        (-3)
#define HAIRPIN_LEFT           3
#define HAIRPIN                3
#define ELBOW_RIGHT          (-4)
#define ELBOW_LEFT             4
#define ELBOW                  4
#define SHORT_HARD_RIGHT     (-5)
#define SHORT_HARD_LEFT        5
#define SHORT_HARD             5
#define LONG_HARD_RIGHT      (-6)
#define LONG_HARD_LEFT         6
#define LONG_HARD              6
#define SHORT_MEDIUM_RIGHT   (-7)
#define SHORT_MEDIUM_LEFT      7
#define SHORT_MEDIUM           7
#define LONG_MEDIUM_RIGHT    (-8)
#define LONG_MEDIUM_LEFT       8
#define LONG_MEDIUM            8
#define SHORT_EASY_RIGHT     (-9)
#define SHORT_EASY_LEFT        9
#define SHORT_EASY             9
#define LONG_EASY_RIGHT     (-10)
#define LONG_EASY_LEFT        10
#define LONG_EASY             10

using namespace std;

class Slotcar : public WrapperBaseDriver {
  public:
	// Constructor.
	Slotcar(){};

	// Initialization of the desired angles for the rangefinders.
	virtual void init(float *angles);

	// Called at the end of the race, before being unloaded.
	virtual void onShutdown();

	// Called when the race is restarted.
	virtual void onRestart();

    // Change the effectors values in order to drive the car.
    virtual CarControl wDrive(CarState cs);

  protected:
    // Gear changing constants.
    static const int maxRpmAtGear[6];
    static const int minRpmAtGear[7];
    static const int SENSORS[TRACK_SENSORS_NUM];

    // How many time steps the car wait before recovering from a stuck position.
    static const int stuckTime;
    // When car angle w.r.t. track axis is grather tan stuckAngle, the car is
    // probably stuck.
    static const float stuckAngle;

    // Indicate for how many ticks the car is stuck.
    int stuckCounter;

    // File where the sensor readings will be written
    ofstream outputStream;

    // Distance from start measured in the last tick.
    float lastDistance;

    float lastCurLapTime;

    float trackLength;

    // Lap counter. If equals to zero, then the first lap hasn't started yet.
    int lap;

    unsigned int currentSegment;

    // List of scanned data of track stretches.
    list<StretchData> stretches;

    // Vector of classified segments which compose the track.
    vector<int> segmentsType;

    // Distance from start of the end of each segment.
    vector<float> segmentsEnd;

    // Length of each segment.
    vector<float> segmentsLength;

    // Flag activated when aplying recover policy.
    bool recovering;

    // Select to gear up or down according to RPM.
    int selectGear(CarState &cs);

    // Returns acceleartion based on the distance to the next bend.
    virtual float getAcceleration(CarState &cs);
    virtual float getSlowAcceleration(CarState &cs);

    // Returns braking based on the distance to the next bend.
    virtual float getBraking(CarState &cs);
    virtual float getSlowBraking(CarState &cs);

    // Returns brake after pass through ABS filter.
    virtual float ABS(CarState &cs, float brake);

    float getSteer(CarState &cs);
    float getReverseSteer(CarState &cs);
    float getSoftSteer(CarState &cs);

    // Writes on output file relevant current state data.
    void registerData(CarState &cs, float steer);

    // Responsible for updating lap counter.
    void updateLap(CarState &cs);

    // Update in which segment the car is.
    void updateCurrentSegment(CarState &cs);

    // Process registered data and fill segments list.
    void preprocessData(vector<float>& angles, vector<float>& distances);
    void meanFilter(vector<float>& angles, vector<float>& filteredAngles);
    void flatFilter(vector<float>& angles, vector<float>& distances,
                    vector<float>& flatAngles, vector<float>& flatDistances);
    void accumulateAngles(vector<float>& angles, vector<float>& distances,
                          vector<float>& flatDistances,
                          vector<float>& accAngles);
    bool isPeak(vector<float>& angles, int i);
    void processData();
    float getTurnAngle(StretchData *data, StretchData *lastData);

    // Return the type of the segment.
    int classifySegment(float angle, float radius, float length);
    bool isRightTurn(float angle);

    // True when when there is a gap between alpha and beta while analyzing
    // track signal in flatFilter.
    bool isGap(float alpha, float beta, float angleThreshold);

    float mean(int first, int last, vector<float>& v);
    float variance(int first, int last, float mean, vector<float>& v);
    float standardDeviation(int first, int last, float mean, vector<float>& v);

    void writeLapTimes();

    // Just for debug.
    void printSegments();
    void writeSegmentsFile(vector<float>& angles, vector<float>& distances);
    void writeSignalFile(vector<float>& angles, string extension);
    void writeFlatSignalFile(vector<float>& angles, vector<float>& distances,
                             string extension);
};
#endif /* SLOTCAR_H_ */
