/***************************************************************************

    file                 : SlotcarRL.cpp
    created              : Wed Jul 24 18:49:42 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "SlotcarRL.h"

#define PI 3.14159265359

float Q[N_STATES][N_ACTIONS];
int QBest[N_STATES][2];
bool QUsed[N_STATES][N_ACTIONS];
float Q_START_VALUE = 0.0;
float epsilon = EPSILON;
const int TARGET_SPEEDS[N_ACTIONS] = {50, 90, 120, 180, 999};
//const int TARGET_SPEEDS[N_ACTIONS] = {60, 90, 140, 210, 999};
float closeToEnd[N_TYPES * N_FOLLWING];

list<float> rewards;     // Total rewards per episode.
float totalReward = 0.0; // Reward of current episode;

vector<int> followingType;
vector<int> traceBrakingStates;
vector<int> traceBrakingActions;

vector<int> traceFarStates;
vector<int> traceFarActions;

bool cleanLap = true;
bool cleanZone = false;
int cleanWait = 0;

bool debug = false;

// Labels used to describe states.
const char* STRETCH_TYPES_TABULAR[N_TYPES] = {
    "SHORT_STRAIGHT  ",
    "MEDIUM_STRAIGHT ",
    "LONG_STRAIGHT   ",
    "HAIRPIN         ",
    "ELBOW           ",
    "SHORT_HARD      ",
    "LONG_HARD       ",
    "SHORT_MEDIUM    ",
    "LONG_MEDIUM     ",
    "SHORT_EASY      ",
    "LONG_EASY       "};
const char* SPEEDS[N_SPEEDS] = {" 50-", " 90-", "120-", "180-", "180+"};
const char* NEAR_END[2] = {"notNearEnd", "   nearEnd"};
const char* FOLLOWING[2] = {"slow", "fast"};

const float SlotcarRL::DELAY_DISTANCE = 20.0; // (meters)
const int   SlotcarRL::DELAY_TIME = 50;       // (ticks)

// Positive values are throttle and negative ones are brake.
const float SlotcarRL::actions[N_ACTIONS] = {1.0, 0.6, 0.0, -0.4, -1.0};

void SlotcarRL::init(float *angles) {
    static bool done = false;
    if (done) return;

    Slotcar::init(angles);

    for (int i = 0; i < N_STATES; i++) {
        for (int j = 0; j < N_ACTIONS; j++) {
            Q[i][j] = Q_START_VALUE;
        }
        QBest[i][0] = -1;
        QBest[i][1] = 0;
    }

    for (int i = 0; i < N_TYPES * N_FOLLWING; i++) {
        closeToEnd[i] = CLOSE_TO_END;
    }

    ifstream qfile("qmatrix");
    if (qfile.is_open()) {
/***
 * TODO:
 * Adapt file loading for new qmatrix layout. */
        cout << "----- Initialization from file. -----" << endl;
        string line;

        // Look for variables initialization inside the file.
        while (getline(qfile, line)) {
            istringstream iss(line);
            string label;
            getline(iss, label, ':');
            if (label == "EPSILON") {
                iss >> epsilon;
                cout << "EPSILON: " << epsilon << endl;
            } else if (label == "Q_START_VALUE") {
                iss >> Q_START_VALUE;
                cout << "Q_START_VALUE: " << Q_START_VALUE << endl;
                for (int i = 0; i < N_STATES; i++) {
                    for (int j = 0; j < N_ACTIONS; j++) {
                        Q[i][j] = Q_START_VALUE;
                    }
                }
            } else {
                break;
            }
        }

        // Discard lines until it reaches the beginning of the matrix.
        cout << "Discarding trash lines";
        do {
            cout << ".";
        } while (line.find("=====") == std::string::npos &&
                 getline(qfile, line));
        cout << " done!" << endl;

        // Get values until the end of matrix in the file.
        cout << "Reading rows";
        while (getline(qfile, line) && line.find("---") == std::string::npos) {
            cout << ".";
            istringstream iss(line);

            // Get current row.
            int s;
            iss >> s;

            // Discard speed label
            string str;
            getline(iss, str, '|');

            int a = 0;
            while (getline(iss, str, '|')) {
                float value = atof(str.c_str());
                Q[s][a++] = value;
            }
        }

        cout << endl << "Discarding trash lines";
        while (getline(qfile, line) && line.find("---") == std::string::npos) {
            cout << ".";
        }
        cout << " done!" << endl << "Reading close to end";
        while (getline(qfile, line) && line.find("---") == std::string::npos) {
            cout << ".";
            istringstream iss(line);

            // Get current row.
            int i = -1;
            iss >> i;

            // Discard speed label
            string str = "X";
            getline(iss, str, ' ');
            getline(iss, str, ' ');

            int a = -1;
            iss >> a;
            closeToEnd[i] = a;
        }
        qfile.close();
        cout << " done!" << endl;
        cout << "-------------------------------------" << endl;
    }

    this->lastState = -1;
    srand(time(NULL));
    done = true;
}

void SlotcarRL::onShutdown() {
    printQ();
    Slotcar::onShutdown();

    rewards.push_back(totalReward);
    writeRewards();
}

void SlotcarRL::onRestart() {
    printQ();
    Slotcar::onRestart();

    rewards.push_back(totalReward);
    writeRewards();
}

int SlotcarRL::getCurrentState(CarState &cs) {
    static int lastLap = 1;
    int speedState = -1;
    int stretchType;
    int nearEnd;
    int follow;

    if (this->recovering) {
        return RECOVERING;
    }

    if (fabs(cs.getSpeedX()) <= 20.0) {
        return FINAL_STATE_STOP;
    }

    if (fabs(cs.getTrackPos()) >= 0.9) {
        return FINAL_STATE_BAD;
    }

    if (this->lap > lastLap) {
        lastLap = this->lap;
        return FINAL_STATE_GOOD;
    }

    float speed = cs.getSpeedX();
    for (int i = 0; i < N_ACTIONS; i++) {
        speedState = i;
        if (speed <= TARGET_SPEEDS[i]) {
            break;
        }
    }

    unsigned int s = this->currentSegment;
    stretchType = abs(this->segmentsType[s]);
    follow = followingType[this->currentSegment]; // FAST / SLOW.

    // Check if the car is near the end of current segment.
    float length = closeToEnd[stretchType * N_FOLLWING + follow];
    if (stretchType == HAIRPIN || stretchType == ELBOW) {

        length = this->segmentsEnd[s];
        if (s > 0) {
            length -= this->segmentsEnd[s - 1];
        }
        length *= 0.4; // Percent end->begin of where is the end limit.
    }
    nearEnd = 0;
    float nearLimit = this->segmentsEnd[s] - length;
    if (nearLimit < 0.0) {
        nearLimit += this->trackLength;
        if (cs.getDistFromStart() >= nearLimit) {
            nearEnd = 1;
        }
    } else if (cs.getDistFromStart() >= nearLimit &&
               cs.getDistFromStart() < this->segmentsEnd[s]) {
        nearEnd = 1;
    }

    /**
     * Index formula
     * i -> index (matrix row)
     * t -> stretchType; T -> number of types;
     * s -> speedState;  S -> number of speeds;
     * n -> nearEnd;     N -> number of nears;
     * f -> following;   F -> number of followings;
     * i = (S*N*F)*t + (N*F)*s + (F)*n + f
     */
//    int index = (N_ACTIONS * 2) * stretchType + 2 * speedState + nearEnd;
    int index = (N_ACTIONS * 2 * N_FOLLWING) * stretchType +
                (2 * N_FOLLWING) * speedState + (N_FOLLWING) * nearEnd + follow;

    if (index >= N_STATES || index < 0) {
        // This should never happen.
        cout << ">>> ERROR: index >= N_STATES || index < 0" << endl
             << "    index: " << index << "; currentSegment: "
             << this->currentSegment << "; stretchType: " << stretchType
             << "; speedState: " << speedState << "; nearEnd: " << nearEnd
             << endl;
        index = FINAL_STATE_BAD;
    }
    return index;
}

int SlotcarRL::chooseAction(int state) {
    static int currentLap = -1;

    int action;
    float r = ((double) rand() / (RAND_MAX));

    if (r < epsilon) {
        action = rand() % N_ACTIONS;
        if(debug)cout<<" -random action ("<<state<<", "<<action<<")-"<<endl;
    } else {
        action = chooseBestAction(state);
    }

    // Epsilon is kept the same during all lap.
    if (this->lap > currentLap) {
        epsilon *= EPSILON_RATE;
        currentLap = this->lap;
    }

    if (epsilon != 0.0 && epsilon <= 0.0005) {
        epsilon = 0.0;
        cout << ">>>>>>>>>>>>>>>>>>>>> Exploration stoped! epsilon = 0" << endl;
    }

    if (state >= 0) {
        // If not a final state...
        if (inBrakingZone(state)) {
            traceBrakingStates.push_back(state);
            traceBrakingActions.push_back(action);
if(debug)cout << "=>Z (" << state << "," << action << ")" << endl;
        } else {
            traceFarStates.push_back(state);
            traceFarActions.push_back(action);
if(debug)cout << "=>! (" << state << "," << action << ")" << endl;
        }
        QUsed[state][action] = true;
    }
    return action;
}

int SlotcarRL::chooseBestAction(int state) {
    int canditates[N_ACTIONS];
    int nCandidates = 0;
    int bestAction = 0;

    canditates[0] = bestAction;
    nCandidates = 1;

    for (int action = 1; action < N_ACTIONS; action++) {
        if (Q[state][action] > Q[state][bestAction]) {
            bestAction = action;
            canditates[0] = bestAction;
            nCandidates = 1;
        } else if (Q[state][action] == Q[state][bestAction]) {
            canditates[nCandidates] = action;
            nCandidates++;
        }
    }
    bestAction = canditates[rand() % nCandidates];
    /*if (epsilon == 0.0 && QBest[state] >= 0) {
        // If exploration has stopped, follow best lap actions.
        bestAction = QBest[state];
    } else {
        canditates[0] = bestAction;
        nCandidates = 1;

        for (int action = 1; action < N_ACTIONS; action++) {
            if (Q[state][action] > Q[state][bestAction]) {
                bestAction = action;
                canditates[0] = bestAction;
                nCandidates = 1;
            } else if (Q[state][action] == Q[state][bestAction]) {
                canditates[nCandidates] = action;
                nCandidates++;
            }
        }
        bestAction = canditates[rand() % nCandidates];
    }*/
    return bestAction;
}

float SlotcarRL::getReward(CarState &cs, int state, int action) {
    float reward = 0;
    /*if (state == FINAL_STATE_GOOD) {
        reward = 0;
    } else*/ if (state == FINAL_STATE_BAD) {
        reward = -5;
    } else if (state == FINAL_STATE_STOP) {
        reward = -10;
    } else {
        reward = 0;
        if (action == N_ACTIONS - 1) {
            reward = 0.5;
        }
    }
    totalReward += reward;
    return reward;
}

void SlotcarRL::printQ() {
    cout << "EPSILON: " << epsilon << endl;
    cout << "                                           Action: Brake/Accel" << endl
         << "                                           ___________________________________________________" << endl
         << "                                           |  -1.00  |  -0.50  |    0    |   0.50  |   1.00  |" << endl
         << "                                           ===================================================" << endl;

    for (int s = 0; s < N_STATES; s++) {
        // Row label.
        int D = N_SPEEDS * 2 * N_FOLLWING;
        int t = s / D;
        int v = (s % D) / (2 * N_FOLLWING);
        int n = ((s % D) % (2 * N_FOLLWING)) / N_FOLLWING;
        int f = ((s % D) % (2 * N_FOLLWING)) % N_FOLLWING;
        cout << setw(3) <<  s << ". " << STRETCH_TYPES_TABULAR[t] << " "
             << SPEEDS[v] << " " << NEAR_END[n] << " " << FOLLOWING[f] << " |";

        // Values for each action.
        for (int a = 0; a < N_ACTIONS; a++) {
            cout << " " << setprecision(3) << setw(7) << Q[s][a] << " |";
        }
        cout << endl;
    }
    cout << "                                           ---------------------------------------------------" << endl;
    cout << "Close to end:" << endl << "  ----------------------" << endl;
    for (int i = 0; i < N_TYPES * N_FOLLWING; i++) {
        cout << "  " << setw(2) << i << " "
             << STRETCH_TYPES_TABULAR[i / N_FOLLWING] << setw(3)
             << (int)closeToEnd[i] << endl;
    }
    cout << "  ----------------------" << endl;

    for (int s = 0; s < N_STATES; s++) {
        int D = N_SPEEDS * 2 * N_FOLLWING;
        int t = s / D;
        int v = (s % D) / (2 * N_FOLLWING);
        int n = ((s % D) % (2 * N_FOLLWING)) / N_FOLLWING;
        int f = ((s % D) % (2 * N_FOLLWING)) % N_FOLLWING;
        cout << setw(3) <<  s << ". " << STRETCH_TYPES_TABULAR[t] << " "
             << SPEEDS[v] << " " << NEAR_END[n] << " " << FOLLOWING[f] << ": "
             << setw(2) << QBest[s][0] << " " << QBest[s][1] << endl;
    }
    cout << endl;
}

void SlotcarRL::writeRewards() {
    string file_name = "./output/" +string(this->trackName) + ".rewards";
    this->outputStream.open(file_name.c_str());
    if (this->outputStream.is_open()) {
        cout << "File " << file_name << " successfully openned." << endl;
    } else {
        cout << "ERROR: Unable to open output " << file_name << " file." << endl;
    }

    outputStream << setiosflags(ios::fixed);
    int i = 2;
    for (list<float>::iterator r = rewards.begin();
         r != rewards.end(); r++, i++) {

        outputStream << setprecision(8) << i << " "
                     << setprecision(8) << *r << endl;
    }

    if (this->outputStream.is_open()) {
        outputStream.close();
        cout << "File " << file_name << " closed." << endl;
    }
}

CarControl SlotcarRL::wDrive(CarState cs) {
    static bool learning = false;
    static int action;
    static bool first_alignment = true;
    static bool wasBraking = false;
    bool D = debug;

    float accel;
    float brake;
    int gear;
    float steer;
    float clutch;

    Slotcar::updateLap(cs);
    Slotcar::updateCurrentSegment(cs);

    // check if car is currently stuck
    if (fabs(cs.getAngle()) > Slotcar::stuckAngle || fabs(cs.getTrackPos()) > 1.1) {
        // update stuck counter
        this->stuckCounter++;
    } else if (!this->recovering) {
        // if not stuck reset stuck counter
        this->stuckCounter = 0;
    }

    // If car is really stuck, apply recovering policy and stops learning.
    if (this->stuckCounter > Slotcar::stuckTime) {
        if (!this->recovering) {
            //cout << "---> Recovering" << endl;
            this->stuckCounter = 0;
        }
        this->recovering = true;
    }

    static bool inBZone = false;
    static int lastBZoneSegment = -1;
    if (learning) {
        int state = getCurrentState(cs);

        if (state == RECOVERING || this->lastState == RECOVERING) {
            inBZone = inBrakingZone(state);
        } else if (state != this->lastState ||
            state == FINAL_STATE_STOP) {

            if (state == FINAL_STATE_GOOD) {
                // End of lap -> episode.
                //if (this->lap % 10 == 0) printQ();
                applyLapTrace();
                rewards.push_back(totalReward);
                totalReward = 0.0;
            } else if (state < 0) {
                // Accident (stopped or out of the track).
                this->recovering = true;
                cleanLap = false;

                float incentive = 0.0;
                if (state == FINAL_STATE_BAD) {
                    incentive = -0.005; // Should go slower.
                    cout << "  -> Out!  ";
                } else {
                    incentive = 0.005; // Should go faster.
                    cout << "  #> Stop! ";
                }

                cout << "@seg: " << setw(3) << this->currentSegment << " ["
                     << setw(3) << this->lastState << ";" << action << "]<"
                     <<QBest[this->lastState][1]<<">; spX: "
                     << setw(8) << setprecision(3) << cs.getSpeedX()<<flush;

                float r = getReward(cs, state, action);
                bool wasInBrakingZone;

                if (inBrakingZone(this->lastState)) {
                    if(D)cout << "    TRACE.Z(" << this->lastState << "): ";
                    wasInBrakingZone = true;
                } else {
                    if(D)cout << "    TRACE.!(" << this->lastState << "): ";
                    wasInBrakingZone = false;
                }
                applyBadTrace(traceBrakingStates, traceBrakingActions,
                              traceFarStates, traceFarActions, wasInBrakingZone,
                              r, incentive);
                cleanZone = false;
                cleanWait = 0;
            } else if (this->lastState >= 0) {
                // Strech has ended without accidents.
                if (inBrakingZone(state) && !inBZone) {
                    // Just arrived at braking zone.
/**/
if(D)cout << setprecision(2) << "<<< [BZone(" << (this->currentSegment)
     << ") s: " << this->lastState << "|" << state << "; clean: "
     << cleanZone << "]"<<endl;
/**/
                    applyGoodTrace(traceBrakingStates, traceBrakingActions);
                    inBZone = true;
                    lastBZoneSegment = this->currentSegment;
                } else if (!inBrakingZone(state)) {
                    if (inBZone) {
                        // Just left braking zone.
/**/
if(D)cout << setprecision(2) << "<<< [!Zone(" << (this->currentSegment - 1)
     << ") s: " << this->lastState << "|" << state << "; clean: "
     << cleanZone << "] ";
if(D)for (unsigned int i = 0; i < traceBrakingStates.size(); i++) {
    cout<<"("<<traceBrakingStates[i]<<"; "<<traceBrakingActions[i]<<") ";
}
if(D)cout <<endl;
/**/
                        applyGoodTrace(traceFarStates, traceFarActions);
                    } else if (!traceBrakingStates.empty() &&
                               farFromBZone(cs, lastBZoneSegment)) {
/**/
if(D)cout << setprecision(2) << "<<< Left BZ(" << (this->currentSegment)
     << "): [";
if(D)for (unsigned int i = 0; i < traceBrakingStates.size(); i++) {
    cout<<"("<<traceBrakingStates[i]<<"; "<<traceBrakingActions[i]<<") ";
}
if(D)cout << "]"<<endl;
/**/
                        traceBrakingStates.clear();
                        traceBrakingActions.clear();
                    }
                    inBZone = false;
                }
            }

            // Choose next action and keep it until it takes effect
            // (next state change).
            action = chooseAction(state);
            this->lastState = state;
        }
    }
    /******************************************************************/

    static int recoveringGear = 0;
    static int recoveringCounter = 0;
    if (this->recovering) {
        recoveringCounter++;
        accel = 0.5;
        brake = 0.0;
        clutch = 0;

        float pos = cs.getTrackPos();
        float ang = cs.getAngle();
        if (recoveringCounter >= 300) {
            recoveringCounter = recoveringGear = 0;
        }

        if (recoveringGear == 0) {
            // If the car is not pointing to correct direction and it is in the
            // same side of the accident, reverse gear.
            if (ang * pos < 0) {
                recoveringGear = -1;
            } else {
                recoveringGear = 1;
                accel = 0.3;
            }
        }

        // If at any moment the car is pointing to correct direction, go foward.
        if (fabs(ang) < 0.75 * Slotcar::stuckAngle) {
            recoveringGear = 1;

            // If car is aligned to the right direction and in the middle of the
            // track, recovering is done.
            if (fabs(pos) < 0.8 && cs.getSpeedX() >= 30.0 &&
                cs.getSpeedX() <= 60.0 && fabs(cs.getSpeedY()) < 1.5) {

                recoveringGear = 0;
                this->recovering = false;
                this->stuckCounter = recoveringCounter = 0;
                cout << "  <- Back!" << endl;
            }
        }

        if (recoveringGear == 1) {
            if (fabs(cs.getTrackPos()) < 0.85 && this->lap >= 2) {
                steer = SlotcarRL::getSimpleSteer(cs);
                //steer = Slotcar::getSteer(cs);
            } else {
                // Try to go to the middle of the track.
                steer = Slotcar::getSteer(cs);
            }
        } else {
            // Try to get parallel to the thrack direction.
            steer = (recoveringGear) * (cs.getAngle() / STEER_LOCK);
        }

        if (fabs(cs.getSpeedX()) > 31 || cs.getRpm() > 5000) {
            accel = 0.0;
            brake = 0.1;
        }
        gear = recoveringGear;

        // Brake while changing direction (1st gear <-> reverse gear)
        if ((cs.getSpeedX() > 5  && recoveringGear == -1) ||
            (cs.getSpeedX() < -5 && recoveringGear == 1)) {

            accel = 0.0;
            brake = 1.0;
        }
    } else {
        // Before start, use reverse gear to align the car
        if (first_alignment) {
            gear = -1;
            clutch = 0;
            float speedControl = getSpeedControl(cs, -0.75 * SCAN_SPEED);
            accel = brake = 0.0;
            if (speedControl < 0.0) {
                accel = fabs(speedControl);
            } else {
                brake = fabs(speedControl);
            }
            steer = getReverseSteerPID(cs);
            clutch = 0;

            // If angle is lower than 5 degree and is less than 3 percent away
            // from the middle of the track, then it is aligned.
            if (fabs(cs.getAngle()) < 0.087266463 &&
                fabs(cs.getTrackPos()) < 0.03) {
                cout << "===> CAR ALIGNED! >>> angle: " << cs.getAngle()
                     << "; trackPos: " << cs.getTrackPos() << endl;
                first_alignment = false;
            }
        } else if (this->lap < 1) {
            // No registering before first lap starts.
            if (cs.getSpeedX() < 0) {
                accel = 0.0;
                brake = 0.9;
                gear = Slotcar::selectGear(cs);
                steer = Slotcar::getReverseSteer(cs);
                /*cout << " '-> angle: " << cs.getAngle() << " trackPos: "
                     << cs.getTrackPos() << " v: " << cs.getSpeedX()
                     << " steer: " << steer << endl;*/
            } else {
                float speedControl = getSpeedControl(cs, SCAN_SPEED);
                accel = brake = 0.0;
                if (speedControl > 0.0) {
                    accel = fabs(speedControl);
                } else {
                    brake = fabs(speedControl);
                }
                gear = Slotcar::selectGear(cs);
                steer = getSteerPID(cs);
            }
            clutch = 0;
        } else if (this->lap == 1) {
            // No learning until race starts.
            float speedControl = getSpeedControl(cs, SCAN_SPEED);
            accel = brake = 0.0;
            if (speedControl > 0.0) {
                accel = fabs(speedControl);
            } else {
                brake = fabs(speedControl);
            }
            gear = Slotcar::selectGear(cs);
            steer = getSteerPID(cs);
            clutch = 0;
            Slotcar::registerData(cs, steer);
        } else {
            if (this->lap == 2 && this->segmentsType.empty()) {
                Slotcar::processData();
                SlotcarRL::postProcess();
                //Slotcar::printSegments();
            }

            // Lap >= 2 and not recovering...
            if (!learning) {
                learning = true;
                this->lastState = getCurrentState(cs);
                action = chooseAction(this->lastState);
            }
            clutch = 0;
            gear = Slotcar::selectGear(cs);
            steer = getSimpleSteer(cs);
            //steer = getSteerPID(cs);
            //steer = Slotcar::getSteer(cs);

            action -= 2;
            accel = brake = 0.0;
            if (action > 0) {
                accel = SlotcarRL::TCS(cs, action / 2.0);
                wasBraking = false;
            } else if (action < 0) {
                brake = SlotcarRL::ABS(cs, -action / 2.0, wasBraking);
                wasBraking = true;
            }
            action += 2;
            SlotcarRL::ESC(accel, brake, steer, cs);
        }
    }

    CarControl cc(accel, brake, gear, steer, clutch);

    // Updating lastDistance only after everything else is done.
    this->lastDistance = cs.getDistFromStart();
    this->lastCurLapTime = cs.getCurLapTime();

    return cc;
}

// Anti-lock braking system.
float SlotcarRL::ABS(CarState &cs, float brake, bool wasBraking) {
    static float B = 0.0;
    static float b = 0.0;
    static float step = 0.0;
    static float lastStableBrake = 0.0;
    static bool hasLocked = false;

    // Ramping
    if (!wasBraking || fabs(B - brake) <= 0.01) {
        // Reset.
        if (!wasBraking) {
            b = lastStableBrake = 0.0;
        }
        B = brake;
        step = 0.05; // if b > B then b <- B down the code.
    }

    // Car speed in m/s.
    float speed = cs.getSpeedX() / 3.6;
    if (speed > 0) speed = max(speed, 0.0001f);
    else speed = min(speed, -0.0001f);

    // WheelSpinVel in rad/s.
    float frontWheelsVel = (cs.getWheelSpinVel(0) +
                      cs.getWheelSpinVel(1)) * FRONT_WHEEL_RADIUS;
    float rearWheelsVel = (cs.getWheelSpinVel(2) +
                     cs.getWheelSpinVel(3)) * REAR_WHEEL_RADIUS;
    float wheelsVel = (frontWheelsVel + rearWheelsVel) / 4;
    float slip = (speed - wheelsVel) / speed;

    if (slip > 0.2) {
        // Instable zone -> no grip.
        //cout<<" * ";
        b = 0.05;
        hasLocked = true;
    } else {
        if (hasLocked) {
            b = 0.85 * lastStableBrake;
        } else {
            lastStableBrake = b;
        }
        if (slip > 0.18) {
            // Optimal zone -> it is a bad idea to brake harder.
            //cout<<" - ";
        } else {
            // Safe zone -> not at maximum braking efficiency.
            //cout<<"   ";
            b = min((b + step), B);
        }
        hasLocked = false;
    }
    brake = b;

    /*
    cout<<"[ABS]: v(m/s): "<<setw(7)<<fixed<<setprecision(4)<<speed<<"; vy: "
        <<setw(7)<<setprecision(3)<<(cs.getSpeedY()/3.6)
        <<"; w[0]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(0)*FRONT_WHEEL_RADIUS
        <<"; w[1]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(1)*FRONT_WHEEL_RADIUS
        <<"; w[2]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(2)*REAR_WHEEL_RADIUS
        <<"; w[3]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(3)*REAR_WHEEL_RADIUS
        <<" w: "<<wheelsVel<<" slip: "<<slip<<" b: "<<brake<<" B: "<<B<<endl;
    */
    return brake;
}

// Traction control system.
float SlotcarRL::TCS(CarState &cs, float accel) {
    /*
    // Car speed in m/s.
    float speed = cs.getSpeedX() / 3.6;
    if (speed > 0) speed = max(speed, 0.0001f);
    else speed = min(speed, -0.0001f);

    // WheelSpinVel in rad/s.
    float rearWheelsVel = (cs.getWheelSpinVel(2) +
                     cs.getWheelSpinVel(3)) * REAR_WHEEL_RADIUS;
    float wheelsVel = rearWheelsVel / 2;
    float slip = (wheelsVel - speed) / speed;

    cout<<" [TCS]: v(m/s): "<<setw(7)<<fixed<<setprecision(4)<<speed<<"; vy: "
        <<setw(7)<<setprecision(3)<<(cs.getSpeedY()/3.6)
        <<"; w[0]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(0)*FRONT_WHEEL_RADIUS
        <<"; w[1]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(1)*FRONT_WHEEL_RADIUS
        <<"; w[2]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(2)*REAR_WHEEL_RADIUS
        <<"; w[3]: "<<setw(7)<<setprecision(4)<<cs.getWheelSpinVel(3)*REAR_WHEEL_RADIUS
        <<" w: "<<setw(7)<<setprecision(4)<<wheelsVel<<" slip: "<<setw(8)
        <<slip<<" gear: "<<cs.getGear()<<" a: "<<accel<<endl;
    */
    return accel;
}

// "Eletronic" stability control.
void SlotcarRL::ESC(float &accel, float &brake, float &steer, CarState &cs) {
/**********/
//return;
/**********/
    float speedY = fabs(cs.getSpeedY());
    float intention = accel - brake;

    if (speedY >= 4.0) {
        // Out of control.
        if(debug){cout<<"<INSTABLE!> i: "<<intention<<"->0.0 vy: "
                      <<cs.getSpeedY()<<" vx: "<<cs.getSpeedX()<<endl;}
        intention = 0.0;
    } else if (speedY >= 3.5) {
        if(debug){cout<<"<Instable> i: "<<intention<<"->i/3 vy: "
                      <<cs.getSpeedY()<<" vx: "<<cs.getSpeedX()<<endl;}
        intention *= 0.33;
    } else if (speedY >= 2.55) {
        // Close to the limit. Better do not push harder.
        if(debug){cout<<"<warning> i: "<<intention<<"->i2/3 vy: "
                      <<cs.getSpeedY()<<" vx: "<<cs.getSpeedX()<<endl;}
        intention *= 0.5;
    } else if (speedY >= 2.0 && 
               fabs(cs.getAngle()) < 0.75 * Slotcar::stuckAngle) {
        intention *= 0.1;
    } else if (speedY >= 1.2 &&
               fabs(cs.getAngle()) < 0.75 * Slotcar::stuckAngle) {
        intention *= 0.05;
    }

    accel = max(intention, 0.0f);
    brake = -1.0 * min(intention, 0.0f);
}

bool SlotcarRL::isNearEnd(int state) {
    return (state % (2 * N_FOLLWING) >= N_FOLLWING);
}

bool SlotcarRL::inBrakingZone(int state) {
    int type = abs(getType(this->currentSegment));
    return ((type == MEDIUM_STRAIGHT || type == LONG_STRAIGHT ||
             type == LONG_HARD || type == LONG_MEDIUM || type == LONG_EASY) &&
            isNearEnd(state));
}

bool SlotcarRL::farFromBZone(CarState &cs, unsigned int lastBZoneSegment) {
    bool far = false;
    // True if...
    // - is in third segment from braking zone;
    unsigned int end = lastBZoneSegment + 3;
    if (this->currentSegment >= end % this->segmentsEnd.size() &&
        (end < this->segmentsEnd.size() ||
         this->currentSegment < lastBZoneSegment)) {
        far = true;
    }
//----------------------------------------------------------------------------
    int type = abs(getType(this->currentSegment));
    float begin = 0;
    float dfs = cs.getDistFromStart();
    float dfb = 0; // distFromBegin.
    if (this->currentSegment == 0) {
        begin = this->segmentsEnd[this->segmentsEnd.size() - 1];
        dfb = dfs - begin;
        if (begin > this->trackLength) {
            begin = 0.0;
            dfb = dfs;
        }
        if (dfs < begin) {
            dfb = dfs + (this->trackLength - begin);
        }
    } else {
        begin = this->segmentsEnd[this->currentSegment - 1];
        dfb = dfs - begin;
    }
    float currLength = this->segmentsLength[this->currentSegment];
    float percent = dfb / currLength;

/*
    // - is after the apex of a short not easy turn;
    if ((type == HAIRPIN || type == ELBOW) && percent >= 0.7) {
        far = true;
    }
*/
//----------------------------------------------------------------------------
    // - is after 10% of a "preparing" segment
    if ((type == MEDIUM_STRAIGHT || type == LONG_STRAIGHT || type == LONG_HARD
        || type == LONG_MEDIUM || type == LONG_EASY) && percent >= 0.15) {
        far = true;
    }

    return far;
}

void SlotcarRL::applyBadTrace(vector<int>& brakingStates,
                              vector<int>& brakingActions,
                              vector<int>& farStates, vector<int>& farActions,
                              bool wasInBrakingZone, float r, float incentive) {

bool D = true;
    bool used[N_SPEEDS * 2 * N_FOLLWING][N_ACTIONS];
    for (int i = 0 ; i < N_SPEEDS * 2 * N_FOLLWING; i++)
        for (int j = 0; j < N_ACTIONS; j++) used[i][j] = false;
    int punishments = 0;
    float lambda = LAMBDA;
    if (incentive < 0) {
        // Car has gone out of track.

        vector<int>::reverse_iterator s;
        vector<int>::reverse_iterator a;
        vector<int>::reverse_iterator end;

        if (wasInBrakingZone) {
            if(D)cout<<" BZ ";
            s = brakingStates.rbegin();
            a = brakingActions.rbegin();
            end = brakingStates.rend();
            for (; s != end; s++, a++) {
                int i = (*s) % (N_SPEEDS * 2 * N_FOLLWING);
                if (used[i][*a]) continue; used[i][*a] = true;

                updateQ(*s, *a, r, lambda, incentive);
                lambda *= LAMBDA;
                if(D) cout << "[" << *s << ";" << *a << "].";
            }
            if(D) cout << endl;
        } else { // !wasInBrakingZone...
            // A fraction of punishment goes to actions taken after the braking
            // zone, giving more weight to extreme actions.
            if(D)cout<<" !Z ";
            s = farStates.rbegin();
            a = farActions.rbegin();
            end = farStates.rend();
            int stretchType = abs(this->segmentsType[this->currentSegment]);
//cout<<" T: "<<stretchType;
            for (; s != end; s++, a++) {
                int i = (*s) % (N_SPEEDS * 2 * N_FOLLWING);
                if (used[i][*a]) continue; used[i][*a] = true;

                // If the car is instable (after the end of a turn) pretend to
                // still be at last segment.
                int stateType = *s / (N_SPEEDS * 2 * N_FOLLWING);
//cout<<" (t: "<<stateType<<" N: "<<isNearEnd(*s)<<") ";

                // Increase the "risk" level.
                if (*a == QBest[*s][0] && epsilon <= 0.002) {
                    QBest[*s][1] += 1;
                    if (QBest[*s][1] > 4) {
                        // Give up the action: too risky.
cout<<"     --------------------Giving up: "<<*s<<", "<<QBest[*s][0]<<endl;
                        if (QBest[*s][0] > N_ACTIONS / 2) QBest[*s][0] -= 1;
                        else QBest[*s][0] = -1;
                        QBest[*s][1] = 0;
                    }
                }
                if (*a > N_ACTIONS / 2 && (
                    (brakingStates.empty() && isNearEnd(*s)) ||
                    (!isNearEnd(*s) && isEasy(stateType) &&
                     stateType != stretchType) ||
                    (!isNearEnd(*s) && !isEasy(stateType)) ||
                    (isNearEnd(*s) && !isEasy(stateType) &&
                     stateType == stretchType))) {

                    updateQ(*s, *a, r, 0.5, 10 * incentive);
                    punishments++;
                    if(D) cout << "[" << *s << ";" << *a << "; b:"
                               << QBest[*s][0] << "," << QBest[*s][1] << "].";
                } else {
                    updateQ(*s, *a, r, 0.05, incentive);
                    if(D) cout << "<" << *s << ";" << *a << ">.";
                }
            }
            if(D) cout << "||";
            for (int i = 0 ; i < N_SPEEDS * 2 * N_FOLLWING; i++)
                for (int j = 0; j < N_ACTIONS; j++) used[i][j] = false;
            int worstAction = N_ACTIONS - 1;
            lambda = LAMBDA;
            while (punishments == 0 && worstAction > 0 &&
                   !brakingStates.empty()) {
                s = brakingStates.rbegin();
                a = brakingActions.rbegin();
                end = brakingStates.rend();
                if(D) cout << " (w=" << worstAction << ") .";
                for (; s != end; s++, a++) {
                    int i = (*s) % (N_SPEEDS * 2 * N_FOLLWING);
                    if (used[i][*a]) continue;

                    if (*a == worstAction) {
                        updateQ(*s, *a, r, lambda, incentive);
                        lambda *= LAMBDA;
                        punishments++;
                        used[i][*a] = true;
                        if (*a == QBest[*s][0] && epsilon <= 0.002) {
                            QBest[*s][1] += 1;
                            if (QBest[*s][1] > 4) {
                                // Give up the action: too risky.
    cout<<"     ++++++++++++++++++++Giving up: "<<*s<<", "<<QBest[*s][0]<<endl;
                                if (QBest[*s][0] > N_ACTIONS / 2) QBest[*s][0] -= 1;
                                else QBest[*s][0] = -1;
                                QBest[*s][1] = 0;
                            }
                        }
                        if(D) cout << "[" << *s << ";" << *a << "; b:"
                                   << QBest[*s][0] << "," << QBest[*s][1] << "].";
                    }
                    else if(D) cout << "<" << *s << ";" << *a << ">.";
                }
                worstAction--;
            }
            if (punishments == 0 && !brakingStates.empty()) {
                // If all actions were braking and the car still goes out of
                // track, then it should brake earlier (increase close_to_end).
                int type = abs(*(brakingStates.rbegin())) /
                               (N_SPEEDS * 2 * N_FOLLWING);
                int follow = abs(*(brakingStates.rbegin())) % N_FOLLWING;
                int index = type * N_FOLLWING + follow;

                closeToEnd[index] += 2.5;
                cout << "  (close to end of " << STRETCH_TYPES_TABULAR[type]
                     << follow << " = " << closeToEnd[index] << ")" << endl;
            }
        }
        if(D) cout << endl;
    } else { // Car has stopped.
        // Current zone trace.
        vector<int>::reverse_iterator s1, S1;
        vector<int>::reverse_iterator a1, A1;
        vector<int>::reverse_iterator end1;

        // Previous zone trace.
        vector<int>::reverse_iterator s2, S2;
        vector<int>::reverse_iterator a2, A2;
        vector<int>::reverse_iterator end2;

        if (wasInBrakingZone) {
            s1 = S1 = brakingStates.rbegin();
            a1 = A1 = brakingActions.rbegin();
            end1    = brakingStates.rend();
            s2 = S2 = farStates.rbegin();
            a2 = A2 = farActions.rbegin();
            end2    = farStates.rend();
        } else {
            s1 = S1 = farStates.rbegin();
            a1 = A1 = farActions.rbegin();
            end1    = farStates.rend();
            s2 = S2 = brakingStates.rbegin();
            a2 = A2 = brakingActions.rbegin();
            end2    = brakingStates.rend();
        }

        // Find the last braking.
        for (s1 = S1, a1 = A1; s1 != end1 && punishments <= 0; s1++, a1++) {
            int i = (*s1) % (N_SPEEDS * 2 * N_FOLLWING);
            if (used[i][*a1]) continue;

            if (*a1 < N_ACTIONS / 2) {
                updateQ(*s1, *a1, r, lambda, incentive);
                used[i][*a1] = true;
                punishments++;
                if (*s1 % N_FOLLWING == 0) {
                    // ... also update fast.
                    updateQ(*s1 + 1, *a1, r, lambda, incentive);
                    if(D)cout << "[" << *s1 << "|" << (*s1 + 1) << ";";
                } else {
                    // ... also update slow.
                    updateQ(*s1 - 1, *a1, r, lambda, incentive);
                    if(D)cout << "[" << *s1 << "|" << (*s1 - 1) << ";";
                }
                lambda *= LAMBDA;
            }
        }
        if (punishments <= 0) {
            // If wasn't braking, find last no action.
            for (s1 = S1, a1 = A1; s1 != end1 && punishments <= 0; s1++, a1++) {
                int i = (*s1) % (N_SPEEDS * 2 * N_FOLLWING);
                if (used[i][*a1]) continue;

                if (*a1 == N_ACTIONS / 2) {
                    updateQ(*s1, *a1, r, lambda, incentive);
                    used[i][*a1] = true;
                    punishments++;
                    if (*s1 % N_FOLLWING == 0) {
                        // ... also update fast.
                        updateQ(*s1 + 1, *a1, r, lambda, incentive);
                        if(D)cout << "[" << *s1 << "|" << (*s1 + 1) << ";";
                    } else {
                        // ... also update slow.
                        updateQ(*s1 - 1, *a1, r, lambda, incentive);
                        if(D)cout << "[" << *s1 << "|" << (*s1 - 1) << ";";
                    }
                    lambda *= LAMBDA;
                }
            }
        }
        if (punishments <= 0) {
            // Find the last braking.
            for (s2 = S2, a2 = A2; s2 != end2 && punishments <= 0; s2++, a2++) {
                int i = (*s2) % (N_SPEEDS * 2 * N_FOLLWING);
                if (used[i][*a2]) continue;

                if (*a2 < N_ACTIONS / 2) {
                    updateQ(*s2, *a2, r, lambda, incentive);
                    used[i][*a2] = true;
                    punishments++;
                    if (*s2 % N_FOLLWING == 0) {
                        // ... also update fast.
                        updateQ(*s2 + 1, *a2, r, lambda, incentive);
                        if(D)cout << "[" << *s2 << "|" << (*s2 + 1) << ";";
                    } else {
                        // ... also update slow.
                        updateQ(*s2 - 1, *a2, r, lambda, incentive);
                        if(D)cout << "[" << *s2 << "|" << (*s2 - 1) << ";";
                    }
                    lambda *= LAMBDA;
                }
            }
        }
        if (punishments <= 0) {
            // If wasn't braking, find last no action.
            for (s2 = S2, a2 = A2; s2 != end2 && punishments <= 0; s2++, a2++) {
                int i = (*s2) % (N_SPEEDS * 2 * N_FOLLWING);
                if (used[i][*a2]) continue;

                if (*a2 == N_ACTIONS / 2) {
                    updateQ(*s2, *a2, r, lambda, incentive);
                    used[i][*a2] = true;
                    punishments++;
                    if (*s2 % N_FOLLWING == 0) {
                        // ... also update fast.
                        updateQ(*s2 + 1, *a2, r, lambda, incentive);
                        if(D)cout << "[" << *s2 << "|" << (*s2 + 1) << ";";
                    } else {
                        // ... also update slow.
                        updateQ(*s2 - 1, *a2, r, lambda, incentive);
                        if(D)cout << "[" << *s2 << "|" << (*s2 - 1) << ";";
                    }
                    lambda *= LAMBDA;
                }
            }
        }
    }
    brakingStates.clear();
    brakingActions.clear();
    farStates.clear();
    farActions.clear();
}

void SlotcarRL::applyGoodTrace(vector<int>& states, vector<int>& actions) {
    vector<int>::iterator s;
    vector<int>::iterator a;

    if (states.empty()) return;

    if (cleanWait >= 2) {
        cleanZone = true;
    } else {
        cleanWait++;
        cleanZone = false;
    }

    //if (cleanZone) {
    if (cleanZone && epsilon > 0.0) {
        bool used[N_SPEEDS * 2 * N_FOLLWING][N_ACTIONS];
        for (int i = 0 ; i < N_SPEEDS * 2 * N_FOLLWING; i++)
            for (int j = 0; j < N_ACTIONS; j++) used[i][j] = false;

        if(debug)cout<<" GT: ";
        for (s = states.begin(), a = actions.begin(); s != states.end();
             s++, a++) {
            int i = (*s) % (N_SPEEDS * 2 * N_FOLLWING);
            if (used[i][*a]){if(debug)cout<<"<"<<*s<<", "<<*a<<"> ";continue; used[i][*a] = true;}

            float absR = 0.005;
            for (int j = 0; j < N_ACTIONS; j++) {
                Q[*s][j] += absR * (j - *a);
            }
            totalReward -= absR;
            if(debug)cout<<"["<<*s<<", "<<*a<<"] ";
        }
        if(debug)cout<<endl;
    }
    states.clear();
    actions.clear();
}

void SlotcarRL::applyLapTrace() {
    static float bestLapTime = -1.0;
    static float bestLapBonus = 0.5;
    static float bestReinforce = 0.05;
    bool D = true; // Debug.

    if (cleanLap) {
        float timeDiff = this->lastCurLapTime - bestLapTime;

        // If first clean lap or fastest lap...
        if (bestLapTime < 0 || timeDiff <= 0.0) {
            bestLapTime = this->lastCurLapTime;
            bestReinforce = 0.05;

            float bonus = 0;
            int nDiffs = 0;
            for (int s_t = 0; s_t < N_STATES; s_t++) {
                QBest[s_t][1] = 0;
                for (int a_t = 0; a_t < N_ACTIONS; a_t++) {
                    if (!QUsed[s_t][a_t]) continue;

                    int b = QBest[s_t][0]; // Best action.
                    if (a_t != b) {
                        if(D)cout<<"  s: "<<s_t<<" a_t: "<<a_t<<"("<<Q[s_t][a_t]
                                 <<") b: "<<b<<"("<<Q[s_t][b]
                                 <<") - bonus: "<<bestLapBonus<<endl;
                        Q[s_t][a_t] += bestLapBonus;
                        totalReward += bestLapBonus;
                        bonus += bestLapBonus;
                        nDiffs++;

                        if (!QUsed[s_t][b] ||
                            (QUsed[s_t][b] && (a_t > b ||
                                               Q[s_t][a_t] > Q[s_t][b]))) {
                            QBest[s_t][0] = a_t;
                        }
                    }
                }
            }
            cout << "<!> Best lap: " << bestLapTime << " bonus: " << bonus
                 << " nDiffs: " << nDiffs << endl;
        } else {
            float bonus = 0;
            int nDiffs = 0;
            for (int s_t = 0; s_t < N_STATES; s_t++) {
                for (int a_t = 0; a_t < N_ACTIONS; a_t++) {
                    if (!QUsed[s_t][a_t]) continue;

                    if (a_t != QBest[s_t][0]) {
                        if (QBest[s_t][0] >= 0) {
                            //float punishment = max(-0.12 * timeDiff, -0.5);
                            /*float punishment = -0.5;
                            if ((QBest[s_t][0] >= N_ACTIONS / 2 &&
                                 a_t > QBest[s_t][0]) ||
                                (QBest[s_t][0] < N_ACTIONS / 2 &&
                                 a_t < QBest[s_t][0])) {
                                punishment *= 0.2;
                            }
                            if (epsilon <= 0.002) punishment /= pow(2, QBest[s_t][1]);*/
                            //if (epsilon <= 0.002) punishment /= (1 + QBest[s_t][1]);
                            float punishment = max(-0.01 * pow(2, 4*timeDiff),
                                                   -0.5);

                            if(D)cout<<"  s: "<<s_t<<" a_t: "<<a_t<<"("<<Q[s_t][a_t]
                                     <<") b: "<<QBest[s_t][0]<<"<"<<QBest[s_t][1]
                                     <<">(" <<Q[s_t][QBest[s_t][0]]
                                     <<") - punishment: "<<punishment<<endl;
                            Q[s_t][a_t] += punishment;
                            totalReward += punishment;
                            bonus += punishment;
                            nDiffs++;
                        }
                    }
                }
            }
            if(D)cout << "  diff: " << timeDiff << " bonus: " << bonus
                      << " nDiffs: " << nDiffs << endl;
        }
    }

    /*** Reinforce the reward of best lap. ***/
    if (epsilon <= 0.008 && bestLapTime > 0) {
        if(D)cout<<" ... bestReinforce: "<<setprecision(6)<<bestReinforce
                 <<" E: "<<epsilon<<endl;
        for (int i = 0; i < N_STATES; i++) {
            if (QBest[i][0] >= 0) {
                // Reinforce only until value gets a little positive.
                if (Q[i][QBest[i][0]] <= 1.0) {
                    Q[i][QBest[i][0]] += bestReinforce;
                }
            }
        }
        if (epsilon <= 0.002) bestReinforce += 0.0005;
    }

    for (int i = 0; i < N_STATES; i++)
        for (int j = 0; j < N_ACTIONS; j++) QUsed[i][j] = false;
    cleanLap = true;
}

void SlotcarRL::updateQ(int s, int a, float r, float lambda, float incentive) {
    for (int j = 0; j < N_ACTIONS; j++) {
        // If the car has stopped, do not punish accelerating. It it has gone
        // out of the track, do not punish braking.
        if ((incentive < 0 && j < a) ||
            (incentive > 0 && j > a)) {
            continue;
        }
        if (j == a) {
            Q[s][j] += lambda * r;
        } else {
            // Give increasing reward (punishment) to the farthest action.
            // Ex: a = 0; j = 4; r < 0; incentive < 0 ---> big punishment.
            Q[s][j] += lambda * r * incentive * (a - j);
        }
    }
}

float SlotcarRL::getSimpleSteer(CarState &cs) {
//return Slotcar::getSteer(cs);
    float targetAngle;
    float steer;

    // Preparing information ---------------------------------------------------
    int currType = getType(this->currentSegment);
    float currEnd = this->segmentsEnd[this->currentSegment];
    float begin = 0;
    float dfs = cs.getDistFromStart();
    float dfb = 0; // distFromBegin.
    if (this->currentSegment == 0) {
        begin = this->segmentsEnd[this->segmentsEnd.size() - 1];
        dfb = dfs - begin;
        if (begin > this->trackLength) {
            begin = 0.0;
            dfb = dfs;
        }
        if (dfs < begin) {
            dfb = dfs + (this->trackLength - begin);
        }
    } else {
        begin = this->segmentsEnd[this->currentSegment - 1];
        dfb = dfs - begin;
    }
    float currLength = this->segmentsLength[this->currentSegment];
    float percent = dfb / currLength;
    float attackPoint = currEnd - pow(cs.getSpeedX() / 60.0, 3);

    int nextType = getType(this->currentSegment + 1);
    bool currIsEasy = isEasy(currType);
    bool nextIsEasy = isEasy(nextType);
    bool nextIsOpposite = (currType * nextType < 0.0);
    // -------------------------------------------------------------------------

    bool D = debug;
    static int stage = 1000;
    static unsigned int s = 1000;
    if (s != this->currentSegment) {
        s = this->currentSegment;
        stage = 1000;
        if(D){cout<<"------ "<<setw(3)<<s<<" type: "<<currType<<" next: "
                  <<nextType<<endl;}
    }

    if (currIsEasy) {
        if (abs(currType) == SHORT_EASY || (nextIsEasy &&
            (abs(nextType) == LONG_EASY || (abs(currType) != MEDIUM_STRAIGHT &&
            abs(currType) != LONG_STRAIGHT)))) {

            float side = 0.0;
            if (abs(currType) > LONG_STRAIGHT) {
                side = currType;
            } else if (abs(nextType) > LONG_STRAIGHT) {
                side = -nextType;
            }
            bool resetAttack = false;
            if (stage != -1) {
                stage = -1;
                if(D){cout<<"[EASY:  ] Super easy..."<<endl;}
                resetAttack = true;
            }
            targetAngle = attackAnglePID(cs, side, resetAttack, true);
        //} else if (abs(nextType) == SHORT_EASY && dfs > attackPoint - 15) {
        } else if (abs(nextType) == SHORT_EASY && dfs > attackPoint) {
            bool resetAttack = false;
            if (stage != -2) {
                stage = -2;
                if(D){cout<<"[EASY:  ] Short..."<<endl;}
                resetAttack = true;
            }
            //targetAngle = attackAnglePID(cs, nextType, resetAttack, true);
            targetAngle = attackAnglePID(cs, nextType, resetAttack);
        } else if (!nextIsEasy &&
            ((attackPoint < begin && dfs < begin && dfs > attackPoint) ||
            ((attackPoint > begin) && (dfs < begin || dfs > attackPoint)))) {

            // In the end of segment, aim for inside corner.
            bool resetAttack = false;
            if (stage != 0) {
                stage = 0;
                if(D){cout<<"[EASY:  ] Attacking..."<<endl;}
                resetAttack = true;
            }
            targetAngle = attackAnglePID(cs, nextType, resetAttack);
        } else if (percent < 0.075) {
            // In the beginning of non curve segments, just stabilize the car.
            if(D&&stage!=1){stage=1;cout<<"[EASY:  ] Stabilizing..."<<endl;}
            targetAngle = stabilizingAnglePID(cs);
        } else {
            // Prepare for next turn.
            unsigned int n = this->currentSegment + 1;
            for (; nextIsEasy && n != this->currentSegment; n++) {
                nextType = getType(n);
                nextIsEasy = (isEasy(nextType) && abs(nextType) != SHORT_EASY);
                if (n == this->segmentsEnd.size() - 1) {
                    n = -1;
                }
            }
            bool firstTicPreparing = false;
            if (n == this->currentSegment) {
                // All segments are easy.
                if(D&&stage!=2){stage=2;cout<<"[EASY:  ] Centering..."<<endl;}
                targetAngle = preparingAnglePID(cs, currType, 0.0);
            } else {
                if (stage != 3) {
                    if(D){cout<<"[EASY:  ] Preparing... next: "<<nextType<<endl;}
                    stage = 3;
                    firstTicPreparing = true;
                }
                targetAngle = preparingAnglePID(cs, nextType, 0.72,
                                                firstTicPreparing);
            }
        }
    } else { // Current segment is a turn.
        if (nextIsEasy) {
            /* Simple turn. */
            if (percent <= APEX) {
                // Until not at apex...
                bool resetAttack = false;
                if (stage != 4) {
                    if(D){cout<<"[SIMPLE:] Attacking..."<<nextType<<endl;}
                    stage = 4;
                    resetAttack = true;
                }
                targetAngle = attackAnglePID(cs, currType, resetAttack);
            } else {
                // Leaving the turn.
                if(D&&stage!=6){stage=6;cout<<"[SIMPLE:] Stabilizing..."<<endl;}
                targetAngle = stabilizingAnglePID(cs);
            }
        } else if (nextIsOpposite) {
            /* "S" turn. */
            if (percent <= APEX) {
                bool resetAttack = false;
                if (stage != -7) {
                    stage = -7;
                    if(D){cout<<"[S TURN:] Attacking..."<<endl;}
                    resetAttack = true;
                }
                targetAngle = attackAnglePID(cs, currType, resetAttack);
            } else {
                bool resetAttack = false;
                if (stage != 7) {
                    stage = 7;
                    if(D){cout<<"[S TURN:] Attacking2..."<<endl;}
                    resetAttack = true;
                }
                targetAngle = attackAnglePID(cs, nextType, resetAttack);
            }
        } else {
            /* "U" turn. */
            if (percent >= 0.2 && (abs(currType) == SHORT_MEDIUM ||
                abs(currType) == LONG_MEDIUM)) {

                bool firstTicPreparing = false;
                if (stage != 10) {
                    if(D){cout<<"[U TURN:] Preparing..."<<endl;}
                    stage = 10;
                    firstTicPreparing = true;
                }
                targetAngle = preparingAnglePID(cs, nextType, 0.2,
                                                firstTicPreparing);
            } else {
                if (percent <= APEX) {
                    // Until not at apex...
                    bool resetAttack = false;
                    if (stage != 11) {
                        stage = 11;
                        if(D){cout<<"[U TURN:] Attacking..."<<endl;}
                        resetAttack = true;
                    }
                    targetAngle = attackAnglePID(cs, currType, resetAttack);
                } else {
                    // Leaving the turn.
                    if(D&&stage!=12){stage=12;cout<<"[U TURN:] Stabilizing..."<<endl;}
                    targetAngle = stabilizingAnglePID(cs);
                }
            }
        }
    }

    steer = targetAngle / STEER_LOCK;
    steer = max<float>(min<float>(1.0, steer), -1.0);
    return steer;
}

int SlotcarRL::getType(int segment) {
    return this->segmentsType[segment % this->segmentsType.size()];
}

bool SlotcarRL::isEasy(int type) {
    return (abs(type) == SHORT_STRAIGHT || abs(type) == MEDIUM_STRAIGHT ||
            abs(type) == LONG_STRAIGHT  ||
            abs(type) == SHORT_EASY || abs(type) == LONG_EASY);
}

float SlotcarRL::attackAnglePID(CarState &cs, float side, bool reset,
                                bool easy) {
    int middleSensor = TRACK_SENSORS_NUM / 2;
    static int lastBestSensor = middleSensor;
    static float A = 0.0;
    static float a = 0.0;
    static float rampStep = 0.0;
    float targetAngle = 0.0;

    if (reset) {
        lastBestSensor = middleSensor;
        atkPID.reset();
        a = -1.0 * SENSORS[lastBestSensor] * (PI / 180.0);
        A = 0.0;
    }

    int bestSensor = middleSensor;
    float bestDist = cs.getTrack(bestSensor);
    for (int i = middleSensor + 1; i < TRACK_SENSORS_NUM; i++) {
        // Search for best sensor giving preference to closest to the middle.
        if (cs.getTrack(i) > bestDist) {
            // Sensors at right side.
            bestDist = cs.getTrack(i);
            bestSensor = i;
        }
        if (cs.getTrack(TRACK_SENSORS_NUM - i) > bestDist) {
            // Sensors at left side.
            bestDist = cs.getTrack(TRACK_SENSORS_NUM - i);
            bestSensor = TRACK_SENSORS_NUM - i;
        }
    }

    if (bestSensor != 0 && bestSensor != TRACK_SENSORS_NUM - 1) {
        if (side < 0) side = RIGHT;
        else side = LEFT;

        if (cs.getTrack(bestSensor - 1) == cs.getTrack(bestSensor + 1)) {
            side = 0.0;
        } else if (cs.getTrack(bestSensor - 1) > cs.getTrack(bestSensor + 1)) {
            bestSensor++; // The right sensor is current best estimative of tg.
            side = RIGHT;
        } else {
            bestSensor--; // The left sensor is current best estimative of tg.
            side = LEFT;
        }
    }

    float l = cs.getTrack(bestSensor);
    float correction = side * atan2(TRACK * 0.75, l);
    float A_ = -1.0 * SENSORS[bestSensor] * (PI / 180.0) - correction;
    A = 0.68 * A + 0.32 * A_; // Make smooth changes.

    if (reset || lastBestSensor != bestSensor) {
        // Ramp setpoint.
        //atkPID.reset();
        rampStep = 0.02;
        if (A < a) {
            rampStep *= -1;
        }
        /*cout<<"--- reset ramp --- r: "<<reset<<" lBS: "<<lastBestSensor
            <<" bS: "<<bestSensor<<" rS: "<<rampStep<<endl;*/
    }
    lastBestSensor = bestSensor;

    if (fabs(a - A) >= fabs(rampStep)) {
        a += rampStep;
    } else {
        a = A;
    }
    targetAngle = a;
    /*cout<<" t0: "<<targetAngle<<" side: "<<side<<" a: "<<a<<" A: "<<A
        <<" C: "<<correction;*/
    if (easy) {
        // Only "P" when in super easy segments.
        targetAngle *= 0.4;
        //cout<<" E";
    } else {
        targetAngle = -1.0 * atkPID.compute(0.0, targetAngle);
        //cout<<"  ";
    }
    /*cout<<" ta: "<<targetAngle<<" steer: "<<targetAngle / STEER_LOCK<<" best("
        <<bestSensor<<"): "<<SENSORS[bestSensor]<<endl;*/

    return targetAngle;
}

float SlotcarRL::preparingAnglePID(CarState &cs, float nextType, float pos,
                                   bool resetRamp) {
    static float trueP = 0.0;
    static float lastP = 0.0;
    static float rampStep = 0.0;

    float targetAngle = 0.0;
    if (nextType > 0) {
        pos *= -1.0; // Next is to the left, so stay on the right.
    }
    float p = cs.getTrackPos();

    // Start ramping to reduce derivative jump when setpoint changes.
    if (resetRamp || trueP != pos) {
        trueP = pos;
        lastP = p;
        //rampStep = 0.1 * pow(2.0, -0.023 * cs.getSpeedX());
        rampStep = 0.035;
        if (lastP > trueP) {
            rampStep *= -1;;
        }
        prepPID.reset();
    }

//    targetAngle = 3.0 * prepPID.compute(lastP, p);
    targetAngle = 2.0 * prepPID.compute(lastP, p);
    if (fabs(p) > fabs(lastP) && p * lastP >= 0) {
        // The car has passed P.
        targetAngle *= (55 * fabs(lastP - p));
    }
    /*cout<<" ta: "<<targetAngle<<" a: "<<cs.getAngle()<<" p: "<<p<<" P: "
        <<lastP<<" tP: "<<trueP<<" ram: "<<rampStep<<" steer: "
        <<targetAngle / STEER_LOCK<<endl;*/

    // Ramp step.
    if (fabs(lastP - trueP) > fabs(rampStep)) {
        lastP += rampStep;
    } else {
        lastP = trueP;
    }

    return targetAngle;
}

/* Steering method used in the first lap. */
float SlotcarRL::getSteerPID(CarState &cs, float pos) {
    /*cout<<setprecision(9);*/
    float targetAngle = posPID.compute(pos, cs.getTrackPos());
    float steer = targetAngle / STEER_LOCK;
    /*cout<<" ta: "<<targetAngle<<" p: "<<cs.getTrackPos()<<" P: "<<pos
        <<" steer: "<<steer<<endl;*/
    steer = max<float>(min<float>(1.0, steer), -1.0);

    return steer;
}

float SlotcarRL::getReverseSteerPID(CarState &cs, float pos) {
    /*cout<<setprecision(9);*/
    float targetAngle = revposPID.compute(pos, cs.getTrackPos());
    float steer = targetAngle / STEER_LOCK;
    /*cout<<" ta: "<<targetAngle<<" p: "<<cs.getTrackPos()<<" P: "<<pos
        <<" steer: "<<steer<<endl;*/
    steer = max<float>(min<float>(1.0, steer), -1.0);

    return steer;
}

float SlotcarRL::getSpeedControl(CarState &cs, float speed) {
    float control = spdPID.compute(speed, cs.getSpeedX());
    //cout<<" v: "<<cs.getSpeedX()<<" V: "<<speed<<" ctrl: "<<control<<endl;
    return control;
}

float SlotcarRL::stabilizingAngle(CarState &cs) {
    return 3.0 * cs.getAngle();
}

float SlotcarRL::stabilizingAnglePID(CarState &cs) {
    float targetAngle = angPID.compute(0.0, cs.getAngle());
    /*cout<<" ta: "<<targetAngle<<" a: "<<cs.getAngle()<<" steer: "
        <<-targetAngle/STEER_LOCK<<endl;*/
    return -targetAngle;
}

void SlotcarRL::postProcess() {
    bool D = false;
    if(D)cout<<"= PostProcessing ="<<endl;
    for (unsigned int i = 0; i < this->segmentsType.size(); i++) {
        int follow = FAST;

        int currType = abs(getType(i));
        int nextType = abs(getType(i + 1));
        int nextNextType = abs(getType(i + 2));
        if (nextType == HAIRPIN || nextType == ELBOW ||
            nextType == SHORT_HARD) {
            follow = SLOW;
        } else if (currType != HAIRPIN && currType != ELBOW &&
                   currType != SHORT_HARD && (nextType == SHORT_STRAIGHT ||
                                              nextType == SHORT_EASY) &&
                   (nextNextType == HAIRPIN || nextNextType == ELBOW ||
                    nextNextType == SHORT_HARD)) {
            follow = SLOW;
        }
        followingType.push_back(follow);
        if(D)cout<<setw(3)<<i<<": "<<FOLLOWING[follow]<<endl;
    }
    if(D)cout<<"=================="<<endl;
}
