/***************************************************************************

    file                 : SlotcarRL.h
    created              : Wed Jul 24 18:49:42 BRT 2013
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SLOTCAR_RL_H_
#define SLOTCAR_RL_H_

#include <cmath>
#include <ctime>
#include <vector>
#include <iomanip>
#include <cstdlib>

#include "CarState.h"
#include "CarControl.h"
#include "Slotcar.h"
#include "PID.h"

#define N_SPEEDS 5
#define N_FOLLWING 2
#define SLOW 0
#define FAST 1

#define N_STATES (N_TYPES*N_SPEEDS*2*N_FOLLWING)
#define N_ACTIONS 5
#define FINAL_STATE_BAD  (-1)
#define FINAL_STATE_STOP (-2)
#define FINAL_STATE_GOOD (-3)
#define RECOVERING       (-4)
#define CLOSE_TO_END 10

#define EPSILON 0.1 // Percentage of exploration
#define EPSILON_RATE 0.975
#define ALPHA   0.1 // Learning rate. When 0, the learning stops. When 1,
                    // previous experiences are ignored.
#define GAMA    0.9 // Discount factor. When 0, only the current reward is
                    // considered.
#define LAMBDA  0.4
#define LAPS_TO_SKIP 0
#define APEX 0.6

// Used to control speed (accel and brake).
#define SCAN_SPEED 40    // Km/h.
#define KP_SPD   0.3     // Gain for current error.
#define KI_SPD   0.00006 // Gain for accumulated error.
#define KD_SPD   0.12    // Gain for error variation.

// Used for go and stay at a specific position (first lap steering).
#define KP_POS  12.0    // Gain for current error.
#define KI_POS   0.000  // Gain for accumulated error.
#define KD_POS  12.0    // Gain for error variation.

// Used for preparing the position for next turn.
#define KP_PRP   0.06   // Gain for current error.
#define KI_PRP   0.0    // Gain for accumulated error.
#define KD_PRP   3.0    // Gain for error variation.

// Used to align the car with the track axis (stabilizing).
#define KP_ANG   1.5    // Gain for current error.
#define KI_ANG   0.0003 // Gain for accumulated error.
#define KD_ANG  10.0    // Gain for error variation.

// Used to attack a turn.
#define KP_ATK   0.4    // Gain for current error.
#define KI_ATK   0.0005 // Gain for accumulated error.
#define KD_ATK   1.7    // Gain for error variation.

/**
 * Q-Matrix:
 *                              Action: Target speed
 *                              _______________________________
 * Segment type                 |  30 |  80 | 140 | 210 | Inf |
 *                              ===============================
 * stretchType; speed; nearEnd  |     |     |     |     |     |
 *                              -------------------------------
 * for stretchType in [SHORT_STRAIGHT,
 *                     MEDIUM_STRAIGHT,
 *                     LONG_STRAIGHT,
 *                     HAIRPIN,
 *                     ELBOW,
 *                     SHORT_HARD,
 *                     LONG_HARD,
 *                     SHORT_MEDIUM,
 *                     LONG_MEDIUM,
 *                     SHORT_EASY,
 *                     LONG_EASY]
 *     speed       in [30-, 80-, 140-, 210-, 210+]
 *     nearEnd     in [true, false]
 *     following   in [slow, fast]
 */

using namespace std;

class SlotcarRL : public Slotcar {
  public:
	// Constructor.
	SlotcarRL():spdPID(KP_SPD, KI_SPD, KD_SPD, -1.0, 1.0, 1),
	            posPID(KP_POS, KI_POS, KD_POS, -1.0, 1.0, 1),
	            revposPID(0.25, KI_POS, 10, -1.0, 1.0, 1),
                prepPID(KP_PRP, KI_PRP, KD_PRP, -1.0, 1.0, 1),
                angPID(KP_ANG, KI_ANG, KD_ANG, -STEER_LOCK, STEER_LOCK, 1, true),
                atkPID(KP_ATK, KI_ATK, KD_ATK, -STEER_LOCK, STEER_LOCK, 1)
                {};

	// Initialization of Q and R. Call Slotcar::init() to handle angles.
	virtual void init(float *angles);

	// Called at the end of the race, before being unloaded.
	virtual void onShutdown();

	// Called when the race is restarted.
	virtual void onRestart();

    virtual CarControl wDrive(CarState cs);

  protected:
    // Returns brake after pass through ABS filter.
    virtual float ABS(CarState &cs, float brake, bool wasBraking);
    virtual float TCS(CarState &cs, float accel);
    virtual void ESC(float &accel, float &brake, float &steer, CarState &cs);
    float getSimpleSteer(CarState &cs);
    float getSteerPID(CarState &cs, float pos = 0.0);
    float getReverseSteerPID(CarState &cs, float pos = 0.0);
    float getSpeedControl(CarState &cs, float speed);
    int getType(int segment);
    bool isEasy(int type);
    float attackAnglePID(CarState &cs, float side, bool reset = false,
                         bool easy = false);
    float preparingAnglePID(CarState &cs, float nextType, float pos=0.72,
                            bool resetRamp = false);
    float stabilizingAngle(CarState &cs);
    float stabilizingAnglePID(CarState &cs);
    void postProcess();

  private:
    static const float DELAY_DISTANCE;
    static const int   DELAY_TIME;
    // Positive values are throttle and negative ones are brake.
    static const float actions[N_ACTIONS];
    int lastState;
    PID spdPID;
    PID posPID;
    PID revposPID;
    PID prepPID;
    PID angPID;
    PID atkPID;

    int getCurrentState(CarState &cs);
    int chooseAction(int state);
    int chooseBestAction(int state);
    float getReward(CarState &cs, int state, int action);
    void printQ();
    void writeRewards();
    bool isNearEnd(int state);
    bool inBrakingZone(int state);
    bool farFromBZone(CarState &cs, unsigned int lastBZoneSegment);
    void updateQ(int s, int a, float r, float lambda, float incentive);
    void applyBadTrace(vector<int>& brakingStates, vector<int>& brakingActions,
                       vector<int>& farStates, vector<int>& farActions,
                       bool wasInBrakingZone, float reward, float incentive);
    void applyGoodTrace(vector<int>& states, vector<int>& actions);
    void applyLapTrace();
};
#endif /* SLOTCAR_RL_H_ */
