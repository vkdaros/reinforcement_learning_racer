/***************************************************************************

    file                 : StretchData.cpp
    created              : Wed Oct 3 15:06:57 BRT 2012
    copyright            : (C) 2012 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iomanip>

#include "StretchData.h"

StretchData::StretchData(int stretchNumber, 
                         float angle,
                         float steerAngle,
                         float lapTime,
                         float distFromStart,
                         float distRaced,
                         float speedX,
                         float speedY,
                         float speedZ,
                         float frontRightWheelSpin,
                         float frontLeftWheelSpin,
                         float rearRightWheelSpin,
                         float rearLeftWheelSpin,
                         float z,
                         float trackWidth) {

    this->stretchNumber = stretchNumber;
    this->angle = angle;
    this->steerAngle = steerAngle;
    this->lapTime = lapTime;
    this->distFromStart = distFromStart;
    this->distRaced = distRaced;
    this->speedX = speedX;
    this->speedY = speedY;
    this->speedZ = speedZ;
    this->frontRightWheelSpin = frontRightWheelSpin;
    this->frontLeftWheelSpin = frontLeftWheelSpin;
    this->rearRightWheelSpin = rearRightWheelSpin;
    this->rearLeftWheelSpin = rearLeftWheelSpin;
    this->z = z;
    this->trackWidth = trackWidth;
}

int StretchData::getStretchNumber() {
    return this->stretchNumber;
}

void StretchData::setStretchNumber(int number) {
    this->stretchNumber = number;
}

float StretchData::getAngle() {
    return this->angle;
}

void StretchData::setAngle(float angle) {
    this->angle = angle;
}

float StretchData::getSteerAngle() {
    return this->steerAngle;
}

void StretchData::setSteerAngle(float steerAngle) {
    this->steerAngle = steerAngle;
}

float StretchData::getLapTime() {
    return this->lapTime;
}

void StretchData::setLapTime(float lapTime) {
    this->lapTime = lapTime;
}

float StretchData::getDistFromStart() {
    return this->distFromStart;
}

void StretchData::setDistFromStart(float distFromStart) {
    this->distFromStart = distFromStart;
}

float StretchData::getDistRaced() {
    return this->distRaced;
}

void StretchData::setDistRaced(float distRaced) {
    this->distRaced = distRaced;
}

float StretchData::getSpeedX() {
    return this->speedX;
}

void StretchData::setSpeedX(float speedX) {
    this->speedX = speedX;
}

float StretchData::getSpeedY() {
    return this->speedY;
}

void StretchData::setSpeedY(float speedY) {
    this->speedY = speedY;
}

float StretchData::getSpeedZ() {
    return this->speedZ;
}

void StretchData::setSpeedZ(float speedZ) {
    this->speedZ = speedZ;
}

float StretchData::getFrontRightWheelSpin() {
    return this->frontRightWheelSpin;
}

void StretchData::setFrontRightWheelSpin(float frontRightWheelSpin) {
    this->frontRightWheelSpin = frontRightWheelSpin;
}

float StretchData::getFrontLeftWheelSpin() {
    return this->frontLeftWheelSpin;
}

void StretchData::setFrontLeftWheelSpin(float frontLeftWheelSpin) {
    this->frontLeftWheelSpin = frontLeftWheelSpin;
}

float StretchData::getRearRightWheelSpin() {
    return this->rearRightWheelSpin;
}

void StretchData::setRearRightWheelSpin(float rearRightWheelSpin) {
    this->rearRightWheelSpin = rearRightWheelSpin;
}

float StretchData::getRearLeftWheelSpin() {
    return this->rearLeftWheelSpin;
}

void StretchData::setRearLeftWheelSpin(float rearLeftWheelSpin) {
    this->rearLeftWheelSpin = rearLeftWheelSpin;
}

float StretchData::getZ() {
    return this->z;
}

void StretchData::setZ(float z) {
    this->z = z;
}

float StretchData::getTrackWidth() {
    return this->trackWidth;
}

void StretchData::setTrackWidith(float width) {
    this->trackWidth = width;
}

void StretchData::print(ofstream& outputStream) {

    outputStream << setiosflags(ios::fixed)
                 //<< setw(6) << stretchNumber << ";"
                 << setprecision(5) << setw(11) << angle << ";"
                 << setprecision(5) << setw(11) << steerAngle << ";"
                 << setprecision(5) << setw(11) << lapTime << ";"
                 << setprecision(5) << setw(11) << distFromStart << ";"
                 << setprecision(5) << setw(11) << distRaced << ";"
                 << setprecision(5) << setw(11) << speedX << ";"
                 << setprecision(5) << setw(11) << speedY << ";"
                 << setprecision(5) << setw(11) << speedZ << ";"
                 << setprecision(5) << setw(11) << frontRightWheelSpin << ";"
                 << setprecision(5) << setw(11) << frontLeftWheelSpin << ";"
                 << setprecision(5) << setw(11) << rearRightWheelSpin << ";"
                 << setprecision(5) << setw(11) << rearLeftWheelSpin << ";"
                 << setprecision(5) << setw(11) << z << endl;
                 //<< setprecision(5) << setw(11) << trackWidth << endl;
}
