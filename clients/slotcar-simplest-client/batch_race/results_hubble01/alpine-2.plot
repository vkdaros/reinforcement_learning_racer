set terminal wxt
#set terminal pngcairo size 1200,700
set grid
set title "alpine-2"
set xlabel "Distance (m)" 
set ylabel "Angle (rad)" 
plot "alpine-2.signal" title "Raw signal" with lines lw 1.5 lc 2, "alpine-2.filteredSignal" title "Filtered signal" with lines lw 1.5 lc 1, "alpine-2.flatSignal" title "Flat signal" with lines lw 1.5 lc 3
