#!/bin/bash

for f in *.plot; do
#    echo "File: $f [hit enter]";
#    read -r answer;
    echo "set terminal wxt size 350,262 enhanced font 'Verdana,10' persist;" > ${f%%.*}_rewards_laptime.plot;
    echo "plot '${f%%.*}.rewards' with linespoints ls 1, '${f%%.*}.lapTimes' with linespoints ls 2" >> ${f%%.*}_rewards_laptime.plot;
done;
