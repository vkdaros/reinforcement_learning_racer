set terminal wxt
#set terminal pngcairo size 1200,700
set grid
set title "e-track-6"
set xlabel "Distance (m)" 
set ylabel "Angle (rad)" 
plot "e-track-6.signal" title "Raw signal" with lines lw 1.5 lc 2, "e-track-6.filteredSignal" title "Filtered signal" with lines lw 1.5 lc 1, "e-track-6.flatSignal" title "Flat signal" with lines lw 1.5 lc 3
