set terminal wxt
#set terminal pngcairo size 1200,700
set grid
set title "g-track-3"
set xlabel "Distance (m)" 
set ylabel "Angle (rad)" 
plot "g-track-3.signal" title "Raw signal" with lines lw 1.5 lc 2, "g-track-3.filteredSignal" title "Filtered signal" with lines lw 1.5 lc 1, "g-track-3.flatSignal" title "Flat signal" with lines lw 1.5 lc 3
