set terminal wxt
#set terminal pngcairo size 1200,700
set grid
set title "wheel-1"
set xlabel "Distance (m)" 
set ylabel "Angle (rad)" 
plot "wheel-1.signal" title "Raw signal" with lines lw 1.5 lc 2, "wheel-1.filteredSignal" title "Filtered signal" with lines lw 1.5 lc 1, "wheel-1.flatSignal" title "Flat signal" with lines lw 1.5 lc 3
