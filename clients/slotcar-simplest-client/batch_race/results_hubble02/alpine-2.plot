set terminal wxt
#set terminal pngcairo size 1200,700
#set terminal pdf solid font 'Helvetica,13' size 10,3
#set output 'alpine-2.pdf
set title "alpine-2"
#set key right bottom
#set xrange [0:2200]
set grid
set xlabel "Distância (m)" 
set ylabel "Ângulo (rad)" 
plot \
"alpine-2.filteredSignal" title "Sinal bruto" with lines lw 2 lt rgb "green", \
"alpine-2.signal" title "Filtro de média" with lines lw 1.5 lt rgb "blue", \
"alpine-2.flatSignal" title "Filtro de patamar" with lines lw 1.5 lt rgb "red" 
