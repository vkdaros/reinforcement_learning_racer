set terminal wxt
#set terminal pngcairo size 1200,700
#set terminal pdf solid font 'Helvetica,13' size 10,3
#set output 'e-track-4.pdf
set title "e-track-4"
#set key right bottom
#set xrange [0:2200]
set grid
set xlabel "Distância (m)" 
set ylabel "Ângulo (rad)" 
plot \
"e-track-4.filteredSignal" title "Sinal bruto" with lines lw 2 lt rgb "green", \
"e-track-4.signal" title "Filtro de média" with lines lw 1.5 lt rgb "blue", \
"e-track-4.flatSignal" title "Filtro de patamar" with lines lw 1.5 lt rgb "red" 
