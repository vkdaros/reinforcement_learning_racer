set terminal wxt
#set terminal pngcairo size 1200,700
#set terminal pdf solid font 'Helvetica,13' size 10,3
#set output 'wheel-1.pdf
set title "wheel-1"
#set key right bottom
#set xrange [0:2200]
set grid
set xlabel "Distância (m)" 
set ylabel "Ângulo (rad)" 
plot \
"wheel-1.filteredSignal" title "Sinal bruto" with lines lw 2 lt rgb "green", \
"wheel-1.signal" title "Filtro de média" with lines lw 1.5 lt rgb "blue", \
"wheel-1.flatSignal" title "Filtro de patamar" with lines lw 1.5 lt rgb "red" 
