#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
import shutil
import time
import glob
from xml.dom import minidom
from subprocess import Popen # Start a new process and don't wait.
from subprocess import call  # Start a new process and wait it finishes.
from datetime import datetime


def main():
    if len(sys.argv) < 4:
        print("Usage: run_test.py <TRACK_LIST_FILE> <quickrace.xml> <client>")
        sys.exit()

    track_list_file = sys.argv[1]
    with open(track_list_file, "r") as file:
        tracks = file.read().split()

    quickrace_path = sys.argv[2]
    client_path = sys.argv[3]

    results_path = "results_" + datetime.now().strftime("%Y.%m.%d-%H.%M.%S")
    print("Creating a directory for the results: " + results_path)
    os.makedirs(results_path)

    print("Number of tracks: " + str(len(tracks)))
    print("Starting tests...")
    i = 0
    for track in tracks:
        i += 1
        print("\n#####################################")
        print("# " + str(i).rjust(2) + ". " + track)
        print("#####################################")
        config_race(quickrace_path, track)
        race_outputs = run_torcs(client_path, track)
        move_output_to_results_dir(race_outputs, track, results_path)
        create_plot_file(track, results_path, True)
        plot_file(track, results_path)
        create_plot_file(track, results_path, False)
        #plot_file(track, results_path, True)

    print("Done")


def config_race(xml_file_name, current_track_name):
    with open(xml_file_name, "r") as xml_file:
        xml_str = xml_file.read()

    dom = minidom.parseString(xml_str)

    sections = dom.getElementsByTagName("section")

    current_track_category = "road"
    #current_track_category = "oval"
    current_race_mode = "results only"
    current_race_laps = "300"

    for section in sections:
        name = section.getAttribute("name")
        if name == "Tracks":
            attstrs = section.getElementsByTagName("attstr")
            for attstr in attstrs:
                att_name = attstr.getAttribute("name")
                if att_name == "name":
                    attstr.setAttribute("val", current_track_name)
                elif att_name == "category":
                    attstr.setAttribute("val", current_track_category)
        elif name == "Quick Race":
            attstrs = section.getElementsByTagName("attstr")
            for attstr in attstrs:
                att_name = attstr.getAttribute("name")
                if att_name == "display mode":
                    attstr.setAttribute("val", current_race_mode)

            attnums = section.getElementsByTagName("attnum")
            for attnum in attnums:
                att_name = attnum.getAttribute("name")
                if att_name == "laps":
                    attnum.setAttribute("val", current_race_laps)

    with open(xml_file_name, "w") as xml_file:
        xml_file.write(dom.toxml())


def run_torcs(client, track):
    # Start server and continue with this script.
    print("Starting Torcs server.")
    log_file = open("run.log", "a");
    log_file.write("==========================\n" +
                   track +
                   "\n==========================\n")
    log_file.flush()
    Popen(["torcs", "-T", "-nofuel", "-nodamage", "-nolaptime"], stdout=log_file, stderr=log_file)
    #Popen(["torcs", "-T"])
    time.sleep(1)
    log_file.flush()
    log_file.write("\n")
    log_file.flush()
    log_file.close()

    # Start client and wait it returns.
    print("Starting driver client.")
    call([client, "track:" + track])
    #call([client, "port:3001", "id:ClassifierBot", "track:" + track, "stage:3"])

    print("Race finished.")
    race_outputs = ["./output/" + track + "." + extension for extension in [
                        "segments",
                        "signal",
                        "filteredSignal",
                        "flatSignal",
                        "cluster",
                        "lapTimes",
                        "rewards"]]
    return race_outputs


def move_output_to_results_dir(race_outputs, track, results_path):
    print("Moving race outputs to results directory")
    for output in race_outputs:
        shutil.move(output, results_path)


def create_plot_file(track, results_path, is_pdf):
    file_path = os.path.join(results_path, track)
    with open(file_path + ".plot", "w") as file:
        if is_pdf:
            #file.write('set terminal pngcairo size 1200,700\n')
            #file.write('#set terminal wxt\n')
            file.write('set terminal pdf solid font \'Helvetica,13\' size 10,3\n' +
                       'set output \'' + track + '.pdf\n')
        else:
            file.write('set terminal wxt\n')
            file.write('#set terminal pngcairo size 1200,700\n')
            file.write('#set terminal pdf solid font \'Helvetica,13\' size 10,3\n' +
                       '#set output \'' + track + '.pdf\n')
        file.write('set title "' + track + '"\n')
        file.write('#set key right bottom\n')
        file.write('#set xrange [0:2200]\n')
        file.write('set grid\n')
        file.write('set xlabel "Distância (m)" \n')
        file.write('set ylabel "Ângulo (rad)" \n')
        file.write('plot \\\n' +
                   '"' + track + '.filteredSignal" title "Sinal bruto" with lines lw 2 lt rgb "green", \\\n' +
                   '"' + track + '.signal" title "Filtro de média" with lines lw 1.5 lt rgb "blue", \\\n' +
                   '"' + track + '.flatSignal" title "Filtro de patamar" with lines lw 1.5 lt rgb "red" \n')


def plot_file(track, results_path, open_gnuplot=False):
    print("\nPlotting signal: " + track)
    original_path = os.getcwd()
    os.chdir(results_path)
    plot = track + ".plot"
    if (not open_gnuplot):
        print("Creating " + plot + ".pdf file")
        p = Popen(["gnuplot", plot])
        p.wait()
        #with open(plot + ".pdf", "w") as pdf_output:
        #    print("Creating " + plot + ".pdf file")
        #    p = Popen(["gnuplot", plot], stdout = pdf_output)
        #    p.wait()
    else:
        p = Popen(["gnuplot", "-p", plot])
        p.wait()
    os.chdir(original_path)


main()
