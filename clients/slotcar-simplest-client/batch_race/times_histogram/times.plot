#set terminal wxt
set terminal pdf solid font ',13' size 10,7 enhanced
set output 'times.pdf'
set grid
set yrange [0:130]

# Rotate x axis labels.
set xtics rotate out
set ytics rotate out

# Select histogram style.
set style data histogram
set style histogram clustered

# Bars style.
set style line 2 lc rgb 'black' lt 1 lw 1
set style fill solid border -1
#set boxwidth 0.7

# Labels
set key at graph 0.076, 0.9 horizontal samplen 1 spacing 8 width -1.75
set label 1 'Pistas' at graph 0.03, -0.07 centre rotate by 90
set ylabel 'Tempo (s)'
set label 3 'Autopia'   at graph 0.016, 0.9 left rotate by 90
set label 4 'MrRacer'   at graph 0.039, 0.9 left rotate by 90
set label 5 'RL Driver' at graph 0.062, 0.9 left rotate by 90

plot 'times.data' using 2               every ::1 title ' ' lc rgb '#E5E5E5', \
     ''           using 3               every ::1 title ' ' ls 2 fs pattern 2, \
     ''           using 4:xticlabels(1) every ::1 title ' ' ls 2 fs pattern 6
