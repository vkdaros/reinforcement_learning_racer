#!/usr/bin/python

import os
import sys

file_names = sys.argv[1:]
for file_name in file_names:
    file = open(file_name, "r")
    lines = file.readlines()
    file.close()

    file = open(os.path.splitext(file_name)[0] + ".clusterSegments", "w")
    for i in range(len(lines)):
        file.write("%s,cluster%d\n" %(lines[i][:-2], i%7))
    file.close()

