set terminal wxt size 350,262 enhanced font 'Verdana,10' persist;
#set xrange[0:400];
#set yrange[0:40];
#set samples 1000;

#plot (x >= 0) ? 1.9 * exp(0.008*x): 1/0, (x/60)**2, (x/55)**2, (x/50)**2, (x/45)**2, (x/40)**2
plot '../output/unknown.rewards' with linespoints ls 1,\
     '../output/unknown.lapTimes' with linespoints ls 2
