#!/usr/bin/python

import sys
import os

error_message = """
ERROR: no input file provided. At least one segment file must be provided.
    Usage: segments2all.py <FILE1> [FILE2 FILE3 ...]
"""

def get_file_names():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()
    return sys.argv[1:]


def write_header(file):
    file.write("@relation all\n\n")
    file.write("@attribute 'Length' numeric\n")
    file.write("@attribute 'Angle' numeric\n\n")
    file.write("@data\n")


def main():
    file_names = get_file_names()
    output_file = open("all.arff", "w")
    write_header(output_file)

    for name in file_names:
        output_file.write("\n% " + os.path.splitext(name)[0] + "\n")
        file = open(name, "r")
        output_file.write(file.read())
        file.close()

    output_file.close()


main()
