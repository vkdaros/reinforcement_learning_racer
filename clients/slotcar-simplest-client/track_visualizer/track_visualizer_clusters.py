#!/usr/bin/python2

import os
import sys
import string
import math
import pygame
from pygame.locals import *

import cairo
import rsvg

# CONSTANTS
SCREEN_SIZE = [1024, 750]
TRACK_SURFACE_SIZE = [2000, 2000]
INITIAL_POINT = (TRACK_SURFACE_SIZE[0] / 2, TRACK_SURFACE_SIZE[1] / 2)
TRACK_WIDTH = 13

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255,255,0)
MAGENTA = (255,0,255)
CYAN = (0,255,255)
ORANGE = (255, 165, 0)
WATER_GREEN = (0, 230, 75)
PURPLE = (165, 0, 255)
BROWN = (100, 100, 0)
DARK_PURPLE = (100, 0, 100)
BLUE_GRAY = (0, 100, 100)
PINK = (255, 210, 255)

FOREST_GREEN = (34, 139, 34)
YELLOW_GREEN = (153, 204, 50)
AQUAMARINE = (112, 219, 147)
ROYALL_BLUE1 = (72, 118, 255)
SLATE_BLUE = (106, 90, 205)
LIME_GREEN = (50, 205, 50)
DARK_ORCHID = (153, 50, 204)

SHORT_STRAIGHT = (110, 255, 255)
MEDIUM_STRAIGHT = (10, 110, 245)
LONG_STRAIGHT = (0, 10, 90)
HAIRPIN = (255, 10, 10)
ELBOW = (255, 130, 0)
SHORT_HARD = (185, 165, 10)
LONG_HARD = (245, 245, 10)
SHORT_MEDIUM = (150, 10, 190)
LONG_MEDIUM = (235, 190, 255)
SHORT_EASY = (5, 100, 5)
LONG_EASY = (10, 240, 10)

error_message = """
ERROR: no input file provided
    Usage: track_visualizer_cluster.py <FILE1> <FILE2> [SCALE_FACTOR] [WIDTH HEIGHT]
    FILE1: track file in which each line has two numbers distance and angle.
    FILE2: cluster file.
    SCALE_FACTOR: the greater the factor, the larger the zoom.
    WIDTH HEIGHT: screen size.
"""

help_message = """
INTERACTION:
       Arrow keys: move image;
         + - keys: to scale image;
            v key: to create a screenshot .png file;
            s key: to create a screenshot .svg file;
            g key: to toggle on/off track grid;
            c key: to toggle on/off clusters color;
            e key: to toggle on/off clusters edges;
            l key: to toggle on/off distance labels;
    ESC BACKSPACE: quit.
"""


def get_track_file_name():
    if len(sys.argv) < 3:
        print(error_message)
        sys.exit()

    return sys.argv[1]


def get_clusters_file_name():
    if len(sys.argv) < 3:
        print(error_message)
        sys.exit()

    return sys.argv[2]


def get_scale_factor():
    scale_factor = 1
    if len(sys.argv) >= 4:
        scale_factor = float(sys.argv[3])

    return scale_factor


def get_screen_size():
    width = SCREEN_SIZE[0]
    height = SCREEN_SIZE[1]
    if len(sys.argv) >= 6:
        width = int(sys.argv[4])
        height = int(sys.argv[5])

    return [width, height]


def get_data(file_name):
    file = open(file_name, "r")
    data = [[float(val) for val in line.split()] for line in file.readlines()]
    file.close()

    # data = [[dist, angle], [dist, angle], ... ]
    return data


def get_clusters(file_name):
    file = open(file_name, "r")
    clusters = []
    for line in file.readlines():
        # Remove new lines and make a list: [distance, angle, cluster]
        line = line.strip().split(',')

        if len(line) < 3:
            continue

        #clusters.append([float(line[0]), float(line[1]),\
        #               int(line[2][len("cluster"):]),float(line[3]),\
        #               float(line[4])])

        clusters.append([float(line[0]), float(line[1]),\
                        int(line[2][len("cluster"):])])

    file.close()

    # data = [[dist, angle, cluster], [dist, angle, cluster], ... ]
    return clusters


def recalculate_clusters(clusters, data):
    accumulated_distance = 0
    accumulated_angle = 0

    i = 0
    c = 0

    while c < len(clusters) and i <= len(data):
        accumulated_distance += data[i][0]
        accumulated_angle += data[i][1]
        #print ("%4d: a: %10f; accA: %10f; d: %10f; accD: %10f" %(i, data[i][1], accumulated_angle, data[i][0], accumulated_distance))

        if accumulated_distance >= clusters[c][0]:
            if c == 0:
                length = clusters[c][0]
            else:
                length = clusters[c][0] - clusters[c - 1][0]

            if clusters[c][1] == 0.0:
                radius = -1
            else:
                radius = length / abs(accumulated_angle)

            #print("----> f: %d; i: %d; A: %f, D: %f" %(c, i, accumulated_angle, accumulated_distance))
            clusters[c][2] = classify(accumulated_angle, radius, length)

            c += 1
            accumulated_angle = 0

        i += 1


def classify(angle, radius, length):
    angle = abs(angle)
    #print("angle: %f; radius: %f; length: %f" % (angle, radius, length))

    # Straight
    if (radius == -1):
        if length <= 50:
            return 0
        if length <= 150:
            return 1
        return 2
    else:
        if radius <= 60:
            if angle >= 2.094:
                return 3    # Hairpin
            if angle >= 0.873:
                return 4    # Elbow
        if radius <= 127:
            if angle <= 0.873:
                return 5    # Short hard
            return 6        # Long hard
        if radius <= 185:
            if angle <= 0.873:
                return 7    # Short medium
            return 8        # Long medium
        else:
            if angle <= 0.873:
                return 9    # Short easy
            return 10       # Long easy


def get_points(data, scale_factor):
    last_point = INITIAL_POINT
    angle = math.pi / 2 # First segment will point upward.

    points = [last_point]
    for d in data:
        angle += d[1]
        distance = d[0]
        x = math.sin(angle) * distance / (1 / scale_factor)
        y = math.cos(angle) * distance / (1 / scale_factor)
        new_point = (last_point[0] + x, last_point[1] + y)
        points.append(new_point)
        last_point = new_point

    return points


# Calculate right and left edges of the track anc returns these two lists.
def get_edge_points(data, center_points, scale_factor):
    right_points = []
    left_points = []
    angle = math.pi / 2 # First segment will point upward.

    for i in range(len(data)):
        gama = angle - math.pi / 2 + data[i][1] / 2

        x = math.sin(gama) * TRACK_WIDTH / (1 / scale_factor)
        y = math.cos(gama) * TRACK_WIDTH / (1 / scale_factor)

        new_right = (center_points[i][0] + x, center_points[i][1] + y)
        new_left = (center_points[i][0] - x, center_points[i][1] - y)

        right_points.append(new_right)
        left_points.append(new_left)

        angle += data[i][1]

    i += 1
    last_right = (center_points[i][0] + x, center_points[i][1] + y)
    last_left = (center_points[i][0] - x, center_points[i][1] - y)

    right_points.append(last_right)
    left_points.append(last_left)

    return right_points, left_points


def draw_crosshair(surface):
    width = surface.get_width()
    height = surface.get_height()

    size = 0.05 # Percentage with respect to half of the screen.

    up = height * (1 - size) / 2
    down = height * (1 + size) / 2
    left = width * (1 - size) / 2
    right = width * (1 + size) / 2

    pygame.draw.line(surface, GREEN, (left, up), (right, down))
    pygame.draw.line(surface, GREEN, (left, down), (right, up))


def draw_points(clear, surface, points, color):
    if clear:
        surface.fill((200, 200, 200))

    last_point = points[0]
    for point in points[1:]:
        if math.isnan(point[0]) or math.isnan(point[1]):
            print("NAN found!")
            continue
        pygame.draw.line(surface, color, last_point, point)
        last_point = point


def draw_grid(clear, surface, data, clusters, right_points, left_points, color):
    if clear:
        surface.fill((200, 200, 200))

    accumulated_distance = data[0][0]
    i = 1

    pygame.draw.line(surface, color, right_points[i], left_points[i])
    while i < len(data):
        accumulated_distance += data[i][0]
        i += 1
        pygame.draw.line(surface, color, right_points[i], left_points[i])


def draw_edges(clear, surface, data, clusters, right_points, left_points,
               cluster_edge_color):
    if clear:
        surface.fill((200, 200, 200))

    accumulated_distance = data[0][0]
    i = 1
    c = 0

    while i < len(data):
        accumulated_distance += data[i][0]
        i += 1

        if c < len(clusters) and accumulated_distance >= clusters[c][0]:
            pygame.draw.line(surface, cluster_edge_color, right_points[i],
                             left_points[i], 3)
            c += 1


def draw_lables(clear, surface, data, clusters, right_points, left_points):
    if clear:
        surface.fill((200, 200, 200))

    accumulated_distance = data[0][0]
    accumulated_angle = data[0][0]
    i = 1
    c = 0
    labels = []

    while i < len(data):
        accumulated_distance += data[i][0]
        accumulated_angle += data[i][1]

        i += 1

        if c < len(clusters) and accumulated_distance >= clusters[c][0]:
            if c == 0:
                length = clusters[c][0]
            else:
                length = clusters[c][0] - clusters[c - 1][0]

            if clusters[c][1] == 0.0:
                radius = -1
            else:
                radius = length / abs(accumulated_angle)

            degrees = accumulated_angle * 180 / math.pi

            labels.append((["D: %d; L: %d" % (accumulated_distance, length),
                            "R: %.1f" % radius,
                            "A: %9f (%.2f*)" % (accumulated_angle, degrees)],
                           right_points[i], left_points[i]))
            c += 1
            accumulated_angle = 0

    # Draw distanceFromStart at the end of each cluster.
    verdana = pygame.font.Font(pygame.font.match_font("Verdana"), 14)
    alt = False
    for label in labels:
        a = label[1]
        b = label[2]
        if alt:
            c = (1.5 * b[0] - 0.5 * a[0], 1.5 * b[1] - 0.5 * a[1])
        else:
            c = (2.2 * b[0] - 1.2 * a[0], 2.2 * b[1] - 1.2 * a[1])
        alt = not alt
        pygame.draw.line(surface, WHITE, b, c)
        label_surface_d = verdana.render(label[0][0], True, WHITE, (100, 100, 100))
        label_surface_l = verdana.render(label[0][1], True, WHITE, (100, 100, 100))
        label_surface_a = verdana.render(label[0][2], True, WHITE, (100, 100, 100))
        surface.blit(label_surface_d, c)
        surface.blit(label_surface_l, (c[0], c[1] + 14))
        surface.blit(label_surface_a, (c[0], c[1] + 28))


def draw_clusters(clear, surface, data, clusters, right_points, left_points):
    if clear:
        surface.fill((200, 200, 200))

    colors = [SHORT_STRAIGHT, MEDIUM_STRAIGHT, LONG_STRAIGHT, HAIRPIN, ELBOW,
              SHORT_HARD, LONG_HARD, SHORT_MEDIUM, LONG_MEDIUM, SHORT_EASY,
              LONG_EASY]

    accumulated_distance = 0
    i = 0
    c = 0

    while i < len(data):
        # Handle the special case when the last piece of track is part of the
        # first cluster.
        j = c
        if j >= len(clusters):
            j = 0

        left_rev = left_points[i: i + 2]
        left_rev.reverse()
        pygame.draw.polygon(surface, colors[abs(clusters[j][2])], \
                            right_points[i: i + 2] + left_rev)

        # If current segment is the last of a cluster, then next one will use a
        # different color.
        accumulated_distance += data[i][0]
        if c < len(clusters) and accumulated_distance >= clusters[c][0]:
            c += 1

        i += 1


def clear(surface):
    if clear:
        surface.fill((200, 200, 200))


def draw_all(surface, data, clusters, points, right_points, left_points, \
             show_clusters, show_grid, show_edges, show_labels):
    clear(surface)
    cluster_edge_color = RED
    grid_color = BLACK
    if show_clusters:
        draw_clusters(False, surface, data, clusters, right_points, left_points)
        cluster_edge_color = BLACK
        grid_color = WHITE

    draw_points(False, surface, points, grid_color)
    draw_points(False, surface, right_points, BLACK)
    draw_points(False, surface, left_points, BLACK)

    if show_grid:
        draw_grid(False, surface, data, clusters, right_points, left_points,
                  grid_color)
    if show_edges:
        draw_edges(False, surface, data, clusters, right_points, left_points,
                   cluster_edge_color)
    if show_labels:
        draw_lables(False, surface, data, clusters, right_points, left_points)


def print_clusters_edges(data, clusters):
    #file = open("angle_radius", "a")
    accumulated_distance = 0
    accumulated_angle = 0

    i = 0
    c = 0

    print("Cluster edges:")
    print("================================================================================")
    print("| #  |   end    | len  |   angle   |      acc_angle       |    radius     |  C |")
    print("--------------------------------------------------------------------------------")
    while c < len(clusters) and i < len(data):
        accumulated_distance += data[i][0]
        accumulated_angle += data[i][1]

        if accumulated_distance >= clusters[c][0]:
            if c == 0:
                length = clusters[c][0]
            else:
                length = clusters[c][0] - clusters[c - 1][0]

            if clusters[c][1] == 0.0:
                radius = -1
            else:
                radius = length / abs(accumulated_angle)

            degrees = accumulated_angle * 180 / math.pi

            print("| " + str(c).rjust(2) + " | %8.2f" % accumulated_distance +
                  " | %4d" % length +
                  " | %9f" % clusters[c][1] +
                  " | %9f (%7.2f*)" % (accumulated_angle, degrees) +
                  " | %12.6f " % radius +
                  " | %2d |" %clusters[c][2])
            #file.write("%f %f\n" % (accumulated_angle, radius))
            c += 1
            accumulated_angle = 0

        i += 1
    print("================================================================================")
    #file.close();


def handle_events(offset):
    running = True
    scale = 1
    step = 10    # The amount of pixels offset will move.
    factor = 0.1 # Proportion by which scale will increase/decrease.
    segmentation_changed = False
    should_export_screenshot = False
    should_export_svg = False
    toggle_clusters = False
    toggle_grid = False
    toggle_edge = False
    toggle_labels = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == KEYUP:
            if event.key == K_ESCAPE or event.key == K_BACKSPACE or \
               event.key == K_q:

                running = False

        if event.type == KEYDOWN:
            if event.key == K_RIGHT:
                offset[0] -= step
            elif event.key == K_LEFT:
                offset[0] += step
            if event.key == K_UP:
                offset[1] += step
            elif event.key == K_DOWN:
                offset[1] -= step
            if event.key == K_v:
                should_export_screenshot = True
            if event.key == K_s:
                should_export_svg = True
            if event.key == K_c:
                toggle_clusters = True
            if event.key == K_g:
                toggle_grid = True
            if event.key == K_e:
                toggle_edge = True
            if event.key == K_l:
                toggle_labels = True
            if event.unicode == "+":
                scale += factor
                offset[0] -= (SCREEN_SIZE[0] / 2 - offset[0]) * factor
                offset[1] -= (SCREEN_SIZE[1] / 2 - offset[1]) * factor
            elif event.key == K_MINUS or event.key == K_KP_MINUS:
                scale -= factor
                offset[0] += (SCREEN_SIZE[0] / 2 - offset[0]) * factor
                offset[1] += (SCREEN_SIZE[1] / 2 - offset[1]) * factor

    return running, scale, offset, segmentation_changed, \
           should_export_screenshot, should_export_svg, toggle_clusters, \
           toggle_grid, toggle_edge, toggle_labels


def scale_surface(surface, scale):
    new_size = [int(x * scale) for x in surface.get_size()]
    surface = pygame.transform.scale(surface, new_size)
    TRACK_SURFACE_SIZE[0] = new_size[0]
    TRACK_SURFACE_SIZE[1] = new_size[1]

    return surface


def scale_points(points, scale):
    scaled_points = [[(val * scale) for val in tuple] for tuple in points]

    return scaled_points


def export_screenshot(surface):
    file_name = get_clusters_file_name()
    file_name = os.path.splitext(file_name)[0] + ".png"

    print("Exporting screenshot to file: " + file_name)
    pygame.image.save(surface, file_name)
    print("Exportation finished.\n")


# ==================== SVG ====================
def export_svg(points, right_points, left_points, data, clusters):
    file_name = get_clusters_file_name()
    file_name = os.path.splitext(file_name)[0] + ".svg"

    offset_x, offset_y, track_width, track_height = get_dimesions(points)
    margin = 17
    scale = get_scale(track_width, track_height, margin)

    print("Exporting screenshot to file: " + file_name)
    svg_file = file(file_name, 'w')
    surface = cairo.SVGSurface(svg_file, scale * (track_width + 2 * margin),
                                         scale * (track_height + 2 * margin))

    x = margin - offset_x
    y = margin - offset_y
    #draw_svg_points(surface, points, scale, x, y, RED)
    draw_svg_cluters(surface, scale, x, y, data, clusters, right_points,
                  left_points)
    #draw_svg_grid(surface, scale, x, y, data, clusters, right_points,
    #              left_points, BLACK)
    draw_svg_edges(surface, scale, x, y, data, clusters, right_points,
                  left_points, BLACK)
    draw_svg_points(surface, left_points, scale, x, y, BLACK)
    draw_svg_points(surface, right_points, scale, x, y, BLACK)
    surface.finish()
    print("Exportation finished.\n")


def get_dimesions(points):
    upper_x = points[0][0]
    lower_x = upper_x
    upper_y = points[0][1]
    lower_y = upper_y

    for point in points[1:]:
        if math.isnan(point[0]) or math.isnan(point[1]):
            print("NAN found!")
            continue
        upper_x = max(point[0], upper_x)
        lower_x = min(point[0], lower_x)
        upper_y = max(point[1], upper_y)
        lower_y = min(point[1], lower_y)

    track_width = upper_x - lower_x
    track_height = upper_y - lower_y

    return lower_x, lower_y, track_width, track_height


def get_scale(track_width, track_height, margin):
    width = track_width + 2 * margin
    height = track_height + 2 * margin
    scale_x = SCREEN_SIZE[0] / width
    scale_y = SCREEN_SIZE[1] / height
    return min(scale_x, scale_y)


def draw_svg_points(surface, points, scale_factor, offset_x, offset_y, color):
    context = cairo.Context(surface)
    context.scale(scale_factor, scale_factor)
    context.translate(offset_x, offset_y)
    context.move_to(points[0][0], points[0][1])

    for point in points[1:]:
        if math.isnan(point[0]) or math.isnan(point[1]):
            print("NAN found!")
            continue
        x = point[0]
        y = point[1]
        context.line_to(x, y)

    context.set_source_rgb(color[0], color[1], color[2])
    context.set_line_width(0.1)
    context.stroke()


def draw_svg_grid(surface, scale_factor, offset_x, offset_y, data, clusters,
                  right_points, left_points, color):
    context = cairo.Context(surface)
    context.scale(scale_factor, scale_factor)
    context.translate(offset_x, offset_y)

    accumulated_distance = data[0][0]
    i = 1

    context.move_to(right_points[i][0], right_points[i][1])
    context.line_to(left_points[i][0], left_points[i][1])
    while i < len(data):
        accumulated_distance += data[i][0]
        i += 1
        context.move_to(right_points[i][0], right_points[i][1])
        context.line_to(left_points[i][0], left_points[i][1])

    context.set_source_rgb(color[0], color[1], color[2])
    context.set_line_width(0.1)
    context.stroke()


def draw_svg_edges(surface, scale_factor, offset_x, offset_y, data, clusters,
                  right_points, left_points, color):
    context = cairo.Context(surface)
    context.scale(scale_factor, scale_factor)
    context.translate(offset_x, offset_y)

    accumulated_distance = data[0][0]
    i = 1
    c = 0

    while i < len(data):
        accumulated_distance += data[i][0]
        i += 1

        if c < len(clusters) and accumulated_distance >= clusters[c][0]:
            context.move_to(right_points[i][0], right_points[i][1])
            context.line_to(left_points[i][0], left_points[i][1])
            c += 1

    context.set_source_rgb(color[0], color[1], color[2])
    context.set_line_width(3)
    context.stroke()


def draw_svg_cluters(surface, scale_factor, offset_x, offset_y, data, clusters,
                  right_points, left_points):
    colors = [SHORT_STRAIGHT, MEDIUM_STRAIGHT, LONG_STRAIGHT, HAIRPIN, ELBOW,
              SHORT_HARD, LONG_HARD, SHORT_MEDIUM, LONG_MEDIUM, SHORT_EASY,
              LONG_EASY]

    context = cairo.Context(surface)
    context.scale(scale_factor, scale_factor)
    context.translate(offset_x, offset_y)

    accumulated_distance = 0
    i = 0
    c = 0

    while i < len(data):
        j = c
        if j >= len(clusters):
            j = 0

        context.move_to(right_points[i][0], right_points[i][1])
        context.line_to(right_points[i + 1][0], right_points[i + 1][1])
        context.line_to(left_points[i + 1][0], left_points[i + 1][1])
        context.line_to(left_points[i][0], left_points[i][1])
        context.close_path()

        color = colors[abs(clusters[j][2])]
        context.set_source_rgb(color[0] / 255.0,
                               color[1] / 255.0,
                               color[2] / 255.0)
        context.fill()

        context.move_to(right_points[i + 1][0], right_points[i + 1][1])
        context.line_to(left_points[i + 1][0], left_points[i + 1][1])
        context.move_to(left_points[i][0], left_points[i][1])
        context.line_to(right_points[i][0], right_points[i][1])

        context.set_line_width(2)
        context.stroke()

        accumulated_distance += data[i][0]
        if c < len(clusters) and accumulated_distance >= clusters[c][0]:
            c += 1

        i += 1
# ============================================


def main():
    global SCREEN_SIZE

    track_file_name = get_track_file_name()
    clusters_file_name = get_clusters_file_name()

    data = get_data(track_file_name)
    clusters = get_clusters(clusters_file_name)
    #recalculate_clusters(clusters, data)

    scale_factor = get_scale_factor()
    SCREEN_SIZE = get_screen_size()
    points = get_points(data, scale_factor)
    right_points, left_points = get_edge_points(data, points, scale_factor)
    show_clusters = True
    show_grid = False
    show_edges = True
    show_labels = False

    print(help_message)
    print_clusters_edges(data, clusters)

    pygame.init()
    pygame.key.set_repeat(100, 10)

    window = pygame.display.set_mode(SCREEN_SIZE)
    track_surface = pygame.Surface(TRACK_SURFACE_SIZE, pygame.SRCALPHA, 32)

    draw_all(track_surface, data, clusters, points, right_points, left_points, \
            show_clusters, show_grid, show_edges, show_labels)

    offset = [-INITIAL_POINT[0] + SCREEN_SIZE[0] / 2, \
              -INITIAL_POINT[1] + SCREEN_SIZE[1] / 2]
    running = True
    while running:
        running, scale, offset, segmentation_changed, should_export_screenshot,\
        should_export_svg, toggle_clusters, toggle_grid, toggle_edge, \
        toggle_labels = handle_events(offset)

        window.fill(BLACK)
        #draw_crosshair(window)

        redraw = False
        if scale != 1:
            track_surface = scale_surface(track_surface, scale)
            points = scale_points(points, scale)
            right_points = scale_points(right_points, scale)
            left_points = scale_points(left_points, scale)
            redraw = True

        if segmentation_changed:
            redraw = True

        if toggle_clusters:
            show_clusters = not show_clusters
            redraw = True

        if toggle_grid:
            show_grid = not show_grid
            redraw = True

        if toggle_edge:
            show_edges = not show_edges
            redraw = True

        if toggle_labels:
            show_labels = not show_labels
            redraw = True

        if redraw:
            draw_all(track_surface, data, clusters, points, right_points, \
                     left_points, show_clusters, show_grid, show_edges, show_labels)

        window.blit(track_surface, offset)

        if should_export_screenshot:
            export_screenshot(window)

        if should_export_svg:
            export_svg(points, right_points, left_points, data, clusters)

        pygame.display.update()

    pygame.quit()


main()
