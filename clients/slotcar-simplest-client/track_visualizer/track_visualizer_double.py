#!/usr/bin/python2

import sys
import string
import math
import pygame
from pygame.locals import *

# CONSTANTS
SCREEN_SIZE = [1024, 768]
TRACK_SURFACE_SIZE = [1000, 1000]
INITIAL_POINT = (TRACK_SURFACE_SIZE[0] / 2, TRACK_SURFACE_SIZE[1] / 2)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

error_message = """
ERROR: no input file provided
    Usage: track_visualizer.py <FILE> [SCALE_FACTOR]
    FILE: track file in which each line has two numbers distance and angle
    SCALE_FACTOR: the greater the factor, the larger the zoom.
"""

help_message = """
INTERACTION:
    Arrow keys: move
    + - keys: scale
    ESC BACKSPACE: quit
"""

def get_file_name():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()

    return sys.argv[1]


def get_scale_factor():
    scale_factor = 1
    if len(sys.argv) == 3:
        scale_factor = float(sys.argv[2])

    return scale_factor


def get_data(file_name):
    file = open(file_name, "r")
    data = [[float(val) for val in line.split()] for line in file.readlines()]
    file.close()

    data1 = []
    data2 = []
    for d in data:
        data1.append([d[0], d[1]])
        data2.append([d[2], d[3]])

    # data = [[dist, angle], [dist, angle], ... ]
    return data1, data2


def get_points(data, scale_factor):
    last_point = INITIAL_POINT
    angle = math.pi # First segment will point upward.

    points = [last_point]
    for d in data[1:]:
        angle += d[1]
        distance = d[0]
        x = math.sin(angle) * distance / (1 / scale_factor)
        y = math.cos(angle) * distance / (1 / scale_factor)
        new_point = (last_point[0] + x, last_point[1] + y)
        points.append(new_point)
        last_point = new_point

    return points


def draw_crosshair(surface):
    width = surface.get_width()
    height = surface.get_height()

    size = 0.05 # Percentage with respect to half of the screen.

    up = height * (1 - size) / 2
    down = height * (1 + size) / 2
    left = width * (1 - size) / 2
    right = width * (1 + size) / 2

    pygame.draw.line(surface, GREEN, (left, up), (right, down))
    pygame.draw.line(surface, GREEN, (left, down), (right, up))


def draw_points(clear, surface, points, color):
    if clear:
        surface.fill((255, 255, 255, 20))

    last_point = points[0]
    for point in points[1:]:
        if math.isnan(point[0]) or math.isnan(point[1]):
            print("NAN found!")
            continue
        pygame.draw.line(surface, color, last_point, point)
        last_point = point


def handle_events(offset):
    running = True
    scale = 1
    step = 10    # The amount of pixels offset will move.
    factor = 0.1 # Proportion by which scale will increase/decrease.

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == KEYUP:
            if event.key == K_ESCAPE or event.key == K_BACKSPACE:
                running = False

        if event.type == KEYDOWN:
            if event.key == K_RIGHT:
                offset[0] -= step
            elif event.key == K_LEFT:
                offset[0] += step
            elif event.key == K_UP:
                offset[1] += step
            elif event.key == K_DOWN:
                offset[1] -= step
            elif event.unicode == "+":
                scale += factor
                offset[0] -= (SCREEN_SIZE[0] / 2 - offset[0]) * factor
                offset[1] -= (SCREEN_SIZE[1] / 2 - offset[1]) * factor
            elif event.key == K_MINUS or event.key == K_KP_MINUS:
                scale -= factor
                offset[0] += (SCREEN_SIZE[0] / 2 - offset[0]) * factor
                offset[1] += (SCREEN_SIZE[1] / 2 - offset[1]) * factor

    return running, scale, offset


def apply_scale(surface, points1, points2, scale):
    scaled_points1 = [[(val * scale) for val in tuple] for tuple in points1]
    scaled_points2 = [[(val * scale) for val in tuple] for tuple in points2]
    new_size = [int(x * scale) for x in surface.get_size()]
    surface = pygame.transform.scale(surface, new_size)
    TRACK_SURFACE_SIZE[0] = new_size[0]
    TRACK_SURFACE_SIZE[1] = new_size[1]

    return surface, scaled_points1, scaled_points2


def main():
    file_name = get_file_name()
    scale_factor = get_scale_factor()
    data1, data2 = get_data(file_name)
    points1 = get_points(data1, scale_factor)
    points2 = get_points(data2, scale_factor)

    pygame.init()
    pygame.key.set_repeat(100, 10)

    window = pygame.display.set_mode(SCREEN_SIZE)
    track_surface = pygame.Surface(TRACK_SURFACE_SIZE, pygame.SRCALPHA, 32)

    draw_crosshair(window)
    draw_points(True, track_surface, points1, WHITE)
    draw_points(False, track_surface, points2, RED)

    offset = [-INITIAL_POINT[0] + SCREEN_SIZE[0] / 2, \
              -INITIAL_POINT[1] + SCREEN_SIZE[1] / 2]
    running = True
    while running:
        running, scale, offset = handle_events(offset)
        window.fill(BLACK)
        draw_crosshair(window)

        if scale != 1:
            track_surface, points1, points2 = apply_scale(track_surface, points1, points2, scale)
            draw_points(True, track_surface, points1, WHITE)
            draw_points(False, track_surface, points2, RED)

        window.blit(track_surface, offset)

        pygame.display.update()

    pygame.quit()


main()
