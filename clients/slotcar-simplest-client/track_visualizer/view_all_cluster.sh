#!/bin/bash

if [[ "$1" = "" ]]
then
    echo "USAGE:"
    echo "    view_all_cluster.sh <PATH> [-s]"
    echo ""
    echo "ARGUMENTS:"
    echo "    PATH: Path to results directory"
    echo "      -s: When this argument is passed, a scale factor will be asked for each track visualization."
    echo ""
    exit
fi

for f in `ls $1 | grep segments`
do
    echo "Opening file: $1${f%.*}"
    echo "Continue? [Y/n]"
    read -s -n 1 key
    if [ "$key" = "n" -o "$key" = "N" -o "$key" = q ]
    then
        break
    fi

    if [ "$2" = "-s" ]
    then
        echo -n "Scale factor [1]: "
        read scale
    fi
    ./track_visualizer_clusters.py $1/"${f%.*}".segments $1/"${f%.*}".cluster $scale
done
