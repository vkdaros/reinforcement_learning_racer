/***************************************************************************

    file                 : VKDumb.cpp
    created              : Sun Sep 23 13:22:31 BRT 2012
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "VKDumb.h"
#include <stdio.h>

void VKDumb::init(float *angles) {
	// set angles from -90 to 90 degrees with 10 degree step.
    for (int i = 0; i < TRACK_SENSORS_NUM; i++) {
        angles[i] = -90 + i * 10;
    }
}

void VKDumb::onShutdown() {
}

void VKDumb::onRestart() {
}

CarControl VKDumb::wDrive(CarState cs) {
    float accel;
    float brake;
    int gear;
    float steer;
    float clutch;

    accel = getAcceleration(cs);
    brake = getBraking(cs);
    gear = selectGear(cs);
    steer = getSteer(cs);
    clutch = 0;

    CarControl cc(accel, brake, gear, steer, clutch);
    return cc;
}

int VKDumb::selectGear(CarState cs) {
    int gear = cs.getGear();

    if (gear == 0) {
        gear = 1;
    } else if (cs.getRpm() >= 9500) {
        gear++;
    } else if ((gear == 2 && cs.getRpm() <= 5000) ||
               (gear > 2 && cs.getRpm() <= 6000)) {
        gear--;
    }

    return gear;
}

float VKDumb::getAcceleration(CarState cs) {
    float frontDistance = cs.getTrack(9);
    float speedX = cs.getSpeedX();
    float accel;
    
    if (frontDistance < 10 && speedX >= 40) {
        accel = 0;
    } else if (frontDistance < 50 && speedX >= 100) {
        accel = 0.5;
    } else if (frontDistance < 75 && speedX >= 150) {
        accel = 0.75;
    } else if (frontDistance < 50) {
        accel = 0.3;
    } else {
        accel = 1;
    }

    return accel;
}

float VKDumb::getBraking(CarState cs) {
    float frontDistance = cs.getTrack(9);
    float speedX = cs.getSpeedX();
    float brake;
    
    if (frontDistance < 10 && speedX >= 25) {
        brake = 1;
    } else if (frontDistance < 50 && speedX >= 100) {
        brake = 0.4;
    } else if (frontDistance < 75 && speedX >= 150) {
        brake = 0.6;
    } else if (frontDistance < 100 && speedX >= 200) {
        brake = 0.9;
    } else {
        brake = 0;
    }

    return brake;
}

float VKDumb::getSteer(CarState cs) {
    float targetAngle;
    float steer;

    if (cs.getTrack(9) < 100) {
        // Search for the sensor which has the highest distance value.
        int fartherSensor = 0;
        for (int i = 1; i < TRACK_SENSORS_NUM; i++) {
            if (cs.getTrack(i) > cs.getTrack(fartherSensor)) {
                fartherSensor = i;
            }
        }

        // Steers in the direction of the farther sensor.
        targetAngle = -90 + fartherSensor * 10;
        targetAngle *= PI / 180;
        steer = targetAngle / -0.785398;
    } else {
        // Keep on the midle of the track.
        targetAngle = (cs.getAngle() - cs.getTrackPos() * 0.5);
        steer = targetAngle / 0.785398; // steerLock magic number
    }

    return steer;
}
