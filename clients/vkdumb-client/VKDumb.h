/***************************************************************************

    file                 : VKDumb.h
    created              : Sun Sep 23 13:22:31 BRT 2012
    copyright            : (C) 2002 Vinicius K. Daros

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VKDUMB_H_
#define VKDUMB_H_

#include "BaseDriver.h"
#include "CarState.h"
#include "CarControl.h"
#include "WrapperBaseDriver.h"

#define PI 3.14159265359

using namespace std;

class VKDumb : public WrapperBaseDriver {
  public:
	// Constructor.
	VKDumb(){};

	// Initialization of the desired angles for the rangefinders.
	virtual void init(float *angles);

	// Called at the end of the race, before being unloaded.
	virtual void onShutdown();

	// Called when the race is restarted.
	virtual void onRestart();

    // Change the effectors values in order to drive the car.
    virtual CarControl wDrive(CarState cs);

  private:
    // Select to gear up or down according to RPM.
    int selectGear(CarState cs);

    // Returns acceleartion based on the distance to the next bend.
    float getAcceleration(CarState cs);

    // Returns braking based on the distance to the next bend.
    float getBraking(CarState cs);

    // Returns braking based on the distance to the next bend.
    float getSteer(CarState cs);
};
#endif /* VKDUMB_H_ */
