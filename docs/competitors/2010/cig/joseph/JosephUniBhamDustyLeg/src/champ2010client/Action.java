package champ2010client;

/**
 *
 * @author j
 * Class modified from the original client so that we only ever make one Action object instead of many
 * and instead just alter it's variables.
 * The difference is that I create one action and rely on the user to update these values
 * otherwise they stay unchanged so I rely on the user to reset/update the values
 */
public class Action {

        public double accelerate;
    public double brake;
    public double clutch;
    public int gear;
    public double steering;
    public boolean restartRace;
    public int focus;

    // constructor
    public Action(){
    accelerate = 0; // 0..1
    brake = 0; // 0..1
    clutch=0; // 0..1
    gear = 0; // -1..6
    steering = 0;  // -1..1
    restartRace = false;
    focus = 360;//ML Desired focus angle in degrees [-90; 90], set to 360 if no focus reading is desired.
    }

/*    Reset default values. I do this so only one Action object need ever be made. When we have a single
 *     pointer e.g. Action action, and we say action = new Action(); then this allocates an object somewhere in memory.
 *     when we say again action = new Action(); this allocates another object somewhere in memory. Since the old previous
 *     Action is unreferenced is no longer referenced it will eventually "destroyed" by garbage collection....
 *     Though the code looks neater and the mechanism is slightly more user-friendly when creating new Objects,
 *    concerning efficiency it is not good. Since TORCS will be creating an Action every 30ms and the warmup lasts
 *    30 minutes. Then we can say we must create approx 30 Actions per second. The warmup will last 30 minutes which is
 *    1800 seconds. So in total 1800*30 = 54,000 seperate objects in memory! The same would go for the sensors
 * which are considerably larger objects.
 * Therefore I simply reset the values in each action and in fact the
 *  this would be quicker then creating a new object without even considering garbage collection. */


    public void resetValues()
    {
    accelerate = 0;
    brake = 0; 
    clutch=0; 
    gear = 0; 
    steering = 0;  
    restartRace = false;
    focus = 360;
    }


    @Override
    public String toString () {
        limitValues ();
        return "(accel " + accelerate  + ") " +
               "(brake " + brake  + ") " +
               "(clutch " + clutch  + ") " +
               "(gear " + gear + ") " +
               "(steer " + steering + ") " +
               "(meta " + (restartRace ? 1 : 0)
               + ") " + "(focus " + focus
               + ")";
    }



    public void limitValues () {
        accelerate = Math.max (0, Math.min (1, accelerate));
        brake = Math.max (0, Math.min (1, brake));
        clutch = Math.max(0, Math.min(1, clutch));
        steering = Math.max (-1, Math.min (1, steering));
        gear = Math.max (-1, Math.min (6, gear));

    }

}
