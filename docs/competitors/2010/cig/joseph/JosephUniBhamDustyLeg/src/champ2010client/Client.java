package champ2010client;

import java.io.IOException;
import java.net.SocketException;
import java.util.StringTokenizer;

/**
 *
 * @author j
 * This is the main class to be run to start up the client.
 * The arguments are described below.
 */
/**
 * @param args
 *            is used to define all the options of the client.
 *            <port:N> is used to specify the port for the connection (default is 3001)
 *            <host:ADDRESS> is used to specify the address of the host where the server is running (default is localhost)
 *            <id:ClientID> is used to specify the ID of the client sent to the server (default is championship2009)
 *            <verbose:on> is used to set verbose mode on (default is off)
 *            <maxEpisodes:N> is used to set the number of episodes (default is 1)
 *            <maxSteps:N> is used to set the max number of steps for each episode (0 is default value, that means unlimited number of steps)
 *            <stage:N> is used to set the current stage: 0 is WARMUP, 1 is QUALIFYING, 2 is RACE, others value means UNKNOWN (default is UNKNOWN)
 *            <trackName:name> is used to set the name of current track
 */
public class Client {

    private String[] arguments;
    private Communicator communicator;

    private Driver driver;

    // class variables initialized to default values
    private int UDP_TIMEOUT = 10000;
    private int port = 3001;
    private String host = "localhost";
    private String clientId = "championship2010";
    private boolean verbose = false;
    private int maxEpisodes = 1;
    private int maxSteps = 0;
    private int stage = 3; // 3 means unknown
    private String trackName = "unknown";

    

    // action and sensor
    Action action = new Action();
    Sensor sensor = new Sensor();

    // recieved message
    String receivedMessage;

    // A bot can be put through many episodes
    long currentEpisode = 0;

    // and each episode is a guven number of steps
    long currentStep = 0;
    private boolean shutdownOccured = false;

    public static void main(String[] args) throws SocketException, IOException {
        // create a client object
        Client client = new Client(args);
    }

    // Constructor
    public Client(String[] args) throws SocketException, IOException {


        // get the command line arguments out of a static context
        arguments = args;

        // parse the parameters from these arguments
        parseParameters();

        /* open the commnunicaiton between the client and server.
         * we pass verbose, in case we want to print output.. */
        communicator = new Communicator(host, port, verbose);


        initializeDriver();



    }

    private void initializeDriver() throws IOException {
        // create the appropriate driver
        if (stage == 0) // 0 is the warmup
        {

            driver = new champ2010client.warmup.WarmupDriver(action, sensor, trackName);



            warmupLoop();

        } else if (stage == 1 || stage == 3) // competitive stages
        {

            driver = new champ2010client.competitive.QualifyingDriver(action, sensor, trackName);
    

            normalLoop();
        } 
        else // stage unknown (3)
        {


            System.out.println("No driver available for unknown stage (3)");


        }


    }

    private void normalLoop() throws IOException {


        do {

            /* this do-while loop keeps trying to connect to the server
            * until a server is actually seen as present */
            do {
                System.out.println("waiting for server");
                communicator.send(driver.initAngles(clientId));
                receivedMessage = communicator.receive(UDP_TIMEOUT);

                System.out.println(receivedMessage);

                // send
                // receive with timout (add to communicator)
            } while (receivedMessage == null || receivedMessage.indexOf("***identified***") < 0);

            System.out.println("found server");
            /* original while loop
             * proceed to get the driver to act.
             * now we listen for a sensor (to act to) or a message such as reset or shutdown */

            currentStep = 0;
            shutdownOccured = false;

            
            while (true) {
                // first we receive the sensors info
                receivedMessage = communicator.receive(UDP_TIMEOUT);

                if (receivedMessage != null) {

                  
                   
                        if (receivedMessage.equals("***shutdown***")) {

                            shutdownOccured = true;
                            System.out.println("Server shutdown");
                
 
                            break;
                        }

                        if (receivedMessage.equals("***restart***")) {
                            driver.reset();

                            if (verbose)
                            {
                                System.out.println("Server restarting!");
                            }



                            break;
                        }
                     

                        if (currentStep < maxSteps || maxSteps == 0) {
                            // Update the sensor , so the driver can read them and send the appropriate action back.
                            sensor.parse(receivedMessage);
                            // then we call the Drivers control method to update its actions
// proof of testing
//                            timeBefore=System.currentTimeMillis();

                            driver.control();
// proof of testing
//                            System.out.println(System.currentTimeMillis()-timeBefore);

                        } else {
                   
                            action.restartRace = true;
                        }

                        currentStep++;
                        communicator.send(action.toString());

                    

                } else {
                    System.out.println("Server did not respond within the timeout");
                }

            }

        } while (++currentEpisode < maxEpisodes && !shutdownOccured);

        	driver.shutdown();

         
		communicator.close();
		

                System.out.println("Client shutdown.");
		System.out.println("Bye, bye!");

    }
        /* the warmup loop is slighly different from the "normal loop"
     * in that it keeps in the sync with the server as normal by calling
     * the "control" method and then sending the Actions over the socket...
     * the client has 10ms to do this. After that 10ms then the client waits another
     * 10ms for the next sensort input.... This is valuable time where we can be recording the track
     * I could bundle this recording in with the control method, but doing this in the "idle time"
     * ensures we don't time out. Once we start timing out, we lose sync with server and our client is useless
     */

        private void warmupLoop() throws IOException {


        do {


            do {
                System.out.println("waiting for server");
                communicator.send(driver.initAngles(clientId));
                receivedMessage = communicator.receive(UDP_TIMEOUT);

                System.out.println(receivedMessage);


            } while (receivedMessage == null || receivedMessage.indexOf("***identified***") < 0);

            System.out.println("found server");
  
            currentStep = 0;
            shutdownOccured = false;


            while (true) {
              
                receivedMessage = communicator.receive(UDP_TIMEOUT);

                if (receivedMessage != null) {
                  
                        if (receivedMessage.equals("***shutdown***")) {

                            shutdownOccured = true;
                            System.out.println("Server shutdown");
                   
                 


                            break;
                        }

                        if (receivedMessage.equals("***restart***")) {
                            driver.reset();

                            if (verbose)
                            {
                                System.out.println("Server restarting!");
                            }



                            break;
                        }


                        if (currentStep < maxSteps || maxSteps == 0) {

                            sensor.parse(receivedMessage);

                            driver.control();

                            
                        } else {

                            action.restartRace = true;
                        }

                        currentStep++;
                        communicator.send(action.toString());

                        /* The difference in the warmup loop is that we
                         * wisely use our 10ms idle time record the track details
                         */

                        driver.record();



                } else {
                    System.out.println("Server did not respond within the timeout");
                }

            }

        } while (++currentEpisode < maxEpisodes && !shutdownOccured);

        	driver.shutdown();


		communicator.close();


                System.out.println("Client shutdown.");
		System.out.println("Bye, bye!");

    }

    private void parseParameters() {

        for (int i = 0; i < arguments.length; i++) {
            StringTokenizer st = new StringTokenizer(arguments[i], ":");
            String entity = st.nextToken();
            String value = st.nextToken();
            if (entity.equals("port")) {
                port = Integer.parseInt(value);
            }
            if (entity.equals("host")) {
                host = value;
            }
            if (entity.equals("id")) {
                clientId = value;
            }
            if (entity.equals("verbose")) {
                if (value.equals("on")) {
                    verbose = true;
                } else if (value.equals("off")) {
                    verbose = false;
                } else {
                    System.out.println(entity + ":" + value
                            + " is not a valid option");
                    System.exit(0);
                }
            }
            if (entity.equals("id")) {
                clientId = value;
            }
            if (entity.equals("stage")) {
                stage = Byte.parseByte(value);
            }
            if (entity.equals("trackName")) {
                trackName = value;
            }
            if (entity.equals("maxEpisodes")) {
                maxEpisodes = Integer.parseInt(value);
                if (maxEpisodes <= 0) {
                    System.out.println(entity + ":" + value
                            + " is not a valid option");
                    System.exit(0);
                }
            }
            if (entity.equals("maxSteps")) {
                maxSteps = Integer.parseInt(value);
                if (maxSteps < 0) {
                    System.out.println(entity + ":" + value
                            + " is not a valid option");
                    System.exit(0);
                }
            }
        }

    }
}
