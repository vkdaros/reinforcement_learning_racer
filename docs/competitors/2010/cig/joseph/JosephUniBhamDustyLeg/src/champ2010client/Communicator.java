/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package champ2010client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 *
 * @author j
 * Communicates with the socket
 */
public class Communicator {

    boolean verbose;
    // buffer for recieving
    byte[] recieveBuffer = new byte[1024];
    // JAVa UDP connection
    DatagramSocket socket;
    // send stuff
    String sendString;
    // buffer for sending
    byte[] sendBuffer;
    DatagramPacket sendPacket;
    // receive stuff
    DatagramPacket receivePacket = new DatagramPacket(recieveBuffer, recieveBuffer.length);
    String receiveString;
    // just testing out.
    String receiveToString;
    // Our IP Address is needed for sending packets
    private InetAddress address;
    // We need to know to which port we are sending the packets
    private String host;
    private int port;
    // received message
    private String receivedMessage;

    // constructor creates socket
    public Communicator(String host, int port, boolean verbose) throws SocketException, UnknownHostException {
        this.host = host;
        this.port = port;
        this.verbose = verbose;
        // initialise socket for the client
        socket = new DatagramSocket();



        address = InetAddress.getLocalHost();



    }

    // send from socket
    public void send(String msg) throws IOException {

        if (verbose) {
            System.out.println("Sent: " + msg);
        }

        sendBuffer = msg.getBytes();

        // send it

        sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, address, port);


        socket.send(sendPacket);

    }

    // receive from socket
    public String receive() throws IOException {




        // else it is the server telling us something such as shut down or reset

        		try {
                             

        socket.receive(receivePacket);



     

        receivedMessage = new String(receivePacket.getData(), 0, receivePacket.getLength() - 1);
 


        if (verbose) {
            System.out.println("recieved");

            System.out.println(receivedMessage);

        }

        return receivedMessage;


		} catch(SocketTimeoutException se){
			if (verbose)
				System.out.println("Socket Timeout!");
		} catch (Exception e) {
			e.printStackTrace();
		}

                        return null;

    }

    public String receive(int timeout) throws SocketException, IOException {




        		try {
			        // set a timout
        socket.setSoTimeout(timeout);


        receivedMessage = receive();
        // put the timout back to zero
        socket.setSoTimeout(0);

        return receivedMessage;
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;


    }

    public void close() {
        socket.close();
    }
}
