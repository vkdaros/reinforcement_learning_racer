/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package champ2010client;

/**
 *
 * @author j
 */
public abstract class Driver {

    // we just have a single action and sensor and mofify that :)
    public Action action;
    public Sensor sensor;
    public String trackName;

  
    public abstract void control();

    public abstract void reset(); // called at the beginning of each new trial

    public abstract void shutdown();

    public String initAngles(String clientName)
    {

        // standard values I used for the sensors in all my final bots.
        // I have multiple values at the same position as my simple approach to noise filtering
        return clientName + "(init -30.0 -30.0 -30.0 -30.0 -30.0 -30.0 -30.0 -30.0 -30.0 0.0 30.0 30.0 30.0 30.0 30.0 30.0 30.0 30.0 30.0)";


    }

    // just for warmup
    public abstract void record();

}
