/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package champ2010client;

import java.util.StringTokenizer;

/**
 *
 * @author j
 */
public class Sensor {


    StringTokenizer tokens;


    private double angle;
    private double currentLapTime;
    private double damage;
    private double distanceFromStart;
    private double distanceRaced;
    private double fuel;
    private byte gear;
    private double lastLapTime;
    private double[] opponents;
    private byte racePosition;
    private double rpm;
    private double speed;
    private double speedY;
    private double speedZ;
    private double[] trackSensors;
    private double trackPosition;


    private double[] wheelSpinVelocity;
    private double z;
    private double[] focus;

    String t;


    public Sensor()
    {
        opponents = new double[36];
        trackSensors = new double[19];
        wheelSpinVelocity = new double[4];
    }


    public void parse(String message) {
        // delimiters are ()
        tokens = new StringTokenizer(message, " ()");


   

        //angle
        t= tokens.nextToken();
        t=tokens.nextToken();
        angle = Double.parseDouble(t);


        tokens.nextToken();
        currentLapTime = Double.parseDouble(tokens.nextToken());


        tokens.nextToken();
        damage = Double.parseDouble(tokens.nextToken());



        tokens.nextToken();
        setDistanceFromStart(Double.parseDouble(tokens.nextToken()));


        tokens.nextToken();
        distanceRaced = Double.parseDouble(tokens.nextToken());


        tokens.nextToken();
        fuel = Double.parseDouble(tokens.nextToken());


        tokens.nextToken();
        gear = Byte.parseByte(tokens.nextToken());


        tokens.nextToken();
        lastLapTime = Double.parseDouble(tokens.nextToken());



        tokens.nextToken();

        for (int i=0; i<opponents.length; i++) {
            opponents[i] = Double.parseDouble(tokens.nextToken());
        }

        tokens.nextToken();
        racePosition = Byte.parseByte(tokens.nextToken());


        tokens.nextToken();
        rpm = Double.parseDouble(tokens.nextToken());


        tokens.nextToken();
        speed = Double.parseDouble(tokens.nextToken());


        tokens.nextToken();
        speedY = Double.parseDouble(tokens.nextToken());


        tokens.nextToken();
        speedZ = Double.parseDouble(tokens.nextToken());



        tokens.nextToken();


        for (int i=0; i<trackSensors.length; i++) {
            trackSensors[i] = Double.parseDouble(tokens.nextToken());
        }



        tokens.nextToken();
        trackPosition = Double.parseDouble(tokens.nextToken());



       tokens.nextToken();

       for (int i=0; i<wheelSpinVelocity.length; i++) {
       wheelSpinVelocity[0]=Double.parseDouble(tokens.nextToken());
        }


       tokens.nextToken();
       z = Double.parseDouble(tokens.nextToken());



       // I won't parse the focus sensors because I do not use them.

    }

    /**
     * @return the angle
     */
    public double getAngle() {
        return angle;
    }

    /**
     * @return the currentLapTime
     */
    public double getCurrentLapTime() {
        return currentLapTime;
    }

    /**
     * @return the damage
     */
    public double getDamage() {
        return damage;
    }

    /**
     * @return the distanceFromStart
     */
    public double getDistanceFromStart() {
        return distanceFromStart;
    }

    /**
     * @return the distanceRaced
     */
    public double getDistanceRaced() {
        return distanceRaced;
    }

    /**
     * @return the fuel
     */
    public double getFuel() {
        return fuel;
    }

    /**
     * @return the gear
     */
    public byte getGear() {
        return gear;
    }

    /**
     * @return the lastLapTime
     */
    public double getLastLapTime() {
        return lastLapTime;
    }

    /**
     * @return the opponents
     */
    public double[] getOpponents() {
        return opponents;
    }

    /**
     * @return the racePosition
     */
    public byte getRacePosition() {
        return racePosition;
    }

    /**
     * @return the rpm
     */
    public double getRpm() {
        return rpm;
    }

    /**
     * @return the speed
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * @return the speedY
     */
    public double getSpeedY() {
        return speedY;
    }

    /**
     * @return the speedZ
     */
    public double getSpeedZ() {
        return speedZ;
    }

    /**
     * @return the trackSensors
     */
    public double[] getTrackSensors() {
        return trackSensors;
    }

    /**
     * @return the trackPosition
     */
    public double getTrackPosition() {
        return trackPosition;
    }

    /**
     * @param distanceFromStart the distanceFromStart to set
     */
    public void setDistanceFromStart(double distanceFromStart) {
        this.distanceFromStart = distanceFromStart;
    }

    /**
     * @return the wheelSpinVelocity
     */
    public double[] getWheelSpinVelocity() {
        return wheelSpinVelocity;
    }

    /**
     * @return the z
     */
    public double getZ() {
        return z;
    }

    /**
     * @return the focus
     */
    public double[] getFocus() {
        return focus;
    }
}
