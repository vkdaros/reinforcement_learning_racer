package champ2010client.competitive;

import champ2010client.Sensor;

/**
 *
 * @author j
 * detects whether the car has gone airbourne... if so we must slow down
 */
public class FlyingDetector {

    Sensor sensor;

    public FlyingDetector(Sensor sensor) {

        this.sensor = sensor;

    }


    /* if we reach a certain distance of the ground I classify this as flying.....
     * this is a signal to slow down. */
    public boolean isFlying() {

        System.out.println(sensor.getZ());


        if (sensor.getZ() > 0.5) {
            return true;
        }

        return false;
    }
}
