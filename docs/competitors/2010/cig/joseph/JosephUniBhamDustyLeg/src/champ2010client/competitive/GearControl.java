package champ2010client.competitive;

import champ2010client.*;

/**
 *
 * @author j
 */
public class GearControl {

    Action action;
    Sensor sensor;

    public GearControl(Action action, Sensor sensor) {
        this.action = action;
        this.sensor = sensor;
    }

    public void gear() {

        /* 7500 rpm was found to be the value the car changes up gear
         * when playing as a human in torcs */


        if (sensor.getRpm() >= 7500 && sensor.getSpeed() > 30) {
            System.out.println("raise gear");
            action.gear++;
        } else if (sensor.getRpm() <= 4.5 && action.gear > 1) {
            System.out.println("drop gear");
            action.gear--;

        }




    }
}
