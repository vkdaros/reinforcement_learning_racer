/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package champ2010client.competitive;

import champ2010client.Action;
import champ2010client.Clutch;
import champ2010client.Driver;
import champ2010client.Sensor;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author j
 */
public class QualifyingDriver extends Driver {

    Reader reader;
    TurnControl steer;
    SpeedControl speedControl;
    GearControl gear;
    Clutch clutch;
    int currentPosition;
    int gameTicks = 0;

    public QualifyingDriver(Action action, Sensor sensor, String trackName) throws FileNotFoundException, IOException {

        System.out.println("Initializing competitive (stage 1 or 2) driver.");
        this.action = action;
        this.sensor = sensor;
        this.trackName = trackName;

        reader = new Reader(trackName);

//        steer = new TurnControl(action, sensor, reader.getBearings());

        steer = new TurnControl(action, sensor, reader.getBearings(), reader.getSpeeds());

        // dynamic speed based on track file
//        speedControl = new SpeedControl(action, sensor, reader.getSpeeds());

        gear = new GearControl(action, sensor);

        clutch = new Clutch(action);

        action.gear = 1;
    }

    @Override
    public void control() {

        currentPosition = (int) sensor.getDistanceFromStart();


        steer.steer(currentPosition);

//        speedControl.adjustToDynamicSpeed(currentPosition);

        gear.gear();

        clutch.clutch();

        gameTicks++;

//        System.out.println(sensor.getDistanceFromStart()+"      "+sensor.getZ());





    }

    @Override
    public void reset() {

        action.resetValues();
        System.out.println("Competitive Stage (1 or 2) driver reset.");
    }

    @Override
    public void shutdown() {
        System.out.println("game ticks " + gameTicks);
        System.out.println("Competitive Stage (1 or 2) driver shutdown.");
    }

    @Override
    public void record() {
        System.out.println("Record not implemented on qualifying stage (1) driver.");
    }
}
