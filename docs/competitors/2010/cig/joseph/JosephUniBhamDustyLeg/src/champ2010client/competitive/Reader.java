package champ2010client.competitive;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author j
 * Loads all the turn data from a file into  data structures
 * This data is then accessed by other modules to regulate speed
 * And later it will be used for overtaking opponents
 */
public class Reader {

    File file;
    BufferedReader bufferedReader;
    int lengthOfTrack;
    int[] speeds;
    char[] bearings;

    // load file in constructor
    public Reader(String trackName) throws FileNotFoundException, IOException {

        file = new File(trackName);
        bufferedReader = new BufferedReader(new FileReader(file));

        read();

    }

    // reads all the track positions into an array...
    private void read() throws IOException {


        System.out.println("read all values... ");


        lengthOfTrack = Integer.parseInt(bufferedReader.readLine());


        speeds = new int[lengthOfTrack];



        for (int i = 0; i < speeds.length; i++) {




            speeds[i] = Integer.parseInt(bufferedReader.readLine());

        }

        bearings = new char[lengthOfTrack];

        for (int i = 0; i < bearings.length; i++) {
            bearings[i] = bufferedReader.readLine().charAt(0);
        }

        bufferedReader.close();



    }

    public int[] getSpeeds() {
        return speeds;
    }

    public char[] getBearings() {
        return bearings;
    }
}
