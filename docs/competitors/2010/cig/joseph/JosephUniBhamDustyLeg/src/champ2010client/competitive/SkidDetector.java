/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package champ2010client.competitive;

import champ2010client.Sensor;

/**
 *
 * @author j
 * detects if there is a significant skid... if so, deal with it
 */
public class SkidDetector {

    Sensor sensor;
    // STDEV vars
    final byte nMinus1 = 3;
    double mean;
    double[] xMinusXbarSquared;
    double stdev;

    public SkidDetector(Sensor sensor) {

        this.sensor = sensor;

        xMinusXbarSquared = new double[4];

    }

    public boolean isSkidding() {

        getSTDEV();

        if (stdev > 20) {
            // testing area
//            System.out.println("a definite skid!");
            return true;
        }

        return false;





    }

    private void getSTDEV() {
// testing area
//        System.out.println("sensor values = "+sensor.getWheelSpinVelocity()[0]+
//                sensor.getWheelSpinVelocity()[1]+
//                sensor.getWheelSpinVelocity()[2]+
//                sensor.getWheelSpinVelocity()[3]);

        mean = sensor.getWheelSpinVelocity()[0]
                + sensor.getWheelSpinVelocity()[1]
                + sensor.getWheelSpinVelocity()[2]
                + sensor.getWheelSpinVelocity()[3];

        mean = mean / 4;

        xMinusXbarSquared[0] = Math.pow(sensor.getWheelSpinVelocity()[0] - mean, 2);
        xMinusXbarSquared[1] = Math.pow(sensor.getWheelSpinVelocity()[1] - mean, 2);
        xMinusXbarSquared[2] = Math.pow(sensor.getWheelSpinVelocity()[2] - mean, 2);
        xMinusXbarSquared[3] = Math.pow(sensor.getWheelSpinVelocity()[3] - mean, 2);

        stdev = xMinusXbarSquared[0]
                + xMinusXbarSquared[1]
                + xMinusXbarSquared[2]
                + xMinusXbarSquared[3];

        stdev = stdev / nMinus1;

        stdev = Math.sqrt(stdev);
// testing area
//        System.out.println("stdev = "+stdev);


    }
}
