package champ2010client.competitive;

import champ2010client.Action;
import champ2010client.Sensor;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author j
 */
public class SpeedControl {

    private Action action;
    private Sensor sensor;
    int[] speeds;
    // Warmup
    private int speed;
    private int upperSpeedBand;
    private int lowerSpeedBand;
    boolean speedOff = false;
    SkidDetector skidDetector;
    FlyingDetector flyingDetector;

    // Competition
    public SpeedControl(Action action, Sensor sensor, int[] speeds) throws FileNotFoundException, IOException {
        this.action = action;
        this.sensor = sensor;

        this.speeds = speeds;

        skidDetector = new SkidDetector(sensor);
        flyingDetector = new FlyingDetector(sensor);





    }

    // warmup , backup
    private void adjustToSpeed() {

        if (skidDetector.isSkidding() || flyingDetector.isFlying()) {
            action.accelerate = 0;
        } else {
            // if I'm under the low speed bracket speed up
            if (sensor.getSpeed() < lowerSpeedBand) {


                action.accelerate += 0.1;
                action.brake = 0;

            } else if (sensor.getSpeed() > upperSpeedBand) {
                action.brake += 0.1;
                action.accelerate = 0;
            } else {
                action.accelerate = 0;
                action.brake = 0;
            }

        }





    }

    public void adjustForHairpinApproach() {
        speed = 40;

        adjustToSpeed();

    }

    // competition
    public void adjustToDynamicSpeed(int currentPosition) {
        // modify the speeds
        speed = speeds[currentPosition];

//            System.out.println("On segment "+Math.floor(sensor.getDistanceFromStart()) +"I should be going "+speed+" , I am going "+sensor.getSpeed());
        lowerSpeedBand = speed - 5;
        upperSpeedBand = speed + 5;

        adjustToSpeed();



    }

    public void coast() {

        action.clutch = 1;
        action.accelerate = 0;

    }
}
