package champ2010client.competitive;

import champ2010client.*;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author j
 */
public class TurnControl {

    Sensor sensor;
    Action action;
    Double steering;
    char[] bearings;
    // 200 * 0.005 = 1
    final double convFactor = 0.005;
    final double maxSteerAngle = Math.PI / 4;
    final double angleToSteer = 1 / maxSteerAngle;
    SpeedControl speedControl;

    public TurnControl(Action action, Sensor sensor, char[] bearings, int[] speeds) throws FileNotFoundException, IOException {
        this.action = action;

        this.sensor = sensor;

        this.bearings = bearings;



        System.out.println("correct steer class :)");

        speedControl = new SpeedControl(action, sensor, speeds);


    }

    public void steer(int currentPosition) {


        if (sensor.getTrackPosition() >= 1) {
            action.steering = -1;



        } else if (sensor.getTrackPosition() <= -1) {
            action.steering = 1;


        } else {



            switch (bearings[(currentPosition + 5) % bearings.length]) {
                case 'l':
                    speedControl.coast();
                    System.out.println("left hairpin");
                    leftTurn();
                    break;
                case 'r':
                    speedControl.coast();
                    System.out.println("right hairpin");
                    rightTurn();
                    break;
                default:

                    if (isHairpinAhead(currentPosition)) {

                        speedControl.adjustForHairpinApproach();
                        sensorFeeler();



                    } else {

                        speedControl.adjustToDynamicSpeed(currentPosition);
                        sensorFeeler();
                    }

                    break;
            }



        }



    }

    /*     hairpins are recorded at around 40 segments
     *     therefore if we look ahead 20 before a hairpin
     *     we should always see it. */
    private boolean isHairpinAhead(int currentPosition) {

        /* if the value at 10 segments ahead is anything other then a zero
         * we have an 'l', or 'r' ; a hairpin. */
        if (bearings[(currentPosition + 30) % bearings.length] != 0) {
            return true;

        }

        return false;
    }

    private void sensorFeeler() {
        steering = sensor.getTrackSensors()[0] * convFactor;
        steering -= sensor.getTrackSensors()[18] * convFactor;

        steering += sensor.getTrackSensors()[1] * convFactor;
        steering -= sensor.getTrackSensors()[17] * convFactor;

        steering += sensor.getTrackSensors()[2] * convFactor;
        steering -= sensor.getTrackSensors()[16] * convFactor;

        steering += sensor.getTrackSensors()[3] * convFactor;
        steering -= sensor.getTrackSensors()[15] * convFactor;

        steering += sensor.getTrackSensors()[4] * convFactor;
        steering -= sensor.getTrackSensors()[14] * convFactor;

        steering += sensor.getTrackSensors()[5] * convFactor;
        steering -= sensor.getTrackSensors()[13] * convFactor;

        steering += sensor.getTrackSensors()[6] * convFactor;
        steering -= sensor.getTrackSensors()[12] * convFactor;

        steering += sensor.getTrackSensors()[7] * convFactor;
        steering -= sensor.getTrackSensors()[11] * convFactor;

        steering += sensor.getTrackSensors()[8] * convFactor;
        steering -= sensor.getTrackSensors()[10] * convFactor;

        action.steering = steering;
    }

    private void leftTurn() {




        if (sensor.getTrackPosition() < 0.5 && sensor.getAngle() > 0) {



            action.steering = 1;

        } else {


            sensorFeeler();
        }



    }

    private void rightTurn() {


        if (sensor.getTrackPosition() > -0.5 && sensor.getAngle() < 0) {
            System.out.println("over track steer rhpin");


            action.steering = -1;
        } else {
            System.out.println("inside track - reset");

            sensorFeeler();
        }



    }
}
