/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package champ2010client.warmup;

import champ2010client.*;

/**
 *
 * @author j
 */
public class ClutchControl {
    
    Action action;



    public ClutchControl(Action action)
    {
        this.action = action;

        action.clutch=1;
    }

    // reduce clutch for a steady start
    public void clutch()
    {
        action.clutch-=0.01;
        
    }

    public void resetClutch()
    {
        action.clutch=1;
    }



}
