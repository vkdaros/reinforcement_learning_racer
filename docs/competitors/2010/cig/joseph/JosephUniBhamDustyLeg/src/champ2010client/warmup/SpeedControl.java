package champ2010client.warmup;

import champ2010client.*;

/**
 *
 * @author j
 */
class SpeedControl {

    private Action action;
    private Sensor sensor;

    private int speed;
    private int upperSpeedBand = 11;
    private int lowerSpeedBand = 6;
    boolean speedOff = false;


    public SpeedControl(champ2010client.Action action, champ2010client.Sensor sensor) {
        this.action = action;
        this.sensor = sensor;

    }


    public void adjust() {

        // if I'm under the low speed bracket speed up
        if (sensor.getSpeed() < lowerSpeedBand) {


          action.accelerate += 0.1;

            action.brake = 0;

        } else if (sensor.getSpeed() > upperSpeedBand) {
            action.brake += 0.1;
            action.accelerate = 0;
        } else {
            action.accelerate = 0;
            action.brake = 0;
        }

    }
}
