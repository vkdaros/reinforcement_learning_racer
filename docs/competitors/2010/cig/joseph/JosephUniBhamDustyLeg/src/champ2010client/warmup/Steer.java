package champ2010client.warmup;

import champ2010client.*;

/**
 *
 * @author j
 * All the warmup stage does is match the our steering to the track angle.
 *
 * Of course we react too late however it is not a problem on warmup because:
 * a) We have over 30  minutes to complete a single lap (for recording purposes)
 * b) It does not matter if we lightly bump or scrape the walls
 *
 * Other then being an old value the trackAngle would be our perfect sensor.
 * It is noiseless and the easiest to interpret.
 *
 * In the warmup we simply use it to record where the turns are.....
 * We do bump and scrape walls but our damage is not high enough to get thrown out the track :)
 */
public class Steer {

    Sensor sensor;
    Action action;
    Double steering;
    final double maxSteerAngle = Math.PI / 4;
    final double angleToSteer = 1 / maxSteerAngle;

    public Steer(Action action, Sensor sensor) {
        this.action = action;

        this.sensor = sensor;

        System.out.println("correct steer class :)");


    }

    public void steer() {


        if (sensor.getAngle() >= 1) {
            action.steering = 1;

        } else if (sensor.getAngle() <= -1) {
            action.steering = -1;
        } else {

         
                    action.steering = sensor.getAngle() * angleToSteer;
                

          

            }
            

        // if the angle is bigger then x AND we are over the centre line make a full steer to turn un

        // ...
    }
}
