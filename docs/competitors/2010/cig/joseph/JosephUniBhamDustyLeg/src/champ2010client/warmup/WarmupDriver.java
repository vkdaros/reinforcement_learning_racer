package champ2010client.warmup;

import champ2010client.Action;
import champ2010client.Clutch;
import champ2010client.Driver;
import champ2010client.Sensor;
import champ2010client.warmup.record.Recorder;
import champ2010client.warmup.write.Writer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author j
 */
public class WarmupDriver extends Driver {

    Steer steer;
    SpeedControl speedControl;
    Clutch clutch;
    Writer writer;
    Recorder recorder;
    double hairPinDouble = Math.PI / 8;
    int gameTicks = 0;

    public WarmupDriver(Action action, Sensor sensor, String trackName) throws IOException {
        this.action = action;
        this.sensor = sensor;
        this.trackName = trackName;

        steer = new Steer(action, sensor);

        // fixed speed control at 60 kmph
        speedControl = new SpeedControl(action, sensor);

        recorder = new Recorder(action, sensor);






        clutch = new Clutch(action);


        action.gear = 1;
    }

    @Override
    public void control() {

        steer.steer();

        speedControl.adjust();





        clutch.clutch();

        gameTicks++;


        System.out.println(sensor.getDistanceFromStart() + "          " + action.steering);


    }

    @Override
    public void reset() {
        action.resetValues();
        System.out.println("Warmup Stage (0) driver reset.");
    }

    @Override
    // parses all the Recorders and writes to a file
    public void shutdown() {

        recorder.endRecord();

        
        try {
            writer = new Writer(trackName, sensor, action, recorder.getTurns(), recorder.getBumpFactors());
        } catch (IOException ex) {
            Logger.getLogger(WarmupDriver.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    @Override
    public void record() {

        recorder.record();

    }
}
