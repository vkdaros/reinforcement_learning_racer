/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package champ2010client.warmup.record;


import champ2010client.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author j
 */
public class BumpRecorder {


    ArrayList<Double>[] bumps;

    // bump factor represents the biggest overall bumpiness of each metre of the track
    // 0.4 and over is considered very high bumpiness
    // down to 0 which is low bumpiness i.e. a flat stretch of track.
    double[] bumpFactors;




    Sensor sensor;

    int currentPosition;

    public BumpRecorder(Sensor sensor)
    {

        this.sensor = sensor;

    }

    public void initialize(int length)
    {
        bumps = new ArrayList[length];

        bumpFactors = new double[length];




    }


    public void record(int position)
    {

        if(bumps[position] == null)
        {
            bumps[position] = new ArrayList<Double>();
        }

        bumps[position].add(sensor.getSpeedZ());

    }

    // take the averages within the ArrayLists and create single values
    // these values represent the average turn per metre of track
    // these values are easier to make use of
    public void parse()
    {

     

//
        // iterate through each ArrayList
        for(int i=0; i < bumps.length; i++)
        {

            bumpFactors[i]= Collections.max(bumps[i])-Collections.min(bumps[i]);

            // our bump factors should be no bigger then one, since I aim for a bump factor
            // between 0 and 1
             if(bumpFactors[i] > 1)
             {
                 bumpFactors[i] = 1;
            }

            
            System.out.println("section "+i+"bump factor is "+bumpFactors[i]);
        }

        // get the differences within each section
        // difference is defined a

//        // continue later... minus min from max... there you have it.
//        Collections.min(bumps[0]);


    }


    public double[] getBumpFactors()
    {
        return bumpFactors;

    }

    }



    


    


