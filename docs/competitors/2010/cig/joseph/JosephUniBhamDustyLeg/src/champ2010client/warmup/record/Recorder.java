/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package champ2010client.warmup.record;

import champ2010client.Action;
import champ2010client.Sensor;

/**
 *
 * @author j
 */
public class Recorder {

    Sensor sensor;


    TurnRecorder turnRecorder;

    BumpRecorder bumpRecorder;

    int currentPosition;
    int previousPosition;

        boolean start = true;
    boolean recording = false;

    

    
    public Recorder(Action action, Sensor sensor)
    {

        this.sensor = sensor;

        turnRecorder = new TurnRecorder(action);

        bumpRecorder = new BumpRecorder(sensor);

    }

    public void record()
    {

        currentPosition = (int) Math.floor(sensor.getDistanceFromStart());

//
//        turnRecorder.record(currentPosition);
//
//        bumpRecorder.record(currentPosition);





        // if we are just starting out we have no previous currentPosition so have to use our
        // current currentPosition
        if (start) {
            previousPosition = currentPosition;



            start = false;

        }


        if (recording) {


            // whatever you want to record....
            turnRecorder.record(currentPosition);
            bumpRecorder.record(currentPosition);



        } else {
                    // INITIALIZATION
                    // if our previous currentPosition has gone less then our current currentPosition then we have crossed
                    // the start line and should start recording
            if (previousPosition > currentPosition) {


                turnRecorder.initialize(previousPosition+1);
                bumpRecorder.initialize(previousPosition+1);


                recording = true;

//                System.out.println("start recording on next tick");

            }

        }









        // at the end set the previous currentPosition
        previousPosition = currentPosition;
    }

    public void endRecord()
    {
        turnRecorder.parse();
        bumpRecorder.parse();
    }

    public double[] getTurns()
    {
        return turnRecorder.getTurns();
    }

    public double[] getBumpFactors()
    {

        return bumpRecorder.getBumpFactors();
    }

}
