/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package champ2010client.warmup.record;


import champ2010client.*;
import java.util.ArrayList;

/**
 *
 * @author j
 */
public class TurnRecorder {


    ArrayList<Double>[] turns;

    double[] sectionTurns;




    Action action;

    int currentPosition;

    public TurnRecorder(Action action)
    {


        this.action= action;
      

    }

    public void initialize(int length)
    {
        turns = new ArrayList[length];

        sectionTurns = new double[length];


    }


    public void record(int position)
    {

        if(turns[position] == null)
        {
            turns[position] = new ArrayList<Double>();
        }

        // turn no bigger then 1 (max)
        if(action.steering > 1)
        {
            turns[position].add(1D);

        }
        else
        {
            turns[position].add(action.steering);
        }

        

    }

    // take the averages within the ArrayLists and create single values
    // these values represent the average turn per metre of track
    // these values are easier to make use of
    public void parse()
    {

     
        double average;
        
        // iterate through each ArrayList
        for(int i=0; i < turns.length; i++)
        {
            average=0;
            // iterate through all the entries WITHIN the ArrayList
            for(int j=0; j < turns[i].size(); j++)
            {

                average += turns[i].get(j);
            }

            sectionTurns[i] = average / turns[i].size();

            System.out.println("section "+i+" turn is "+sectionTurns[i]);
        }

    }


    public double[] getTurns()
    {
        return sectionTurns;
    }



    


    

}
