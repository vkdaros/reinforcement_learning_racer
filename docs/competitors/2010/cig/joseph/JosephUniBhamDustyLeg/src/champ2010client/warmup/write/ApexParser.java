package champ2010client.warmup.write;

/**
 *
 * @author j
 * used for the warmup driver
 */
public class ApexParser {

    // to parse
    double[] turns;
    // to return
    char[] apexes;

    // states of parser
    enum States {

        CENTRE, LEFT, RIGHT
    };
    States state = States.CENTRE;
    int startOfTurn;
    boolean hairpin = false;

    public ApexParser(double[] turns) {

        this.turns = turns;


        apexes = new char[turns.length];


        parse();

    }

    private void parse() {
        // iterate through all the turns

        for (int i = 0; i < turns.length; i++) {

            if (state == States.CENTRE) {
                // signal left turn
                if (turns[i] > 0.1) {


                    startOfTurn = i;
                    state = States.LEFT;

                } else if (turns[i] < -0.1) {

                    startOfTurn = i;
                    state = States.RIGHT;
                }

            } else if (state == States.LEFT) {

                if (!(turns[i] > 0.1)) {
                    System.out.println("left turn end!");
                    // if our turn is a hairpin write this whole stretch off
                    // as a "hairpin section"
                    if (hairpin) {
                        System.out.println("record left hairpin turn");
                        // record
                        recordLs(i);

                        hairpin = false;

                    }

                    state = States.CENTRE;

                }


                // 0.43 was the lowest value I found for a hairpin.
                if (turns[i] > 0.43) {
                    System.out.println("left hairpin!");
                    hairpin = true;
                }


            } else // RIGHT
            {
                if (!(turns[i] < -0.1)) {
                    System.out.println("right turn end!");
                    // if our turn is a hairpin write this whole stretch off
                    // as a "hairpin section"
                    if (hairpin) {
                        System.out.println("record right hairpin turn");

                        // record
                        recordRs(i);

                        hairpin = false;

                    }

                    state = States.CENTRE;

                }


                if (turns[i] < -0.43) {
                    System.out.println("righthairpin!");
                    hairpin = true;
                }

            }

        }
    }


    private void recordLs(int currentPosition) {


        for (int i = startOfTurn; i != currentPosition; i = (i + 1) % apexes.length) {
            System.out.println("record 'l' at " + i);
            apexes[i] = 'l';

        }


    }

    private void recordRs(int currentPosition) {
        for (int i = startOfTurn; i != currentPosition; i = (i + 1) % apexes.length) {
            apexes[i] = 'r';

        }
    }

    public char[] getApexes() {
        return apexes;
    }
}
