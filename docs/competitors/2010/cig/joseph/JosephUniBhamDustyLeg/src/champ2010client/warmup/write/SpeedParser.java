package champ2010client.warmup.write;

/**
 *
 * @author j
 * used for the warmup driver
 */
public class SpeedParser {

    // to parse
    double[] turns;
    double[] bumpFactors;
    // to return
    int[] speeds;

    public SpeedParser(double[] turns, double[] bumpFactors) {

        this.turns = turns;

        this.bumpFactors = bumpFactors;


        initializeSpeeds();

        parse();
    }

    // at first our speeds are set to their maximum value
    // 250 k/h
    private void initializeSpeeds() {

        speeds = new int[turns.length];


        for (int i = 0; i < speeds.length; i++) {
            speeds[i] = 250;
        }

    }

    private void parse() {
        System.out.println("start parse");


        // iterate through the turns and bumpFactors
        // both same length
        for (int i = 0; i < turns.length; i++) {

            // a full turn or a full bump factor alone can reduce our speed to 50 k/h
            speeds[i] -= Math.abs(turns[i]) * 200;
            speeds[i] -= bumpFactors[i] * 200;

            // the slowest speeds we set are 40 k/h
            if (speeds[i] < 40) {
                speeds[i] = 40;
            }


        }

    }

    public int[] getSpeeds() {
        return speeds;
    }
}
