package champ2010client.warmup.write;

import champ2010client.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author j
 * used for the warmup driver
 * to write all the data needed for our competitive driver
 */
public class Writer {

    File file;
    FileWriter fileWriter;
    PrintWriter printWriter;
    double[] turns;
    double[] bumpFactors;
    int[] speeds;
    char[] apexes;
    ApexParser apexParser;
    SpeedParser speedParser;

    // the constructor writes everything to a file
    public Writer(String trackName, Sensor sensor, Action action, double[] turns, double[] bumpFactors) throws IOException {

        file = new File(trackName);

        fileWriter = new FileWriter(file);

        // print writer with auto-flush (true)
        printWriter = new PrintWriter(fileWriter, true);

        apexParser = new ApexParser(turns);

        speedParser = new SpeedParser(turns, bumpFactors);

        write();


    }

    // write to file
    private void write() {

        speeds = speedParser.getSpeeds();

        apexes = apexParser.getApexes();

        // first write the length of both our arrays
        printWriter.print(speeds.length);

        // write speeds
        for (int i = 0; i < speeds.length; i++) {
            printWriter.println();
            printWriter.print(speeds[i]);
        }

        // write bearings
        for (int i = 0; i < apexes.length; i++) {
            printWriter.println();
            printWriter.print(apexes[i]);
        }


        printWriter.close();


    }
}
