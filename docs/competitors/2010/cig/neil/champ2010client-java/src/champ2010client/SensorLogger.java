package champ2010client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SensorLogger {

	//Arraylists used to log track data and output data
	ArrayList<Double> inputData = new ArrayList<Double>();
	ArrayList<Double> outputTargetData = new ArrayList<Double>();

	// writer used to logsensor data
	BufferedWriter out;

	// arrays used to store logged data
	double[] trackedge;
	double[] edgeSensors;
	double[] focusSensors;
	double[] opponentSensors;
	double[] wheelSpinVelocity;

	//Which Training Data Inputs to Include, this is used by NNDriver and NN
	private boolean[] trainingSensors = new boolean [24];
	{
		trainingSensors[0] = true; //Speed
		trainingSensors[1] = true; //AngleToTrackAxis
		trainingSensors[2] = false; //trackedge + 90 Degrees
		trainingSensors[3] = false; //trackedge + 80
		trainingSensors[4] = false; //trackedge + 70
		trainingSensors[5] = false; //trackedge + 60
		trainingSensors[6] = false; //trackedge + 50
		trainingSensors[7] = true; //trackedge + 40
		trainingSensors[8] = true; //trackedge + 30
		trainingSensors[9] = true; //trackedge + 20
		trainingSensors[10] = true; //trackedge + 10
		trainingSensors[11] = true; //trackedge Straight ahead
		trainingSensors[12] = trainingSensors[10]; //trackedge - 10 //These are matched up with thier opposite partners but do not have to be
		trainingSensors[13] = trainingSensors[9]; //trackedge - 20 //Having two variables for each makes it easier to program later because of the way the scanner reads in data
		trainingSensors[14] = trainingSensors[8]; //trackedge - 30
		trainingSensors[15] = trainingSensors[7]; //trackedge - 40
		trainingSensors[16] = trainingSensors[6]; //trackedge - 50
		trainingSensors[17] = trainingSensors[5]; //trackedge - 60
		trainingSensors[18] = trainingSensors[4]; //trackedge - 70
		trainingSensors[19] = trainingSensors[3]; //trackedge - 80
		trainingSensors[20] = trainingSensors[2]; //trackedge - 90
		trainingSensors[21] = true; //Track Position
		trainingSensors[22] = true; //Lateral Speed
		trainingSensors[23] = false; // 5 Opponent Sensors
	}

	public boolean getSensorInclude(int i)
	{
		return trainingSensors[i];
	}

	public SensorLogger(boolean training)
	{
		if(training)
			out = makeBufferedWriter();			// writer used to logsensor data
	}

	BufferedWriter makeBufferedWriter()
	{
		boolean append = true;
		try 
		{ 
			File outfile = new File("SensorLog.txt");
			if (outfile.exists())
			{
				System.out.println("File Found, Writing to SensorLog.txt");
			}
			else
			{
				System.out.println("File NOT Found, Creating File SensorLog.txt");
				append = false;
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(outfile,append));
			return out;
		}
		catch (IOException e) 
		{ 
			System.out.println("Error Creating File");
		}
		System.out.println("File Writer Not Made");
		return null;
	}

	ArrayList<Double> processInputSensors (SensorModel sensors, boolean racingMode)
	{
		inputData.clear();

		if(racingMode)
		{
			//Order of data is speed, angle, 13*Track Edge, track pos, lateral speed, 36* opponents
			if (trainingSensors[0])
				inputData.add(sensors.getSpeed());
			if (trainingSensors[1])
				inputData.add(sensors.getAngleToTrackAxis ());

			trackedge = sensors.getTrackEdgeSensors ();
			for(int i = 0; i < trackedge.length; i++) //adds the sensors in front and to either side
			{
				if (trainingSensors[i + 2]) //The include flags are two more than the position in the trackedge sensor
					inputData.add(trackedge[i]);
			}
			if (trainingSensors[21])
				inputData.add(sensors.getTrackPosition());
			if (trainingSensors[22])
				inputData.add(sensors.getLateralSpeed ());

			if (trainingSensors[23])
			{
				opponentSensors = sensors.getOpponentSensors ();	// basic information about other cars (only useful for multi-car races)
				for(int i = 14; i < opponentSensors.length - 16; i++) //Went for few at the front here, I think this is how the opponent sensors work!! :( the manual isn't too clear here
					inputData.add(opponentSensors[i]); //array
			}
		}
		else
		{
			//Store all the input values since we are training
			//Order of data is speed, angle, 13*Track Edge, track pos, lateral speed, 36* opponents
			inputData.add(sensors.getSpeed());
			inputData.add(sensors.getAngleToTrackAxis ());
			trackedge = sensors.getTrackEdgeSensors ();
			for(int i = 0; i < trackedge.length; i++) //adds the sensors in front and to either side
			{
				inputData.add(trackedge[i]);
			}
			inputData.add(sensors.getTrackPosition());					
			inputData.add(sensors.getLateralSpeed ());

			//Not going to log data about cars behind car
			opponentSensors = sensors.getOpponentSensors ();	// basic information about other cars (only useful for multi-car races)
			for(int i = 6; i < opponentSensors.length - 6; i++)
				inputData.add(opponentSensors[i]); //array
		}
		return inputData;
	}



	ArrayList<Double> processOutputSensors (SensorModel sensors)
	{
		outputTargetData.clear();

		outputTargetData.add(KeyLogger.getAccel());

		outputTargetData.add(KeyLogger.getBrake());

		outputTargetData.add(KeyLogger.getSteering());

		return outputTargetData;
	}



	void writeInputSensors(ArrayList<Double> inputData)
	{
		try
		//Write the data to our file
		{ 
			for(int i = 0; i < inputData.size(); i++)
				out.append(inputData.get(i).toString()+" ");
			out.append(" /input ");
		}
		catch (IOException e) 
		{ 
			System.out.println("Error logging data");
		}
	}

	void writeTargetSensors(ArrayList<Double> outputTargetData)
	{
		try
		//Write the data to our file
		{ 
			for(int i = 0; i < outputTargetData.size(); i++)
				out.append(outputTargetData.get(i).toString()+" ");
			out.append(" /targets ");
		}
		catch (IOException e) 
		{ 
			System.out.println("Error logging data");
		}
	}

	void shutDown()
	{
		try {
			//out.append(" EOF ");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error Closing File");
		}
	}

}
