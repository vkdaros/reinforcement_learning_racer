//Filters TXT Files
//Developed from http://home.tiscali.nl/~bmc88/java/sbook/0128.html by Neil Clarke for TORCS Project

package champ2010client;

import java.io.FilenameFilter;
import java.io.File;

class TXTFilter implements FilenameFilter 
{
	public boolean accept(File dir, String name) 
	{
		return (name.endsWith(".txt"));
	}
}