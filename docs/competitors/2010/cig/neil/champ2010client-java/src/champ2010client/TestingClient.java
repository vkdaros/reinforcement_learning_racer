package champ2010client;

import java.io.File;
import java.io.FilenameFilter;
import java.util.StringTokenizer;
import champ2010client.Controller.Stage;

/**
 * @author Daniele Loiacono
 * Edited by Neil Clarke to test TORCS cars
 */
public class TestingClient 
{
	private static int UDP_TIMEOUT = 10000;
	private static int port;
	private static String host;
	private static String clientId;
	private static boolean verbose;
	private static int maxEpisodes;
	private static int maxSteps;
	private static Stage stage;


	public static String getTrackName() 
	{
		return trackName;
	}

	private static String trackName;

	/**
	 * @param args
	 *            is used to define all the options of the client.
	 *            <port:N> is used to specify the port for the connection (default is 3001)
	 *            <host:ADDRESS> is used to specify the address of the host where the server is running (default is localhost)  
	 *            <id:ClientID> is used to specify the ID of the client sent to the server (default is championship2009) 
	 *            <verbose:on> is used to set verbose mode on (default is off)
	 *            <maxEpisodes:N> is used to set the number of episodes (default is 1)
	 *            <maxSteps:N> is used to set the max number of steps for each episode (0 is default value, that means unlimited number of steps)
	 *            <stage:N> is used to set the current stage: 0 is WARMUP, 1 is QUALIFYING, 2 is RACE, others value means UNKNOWN (default is UNKNOWN)
	 *            <trackName:name> is used to set the name of current track
	 */
	public static void main(String[] args) 
	{
		XMLExtractor judge = new XMLExtractor();
		FilenameFilter xmlfilter = new XMLFilter();
		FilenameFilter txtfilter = new TXTFilter();
		File weightfiles = new File("C:/Users/Neil/Documents/University/Project/Software/champ2010client-java/champ2010client-java/weights for Multiple practices"); //NEEDS to point to the results/practice directory on the relevant computer
		File [] allweightfiles = weightfiles.listFiles(txtfilter); //Finds all the files in the above directory

		System.out.println(allweightfiles.length+" weights files found");
		//		for (int a = 0; a < allfiles.length; a++){ 
		//			System.out.println(allfiles[a].getName());}

		//Open or make file that stores results with the weight values they came from
		String write_to = "C:/Program Files (x86)/torcs/results/practice/TABLEOFWEIGHTSANDRESULTS.txt";
		String practicelocation = "C:/Program Files (x86)/torcs/results/practice/";
		String winningresultsloc = "C:/Program Files (x86)/torcs/results/practice/TEST WINNERS/";
		String losingresultsloc = "C:/Program Files (x86)/torcs/results/practice/TEST LOSERS/";
		String winningweightsloc = "C:/Users/Neil/Documents/University/Project/Software/champ2010client-java/champ2010client-java/weights for Multiple practices/TEST WINNERS/";
		String losingweightsloc = "C:/Users/Neil/Documents/University/Project/Software/champ2010client-java/champ2010client-java/weights for Multiple practices/TEST LOSERS/";

		for (int a = 0; a <= allweightfiles.length ; a++)
		{
			File pathfiles_results = new File(practicelocation); //NEEDS to point to the results/practice directory on the relevant computer
			File [] allresultsfiles = pathfiles_results.listFiles(xmlfilter);
			while (a > 0 && allresultsfiles.length < 1) //wait for a result file to appear, after the first run
			{
				wait(1); //Wait 1 second
				allresultsfiles = pathfiles_results.listFiles(xmlfilter); //Finds all the files in the above directory
			}

			for (int a1 = 0; a1 < allresultsfiles.length; a1++)
			{
				if (allresultsfiles.length > 1) //All results should be taken care of by previous run, so if there is more than one something has gone wrong
				{
					System.out.println("ERROR MORE THAN ONE PRACTICE RESULT FOUND");
					System.exit(1);
				}
				if (allresultsfiles.length == 1) //First run there wil be no result file
				{
					//Find out if the previously ran car is a successful or not and move it as so..
					boolean move = true;
					if(judge.calculate(allresultsfiles[a1]))
					{
						move = allresultsfiles[a1].renameTo(new File(winningresultsloc.concat(allweightfiles[a-1].getName().substring(0, allweightfiles[a-1].getName().length()-4)).concat(".xml")));//The result file left corresponds to the previous weight file ran
						allweightfiles[a-1].renameTo(new File(winningweightsloc.concat(allweightfiles[a-1].getName())));
						System.out.println("Winner "+allweightfiles[a-1].getName());
					}
					else
					{
						move = allresultsfiles[a1].renameTo(new File(losingresultsloc.concat(allweightfiles[a-1].getName().substring(0, allweightfiles[a-1].getName().length()-4)).concat(".xml")));//The result file left corresponds to the previous weight file ran
						allweightfiles[a-1].renameTo(new File(losingweightsloc.concat(allweightfiles[a-1].getName())));
						System.out.println("Loser "+allweightfiles[a-1].getName());
					}
					boolean delete = false;
					if(move)
						System.out.println("File move complete");
					else //Sometimes the file will not move but we can delete it anyway, results not needed after being judged
					{
						delete = allresultsfiles[a1].delete();
						if(delete)
							System.out.println("File delete complete");
						else
						{
							System.out.println("FILE MOVE AND DELETE FAILED, EXITING!!!!!!");
							System.exit(1);
						}
					}
				}
			}

			if(a == allweightfiles.length ) //Finished all the weights, just had to sort out the last result, no more drivers to run so quit
				System.exit(0);

			//Rests of normal Client as given in SCRC package
			parseParameters(args);
			SocketHandler mySocket = new SocketHandler(host, port, verbose);
			String inMsg;

			Controller driver = load(args[0], allweightfiles[a], write_to);
			driver.setStage(stage);
			driver.setTrackName(trackName);

			/* Build init string */
			float[] angles = driver.initAngles();
			String initStr = clientId + "(init";
			for (int i = 0; i < angles.length; i++) {
				initStr = initStr + " " + angles[i];
			}
			initStr = initStr + ")";

			long curEpisode = 0;
			boolean shutdownOccurred = false;
			do {

				/*
				 * Client identification
				 */

				do {
					mySocket.send(initStr);
					inMsg = mySocket.receive(UDP_TIMEOUT);
				} while (inMsg == null || inMsg.indexOf("***identified***") < 0);

				/*
				 * Start to drive
				 */
				long currStep = 0;
				while (true) {
					/*
					 * Receives from TORCS the game state
					 */
					inMsg = mySocket.receive(UDP_TIMEOUT);

					if (inMsg != null) {

						/*
						 * Check if race is ended (shutdown)
						 */
						if (inMsg.indexOf("***shutdown***") >= 0) {
							shutdownOccurred = true;
							System.out.println("Server shutdown!");
							break;
						}

						/*
						 * Check if race is restarted
						 */
						if (inMsg.indexOf("***restart***") >= 0) {
							driver.reset();
							if (verbose)
								System.out.println("Server restarting!");
							break;
						}

						Action action = new Action();
						if (currStep < maxSteps || maxSteps == 0)
							action = driver.control(new MessageBasedSensorModel(inMsg));
						else
							action.restartRace = true;

						currStep++;
						mySocket.send(action.toString());
					} else
						System.out.println("Server did not respond within the timeout");
				}

			} while (++curEpisode < maxEpisodes && !shutdownOccurred);

			/*
			 * Shutdown the controller
			 */
			driver.shutdown();
			mySocket.close();
			System.out.println("Client shutdown.");
			System.out.println("Bye, bye!");
			//}
		}
	}

	private static void parseParameters(String[] args) {
		/*
		 * Set default values for the options
		 */
		port = 3001;
		host = "localhost";
		clientId = "championship2010";
		verbose = false;
		maxEpisodes = 1;
		maxSteps = 0;
		stage = Stage.UNKNOWN;
		trackName = "unknown";

		for (int i = 1; i < args.length; i++) {
			StringTokenizer st = new StringTokenizer(args[i], ":");
			String entity = st.nextToken();
			String value = st.nextToken();
			if (entity.equals("port")) {
				port = Integer.parseInt(value);
			}
			if (entity.equals("host")) {
				host = value;
			}
			if (entity.equals("id")) {
				clientId = value;
			}
			if (entity.equals("verbose")) {
				if (value.equals("on"))
					verbose = true;
				else if (value.equals(false))
					verbose = false;
				else {
					System.out.println(entity + ":" + value
							+ " is not a valid option");
					System.exit(0);
				}
			}
			if (entity.equals("id")) {
				clientId = value;
			}
			if (entity.equals("stage")) {
				stage = Stage.fromInt(Integer.parseInt(value));
			}
			if (entity.equals("trackName")) {
				trackName = value;
			}
			if (entity.equals("maxEpisodes")) {
				maxEpisodes = Integer.parseInt(value);
				if (maxEpisodes <= 0) {
					System.out.println(entity + ":" + value
							+ " is not a valid option");
					System.exit(0);
				}
			}
			if (entity.equals("maxSteps")) {
				maxSteps = Integer.parseInt(value);
				if (maxSteps < 0) {
					System.out.println(entity + ":" + value
							+ " is not a valid option");
					System.exit(0);
				}
			}
		}
	}

	private static Controller load(String name, File weights, String write_to) 
	{
		NNDriver Driver = new NNDriver(weights, true);
		Controller controller = (Controller) (Object) Driver;  //Only going to run it with this class so no need for general case
		return controller;
	}

	//Taken from http://www.programmersheaven.com/mb/java_beginners/360618/360618/how-to-make-java-wait/
	//Passes n seconds
	private static void wait (int n)
	{
		long t0, t1;
		t0 =  System.currentTimeMillis();
		do
		{
			t1 = System.currentTimeMillis();
		}
		while ((t1 - t0) < (n * 1000));
	}
}
