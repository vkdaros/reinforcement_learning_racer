//Editedfrom http://www.roseindia.net/xml/dom/DOMElements.shtml by Neil Clarke for TORCS project
// Goes through the XML files in the practice directory and finds the best car out of all the tests, used for cross validation

package champ2010client;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class XMLExtractor 
{
	boolean calculate(File xmlfile)
	{
		int lapsran = 3; //How many laps were the cars tested on, easier to manually put it here
		double max_damage = 100; //Max damage a car can take before being considered a failure, cars with opponenets will need more, use 100 for no cars

		double total_time = 0.0;
		double total_damage = 0.0;
		int lapcounter = 0;

		// Create a factory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		// Use the factory to create a builder
		DocumentBuilder builder;
		try 
		{
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlfile);

			// Get a list of all elements in the document
			NodeList list = doc.getElementsByTagName("*");
			for (int i=0; i<list.getLength(); i++)
			{
				// Get element
				Element element = (Element)list.item(i);
				if(element.getAttribute("name").equals("time"))
				{
					//System.out.println(element.getAttribute("val"));
					lapcounter++;
					total_time += Double.parseDouble(element.getAttribute("val"));
				}
				if(element.getAttribute("name").equals("dammages")) //Its spelt incorrectly in the XML files
				{
					total_damage = Double.parseDouble(element.getAttribute("val")); //Just need to take the last one
				}
			}
			System.out.println("\nTotal time = "+total_time+" Total Damage = "+total_damage+" Laps Completed = "+lapcounter+"\n");
		}
		catch (SAXException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		catch (ParserConfigurationException e1) 
		{
			e1.printStackTrace();
		}
		
		if(lapcounter == lapsran && total_damage < max_damage)
			return true;
		else
			return false;
	}	
}