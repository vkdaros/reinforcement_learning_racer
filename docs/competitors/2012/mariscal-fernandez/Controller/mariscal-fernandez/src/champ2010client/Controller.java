package champ2010client;

/**
 * @author   samsung
 */
public abstract class Controller {

	/**
	 * @author   samsung
	 */
	public enum Stage {
		
		/**
		 * @uml.property  name="wARMUP"
		 * @uml.associationEnd  
		 */
		WARMUP,/**
		 * @uml.property  name="qUALIFYING"
		 * @uml.associationEnd  
		 */
		QUALIFYING,/**
		 * @uml.property  name="rACE"
		 * @uml.associationEnd  
		 */
		RACE,/**
		 * @uml.property  name="uNKNOWN"
		 * @uml.associationEnd  
		 */
		UNKNOWN;
		
		static Stage fromInt(int value)
		{
			switch (value) {
			case 0:
				return WARMUP;
			case 1:
				return QUALIFYING;
			case 2:
				return RACE;
			default:
				return UNKNOWN;
			}			
		}
	};
	
	/**
	 * @uml.property  name="stage"
	 * @uml.associationEnd  
	 */
	private Stage stage;
	/**
	 * @uml.property  name="trackName"
	 */
	private String trackName;
	
	public float[] initAngles()	{
		float[] angles = new float[19];
		for (int i = 0; i < 19; ++i)
			angles[i]=-90+i*10;
		return angles;
	}
	
	/**
	 * @return
	 * @uml.property  name="stage"
	 */
	public Stage getStage() {
		return stage;
	}
		
	/**
	 * @param  stage
	 * @uml.property  name="stage"
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	/**
	 * @return
	 * @uml.property  name="trackName"
	 */
	public String getTrackName() {
		return trackName;
	}

	/**
	 * @param  trackName
	 * @uml.property  name="trackName"
	 */
	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

    public abstract Action control(SensorModel sensors);

    public abstract void reset(); // called at the beginning of each new trial
    
    public abstract void shutdown();
    
    public abstract void loadFiles();

}