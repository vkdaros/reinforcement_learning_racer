package champ2010client;

/**
 * @author   David Mariscal Fernández
 */
public class Curva extends Reconocimiento {
	/**
	 * @uml.property  name="posicion"
	 */
	private double posicion;
	/**
	 * @uml.property  name="direccion"
	 */
	private String direccion;
	
	public Curva(double punto, String giro){
		posicion = punto;
		direccion = giro;
	}
	
	/**
	 * @return
	 * @uml.property  name="posicion"
	 */
	public double getPosicion(){
		return posicion;
	}
	
	/**
	 * @return
	 * @uml.property  name="direccion"
	 */
	public String getDireccion(){
		return direccion;
	}
}
