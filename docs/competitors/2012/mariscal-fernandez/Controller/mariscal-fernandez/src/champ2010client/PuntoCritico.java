package champ2010client;

/**
 * @author  David Mariscal Fernández
 */
public class PuntoCritico extends Reconocimiento {
	/**
	 * @uml.property  name="punto"
	 */
	private double punto;
	/**
	 * @uml.property  name="gravedad"
	 */
	private int gravedad;
	private double ultimaRevision;
	
	public PuntoCritico(double distancia,double distTotal){
		punto = distancia;
		gravedad = 1;
		ultimaRevision = distTotal;
	}
	public PuntoCritico(double distancia, int grav){
		punto = distancia;
		gravedad = grav;
	}
	
	public double calcular(){
		return 0.25 - (gravedad*0.25);
	}
	
	public void incrementarGravedad(double punto){
		if(Math.abs(punto - ultimaRevision) > 300){
			ultimaRevision = punto;
			if(gravedad<5){
				gravedad++;
			}
		}
	}
	/**
	 * @return
	 * @uml.property  name="punto"
	 */
	public double getPunto(){
		return punto;
	}
	
	/**
	 * @return
	 * @uml.property  name="gravedad"
	 */
	public int getGravedad(){
		return gravedad;
	}
	
	/**
	 * @param  punto
	 * @uml.property  name="ultimaRevision"
	 */
	public void setUltimaRevision (double punto){
		ultimaRevision = punto;
	}
}
