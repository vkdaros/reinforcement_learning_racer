package champ2010client;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Reconocimiento {
	private List<PuntoCritico> lista;
	private List<Curva> listaCurvas;
	private int indice;
	private int indiceCurva;
	public boolean isDirty = false;
	public boolean isOval = false;
	
	public Reconocimiento(){
		lista = new LinkedList<PuntoCritico>();
		listaCurvas = new LinkedList<Curva>();
	}
	
	public void añadirCurva(double punto, String direccion){
		if(!curvaAñadida(punto)){
			listaCurvas.add(new Curva(punto,direccion));
		}
	}
	
	public boolean curvaAñadida(double punto){
		boolean encontrado = false;
		int i=0;
		while(!encontrado && i<listaCurvas.size()){
			if(Math.abs( listaCurvas.get(i).getPosicion() - punto)<=300){
				encontrado = true;
			}
			i++;
		}
		return encontrado;
	}
	
	public boolean curvaCerca(double punto){
		boolean encontrado = false;
		int i=0;
		while(!encontrado && i<listaCurvas.size()){
			//if(Math.abs( lista.get(i) - punto)<=50){
			if( ((listaCurvas.get(i).getPosicion() - punto)>=0) 
					&& ((listaCurvas.get(i).getPosicion() - punto)<=300)){
				encontrado = true;
				indiceCurva = i;
			}
			i++;
		}
		return encontrado;
	}
	
	public String direccionCurva(){
		return listaCurvas.get(indiceCurva).getDireccion();
	}
	
	public void añadirPunto(double punto, double distTotal){
		if(!alrededor(punto,distTotal)){
			lista.add(new PuntoCritico (punto,distTotal));
		}
	}
	
	private boolean alrededor(double punto,double distTotal){
		boolean encontrado = false;
		int i=0;
		while(!encontrado && i<lista.size()){
			if(Math.abs( lista.get(i).getPunto() - punto)<=200){
				encontrado = true;
				lista.get(i).incrementarGravedad(distTotal);
			}
			i++;
		}
		return encontrado;
	}
	public boolean cerca(double punto){
		boolean encontrado = false;
		int i=0;
		while(!encontrado && i<lista.size()){
			//if(Math.abs( lista.get(i) - punto)<=50){
			if( ((lista.get(i).getPunto() - punto)>=0) 
					&& ((lista.get(i).getPunto() - punto)<=200)){
				encontrado = true;
				indice = i;
			}
			i++;
		}
		return encontrado;
	}
	
	public int getGravedad(){
		return lista.get(indice).getGravedad();
	}
	
	public void escribir(String track,float maxStraight,double distanceFirstChange, int numberOfGearChanges){
		PrintWriter pw = null;
		try {
			pw = new PrintWriter("puntosCriticos"+track+".txt");
			int i=0;
		//	pw.println("****PUNTOS CRÍTICOS DEL CIRCUITO****");
			while(i<lista.size()){
				pw.println(lista.get(i).getPunto() + "\t"+ lista.get(i).getGravedad());
				i++;
			}
			pw.close();
			pw = new PrintWriter("curvas"+track+".txt");
			i=0;
		//	pw.println("****CURVAS TRAS LARGAS RECTAS****");
			if(maxStraight >=15 && checkCornersSameDirection()){
				pw.println("Track" + "\t"+ "OVAL");
			}else if(distanceFirstChange<10 && numberOfGearChanges>3){
				pw.println("Track" + "\t"+ "DIRT");
			}else{
				pw.println("Track" + "\t"+ "ROAD");
			}
			while(i<listaCurvas.size()){
				pw.println(listaCurvas.get(i).getPosicion() + "\t"+ listaCurvas.get(i).getDireccion());
				i++;
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void cargarDesdeArchivo(String track) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader("puntosCriticos"+track+".txt"));
		
		String linea = br.readLine();
		double distancia;
		int gravedad;
		while(linea!=null){
			StringTokenizer st=new StringTokenizer(linea,"\t");
			while (st.hasMoreTokens()){
				distancia = Double.parseDouble(st.nextToken());
				gravedad = Integer.parseInt(st.nextToken());
				lista.add(new PuntoCritico(distancia,gravedad));
			}
			linea=br.readLine();
		}
		
		br.close();
		
		br = new BufferedReader(new FileReader("curvas"+track+".txt"));
		linea = br.readLine();
		Double punto;
		String direccion;
		String trackData;

		StringTokenizer st=new StringTokenizer(linea,"\t");
		trackData = st.nextToken();
		trackData = st.nextToken();
		if(trackData.equals("OVAL")){
			isOval = true;
		}else if (trackData.equals("DIRT")){
			isDirty = true;			
		}
		
		linea = br.readLine();
		while(linea!=null){
			st=new StringTokenizer(linea,"\t");
			while (st.hasMoreTokens()){
				punto = Double.parseDouble(st.nextToken());
				direccion = st.nextToken();
				listaCurvas.add(new Curva(punto,direccion));
			}
			linea=br.readLine();
		}
		br.close();
		
	}
	public boolean checkCornersSameDirection(){

		if(listaCurvas.size()<1) return false;
		else{
			String firstCorner = listaCurvas.get(0).getDireccion();
			boolean check = true;
			int i=1;
			while(check && i<listaCurvas.size()){
				if(listaCurvas.get(i).getDireccion().equals(firstCorner)){
					i++;
				}else{
					check = false;
				}
			}
			return check;
			
			
		}
		
		
	}
}
