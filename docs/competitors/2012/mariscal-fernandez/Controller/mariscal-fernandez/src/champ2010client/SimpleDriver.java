package champ2010client;

import java.io.IOException;

/**
 * @author   David Mariscal Fern�ndez
 */
public class SimpleDriver extends Controller{
	/* Gear changing constants */
	int[]  gearUp = {9500, 9400, 9400, 9500, 9500, 0};
	int[]  gearDown =  {0, 3300, 6200, 7000, 7300, 7700};
	double distanciaRecorrida = 0;
	double da�os=0;
	/* Stuck constants*/
	final int  stuckTime = 25;
	final float  stuckAngle = (float) 1.047;//PI/3  0.523598775; //PI/6

	/* Accel and Brake Constants*/
	final double[] acelAgresiva = {0.509220026549807,-0.1751054305482177,0.14710186945701376};
	final double[] acelSuave = {0.2813510482145047,-0.10571881061225102,0.07803871128297106};
	final double[] acelTierra = {0.9926550313826121,-0.4729012174328087,0.9599799679184671};

	final float maxSpeedDist=100;
	final float maxSpeed=280;
	final float sin5 = (float) 0.08716;
	final float cos5 = (float) 0.99619;
	float [] accelParam;

	/* Steering constants*/
	final float steerLock=(float) 0.785398;
	final float steerSensitivityOffset=(float) 80.0;
	final float wheelSensitivityCoeff=1;
	float [] steeringParam;

	/* ABS Filter Constants */
	final float wheelRadius[]={(float) 0.3179,(float) 0.3179,(float) 0.3276,(float) 0.3276};
	final float absSlip=(float) 2.0;
	final float absRange=(float) 3.0;
	final float absMinSpeed=(float) 3.0;
	
	/* Clutching Constants */
	final float clutchMax=(float) 0.5;
	final float clutchDelta=(float) 0.05;
	final float clutchRange=(float) 0.82;
	final float	clutchDeltaTime=(float) 0.02;
	final float clutchDeltaRaced=10;
	final float clutchDec=(float) 0.01;
	final float clutchMaxModifier=(float) 1.3;
	final float clutchMaxTime=(float) 1.5;
	
	/* Parametros para oponentes */
	double distanciaAnterior = 0;
	double distanciaActual = 0;
	boolean alerta = false;
	double distanciaAlerta = -0.3; //Distancia recorrida en 20 ms a 54km/h
	double puntoAlerta = 100;
	int indiceAlerta = 0;
	int contador = 0;
	int contadorAlerta = 0;
	int enAlerta = 20;
	double lastOvertakePlace=-1000;
	
	/* Parametros para reconocer el circuito */
	int enRectaLarga = 50; //Equivalen a 2 segundos
	int contadorRecta = 0;
	int enCurva = 50;
	int contadorCurvaDcha = 0;
	int contadorCurvaIzqda = 0;
	boolean esperandoCurva = false;
	
	/* Rebufos */
	boolean enRebufo = false;
	double ultimoPuntoRebufo = -100;
	double distanciaRebufoActual = 0;
	double distanciaRebufoAnterior = 0;
	int contadorRebufo = 0;
	int ticsRebufo = 25;

	private int stuck=0;
	int ACCEL_PARAM = 3;
	int STEERING_PARAM = 19;
	
	// current clutch
	private float clutch=0;
	
	/**
	 * @uml.property  name="reconocimiento"
	 * @uml.associationEnd  
	 */
	private Reconocimiento reconocimiento;
	//boolean practica = false;
//	boolean tierra = false;
//	boolean oval = false;
	
	float biggestStraight  = 0;
	float actualStraight = 0;
	boolean decided = false;
	float OVAL_STRAIGHT = 15.0f;
	
	boolean firstGearChange = false;
	double distanceFirstChange = 0.0f;
	
	int lastGear = 0;
	int numberOfGearChanges = 0;
	
	public SimpleDriver() throws IOException{
		reconocimiento = new Reconocimiento();

	}
	
	public void loadFiles(){
		if(getStage().equals(Stage.QUALIFYING)|| getStage().equals(Stage.RACE)){
			try {
				reconocimiento.cargarDesdeArchivo(getTrackName());
				if(reconocimiento.isDirty){
					gearUp[0] = 8500; gearUp[1] = 9000;gearUp[2] = 9400;
					gearUp[3] =9500 ; gearUp[4] = 9500 ; gearUp[5] = 0;
					
					gearDown[0] = 0; gearDown[1] = 3300;gearDown[2] = 6200;
					gearDown[3] =7000 ; gearDown[4] = 7300 ; gearDown[5] = 7700;

				}
			} catch (IOException e) {

			}
		}
	}
	public void reset() {
		System.out.println("Restarting the race!");
		
	}

	public void shutdown() {
		if(getStage().equals(Stage.WARMUP)){
			reconocimiento.escribir(getTrackName(),biggestStraight,distanceFirstChange,numberOfGearChanges);
		}	
	}

	
	private int getGear(SensorModel sensors){
	    int gear = sensors.getGear();
	    double rpm  = sensors.getRPM();
	    if(!firstGearChange && gear!=1 && gear!=0){
	    	distanceFirstChange = sensors.getDistanceRaced();
	    	firstGearChange = true;
	    	
	    }
	    if(sensors.getDistanceRaced()<20 && lastGear!=gear){
	    	numberOfGearChanges ++;
	    }
	    lastGear = gear;
	    // if gear is 0 (N) or -1 (R) just return 1 
	    if (gear<1)
	        return 1;
	    // check if the RPM value of car is greater than the one suggested 
	    // to shift up the gear from the current one     
	    if (gear <6 && rpm >= gearUp[gear-1])
	        return gear + 1;
	    else
	    	// check if the RPM value of car is lower than the one suggested 
	    	// to shift down the gear from the current one
	        if (gear > 1 && rpm <= gearDown[gear-1])
	            return gear - 1;
	        else // otherwhise keep current gear
	            return gear;
	}

	private float getSteer(SensorModel sensors){
		double[] s = sensors.getTrackEdgeSensors();
		double[] op = sensors.getOpponentSensors();
		double trackPos = sensors.getTrackPosition();
		float valor=0;

			double max=-2.0;
			int indice=0;
			for(int i=0;i<s.length;i++){
				if(s[i]>max){
					max = s[i];
					indice = i;
				}
			}
			
			/** DIRECCION PREFERIDA DE GIRO **/
			float angulo = sensor2Angle(indice);
			valor = Math.abs(angulo)/90;
			if (indice>9) valor*=-1; //Turn right
			
			/** SISTEMA DE RECONOCIMIENTO **/
			if (getStage().equals(Stage.WARMUP)){
				if(s[9]>80 && !esperandoCurva){
					contadorRecta++;
				}else if (s[9]<=80){
					contadorRecta = 0;
				}
				
				if(contadorRecta > enRectaLarga){
					esperandoCurva = true;
					contadorRecta = 0;
				}
				
				if(esperandoCurva){
					if(valor < 0){
						contadorCurvaDcha++;
						contadorCurvaIzqda = 0;
					}else if(valor>0){
						contadorCurvaIzqda++;
						contadorCurvaDcha=0;
					}
				}
				if(contadorCurvaDcha > enCurva){
					reconocimiento.a�adirCurva(sensors.getDistanceFromStartLine(), "DERECHA");
					esperandoCurva = false;
					contadorCurvaDcha = 0;
					contadorCurvaIzqda = 0;
				}else if (contadorCurvaIzqda > enCurva){
					reconocimiento.a�adirCurva(sensors.getDistanceFromStartLine(), "IZQUIERDA");
					esperandoCurva = false;
					contadorCurvaDcha = 0;
					contadorCurvaIzqda = 0;
				}
			}

						
			/** FUERA DEL CIRCUITO **/
			if (max==-1){
				if(trackPos>1){
					return -0.5f;
				}else if (trackPos<-1){
					return 0.5f;
				}
			}
			
			/** SISTEMA DE ADELANTAMIENTOS **/
			if(s[9]>80 && !enRebufo){
				int indiceOpMasCercano=0;
				double giro = 0;
				double minOp = 201.0;
				boolean oponenteCerca = false;
				//indiceOpMasCercano = 17;
				for(int i=16;i<20;i++){
					if(op[i]<minOp){
						minOp = op[i];
						indiceOpMasCercano = i;
					}
				}
				oponenteCerca = minOp<=25;


				if(oponenteCerca){
					double o17 = op[17];
					double o18 = op[18];
					double o19 = op[19];
				
					if(indiceOpMasCercano <= 17 && o18>20 && o19 > 20){
						if(trackPos > -0.8){
							if(!reconocimiento.isOval){
								giro = -0.3;
							}else{
								giro = -0.1;
							}
						}else{
							//Abortar adelantamiento, no es adecuado
							//giro = 0.5;
						}
						lastOvertakePlace = sensors.getDistanceRaced();
					}else if(indiceOpMasCercano == 18 && o17>20 && o19 > 20){
						if(trackPos > 0){
							if(!reconocimiento.isOval){
								giro = -0.3;
							}else{
								giro = -0.1;
							}
							lastOvertakePlace = sensors.getDistanceRaced();
						}else{
							if(!reconocimiento.isOval){
								giro = 0.3;
							}else{
								giro = 0.1;
							}
							lastOvertakePlace = sensors.getDistanceRaced();
						}
						
					}else if(indiceOpMasCercano >= 19 && o17 >20 && o18 > 20){
						if(trackPos < 0.8){
							if(!reconocimiento.isOval){
								giro = 0.3;
							}else{
								giro = 0.1;
							}
						}else{
							//Abortar adelantamiento, no es adecuado
							//giro = -0.5;
						}
						lastOvertakePlace = sensors.getDistanceRaced();
					}else{
						giro = 0;
					}
					
					valor = (float) giro;
				}
			}
			
			/** REBUFOS **/
			boolean oponenteRebufo = false;
			int indiceRebufo=18;
			if(s[9]>100 && sensors.getDistanceRaced()>800){
				double minOp = 201.0;
				//indiceOpMasCercano = 17;
				for(int i=17;i<20;i++){
					if(op[i]<minOp){
						minOp = op[i];
						indiceRebufo = i;
					}
				}
				distanciaRebufoAnterior = distanciaRebufoActual;
				distanciaRebufoActual = minOp;
				oponenteRebufo = (minOp>=15 && minOp<=120);
				System.out.println("*********Contador = " + contadorRebufo);
				System.out.println("*********Distancia menor = " + distanciaRebufoActual);
				System.out.println("*********Oponente en rebufo? = " + oponenteRebufo);
				System.out.println("*********Oponente en rebufo? = " + oponenteRebufo);
			}
			
			if(oponenteRebufo){
				contadorRebufo++;
			}else{
				contadorRebufo = 0;
				enRebufo = false;
			}
			
			if(alerta){
				enRebufo = false;
				contadorRebufo = 0;
			}else{
				if (contadorRebufo > ticsRebufo){
					enRebufo = true;
				}
			}

			if(enRebufo && s[9]> 100){
				ultimoPuntoRebufo = sensors.getDistanceRaced();
				valor =(float)(-1.0*((indiceRebufo - 18.0) / 9.0));
				if (valor > 0 && s[8]>40){
					return valor;
				}else if (valor < 0 && s[10]>40){
					return valor;
				}else if(valor != 0.0f){
					enRebufo = false;
					contadorRebufo = 0;
				}else{
					return 0.0f;
				}
			}else if(enRebufo && s[9]<= 100){
				enRebufo = false;
				contadorRebufo = 0;
			}
			
			
			
			/** APLICAR CONOCIMIENTO DEL CIRCUITO **/
			if(s[9]>80 && !reconocimiento.isDirty && !reconocimiento.isOval){
				if(reconocimiento.curvaCerca(sensors.getDistanceFromStartLine())){
					if(reconocimiento.direccionCurva().equals("IZQUIERDA")){
						if(trackPos > -0.7 && (sensors.getDistanceRaced()- lastOvertakePlace > 50) ){
							return -0.01f;
						}else{
							return 0;
						}
					}else if(reconocimiento.direccionCurva().equals("DERECHA")){
						if(trackPos < 0.7 && (sensors.getDistanceRaced()- lastOvertakePlace > 50)){
							return 0.01f;
						}else{
							return 0;
						}
					}
				}
			}

			return valor;


	}


	private float sensor2Angle(int indice) {
		float angulo = 0;
		if(indice==9){
			angulo = 0;
		}else if(indice>=0 && indice<5){
			angulo = -90+indice*15;
		}else if(indice>=5 && indice<9){
			angulo =-20+(indice-5)*5;
		}else if(indice>=10 && indice<=13){
			angulo = 20-(indice-5)*5;
		}else if (indice>=14 && indice<=18){
			angulo = 90-indice*15;
		}
		return angulo;
	}
	
	private float getAccel(SensorModel sensors)
	{
		double[] tempTrack, tempOpp;
		tempOpp = sensors.getOpponentSensors();
		tempTrack = sensors.getTrackEdgeSensors();
		double speed = sensors.getSpeed();
		
		double max=0.0;
		int indice=0;
		for(int i=0;i<tempTrack.length;i++){
			if(tempTrack[i]>max){
				max = tempTrack[i];
				indice = i;
			}
		}
		float valor;
		if(!reconocimiento.isDirty){
			valor = (float) ((float) (tempTrack[9]*acelAgresiva[0]) 
					+ (speed*acelAgresiva[1])
					+ (acelAgresiva[2]*(tempTrack[indice]-tempTrack[9])));
		}else{
			valor = (float) ((float) (tempTrack[9]*acelTierra[0]) 
					+ (speed*acelTierra[1])
					+ (acelTierra[2]*(tempTrack[indice]-tempTrack[9])));
			if(valor>=0.8){
				valor = 0.8f;
			}
		}

		/****** SISTEMA DE RECONOCIMIENTO **********/
		if (reconocimiento.cerca(sensors.getDistanceFromStartLine()) && reconocimiento.getGravedad()>1){

			valor = (float) ((float) (tempTrack[9]*acelSuave[0]) 
					+ (speed*acelSuave[1])
					+ (acelSuave[2]*(tempTrack[indice]-tempTrack[9])));
		}

		/** COMPORTAMIENTO TRAS SALIRSE DE PISTA **/
		
		if (Math.abs(sensors.getTrackPosition())>1){
			valor =0;
			if(getStage().equals(Stage.WARMUP)){
	    		reconocimiento.a�adirPunto(sensors.getDistanceFromStartLine()
	    			,sensors.getDistanceRaced());
			}
		} 
		if (speed<=20 && sensors.getDistanceRaced()>10) valor = 0.5f;
		

		/**Calcular si hay coches cercanos en +- 10 grados**/
		max = 201.0;
		for(int i=17;i<=19;i++){
			if(tempOpp[i]<max){
				max = tempOpp[i];
				puntoAlerta = max;
				indiceAlerta = i;
			}
		}
		distanciaAnterior = distanciaActual;
		distanciaActual = max;
		
		if(max < 15 && max > -1 &&sensors.getDistanceRaced()>8 &&!enRebufo){
			if(distanciaActual - distanciaAnterior <=0){
				if(contador < 35){
					valor = 0;
					contador++;
				}else{
					valor = -1;
				}
			}
		}else{
			contador = 0;
		}
		
		/** DETECTAR SITUACIONES DE ALERTA **/
		if((distanciaActual - distanciaAnterior < distanciaAlerta) && 
				sensors.getDistanceRaced()>200 && max<100){
			contadorAlerta++;
		}else{
			contadorAlerta=0;
		}
		
		if(contadorAlerta>enAlerta){
			alerta=true;
		}else{
			alerta = false;
		}

		//Si hay alerta y voy acelerando levantar el pie
		if(alerta && valor >=0 && max > 15) valor = 0;
		
		if(getStage().equals(Stage.WARMUP)){
			if(valor>=1.0f && !decided){
				actualStraight+=0.02;
				if(actualStraight > biggestStraight){
					biggestStraight = actualStraight;
				}
				if(biggestStraight > OVAL_STRAIGHT){
					decided = true;
				}
			}else{
				actualStraight = 0;
			}
		}
		return  valor;
	}
	

	

	public Action control(SensorModel sensors){
		
		distanciaRecorrida=sensors.getDistanceRaced();		
		da�os = sensors.getDamage();
		
		if ( Math.abs(sensors.getAngleToTrackAxis()) > stuckAngle )
	    {
			// update stuck counter
	        stuck++;
	    }
	    else
	    {
	    	// if not stuck reset stuck counter
	        stuck = 0;
	    }

		// after car is stuck for a while apply recovering policy
	    if (stuck > stuckTime)
	    {
	    	if(getStage().equals(Stage.WARMUP)){
	    		/******SISTEMA DE RECONOCIMIENTO**********/
	    		reconocimiento.a�adirPunto(sensors.getDistanceFromStartLine()
	    			,sensors.getDistanceRaced());
	    	}
	    	/* set gear and sterring command assuming car is 
	    	 * pointing in a direction out of track */
	    	
	    	// to bring car parallel to track axis
	        float steer = (float) (- sensors.getAngleToTrackAxis() / steerLock); 
	        int gear=-1; // gear R
	        
	        // if car is pointing in the correct direction revert gear and steer  
	        if (sensors.getAngleToTrackAxis()*sensors.getTrackPosition()>0)
	        {
	            gear = 1;
	            steer = -steer;
	        }
	        clutch = clutching(sensors, clutch);
	        // build a CarControl variable and return it
	        Action action = new Action ();
	        action.gear = gear;
	        action.steering = steer;
	        action.accelerate = 0.6;
	        action.brake = 0;
	        action.clutch = clutch;
	        return action;
	    }

	    else // car is not stuck
	    {
	    	// compute accel/brake command
	        float accel_and_brake = getAccel(sensors);
	        // compute gear 
	        int gear = getGear(sensors);
	        // compute steering
	        float steer = getSteer(sensors);
	        

	        // normalize steering
	        if (steer < -1)
	            steer = -1;
	        if (steer > 1)
	            steer = 1;
	        
	        // set accel and brake from the joint accel/brake command 
	        float accel,brake;
	        if (accel_and_brake>0)
	        {
	            accel = accel_and_brake;
	            brake = 0;
	        }
	        else
	        {
	            accel = 0;
	            // apply ABS to brake
	            brake = filterABS(sensors,-accel_and_brake);
	        }

	        clutch = clutching(sensors, clutch);
	        
	        // build a CarControl variable and return it
	        Action action = new Action ();
	        action.gear = gear;
	        action.steering = steer;
	        action.accelerate = accel;
	        action.brake = brake;
	        action.clutch = clutch;
	        return action;
	    }
	}

	private float filterABS(SensorModel sensors,float brake){
		// convert speed to m/s
		float speed = (float) (sensors.getSpeed() / 3.6);
		// when spedd lower than min speed for abs do nothing
	    if (speed < absMinSpeed)
	        return brake;
	    
	    // compute the speed of wheels in m/s
	    float slip = 0.0f;
	    for (int i = 0; i < 4; i++)
	    {
	        slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];
	    }

	    // slip is the difference between actual speed of car and average speed of wheels
	    slip = speed - slip/4.0f;	    

	    // when slip too high applu ABS
	    if (slip > absSlip)
	    {
	        brake = brake - (slip - absSlip)/absRange;
	    }
	    
	    // check brake is not negative, otherwise set it to zero
	    if (brake<0)
	    	return 0;
	    else
	    	return brake;
	}
	
	float clutching(SensorModel sensors, float clutch)
	{
	  	 
	  float maxClutch = clutchMax;

	  // Check if the current situation is the race start
	  if (sensors.getCurrentLapTime()<clutchDeltaTime  && getStage()==Stage.RACE && sensors.getDistanceRaced()<clutchDeltaRaced)
	    clutch = maxClutch;

	  // Adjust the current value of the clutch
	  if(clutch > 0)
	  {
	    double delta = clutchDelta;
	    if (sensors.getGear() < 2)
		{
	      // Apply a stronger clutch output when the gear is one and the race is just started
		  delta /= 2;
	      maxClutch *= clutchMaxModifier;
	      if (sensors.getCurrentLapTime() < clutchMaxTime)
	        clutch = maxClutch;
		}

	    // check clutch is not bigger than maximum values
		clutch = Math.min(maxClutch,clutch);

		// if clutch is not at max value decrease it quite quickly
		if (clutch!=maxClutch)
		{
		  clutch -= delta;
		  clutch = Math.max((float) 0.0,clutch);
		}
		// if clutch is at max value decrease it very slowly
		else
			clutch -= clutchDec;
	  }
	  return clutch;
	}
	
	public float[] initAngles()	{
		
		float[] angles = new float[19];

		/* set angles as {-90,-75,-60,-45,-30,-20,-15,-10,-5,0,5,10,15,20,30,45,60,75,90} */
		for (int i=0; i<5; i++)
		{
			angles[i]=-90+i*15;
			angles[18-i]=90-i*15;
		}

		for (int i=5; i<9; i++)
		{
				angles[i]=-20+(i-5)*5;
				angles[18-i]=20-(i-5)*5;
		}
		angles[9]=0;
		return angles;
	}
}
