Mariscal-Fernández Controller.

Source code is located at Controller/mariscal-fernandez/src/champ2010client

Compile with:
javac -d ../classes champ2010client/*.java

Launch with:
java champ2010client.Client champ2010client.SimpleDriver host:localhost port:3001 id:championship2010 maxEpisodes:1 maxSteps:0 verbose:on 