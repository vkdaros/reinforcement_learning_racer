source files are in src folder

car design layout in carDesign folder

to run bot you can either run included jar(Ready2WinController.jar) or compile source files


use foloowing arguments:


controller.Ready2WinController host:localhost port:<port number> id:<controller's id>
maxEpisodes:1 maxSteps:100000 trackName:<track name> stage:{0,1,2 where 0 means warmUp,1 qualification,2 race}


For example: 
"C:\Users\Parino>java -jar C:\Users\Parino\Documents\Ready2WinController.jar controller.Ready2WinController host:localhost port:3001 id:championship2011 maxEpisodes:1 maxSteps:100000 trackName:trackOle stage:0"

Our controller creates file with track data, which is saved to "working directory", it also saves telemetry files to directory Telemetry.