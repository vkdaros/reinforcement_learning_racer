package controller;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

import controller.model.Turn;
import controller.model.Turn.Direction;

import champ2011client.Action;
import champ2011client.SensorModel;




//usage
/*
 * double straightSensors[] = new double[] {trackEdgeSensors[1],trackEdgeSensors[2],trackEdgeSensors[9],trackEdgeSensors[16],trackEdgeSensors[17]};
 * firstLapSteer(SimpleNoisyRemover.average(straightSensors),sm.getTrackEdgeSensors[max],sm.getAngleToTrackAxis(),sm.getTrackPosition());
 */

public class DriveModul {

	//generally used variables
	//track width	
	private double trackWidth= 0;
	private ArrayList<Integer> straightSensorIndexes= new ArrayList<Integer>();
	
	//variables for the first lap, firstlapsteer
	private boolean straightMid =false; //determines, whether the vehicle is in the middle of the track and parallel with the axis
	
	private boolean rovinaNemeratZakruty = false; //determines, whether the vehicle is on a straight or in a turn
	
	private Deque<Integer> turnDirection = new ArrayDeque<Integer>();
	
	//variables for friction measurement
	private double friction;
	
	private double breakSpeed;
	//a place of the brake start
	private double brakeStartDist;
	//braking, flag representing that the vehicle is in a braking phase
	private boolean brzdi = false;
	//a flag representing that the vehicle is not moving and all requeired data is measured, so friction can be computed
	private boolean vypocitajFriction = false;
	//a flag representing that the wheels are locked during the braking
	private boolean kolesaStoja = false;
	//
	private boolean notStraightAfterFrictMeasuring=true;
	
	//driving variables
	//a duration during which the gear cannot be changed
	private int dontGear =0;
	//
	private double clutchCur=0;
	//
	private Deque<Double> rpm30Tick = new ArrayDeque<Double>();
	
	//-----variables used in launchAssistSteer-----
	//hold the wanted value of track position during launch
	private double launchTrackPos=-3d;
	//holds value of track position on start
	private double startingTrackPos=-3d;
	//index of the sensor that sees oponent
	//private double startAssistSensorLastIndex=-1;
	//tick counter used in launchAssistSteer, its value should be set to 20
	//private int launchAssistSteer=0;
	//private double lastOppPos=0.1;
	private double MIN_CAR_DISTANCE=1.96;
	private double ACCEL_ADJUSTMENT=0;
	private static double OK_POS_ADJUSTMENT=0.03;
	private static double WRONG_WAY_POS_ADJUSTMENT=0.06;
	//private static final double MAX_DIFF_RANGE = 0.02;
	
	//normal steer variables
	private double lastSteer=0;
	
	/**
	 * default constructor provides a speed by which it is possible to measeure friction to 125, straight indexes 1,2,16,17
	 */
	public DriveModul(){
		this.breakSpeed=125;
		friction=16;
		ArrayList<Integer> def= new ArrayList<Integer>();
		def.add(1);
		def.add(2);
		def.add(16);
		def.add(17);
		setStraightSensorIndexes(def);
		
	}
	/**
	 * a constructor setting brake speed for friction and allows setting indices of sensors that are parallel to the vehicle axis
	 * @param breakSpeed a speed after which the braking starts when friction is measured (it can start also soones, if the track doesnt allow reaching this speed in the finish line)
	 * @param StraightSensorIndexes collection containoing integers representing indices of sensors that are parallel to the vehicle axis
	 */
	public DriveModul(double breakSpeed,Collection<Integer> StraightSensorIndexes){
		this.breakSpeed=breakSpeed;
		friction=16;
		setStraightSensorIndexes(StraightSensorIndexes);
	}
	 /**
     * adjust the steer so that the driver goes on the nearest edge of the track, steering adjustment is modified by steer
     * @param sm
     * @param steeradjustmnet not implemented
     * @return steer
     */
    public double launchAssitSteer(SensorModel sm,double steeradjustmnet){
    	double steer;
    	double mypos=0;
    	if(startingTrackPos==-3d){
    		startingTrackPos=sm.getTrackPosition();
    		//System.out.println(startingTrackPos);
    	}
    	if(launchTrackPos==-3d){
    		
    		//launchTrackPos=(trackWidth/2-1.35)/(trackWidth/2);
    		/*if(trackWidth<=12){
    			//launchTrackPos=(trackWidth/2-0.98)/(trackWidth/2);
    			mypos=(trackWidth/2)*Math.abs(sm.getTrackPosition());
        		launchTrackPos=(mypos+0.1)/(trackWidth/2);
    		}
    		else if(trackWidth>=15){
    			mypos=(trackWidth/2)*Math.abs(sm.getTrackPosition());
        		launchTrackPos=(mypos+0.1)/(trackWidth/2);
    		}else{
    			mypos=(trackWidth/2)*Math.abs(sm.getTrackPosition());
        		launchTrackPos=(mypos+MIN_CAR_DISTANCE)/(trackWidth/2);
    		}*/
    		mypos=(trackWidth/2)*Math.abs(sm.getTrackPosition());
    		launchTrackPos=(mypos+0)/(trackWidth/2);
    		//System.out.println(trackWidth);
    		launchTrackPos=launchTrackPos*(Math.abs(sm.getTrackPosition())/sm.getTrackPosition());
    		//System.out.println("hm: "+launchTrackPos+" mypos: "+mypos+" trackpos: "+MIN_CAR_DISTANCE);
    	}
    	//computes opponent track, does not work properly
    	/*	launchAssistSteer--;
    	//&& isAngleToTrackAxisSmall(sm)
    	if((startAssistSensorLastIndex!=isOppInCloseArea(sm) || launchAssistSteer<0) ){
    		
    		//checking opponent's trackpos after 20 ticks
    		//launchAssistSteer=20;
    		if(isOppInCloseArea(sm)!=-1){
    			launchTrackPos=calculateNewTrackPos(sm,isOppInCloseArea(sm),startAssistSensorLastIndex);
    			startAssistSensorLastIndex=isOppInCloseArea(sm);
    			launchAssistSteer=1;
    			System.out.println("som v normalne: "+launchTrackPos);
    		}else
    			startAssistSensorLastIndex=-1;
    		//startAssistSensorLastIndex=isOppInCloseArea(sm);
    	}*/
    	//if(isOppInCloseArea(sm)==-1){
    	if(isOppInCloseArea(sm)!=-1){
    		launchTrackPos+=calculTrackPosAdjustment(sm,isOppInCloseArea(sm));
    		//System.out.println("launchPos: "+launchTrackPos+" cardistance: "+MIN_CAR_DISTANCE+" speedadjustment: "+ACCEL_ADJUSTMENT);
    	}
    	else if(isAngleToTrackAxisSmall(sm) ){
    		//System.out.println("18: "+sm.getOpponentSensors()[18]+" 17: "+sm.getOpponentSensors()[17]);
    		if(sm.getOpponentSensors()[18]>4 && sm.getOpponentSensors()[18]<16){
    			mypos=(trackWidth/2)*Math.abs(sm.getTrackPosition());
    			launchTrackPos=(mypos+1*Math.abs(sm.getTrackPosition())/sm.getTrackPosition())/(trackWidth/2);
    			//System.out.println("18: "+sm.getOpponentSensors()[18]);
    		}
    		if(sm.getOpponentSensors()[17]>4 && sm.getOpponentSensors()[17]<16){
    			mypos=(trackWidth/2)*Math.abs(sm.getTrackPosition());
    			launchTrackPos=(mypos-1*Math.abs(sm.getTrackPosition())/sm.getTrackPosition())/(trackWidth/2);
    			//System.out.println("17: "+sm.getOpponentSensors()[17]);
    		}
    	}
    	launchTrackPos=normalizeTrackPos(sm.getTrackPosition());
    	steer=(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-launchTrackPos)*(0.5+ACCEL_ADJUSTMENT))/0.366519;
    	
    	//}else{
    	//	steer = avoidCloseOppnent(sm,isOppInCloseArea(sm),sm.getTrackPosition());
    		//System.out.println("-------------------------------------------som v else avoid opponent: "+sm.getAngleToTrackAxis());
    	//}
    	
    	return steer;
    }
    /**
     * fixes value if the range is exceeded
     * @param trackPos
     * @return
     */
    private double normalizeTrackPos(double trackPos){
    	double trackWidthAdjustment=0;
    	if(trackWidth<=12)
    		trackWidthAdjustment=1;
    	else if(trackWidth<=10)
    		trackWidthAdjustment=1;
    	
    	if(trackPos>1-(0.98-trackWidthAdjustment)/(trackWidth/2))
    		return 1-(0.98-trackWidthAdjustment)/(trackWidth/2);
    	if(trackPos<-1+(0.98-trackWidthAdjustment)/(trackWidth/2))
    		return -1+(0.98-trackWidthAdjustment)/(trackWidth/2);
    	return launchTrackPos;
    }
    /**
     * if an angle of the axis is lower that 0.01 rad
     * @param sm
     * @return
     */
    private boolean isAngleToTrackAxisSmall(SensorModel sm){
    	return (sm.getAngleToTrackAxis()<0.01 && sm.getAngleToTrackAxis()>-0.01);
    }
    /**
     * determins wheather some oponent is in close area and returns index of sensor that sees him
     * @param sm
     * @return index of the senzor that show opponent in close area
     */
    private int isOppInCloseArea(SensorModel sm){
    	if(sm.getOpponentSensors()[9]<3)
    		return 9;
    	if(sm.getOpponentSensors()[12]<3)
    		return 12;
    	if(sm.getOpponentSensors()[14]<5)
    		return 14;
    	if(sm.getOpponentSensors()[15]<7)
    		return 15;
    	if(sm.getOpponentSensors()[16]<(14))
    		return 16;
        if(sm.getOpponentSensors()[17]<15)
        	return 17;
        if(sm.getOpponentSensors()[18]<15)
        	return 18;
        if(sm.getOpponentSensors()[19]<14)
        	return 19;
        if(sm.getOpponentSensors()[20]<7)
        	return 20;
        if(sm.getOpponentSensors()[21]<5)
        	return 21;
        if(sm.getOpponentSensors()[23]<3)
        	return 23;
        if(sm.getOpponentSensors()[26]<3)
        	return 26;
        return -1;
    }
    private double calculTrackPosAdjustment(SensorModel sm,int index){
    	double adjustment =0;
    	//System.out.print("sensor: "+sm.getOpponentSensors()[index]+" index: "+index+" ");
    	if(sm.getOpponentSensors()[index]>(MIN_CAR_DISTANCE/0.191) && (index ==17 || index ==18)){
    		adjustment=0;
    		ACCEL_ADJUSTMENT=0;
    	}else
    	//should be 16 or 19
    	if(sm.getOpponentSensors()[index]<(MIN_CAR_DISTANCE/0.191) && sm.getOpponentSensors()[index]>(MIN_CAR_DISTANCE/0.358) ){
    		if(index <=16 ){
    			adjustment=-OK_POS_ADJUSTMENT/3;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index >=19){
    			adjustment=OK_POS_ADJUSTMENT/3;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index == 17){
    			if(isSpaceNewRight(sm.getTrackPosition(),MIN_CAR_DISTANCE,0)){
    				adjustment=-OK_POS_ADJUSTMENT;
    				
    			}else
    				adjustment=WRONG_WAY_POS_ADJUSTMENT;
    			ACCEL_ADJUSTMENT=0.1;
    		}else if(index ==18){
    			if(isSpaceNewLeft(sm.getTrackPosition(),MIN_CAR_DISTANCE,0)){
					adjustment=OK_POS_ADJUSTMENT;
				}else
					adjustment=-WRONG_WAY_POS_ADJUSTMENT;
    			ACCEL_ADJUSTMENT=0.1;
    		}
    		
    	}
    	//should be 15 or 20
    	if(sm.getOpponentSensors()[index]<(MIN_CAR_DISTANCE/0.358) && sm.getOpponentSensors()[index]>(MIN_CAR_DISTANCE/0.515) ){
    		if(index <=15 ){
    			adjustment=-OK_POS_ADJUSTMENT/4;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index >=20){
    			adjustment=OK_POS_ADJUSTMENT/4;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index >15 && index <=17){
    			if(isSpaceNewRight(sm.getTrackPosition(),MIN_CAR_DISTANCE-0.77,0)){
    				adjustment=-OK_POS_ADJUSTMENT*Math.pow(2, index-15);
    			}else
    				adjustment=WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, index-15);
    			ACCEL_ADJUSTMENT=0.1*(index-15);
    		}else if(index <20 && index >=18){
    			if(isSpaceNewLeft(sm.getTrackPosition(),MIN_CAR_DISTANCE-0.77,0)){
					adjustment=OK_POS_ADJUSTMENT*Math.pow(2, 20-index);
				}else
					adjustment=-WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, 20-index);
    			ACCEL_ADJUSTMENT=0.1*(20-index);
    		}
    		
    	}
    	
    	if(sm.getOpponentSensors()[index]<(MIN_CAR_DISTANCE/0.515)  ){
    		if(index <=14){
    			adjustment=-OK_POS_ADJUSTMENT/5;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index >=21){
    			adjustment=OK_POS_ADJUSTMENT/5;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index >14 && index <=17){
    			if(isSpaceNewRight(sm.getTrackPosition(),MIN_CAR_DISTANCE-1.13,0)){
    				adjustment=-OK_POS_ADJUSTMENT*Math.pow(2, index-14);
    			}else
    				adjustment=WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, index-14);
    			ACCEL_ADJUSTMENT=0.1*(index-14);
    		}else if(index <21 && index >=18){
    			if(isSpaceNewLeft(sm.getTrackPosition(),MIN_CAR_DISTANCE-1.13,0)){
					adjustment=OK_POS_ADJUSTMENT*Math.pow(2, 21-index);
				}else
					adjustment=-WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, 21-index);
    			ACCEL_ADJUSTMENT=0.1*(21-index);
    		}
    	}
    	/*if(sm.getOpponentSensors()[index]<(MIN_CAR_DISTANCE/0.515)  ){
    		if(index <=14 || index >=21){
    			adjustment=0;
    			ACCEL_ADJUSTMENT=0;
    		}else if(index >14 && index <=17){
    			if(isSpaceNewRight(sm.getTrackPosition(),MIN_CAR_DISTANCE-1.13,0)){
    				adjustment=-OK_POS_ADJUSTMENT*Math.pow(2, index-15);
    			}else
    				adjustment=WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, index-15);
    		}else if(index <21 && index >=18){
    			if(isSpaceNewLeft(sm.getTrackPosition(),MIN_CAR_DISTANCE-1.13,0)){
					adjustment=OK_POS_ADJUSTMENT*Math.pow(2, 20-index);
				}else
					adjustment=-WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, 20-index);
    		}
    	}*/
    	if(sm.getOpponentSensors()[index]<(MIN_CAR_DISTANCE/0.875)  ){
    		if(index <=12 || index >=23){
    			adjustment=0;
    			
    		}else if(index >12 && index <=17){
    			if(isSpaceNewRight(sm.getTrackPosition(),MIN_CAR_DISTANCE-1.96,0.5)){
    				adjustment=-OK_POS_ADJUSTMENT*Math.pow(2, index-15);
    			}else
    				adjustment=WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, index-15);
    		}else if(index <23 && index >=18){
    			if(isSpaceNewLeft(sm.getTrackPosition(),MIN_CAR_DISTANCE-1.96,0.5)){
					adjustment=OK_POS_ADJUSTMENT*Math.pow(2, 20-index);
				}else
					adjustment=-WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, 20-index);
    		}
    		ACCEL_ADJUSTMENT=0;
    	}
    	if(sm.getOpponentSensors()[index]<(MIN_CAR_DISTANCE)  ){
    		if(index <=9 || index >=26){
    			adjustment=0;
    			
    		}else if(index >9 && index <=17){
    			if(isSpaceNewRight(sm.getTrackPosition(),MIN_CAR_DISTANCE-2.26,0.8)){
    				adjustment=-OK_POS_ADJUSTMENT*Math.pow(2, index-15);
    			}else
    				adjustment=WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, index-15);
    		}else if(index <26 && index >=18){
    			if(isSpaceNewLeft(sm.getTrackPosition(),MIN_CAR_DISTANCE-2.26,0.8)){
					adjustment=OK_POS_ADJUSTMENT*Math.pow(2, 20-index);
				}else
					adjustment=-WRONG_WAY_POS_ADJUSTMENT*Math.pow(2, 20-index);
    		}
    		ACCEL_ADJUSTMENT=0;
    	}
    		
    		
    		
    	return 	adjustment;
    }
    private boolean isSpaceNewRight(double pos,double width,double adjustment){
    	double trackWidthAdjustment=0;
    	if(trackWidth<=12)
    		trackWidthAdjustment=1;
    	else if(trackWidth<=10)
    		trackWidthAdjustment=1;
    	if(pos>0)
    		return true;
    	return ((trackWidth)/2)*Math.abs(pos)+width<((trackWidth)/2-0.98+trackWidthAdjustment+adjustment);
    }
    private boolean isSpaceNewLeft(double pos,double width,double adjustment){
    	double trackWidthAdjustment=0;
    	if(trackWidth<=12)
    		trackWidthAdjustment=1;
    	else if(trackWidth<=10)
    		trackWidthAdjustment=1.0;
    	if(pos<0)
    		return true;
    	return ((trackWidth)/2)*Math.abs(pos)+width<((trackWidth)/2-0.98+trackWidthAdjustment+adjustment);
    }
	/**
	 * a method for setting steering in the first lap, it is still being used in all laps
	 * also modifies global values, i.e. rovinaNemeratZakruty, turnDirection 	
	 *	
	 * @param trackEdgeSensorsStraight
	 * @param trackEdgeSensorsMax
	 * @param angleToTrackAxis
	 * @param trackPosition
	 * @param max index of the longest sensor
	 * @return steering
	 */
	public double firstLapSteer(double trackEdgeSensorsStraight,double trackEdgeSensorsMax,double angleToTrackAxis,double trackPosition,int max){
		//TODO zero queue of the direction after direction change
		
		double steer=0;
		//int max=0;
		//it is not necessary to find maximum, as we have it as a parameter
		/*for (int i = 1; i < trackEdgeSensors.length; i++)
			if (trackEdgeSensorsMax < trackEdgeSensors[i] && i!=1 && i!=2 && i!=16 && i!=17)
				max = i;*/
		int uhol= 0;	
		//boolean while creating model
		if(straightMid && (angleToTrackAxis<-0.001 || angleToTrackAxis>0.001)  && (trackPosition>0.01 || trackPosition<-0.01))
			rovinaNemeratZakruty=false;
		
		//System.out.println("lavo: "+sm.getTrackEdgeSensors()[8]+"stredny: "+sm.getTrackEdgeSensors()Straight+" pravo: "+sm.getTrackEdgeSensors()[10]);
		
		if(max<9  && ((trackEdgeSensorsMax-trackEdgeSensorsStraight)>trackWidth || trackEdgeSensorsMax<2*trackWidth || turnDirection.getFirst()!=0) ){
			uhol=1;
			
		}
		else if(max>9 && ((trackEdgeSensorsMax-trackEdgeSensorsStraight)>trackWidth || trackEdgeSensorsMax<2*trackWidth || turnDirection.getFirst()!=0) ){
			uhol=-1;
			
		}
		else{
			uhol =0;
		}
		//an angle int the previous tick multiplied by -1 so that it would be equal to the angle in the current tick, if zero is exceeded
		int uholLast=getTurnDirection(turnDirection)*-1;
		if(turnDirection.size()==5)
			turnDirection.pollFirst();
		turnDirection.addLast(uhol);
		uhol =getTurnDirection(turnDirection);
		//System.out.println(uhol);
		//a direction of the turn was changed without zero being registered
		/*if(Math.abs(lastSteer -steer)>MAX_DIFF_RANGE){
			rovinaNemeratZakruty=true;
			uhol=0;
		}*/
		if(uhol==uholLast){
			rovinaNemeratZakruty=true;
			uhol =0;
		}
		//registered direction straight line
		else if(uhol==0){
			rovinaNemeratZakruty=true;
			straightMid=false;
		}else if(uhol!=0)//I am in the turn and I set straightMid to true, so that the turn will be registered
			straightMid=true;
		//System.out.println("angle: "+uhol);
		//if(!rovinaNemeratZakruty)
			//System.out.println(sm.getAngleToTrackAxis());
		if(!rovinaNemeratZakruty && straightMid){
			//if(!(sm.getAngleToTrackAxis()>-0.001 && sm.getAngleToTrackAxis()<0.001)&& uhol !=0 ){
				
				steer=(steeringAdjustmentNew(trackEdgeSensorsStraight,(trackWidth),0)*(uhol) -(trackPosition*0.5)/0.366519);
				//System.out.println(sm.getTrackEdgeSensors()[1]+" "+sm.getTrackEdgeSensors()[2]+" "+sm.getTrackEdgeSensors()Straight);
			}else {
					steer=(angleToTrackAxis-trackPosition*0.3)/0.366519;
			}
		
		//start is parallel with an axis and on the middle of the track
		if (!straightMid && (angleToTrackAxis>-0.001 && angleToTrackAxis<0.001) && trackPosition<0.02 && trackPosition>-0.02)
			straightMid=true;
		else
			steer=(angleToTrackAxis-trackPosition*0.5)/0.366519;
		//nech nezaznamenava zakruty pokial nodmeral frikciu a neni na strede trate
		if(friction==16 || notStraightAfterFrictMeasuring){
			rovinaNemeratZakruty=true;
			if((angleToTrackAxis>-0.001 && angleToTrackAxis<0.001) && trackPosition<0.02 && trackPosition>-0.02)
				notStraightAfterFrictMeasuring=false;
			else
				steer=(angleToTrackAxis-trackPosition*0.3)/0.366519;
			//System.out.println("som tu");
		}
		lastSteer =steer;
		return steer;
	}
	private double countBreakSpeed(double speed,double straightSensor){
		//System.out.println(Util.countDistanceFromSpeed(speed, 19)+" hm "+straightSensor);
		if(Util.countDistanceFromSpeed(speed, 19)>straightSensor)
			return speed;
		return breakSpeed;
	}
	
	/**
	 * a method for friction computation from the brake track with all 4 wheels locked
	 * uses global variables: breakSpeed(125), breakStartDist, tri 
	 * uses flags: vypocitajFriction, brzdi, kolesaStoja
	 *  
	 * @param wheelSpinVelocity an array with wheel speeds
	 * @param speed current speed v
	 * @param distanceRaced distance covered
	 * @param action current action
	 * @return action modified according to the need of friction measurement
	 */
	public Action getFriction(SensorModel sm,double sensorStraight, Action action){
    	
    	
		if(sm.getSpeed()<=breakSpeed && !brzdi){
			action.accelerate=1;
			action.brake=0;
			brakeStartDist = sm.getDistanceRaced();
			breakSpeed = countBreakSpeed(sm.getSpeed(),sensorStraight);
			//System.out.println(brakeS);
		}else{
			action.steering=(sm.getAngleToTrackAxis()-sm.getTrackPosition()*0.5)/0.366519;
			double avgWheelSpeed =0;
			//System.out.print(zrychlenie(sensorsBefore.getSpeed(), sensors.getSpeed(), sensorsBefore.getCurrentLapTime(), sensors.getCurrentLapTime()));
			//System.out.print(" =||= ");
			for(int i=0;i<4;i++){
				avgWheelSpeed+=sm.getWheelSpinVelocity()[i]*0.3179;
				//System.out.print(" || "+zrychlenie(sensorsBefore.getWheelSpinVelocity()[i]*0.3179, sensors.getWheelSpinVelocity()[i]*0.3179, sensorsBefore.getCurrentLapTime(), sensors.getCurrentLapTime()));
			}
			avgWheelSpeed/=4;
			//System.out.println();
			//System.out.println("rychlost kolies: "+speed +" brakespeed "+breakSpeed+" brakedist "+brakeStartDist);
			
			if(avgWheelSpeed<0.05 && !kolesaStoja){
				brakeStartDist= sm.getDistanceRaced();
				kolesaStoja=true;
				breakSpeed=sm.getSpeed();
			}
			brzdi=true;
			action.accelerate=0;
			action.brake=1;
			action.clutch=0;
			if(sm.getSpeed()<0.01 && !vypocitajFriction){
				//System.out.println("drzdna draha"+(sensors.getDistanceRaced()-brakeStartDist)+ " rychlost kolies: "+speed +" brakespeed "+breakSpeed+" brakedist "+brakeStartDist+" friction "+(Math.pow(breakSpeed, 2)/(2*9.81*(sensors.getDistanceRaced()-brakeStartDist))));
				vypocitajFriction=true;
				//System.out.println();
				
				friction= (Math.pow(breakSpeed, 2)/(2*9.81*(sm.getDistanceRaced()-brakeStartDist)));
				//System.out.println("friction: "+friction+" distance raced: "+sm.getDistanceRaced());
			}
			
		}
		//friction= 16;
		return action;
	}
    /**
     * friction
     * @return friction
     */
    public double getFriction(){
    	return friction;
    }
    /**
     * a method of the driving moduel for determining gear from engine speed and its momentum
     * includes also line assist
	 * 
     * @param sensors vehicle sensors according to torcs 2010 specs
     * @param action performed action
     * @return the action modified by set gear and clutch, start of the race is handled by startassist
     */
    public Action getGear(SensorModel sensors,Action action){
		int gearCur;
		double rpm = sensors.getRPM(),lastRpm=9000;
		int[]  gearUp = {9500, 9500, 9500, 9500, 9500, 0};
		int[]  gearDown =  {0, 4300, 6200, 7000, 7300, 7700};
		gearCur=sensors.getGear();
		
		//updated RPM in last 30 ticks
		if(rpm30Tick.size()>0)
			lastRpm=rpm30Tick.getLast();
		if(rpm30Tick.size()==30 && dontGear==0)
			rpm30Tick.pollFirst();
		if(dontGear==0)
			rpm30Tick.addLast(rpm);
		else
			dontGear--;
		
		//on start and the race has not been started yet, push clutch, gear to 1, push gas
		if(gearCur<1){
			gearCur=1;
			action.gear=gearCur;
			action.accelerate= 1-ACCEL_ADJUSTMENT;
			clutchCur=0.9;
			//dontGear=8;
		}
		//the race has started, clutch is pushed, gear is 1, if RPM grows and are over 9000, then lower clutch till 0.05, if they drop under 7000, lower till 0.02, if they drop under 6500, increase clutch by 0.01
		//a condition clutchCur>0.67 and action clutchCur-=0.34;
		//TODO when asr is with custom slip, then change the codition with time and put there instead of acc 1 asr
		else if(((clutchCur>0.05 && gearCur==1 )|| sensors.getSpeed()<55) && sensors.getCurrentLapTime()<3.5 && sensors.getLastLapTime()==0){
			action.accelerate=1-ACCEL_ADJUSTMENT;
			//if((rpm-lastRpm)>=0 && rpm > 9300 && sensors.getSpeed()<40)
			//	clutchCur-=0.08;
			//else 
			if((rpm-lastRpm)>=0 && rpm > 9000)
				clutchCur-=0.05;
			else if((rpm-lastRpm)>=0 && rpm > 7000)
				clutchCur-=0.02;
			else if(rpm < 6500)
				clutchCur+=0.01;
			//System.out.println("I am here");
		}else if(clutchCur>0 && (rpm-lastRpm)>0 && rpm>7000 && gearCur==1 && rpm<9499){
			clutchCur-=0.03;
			//System.out.println("lol");
		}
		// the gear is lower than 6 and at the same time RPM is greater/equal to RPM when changing gear and RPM was increasing in the last 30 ticks => gear up
		else if (gearCur < 6 && rpm >= gearUp[gearCur-1] && dontGear==0)
		{	
			if(gearCur==1)
				clutchCur=0.63;
			if(isDequeGrowing(rpm30Tick)){
				dontGear=8;
				gearCur++;
			}
		}
		// the gear is higher than 1 and at the same time is RPM lower/equal ot the RPM to RPM when changing gear to lower and RPM is lowering in the last 30 ticks
		else if (gearCur > 1 && rpm <= gearDown[gearCur-1]  && dontGear==0 )
		{
			if(!isDequeGrowing(rpm30Tick)){
				dontGear=8;
				gearCur--;	
			}
		} 
			if (sensors.getSpeed()>78 && gearCur == 1){
				clutchCur=0.63;
				dontGear=8;
				gearCur++;
			}	
		//the gear is different than 1 and the clutch is pushed , if the gear is 2 and RPM is over 6500 then release the clutch, if other gear is set then release the clutch
		if((clutchCur>0 && gearCur>1 && (rpm-lastRpm)>0) || rpm>9000 && gearCur>1){
			if(gearCur==2 && rpm>7000)
				clutchCur-=0.01;
			else if(gearCur==2 && rpm>8500)
				clutchCur-=0.08;
			else if (gearCur!=2){
				clutchCur-=0.15;
				
			}
		}
		//System.out.println(rpm);
		if(sensors.getCurrentLapTime()<6 && sensors.getLastLapTime()==0)
			action.accelerate=1-ACCEL_ADJUSTMENT;
		action.gear=gearCur;
		action.clutch=clutchCur;
		return action;
	}
    private boolean isInFirstHalf(Turn turn,double distFromStart){
    	if((turn.getEndDistance()-turn.getStartDistance()/2)>distFromStart-turn.getStartDistance())
    		return true;
    	return false;
    }
    
    private int getDir(Turn turn){
    	if(turn.getDirection()==Direction.RIGHT)
    		return -1;
    	if(turn.getDirection()==Direction.LEFT)
    		return 1;
    	return 0;
    	
    }
    /**
	 * a method for driving after the first lap, based on the longest sensor
	 * @param sm
	 * @param aktTurn
	 * @return steering
	 */
    public double steerNormal(double[] trackEdgeSensors,double distanceFromStartLine,double angleToTrackAxis,double trackPosition,Turn aktTurn,double lastTurnEnd, boolean isTurnFront){
    	straightMid=true;
    	double steer=0;
		//double[] prekrytesenzory = prekriOpponentSenzorySTrackEdge(sm.getOpponentSensors(),sm.getTrackEdgeSensors(),25.0,sm);
		
		double uhol = -computeAvgLongestSensorAngle(trackEdgeSensors);
		double[]  maxSteers = {0,0.25, 0.38, 0.48,0.68,0.70,0.85,1,1,1 };
		int maxSteerIndex=(int)Math.abs(uhol)/10;
		//smer zakruty
		int dir = ((int) (Math.abs(uhol)/uhol));
		/*if(isInTurn(distanceFromStartLine,aktTurn)){
			dir=getDir(aktTurn);
			System.out.println("setujem dir "+dir);
		}*/
		//System.out.println("smer: "+dir+" laststeer: "+lastSteer+" ");
		//if(aktTurn.getStartDistance()<sm.getDistanceFromStartLine() && aktTurn.getEndDistance()>sm.getDistanceFromStartLine()){
			//System.out.println("smer: "+dir+" laststeer: "+lastSteer+" steer: "+steer+" uhol: "+uhol);
			if(maxSteers[maxSteerIndex]>Math.abs(lastSteer) ){
				steer=(Math.abs(lastSteer)+0.01*maxSteerIndex)*dir;
			}else 
				steer=(Math.abs(lastSteer)-0.01*maxSteerIndex)*dir;
		//}else if(aktTurn.getStartDistance()-sm.getDistanceFromStartLine()>200 && aktTurn.getPeakValue()>0.25 || aktTurn.getPeakValue()<0.25 && aktTurn.getStartDistance()-sm.getDistanceFromStartLine()>50)
		//	steer=(sm.getAngleToTrackAxis())/0.366519;
		//}else 
		//	steer=0;
		//System.out.println("smer: "+dir+" laststeer: "+lastSteer+" steer: "+steer+" uhol: "+uhol);
		if(lastSteer==0 && steer!=0 && isInTurn(distanceFromStartLine,aktTurn))
			steer=aktTurn.getPeakValue();
		//lol nevieme na co to je	
		if(steer == 0 && lastSteer == 0 && aktTurn.getStartDistance()>distanceFromStartLine && aktTurn.getStartDistance()-distanceFromStartLine>2*trackWidth){
			if(trackPosition>0.5)
				steer=(angleToTrackAxis-0.05)/0.366519;
			else if(trackPosition<-0.5)
				steer=(angleToTrackAxis+0.05)/0.366519;
			
			
		}
		if(uhol ==0 && lastSteer!=0){
			if(isInTurn(distanceFromStartLine,aktTurn) && isInFirstHalf(aktTurn,distanceFromStartLine))
				steer=(Math.abs(lastSteer)-0.005)*-(Math.abs(lastSteer)/lastSteer);
			else 
				steer=(Math.abs(lastSteer)-0.03)*-(Math.abs(lastSteer)/lastSteer);
			if(lastSteer<0.03 && lastSteer>-0.03)
				steer=0;
		}
			
		//driving according to the axis in the middle
		if(!isInTurn(distanceFromStartLine, aktTurn) && aktTurn.getStartDistance()-lastTurnEnd>50 && aktTurn.getEnterKm()>distanceFromStartLine && lastTurnEnd<distanceFromStartLine || lastTurnEnd>aktTurn.getStartDistance() || (!isTurnFront && aktTurn.getEndDistance()<distanceFromStartLine)){
			steer=tuneSteering((angleToTrackAxis-trackPosition*0.1)/0.366519);
			/*if(trackPosition<-0.8)
				steer=0.05;
			if(trackPosition>0.8)
				steer=-0.05;*/
			//System.out.println("middle >>>>>>>>>>>>>");
		}
		if(angleToTrackAxis>-0.01 && angleToTrackAxis<0.01 && ((trackPosition-0.5>0 && dir>0) || (trackPosition+0.5<0 && dir<0)))
			steer = lastSteer;
		
		/*if(Math.abs(angleToTrackAxis)<0.17 && aktTurn.getPeakValue()>0.2 && distanceFromStartLine<aktTurn.getEndDistance() && distanceFromStartLine>((aktTurn.getStartDistance()+(aktTurn.getEndDistance()-aktTurn.getStartDistance())/2))) {
			if((aktTurn.getDirection()==Direction.LEFT && trackPosition>0) || (aktTurn.getDirection()==Direction.RIGHT && trackPosition<0))
				{ 
				steer*=0.8;
				//System.out.println("vyjazd >>>>>>>>>>>>>  "+angleToTrackAxis);	
				}
			else if(Math.abs(trackPosition)<0.3) {
				steer*=0.8;
				//System.out.println("vyjazd >>>>>>>>>>>>> *08  "+angleToTrackAxis);	
			}
			else if(Math.abs(trackPosition)<0.4) {
				steer*=0.85;
				//System.out.println("vyjazd >>>>>>>>>>>>> *085  "+angleToTrackAxis);	
			}
			else if(Math.abs(trackPosition)<0.5) {
				steer*=0.9;
				//System.out.println("vyjazd >>>>>>>>>>>>> *09  "+angleToTrackAxis);	
			}
			else if(Math.abs(trackPosition)<0.7) {
				steer*=0.95;
				//System.out.println("vyjazd >>>>>>>>>>>>> *095  "+angleToTrackAxis);	
			}
			else if(Math.abs(trackPosition)>0.8) {
				steer*=1.05;
				//System.out.println("vyjazd >>>>>>>>>>>>> *105  "+angleToTrackAxis);	
			}
			//System.out.println("vyjazd >>>>>>>>>>>>>");
		}*/
		
		//if the vehicle is in more than 45 degree angle with the track axis, then radically steer in the turn direcion
		if(angleToTrackAxis>0.8722 && angleToTrackAxis<-0.8722 )
			steer=1*dir;
		
		lastSteer =steer;
		return steer;
		
	}
    
    /**
	 * steering tuning
	 * @param steer
	 * @return
	 */
	private double tuneSteering(double steer) {
		//ladenie trhaneho pohybu
		if((steer>0 && steer<0.01) || (steer<0 && steer>-0.01))
			return 0;
		if(steer>0.4)
			return 0.4;
		if(steer<-0.4)
			return -0.4;
		else
			return steer;
	}
    /**
     * track width setter
     * @param trackWidth
     */
    public void setTrackWidth(double trackWidth){
    	this.trackWidth=trackWidth;
    	//System.out.println("trackwidth: "+trackWidth);
    	if(trackWidth>=14){
    		MIN_CAR_DISTANCE+=0.4;
    		
    	}else if(trackWidth>=12){
    		MIN_CAR_DISTANCE+=0.3;
    	}else if(trackWidth>10)
    		MIN_CAR_DISTANCE+=0.2;
    	else{ 
    		MIN_CAR_DISTANCE+=0.1;
    	}
    }
	
	/**
	 * computes direction from the last que.size() ticks and determines the direction
	 * @param que integer queue
	 * @return 1,-1,0 - turn change
	 */
	private int getTurnDirection(Deque<Integer> que){
		Iterator<Integer> i = que.iterator();
		int aktValue=0;
		int zeroCount=0;
		int ret=0;
		while(i.hasNext()){
			aktValue= i.next();
			ret+=aktValue;
			if(aktValue==0)
				zeroCount++;
			
		}
		if(ret>0)
			return 1;
		else if(ret <0)
			return -1;
		else if(zeroCount>=4)
			return 0;
		else
			return aktValue;
	}
	/**
	 * Determines steering basd on the sensor length parallel with the track axis, normalises steering
	 * @param distance dlzka senzoru 
	 * @param trackWidth sirka trate
	 * @param strana
	 * @return
	 */
	private double steeringAdjustmentNew(double distance,double trackWidth,double strana){
		//TODO 1st two condtitions based on the track width might perform better if they are based on the track position
		
		distance=distance-(trackWidth-6);
		
		if(distance<42)
			distance= distance*1.05;
		if(distance<15)
			distance= distance*1.1052;
		
		
		if(distance>138)
			return 0.0075;
		if(distance>43){
			strana= (0.8409/(distance-26.9920))*(1+(strana));
			
		}else if(distance>15){
			strana= (2.2216/(distance-9.9210))*(0.975+(strana));
		}else if (distance >6.3545)
			strana= (3.8193/(distance-6.3542))*(1+(strana));
		else
			strana =1; 
		if(strana>1)
			strana=1;
		if(strana<-1)
			strana=-1;
		
			
		return Math.abs(strana);
	}
	/**
	 * a methos returns true if queue<double> is growing at least in one half, false if not
	 * 
	 * @param que queue of numbers on which it should be determined whether it is growing.
	 * @return true or false
	 */
	private boolean isDequeGrowing(Deque<Double> que){
		
		/*if(que.getLast()-que.getFirst()>0)
			return true;
		else
			return false;*/
		
		Deque<Double> pom = new ArrayDeque<Double>(que);
		boolean rastie[] = new boolean[pom.size()];
		double curValue = pom.pollFirst().doubleValue();
		
		for(int i=0;i<que.size()-1;i++){
			rastie [i]= curValue<pom.peekFirst().doubleValue();
			curValue = pom.pollFirst().doubleValue();
			
		}
		
		int h=0;
		for(int i=0;i<rastie.length;i++){
			if(rastie[i])
				h++;
		}
		
		if(h>=rastie.length/2)
			return true;
		else
			return false;
	}
	
	private boolean isInTurn(double distanceFromStartLine, Turn aktTurn){
		if(aktTurn!=null)
			return aktTurn.getEnterKm()<distanceFromStartLine && aktTurn.getEndDistance()>distanceFromStartLine;
		return false;
	}
	/**
	 * A method return an average of angles of sensors measuring the furthest edge of the track
	 * in the range <-90; 90>. Averaged is only one uninterrupted sequence of sensors.
	 * @param trackEdgeSensors track edge sensors
	 * @return an average of angles measuring the furthes edge of the track
	 */
	private double computeAvgLongestSensorAngle(double[] trackEdgeSensors)
	{
		int max = 0, numberOfLongestSensors = 1;
		double angle = 0;

		// finds index of the first sensor in the direction towards the furthest edge of the track
		for (int i = 1; i < trackEdgeSensors.length; i++)
			
			if (!isSensorIndexStraight(i) && trackEdgeSensors[max] < trackEdgeSensors[i])
					max = i;
		
		// determines how many sensors have the highest value
		while (max + numberOfLongestSensors < trackEdgeSensors.length &&
				trackEdgeSensors[max] == trackEdgeSensors[max + numberOfLongestSensors])
			numberOfLongestSensors++;

		// computes average, the result is in range <-90; 90>
		for (int i = 0; i < numberOfLongestSensors; i++)
			angle = max + i;
		angle = (angle / numberOfLongestSensors) * 10 - 90;
		
		return angle;
	}
	/**
	 * determines index that belongs to indices set parallel with the vehicle axis, the method was described when these indices were 1,2,16,17(if they were changed then the method should be changed as well) even thought 9 is parallel with the axis, it is not set as true, see method setStraightSensorIndexes
	 * @param i index ktory overujeme
	 * @return
	 */
	public boolean isSensorIndexStraight(int i){
		Iterator<Integer> iter =straightSensorIndexes.iterator();
		while(iter.hasNext()){
			if(i==iter.next())
				return true;
		}
		return false;
	}
	
	
	/**
	 * returns if it is in the middle and parallel with the axis, used in model creation
	 * @return
	 */
	public boolean isStraightMid() {
		return straightMid;
	}
	/**
	 * return whether in staright or turn
	 * @return
	 */
	public boolean isRovinaNemeratZakruty() {
		return rovinaNemeratZakruty;
	}
	/**
	 * a method than provides setting of sensor indices that are parallel with the axis of the vehicle
	 * @param colection collection of integers, sensor indices that head parallel with the vehicle axis
	 */
	public void setStraightSensorIndexes(Collection<Integer> colection){
		straightSensorIndexes.clear();
		straightSensorIndexes.addAll(colection);
	}
	/**
	 * friction
	 * @param friction
	 */
	public void setFriction(double friction) {
		this.friction = friction;
	}
	
	/**
	 * a method for length computation from the speed for the later speed computation
	 * @param speed
	 * @param friction
	 * @return
	 */
	public static double countDistanceFromSpeed(double speed, double friction) {
		return ((Math.pow(speed, 2))/(2*9.81*friction));
	}
	/*computes speed from steering*/
	/*private double countSpeed(double steering){
		if(steering>0.4){
			return 116.28-114.9683*steering+51.2030*Math.pow(steering, 2);
		}else if(steering>0.14){
			return 189.6921-506.5*steering+582.0934*Math.pow(steering, 2);
		}else if(steering>=0.1){
			return 268.5877-1508.3*steering+3798.4*Math.pow(steering, 2);
		}else if(steering>=0.018){
			return 22.4685/(steering+0.0471);
		}else
			return 500;
			
	}
	*///try to guess the radius
	/*private double radiusFromDistance(double distance){
		if(distance>142)
			return 200;
		else if(distance>44)
			return 0.8107+1.0316*distance-0.0002*Math.pow(distance, 2);
		else if(distance>15)
			return 0.0579+1.0117*distance;
		else
			return 10.0168-0.6658*distance+0.0696*Math.pow(distance, 2);
	}*/
	 //a method that in every tick computes opponent track pos, not working when sensors are being changed - pushes to opponent
    /*private double avoidCloseOppnent(SensorModel sm,int index,double trackPos){
    	double oppTrackPos=0;
    	double ourNewTrackPos;
    	if(index<=17){
    		oppTrackPos=sm.getTrackPosition()+((Math.sin(indexToRad(index))*sm.getOpponentSensors()[index])/(trackWidth/2))*1.15;
    		System.out.println(Math.sin(indexToRad(index))+" "+sm.getOpponentSensors()[index]+" "+ (trackWidth/2));
    		System.out.println("opponent pos: "+oppTrackPos+" my pos: "+trackPos+" index "+index);
    	}
    	else{
    		oppTrackPos=sm.getTrackPosition()-((Math.sin(indexToRad(index))*sm.getOpponentSensors()[index])/(trackWidth/2))*1.15;
    		System.out.println(Math.sin(indexToRad(index))+" "+sm.getOpponentSensors()[index]+" "+ (trackWidth/2));
    		System.out.println("opponent pos: "+oppTrackPos+" my pos: "+trackPos+" index "+index);
    	}
    	//oppTrackPos=Math.abs(oppTrackPos);
    	
    	
    	//opponent is on the left and there is enough space
    	if(index<17 && isSpace(oppTrackPos,index-13) && trackPos>oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos+2.5)/(trackWidth/2));
    		System.out.println(" 1 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	else if(index<17 && isSpace(oppTrackPos,index-13) && trackPos<oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos+2.5)/(trackWidth/2));
    		System.out.println(" 2 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	//oponent is on the left and there is not enough space
    	else if(index<17 && !isSpace(oppTrackPos,index-13)&& trackPos>oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos-2.5)/(trackWidth/2));
    		System.out.println(" 3 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	else if(index<17 && !isSpace(oppTrackPos,index-13)&& trackPos<oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos-2.5)/(trackWidth/2));
    		System.out.println(" 4 newtrackpos "+ourNewTrackPos);
    		return -(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	//oponent is on the rigth and there is enough space
    	else if(index>18 && isSpace(oppTrackPos,Math.abs(index-22))&& trackPos>oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos+2.5)/(trackWidth/2));
    		System.out.println(" 5 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	//oponent is on the right and there is not enough space
    	else if(index>18 && !isSpace(oppTrackPos,Math.abs(index-22))&& trackPos>oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos-2.5)/(trackWidth/2));
    		System.out.println(" 6 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	else if(index>18 && isSpace(oppTrackPos,Math.abs(index-22))&& trackPos<oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos+2.5)/(trackWidth/2));
    		System.out.println(" 7 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	else if(index>18 && !isSpace(oppTrackPos,Math.abs(index-22))&& trackPos<oppTrackPos){
    		launchTrackPos=ourNewTrackPos=sampleTrackPos(launchTrackPos,((trackWidth/2)*oppTrackPos-2.5)/(trackWidth/2));
    		System.out.println(" 8 newtrackpos "+ourNewTrackPos);
    		return 2*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos))/0.366519;
    	}
    	else if(index>=18 && isSpace(oppTrackPos,Math.abs(index-22))&& trackPos>oppTrackPos){
    		launchTrackPos=ourNewTrackPos=((trackWidth/2)*oppTrackPos-3.5)/(trackWidth/2);
    		System.out.println(" 5 newtrackpos "+ourNewTrackPos);
    		return 3*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos)*0.5)/0.366519;
    	}
    	else if(index<=17 && isSpace(oppTrackPos,index-13)&& trackPos<oppTrackPos){
    		launchTrackPos=ourNewTrackPos=((trackWidth/2)*oppTrackPos-3.5)/(trackWidth/2);
    		System.out.println(" 6 newtrackpos "+ourNewTrackPos);
    		return 3*(sm.getAngleToTrackAxis()-(sm.getTrackPosition()-ourNewTrackPos)*0.5)/0.366519;
    	}
    	if(index==17  && isSpace(oppTrackPos,index-13)){
    		launchTrackPos=sm.getTrackPosition();
    		System.out.println("17 true newtrackpos "+launchTrackPos);
    		return 0.5;
    	}
    	if(index==17  && !isSpace(oppTrackPos,index-13)){
    		launchTrackPos=sm.getTrackPosition();
    		System.out.println("17 false newtrackpos "+launchTrackPos);
    		return -0.5;
    	}
    	if(index==18 && isSpace(oppTrackPos,Math.abs(index-22))){
    		launchTrackPos=sm.getTrackPosition();
    		System.out.println("18 true newtrackpos "+launchTrackPos);
    		return 0.5;
    	}
    	if(index==18 && !isSpace(oppTrackPos,Math.abs(index-22))){
    		launchTrackPos=sm.getTrackPosition();
    		System.out.println("18 false newtrackpos "+launchTrackPos);
    		return -0.5;
    	}
    	if(sm.getOpponentSensors()[9]<2.5){
    		launchTrackPos-=0.06;
    	}
    	if(sm.getOpponentSensors()[26]<2.5){
    		launchTrackPos+=0.06;
    	}
    	
    	return 0;
    	
    	
    	
    }*/
	/*//based on presumtion than on one side can be 2 vehicles and there is 30 cm gap
    private double calculateNewTrackPos(SensorModel sm,double index,double lastindex){
    	double trackPosNew,opponentTrackPos=startingTrackPos;
    	//je pred nami 
    	if( (index==lastindex||index==lastindex)){
    		opponentTrackPos=lastOppPos;
    		System.out.println("1 oponent: "+opponentTrackPos+" index: "+index);
    	//je na pravo	
    	}else if(index >=18){
    		opponentTrackPos=sm.getTrackPosition()-((Math.sin(indexToRad(index,lastindex)+sm.getAngleToTrackAxis())*sm.getOpponentSensors()[(int)index])/(trackWidth/2));
    		System.out.println("2 oponentpos: "+opponentTrackPos+" index: "+index);
    	//je na pravo
    	}else if(index <=17){
    		opponentTrackPos=sm.getTrackPosition()+((Math.sin(indexToRad(index,lastindex)+sm.getAngleToTrackAxis())*sm.getOpponentSensors()[(int)index])/(trackWidth/2));
    		System.out.println("3 oponentpos: "+opponentTrackPos+" index: "+index);
    	}

    	//je vedla eho miesto && sameSide(opponentTrackPos,sm.getTrackPosition())
    	if(isSpace(opponentTrackPos) ){
    		//vypocitam poziciu posunutu od oponenta a preasobim ju znamienkm pre poziciu na trati
    		trackPosNew=((((Math.abs(opponentTrackPos)*(trackWidth/2))+(2.8))/(trackWidth/2))*(Math.abs(opponentTrackPos)/opponentTrackPos));
    		System.out.println("5 : "+(((Math.abs(opponentTrackPos)*(trackWidth/2))+(2.8))/(trackWidth/2) +" dir: "+(Math.abs(opponentTrackPos)/opponentTrackPos)));
    	}
    	//nieje vedla oponenta miesto
    	else if(!isSpace(opponentTrackPos) ){
      		//to iste co hore akuran na druhu stranu oponenta
    		trackPosNew=(((Math.abs(opponentTrackPos)*trackWidth/2-(2.8))/(trackWidth/2)))*(Math.abs(opponentTrackPos)/opponentTrackPos);
    		System.out.println("6 : "+((Math.abs(opponentTrackPos)*trackWidth/2-(2.8))/(trackWidth/2) +" dir: "+(Math.abs(opponentTrackPos)/opponentTrackPos)));
    	}
    	else
    		trackPosNew=launchTrackPos;	
    	
    	lastOppPos=opponentTrackPos;
    	return trackPosNew;
    		
    }*/
	
}
