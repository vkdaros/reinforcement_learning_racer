package controller;

import champ2011client.SensorModel;
/**
 * A class for the vehicle stabilisation - ABS & ASR.
 * 
 * @author Maros Bednar
 */
public class ESP {
	
	final static float wheelRadius[]={(float) 0.3179,(float) 0.3179,(float) 0.3276,(float) 0.3276};
	final static float slipAllow=(float) 0.1; // = 10%, a parameter observing occurence of unwanted effects
	static double actionBrakeAktual = (double)0;
	static double actionBrakeTickBack = (double)0;
	static double actionAccelAktual = (double)0;
	static double actionAccelTickBack = (double)0;
	
	/**
	 * ABS method. Brade pedal pressure regulation
	 * 
	 * @param sensors sensor model
	 * @param double brake pressure
	 * @return double brake pressure - modified so that a lock will not occur
	 */
	public static double filterABS(SensorModel sensors,double brake){
		double speed = (double) (sensors.getSpeed() / 3.6);
		double slip = 0.0;
	    for (int i = 0; i < 4; i++)
	    {
	        slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];
	    }
	    double speedWheelAverage = (double) slip/4.0f;
	    double alfa = 1 - (speedWheelAverage/speed);
	    if (alfa > slipAllow)
	    {
	    
	    	//NEGATIVE EFFECT
	    	brake = actionBrakeTickBack ;
	    	actionBrakeAktual = brake;
	    	actionBrakeTickBack -= 0.1;    	
	    }
	    else{
	    	//IS NOT SLIPPING, within 20%
	    	actionBrakeTickBack = brake;
	    	brake  = brake +(double)0.1;
	    	if(brake > 1){
	    		actionBrakeAktual = 1;
	    		brake = 1;
	    	}
	    	else{
	    		actionBrakeAktual = brake;
	    	}
	    	
	    }
	
	   	return brake;
	}
	/**
	 * ASR method. accelerator regulation
	 * 
	 * @param sensors sensor model
	 * @param double accelerator
	 * @return double accelerator - modified so that slips wont occur
	 */
	public static double filterASR(SensorModel sensors,double accelerate){
		double speed = (double) (sensors.getSpeed() / 3.6);
		double slipR = 0.0;
		slipR = (sensors.getWheelSpinVelocity()[2]+
				sensors.getWheelSpinVelocity()[3])
				* wheelRadius[2];
		
		double speedWheelAverage = slipR/2.0;//slips of the rear
		double alfa = (speedWheelAverage/speed)-1;
	    
		if (alfa > slipAllow)
		{
			//NEGATIVE EFFECT
			accelerate = actionAccelTickBack ;
	    	actionAccelAktual = accelerate;
	    	actionAccelTickBack -= 0.1;    	
	    }
	    else{
	    	//IS NOT SLIPPING
	    	actionAccelTickBack = accelerate;
	    	accelerate  = accelerate +(double)0.1;
	    	if(accelerate > 1){
	    		actionAccelAktual = 1;
	    		accelerate = 1;
	    	}
	    	else{
	    		actionAccelAktual = accelerate;
	    	}
	    	
	    }
		return accelerate;
	}
}
