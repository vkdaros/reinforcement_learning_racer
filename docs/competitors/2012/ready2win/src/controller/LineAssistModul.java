package controller;

import controller.model.TrackData;
import controller.model.Turn;
import controller.model.Turn.Direction;
import champ2011client.Action;
import champ2011client.SensorModel;

public class LineAssistModul {
	private double targetPosP=0.8;
	private double targetPosM=0.6;
	private double trackWidth;
	private int tickCount=4;
	
	public double getTargetPosP() {
		return targetPosP;
	}
	
	public double getTargetPosM() {
		return targetPosM;
	}

	public void setTargetPosP(double targetPos) {
		this.targetPosP = targetPos;
	}
	
	public void setTargetPosM(double targetPos) {
		this.targetPosM = targetPos;
	}

	public double getTrackWidth() {
		return trackWidth;
	}

	public void setTrackWidth(double trackWidth) {
		this.trackWidth = trackWidth;
	}
	
	/**
	 * A method that modifies steering before leaving the turn
	 * @param sm
	 * @param action
	 * @param nextTurn - next turn
	 * @param lastTurn - previous turn
	 * @return - action
	 * @author Juraj Kosmel
	 */
	public Action assist(SensorModel sm, Action action, TrackData trackData) {
		//******entry to the turn*******//
		Turn nextTurn = trackData.getNextTurn();
		boolean isNext = (trackData.getNext2Turn()!=null);
		Turn next2Turn = trackData.getNext2Turn();
		Turn lastTurn = trackData.getLastTurn();
		double myPos = sm.getDistanceFromStartLine();
		double start = nextTurn.getStartDistance();
		double end = nextTurn.getEndDistance();
		
			if(inAssist(sm, nextTurn) && myPos<nextTurn.getEnterKm() && (nextTurn.getEnterKm()-myPos)<200 && (lastTurn!=null ? ((start-lastTurn.getEndDistance())>50):true)) {
				//System.out.println(sm.getDistanceFromStartLine()+" LINE ASSIST turn: "+trackData.getNextTurn().getStartDistance()+" "+(trackData.getLastTurn()!=null ? trackData.getLastTurn().getEndDistance():"0")); 
					action=navigateToPosition(action,sm, trackData.getNextTurn().getEnterKm(),trackData.getNextTurn().getEnterPos());
					//steering smoothing
					/*if(Math.abs(sm.getTrackPosition())+Math.abs(nextTurn.getEnterPos())>0.5 && nextTurn.getEnterKm()-myPos<50 && sm.getSpeed()>150){
						action.steering=(sm.getAngleToTrackAxis())/0.366519;//action.steering=minimizeSteering(action.steering);
						System.out.println("podla osi");
					}
					else*/
						action.steering=tuneSteering(action.steering);
					/*System.out.println("peak>"+trackData.getNextTurn().getPeakValue());
					System.out.println("dir>"+trackData.getNextTurn().getDirection());
					System.out.println("pos>"+trackData.getNextTurn().getEnterPos());
					System.out.println();*/
			}
		
		return action;
	}
	
	/**
	 * A method that checks whether line assist should be used, or the vehicle "sees" the turn and turning can begin.
	 * @param sm
	 * @param turn
	 * @return
	 */
	private boolean inAssist(SensorModel sm, Turn turn) {
		if(turn.getDirection() == Direction.LEFT) {
			for (int i = 3; i <= 8; i++) {
				if(sm.getTrackEdgeSensors()[i]>100)
					return false;
			}
		}
		
		else {
			for (int i = 10; i <= 15; i++) {
				if(sm.getTrackEdgeSensors()[i]>100)
					return false;
			}
		}
		return true;
		
	}

	/**
	 * A method computes an angle between current position and a point on the track.
	 * @param sm senzory
	 * @param positionDistance a distance of the observed point from the start
	 * @param position a vertical position of the observed point
	 * @return angle
	 * @author Juraj Kosmel
	 */
	private double computeAngle(SensorModel sm, double positionDistance, double position) { 
		double alfa=0;
		double vertical=0;
		double horizontal=0;
		if((sm.getTrackPosition()>0 && position>0) || (sm.getTrackPosition()<0 && position<0)) { //the position where we are heading is on the same side of the track
			vertical=Math.abs((trackWidth/2.0)*Math.abs(sm.getTrackPosition())-trackWidth/2.0*Math.abs(position));
			horizontal=positionDistance-sm.getDistanceFromStartLine();
			alfa = Math.atan(vertical/horizontal);
		}
		else {
			vertical=Math.abs((trackWidth/2.0)*Math.abs(sm.getTrackPosition())+trackWidth/2.0*Math.abs(position)); //the position where we are heading is on the different side of the track
			horizontal=positionDistance-sm.getDistanceFromStartLine();
			alfa = Math.atan(vertical/horizontal);
		}
		return alfa;
	}
	
	/**
	 * A method modifies steering direction and novigation to the specific point on the track.
	 * @param action current set of effectors
	 * @param sm sensors
	 * @param positionDistance a position that needs to be reached - km from the start of the track
	 * @param position a position on the track in the given position
	 * @return modified action - wheel orientation
	 * @author Juraj Kosmel
	 */
	private Action navigateToPosition(Action action, SensorModel sm, double positionDistance, double position) {
		double alfa, beta, sum, diff;                      //an angle towards the target position, an angle between the vehicle and the axis, sum and diff
		alfa=computeAngle(sm, positionDistance, position); //computation of an angle towards the place where the vehicle should be heading
		beta = sm.getAngleToTrackAxis();				   //an angle of the current direction
		sum=Math.abs(alfa)+Math.abs(beta);				   //sum
		diff=Math.abs(Math.abs(alfa)-Math.abs(beta));	   //difference
		//System.out.println("beta: "+Math.toDegrees(beta)+" alfa: "+Math.toDegrees(alfa));
		
		if(beta>0) { //heading right
			if(sm.getTrackPosition() >= position) { //I want to go right
				if(Math.abs(beta)<=alfa) {
					action.steering=-(sum/0.36651943);
					//System.out.println("heading right, want to go right, beta lower");
				}
				else {
					action.steering=diff/0.36651943;
					//System.out.println("heading right, want right, beta higher");
				}
			}
			else {								   //I want to go left
				action.steering=sum/0.36651943;
				//System.out.println("heading right, want to go left");
			}
		}
		else {		 //heading left
			if(sm.getTrackPosition() >= position) { //want to go left
				action.steering=-(sum/0.36651943);
				//System.out.println("heading left, want to go right");
			}
			else {								   //want to go left
				if(Math.abs(beta)<=alfa) {
					action.steering=sum/0.36651943;
					//System.out.println("heading left, want to go left, beta lower");
				}
				else {
					action.steering=-(diff/0.36651943);
				//	System.out.println("heading left, want to go left, beta higher");
				}
			}
		}
		//System.out.println("steering: "+action.steering);
		return action;
	}
	
	/**
	 * steering smoothing
	 * @param steer
	 * @return
	 */
	private static double tuneSteering(double steer) {
		//steering smoothing
		if((steer>0 && steer<0.01) || (steer<0 && steer>-0.01))
			return 0;
		if(steer>0.2)
			return 0.2;
		if(steer<-0.2)
			return -0.2;
		if(Math.abs(steer)>0.1 && Math.abs(steer)<0.2)
			return steer*0.8;
		return steer;
	}
	
	/**
	 * steering smoothing to the minimum
	 * @param steer
	 * @return
	 */
	private static double minimizeSteering(double steer) {
		//steering smoothing
		if((steer>0 && steer<0.01) || (steer<0 && steer>-0.01))
			return 0;
		if(steer>0.05)
			return 0.05;
		if(steer<-0.05)
			return -0.05;
		return steer;
	}
	
	/**
	 * speed computation
	 * @param action
	 * @param sm
	 * @param trackData
	 * @param speedAdjustment
	 * @return
	 */
	public Action getSpeedFromNextTurn(Action action, SensorModel sm, TrackData trackData, double speedAdjustment, double sensorStr) {
		
		Turn nextTurn = trackData.getNextTurn();
		Turn next2Turn = trackData.getNext2Turn();
		double breakDist=nextTurn.getBreakDist();
		double start = nextTurn.getEnterKm();
		double end = nextTurn.getEndDistance();
		boolean isNext = next2Turn!=null;
		double nextStart = isNext ? next2Turn.getEnterKm():0;
		double peak = nextTurn.getPeakValue();
		double peakNext = isNext ? next2Turn.getPeakValue():0.0;
		double myPos = sm.getDistanceFromStartLine();
		double halfTurnDist = nextTurn.getStartDistance()+((nextTurn.getEndDistance()-nextTurn.getStartDistance())/2);
		//still in the turn - the longest sensor multiplied by the coefficient of an acceleration for the given turn
		if(myPos<end && myPos>start) {
			//System.out.println("str> "+sensorStr+" index> "+nextTurn.getSpeedIndex());
			breakDist=sensorStr*nextTurn.getSpeedIndex();
			//System.out.println("turning, speed <"+breakDist);
			if(isNext) {
				if(breakDist>(nextStart-myPos+trackData.getNext2Turn().getBreakDist())) {
						breakDist=trackData.getNext2Turn().getBreakDist()+(nextStart-myPos);
						//System.out.println("turning, next <"+breakDist);
				}
			}
			
		}
		else { //finished turning - driving according to the model
			if(trackData.getNextTurn()!=null) {
				breakDist=trackData.getNextTurn().getBreakDist()+(start-myPos);
				//System.out.println("before the turn, speed <"+breakDist);
				if(isNext ) {
					if(breakDist>(nextStart-myPos+trackData.getNext2Turn().getBreakDist())) {
							breakDist=trackData.getNext2Turn().getBreakDist()+(nextStart-myPos);
							//System.out.println("before, next <"+breakDist);
					}
				}
			}
			else
				breakDist=sensorStr;
			
		}
		
		/*if(breakDist<sensorStr);
			breakDist=sensorStr;*/
		
		if (breakDist<0)
			breakDist=sensorStr;
		
		if ((sm.getSpeed() + speedAdjustment) < Math.sqrt(2 * 9.81 * trackData.getFriction() * (Math.abs(breakDist)))){
			action.accelerate = ESP.filterASR(sm, ESP.actionAccelAktual);
			//System.out.println("ESP1 >"+action.accelerate+"  >"+action.accelerate);
			ESP.actionBrakeAktual = 0;
			ESP.actionBrakeTickBack = 0;
		}else if(sm.getSpeed()<20){
			//System.out.println("ESP2 >"+action.accelerate+"  >"+action.accelerate);
			action.accelerate = 0.8;
			ESP.actionBrakeAktual = 0;
			ESP.actionBrakeTickBack = 0;
		}
		else //if(!(myPos>start && myPos<halfTurnDist))
			{
			//System.out.println("ESP3 >"+action.accelerate+"  >"+action.accelerate);
			action.brake = ESP.filterABS(sm, ESP.actionBrakeAktual);
			ESP.actionAccelAktual = 0;
			ESP.actionAccelTickBack = 0;
		}
		if(tickCount>3 && action.brake>0)
			action.brake=0;
		if(tickCount==1)
			tickCount=7;
		tickCount--;
			
		
		//som v 2 polovici a brzdim max 80 s peak val	
		if(myPos<end && myPos>start && action.brake>0){
			action.accelerate=0.3;
			//System.out.println("pridavam pocas brzdenia");
		}
		//som v prvej polke  tak pridavam iba 1-peak val max
		if(myPos>start && myPos<halfTurnDist && trackData.getNextTurn().getSpeedIndex()<1 && action.accelerate>0){
			if(trackData.getNextTurn().getSpeedIndex()-peak>=0)
				action.accelerate=trackData.getNextTurn().getSpeedIndex()-peak;
			else
				action.accelerate=0;
		}
		/*if(myPos<end && myPos>start){
			System.out.println("som v zakrute");
			
		}else
			System.out.println("som na rovine");*/
		//System.out.println("breakDist: "+breakDist+" < "+trackData.getNextTurn().getPeakValue());
		return action;
	}
	
}
