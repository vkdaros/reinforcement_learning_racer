package controller;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import telemetry.DataStorage;
import telemetry.DataUnit;

import champ2011client.Action;
import champ2011client.Controller;
import champ2011client.SensorModel;

/**
 * Kontroler vozidla zatacajuci podla najdlhsieho senzora.
 */
public class Prototype extends Controller
{	
	DataStorage dS;
	//instancia recovery modul
	RecoveryModule recMod;
	//radenie
	//zoradeny rad otacok za poslenych 30 tikov
	private Deque<Double> rpm30Tick = new ArrayDeque<Double>(); 
	private double clutchCur=0;
	boolean RPMGrows=false;
	boolean notOnTrack =false;
	int dontGear =0;// 7 tikov trva kym otacky zacnu po radeni stupat
	/*
	 * Koeficient frikcie medzi pneumatikou a vozovkou; tato hodnota 
	 * sa v prvom kole na zacitaku zistuje pri rychlosti 150 z brzdnej 
	 * drahy a flagy potrebne pri jej urceni
	 */
	private double friction=0;
	private boolean kolesaStoja = false,brzdi =false,vypocitajFriction=false;
	private double brakeStartDist;
	//rychlost pri akej sa ma merat frikcia
	private double breakSpeed=150;
	
	// rad sluziaci na komunikaciu s vlaknom vytvarajucim model
//	private final BlockingQueue<TickInfo> tickInfoPipe;
	
	// rad sluziaci na komunikaciu s vlaknom vytvarajucim model
//	private final BlockingQueue<SensorInfo> sensorInfoPipe;

	//premenna pre sirku trate
	private double trackWidth=0;
	private boolean trackWidthMeasuring = true;
	
	//premenna pre dlzku trate
	private double trackLength=0;
	private boolean trackLengthMeasuring = true;
	
	//premenna pre zaznamenavanie radiusov jednotlivych metrov trate
	private int trackRadius = 1;
	
	// potrebne k recovery
	private int ticCount=0;
	private boolean afterStart=false;

	//zaciatky kol
	private boolean inLap=true;
	
	public Prototype()
	{
/*		tickInfoPipe = new LinkedBlockingQueue<TickInfo>(1000);
		
		Thread t = new Thread(new TurnDetector(tickInfoPipe));
		t.setDaemon(true);
		t.setPriority(Thread.MIN_PRIORITY);
		t.start();
		
		sensorInfoPipe = new LinkedBlockingQueue<SensorInfo>(1000);
		
		Thread t2 = new Thread(new SensorDetector(sensorInfoPipe));
		t2.setDaemon(true);
		t2.setPriority(Thread.MIN_PRIORITY);
		t2.start();
*/		
		recMod=new RecoveryModule();
		dS = new DataStorage();
	}

	public Action control(SensorModel sm)
	{
	// sleduje, ci uz je po starte (100 tickov) 
		ticCount++;
		if (ticCount == 100)
			afterStart=true;
		Action action = new Action ();
		
		double radius;
		//zistenie radiusu
		if ((sm.getDistanceFromStartLine() > trackRadius) && (sm.getDistanceFromStartLine() < (trackRadius+3)))
		{
			trackRadius += 3;
			if (sm.getTrackPosition() > 0)
				radius = computeRadius(sm, 13);
			else
				radius = computeRadius(sm, 3);
			
			if (radius < 2000);
			//	System.out.println(radius);
		}
		
	//zistenie sirky trate
		if (trackWidthMeasuring)
		{
			trackWidth=sm.getTrackEdgeSensors()[0]+sm.getTrackEdgeSensors()[18]; 
			//System.out.println(trackWidth+"sirka trate");
			//System.out.println(sm.getTrackPosition());
			trackWidthMeasuring = false;
		}
		
		//zistenie dlzky trate
		if (trackLengthMeasuring)
		{
			if ((sm.getDistanceFromStartLine() < trackLength) && (trackLength > 0))
			{
				trackLengthMeasuring = false;
				//System.out.println(trackLength+"dlzka trate");
				dS.setLength(trackLength);
			}
			else
				trackLength=sm.getDistanceFromStartLine();		
		}
		
		//updatne sa zoznam otacok za poslednych 30 tikov
		if(rpm30Tick.size()==15 && dontGear==0)
			rpm30Tick.pollFirst();
		if(dontGear==0)
			rpm30Tick.addLast(sm.getRPM());
		else
			dontGear--;
			
		/*if(notOnTrack){
			notOnTrack=false;
			RPMGrows=false;
		}else			
			RPMGrows= isDequeGrowing(rpm30Tick);*/
		
		/* * * * * R E C O V E R Y * * * Z A C I A T O K * * */

		/* 
		 * v kazdom ticku sa overi, ci sa pilot nenachadza v specifuckych situaciach recovery 
		 * 
		 */
		
		// ak je po starte, kontorluje sa specificka situacia
		if (afterStart)
			recMod.checkRecovery(sm);
		
		// zablokovany pri okraji trate (kolmo) - prechod do noveho recovery stavu
		if ((recMod.isBlocked()) && afterStart && (-1<sm.getTrackPosition() && sm.getTrackPosition()<1)){
			if (sm.getTrackPosition()<0)
				return recMod.recoverRight(sm); 
			else if (sm.getTrackPosition()>0)
				return recMod.recoverLeft(sm);
		}
			
		// zablokovany v kopci 			
		if ((recMod.isBlocked()) && afterStart)
			return recMod.goStraight(sm);

		// vyletel som na lavu stranu cesty alebo bol v recovrery state left a neje v pozicii kedy moze odovzdat auto modulu riadenia
		if ((sm.getTrackPosition()>1 || recMod.isInRecoveryStateLeft()) && (!recMod.isBlocked())){
			notOnTrack=true;
			return recMod.recoverLeft(sm);
		} //else if (recMod.isBlocked())
		
		// vyletel som na pravu stranu cesty alebo bol v recovrery state right a neje v pozicii kedy moze odovzdat auto modulu riadenia
		if ((sm.getTrackPosition()<-1 || recMod.isInRecoveryStateRight()) && (!recMod.isBlocked())){
			notOnTrack=true;
			return recMod.recoverRight(sm);
		} //else if (recMod.isBlocked())

		// som na trati v protismere - natoceny viac do lava 
		if ((sm.getAngleToTrackAxis()>Math.PI/2.0) && (sm.getAngleToTrackAxis()<Math.PI)){
//			System.out.println("som na trati v protismere - natoceny viac do lava");
			return recMod.recoverRight(sm);
		}
		
		// som na trati v protismere - natoceny viac do prava
		if ((sm.getAngleToTrackAxis()<-Math.PI/2.0) && (sm.getAngleToTrackAxis()>-Math.PI)){
//			System.out.println("som na trati v protismere - natoceny viac do prava");
			return recMod.recoverLeft(sm);
		}
		
		/* * * * * R E C O V E R Y * * * K O N I E C * * */		
		
		
		/*
		 * rychlost - akceleracia naplno do dosiahnutia maximalnej rychlosti, ktoru je mozne ubrzdit na vzdialenost
		 * najdlhsieho senzora,
		 * vzorec			v = odmocnina(2 * g * mi * brzdna draha)
		 */
		if (sm.getSpeed ()< Math.sqrt(2 * 9.81 * friction * sm.getTrackEdgeSensors()[18 - (int) ((computeAvgLongestSensorAngle(sm.getTrackEdgeSensors())+90)/10)]))
		{	action.accelerate = ESP.filterASR(sm, ESP.actionAccelAktual);
			ESP.actionBrakeAktual = 0;
			ESP.actionBrakeTickBack = 0;
		}
		else{
			action.brake = ESP.filterABS(sm, ESP.actionBrakeAktual);
			ESP.actionAccelAktual = 0;
			ESP.actionAccelTickBack = 0;
		}
	
		// * riadenie *
		// zmena smeru na zaklade najdlhsieho senzora
		action.steering = steeringAdjustment(computeAvgLongestSensorAngle(sm.getTrackEdgeSensors()));
		
		
		/* testovanie navigacie na konkretne miesto - nastavene na prvu zakrutu trate CG SpeedWay
		if(sm.getDistanceFromStartLine()>310) action.steering = steeringAdjustment(computeAvgLongestSensorAngle(sm.getTrackEdgeSensors()));
		if(sm.getDistanceFromStartLine()<30 && sm.getDistanceFromStartLine()>10) {
			action.steering=-0.15; //vytocenie na pravu stranu
		}
		if(sm.getDistanceFromStartLine()>30 && sm.getDistanceFromStartLine()<310) { //navigacia az po zaciatok zakruty
			System.out.println("navigujem...");
			action=navigateToPosition(action, sm, 300, 0.1); //posledny parameter urcuje vertikalnu poziciu kde mozme auto smerovat
		}*/
		
				
		// * prevodovka *
		action = getGear(sm,action,rpm30Tick);

		// posielanie informacie potrebnej na vytvaranie modelu trate
/*		try {
			tickInfoPipe.put(new TickInfo(action, sm));
		} catch (InterruptedException e) {
			System.err.println("Error: chyba pri posielani akcie do modelu trate.");
			e.printStackTrace();
		}
*/
/*		try
		{
			sensorInfoPipe.put(new SensorInfo((int)(computeAvgLongestSensorAngle(sm.getTrackEdgeSensors())+90)/10, 1, sm.getDistanceFromStartLine()));
		}catch (InterruptedException e) {
			System.err.println("Error: chyba pri posielani akcie do modelu trate.");
			e.printStackTrace();
	}*/

		if(friction ==0){
			friction=getFriction(sm,action);
			//System.out.println(friction);
		}else if(sm.getSpeed()<1)
			action.accelerate=1;
		
		double accel;
		if (action.accelerate > 0)
			accel = action.accelerate;
		else
			accel = -action.brake;
		if (dS.getCount()>0)
		{
			if (((int)sm.getDistanceRaced())>(int)dS.getLast().getDistance())
			{
					dS.store(new DataUnit(accel, sm.getSpeed(), sm.getRPM(), action.gear, sm.getTrackPosition(), sm.getCurrentLapTime(), sm.getDistanceRaced()));
					if ((inLap) && (sm.getDistanceFromStartLine() > 0) && (sm.getDistanceFromStartLine() < 5))
					{
						inLap = false;
						dS.addFirstMeters(dS.getCount()-1);
					}
					if ((!inLap) && (sm.getDistanceFromStartLine() > 5))
						inLap = true;
			}
		}
		else
			dS.store(new DataUnit(accel, sm.getSpeed(), sm.getRPM(), action.gear, sm.getTrackPosition(), sm.getCurrentLapTime(), sm.getDistanceRaced()));
		
		
		return action;
	}
	/**
	 * metoda na zistenie frikcie z brzdnej drahy pri zaseknutych kolesach(vsetky4),
	 *  pouziva v sebe globalne premenne breakSpeed(150), breakStartDist a tri 
	 *  flagy a to vypocitajFriction, brzdi, kolesaStoja
	 * 
	 * @param sensors aktualne senzory
	 * @param action aktualna akcia
	 * @return 0 ak auto este nezastavilo a frikciu pre povrch ak auto zastalo
	 */
    private double getFriction(SensorModel sensors, Action action){
		
		
		if(sensors.getSpeed()<=breakSpeed && !brzdi){
			action.accelerate=1;
			action.brake=0;
			brakeStartDist = sensors.getDistanceRaced();
			//System.out.println(brakeS);
		}else{
			double speed =0;
			//System.out.print(zrychlenie(sensorsBefore.getSpeed(), sensors.getSpeed(), sensorsBefore.getCurrentLapTime(), sensors.getCurrentLapTime()));
			//System.out.print(" =||= ");
			for(int i=0;i<4;i++){
				speed+=sensors.getWheelSpinVelocity()[i]*0.3179;
				//System.out.print(" || "+zrychlenie(sensorsBefore.getWheelSpinVelocity()[i]*0.3179, sensors.getWheelSpinVelocity()[i]*0.3179, sensorsBefore.getCurrentLapTime(), sensors.getCurrentLapTime()));
			}
			speed/=4;
			//System.out.println();
			//System.out.println("rychlost kolies: "+speed +" brakespeed "+breakSpeed+" brakedist "+brakeStartDist);
			
			if(speed<0.05 && !kolesaStoja){
				brakeStartDist= sensors.getDistanceRaced();
				kolesaStoja=true;
				breakSpeed=sensors.getSpeed();
			}
			brzdi=true;
			action.accelerate=0;
			action.brake=1;
			action.clutch=0;
			if(sensors.getSpeed()<0.01 && !vypocitajFriction){
				//System.out.println("drzdna draha"+(sensors.getDistanceRaced()-brakeStartDist)+ " rychlost kolies: "+speed +" brakespeed "+breakSpeed+" brakedist "+brakeStartDist+" friction "+(Math.pow(breakSpeed, 2)/(2*9.81*(sensors.getDistanceRaced()-brakeStartDist))));
				vypocitajFriction=true;
				return (Math.pow(breakSpeed, 2)/(2*9.81*(sensors.getDistanceRaced()-brakeStartDist)));
				
			}//System.out.println(sensors.getSpeed());
			
		}
		return 0;
	}

	/**
	 * metoda urcujuca polomer useku trate na zaklade troch trackEdge senyorov
	 * 
	 * @param sensors senzormodel
	 * @param first prvy z trojice senzorov, od ktoreho sa pocita radius
	 * @return radius useku trate
	 */
	private double computeRadius(SensorModel sensors, int first)
	{
		double x1 = Math.cos(Math.PI/6) * sensors.getTrackEdgeSensors()[first];
		double y1 = Math.sin(Math.PI/6) * sensors.getTrackEdgeSensors()[first];
		double x2 = Math.cos(Math.PI/4.5) * sensors.getTrackEdgeSensors()[first+1];
		double y2 = Math.sin(Math.PI/4.5) * sensors.getTrackEdgeSensors()[first+1];
		double x3 = Math.cos(Math.PI/3.6) * sensors.getTrackEdgeSensors()[first+2];
		double y3 = Math.sin(Math.PI/3.6) * sensors.getTrackEdgeSensors()[first+2];
		
		double a,b,c,s;
		
		a = Math.sqrt(Math.pow((x1-x2), 2)+Math.pow((y1-y2), 2));
		b = Math.sqrt(Math.pow((x2-x3), 2)+Math.pow((y2-y3), 2));
		c = Math.sqrt(Math.pow((x3-x1), 2)+Math.pow((y3-y1), 2));
		s = (a+b+c)/2;
		
		return (a*b*c)/(4*Math.sqrt(s*(s-a)*(s-b)*(s-c)));
	}
	
	/**
	 * metoda modulu riadenia na urcenie prevodoveho stupna s otacok motora a podla toho ci rastu/klesaju
	 * 
	 * @param sensors senzormodel
	 * @param action akcia na vykonanie
	 * @param rpmGrows boolena hovorici o tom ci otacky klesaju alebo stupaju
	 * @return akciu, ktora ma nastaveny aj prevodovy stupen 
	 */
	private Action getGear(SensorModel sensors,Action action, Deque<Double> rad){
		int gearCur;
		double rpm = sensors.getRPM();
		int[]  gearUp = {9500, 9500, 9500, 9500, 9500, 0};
		int[]  gearDown =  {0, 4300, 6200, 7000, 7300, 7700};
		gearCur=sensors.getGear();//berem akt stupen
		//som na starte a pretek este nebol odstarovany radim 1, stlacim spojku, pridam plyn 
		if(gearCur<1){
			gearCur=1;
			action.gear=gearCur;
			action.accelerate=1;
			clutchCur=1;
			action.clutch=clutchCur;
		}
		//uz je odstartovane a mam stlacenu spojku, yniyim stlacenie o 0.05, stale pridavam
		else if(clutchCur>0.65){
			action.accelerate=1;
			clutchCur-=0.05;
			action.clutch=clutchCur;
			//System.out.println("som tu");
		}
		// rychlost je mensia ako 6 sucastne su otaky vecsie/rovne otackam ked radim, a sucasne otacky v predchadzajucich 30 tikoch stupali => radim hore
		else if (gearCur < 6 && rpm >= gearUp[gearCur-1] && dontGear==0)
		{		
			if(isDequeGrowing(rad)){
				dontGear=8;
				gearCur++;
			}
		}
		// rychlost je vetsia ako 1 sucastne su otaky mensie/rovne otackam ked podradzujem a otacky za poslednych 30 tikov klesaju
		else if (gearCur > 1 && rpm <= gearDown[gearCur-1]  && dontGear==0)
		{
			if(!isDequeGrowing(rad)){
				dontGear=8;
				gearCur--;	
			}
			
		}
		if(clutchCur>0 && gearCur>1)
			clutchCur-=0.05;
		action.gear=gearCur;
		action.clutch=clutchCur;
		return action;
	}
	/**
	 * metoda vrati true ak rad<double> apon s polovice rastie a false ak nsatane opacne situacia
	 * 
	 * @param que rad cisel ktorych rast sa ma urcit
	 * @return true alebo false
	 */
	private boolean isDequeGrowing(Deque<Double> que){
		Deque<Double> pom = new ArrayDeque<Double>(que);
		boolean rastie[] = new boolean[pom.size()];
		double curValue = pom.pollFirst().doubleValue();
		//System.out.print(pom.size()+" "+curValue+ " ");
		for(int i=0;i<que.size()-1;i++){
			rastie [i]= curValue<pom.peekFirst().doubleValue();
			curValue = pom.pollFirst().doubleValue();
			//System.out.print(curValue+ " "+rastie [i]+";");
		}
		
		int h=0;
		for(int i=0;i<rastie.length;i++){
			if(rastie[i])
				h++;
		}
		//System.out.print(h+" "+pom.size());
		//System.out.println();
		if(h>=rastie.length/2)
			return true;
		else
			return false;
	}
	
	
	/**
	 * Metoda, ktora vypocita uhol medzi aktualnou poziciou auta a bodom na trati pred autom.
	 * @param sm senzory
	 * @param positionDistance vzdialenost sledovaneho bodu od startu
	 * @param position vertikalna pozicia sledovaneho bodu na trati
	 * @return uhol
	 * @author Juraj Kosmel
	 */
	private double computeAngle(SensorModel sm, double positionDistance, double position) { 
		double alfa=0;
		double vertical=0;
		double horizontal=0;
		if((sm.getTrackPosition()>0 && position>0) || (sm.getTrackPosition()<0 && position<0)) { //nasa pozicia aj pozicia kde smerujeme je na rovnakej strane trate
			vertical=Math.abs((trackWidth/2.0)*Math.abs(sm.getTrackPosition())-trackWidth/2.0*Math.abs(position));
			horizontal=positionDistance-sm.getDistanceFromStartLine();
			alfa = Math.atan(vertical/horizontal);
		}
		else {
			vertical=Math.abs((trackWidth/2.0)*Math.abs(sm.getTrackPosition())+trackWidth/2.0*Math.abs(position)); //nasa pozicia a pozicia kde smerujeme su na roznych stranach trate
			horizontal=positionDistance-sm.getDistanceFromStartLine();
			alfa = Math.atan(vertical/horizontal);
		}
		return alfa;
	}
	
	/**
	 * Metoda na upravu smeru jazdy a navigaciu na konkretnu poziciu na trati.
	 * @param action aktualne nastavenie efektorov
	 * @param sm senzory
	 * @param positionDistance pozicia, na ktoru sa chceme dostat - kilometer od zaciatku trate
	 * @param position poloha na trati na danej pozicii
	 * @return upravena action - natocenie kolies
	 * @author Juraj Kosmel
	 */
	private Action navigateToPosition(Action action, SensorModel sm, double positionDistance, double position) {
		double alfa, beta, sum, diff;                                 //uhol natocenia k cielovej pozicii a uhol auta s osou, sucet a rozdiel uhlov
		alfa=computeAngle(sm, positionDistance, position); //vypocet uhla k miestu kde chceme smerovat
		beta = sm.getAngleToTrackAxis();				   //uhol nasho aktualneho smeru
		sum=Math.abs(alfa)+Math.abs(beta);				   //sucet uhlov
		diff=Math.abs(Math.abs(alfa)-Math.abs(beta));				   //rozdiel uhlov
		System.out.println("beta: "+Math.toDegrees(beta)+" alfa: "+Math.toDegrees(alfa));
		
		if(beta>0) { //som otoceny vpravo
			if(sm.getTrackPosition() >= position) { //chcem ist vpravo vzhladom na moju poziciu
				if(Math.abs(beta)<=alfa) {
					action.steering=-(sum/0.36651943);
					System.out.println("natoceny vpravo chcem vpravo beta mensia");
				}
				else {
					action.steering=diff/0.36651943;
					System.out.println("natoceny vpravo chcem vpravo beta vacsia");
				}
			}
			else {								   //chcem ist vlavo vzhladom na moju poziciu
				action.steering=sum/0.36651943;
				System.out.println("natoceny vpravo chcem vlavo");
			}
		}
		else {		 //som otoceny vlavo
			if(sm.getTrackPosition() >= position) { //chcem ist vpravo vzhladom na moju poziciu
				action.steering=-(sum/0.36651943);
				System.out.println("natoceny vlavo chcem vpravo");
			}
			else {								   //chcem ist vlavo vzhladom na moju poziciu
				if(Math.abs(beta)<=alfa) {
					action.steering=sum/0.36651943;
					System.out.println("natoceny vlavo chcem vlavo beta mensia");
				}
				else {
					action.steering=-(diff/0.36651943);
					System.out.println("natoceny vlavo chcem vlavo beta vacsia");
				}
			}
		}
		System.out.println("steering: "+action.steering);
		return action;
	}
	
	
	
	
	
	/**
	 * Metoda urcujuca vztah medzi najdlhsim uhlom a natocenim volantu.
	 * @param longestAngle uhol k najvzdialenejsiemu okraju trate
	 * @return natocenie volantu
	 */
	private double steeringAdjustment(double longestAngle)
	{
		if (Math.abs(longestAngle) > 21)
			if (longestAngle > 0)
				return -1;
			else
				return 1;
		
		return -longestAngle / 21;
	}

	/**
	 * Metoda prebrana zo SimpleDriver sluziaca na navrat na trat.
	 * @param sm senzory
	 * @return akcia smerujuca k nabraniu vhodneho smeru
	 */
	private Action recoverToTrack(SensorModel sm)
	{
		//System.out.println("Prebieha zotavovanie z uviaznutia.");
		Action action = new Action();
		action.accelerate = 1;
					
		// zistime, ci je auto natocene spravnym smerom alebo smeruje von z trate
		if (sm.getAngleToTrackAxis() * sm.getTrackPosition() > 0)
		{
			action.gear = 1;
			action.steering = sm.getAngleToTrackAxis() / (Math.PI / 4);
		}
		else
		{
			action.gear = -1;
			action.steering = -sm.getAngleToTrackAxis() / (Math.PI / 4);
		}
		
		return action;
	}

	/**
	 * Metoda vrati priemer uhlov senzorov zaznamenavajucich najvzdialenejsi okraj trate
	 * v rozsahu <-90; 90>. Priemerovana je len jedna suvisla postupnost senzorov.
	 * @param trackEdgeSensors senzory okrajov trate
	 * @return priemer uhlov senzorov zaznamenavajucich najvzdialenejsi okraj trate
	 */
	private double computeAvgLongestSensorAngle(double[] trackEdgeSensors)
	{
		int max = 0, numberOfLongestSensors = 1;
		double angle = 0;

		// najde index prveho senzora v smere k najvzdialenejsiemu okraju trate
		for (int i = 1; i < trackEdgeSensors.length; i++)
			if (trackEdgeSensors[max] < trackEdgeSensors[i])
				max = i;
		
		// zisti kolko senzorov ma najvyssiu hodnotu
		while (max + numberOfLongestSensors < trackEdgeSensors.length &&
				trackEdgeSensors[max] == trackEdgeSensors[max + numberOfLongestSensors])
			numberOfLongestSensors++;

		// vypocet priemeru, vysledok je z rozsahu <-90; 90>
		for (int i = 0; i < numberOfLongestSensors; i++)
			angle = max + i;
		angle = (angle / numberOfLongestSensors) * 10 - 90;
		
		return angle;
	}

	/**
	 * Metoda vracajuca vhodnu hodnotu prevodoveho stupna.
	 * 
	 * Implementacia nie je urcena na zaradenie do "produkcneho" kodu.
	 * Jej cielom je uplna oddelenost od ostatnych prvkov ovladaca pre
	 * prehladnost vyvoja.
	 */
	private int setGear(SensorModel sensors)
	{
		int gear = sensors.getGear();

		int[]  gearUp = { 9500, 9400, 9500, 9500, 9500, 0 };
		int[]  gearDown = { 0, 3300, 6200, 7000, 7300, 7700 };
		
		double rpm = sensors.getRPM();
		if (gear < 1)
			gear = 1;

		// check if the RPM value of car is greater than the one suggested
		// to shift up the gear from the current one
		else if (gear < 6 && rpm >= gearUp[gear-1])
			gear++;
		
		// check if the RPM value of car is lower than the one suggested
		// to shift down the gear from the current one
		else if (gear > 1 && rpm <= gearDown[gear-1])
			gear--;
		
		return gear;
	}
	
	public void reset()
	{
		System.out.println("restarting the race");
	}

	public void shutdown()
	{
		dS.visualize();
		System.out.println("shutdown");		
	}
}
