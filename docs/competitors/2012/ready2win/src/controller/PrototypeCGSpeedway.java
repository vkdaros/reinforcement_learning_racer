package controller;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import controller.model.TickInfo;


import champ2011client.Action;
import champ2011client.Controller;
import champ2011client.SensorModel;

/**
 * Kontroler vozidla zatacajuci podla najdlhsieho senzora.
 * Verzia prisposobena na prejdenie trate CGSSpeedway.
 */
public class PrototypeCGSpeedway extends Controller
{	
	/*
	 * Hodnoty k ABSfiltru, prebrate zo simpledrivera
	 */
	final float wheelRadius[]={(float) 0.3179,(float) 0.3179,(float) 0.3276,(float) 0.3276};
	final float absSlip=(float) 2.0;
	final float absRange=(float) 3.0;
	final float absMinSpeed=(float) 3.0;

	/*
	 * Hodnoty k ASRfiltru, prebrate z cobostar
	 */
	final float asrSlip=(float) 3.2;
	final float asrRange=(float) 10.86;
	final float asrMaxSpeed=(float) 150;

	RecoveryModule recMod;

	/*
	 * Koeficient frikcie medzi pneumatikou a vozovkou; tato hodnota je urcena na zaklade brzdnej drahy pri 
	 * styroch roznych rychlostiach na okruhu CG speedway, na dalsich troch "Road" tratiach odskusana -> na
	 * Road tratiach je mozne ju pouzivat zatial
	 */
	final float mi=(float)24;
	final float g=(float)9.81;

	// rad sluziaci na komunikaciu s vlaknom vytvarajucim model
	private final BlockingQueue<TickInfo> tickInfoPipe;

	// rad sluziaci na komunikaciu s vlaknom vytvarajucim model
	private final BlockingQueue<SensorInfo> sensorInfoPipe;

	private double[] start = {273.8, 629.4, 926.5, 1030.9, 1264.4, 1661.2, 1880.5};
	private double[] end = {553.7, 862.3, 1020.7, 1209.6, 1311.8, 1673.9, 2045};
	private double[] maxSp = {204.7, 204.7, 110, 110, 204.7, 204.7, 204.7};
	private double[] brakDist = {89, 89, 25, 25, 89, 89, 89};

	private int sector = 0;
	private boolean begin = true;
	private boolean outSector = true;


	public PrototypeCGSpeedway()
	{
		tickInfoPipe = new LinkedBlockingQueue<TickInfo>(1000);

		/*	Thread t = new Thread(new TurnDetector(tickInfoPipe));
		t.setDaemon(true);
		t.setPriority(Thread.MIN_PRIORITY);
		t.start();
		 */	
		sensorInfoPipe = new LinkedBlockingQueue<SensorInfo>(1000);

		/*	Thread t2 = new Thread(new SensorDetector(sensorInfoPipe));
		t2.setDaemon(true);
		t2.setPriority(Thread.MIN_PRIORITY);
		t2.start();
		 */	
		recMod = new RecoveryModule();

	}

	public Action control(SensorModel sm)
	{
		Action action = new Action ();

		// vyletel som na lavu stranu cesty alebo bol v recovrery state left a neje v pozicii kedy moze odovzdat auto modulu riadenia
		if (sm.getTrackPosition()>1 || recMod.isInRecoveryStateLeft())
			return recMod.recoverLeft(sm);

		// vyletel som na pravu stranu cesty alebo bol v recovrery state right a neje v pozicii kedy moze odovzdat auto modulu riadenia
		if (sm.getTrackPosition()<-1 || recMod.isInRecoveryStateRight())

			return recMod.recoverRight(sm);


		/*
		 * rychlost - akceleracia naplno do dosiahnutia maximalnej rychlosti, ktoru je mozne ubrzdit na vzdialenost
		 * najdlhsieho senzora,
		 * vzorec			v = odmocnina(2 * g * mi * brzdna draha)
		 */
		/*		if (sm.getSpeed ()< Math.sqrt(2 * 9.81 * mi * sm.getTrackEdgeSensors()[18 - (int) ((computeAvgLongestSensorAngle(sm.getTrackEdgeSensors())+90)/10)]))
			action.accelerate = filterASR(sm, 1);
		else
			action.brake = filterABS(sm, 1);
		 */		
		/*
		 * rychlost na zaklade naucenia
		 */
		if ((sm.getDistanceFromStartLine() > 2000) && begin)
			action.accelerate = filterASR(sm, 1);
		else
		{
			if (sm.getDistanceFromStartLine() < 100 || sm.getDistanceFromStartLine() > 2000)
				System.out.println(sector);
			begin = false;
			if (sector == 6)
			{
				if (outSector && ((Math.pow(sm.getSpeed(), 2)/(2 * mi * g) - brakDist[sector]) < (((end[sector] + start[sector])/2) - sm.getDistanceFromStartLine() - 2047)))
					action.accelerate = filterASR(sm, 1);
				else
					outSector = false;
			}
			else
			{
				if (outSector && ((Math.pow(sm.getSpeed(), 2)/(2 * mi * g) - brakDist[sector]) < (((end[sector] + start[sector])/2) - sm.getDistanceFromStartLine())))
					action.accelerate = filterASR(sm, 1);
				else
					outSector = false;
			}
			if (sector == 6)
				System.out.println(sector);

			if (!outSector && (sm.getSpeed() > maxSp[sector]))
				action.brake = filterABS(sm, 1);
			if (!outSector && (sector == 6) && (sm.getSpeed() > (maxSp[sector] * 0.6)))
				action.brake = filterABS(sm, 1);
			if (!outSector && (sm.getSpeed() < maxSp[sector]*0.9))
				action.accelerate = filterASR(sm, 1);
			if (!outSector && (sector == 6)&& (sm.getSpeed() < maxSp[sector]*0.6))
				action.accelerate = filterASR(sm, 1);
			if (sm.getDistanceFromStartLine() > end[sector])
			{
				if (sector == 0 & sm.getDistanceFromStartLine() > 2000)
					;
				else
					sector++;
				if (sector == 7)
					sector = 0;
				outSector = true;
			}
		}


		// * riadenie *
		// zmena smeru na zaklade najdlhsieho senzora
		action.steering = steeringAdjustment(computeAvgLongestSensorAngle(sm.getTrackEdgeSensors()));

		// * prevodovka *
		action.gear = setGear(sm);

		// posielanie informacie potrebnej na vytvaranie modelu trate
		/*		try {
			tickInfoPipe.put(new TickInfo(action, sm));
		} catch (InterruptedException e) {
			System.err.println("Error: chyba pri posielani akcie do modelu trate.");
			e.printStackTrace();
		}
		 */
		/*		try
		{
			sensorInfoPipe.put(new SensorInfo((int)(computeAvgLongestSensorAngle(sm.getTrackEdgeSensors())+90)/10, 1, sm.getDistanceFromStartLine()));
		}catch (InterruptedException e) {
			System.err.println("Error: chyba pri posielani akcie do modelu trate.");
			e.printStackTrace();
		}
		 */			
		return action;
	}

	/**
	 * Metoda prebrana zo SimpleDriver sluziaca ako ABS na brzdy
	 * @param sensors senzory
	 * @param brake hodnota brzdenia, ktoru chceme upravit 
	 * @return upravena hodnota brzdenia
	 */
	private float filterABS(SensorModel sensors,float brake){
		// convert speed to m/s
		float speed = (float) (sensors.getSpeed() / 3.6);
		// when spedd lower than min speed for abs do nothing
		if (speed < absMinSpeed)
			return brake;

		// compute the speed of wheels in m/s
		float slip = 0.0f;
		for (int i = 0; i < 4; i++)
		{
			slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];
		}
		// slip is the difference between actual speed of car and average speed of wheels
		slip = speed - slip/4.0f;
		// when slip too high applu ABS
		if (slip > absSlip)
		{
			brake = brake - (slip - absSlip)/absRange;
		}

		// check brake is not negative, otherwise set it to zero
		if (brake<0)
			return 0;
		else
			return brake;
	}
	/**
	 * Metoda prebrana z Cobostar sluziaca ako ASR na plyn
	 * @param sensors senzory
	 * @param accelerate hodnota akceleracie, ktoru chceme upravit 
	 * @return upravena hodnota akceleracie
	 */
	private float filterASR(SensorModel sensors,float accelerate){
		// compute the speed of wheels in m/s
		double currentSpeed = sensors.getSpeed();
		double slip = .0;
		for (int i = 0; i < 4; i++)
		{
			slip += sensors.getWheelSpinVelocity()[i] * wheelRadius[i];
		}
		// slip is the difference between actual speed of car and average speed of wheels
		slip = currentSpeed - slip/4.0*3.6;

		double slipF = .0;
		double slipR = .0;
		slipF = (sensors.getWheelSpinVelocity()[0]+
				sensors.getWheelSpinVelocity()[1])
				* wheelRadius[0];

		slipR = (sensors.getWheelSpinVelocity()[2]+
				sensors.getWheelSpinVelocity()[3])
				* wheelRadius[2];
		slipF = currentSpeed - slipF/2.0*3.6;//preklzovanie prednej napravy
		slipR = currentSpeed - slipR/2.0*3.6;//preklzovanie zadnej napravy
		//aplikuje sa aj na trati, nie len mimo trate
		if (currentSpeed < asrMaxSpeed)
		{
			// when negative slip too high apply ASR
			if (-slipR/2. > asrSlip)
			{
				accelerate = (float) Math.max(0, accelerate + (slipR/2. + asrSlip)/asrRange);
				//System.out.println("ASR akceleracia: "+accelerate);
			}
		}
		return accelerate;
	}

	/**
	 * Metoda urcujuca vztah medzi najdlhsim uhlom a natocenim volantu.
	 * @param longestAngle uhol k najvzdialenejsiemu okraju trate
	 * @return natocenie volantu
	 */
	private double steeringAdjustment(double longestAngle)
	{
		if (Math.abs(longestAngle) > 45)
			if (longestAngle > 0)
				return -1;
			else
				return 1;

		return -longestAngle / 45;
	}

	/**
	 * Metoda prebrana zo SimpleDriver sluziaca na navrat na trat.
	 * @param sm senzory
	 * @return akcia smerujuca k nabraniu vhodneho smeru
	 */
	private Action recoverToTrack(SensorModel sm)
	{
		//System.out.println("Prebieha zotavovanie z uviaznutia.");
		Action action = new Action();
		action.accelerate = 1;

		// zistime, ci je auto natocene spravnym smerom alebo smeruje von z trate
		if (sm.getAngleToTrackAxis() * sm.getTrackPosition() > 0)
		{
			action.gear = 1;
			action.steering = sm.getAngleToTrackAxis() / (Math.PI / 4);
		}
		else
		{
			action.gear = -1;
			action.steering = -sm.getAngleToTrackAxis() / (Math.PI / 4);
		}

		return action;
	}

	/**
	 * Metoda vrati priemer uhlov senzorov zaznamenavajucich najvzdialenejsi okraj trate
	 * v rozsahu <-90; 90>. Priemerovana je len jedna suvisla postupnost senzorov.
	 * @param trackEdgeSensors senzory okrajov trate
	 * @return priemer uhlov senzorov zaznamenavajucich najvzdialenejsi okraj trate
	 */
	private double computeAvgLongestSensorAngle(double[] trackEdgeSensors)
	{
		int max = 0, numberOfLongestSensors = 1;
		double angle = 0;

		// najde index prveho senzora v smere k najvzdialenejsiemu okraju trate
		for (int i = 1; i < trackEdgeSensors.length; i++)
			if (trackEdgeSensors[max] < trackEdgeSensors[i])
				max = i;

		// zisti kolko senzorov ma najvyssiu hodnotu
		while (max + numberOfLongestSensors < trackEdgeSensors.length &&
				trackEdgeSensors[max] == trackEdgeSensors[max + numberOfLongestSensors])
			numberOfLongestSensors++;

		// vypocet priemeru, vysledok je z rozsahu <-90; 90>
		for (int i = 0; i < numberOfLongestSensors; i++)
			angle = max + i;
		angle = (angle / numberOfLongestSensors) * 10 - 90;

		return angle;
	}

	/**
	 * Metoda vracajuca vhodnu hodnotu prevodoveho stupna.
	 * 
	 * Implementacia nie je urcena na zaradenie do "produkcneho" kodu.
	 * Jej cielom je uplna oddelenost od ostatnych prvkov ovladaca pre
	 * prehladnost vyvoja.
	 */
	private int setGear(SensorModel sensors)
	{
		int gear = sensors.getGear();

		int[]  gearUp = { 9500, 9400, 9500, 9500, 9500, 0 };
		int[]  gearDown = { 0, 3300, 6200, 7000, 7300, 7700 };

		double rpm = sensors.getRPM();
		if (gear < 1)
			gear = 1;

		// check if the RPM value of car is greater than the one suggested
		// to shift up the gear from the current one
		else if (gear < 6 && rpm >= gearUp[gear-1])
			gear++;

		// check if the RPM value of car is lower than the one suggested
		// to shift down the gear from the current one
		else if (gear > 1 && rpm <= gearDown[gear-1])
			gear--;

		return gear;
	}

	public void reset()
	{
		System.out.println("restarting the race");
	}

	public void shutdown()
	{
		System.out.println("shutdown");		
	}
}
