package controller;

import gui.NewJFrame;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.SwingUtilities;
//import javax.swing.text.StyledEditorKit.BoldAction;

import sk.stuba.fiit.tp2010.team20.defence.Overtake;
import telemetry.DataStorage;
import telemetry.DataUnit;
import controller.model.Event;
import controller.model.JumpsHandler;
import controller.model.TrackData;
import controller.model.Turn;
import controller.model.TurnDetector;
import controller.model.TickInfo;
import champ2011client.Action;
import champ2011client.Controller;
import champ2011client.SensorModel;

/**
 * A controller of a vehicle driving according to the longest sensor.
 */
public class PrototypeFirstTime extends Controller {
	DataStorage dS;
	Overtake overtakeMod;
	// data about the track
	TrackData trackData = null;
	// recovery module
	RecoveryModule recMod;
	// drive module
	DriveModul driveMod;
	// event
	Event event;
	// 9 senzor bez odchylky
	// jump handler - 3D model
	JumpsHandler jumpsHandler;
	double trackEdgeSenzorStraight;

	// gear
	// queued RPM in the last 30 turns
	// private Deque<Double> rpm30Tick = new ArrayDeque<Double>();
	// private double clutchCur=0;
	// boolean RPMGrows=false;
	// boolean notOnTrack =false;
	// int dontGear =0;// 7 tikov trva kym otacky zacnu po radeni stupat
	/*
	 * A friction coefficient between a tyre and the road; this value is determined int he first lap
	 * from the slowdown from 150 to stop and flags
	 */
	// private double friction=16;
	// private boolean kolesaStoja = false,brzdi =false,vypocitajFriction=false;
	// private double brakeStartDist;
	// a speed intended for friction measurement
	// private double breakSpeed=125;

	// a queue for communication with a thread creating model
	BlockingQueue<TickInfo> tickInfoPipe;
	// flag for measurement track width deviation
	private boolean trackWidthMeasuring = true;
	int trackWidtgMeasuringCount = 16;
	// determines whether the middle of the track was reached after the start
	// private boolean straightMid=false;
	// track length
	private double trackLength = 0;
	private boolean trackLengthMeasuring = true;

	// recording radiuses on the track sectors
	private int trackRadius = 1;

	// private Deque<Integer> turnDirection = new ArrayDeque<Integer>();

	// recovery
	private int ticCount = 0;
	private boolean afterStart = false;

	// line assist
	private LineAssistModul lnaMod;
	boolean reCount = false;

	// focus
	private static Boolean requestedChange = false;
	private static int desiredFocus;
	private static double lastChange;
	private static double focusValue = -1;
	public static Object lock = new Object();
	// steering
	// private double lastSteer=0;
	// if it is in the first lap and no file was loaded, then true. else false
	private boolean isFirstLap = true;
	boolean rovinaNemeratZakruty = false;
	// events
	private boolean zmenRychlost = true;
	private boolean zmenRychlost2=true;
	private int actualLap = 0;
	private boolean isActualLap = false;
	// friction adjustment
	private int adjustmentFriction = 0;
	private boolean isFrictionFinal = false;

	// launch assist decision
	boolean lunchAssistSteerOn = true;
	TurnDetector turnDetector = null;

	FileWriter outFile;
	PrintWriter out;
	private String trackName;
	final PrototypeFirstTime me = this;

	boolean brzdiuz = false;
	private boolean inLap = true;

	public PrototypeFirstTime() {
		tickInfoPipe = new LinkedBlockingQueue<TickInfo>(1000);
			turnDetector= new TurnDetector(tickInfoPipe);
		

		dS = new DataStorage();
		overtakeMod = new Overtake();
		recMod = new RecoveryModule();
		driveMod = new DriveModul();
		lnaMod = new LineAssistModul();
		event = new Event();
		// PrototypeFirstTime me = this;
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				NewJFrame inst = new NewJFrame(me);
//				inst.setLocationRelativeTo(null);
//				inst.setVisible(true);
//			}
//		});
	}

	public synchronized void stop() {
		brzdiuz = true;
	}

	public synchronized void start() {
		brzdiuz = false;
	}

	public boolean isInWarmUp() {
		return getStage() == Stage.WARMUP;
	}

	public boolean isInQualify() {
		return getStage() == Stage.QUALIFYING;
	}

	public boolean isInRace() {
		return getStage() == Stage.RACE;
	}

	public Action control(SensorModel sm) {
		//TODO add conditions for what is allowed in specific stages
		//lap counter
		if(sm.getDistanceFromStartLine() <= 2 && !isActualLap)
		{
			actualLap +=1;
			isActualLap = true;
			//System.out.println("kolo:"+ actualLap);
			//TODO check if all turn are optimized
			if(!isFrictionFinal && actualLap>2 && trackData.getFriction()<40 && isInWarmUp()){
				if(trackData.getOffTrackCount()<=1){
					trackData.setFriction(trackData.getFriction()+2);
					driveMod.setFriction(trackData.getFriction());
					//System.out.println("zvysujem friction: "+trackData.getFriction());
					
				}else if(trackData.getOffTrackCount()>=trackData.getTurnList().size()*0.3){
					//upravujem frikciu podla percenta vyleteni
					trackData.setFriction(trackData.getFriction()-(6.6667*((1.0/trackData.getTurnList().size())*(double)trackData.getOffTrackCount())));
					driveMod.setFriction(trackData.getFriction());
					//TODO trackData.clearAdjustments();
					//System.out.println("znizujem friction: "+trackData.getFriction()+", "+(6.6667*((1.0/trackData.getTurnList().size())*(double)trackData.getOffTrackCount())));
				}else{
					isFrictionFinal=true;
					
				}
				
			}
			//System.out.println("pocet: "+trackData.getOffTrackCount());
			trackData.nulOffTrackCount();
			
		}
		else if(sm.getDistanceFromStartLine() > 2 && isActualLap)
		{
			isActualLap = false;
		}
		//zistenie ci sa bude globalna frikcia upravovat hore alebo dole
		/*if(actualLap == 3 && adjustmentFriction == 0)
		{
			if(event.events.size() == 0)
			{
				adjustmentFriction = 1; // global friction will increase
			}
			else if((trackData.getTurnList().size()/2) <= event.events.size())
			{
				adjustmentFriction = 2; // global friction will decrease
			}
		}*/
		//upravovanie globalnej frikcie
		
		
		/*if(!isFrictionFinal)
		{
			//increasing friction
			if(adjustmentFriction == 1 && event.events.size() == 0 && isActualLap) // zvysovat frikciu
			{
				double fric = trackData.getFriction() + 2;
				trackData.setFriction(fric);
				System.out.println("friction: " + trackData.getFriction());
			}
			else if(adjustmentFriction == 1 && event.events.size() != 0 && isActualLap)
			{
				double fric = trackData.getFriction() - 2;
				trackData.setFriction(fric);
				isFrictionFinal = true;
				event.events.clear();
				System.out.println("friction: " + trackData.getFriction());
			}
			//decreasing frikcie
			if(adjustmentFriction == 2 && (trackData.getTurnList().size()/2) <= event.events.size() && isActualLap) // zvysovat frikciu
			{
				double fric = trackData.getFriction() - 2;
				trackData.setFriction(fric);
				event.events.clear();
				System.out.println("friction: " + trackData.getFriction());
			}
			else if(adjustmentFriction == 2 && (trackData.getTurnList().size()/2) > event.events.size() && isActualLap)
			{
				isFrictionFinal = true;
				System.out.println("friction: " + trackData.getFriction());
			}						
		}*/

		// determines whether steering should be computed via launch Assist

		// determines whether the start sequence is fineshed (100 ticks)
		ticCount++;
		if (ticCount == 100)
			afterStart = true;
		Action action = new Action();

		checkFocus(sm, action);

		double radius;
		// determines radius
		/*if ((sm.getDistanceFromStartLine() > trackRadius)
				&& (sm.getDistanceFromStartLine() < (trackRadius + 3))) {
			trackRadius += 3;
			if (sm.getTrackPosition() > 0)
				radius = computeRadius(sm, 13);
			else
				radius = computeRadius(sm, 3);

			if (radius < 2000)
				// System.out.println(radius);
				;
		}*/
		// removes deviation from the track edge sensor with an index 9
		double straightSensors[] = new double[] { sm.getTrackEdgeSensors()[1],
				sm.getTrackEdgeSensors()[2], sm.getTrackEdgeSensors()[9],
				sm.getTrackEdgeSensors()[16], sm.getTrackEdgeSensors()[17] };
		trackEdgeSenzorStraight = SimpleNoisyRemover.average(straightSensors);
		// track width determination

		// trackData was loaded from the file
		
		if (trackData.isLoadData() && isFirstLap && !isInWarmUp()) {
			isFirstLap = false;
			lnaMod.setTrackWidth(trackData.getTrackWidth());
			trackWidthMeasuring = false;
			driveMod.setTrackWidth(trackData.getTrackWidth());
			driveMod.setFriction(trackData.getFriction());
			//event.setEvents(trackData.getEventList());
			event.setJumps(trackData.getJumpList());
			if (!reCount) {
				trackData.reCountEnterPos();
				trackData.reCountBreakDist();
				reCount = true;
			}
		} else if (isFirstLap)
			isFirstLap = (sm.getLastLapTime() == 0.0);
		//passed to the second lap and track data was not loaded
		if (!trackData.isLoadData() && sm.getDistanceFromStartLine() < 2
				&& sm.getLastLapTime() != 0 && isInWarmUp()) {
			if(!turnDetector.isExit())
				turnDetector.stopThread();
			trackData.iterate(sm, actualLap, isInRace());
			if (!reCount) {
				trackData.reCountEnterPos();
				trackData.reCountBreakDist();
				reCount = true;
			}
		}
		
		if(isFirstLap)
			checkFocus(sm, action);
		
		/*
		 * if (trackWidthMeasuring ) {
		 * trackData.setTrackWidth(sm.getTrackEdgeSensors
		 * ()[0]+sm.getTrackEdgeSensors()[18]);
		 * //trackData.setTrackWidth(trackWidth);
		 * //System.out.println(trackWidth+"sirka trate");
		 * //System.out.println(sm.getTrackPosition());
		 * driveMod.setTrackWidth(trackData.getTrackWidth());
		 * lnaMod.setTrackWidth(trackData.getTrackWidth()); trackWidthMeasuring
		 * = false; }
		 */
		if (trackWidthMeasuring && trackWidtgMeasuringCount >= 0 && isInWarmUp()) {
			trackData.setTrackWidth(trackData.getTrackWidth()
					+ sm.getTrackEdgeSensors()[0]
					+ sm.getTrackEdgeSensors()[18]);
			if (trackWidtgMeasuringCount < 16)
				trackData.setTrackWidth(trackData.getTrackWidth() / 2);
			// System.out.println(trackData.getTrackWidth());
			// trackData.setTrackWidth(trackWidth);
			// System.out.println(trackWidth+"sirka trate");
			// System.out.println(sm.getTrackPosition());
			// driveMod.setTrackWidth(trackWidth);
			if (trackWidtgMeasuringCount == 0) {
				// trackWidth=(Util.round(trackWidth, 0));
				trackData
						.setTrackWidth(Util.round(trackData.getTrackWidth(), 0));
				driveMod.setTrackWidth(trackData.getTrackWidth());
				lnaMod.setTrackWidth(trackData.getTrackWidth());
				trackWidthMeasuring = false;
			}

			trackWidtgMeasuringCount--;
		}

		// determines track length
		if (trackLengthMeasuring && isInWarmUp()) {
			if ((sm.getDistanceFromStartLine() < trackLength)
					&& (trackLength > 0)) {
				trackLengthMeasuring = false;
				// System.out.println(trackLength+" track length");
				dS.setLength(trackLength);
			} else
				trackLength = sm.getDistanceFromStartLine();
		}

		// ************ EVENTS and JUMPS **************
		// //events are learned after the first lap and outside the race with opponents
		//System.out.println(sm.getZ()+" "+!isFirstLap);
		if (sm.getZ() > 0.4 && !isFirstLap && isInWarmUp()) {
			event.addJump(sm.getDistanceFromStartLine(), 0, sm.getZ());
		}
		if (!isFirstLap && isInWarmUp()) {
			if ((sm.getTrackPosition() > 1 || recMod.isInRecoveryStateLeft())) {
				if (zmenRychlost2) {
					event.setSpeed(sm.getDistanceFromStartLine(), sm);
					zmenRychlost2 = false;
					//System.out.println("--- menim");
				}
			} else {
				if ((sm.getTrackPosition() < -1 || recMod
						.isInRecoveryStateRight())) {
					if (zmenRychlost2) {
						event.setSpeed(sm.getDistanceFromStartLine(), sm);
						zmenRychlost2 = false;
						//System.out.println("--- menim");
					}
				} else {
					zmenRychlost2 = true;
				}
			}
		}

		// by paro
		if (sm.getDistanceFromStartLine() < 2) {
			trackData.setJumpList(event.getJumps());
			//System.out.println(event.getJumps().size()+" < velkost");
			// System.out.println("na zaciatku "+event.getEvents().size());
		}


		////events are learned after the first lap and outside the race with opponents
		if (!isFirstLap && isInWarmUp()) {
			//vyletel som 
			if(((sm.getTrackPosition() > 1 || recMod.isInRecoveryStateLeft()) || (sm.getTrackPosition() < -1 || recMod.isInRecoveryStateRight())) ){
				//in turn - updating turn
				
				if(isInTurn(sm, trackData.getNextTurn()) && zmenRychlost){
					//System.out.println("tu by som mal zvysit");
					//zmenim break dist o 5% lateralnej rychlosti
					if(trackData.getNextTurn().getSpeedIndex()>1){
						trackData.getNextTurn().setIndexAdjust(trackData.getNextTurn().getIndexIncr());
						trackData.getNextTurn().setAdjustSpeed(trackData.getNextTurn().getSpeedIncr());
					} else {
						trackData.getNextTurn().setAdjustSpeed(trackData.getNextTurn().getSpeed()*0.05);
						trackData.getNextTurn().setIndexAdjust(trackData.getNextTurn().getSpeedIndex()*0.10);
					}
					trackData.addOffTrackCount();
					//trackData.getNextTurn().setSpeedIndex(trackData.getNextTurn().getSpeedIndex()*0.95);
					trackData.getNextTurn().setOptimalized(true);
					//System.out.println("uberam breakdist o v aktzakrute: "+trackData.getNextTurn().getSpeedIndex()*0.10);
				}//if the vehicle gets out the turn, it is inserted into the turn before getting out
				else if(trackData.getLastTurn()!=null && zmenRychlost){
					//System.out.println("tu by som mal znizit");
					if(trackData.getLastTurn().getSpeedIndex()>1){
						trackData.getLastTurn().setIndexAdjust(trackData.getLastTurn().getIndexIncr());
						trackData.getLastTurn().setAdjustSpeed(trackData.getLastTurn().getSpeedIncr());
					} else {
					trackData.getLastTurn().setAdjustSpeed(trackData.getLastTurn().getSpeed()*0.05);
					trackData.getLastTurn().setIndexAdjust(trackData.getLastTurn().getSpeedIndex()*0.10);
					}
					trackData.addOffTrackCount();
					//trackData.getLastTurn().setSpeedIndex(trackData.getLastTurn().getSpeedIndex()*0.95);
					trackData.getLastTurn().setOptimalized(true);
					//System.out.println("uberam breakdist o v lastzakrute: "+trackData.getLastTurn().getSpeedIndex()*0.10);
					
				}
				
				zmenRychlost = false;
			}else{
				zmenRychlost = true;
			}
			if(trackData.isInTurn(sm) && !isInRace()) {
				trackData.getNextTurn().setLateralSpeed(Math.abs(sm.getLateralSpeed()));
				//System.out.println(sm.getLateralSpeed());
			}
		}
		/*if (sm.getZ() > 0.4 && !isFirstLap) {
			event.addJump(sm.getDistanceFromStartLine(), 0, sm.getZ());

		}
		if (!isFirstLap) {
			if ((sm.getTrackPosition() > 1 || recMod.isInRecoveryStateLeft())) {
				event.addEvent(sm.getDistanceFromStartLine(), 0,
						trackData.getNextTurn(), sm);
				trackData.getNextTurn().setOptimalized(true);
				if (zmenRychlost) {
					event.setSpeed(sm.getDistanceFromStartLine(), sm);
					zmenRychlost = false;
				}
			} else {
				if ((sm.getTrackPosition() < -1 || recMod
						.isInRecoveryStateRight())) {
					event.addEvent(sm.getDistanceFromStartLine(), 0,
							trackData.getNextTurn(), sm);
					trackData.getNextTurn().setOptimalized(true);
					if (zmenRychlost) {
						event.setSpeed(sm.getDistanceFromStartLine(), sm);
						zmenRychlost = false;
					}
				} else {
					zmenRychlost = true;
				}
			}

			// by paro
			if (sm.getDistanceFromStartLine() < 2) {
				trackData.setEventList(event.getEvents());
				trackData.setJumpList(event.getJumps());
				// System.out.println("na zaciatku "+event.getEvents().size());
			}
			if(trackData.isInTurn(sm)) {
				trackData.getNextTurn().setLateralSpeed(Math.abs(sm.getLateralSpeed()));
				//System.out.println(sm.getLateralSpeed());
			}
		}*/
		
		/* * * * * R E C O V E R Y * * * * * T H E * B E G I N N I N G * * * * */

		/*
		 * in every tick it is checked, whether the pilot is in specific recovery situations
		 */

		// if after start, specific situation is checked
		if (afterStart)
			recMod.checkRecovery(sm);

		// blocked int the track edge (perpendicularly) - a transfer to the new recovery state
		if ((recMod.isBlocked()) && afterStart
				//&& (-1 < sm.getTrackPosition() && sm.getTrackPosition() < 1)
				&& !brzdiuz) {
			if (sm.getTrackPosition() < 0)
				return recMod.recoverRight(sm);
			else if (sm.getTrackPosition() > 0)
				return recMod.recoverLeft(sm);
		}

		// blocked on the hill
		if ((recMod.isBlocked()) && afterStart && !brzdiuz)
			return recMod.goStraight(sm);

		// ran out of track to the left side of the road or is in the recovery state left and
		// it is not in position that driving control could overtake control
		if ((sm.getTrackPosition() > 1 || recMod.isInRecoveryStateLeft())
				&& (!recMod.isBlocked())) {
			// notOnTrack=true;
			return recMod.recoverLeft(sm);
		} // else if (recMod.isBlocked())

		// ran out of track to the right side of the road or is in the recovery state right and
		// it is not in position that driving control could overtake control
		if ((sm.getTrackPosition() < -1 || recMod.isInRecoveryStateRight())
				&& (!recMod.isBlocked())) {
			// notOnTrack=true;
			return recMod.recoverRight(sm);
		} // else if (recMod.isBlocked())

		// on the in the opposite direction - more to the left
		if ((sm.getAngleToTrackAxis() > Math.PI / 2.0)
				&& (sm.getAngleToTrackAxis() < Math.PI)) {
			// System.out.println("// on the in the opposite direction - more to the left");
			return recMod.recoverRight(sm);
		}

		// // on the in the opposite direction - more to the right
		if ((sm.getAngleToTrackAxis() < -Math.PI / 2.0)
				&& (sm.getAngleToTrackAxis() > -Math.PI)) {
			// System.out.println("// on the in the opposite direction - more to the right");
			return recMod.recoverLeft(sm);
		}

		/* * * * * R E C O V E R Y * * * * * T H E * E N D * * * * */

		/*
		 * speed - full acceleration until max speed is reached,
		 * which is the speed from which it is possible to stop within the length of the longest sensor,
		 * v = sqrt(2 * g * mi * brzdna draha)
		 */
		double speedAdjustment;

		/*
		 * if(!isFirstLap) straightMid=true;
		 */
		// nieje na strede
		
		if (trackData.isLoadData() || !isFirstLap) {
			action = lnaMod.getSpeedFromNextTurn(action, sm, trackData,
					event.getSpeed(trackData.getActEvent()),
					trackEdgeSenzorStraight);
			// System.out.println("accel: <"+action.accelerate);
		} else {
			// if the speed is lower than the calculated speed, then accelerating
			if (!driveMod.isStraightMid())
				speedAdjustment = 120;
			// je v prvom kole
			else if (isFirstLap)
				speedAdjustment = 60;
			// je v zakrute a je prudka
			else
				speedAdjustment = 0;

			if ((sm.getSpeed() + speedAdjustment + event.getSpeed(trackData
					.getActEvent())) < Math.sqrt(2 * 9.81
					* driveMod.getFriction()
					* (Math.abs(trackEdgeSenzorStraight - 4)))) {
				action.accelerate = ESP.filterASR(sm, ESP.actionAccelAktual);
				ESP.actionBrakeAktual = 0;
				ESP.actionBrakeTickBack = 0;
			}
			// ak moj speed klesne pod 20 pridavam aby som sel minimalne 20
			// uplatnuje sa najme v prvom kole ked ja speed adjustment 60 alebo
			// 120
			else if (sm.getSpeed() < 20) {
				action.accelerate = 0.8;
				ESP.actionBrakeAktual = 0;
				ESP.actionBrakeTickBack = 0;
			}
			// braking, if the computed speed is higher than actual speed brzdim ak vypocitana rychlost je mensia ako moja aktualna
			else {
				action.brake = ESP.filterABS(sm, ESP.actionBrakeAktual);
				ESP.actionAccelAktual = 0;
				ESP.actionAccelTickBack = 0;
			}
		}
		// * steering *
		// a change of direction according to the longest sensor
		// action.steering =
		// steeringAdjustment(computeAvgLongestSensorAngle(sm.getTrackEdgeSensors()));
		if (isFirstLap) {
			
			int max = 0;
			
			for (int i = 1; i < sm.getTrackEdgeSensors().length; i++)
				if (sm.getTrackEdgeSensors()[max] < sm.getTrackEdgeSensors()[i]
						&& i != 1 && i != 2 && i != 16 && i != 17)
					max = i;
			
			action.steering = driveMod.firstLapSteer(trackEdgeSenzorStraight,
					sm.getTrackEdgeSensors()[max], sm.getAngleToTrackAxis(),
					sm.getTrackPosition(), max);
			// System.out.println("first lap steer");
		}
		// System.out.println("deviation removal : "+trackEdgeSenzorStraight);

		// straightMid=driveMod.isStraightMid();
		// rovinaNemeratZakruty=driveMod.isRovinaNemeratZakruty();

		// LINE ASSIST
		// *************************************/
		// TrackData actualisation on request
		// *************************************/
		trackData.iterate(sm, actualLap, isInRace());

		// not in the first lap, different steering
		if (!isFirstLap) {
			action.steering = driveMod.steerNormal(sm.getTrackEdgeSensors(),
					sm.getDistanceFromStartLine(), sm.getAngleToTrackAxis(),
					sm.getTrackPosition(), trackData.getNextTurn(),
					trackData.getLastTurnEnd(), trackData.isTurnFront());
			// System.out.println("normal steer");
		}

		// steering correction according to the type of the turn
		if (sm.getLastLapTime() != 0 || trackData.isLoadData()) {
			action = lnaMod.assist(sm, action, trackData);
			// System.out.println(trackData.getNextTurn().getPeakValue());
		}
		// LINE ASSIST
		// *************************************/
		// END
		// *************************************/

		// if this by any chance doesnt work, then variable "|| !notOnTrack" should be uncommented
		if (!isInAir(sm))
			action = driveMod.getGear(sm, action);
		else
			action.gear = sm.getGear();
		//if track data is not loaded, send dat into the pipe
		if(isFirstLap){
			try {
				tickInfoPipe.put(new TickInfo(action, sm, !driveMod
						.isRovinaNemeratZakruty(), sm.getTrackPosition(), Math
						.sqrt(2 * 9.81 * driveMod.getFriction()
								* (Math.abs(trackEdgeSenzorStraight - 4)))));
			} catch (InterruptedException e) {
				System.err.println("Failed to send tickinfo to TurnDetector thread");
				e.printStackTrace();
			}
		}
		// determines whether steering should be computed with the launch assist until the first turn is not reached
		if (isInRace()
				&& trackData.getNextTurn().getStartDistance()
						- trackData.getTrackWidth() < sm
						.getDistanceFromStartLine()
				&& trackData.getNextTurn().getEndDistance() > sm
						.getDistanceFromStartLine() && lunchAssistSteerOn)
			lunchAssistSteerOn = false;
		if (isInRace()) {
			if (lunchAssistSteerOn && trackData.isLoadData()) {
				action.steering = driveMod.launchAssitSteer(sm, 0.0);
			} else {
				if (trackData.getNextTurn() != null)
					action = overtakeMod.overtaking(sm, action,
							trackData.getTrackWidth(), trackData.getNextTurn());
				if (action.brake > 0) {
					ESP.actionAccelAktual = 0;
					ESP.actionAccelTickBack = 0;
				}
			}
		}

		if (driveMod.getFriction() == 16 && isFirstLap) {
			action = driveMod.getFriction(sm, trackEdgeSenzorStraight, action);
			ESP.actionAccelAktual = 1;
			ESP.actionAccelTickBack = 1;
			trackData.setFriction(driveMod.getFriction());
		}

		/*
		 * if(friction ==16 ){ friction=getFriction(sm, action);
		 * ESP.actionAccelAktual = 1; ESP.actionAccelTickBack = 1; }
		 */
		/*
		 * if(sm.getSpeed()>100) System.out.println(sm.getCurrentLapTime());
		 */
		// System.out.println(Math.toDegrees(sm.getAngleToTrackAxis()));
		// System.out.println(-computeAvgLongestSensorAngle(sm.getTrackEdgeSensors()));
		// System.out.println("straight: "+rovinaNemeratZakruty+", angle: "+sm.getAngleToTrackAxis()+", "+"steer: "+action.steering);
		// lastSteer=action.steering;

		// System.out.println(sm.getZ());

		// getSensor9Avg(sm);
		// for(int k=0;k<sm.getOpponentSensors().length;k++)
		// System.out.print(sm.getOpponentSensors()[k]+", ");

		// System.out.println();
		// action=OpponentDetector.monitor(action, sm, trackData);
		// action = simpleOvertakeAdjust(sm, action);

		// action = overtakeMod.overtaking(sm, action,
		// trackData.getTrackWidth());
		// System.out.println("pisem next "+trackData.getNextTurn().getStartDistance());
		/*if (brzdiuz) {
			action.accelerate = 0;
			action.brake = 1;
			ESP.actionAccelAktual = 0;
			ESP.actionAccelTickBack = 0;
		}*/
		
		double accel;
		if (action.accelerate > 0)
			accel = action.accelerate;
		else
			accel = -action.brake;
		if (dS.getCount() > 0) {
			if (((int) sm.getDistanceRaced()) > (int) dS.getLast()
					.getDistance()) {
				dS.store(new DataUnit(action.steering, sm.getSpeed(), sm
						.getRPM(), action.gear, sm.getTrackPosition(), sm
						.getCurrentLapTime(), sm.getDistanceRaced()));
				if ((inLap) && (sm.getDistanceFromStartLine() > 0)
						&& (sm.getDistanceFromStartLine() < 5)) {
					inLap = false;
					dS.addFirstMeters(dS.getCount() - 1);
				}
				if ((!inLap) && (sm.getDistanceFromStartLine() > 5))
					inLap = true;
			}
		} else
			dS.store(new DataUnit(accel, sm.getSpeed(), sm.getRPM(),
					action.gear, sm.getTrackPosition(), sm.getCurrentLapTime(),
					sm.getDistanceRaced()));

		return action;
	}

	/**
	 * find out if the vehicle is off the ground (if it is over 0.4)
	 * 
	 * @param sm
	 * @return
	 */
	boolean isInAir(SensorModel sm) {
		return (sm.getZ() > 0.4);
	}

	/**
	 * A method for requesting focues change.
	 * 
	 * @param desiredFocus desired focus in the range <-90, 90>
	 */
	public static void requestFocusChange(int desiredFocus) {
		synchronized (requestedChange) {
			PrototypeFirstTime.desiredFocus = desiredFocus;
			requestedChange = true;
		}
	}

	/**
	 * find out whether is in the turn in the parameter
	 * 
	 * @param sm
	 * @param aktTurn
	 * @return
	 */
	private boolean isInTurn(SensorModel sm, Turn aktTurn) {
		if (aktTurn != null)
			return aktTurn.getStartDistance() < sm.getDistanceFromStartLine()
					&& aktTurn.getEndDistance() > sm.getDistanceFromStartLine();
		return false;
	}

	/**
	 * 
	 * Overridden method for setting of angles of sensors - sensors 1,2,16,17 were set in the direction oft sensor 9,
	 * so in parallel with the vehicle axis, see isSensorIndexStraight for changes
	 */
	public float[] initAngles() {
		float[] angles = new float[19];
		for (int i = 0; i < 19; ++i)
			angles[i] = -90 + i * 10;
		// see method isSensorIndexStraight
		angles[1] = angles[9];
		angles[2] = angles[9];
		angles[17] = angles[9];
		angles[16] = angles[9];

		return angles;
	}

	/**
	 * A method for acquiring new value of focus after calling requestFocusChange. The method should be called from the method control.
	 * 
	 * @param sm sensor model
	 * @param action action
	 */
	private void checkFocus(SensorModel sm, Action action) {
		// update value about the last change when crossing the start line
		if (sm.getCurrentLapTime() < lastChange)
			lastChange = sm.getCurrentLapTime() - sm.getLastLapTime();
		// getting a new value and informing waiting its acquisition
		if (focusValue == -2) {
			focusValue = sm.getFocusSensors()[2];
			synchronized (lock) {
				lock.notify();
				TurnDetector.setFocusValue(focusValue);
			}
		}
		// focus change
		synchronized (requestedChange) {
			if (requestedChange)
				if (sm.getCurrentLapTime() > lastChange + 1) // focus can be changed every 1s
				{
					action.focus = desiredFocus;
					requestedChange = false;
					lastChange = sm.getCurrentLapTime();
					focusValue = -2; // -2 means that a new value is requested
				} else {
					requestedChange = false;
					TurnDetector.setFocusValue(-3); // -3 means that the method was called too soon
					synchronized (lock) {
						lock.notify();
					}
				}
		}
	}

	/**
	 * a method determining radius of the track sector based on three trackEdge sensors
	 * 
	 * @param sensors sensor model
	 * @param first first among the tree sensors, from which is the radius calculated
	 * @return radius of the track sector
	 */
	private double computeRadius(SensorModel sensors, int first) {
		double x1 = Math.cos(Math.PI / 6)
				* sensors.getTrackEdgeSensors()[first];
		double y1 = Math.sin(Math.PI / 6)
				* sensors.getTrackEdgeSensors()[first];
		double x2 = Math.cos(Math.PI / 4.5)
				* sensors.getTrackEdgeSensors()[first + 1];
		double y2 = Math.sin(Math.PI / 4.5)
				* sensors.getTrackEdgeSensors()[first + 1];
		double x3 = Math.cos(Math.PI / 3.6)
				* sensors.getTrackEdgeSensors()[first + 2];
		double y3 = Math.sin(Math.PI / 3.6)
				* sensors.getTrackEdgeSensors()[first + 2];

		double a, b, c, s;

		a = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
		b = Math.sqrt(Math.pow((x2 - x3), 2) + Math.pow((y2 - y3), 2));
		c = Math.sqrt(Math.pow((x3 - x1), 2) + Math.pow((y3 - y1), 2));
		s = (a + b + c) / 2;

		return (a * b * c) / (4 * Math.sqrt(s * (s - a) * (s - b) * (s - c)));
	}

	public void reset() {
		System.out.println("restarting the race");
	}

	public void shutdown() {
		// out.close();
		// trackData=new TrackData();
		// trackData.setFriction(driveMod.getFriction());
		trackData.setJumpList(event.getJumps());
		//	trackData.setEventList(event.getEvents());

		// trackData.setEventList(event.events);
		// trackData.setTrackWidth(trackData.getTrackWidth());
		// trackData.setTurnList(turnList);
		try {
			saveTrackData("trackData_" + trackName + ".r2w", trackData);
			System.out.println("track data successfully saved into file: "
					+ "trackData_" + trackName + ".r2w");
		} catch (FileNotFoundException e) {
			System.err.println("Failed to sava  data!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Failed to sava  data!");
			e.printStackTrace();
		}
		String directoryName = "telemetry";
		createTelemetryDirIfNeeded(directoryName);
		try {
			directoryName = directoryName.concat(
					System.getProperty("file.separator")).concat(trackName);
			createTelemetryDirIfNeeded(directoryName);
			String identifier = DateFormat.getDateTimeInstance().format(
					new Date());

			saveTelemetryData(directoryName, identifier.replaceAll(":", "_")
					.concat(".tad"), dS);
			System.out.println("telemetry data successfully saved into file: "
					+ identifier + ".tad");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// dS.visualize();
		System.out.println("shutdown");
	}

	private void saveTrackData(String path, Object obj)
			throws FileNotFoundException, IOException {
		File file = new File(path);
		if (!file.exists())
			file.createNewFile();
		ObjectOutput outPut = new ObjectOutputStream(new BufferedOutputStream(
				new FileOutputStream(file)));
		outPut.writeObject(obj);
		outPut.close();
	}

	/**
	 * loads trackData from the file on the road
	 * 
	 * @param path file path
	 * @return trackData if it was succesfuly loaded, otherwise null
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private TrackData readTrackData(String path) throws FileNotFoundException,
			IOException, ClassNotFoundException {
		File file = new File(path);
		ObjectInput input;
		Object readObject;
		if (file.exists()) {
			input = new ObjectInputStream(new BufferedInputStream(
					new FileInputStream(file)));
			readObject = input.readObject();
			input.close();
			if (readObject instanceof TrackData)
				return (TrackData) readObject;
		}

		return null;

	}
	/**
	 * overriden
	 */
	public void setTrackName(String trackName) {
		//TODO conditions need to be done for specific stages of the competition a switch should be added
		this.trackName = trackName;
		try {
			trackData = readTrackData("trackData_" + trackName + ".r2w");
			//checks whether track data obtains all data
			if((trackData!=null && !trackData.loadedComplete()) || isInWarmUp())
				trackData=null;
			
			if (trackData == null) {
				//trackadata is null, so a thread for turn recording is created
				turnDetector.setDaemon(true);
				turnDetector.setPriority(Thread.MIN_PRIORITY);
				turnDetector.start();
				trackData = new TrackData();
				trackData.setLoadData(false);
			} else {
				trackData.setLoadData(true);
				trackData.reCountEnterPos();
				trackData.reCountBreakDist();
				trackData.setOptimalized();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createTelemetryDirIfNeeded(String directoryName) {
		File theDir = new File(directoryName);

		// if the directory does not exist, create it
		if (!theDir.exists())
			theDir.mkdir();
	}

	private void saveTelemetryData(String directoryName, String fileName,
			Object obj) throws FileNotFoundException, IOException {
		File file = new File(directoryName
				+ System.getProperty("file.separator") + fileName);
		if (!file.exists())
			file.createNewFile();
		ObjectOutput outPut = new ObjectOutputStream(new BufferedOutputStream(
				new FileOutputStream(file)));
		outPut.writeObject(obj);
		outPut.close();
	}
	/**
	 * 
	 * @param oppSenz
	 * @param trackSenz
	 * @param kriterium a bound in metres that determines surface under which the sensor overlaps
	 * @return prekryte trackedgesenzor
	 */
	/*
	 * private double[] prekriOpponentSenzorySTrackEdge(double[]
	 * oppSenz,double[] trackSenz,double kriterium,SensorModel sm){
	 * 
	 * double[] pom=new double[19];
	 * 
	 * for (int i = 9; i < 28; i++) { pom[i-9]=oppSenz[i]; }
	 * 
	 * 
	 * for(int i=0;i<trackSenz.length;i++){ if(pom[i]<kriterium ){
	 * System.out.println
	 * ("nahradzam: "+trackSenz[i]+" tymto: "+pom[i]+" na indexoch "
	 * +i+" a "+(i)); trackSenz[i]=0; if(i>9 && i<17 &&
	 * sm.getTrackPosition()>-0.9){ trackSenz[i+1]=0; trackSenz[i+2]=0; }else
	 * if(i<9 && i>1 && sm.getTrackPosition()<0.9){ trackSenz[i-1]=0;
	 * trackSenz[i-2]=0; } else if(i==9){ trackSenz[i+1]=0; trackSenz[i-1]=0; }
	 * else if (i>1 && i<17){ trackSenz[i+1]=0; trackSenz[i-1]=0;
	 * trackSenz[i+2]=0; trackSenz[i-2]=0; }
	 * 
	 * } }
	 * 
	 * return trackSenz; }
	 */
	/**
	 * a method for file writing
	 * 
	 * @param sm
	 * @return
	 */
	/*
	 * private double getSensor9Avg(SensorModel sm){ double pom = 0;
	 * 
	 * out.println(sm.getTrackEdgeSensors()[1]+" "+sm.getTrackEdgeSensors()[2]+" "
	 * +sm.getTrackEdgeSensors()[9]+" "+sm.getTrackEdgeSensors()[16]+" "+sm.
	 * getTrackEdgeSensors()[17]); return pom; }
	 */

}
