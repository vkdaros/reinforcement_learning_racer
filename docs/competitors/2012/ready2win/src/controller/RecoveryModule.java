package controller;

import java.util.ArrayDeque;
import java.util.Deque;

import champ2011client.Action;
import champ2011client.SensorModel;

/**
 * A class providing recovery from outside the track and from specific situations.
 * 
 * @author Marek Bris
 */

public final class RecoveryModule {
	
	private boolean inRecoveryStateLeft; //=false;
	private boolean inRecoveryStateRight; //=false;
	private boolean blocked;
	private int keepBlock;//=0;
	
	// an average speed in the last 20 ticks
	private Deque<Double> speed20Tick = new ArrayDeque<Double>();
	private double sum=0;
	
	public RecoveryModule(){
		setInRecoveryStateLeft(false);
		setInRecoveryStateRight(false);
		setBlocked(false);
		keepBlock=0;
	}
	
	/**
	 * Checks a position of the vehicle. Determines whether recovery is not necessary.
	 * This is evaluated on track and outside track and in specific situations.
	 * @param SensorModel - current sensor values
	 * @return Action - next action
	 */
	
	public void checkRecovery(SensorModel sm){

		// an average speed in the last 20 ticks is updated
		if(speed20Tick.size()<20){
			speed20Tick.addLast(Math.abs(sm.getSpeed()));
			sum+=speed20Tick.getLast();
		} else {
			sum-=speed20Tick.getFirst();
			speed20Tick.pollFirst();
			speed20Tick.addLast(Math.abs(sm.getSpeed()));
			sum+=speed20Tick.getLast();
		}
//		System.out.println("speeed > "+sum/20+" keepblock "+keepBlock);
		
	// provides "lock" recovery outside the track and getting speed
		
		
		if ((sum/20 < 0.5) && keepBlock == 0){
			setBlocked(true);
			keepBlock++;
		} else if (keepBlock == 100){
			//System.out.println("zapinam block");
			setBlocked(false);
			keepBlock=0;
		} else if (keepBlock > 0)
			keepBlock++;
	}
	
	
	/**
	 * Overtakes control of the pilot and gives him a driving direction in the direction of the track.
	 * This is done in case, when the speed is zero and recovery mode is active.
	 * @param SensorModel - current sensor values
	 * @return Action - next action
	 */
	
	public Action goStraight(SensorModel sm){
		Action a=new Action();
		if (sm.getTrackPosition()>1)
			a.steering=-0.8;
		if (sm.getTrackPosition()<-1)
			a.steering=0.8;

		a.gear=1;
		a.accelerate=1;
		a.brake=0;
		return a;
	}
	
	
	/**
	 * Provides recovery of the vehicle from an area left of the track back to the track.
	 * @param SensorModel - current sensor values
	 * @return Action - next action
	 */
	
	public Action recoverLeft(SensorModel sm)
	{
		// P > 1 
		double B=sm.getAngleToTrackAxis();
		Action a= new Action();

		if (sm.getTrackPosition()<-1){
			setInRecoveryStateLeft(false);
			return a;	
		} 
		
		if (isInRecoveryStateRight() && sm.getTrackPosition()>1){
			setInRecoveryStateRight(false);
		} 
		
		a.gear=1;
		a.accelerate = ESP.filterASR(sm, 0.8);
	
		/*if (sm.getSpeed() < 10)
			a.accelerate = ESP.filterASR(sm, 0.3);
		else*/ 
		if (sm.getSpeed() < 30)
			a.accelerate = ESP.filterASR(sm, 0.5);
		else if ((sm.getSpeed() >= 30) && (sm.getSpeed() < 50))
			a.accelerate = ESP.filterASR(sm, 0.7);
		else if ((sm.getSpeed() >= 50) && (sm.getSpeed() < 70))
			a.accelerate = ESP.filterASR(sm, 0.8);
		else if (sm.getSpeed() >= 70)
			a.accelerate = ESP.filterASR(sm, 1);
		
		// som VLAVO
		// rovnobezne s tratou, v smere trate
//			System.out.println("----------- VLAVO "+sm.getSpeed()+" ");
			//System.out.println("rychlost "+sm.getSpeed());
		
			if (-0.785<B && B<0.785){
//			System.out.println("parallel with the track in its direction");
			
			if(sm.getSpeed()<0){
				a.brake=1;
				a.steering=1;
				return a;
			}else {
			double d=(0.436-B)/0.785;
			
			if (d<-1)
				a.steering=-1;
			else
				a.steering=-d;
			// a position, where to give it to the controller
			if (B>0)
				setInRecoveryStateLeft(false);
			else {
				setInRecoveryStateLeft(true);
				}
			}
		} 
		// heading out of the track
		else if (-2.355<B && B<-0.785){
//		System.out.println("heads out of the track");
			a.gear=-1;
			a.steering=1;
			setInRecoveryStateLeft(true);
		}
		// heads to the middle of the track
		else if (0.785<B && B<2.355){
//		System.out.println("heads to the middle of the track");
			a.steering=1;
			setInRecoveryStateLeft(true);
		}
		// parallel with the track, in an opposite direction
		else if ((-2.355>B && B>-3.14) || (2.355<B && B<3.14)){
//		System.out.println("parallel with the track, in an opposite direction");
			a.steering=1;
			setInRecoveryStateLeft(true);
		}
		return a;
	}
	
	/**
	 * Provides recovery of the vehicle from an area right of the track back to the track.
	 * @param SensorModel - current sensor values
	 * @return Action - next action
	 */
	
	public Action recoverRight(SensorModel sm)
	{
		// P < -1 
		double B=sm.getAngleToTrackAxis();
		Action a= new Action();

		if (sm.getTrackPosition()>1){
			setInRecoveryStateRight(false);
			return a;
		} 
		
		if (isInRecoveryStateLeft() && sm.getTrackPosition()<-1){
			setInRecoveryStateLeft(false);
		} 
		
		a.gear=1;
		a.accelerate = ESP.filterASR(sm, 0.8);

		if (sm.getSpeed() < 30)
			a.accelerate = ESP.filterASR(sm, 0.5);
		else if ((sm.getSpeed() >= 30) && (sm.getSpeed() < 50))
			a.accelerate = ESP.filterASR(sm, 0.7);
		else if ((sm.getSpeed() >= 50) && (sm.getSpeed() < 70))
			a.accelerate = ESP.filterASR(sm, 0.8);
		else if (sm.getSpeed() >= 70)
			a.accelerate = ESP.filterASR(sm, 1);
					
		/*if (sm.getSpeed() > 40){
			a.accelerate = 0;
			System.out.println("Nemam akcelerovat "+sm.getSpeed());
		}*/
		
		
//		System.out.println("----------- RIGHT "+sm.getSpeed()+" ");
		if (-0.785<B && B<0.785){
//		System.out.println("parallel with the track in its direction");
		if(sm.getSpeed()<0){
			a.brake=1;
			a.steering=-1;
			return a;
		}else {
			double d=(-0.436-B)/0.785;
			if (d<0) d=-d;
			
			if (d>1)
				a.steering=1;
			else
				a.steering=d;
			// a position, where to give it to the controller
			if (B<0)
				setInRecoveryStateRight(false);
			else{
				setInRecoveryStateRight(true);
			}
			}
		} 
		// heading out of the track
		else if (0.785<B && B<2.355){
//		System.out.println("heads out of the track");
			a.gear=-1;
			a.steering=-1;
			setInRecoveryStateRight(true);
		}
		// heads to the middle of the track
		else if (-2.355<B && B<-0.785){
//		System.out.println("heads to the middle of the track");
			a.steering=-1;
			setInRecoveryStateRight(true);
		}
		// parallel with the track, in an opposite direction
		else if ((-2.355>B && B>-3.14) || (2.355<B && B<3.14)){
//		System.out.println("parallel with the track, in an opposite direction");
			a.steering=-1;
			setInRecoveryStateRight(true);
		}
	
		return a;
	}

	public void setInRecoveryStateRight(boolean inRecoveryStateRight) {
		this.inRecoveryStateRight = inRecoveryStateRight;
	}

	public boolean isInRecoveryStateRight() {
		return inRecoveryStateRight;
	}

	public void setInRecoveryStateLeft(boolean inRecoveryStateLeft) {
		this.inRecoveryStateLeft = inRecoveryStateLeft;
	}

	public boolean isInRecoveryStateLeft() {
		return inRecoveryStateLeft;
	}

	/**
	 * @param blocked the blocked to set
	 */
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	/**
	 * @return the blocked
	 */
	public boolean isBlocked() {
		return blocked;
	}
	
}
