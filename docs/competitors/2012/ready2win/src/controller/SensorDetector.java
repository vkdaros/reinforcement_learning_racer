package controller;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

/**
 * A class making records about sensors on which was based vehicle movement. Runs in a different thread than the control module.
 * 
 * @author Adam Brcek
 */
public class SensorDetector implements Runnable
{
	private final BlockingQueue<SensorInfo> sensorInfoPipe;
	
	SensorInfo sI = null;
	ArrayList<SensorInfo> sensorList = new ArrayList<SensorInfo>();
	
	public SensorDetector(BlockingQueue<SensorInfo> source)
	{
		sensorInfoPipe = source;
	}
	
	public void run()
	{
		try {
			while (true)
			{
				SensorInfo sInfo = sensorInfoPipe.take();
				update(sInfo);
			}
		} catch (InterruptedException e) {
			System.err.println("Error: a failure in an action processing int the track model.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Based on this sensor determines, whether the current record will be update or the a new one will be created.
	 * @param sInfo A record about the timespan, when the vehicle movement was based on one sensor.
	 */
	public void update(SensorInfo sInfo)
	{
		if (sI == null)
			sI = sInfo;
		else
		{
			if (sI.getSensor() != sInfo.getSensor())
			{
				sensorList.add(sI);
				System.out.println("Record No "+(sensorList.size()+1));
				System.out.println("Sensor "+sI.getSensor());
				System.out.println("Duration "+sI.getDuration()+" tick(s)");
				System.out.println("Sector start "+sI.getStart()+" m");
				System.out.println();
				sI = sInfo;
			}
			else
			{
				sI.setDuration(sI.getDuration()+1);
			}
		}
	}
}
