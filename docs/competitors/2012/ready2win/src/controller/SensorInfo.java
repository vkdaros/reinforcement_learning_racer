package controller;

/**
 * A class for transfer of values of the sensor, that was used for the determination of the vehicle direction.
 * 
 * @author Adam Brcek
 */
public class SensorInfo
{
	private int sensor;
	private int duration;
	private double start;
	
	public SensorInfo(int sensor, int duration, double start)
	{
		this.sensor = sensor;
		this.duration = duration;
		this.start = start;
	}
	
	public int getSensor()
	{
		return sensor;
	}
	
	public int getDuration()
	{
		return duration;
	}
	
	public void setDuration(int duration)
	{
		this.duration = duration;
		return;
	}
	
	public double getStart()
	{
		return start;
	}
}
