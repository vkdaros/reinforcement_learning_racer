package controller;

public class SimpleNoisyRemover {
	
	/**
	 * removes deviation from an array of doubles (needs work, it is made for an array of 5 values, nothing bad happens, if there are more).
	 * @param poled
	 * @return estimated value
	 */
	public static double average(double poled[]){
		double result =0,result2=0;
		//change string to double
		for (int i = 0; i < poled.length; i++) {
			
			result+=poled[i];
		}
		result=result/poled.length;
		//System.out.print(result+", ");
		int count=0;
		//compute an average from the values which are by 5% higher or lower than overage
		for (int i = 0; i < poled.length; i++) {
			if(poled[i]<(result+result/20) && poled[i]>(result-result/20)){
				result2+=poled[i];
				count++;
			}
		}
		//System.out.println(result2);
		if(count !=0)
			result2=result2/count;
		
		if(result2>200)
			return 200;
		else if(result2==0.0)
			return result;
		else
			return result2;
	}
}
