package controller;

import java.math.BigDecimal;

import champ2010client.SensorModel;

/**
 * A class comprised of static methods providing status printing and mathematical computations.
 * 
 * @author Ivan Valencik
 */
public class Util
{
	public static void dump(SensorModel sm)
	{
		System.out.println("Status:\n" +
				"\n- angle to track axis: " + sm.getAngleToTrackAxis() +
				"\n- gear: " + sm.getGear() +
				"\n- speed: " + sm.getSpeed() +
				"\n- lateral speed:" + sm.getLateralSpeed() );
	}

	public static double rollingAverage(double value, double average, double duration)
	{
		return (average * duration + value) / (duration + 1);
	}
	/**
	 * round down
	 * @param d
	 * @param decimalPlace
	 * @return
	 */
	public static double round(double d, int decimalPlace){
		
	    BigDecimal bd = new BigDecimal(Double.toString(d));
	    bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
	    return bd.doubleValue();
	}
	/**
	 * a method for the computaion of the length based on the speed for the later computation of the speed
	 * @param speed
	 * @param friction
	 * @return
	 */
	public static double countDistanceFromSpeed(double speed, double friction) {
		return ((Math.pow(speed, 2))/(2*9.81*friction));
	}
}
