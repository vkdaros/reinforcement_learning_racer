package controller.model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;

import champ2011client.SensorModel;

/**
 * A class for recording events on the track
 * 
 * @author Maros Bednar, Marek Bris
 */
public class Event implements Serializable {
	
	public LinkedList<EventInfo> jumps = new LinkedList<EventInfo>();
	private double jumpDeviationRecord = 100; //a deviation between jumps after the record is created
	private double eventDeviationRecord = 100; //a deviation between leaving the track after the record is created
	
	private double jumpDeviationApply = 150; //a distance - applying speed regulation while jumping
	private double eventDeviationApply = 100; //a distance - applying speed lowering when off the track
		
	private double addSpeed = 10;//a value to which should be the max speed lowered

	private boolean dam=false;
	
	private double maxJump=0;
	private double startJump=0;



	/*
	 * Adding of the jump to the list
	 */
	public void addJump(double distance, double brake, double actJump)
	{	
		if (maxJump == 0)
			setStartJump(distance);
		
		//System.err.println("start jump "+getStartJump()+" distance "+distance);
		if (actJump > maxJump){
			maxJump=actJump;
			
			int indexJumpu = inList(jumps,distance,jumpDeviationApply,false);
			if (indexJumpu != -1)
				jumps.get(indexJumpu).setMaxJump(maxJump);
			//System.out.println("here I am "+maxJump);
		}
		else if(inList(jumps,getStartJump(),jumpDeviationRecord,false) == -1)
		{	//System.err.println("adding jump");
			//System.err.println("start jump "+getStartJump()+" distance "+distance);
			//brake=maxJump*15;
		
			jumps.add(new EventInfo(getStartJump(), brake, maxJump));
			//System.err.println("distance"+distance+" startJump: "+getStartJump());
			//System.out.println("add - jump - brake: "+ jumps.getLast().getBrake());
			//System.out.println("add - jump - distance: "+ jumps.getLast().getDistance());
			setMaxJump(0);
		} else setMaxJump(0);
	}
	

	
	/*
	 * Returns index in case that the event/jump with a current position is in the list, otherwise -1
	 */
	public int inList(LinkedList<EventInfo> events, double actualDistance, double deviation, boolean speed)
	{
		int index = 0;
		Iterator it = events.listIterator();

        while(it.hasNext())
        {
          EventInfo event = (EventInfo)it.next();
          if(speed)
          {
        	  if((event.getDistance() - actualDistance) < deviation && (event.getDistance() - actualDistance) > 0)
	          {
	        	  //System.out.println("in the list");
	        	  return index; //if it is in the list
	          }
          }
          else
	          if(Math.abs(actualDistance-event.getDistance()) < deviation)
	          {
	        	  //System.out.println("in the list");
				  //setMaxJump(0);
	        	  return index; //if it is in the list
	          }
          index++;
         
        }
        return -1; // if it isnt in the list
	}
	
	public double getSpeed(EventInfo actEvent)
	{
		if (actEvent == null)
			return 0;
		else {
			//System.out.println("return "+actEvent.getBrake());
			return actEvent.getBrake();
		}
		
		/*
		int indexEventu = inList(events,actualDistance,eventDeviationApply,true);
		int indexJumpu = inList(jumps,actualDistance,jumpDeviationApply,true);
		
		
		if(indexEventu != -1 && indexJumpu != -1 && !events.isEmpty() && !jumps.isEmpty())
		{
			EventInfo ju = jumps.get(indexJumpu);
			
			//the vehicle went off the track after the jump
			//System.out.println("brake jump: "+ju.getBrake());
			return ju.getBrake();
		}
		else
		{
			if(indexEventu != -1 && !events.isEmpty())
			{
				EventInfo ev = events.get(indexEventu);
				//System.out.println("brake event: "+ev.getBrake());
				return ev.getBrake();
				
			}
		}
		return 0;
		*/
	}
	
	public void setSpeed(double actualDistance, SensorModel sm)
	{
		int indexJumpu = inList(jumps,actualDistance,jumpDeviationApply,false);
		//System.out.println("je v liste na pozici "+indexJumpu+" a pocet jumpov je "+jumps.size());
		if(indexJumpu != -1 && !jumps.isEmpty())
		{
			//the vehicle went off the track after the jump
			EventInfo ju = jumps.get(indexJumpu);
			
			addSpeed=ju.getMaxJump()*15;
			
			jumps.set(indexJumpu, new EventInfo(ju.getDistance(), ju.getBrake()+addSpeed, ju.getMaxJump()));
			//System.out.println("setujem speed na "+(ju.getBrake()+addSpeed));
			
			//System.err.println("modified speed rychlost "+(ju.getBrake()+addSpeed)+" max jump "+maxJump);
			//System.err.println("height "+sm.getZ()+" speed Z: "+sm.getZSpeed());
			setMaxJump(0);

			//System.out.println("setting jump speed "+ (ju.getBrake()+addSpeed));
			//System.out.println("Saved brake - jump: " + (ju.getBrake()+addSpeed));
			//System.out.println("Saved brake - jump: " + (ju.getDistance()));
		}
		else
		{
			setMaxJump(0);
				
		}
		
		
	}
	
	
	public LinkedList<EventInfo> getJumps() {
		return jumps;
	}

	public void setJumps(LinkedList<EventInfo> jumps) {
		this.jumps = jumps;
	}

	
	public double getAddSpeed() {
		return addSpeed;
	}

	public void setAddSpeed(double addSpeed) {
		this.addSpeed = addSpeed;
	}
	
	public double getMaxJump() {
		return maxJump;
	}

	public void setMaxJump(double lastJump) {
		this.maxJump = lastJump;
	}
	
	public double getStartJump() {
		return startJump;
	}

	public void setStartJump(double startJump) {
		this.startJump = startJump;
	}
 
}
