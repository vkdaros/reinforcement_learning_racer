package controller.model;

import java.io.Serializable;

public class EventInfo implements Serializable {
	
	private double d;
	private double b;
	private double j;
	
	public EventInfo(double distance, double brake, double jumpMax)
	{
		d = distance;
		b = brake;
		j = jumpMax;
	}

	public double getMaxJump() {
		return j;
	}

	public void setMaxJump(double j) {
		this.j = j;
	}

	public double getDistance()
	{
		return d;
	}
	public double getBrake()
	{
		return b;
	}
}
