package controller.model;

import java.io.Serializable;

public class JumpInfo implements Serializable, Comparable<JumpInfo> {

	private double start;
	private double end;
	private double maxJump;
	private double speed;

	public JumpInfo(double start, double end, double jumpMax, double speed) {
		this.start = start;
		this.end = end;
		this.maxJump = jumpMax;
		this.speed = speed;
	}

	public double getStart() {
		return start;
	}

	public void setStart(double start) {
		this.start = start;
	}

	public double getEnd() {
		return end;
	}

	public void setEnd(double end) {
		this.end = end;
	}

	public double getMaxJump() {
		return maxJump;
	}

	public void setMaxJump(double maxJump) {
		this.maxJump = maxJump;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public int compareTo(JumpInfo jinfo) {
		if (getStart() < jinfo.getStart())
			return -1;
		else if (getStart() == jinfo.getStart())
			return 0;
		else
			return 1;
	}

}
