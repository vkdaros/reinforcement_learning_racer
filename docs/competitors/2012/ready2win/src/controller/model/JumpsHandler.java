package controller.model;

import java.util.Iterator;
import java.util.LinkedList;

import champ2011client.SensorModel;

public class JumpsHandler {
	
	private boolean isInJump;
	private double start;
	private double end;
	private double maxJump;
	private double speed;
	private double distanceTolerance = 100;
	private LinkedList<JumpInfo> jumpList = new LinkedList<JumpInfo>();


	public void jumpControl(SensorModel sm) {
		if (sm.getZ() > 0.4){
			if (!isInJump){
					start = sm.getDistanceFromStartLine();
					speed = sm.getSpeed();
			}
			
			if (maxJump < sm.getZ())
				maxJump = sm.getZ();
			
			setInJump(true);
		}
		else { // zapis po dopade
			end=sm.getDistanceFromStartLine();
			
			if ((end - start > 1) && (isJumpInList(start) == -1)){ 
				jumpList.add(new JumpInfo(start, end, maxJump, speed));
				System.out.println("Pridavam jump");
				//System.out.println("start "+getStart()+" end "+getEnd()+" speed "+getSpeed()+" maxJump "+getMaxJump());
			}
			maxJump = 0;
			setInJump(false);
		}
		
	}
	

	public boolean isInJump() {
		return isInJump;
	}

	public void setInJump(boolean isInJump) {
		this.isInJump = isInJump;
	}

	public double getStart() {
		return start;
	}


	public void setStart(double start) {
		this.start = start;
	}


	public double getEnd() {
		return end;
	}


	public void setEnd(double end) {
		this.end = end;
	}


	public double getMaxJump() {
		return maxJump;
	}


	public void setMaxJump(double maxJump) {
		this.maxJump = maxJump;
	}


	public double getSpeed() {
		return speed;
	}


	public void setSpeed(double speed) {
		this.speed = speed;
	}


	public void setJumpList(LinkedList<JumpInfo> jumpList) {
		this.jumpList = jumpList;
	}


	public LinkedList<JumpInfo> getJumpList() {
		return jumpList;
	}
	
	public int isJumpInList(double actualDistance) {
		int index = 0;
		Iterator it = getJumpList().listIterator();
		while (it.hasNext()) {
			JumpInfo jump = (JumpInfo) it.next();
	
			if (Math.abs(actualDistance - jump.getStart()) < distanceTolerance) {
				//System.out.println("Jump je v zozname");
				return index; // ak je v zozname
			}
			index++;
		}
		return -1; // ak nie je v zozname
	}


	
}
