package controller.model;

public class ThreeDouble {
	private double double1,double2,double3;
	
	public ThreeDouble(){
		
	}
	
	public ThreeDouble(ThreeDouble old){
		this.double1=old.getDouble1();
		this.double2=old.getDouble2();
		this.double3=old.getDouble3();
	}
	
	public ThreeDouble(double double1,double double2, double double3){
		this.double1=double1;
		this.double2=double2;
		this.double3=double3;
	}

	public double getDouble1() {
		return double1;
	}

	public void setDouble1(double double1) {
		this.double1 = double1;
	}

	public double getDouble2() {
		return double2;
	}

	public void setDouble2(double double2) {
		this.double2 = double2;
	}

	public double getDouble3() {
		return double3;
	}

	public void setDouble3(double double3) {
		this.double3 = double3;
	}
}
