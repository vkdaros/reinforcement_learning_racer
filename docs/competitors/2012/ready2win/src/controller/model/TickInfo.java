package controller.model;

import champ2011client.Action;
import champ2011client.SensorModel;

/**
 * A class for transfer of the data about the current tick between the threads.
 * 
 * @author Ivan Valencik
 */
public class TickInfo {
	private Action a;
	private SensorModel sm;
	private boolean turn;
	private double trackPos;
	private double calcSpeed;

	public double getTrackPos() {
		return trackPos;
	}

	public void setTrackPos(double trackPos) {
		this.trackPos = trackPos;
	}
	public TickInfo(){
	
	}

	public TickInfo(Action action, SensorModel sensorModel, boolean isTurning,double trackPos,double calcSpeed) {
		a = action;
		sm = sensorModel;
		turn = isTurning;
		this.trackPos=trackPos;
		this.calcSpeed = calcSpeed;
	}

	public double getCalcSpeed() {
		return calcSpeed;
	}

	public void setCalcSpeed(double calcSpeed) {
		this.calcSpeed = calcSpeed;
	}

	public Action getAction() {
		return a;
	}

	public SensorModel getSensorModel() {
		return sm;
	}

	public boolean isTurning() {
		return turn;
	}
}
