package controller.model;

import java.io.Serializable;
//import java.util.Iterator;
import java.util.Iterator;
import java.util.LinkedList;
import controller.DriveModul;
import controller.Util;
import champ2011client.SensorModel;

/**
 * A class containing all data about the track.
 * serializable
 * 
 * @author doom
 *
 */
public class TrackData  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double friction=16; 
	private double trackWidth=0; //track width
	private LinkedList<Turn> turnList=null; //turn list
	private LinkedList<EventInfo> eventList;
	private LinkedList<EventInfo> jumpList;
	// TODO
	private EventInfo actEvent=null;
	private int indexEvent=-1;
	private int indexJump=-1;
	private int deviation = 70;	
	boolean isLoadData=false; //were data loaded from a file?
	private Turn nextTurn = null; //the next turn
	private Turn next2Turn = null; //the turn after the next one, null if the vehicle is before the last turn
	private Turn lastTurn = null; //the last turn
	private int aktTurnIndex=-1; //index of the current turn in the list
	private double lastTurnEnd; //the end of the last turn
	private boolean isTurnFront=false; //is some turn in front of us? checks whether the vehicle is before the last turn
	private boolean refresh=false;
	private int offTrackCount=0;
	
	public int getOffTrackCount() {
		return offTrackCount;
	}
	public boolean isLoadData() {
		return isLoadData;
	}
	public void setLoadData(boolean trackData) {
		this.isLoadData = trackData;
	}
	public Turn getNextTurn() {
		return nextTurn;
	}
	public void setNextTurn(Turn nextTurn) {
		this.nextTurn = nextTurn;
	}
	public Turn getNext2Turn() {
		return next2Turn;
	}
	public void setNext2Turn(Turn next2Turn) {
		this.next2Turn = next2Turn;
	}
	public Turn getLastTurn() {
		return lastTurn;
	}
	public void setLastTurn(Turn lastTurn) {
		this.lastTurn = lastTurn;
	}
	public double getLastTurnEnd() {
		return lastTurnEnd;
	}
	public void setLastTurnEnd(double lastTurnEnd) {
		this.lastTurnEnd = lastTurnEnd;
	}
	public boolean isTurnFront() {
		return isTurnFront;
	}
	public void setTurnFront(boolean isTurnFront) {
		this.isTurnFront = isTurnFront;
	}
	public double getFriction() {
		return friction;
	}
	public void setFriction(double friction) {
		this.friction = friction;
	}
	public double getTrackWidth() {
		return trackWidth;
	}
	public void setTrackWidth(double trackWidth) {
		this.trackWidth = trackWidth;
	}
	public LinkedList<Turn> getTurnList() {
		return turnList;
	}
	
	public LinkedList<EventInfo> getEventList() {
		return eventList;
	}
public void setEventList(LinkedList<EventInfo> eventList) {
		this.eventList = (LinkedList<EventInfo>) eventList.clone();
		//System.out.println(eventList.size()+"< event");
		this.indexEvent = -1;
	}
	
	public LinkedList<EventInfo> getJumpList() {
		return jumpList;
	}
	public void setJumpList(LinkedList<EventInfo> jumpList) {
		//System.out.println(jumpList.size()+"< jump");
		this.jumpList = (LinkedList<EventInfo>) jumpList.clone();
		this.indexJump = -1;
	}
	
	public boolean isInitTurnList() {
		if(turnList!=null) 
			return true;
		else 
			return false;
	}
	/**
	 * check whether all data is loaded
	 * @return true if all data is loaded, false if an error occured
	 */
	public boolean loadedComplete(){
		if(friction ==16)
			return false;
		if(trackWidth==0)
			return false;
		if(turnList==null)
			return false;
		
		
		return true;
	}
	
	/**
	 * recalculation of the entry positions
	 */
	public void reCountEnterPos() {
		for (Turn turn : turnList) {
			turn.setEnterPosition(this.trackWidth, this.friction);
		}
	}
	
	/**
	 * recalculation breakDist brake distances
	 */
	public void reCountBreakDist() { 
		for (Turn turn : turnList) {
			
			
				turn.setSpeed(turn.getSpeed()-turn.getAdjustment());
				turn.setSpeedIndex(turn.getSpeedIndex()-turn.getIndexAdjust());
		
			
			
			turn.setIndexAdjust(0.0);
			turn.setAdjustSpeed(0.0);
			
			
			turn.setBreakDist(Util.countDistanceFromSpeed(turn.getSpeed(), this.friction));

			//System.out.println("distance from start: "+turn.getStartDistance()+" speed in turn "+turn.getSpeed()+" breakdis "+turn.getBreakDist());
			//System.out.println("dir: "+turn.getDirection()+"peak> "+turn.getPeakValue()+" start> "+turn.getStartDistance()+" end> "+turn.getEndDistance()+ " breakSpeed: "+turn.getBreakDist());
		}
	}
	
	/**
	 * initialisation of the turn list from the class TurnDetector
	 */
	private void initTurnList() {
		this.turnList = TurnDetector.getTurnList(this.trackWidth, this.friction);
		/*if(turnList.get(0).getEndDistance()>turnList.get(1).getStartDistance()) //removal of the first turn, if it is behind the finish line
			turnList.remove();*/
		int pom=0;
		for (int i = 0; i < turnList.size(); i++) {
			if(i<turnList.size()-1)
				if(turnList.get(i+1).getStartDistance()<turnList.get(i).getStartDistance())
					pom=i+1;
		}
		/*for(int i=0;i<pom;i++) {
			turnList.remove(i);
		}*/
		/*if(turnList.get(1).getStartDistance()<turnList.get(0).getStartDistance())
			turnList.remove(0);*/
		aktTurnIndex=0;
		if(aktTurnIndex<turnList.size()) { //initialization of the first turn
			lastTurnEnd=0;
			nextTurn=turnList.get(aktTurnIndex);
			lastTurn=null;
			if(turnList.indexOf(nextTurn)+1<turnList.size())
				next2Turn=turnList.get(turnList.indexOf(nextTurn)+1);
			else
				next2Turn=null;
			isTurnFront=true;
		}
	}
	
	/**
	 * iteration through turns
	 */
	public void iterate(SensorModel sm, int actLap, boolean isInRace) {
		if(sm.getLastLapTime() != 0 && !isInitTurnList() && !isInRace) { //initialisation of the turn list, if there is no saved file
			initTurnList();
		}
		
		if(!refresh && sm.getDistanceFromStartLine()<2 && (isLoadData || sm.getLastLapTime()!=0)) {//restoration of the list iterator after crossing the finish line
			lastTurnEnd=0;
			aktTurnIndex=0;
			nextTurn=turnList.get(aktTurnIndex);
			lastTurn=null;
			if(turnList.indexOf(nextTurn)+1<turnList.size())
				next2Turn=turnList.get(turnList.indexOf(nextTurn)+1);
			else
				next2Turn=null;
			isTurnFront=true;
			if(actLap>2) {
				recalcSpeed(actLap, isInRace);
				reCountBreakDist();
			}
			//System.out.println("restoration of the turn list iterator");
		}
		
		if(sm.getDistanceFromStartLine()>2 && refresh)
			refresh=false;
		
		//iterating to the next turn
		if((sm.getLastLapTime() != 0 || isLoadData) && aktTurnIndex+1<turnList.size() && sm.getDistanceFromStartLine()>nextTurn.getEndDistance()) {
			lastTurn=nextTurn;
			lastTurnEnd=nextTurn.getEndDistance();
			aktTurnIndex++;
			nextTurn=turnList.get(aktTurnIndex);
			if(turnList.indexOf(nextTurn)+1<turnList.size())
				next2Turn=turnList.get(turnList.indexOf(nextTurn)+1);
			else
				next2Turn=null;
			isTurnFront=true;
			/*System.out.println("peak>"+nextTurn.getPeakValue());
			System.out.println("dir>"+nextTurn.getDirection());
			System.out.println("pos>"+nextTurn.getEnterPos());
			System.out.println();*/
		}
		
		//iterating to the next turn, if the current one is too small
		/*if((sm.getLastLapTime() != 0 || isLoadData) && aktTurnIndex+1<turnList.size()) {
			if(nextTurn.getPeakValue()<(trackWidth>13 ? 0.0:0.0)) {
				aktTurnIndex++;
				lastTurn=nextTurn;
				nextTurn=turnList.get(aktTurnIndex);
				if(turnList.indexOf(nextTurn)+1<turnList.size())
					next2Turn=turnList.get(turnList.indexOf(nextTurn)+1);
				else
					next2Turn=null;
				isTurnFront=true;
				//System.out.println(nextTurn.getPeakValue());
			}
		}*/
		
		if((sm.getLastLapTime() != 0 || isLoadData) && aktTurnIndex+1==turnList.size()) 
			isTurnFront=false;
		
		if ((eventList != null) 
				&& (eventList.size() > indexEvent+1) 
					&& ((eventList.get(indexEvent+1).getDistance() - sm.getDistanceFromStartLine()) < deviation && (eventList.get(indexEvent+1).getDistance() - sm.getDistanceFromStartLine()) > 0)){
						actEvent=eventList.get(indexEvent+1);
						//System.out.println(actEvent.getBrake()+"<----------------------------");
						indexEvent++;
		} else if ((jumpList != null) 
				&& (jumpList.size() > indexJump+1) 
					&& ((jumpList.get(indexJump+1).getDistance() - sm.getDistanceFromStartLine()) < deviation && (jumpList.get(indexJump+1).getDistance() - sm.getDistanceFromStartLine()) > 0)){
						actEvent=jumpList.get(indexJump+1);
						indexJump++;
		} else if ((actEvent != null) && ((actEvent.getDistance() - sm.getDistanceFromStartLine()) >= deviation && (actEvent.getDistance() - sm.getDistanceFromStartLine()) <= 0))
			actEvent = null;

	}

	public EventInfo getActEvent() {
		return actEvent;
	}
	
	public boolean isInTurn(SensorModel sm) {
		if(nextTurn != null && sm.getDistanceFromStartLine()>nextTurn.getStartDistance() && sm.getDistanceFromStartLine()<nextTurn.getEndDistance())
			return true;
		else
			return false;
	}
	
	/**
	 * a method, that increases speed in turns according to the current driving
	 * @param actLap
	 */
	private void recalcSpeed(int actLap, boolean isInRace) {
		if(!isInRace) {
			for (Turn turn : turnList) {
				if(!turn.isOptimalized() && turn.getLateralSpeed()<(this.friction>29 ? 10:5)) {
					//System.out.println("zvysujem >"+turn.getSpeed()+" o >"+turn.getSpeed()*(((1-turn.getPeakValue())*0.15)));
					turn.setSpeedIncr(turn.getSpeed()*((1-turn.getPeakValue())*0.2));
					turn.setSpeed(turn.getSpeed()*(1+((1-turn.getPeakValue())*0.2)));
					turn.setSpeedIndex(turn.getSpeedIndex()+((1-turn.getPeakValue())*0.15));
					//System.out.println("spI> "+turn.getSpeedIndex());
				}
				else {
					turn.setOptimalized(true);
					//System.out.println("not increasing >"+turn.getSpeed());
				}
				turn.delLateralSpeed();
				if(turn.getSpeed()>300)
					turn.setOptimalized(true);
			}
			refresh=true;
		}
	}
	
	/**
	 * recalculation breakDist brake distances
	 */
	public void setOptimalized() { 
		for (Turn turn : turnList) {
			turn.setOptimalized(true);
		}
	}
	
	public void addOffTrackCount() {
		offTrackCount++;
		
	}
	public void nulOffTrackCount(){
		offTrackCount=0;
	}
	public void clearAdjustments(){
		if(turnList!=null)
			for(Turn t: turnList){
				t.setAdjustSpeed(0.0);
				t.setIndexAdjust(0.0);
			}
	}
	
	public void addJumpsToTurns(LinkedList<JumpInfo> jumpList) {
		Iterator<JumpInfo> it = jumpList.listIterator();
		while (it.hasNext()) {
			JumpInfo jump = it.next();
			if (!isJumpInTurn(jump))
				turnList.add(new Turn(jump.getStart(), jump.getEnd(), 0, jump.getSpeed(), -1));
		}
	}
	
	public boolean isJumpInTurn(JumpInfo jump){
		Iterator<Turn> it = turnList.listIterator();
		while (it.hasNext()) {
			Turn turn = it.next();
			if (((turn.getStartDistance() <= jump.getStart()) && (jump.getStart() <= turn.getEndDistance()))
				|| ((turn.getStartDistance() <= jump.getEnd()) && (jump.getEnd() <= turn.getEndDistance())))
				return true;
		}
		return false;
	}
	
}
