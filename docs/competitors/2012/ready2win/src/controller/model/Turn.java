package controller.model;

import java.io.Serializable;

import controller.DriveModul;
import controller.Util;

/**
 * @author Ivan Valencik
 */
public class Turn implements Serializable, Comparable<Turn>
{
	private double peakVal, startDist, endDist, speed, frontEdgeDist, enterPos, enterKm, breakDist, lateralSpeed=0, speedIndex=1, speedIncr=0, indexIncr=0;
	private Direction dir;
	private boolean optimalized=false;
	private double adjustment=0;
	private double indexAdjust=0;
	
	public double getIndexIncr() {
		return indexIncr;
	}
	public void setIndexIncr() {
		this.indexIncr = (1-getPeakValue())*0.15;
		}
	public double getIndexAdjust() {
		return indexAdjust;
	}
	public void setIndexAdjust(double indexAdjust) {
		this.indexAdjust = indexAdjust;
	}
	public double getAdjustment() {
		return adjustment;
	}
	public void setAdjustSpeed(Double adjustment){
		this.adjustment=adjustment;
		
	}
	public double getSpeedIndex() {
		return speedIndex;
	}

	public void setSpeedIndex(double speedIndex) {
		this.speedIndex = speedIndex;
	}

	public double getSpeedIncr() {
		return speedIncr;
	}

	public void setSpeedIncr(double speedIncr) {
		this.speedIncr = speedIncr;
	}

	public double getLateralSpeed() {
		return lateralSpeed;
	}

	public void setLateralSpeed(double lateralSpeed) {
		if(lateralSpeed>this.lateralSpeed)
			this.lateralSpeed=Math.abs(lateralSpeed);
	}
	
	public void delLateralSpeed() {
		this.lateralSpeed = 0;
	}

	public boolean isOptimalized() {
		return optimalized;
	}

	public void setOptimalized(boolean optimalized) {
		this.optimalized = optimalized;
	}

	public enum Direction { STRAIGHT, RIGHT, LEFT };

	public Turn(double startDistance, double endDistance, double peakValue,
			double peakSpeed, double frontEdgeDistance)
	{
		peakVal = Math.abs(peakValue);
		if (peakValue < 0)
			dir = Direction.RIGHT;
		else
			dir = Direction.LEFT;
		startDist = startDistance;
		endDist = endDistance;
		speed = peakSpeed;
		setIndexIncr();
		frontEdgeDist = frontEdgeDistance;
	}
	
	/**
	 * a metod that sets entry positions to all turns according tu turn categories a the track width
	 * @param trackWidth
	 */
	public void setEnterPosition(double trackWidth, double friction) {
		if(trackWidth>15) {
			if(this.getPeakValue()<0.35) {
				this.enterPos=((trackWidth-4)/trackWidth)*this.getPeakValue();
				this.enterKm=this.getStartDistance()-3*trackWidth;
				if(this.getDirection()==Direction.LEFT)
					this.enterPos=-this.enterPos;
			}
			else {
				this.enterPos=((trackWidth-4)/trackWidth)*((this.getPeakValue()+0.65)>((trackWidth-1)/trackWidth) ? ((trackWidth-1)/trackWidth):(this.getPeakValue()+0.65));
				this.enterKm=this.getStartDistance()-2*trackWidth;
				if(this.getDirection()==Direction.LEFT)
					this.enterPos=-this.enterPos;
			}
		}
		else if(trackWidth>12) {
			if(this.getPeakValue()<0.25) {
				this.enterPos=((trackWidth-2)/trackWidth)*this.getPeakValue();
				this.enterKm=this.getStartDistance()-3*trackWidth;
				if(this.getDirection()==Direction.LEFT)
					this.enterPos=-this.enterPos;
			}
			else {
				this.enterPos=((trackWidth-4)/trackWidth)*((this.getPeakValue()+0.65)>((trackWidth-1)/trackWidth) ? ((trackWidth-1)/trackWidth):(this.getPeakValue()+0.65));
				this.enterKm=this.getStartDistance()-2*trackWidth;
				if(this.getDirection()==Direction.LEFT)
					this.enterPos=-this.enterPos;
			}
			
		}
		else {
			if(this.getPeakValue()<0.15) {
				this.enterPos=((trackWidth-3)/trackWidth)*this.getPeakValue();
				this.enterKm=this.getStartDistance()-4*trackWidth;
				if(this.getDirection()==Direction.LEFT)
					this.enterPos=-this.enterPos;
			}
			else {
				this.enterPos=((trackWidth-3)/trackWidth)*((this.getPeakValue()+0.65)>((trackWidth-1)/trackWidth) ? ((trackWidth-1)/trackWidth):(this.getPeakValue()+0.65));
				this.enterKm=this.getStartDistance()-2.5*trackWidth;
				if(this.getDirection()==Direction.LEFT)
					this.enterPos=-this.enterPos;
			}
			
		}
		setBreakDist(Util.countDistanceFromSpeed(getSpeed(), friction));
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getBreakDist() {
		return breakDist;
	}

	public void setBreakDist(double breakDist) {
		this.breakDist = breakDist;
	}

	public double getEnterPos() {
		return enterPos;
	}

	public void setEnterPos(double enterPos) {
		this.enterPos = enterPos;
	}

	public double getEnterKm() {
		return enterKm;
	}

	public void setEnterKm(double enterKm) {
		this.enterKm = enterKm;
	}

	public double getPeakValue() {
		return peakVal;
	}
	
	public double getPeakSpeed() {
		return speed;
	}

	public double getStartDistance() {
		return startDist;
	}

	public Direction getDirection() {
		return dir;
	}
	
	public double getEndDistance()
	{
		return endDist;
	}
	
	public double getFrontEdgeDistance() {
		return frontEdgeDist;
	}

	public String toString(){
		return dir.toString()+" : "+getPeakValue() +'\n'+"zaciatok: "+getStartDistance()+'\t'+'\t'+"konec: "+getEndDistance()+'\n';
	}
	@Override
	public int compareTo(Turn turn) {
		if (getStartDistance() < turn.getStartDistance())
			return -1;
		else if (getStartDistance() == turn.getStartDistance())
			return 0;
		else
			return 1;
	}
}
