package controller.model;

import java.util.ArrayDeque;
import java.util.Deque;
//import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import controller.DriveModul;
import controller.Ready2WinController;
import controller.Util;

/**
 * A class creating records of turns. Runs in its own thread.
 * 
 * @author Ivan Valencik
 */
public class TurnDetector extends Thread
{
	private final BlockingQueue<TickInfo> tickInfoPipe;
	
	private static final LinkedList<Turn> turns = new LinkedList<Turn>();

	// this variable should not be used for recognition, as it is used for synchronisation
	// and its behaviour can be changed in the future
	private static double _focusValue;

	public TurnDetector(BlockingQueue<TickInfo> source)
	{
		tickInfoPipe = source;
	}

	public void run()
	{
		try {
			while (true)
			{
				TickInfo tick = tickInfoPipe.take();
				if(exit)
					break;
				inform(tick);
				if(checkExit)
					exit=tickInfoPipe.size()==0;
				//System.out.println("Queue length: " + tickInfoPipe.size());
			}
			System.out.println("thread successfully exited");
		} catch (InterruptedException e) {
			System.err.println("Error: chyba pri prijmani akcie do modelu trate.");
			e.printStackTrace();
		}
	}
		
	/**
	 * A method return a list of recognised turns. <br/><br/> 
	 * Warning: the list is not being actualised, the method must called again for getting the new version
	 * @return a turn list
	 */
	@SuppressWarnings("unchecked")
	public static LinkedList<Turn> getTurnList(double trackWidth, double friction)
	{
		LinkedList<Turn> tmp;
		synchronized (turns)
		{
			tmp =(LinkedList<Turn>) turns.clone();
		}
		for (Turn turn : tmp) {
			turn.setEnterPosition(trackWidth, friction);
			turn.setBreakDist(Util.countDistanceFromSpeed(turn.getSpeed(), friction));
			
		}
		return tmp;
	}
	public synchronized void stopThread(){
		//System.out.println("vypinam thread : "+tickInfoPipe.size());
		if(tickInfoPipe.size()==0){
			exit=true;
			try {
				tickInfoPipe.put(new TickInfo());
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}else
			checkExit=true;

	}
	public synchronized boolean isExit() {
		return exit;
	}

	/**
	 * <p>A metod returns a focus value for the given angle. It is blocking.</p>
	 * <p>Each method call return ASAP, however measurement can be done only
	 * once in the game time ( [senzor_model].getCurrentLapTime() ).
	 * @param desiredFocus desired setting of the focus from range <-90, 90>
	 * @throws TooSoonException an exception is thrown if the method is called sooner that after one second of the game time
	 */
	private double requestSpecificFocusValue(int desiredFocus) throws TooSoonException
	{
		Ready2WinController.requestFocusChange(desiredFocus);

		synchronized(Ready2WinController.lock)
		{
			try {
				Ready2WinController.lock.wait();
				if (_focusValue == -3)
					throw new TooSoonException();
				else
					return _focusValue;
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	/**
	 * This method should be called only when the focus change is requested.
	 * @param focusValue focus value
	 */
	public static void setFocusValue(double focusValue)
	{
		_focusValue = focusValue;
	}
	
	//a variable for splitting two turns in the same direction into 2
	private Deque<Double> steerChange = new ArrayDeque<Double>();
	private boolean exit=false,checkExit=false;
	private List<Double> steeringRecords = null;
	private List<Double> speedRecords = null;
	private List<Double> trackPoss = null;
	private List<Double> distFromStart =null;
	private List<TwoDouble> ticksInTurn = null;
	private double turnStart;
	private double speed;
	//private boolean newTurn=false;
	private LinkedList<Double> allSpeedRecords = new LinkedList<Double>();;
	
	
	private static final double MAX_DIFF_RANGE = 0.01;
	private static final int SEQUENCE_LENGTH = 5;
		
	/**
	 * A method computes an average of values in the sequence, however only
	 * when all values are within the range determined by MAX_DIFF_RANGE.
	 * @param subList list of steering actions
	 * @return an average value of steering actions or -1, if the reqs are not met
	 */
	private double checkValues(List<Double> subList) {
		double min = 1, max = 0;

		for (double d : subList) {
			if ((d > 0 && subList.get(0) < 0) || (d < 0 && subList.get(0) > 0))
				return -2;

			double d1 = Math.abs(d);
			if (d1 < min)
				min = d1;
			if (d1 > max)
				max = d1;
		}
		double sum = 0;
		if (max - min < MAX_DIFF_RANGE) {
			
			for (double d : subList)
				sum += d;
			return sum / subList.size();
		}
		//*****added codde by florek
		
		for (double d : subList)
			sum += d;
		return sum / subList.size();
		//*****
		
		//return -1;
	}
	
	/**
	 * Computes an average speed in  the observed timespan.
	 * @param startIndex Tick of the turn from which should be the speed computed.
	 * @return An average value in the sector of steepest steering.
	 */
	/*private double computeAvgSpeed(int startIndex) {
		double sum = 0;
		
		if (speedRecords.size() < SEQUENCE_LENGTH) {
			for (Double d : speedRecords)
				sum += d;
			return sum / speedRecords.size();
		}
		
		for (int i = startIndex; i < startIndex + SEQUENCE_LENGTH; i++)
			sum += speedRecords.get(i);
		
		return sum / SEQUENCE_LENGTH;
	}*/

	/**
	 * Assigns a number representing the steepest turn according to the steering actions.
	 * @return A value representing turn steepness.
	 */
	/*private double processSteeringRecord() {
		double val = 0;
		double maxVal = 0;
		
		
		for (int i = 0; i <= steeringRecords.size() - SEQUENCE_LENGTH; i++)
			if ((val = checkValues(steeringRecords.subList(i, i + SEQUENCE_LENGTH - 1))) != -1)
			{				
				if (Math.abs(val) > Math.abs(maxVal)) {
					maxVal = val;
					speed = computeAvgSpeed(i);
				}
			}
		
		return maxVal;
	}*/
	/**
	 * 1 if growing, 0 if equal, -1 if decreasing
	 * @param que
	 * @return
	 */
	/*private int steerSampleChange(Deque<Double> que){
		int[] pom = new int[que.size()-1];
		Iterator<Double> iterator = que.iterator();
		int i=0,retVal=0;
		double lastVal=0,aktVal;
		if(iterator.hasNext())
			lastVal=iterator.next();
		//System.out.print("que: "+lastVal+" , ");
		while(iterator.hasNext()){
			aktVal=iterator.next();
			//System.out.print(aktVal+" , ");
			//hodnoty vedal seba surovnake vzhladom na max_diff_range
			if(Math.abs(lastVal)-Math.abs(aktVal)<MAX_DIFF_RANGE){
				pom[i]=0;
			//hodnoty su rozne
			}else{ 
				//stupa
				if(Math.abs(lastVal)<Math.abs(aktVal))
					pom[i]=1;
				//klesa
				else
					pom[i]=-1;
			}
				
			i++;
		}
		for(i=0;i<que.size()-1;i++){
			retVal+=pom[i];
		}
		//System.out.print(" diference: "+(Math.abs(que.getFirst())-Math.abs(que.getLast())));
		if(Math.abs(que.getFirst())-Math.abs(que.getLast())>MAX_DIFF_RANGE*(SEQUENCE_LENGTH/2))
			return 1;
		else if(Math.abs(que.getFirst())-Math.abs(que.getLast())<-MAX_DIFF_RANGE*(SEQUENCE_LENGTH/2))
			return -1;
		else
			return 0;
	}*/
	/*private TwoDouble minMax(Deque<Double> que){
		Iterator<Double> iterator = que.iterator();
		TwoDouble minMax=new TwoDouble();
		double aktVal;
		if(iterator.hasNext()){
			minMax.setMax(iterator.next());
			minMax.setMin(minMax.getMax());
			System.out.print("que: "+minMax.getMax()+" , ");
		}
		while(iterator.hasNext()){
			aktVal=iterator.next();
			if(Math.abs(minMax.getMax())<Math.abs(aktVal))
				minMax.setMax(aktVal);
			else if(Math.abs(minMax.getMin())>Math.abs(aktVal))
				minMax.setMin(aktVal);
			System.out.print(aktVal+" , ");
		}
		return minMax;
	}*/
	
	private List<ThreeDouble> processSteeringRecordNew(TickInfo tick) {
		double val = 0;
		double maxVal = 0,minVal=1;
		steerChange.clear();
		//TwoDouble minMaxAkt=new TwoDouble(1,0),minMaxLast=new TwoDouble(1,0);
		List<ThreeDouble> values;
		
		for (int i = 0; i <= steeringRecords.size() - SEQUENCE_LENGTH; i++){
			//distFromStart.add(tick.getSensorModel().getDistanceFromStartLine());
			val =checkValues(steeringRecords.subList(i, i + SEQUENCE_LENGTH - 1));
			//
			//val =steeringRecords.get(i);
			if(steerChange.size()<3)
				steerChange.addLast(val);
			else{
				steerChange.addLast(val);
				steerChange.removeFirst();
			}
			if(val!=-2)
				ticksInTurn.add(new TwoDouble(val,distFromStart.get(i)));
			if(maxVal<val && val!=-2.0)
				maxVal=val;
			if(minVal>val && val!=-2.0)
				minVal=val;
			
			//int queState=0;
			/*if(steerChange.size()==0){
				if(Math.abs(Math.abs(minMaxAkt.getMax())-Math.abs(minMaxLast.getMax()))>MAX_DIFF_RANGE){
					queState=1;
				}else if(Math.abs(Math.abs(minMaxAkt.getMin())-Math.abs(minMaxLast.getMin()))>MAX_DIFF_RANGE)
					queState=-1;
			}*/
			/*int queState =(steerSampleChange(steerChange));
			//System.out.println(" queState: "+queState);
			// steering value is stable
			if(queState==0){
				if(!newTurn)
					newTurn=true;
			}
			//steering value grows and the stable has been reached once
			else if(queState==1 && newTurn){
				
				steeringRecords =  steeringRecords.subList(i,steeringRecords.size()-1);
				speedRecords =  speedRecords.subList(i,speedRecords.size()-1);;
				trackPoss= trackPoss.subList(i,trackPoss.size()-1);;;
				i=0;
				double angle = tick.getSensorModel().getAngleToTrackAxis();
				double frontEdgeDist = -1;
				try {
					frontEdgeDist = requestSpecificFocusValue((int)(angle * 90));
				} catch (TooSoonException e) {
					//System.out.println("priskoro");
				}
				
				// adding of the record about the turn
				synchronized (turns)
				{
					turns.add(new Turn(turnStart,
							tick.getSensorModel().getDistanceFromStartLine(), minMaxAkt.getMax(),
							speed, frontEdgeDist));
					
					System.out.println(turns.getLast().getDirection().toString() + ": " +
							turns.getLast().getPeakValue() + "\n peak spd: " +
							turns.getLast().getPeakSpeed() + " edge dist:" +
							turns.getLast().getFrontEdgeDistance() + "\n");
				}
				newTurn=false;
			}*/
				
			
		}
		values=getTralalal(minVal,maxVal);
		values=countCardinality(ticksInTurn,values);
		
		
		/*for(ThreeDouble d : values){
			System.out.print(", value: "+d.getDouble1()+" count: "+d.getDouble2()+" distance: "+d.getDouble3());
		}*/
		//System.out.println();
		//System.out.println("---------");
		if(compPercent(values)){
			for(ThreeDouble t : values){
				t.setDouble2(0.0);
				t.setDouble3(0.0);
				
			}
			for(TwoDouble d : ticksInTurn){
				values=recalculAddOne(d,values,ticksInTurn.size(),0.01);
				
			}
			
			for(ThreeDouble d : values){
				d.setDouble2(d.getDouble2()*(100.0/ticksInTurn.size()));
				//System.out.print(", value: "+d.getDouble1()+" count: "+d.getDouble2()+" distance: "+d.getDouble3());
			}
			//System.out.println();
			//System.out.println("---recalculated-----");
			
		}
		if(compPercent(values)){
			for(ThreeDouble t : values){
				t.setDouble2(0.0);
				t.setDouble3(0.0);
				
			}
			for(TwoDouble d : ticksInTurn){
				values=recalculAddOne(d,values,ticksInTurn.size(),0.02);
				
			}
			
			for(ThreeDouble d : values){
				d.setDouble2(d.getDouble2()*(100.0/ticksInTurn.size()));
				//System.out.print(", value: "+d.getDouble1()+" count: "+d.getDouble2()+" distance: "+d.getDouble3());
			}
			//System.out.println();
			//System.out.println("---recalculated2-----");
		}
		ThreeDouble maxInterval=null;
			
		
		if(percentTooLow(values)){
			maxInterval=new ThreeDouble(getMaxPercent(values));
			maxInterval.setDouble2(55.7);
			values.clear();
			values.add(maxInterval);
			//System.out.println(" I am here: "+maxInterval.getDouble2());
		}
		
		return values;
	}
	private boolean percentTooLow(List<ThreeDouble> values){
		for(ThreeDouble t : values){
			if(t.getDouble2()>18)
				return false;
		}
		return true;
	}
	private ThreeDouble getMaxPercent(List<ThreeDouble> values){
		ThreeDouble maxVal=new ThreeDouble(0.0,0.0,0.0);
		for(ThreeDouble t : values){
			if(maxVal.getDouble2()<t.getDouble2())
				maxVal=t;
		}
		return maxVal;
	}
	private boolean compPercent(List<ThreeDouble> retVal){
		double aktval=0;
		//boolean flag =true;
		for(ThreeDouble t : retVal){
			if(aktval>18 && t.getDouble2()>18)
				return true;
			
			aktval = t.getDouble2();
		}
		
		return false;
	}
	/**
	 * determines values, to which should be assigned occurences
	 * 
	 */
	private List<ThreeDouble> getTralalal(double min,double max){
		List<ThreeDouble> retVal= new LinkedList<ThreeDouble>();
		double lastCount =min+0.01;
		retVal.add(new ThreeDouble(lastCount,0,0));
		//System.out.print(" values "+lastCount+" , ");
		while(lastCount<max) {
			lastCount+=0.02;
			//if the max speed is exceeded, break out of the cycle
			if(lastCount-0.01>max)
				break;
			retVal.add(new ThreeDouble(lastCount,0,0));
			//System.out.print(lastCount+" , ");
		}
		//System.out.println();
		
		return retVal;
	}
	private List<ThreeDouble> countCardinality(List<TwoDouble> list,List<ThreeDouble> retVal){
		
		for(TwoDouble d : list){
			retVal=recalculAddOne(d,retVal,list.size(),0.0);
		}
		for(ThreeDouble t : retVal){
			//System.out.println("size: "+list.size()+" getmax: "+t.getMax());
			//System.out.println(" hm2: "+t.getMax()*(100.0/list.size()));
			t.setDouble2(t.getDouble2()*(100.0/list.size()));
			//System.out.println(" hm: "+t.getMax()+" hm2: "+t.getMax()*(100/list.size()));
		}
		
		return retVal;
	}
	/*private List<ThreeDouble>  addOne(double d,List<ThreeDouble> retVal,double size){
		for(ThreeDouble t : retVal){
			if(d>=t.getDouble1()-0.01 && d<=t.getDouble1()+0.01){
				t.setdouble2(t.getDouble2()+1);
				if(t.getdouble3()==0.0)
					t.setDouble3(double3)
				break;
			}
		}
		
		
		return retVal;
	}*/
	private List<ThreeDouble>  recalculAddOne(TwoDouble d,List<ThreeDouble> retVal,double size,double recVal){
		
		
		for(ThreeDouble t : retVal){
			
			if(d.getMin()>=t.getDouble1()-0.01+recVal && d.getMin()<=t.getDouble1()+0.01+recVal){
				if(t.getDouble3()==0.0)
					t.setDouble3(d.getMax());
				t.setDouble2(t.getDouble2()+1);
				break;
			}
		}
		
		return retVal;
	}
	private void addTurns(List<ThreeDouble> list,TickInfo tick){
		
		double angle = tick.getSensorModel().getAngleToTrackAxis();
		double frontEdgeDist = -1;
		try {
			frontEdgeDist = requestSpecificFocusValue((int)(angle * 90));
		} catch (TooSoonException e) {
			//System.out.println("too soon");
		}
		ThreeDouble pom;
		List<ThreeDouble> pomPole = new LinkedList<ThreeDouble>();
		for(int i=0;i<list.size();i++){
			pom =list.get(i);
			if(pom.getDouble2()>18){
		// adding a record about the turn
				pomPole.add(pom);
				
				if(i+1<=list.size()-1 && pom.getDouble2()>18 && list.get(i+1).getDouble2()>18)
					i++;
			}
		}
		for(int i=0;i<pomPole.size();i++){
			synchronized (turns)
			{
				//if it is a last element, then end with tickinfo
				
				//if(pomPole.size()==1){
				//double 3 is a distance from the start, doule 2 is count in percentage, double 1 is steer
				//is alone
				if(pomPole.size()==1){
						turns.add(new Turn(turnStart,
								tick.getSensorModel().getDistanceFromStartLine(), pomPole.get(i).getDouble1(),
								speed, frontEdgeDist));
						
//						System.out.println(turns.getLast().getDirection().toString() + ": " +
//								turns.getLast().getPeakValue() + "\n peak spd: " +
//								turns.getLast().getPeakSpeed() + " edge dist:" +
//								turns.getLast().getFrontEdgeDistance() + "\n");
				}//is not alone and is the first
				else if (i==0){
					turns.add(new Turn(turnStart,
							pomPole.get(i+1).getDouble3()-1, pomPole.get(i).getDouble1(),
							speed, frontEdgeDist));
					
//					System.out.println(turns.getLast().getDirection().toString() + ": " +
//							turns.getLast().getPeakValue() + "\n peak spd: " +
//							turns.getLast().getPeakSpeed() + " edge dist:" +
//							turns.getLast().getFrontEdgeDistance() + "\n");
					
				}//is the last, not the first (duh)
				else if(pomPole.size()-1==i){
					turns.add(new Turn(pomPole.get(i).getDouble3(),
							tick.getSensorModel().getDistanceFromStartLine(), pomPole.get(i).getDouble1(),
							speed, frontEdgeDist));
					
					/*System.out.println(turns.getLast().getDirection().toString() + ": " +
							turns.getLast().getPeakValue() + "\n peak spd: " +
							turns.getLast().getPeakSpeed() + " edge dist:" +
							turns.getLast().getFrontEdgeDistance() + "\n");*/
				}
				//is somewhere in the middle
				else{
					turns.add(new Turn(pomPole.get(i).getDouble3(),
							pomPole.get(i+1).getDouble3()-1, pomPole.get(i).getDouble1(),
							speed, frontEdgeDist));
					
//					System.out.println(turns.getLast().getDirection().toString() + ": " +
//							turns.getLast().getPeakValue() + "\n peak spd: " +
//							turns.getLast().getPeakSpeed() + " edge dist:" +
//							turns.getLast().getFrontEdgeDistance() + "\n");
				}
			}
		}
		
	}
	/**
	 * A method recognising turns based on steering actions
	 * @param tick info on the tick
	 * @throws InterruptedException is thrown when there is an error in waiting for a focus change
	 */
	public void inform(TickInfo tick)
	{
		if (steeringRecords == null && tick.isTurning()) {
			//System.out.println("start of the turn");
			steeringRecords = new LinkedList<Double>();
			speedRecords = new LinkedList<Double>();
			trackPoss=new LinkedList<Double>();
			distFromStart = new LinkedList<Double>();
			ticksInTurn=new LinkedList<TwoDouble>();
			steeringRecords.add(tick.getAction().steering);
			speedRecords.add(tick.getSensorModel().getSpeed());
			turnStart = tick.getSensorModel().getDistanceFromStartLine();
			trackPoss.add(tick.getTrackPos());
			allSpeedRecords.add(tick.getCalcSpeed());
			distFromStart.add(tick.getSensorModel().getDistanceFromStartLine());
		}

		if (steeringRecords != null && tick.isTurning()) {
			steeringRecords.add(tick.getAction().steering);
			trackPoss.add(tick.getTrackPos());
			speedRecords.add(tick.getSensorModel().getSpeed());
			allSpeedRecords.add(tick.getCalcSpeed());
			distFromStart.add(tick.getSensorModel().getDistanceFromStartLine());
		}

		if (steeringRecords != null && !tick.isTurning()) {
			//System.out.println("end of the turn");
			List<ThreeDouble> val = processSteeringRecordNew(tick);
			speed=calcNewSpeed();
			allSpeedRecords.clear();
			addTurns(val,tick);
			//System.out.println(ticksInTurn.size());
			
			steeringRecords = null;
			speedRecords = null;
			trackPoss=null;
			
			// recording of the straight after the line
			/*double angle = tick.getSensorModel().getAngleToTrackAxis();
			double frontEdgeDist = -1;
			try {
				frontEdgeDist = requestSpecificFocusValue((int)(angle * 90));
			} catch (TooSoonException e) {
				//System.out.println("too soon");
			}
			
			// adding a record about the turn
			synchronized (turns)
			{
				turns.add(new Turn(turnStart,
						tick.getSensorModel().getDistanceFromStartLine(), val,
						speed, frontEdgeDist));
				
				System.out.println(turns.getLast().getDirection().toString() + ": " +
						turns.getLast().getPeakValue() + "\n peak spd: " +
						turns.getLast().getPeakSpeed() + " edge dist:" +
						turns.getLast().getFrontEdgeDistance() + "\n");
			}*/
		}
		
		
	}
	private double calcNewSpeed(){
		int indexMin=0;
		double minVal=500;
		
		for(int i=0;i<allSpeedRecords.size();i++){
			double pom=allSpeedRecords.get(i);
			if(minVal>pom){
				minVal=pom;
				indexMin=i;
			}
		}
		/*minVal=0;
		for(int i=0;i<indexMin;i++){
			minVal+=allSpeedRecords.get(i);
			
		}
		minVal/=indexMin+1;*/
		return minVal;		
	}
}

/**
 * An exception thrown when the calling method was used sooner than it is allowd.
 * @author Ivan Valencik
 */
@SuppressWarnings("serial")
class TooSoonException extends Exception {}

