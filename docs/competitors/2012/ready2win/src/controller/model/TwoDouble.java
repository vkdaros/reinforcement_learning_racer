package controller.model;

public class TwoDouble {
	private double min,max;
	
	public TwoDouble(){
		
	}
	
	public TwoDouble(double min,double max){
		this.min=min;
		this.max=max;
	}
	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}
	
}
