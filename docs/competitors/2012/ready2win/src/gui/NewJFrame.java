package gui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import controller.Ready2WinController;

import champ2010client.Controller;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class NewJFrame extends javax.swing.JFrame  {
	private JButton jButton1;
	private JButton jButton2;
	private Ready2WinController cont=null;
	/**
	* Auto-generated main method to display this JFrame
	*/
	
	
	public NewJFrame(Ready2WinController me) {
		
		super();
		this.cont = me;
		initGUI();
	}
	
	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			{
				jButton1 = new JButton();
				getContentPane().add(jButton1, BorderLayout.CENTER);
				jButton1.setText("start");
				jButton1.setPreferredSize(new java.awt.Dimension(258, 73));
				jButton1.addActionListener(new ActionListener(){

					public void actionPerformed(ActionEvent arg0) {
						cont.start();
						
					}
					
				});
			}
			{
				jButton2 = new JButton();
				getContentPane().add(jButton2, BorderLayout.NORTH);
				jButton2.setText("stop");
				jButton2.addActionListener(new ActionListener(){

					public void actionPerformed(ActionEvent arg0) {
						cont.stop();
						
					}
					
				});
			}
			pack();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
