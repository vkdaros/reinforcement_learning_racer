package sk.stuba.fiit.tp2010.team20.defence;

import java.util.ArrayList;


class Opponent {
	static int CAR_LENGHT = 4;
	private ArrayList<OpponentPosition> positions;
	private int sensor;
	private int lenght;

	public static final int SENSOR_DGR = 10;
	public static final double SENSOR_RAD = Math.toRadians(SENSOR_DGR);
	

	Opponent(int sensor, double dist){
		this.sensor = sensor;
		this.positions = new ArrayList<OpponentPosition>();
		this.positions.add(new OpponentPosition(sensor, dist));
		this.lenght = CAR_LENGHT;
	}

	void addPosition(OpponentPosition p){
		this.positions.add(p);
	}

	/**
	 * Determines speed of the opponent in comparison to the measuring vehicle.
	 * The speed can be >0 if the opponent is getting closer (if he is in the rear) or
	 * if he is getting further (if he is in the front), <0 if the opponent is falling back or
	 * if the vehicle is catching up.
	 * In case that we have only the first position of the vehicle, then the speed is 0, because
	 * it is not possible to determine true value based on only one record.
	 * <table style='border:1px solid black'>
	 * 	<tr><th>Relative<br/>speed</th><th>Position</th><th>State</th></tr>
	 * 	<tr><td>>0</td><td>in front</td><td>getting further</td></tr>
	 * 	<tr><td>>0</td><td>begind</td><td>closing distance</td></tr>
	 * 	<tr><td><0</td><td>in front</td><td>catching up</td></tr>
	 * 	<tr><td><0</td><td>behind</td><td>gaining distance</td></tr>
	 * </table> 
	 * @return rychlost v [mps]. 
	 */
	double getRelativeSpeed(){
		double speed=0;
		if(positions.size()>1){
			int c=0;
			double sp1=0, sp2=0, dd, dt;
			OpponentPosition p0, p1;

			for(int i=positions.size()-2; i>positions.size()-5 && i>0; i--){
				p0=positions.get(positions.size()-1);
				p1=positions.get(i);

				dd = (determineVerticalDistance(p0.d)-determineVerticalDistance(p1.d));
				dt = (p0.t-p1.t);
				sp1 = dd/(dt/1000);
				if(!Double.isInfinite(sp1)&&!Double.isNaN(sp1)){
					sp2 += sp1;
					c++;
				}
//System.out.println("DetermineSpeed: " +
//		"p1.d="+p1.d+"("+determineAbsoluteDistance(p1.d)+")\t" +
//		"p0.d="+p0.d+"("+determineAbsoluteDistance(p0.d)+")\t" +
//		"p1.t="+p1.t+"\t" +
//		"p0.t="+p0.t+"\tdd="+dd+"; dt="+dt+"; sp1="+sp1);
				
//				if(!Double.isInfinite(sp1)&&!Double.isNaN(sp1))
//					break;
			}
			sp2 = sp2/c;
			speed=sp2;
		}
		return speed;
	}

	/**
	 * Gives position of the opponent.
	 * @return ORP according to the position of the opponent: BEHIND, BEFORE, LEFT, RIGHT or
	 * CRASH (if the opponent is too close)
	 *//*
	ORP getPositionByDistance(){
		ORP position = null;
		double vd = this.getActualVerticalDistance();
		double hd = this.getActualHorizontalDistance();

		if(vd<OpponentDetector.VDIST_TH){
			position = ORP.BEFORE;
		}else if(vd>-1*OpponentDetector.VDIST_TH){
			position = ORP.BEHIND;
		}else{
			if(hd<OpponentDetector.HDIST_TH){
				position = ORP.RIGHT;
			}else if(hd>-1*OpponentDetector.HDIST_TH){
				position = ORP.LEFT;
			}else{
				position = ORP.CRASH;
			}
		}
		return position;
	}
	*/
/*	ORP getPositionBySensors(){
//		if(OpponentDetector.getSensorAngle(this.sensor)<Math.PI/3 && 
//			OpponentDetector.getSensorAngle(this.sensor)>-1*Math.PI/3 )
//				return ORP.BEFORE;
//		else if(OpponentDetector.getSensorAngle(this.sensor)>=Math.PI/3 && 
//			OpponentDetector.getSensorAngle(this.sensor)<2*Math.PI/3)
//				return ORP.RIGHT;
//		else if((OpponentDetector.getSensorAngle(this.sensor)>=2*Math.PI/3 && 
//			OpponentDetector.getSensorAngle(this.sensor)<=Math.PI) 
//			|| (OpponentDetector.getSensorAngle(this.sensor)>=-1*Math.PI
//			&& OpponentDetector.getSensorAngle(this.sensor)<=-2*Math.PI/3))
//				return ORP.BEHIND;
//		else 
//			return ORP.LEFT;
		int h1=1, h5=18, h7=19, h11=35;
		double hd = this.getActualHorizontalDistance();
		ORP d=null;
		if((this.sensor < h1 || this.sensor >= h11) && (hd > -2 && hd < 2))
			d = ORP.BEHIND;
		else if(this.sensor >=h5 && this.sensor <= h7  && (hd > -2 && hd < 2))
			d = ORP.BEFORE;
		else if((this.sensor >=h1 && this.sensor < h5) || (hd <= -2))
			d = ORP.LEFT;
		else if((this.sensor > h7 && this.sensor < h11) || (hd >= 2))
			d = ORP.RIGHT;
		return d;
	}*/
	
	/**
	 * Computes a distance <i>parallel to the track axias</i> between the oppponent and the vehicle.
	 * @return a distance <b>&gt;0</b> if the opponent is <b>in front</b> of the vehicle or
	 * <b>&lt;0</b> if the opponent is <b>behind</b> the vehicle. In case that the these distances
	 * are <b>equal to 0</b>, a crash occurs.
	 */
	double getActualVerticalDistance(){
		return determineVerticalDistance(this.getActualDistance());
	}
	/**
	 * Computes a distance <i>perpendicular to the track axis</i> between the opponent and the vehicle.
	 * @return a distance <b>&gt;0</b> if the opponent is on <b>the right</b> side
	 * of the vehicle or <b>&lt;0</b> if the opponent is on <b>the left</b> side of the vehicle. 
	 * In case that these distances are <b>equal to 0</b>, a crash occurs.
	 */

	/*
	 * Vypocita <i>na osu trate kolmu</i> vzdialenost oponenta a autopilota.
	 * @return vzdialenost <b>&gt;0</b> ak oponent je <b>na pravej</b> strane 
	 * autopilota, alebo <b>&lt;0</b> ak oponent je <b>na lavej</b> strane autopilota. 
	 * V pripade, ze vzdialenost je <b>rovne 0</b> potom sa nastla <b>zrazka</b>.
	 * */
	double getActualHorizontalDistance(){
		return this.determineHorizontalDistance(this.getActualDistance());
	}
	
	double getActualDistance(){
		return this.positions.get(this.positions.size()-1).d;
	}

	private double determineVerticalDistance(double dist){
		return dist*Math.cos(OpponentDetector.getSensorAngle(this.sensor));
	}
	private double determineHorizontalDistance(double dist){
		return dist*Math.sin(OpponentDetector.getSensorAngle(this.sensor));
	}
	
	double getBlockSpeed(){
		double hd = Math.abs(this.getActualHorizontalDistance());
		double vd = Math.abs(this.getActualVerticalDistance());
		double speed = (vd*hd)/(hd+vd);
//		System.out.println(this.getActualDistance()+"\t"+speed);	//hd+"\t"+vd+"\t"+
		return speed;
	}
	
	@Override
	/**
	 * @return [sensor,positionCount,distance,sensorAngle,absoluteDistance,relativeSpeed] 
	 */
	public String toString(){
		OpponentPosition p = this.positions.get(this.positions.size()-1);
		return "["+sensor+","+positions.size()+","+p.d+","+OpponentDetector.getSensorAngle(this.sensor)+","+
				determineVerticalDistance(p.d)+","+this.getRelativeSpeed()+"]";
		
	}
	
	/**
	 * Computes a time necessary to cover a distance D[m] with a speed SP[mps]
	 * @return
	 */
	double determineContactTime() {
		double d, sp;
		d = this.getActualVerticalDistance();
		sp = this.getRelativeSpeed();
		return this.determineContactTime(sp, d);
	}
	double determineContactTime(double sp, double d) {
		return d/sp;
	}

	int getCarLenght(){
		return this.lenght;
	}
	int getSensor(){
		return this.sensor;
	}

	int getSensorMultiplier(){
		int m;
		if(this.sensor <= 17)
			m = 18 - this.sensor;
		else
			m = this.sensor - 17;
		return m;
	}

	double getHorizontalDistance(){
		return this.getHorizontalDistance(0);
	}

	double getHorizontalDistance(int offset){
		double od, pi, alfa, hd;
		int x;

		od = this.getActualDistance();
		pi = Math.PI;
		alfa = SENSOR_RAD;
		x = this.getSensorMultiplier()+offset;

		hd = (od*Math.sin(2*x*alfa))/(2*Math.sin((pi-2*x*alfa/2)));
		
		return hd;
	}

	double getWorstCaseHorizontalDistance(){
		double dist;
		if(this.sensor == 17 || this.sensor == 18){
			dist = 0;
		}else if(this.sensor < 17){
			dist = this.getHorizontalDistance(1);
		}else if(this.sensor > 18){
			dist = this.getHorizontalDistance(-1);
		}else
			dist = -1;
		return dist;
	}
	
	double getVerticalDistance(){
		return this.getVerticalDistance(0);
	}

	double getVerticalDistance(int offset){
		double vd, alfa, od, pi;
		int x;
		
		x = getSensorMultiplier()+offset;
		od = getActualDistance();
		pi = Math.PI;
		alfa = SENSOR_RAD;
		
		vd = od*Math.sin(pi/2 - x*alfa);
		return vd;
	}
	/**
	 * @param cs worst case switch: 
	 * 	if positive then the worst case is if the car is further, 
	 * 	if negative then the worst case is if the car is nearer.
	 * @return
	 */
	double getWorstCaseVerticalDistance(int cs){
		double dist;
		if(this.sensor == 17 || this.sensor == 18){
			dist = this.getVerticalDistance();
		}else if(this.sensor < 17){
			dist = this.getVerticalDistance(-1*((int)Math.signum(cs)));
		}else if(this.sensor > 18){
			dist = this.getVerticalDistance(1*((int)Math.signum(cs)));
		}else
			dist = -1;
		return dist;
	}
	
	/**
	 * 
	 * @return 0 front, -1 left, 1 right
	 */
	int getSide(){
		if(this.sensor == 17 || this.sensor == 18)
			return 0;
		else if(this.sensor > 18)
			return 1;
		else
			return -1;
	}

}
