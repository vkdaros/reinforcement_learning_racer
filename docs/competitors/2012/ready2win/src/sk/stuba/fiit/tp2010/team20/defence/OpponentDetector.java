package sk.stuba.fiit.tp2010.team20.defence;

import java.util.HashMap;

import controller.model.TickInfo;
import controller.model.TrackData;
import controller.model.Turn;
import controller.model.Turn.Direction;
import champ2011client.Action;
import champ2011client.SensorModel;

/**
 * @author Vojtech Juhasz
 *ORP = Opponent's Relative Position
 */
enum ORP {LEFT, RIGHT, BEFORE, BEHIND, CRASH};

/**
 * @author Vojtech Juhasz
 */
public class OpponentDetector {
	/**
	 * 0 - normal state
	 * 1 - state after overtake
	 */
	public static int OpponentState = 0;
	static final double HDIST_TH = 2.0;
	static final double VDIST_TH = 1.0;
	static final double ANGLE_TRASHOLD = 0.5;
	static final double TURN_TRASHOLD = 0.5;
	static final double TRACK_EDGE = 0.95;
	static final int OVERTAKE_DISTANCE=50;
	static final int BLOCK_DISTANCE=100;
	static final int BLOCK_DISTANCE_L2=60;
	static final int BLOCK_DISTANCE_L3=35;
	static final int BLOCK_DISTANCE_L4=20;
	static final int SAFE_DISTANCE = 5;
	
	static final int OPPONENT_SENSOR_COUNT = 36;
	private static HashMap<String, Opponent> opponentMap;
	private static double[] sensorAngles=null;
	
	static{
		opponentMap = new HashMap<String, Opponent>();
		
		sensorAngles = new double[OPPONENT_SENSOR_COUNT];
		double angle= -1*Math.PI;
		double angleDiff = 2*Math.PI/OPPONENT_SENSOR_COUNT;
		for(int i=0; i<sensorAngles.length; i++){
			sensorAngles[i] = angle;
			angle += angleDiff;
		}
	}

	public static Action monitor(TickInfo input, TrackData trackData){
		return monitor(input.getAction(), input.getSensorModel(), trackData);
	}

	public static Action monitor(Action a, SensorModel sm, TrackData trackData){
//		System.out.println("OpponentModul");
		double[] oppDist = sm.getOpponentSensors();
		double trackPosition = sm.getTrackPosition();
		int closestOpponentIdx = -1;

		//look for opponents and determine the closest (if any)
		for(int i=0; i<oppDist.length; i++){
			if(oppDist[i]>=200)
				continue;
			if(closestOpponentIdx==-1)
				closestOpponentIdx=i;
			else
				if(oppDist[i]<oppDist[closestOpponentIdx])
					closestOpponentIdx=i;

			if(opponentMap.containsKey(""+i)){
				opponentMap.get(""+i).addPosition(new OpponentPosition(i, oppDist[i]));
			}else{
				opponentMap.put(""+i, new Opponent(i, oppDist[i]));
			}
		}

		if(closestOpponentIdx!=-1){
			if((trackPosition >= -1) || (trackPosition <= 1)){
				//the car is on the track
				Opponent nearestOpp = opponentMap.get(""+closestOpponentIdx);
				if(nearestOpp.getActualVerticalDistance()<0){
					//&& (nearestOpp.getSensor() < 5 || nearestOpp.getSensor() > 31 )
					/*if(OpponentState == 1){
						a.brake = 1;
						return a;
					}*/
					//BLOCK
					if(nearestOpp.getRelativeSpeed()>=0 && 
							(-1*nearestOpp.getActualVerticalDistance())<BLOCK_DISTANCE){	
						//oponent sa priblizuje
						return blockOpponent(a, sm, nearestOpp, trackData);
					}else{
						//zanechavame oponenta
						return a;
					}
				}else if(nearestOpp.getActualVerticalDistance()>=0){
					//&& (nearestOpp.getSensor() < 15 || nearestOpp.getSensor() > 20 )
					//OVERTAKE

					/*if(nearestOpp.getActualDistance() > 20)
						OpponentState = 0;
					else if(OpponentState == 1){
						a.brake = 1;
						return a;
					}else
						return a;*/

					if(nearestOpp.getRelativeSpeed()<=0 && 
							nearestOpp.getActualVerticalDistance()<OVERTAKE_DISTANCE){	
						//priblizujeme sa k oponentovi
						return overtakeOpponent(a, sm, nearestOpp, trackData);
					}else{
						//oponent sa vzdialuje
						return a;
					}
				}
			}else{
				//the car is recovering
				//TODO implement recovery defence
			}
		}else{
			//reset defence
			if(opponentMap.size()>0){
				opponentMap.clear();
				System.out.println("RESET-DEFENCE");
			}
		}
		return a;
	}

	private static Action blockOpponent(Action a, SensorModel sm, Opponent o, TrackData nextTurn){
		// TODO BLOCK-OPPONENT
		Action action = a;	
//		System.out.print("BLOCK: ");
		double sd = a.steering;
		double ac = a.accelerate;
		double sp, hd, ad, ot;
		double vd = -1*o.getActualVerticalDistance();
//		double angle = sm.getAngleToTrackAxis();
		
		if(Math.abs(vd)>20)
			OpponentDetector.OpponentState = 1;
			
		if(true)
			return a;
//		if(Math.abs(vd)<=BLOCK_DISTANCE){
//			ORP oppPos = o.getPositionBySensors();
//			if(oppPos == ORP.BEHIND){
//				action = a;
//			}else if(oppPos==ORP.RIGHT){	// oponent je na pravej strane vozidla vodica
//				if(isOnTrackEdge(sm)!=1){	//vodic NIE je na PRAVOM okraji trate
					ad = o.getActualDistance();
					hd = o.getActualHorizontalDistance();
					sp = o.getRelativeSpeed();
//					
//					ot = o.determineContactTime(sp, vd+SAFE_DISTANCE);
//				}
//			}else if(oppPos==ORP.LEFT){
//				if(isOnTrackEdge(sm)!=-1){	//vodic NIE je na lavom kraji trate
//					sp = o.getBlockSpeed();
////					if(sp>40){
////						System.out.println(sp+" l2");
////					}else if(sp>15){
////						System.out.println(sp+" l1");
////					}else
////						System.out.println(sp+" l0");
//				}
//			}
//		}
//		
//		return action;

		System.out.print(vd+" | "+hd+" | "+a.steering+" | ");
		if(vd<=BLOCK_DISTANCE && (sm.getTrackPosition()<1 && sm.getTrackPosition()>-1)){
			sd += hd>0?-0.2:0.2;
			ac += 0.1;
			if(vd<=BLOCK_DISTANCE_L2 && (sm.getTrackPosition()<1 && sm.getTrackPosition()>-1)){
				sd += hd>0?-0.1:0.1;
				ac += 0.2;
				if(vd<=BLOCK_DISTANCE_L3 && (sm.getTrackPosition()<1 && sm.getTrackPosition()>-1)){
					if(hd>5){
						sd += hd>0?-0.10:0.10;
					}else if(hd>3){
						sd += hd>0?-0.30:0.30;
					}else{
						sd += hd>0?-0.40:0.40;
					}
					ac += 0.3;
					if(vd<=BLOCK_DISTANCE_L4 && (sm.getTrackPosition()<1 && sm.getTrackPosition()>-1)){
						sd += hd>0?-0.10:0.10;
						ac += 0.4;
					}
				}
			}
		}
		a.accelerate = ac;
		if(Math.abs(sd)>=TURN_TRASHOLD){
			System.out.print("T\t");
			a.steering = (sd/Math.abs(sd)) *TURN_TRASHOLD;
		}else{
			a.steering = sd;
		}
		System.out.print(a.steering+"\t"+a.accelerate+"\n");
		return a;
		
	}

	static double radianToDegree(double radian){
		return radian*180/Math.PI;
	}
	static double degreeToRadian(double degree){
		return degree*Math.PI/180;
	}
	/**
	 * 
	 * @param sensor
	 * @return an original angle of the given sensor in rads in the interval &lt;-PI, PI&gt, or if the sensor&lt;0 or &gt;<i>index of the last sensor</i>, then return 10
	 */
	static double getSensorAngle(int sensor){
		if(sensor>=0 && sensor<sensorAngles.length)
			return sensorAngles[sensor];
		return 10;
	}

	/**
	 * 
	 * @param sm
	 * @return 0 if the vehicle is not on the edge of the track
	 * +1 if the vehicle is on the right edge of the track
	 * -1 if the vehicle is on the left edge of the track
	 */
	static int isOnTrackEdge(SensorModel sm){
		double p = Math.abs(sm.getTrackPosition());
		if(p>=OpponentDetector.TRACK_EDGE)
			return 1;
		else if(p<=-1*OpponentDetector.TRACK_EDGE)
			return -1;
		return 0;
	}
	/**
	 * Determines if the turn of the car is dangerous. The computation is based
	 * on the current angle between the vehcile and the track, current speed and ...
	 * @param sm
	 * @return
	 */
	static boolean isCarDangerouslyTurned(SensorModel sm){
		return false;
	}

//	private static double OVERTAKE_TH_SLOW = 25;
//	//if the speed is 30kmph=8.33mps then we cover 70m in 8.4s
//	private static double OVERTAKE_TH_SPEED = 30;
//	private static double OVERTEAKE_ACCELERATION_RATE = 0.05;
	
	private static double OVERTAKE_HD = 3;
//	private static double OVERTAKE_STEER_RATE = 0.25;	//~10degrees
	private static double OVERTAKE_STEER_TH = 0.5;
	private static double OVERTAKE_CAR_ANGLE_TH_DEG = 5;
	private static double OVERTAKE_CAR_ANGLE_TH_RAD = Math.toRadians(OVERTAKE_CAR_ANGLE_TH_DEG);
	private static double OVERTAKE_CRITICAL_DISTANCE = 15;

	private static Action overtakeOpponent(Action a, SensorModel sm, Opponent o, TrackData trackData) {
//		System.out.println("OVERTAKE");
		Turn t;
		double d_nc, d_ov, d_start, d_nc_end;
		double hd, vd, ca, od, steerModify;
		int side, trEdge, sensor;
		boolean isInTurn;

		sensor = o.getSensor();
		System.out.println("sensor: "+sensor);
		
		if(sensor < 15 || sensor > 20){
			//System.out.println("Invalid sensor");
			return a;
		}
		trEdge = isOnTrackEdge(sm);
		side = o.getSide();

		if(trEdge != 0 && trEdge == side){
			System.out.println("On track edge");
			return a;
		}

		t = trackData.getNextTurn();
		od = o.getActualDistance();
		d_nc = t==null?0:t.getStartDistance();
		d_nc_end = t==null?0:t.getEndDistance();
		d_ov = assessOvertakeDistance(o, sm);
		d_start = sm.getDistanceRaced();
		ca = sm.getAngleToTrackAxis();
		vd = o.getWorstCaseVerticalDistance(1);
		
		isInTurn = d_start > d_nc && d_start < d_nc_end;

		System.out.println("od:"+od+" | "+side+" | "+isInTurn);
		if(od <= OVERTAKE_CRITICAL_DISTANCE && (side == 0 || isInTurn)){
			a.brake = Math.min(1.5/od, 1);
			System.out.println("Modify brake: "+a.brake+" ["+(1/od)+"]"+isInTurn);
		}
//		System.out.println("NTurn AssDist: "+d_nc+"\t"+(d_start+d_ov));
//		if(d_nc >= d_ov+d_start){
//			System.out.println("Can overtake");
			hd = o.getWorstCaseHorizontalDistance();

			steerModify = determineOvertakeSteering(a.steering, hd, vd, ca, od, isInTurn);
			steerModify *= (side==0?1:side); 
			System.out.println("\tsm: "+steerModify);
//		}else{
//			System.out.println("Cannot overtake");
//			steerModify = 0;
//		}
		
		a.steering += steerModify;
		return a;
	}
	
	/**
	 * 
	 * @param steering
	 * @param hd
	 * @param vd
	 * @param carAngle
	 * @param od
	 * @return absolute value of the required steering modification rate (angle/45degrees)
	 */
	static double determineOvertakeSteering(double steering, double hd, 
			double vd, double carAngle, double od, boolean inTurn){
		double angle = 0;
	
		if(Math.abs(hd) < OVERTAKE_HD &&
				Math.abs(steering) < OVERTAKE_STEER_TH &&
				( Math.abs(carAngle) < OVERTAKE_CAR_ANGLE_TH_RAD) && 
				true){
			angle = getSteeringAngle(od);
			angle = Math.toRadians(angle)/(Math.PI/4);
//			System.out.println("DOS: od hd |A: "+od+" "+hd+" "+" | "+angle);
		}
		return angle;
	}

	static double assessOvertakeDistance(Opponent o, SensorModel sm){
		double opd, ot, ovd, vd, vcur, vrel;
		
		vcur = kmhToMps(sm.getSpeed());
		vd = o.getWorstCaseVerticalDistance(1);
		vrel = Math.abs(o.getRelativeSpeed());		
		opd = vd+SAFE_DISTANCE;
		ot = opd/vrel;
		ovd = vcur*ot;
//System.out.println("worst case vd:"+vd+" "+vrel);
		return ovd;
	}
	
	static double kmhToMps(double k){
		return k/3.6;
	}

	static double getSteeringAngle(double od){
		double angle = 0;
		if(od <= 30 && od > 25){angle = 6;
		}else if(od <= 25 && od > 20){angle = 7;
		}else if(od <= 20 && od > 16){angle = 9;
		}else if(od <= 16 && od > 15){angle = 11;
		}else if(od <= 15 && od > 14){angle = 12;
		}else if(od <= 14 && od > 13){angle = 13;
		}else if(od <= 13 && od > 12){angle = 14;
		}else if(od <= 12 && od > 11){angle = 15;
		}else if(od <= 11 && od > 10){angle = 17;
		}else if(od <= 10 && od > 9){angle = 19;
		}else if(od <= 9 && od > 8){angle = 21;
		}else if(od <= 8 && od > 7){angle = 25;
		}else if(od <= 7 && od > 6){angle = 30;
		}else if(od <= 6) angle = 35;
		else angle = 0;
		angle *= 4;
		return angle;
	}
}
