package sk.stuba.fiit.tp2010.team20.defence;

public class OpponentPosition {
	double d;
	long t=-1;
	int s=-1;

	public OpponentPosition(int sensor, double distance, long timestamp){
		this.d=distance;
		this.t=timestamp;
		this.s=sensor;
	}
	
	public OpponentPosition(int sensor, double distance){
		this(sensor, distance, new java.util.Date().getTime());
	}
}
