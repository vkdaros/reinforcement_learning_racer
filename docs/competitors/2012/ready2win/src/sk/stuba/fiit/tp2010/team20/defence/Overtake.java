package sk.stuba.fiit.tp2010.team20.defence;

import controller.ESP;
import controller.model.Turn;
import champ2011client.Action;
import champ2011client.SensorModel;

public class Overtake {
	int detected = 0;
	double steerPrevious = 0;
	int braking = 0;
	boolean first=true;
	
	public Action overtaking(SensorModel sm, Action action, double width, Turn turn)
	{
		Action act = action;
		boolean inTurn = ((turn.getStartDistance() <= sm.getDistanceFromStartLine()) 
				&& (turn.getEndDistance() >= sm.getDistanceFromStartLine()));
		byte opponents = 0;
		first = false;
		
		boolean debugging=true;
		
		if ((sm.getSpeed() < 40) && (braking > 50))
		{
			if (sm.getTrackPosition() > 0)
			{
				act.steering = -1;
			}
			else
			{
				act.steering = 1;
			}
			steerPrevious = act.steering;
			return act;
		}
						
		if (sm.getOpponentSensors()[16]<20)
			opponents += 8;
		if (sm.getOpponentSensors()[17]<25)
			opponents += 4;
		if (sm.getOpponentSensors()[18]<25)
			opponents += 2;
		if (sm.getOpponentSensors()[19]<20)
			opponents += 1;
				
		if (opponents == 0)
		{
			detected = 0;
			act=dontCrash(sm, action, width);
			if (Math.abs(steerPrevious-act.steering)>0.08)
			{
				if (steerPrevious > act.steering)
					act.steering = steerPrevious-0.08;
				else
					act.steering = steerPrevious+0.08;
			}
			steerPrevious = act.steering;
			return act;
		}
		else
		{
			detected++;
			if ((sm.getOpponentSensors()[17]<8) || (sm.getOpponentSensors()[18]<8)
					 || (sm.getOpponentSensors()[16]<6) || (sm.getOpponentSensors()[19]<6))
			{
				braking++;
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
				if (debugging)	debug(opponents, sm, act.steering);
				steerPrevious = act.steering;
				return act;
			}
		}
		
		if (act.steering >= 0)
		{
			if (opponents == 1)
				;
			if ((opponents == 2) || (opponents == 3))
			{
				if ((act.steering < 0.1) && 
						(sm.getTrackPosition()<(width/2-1.2)/(width/2)))
					act.steering = 0.1;			
			}
			if (opponents == 4)
			{
				if ((act.steering > 0.3) &&
						(sm.getTrackPosition()<(width/2-7)/(width/2)))
					;
				else
				{
					if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
					{
						if (inTurn && turn.getDirection().name().equals("LEFT"))
						{
							act.steering = 0;
						}
						else
						{
							act.steering = -0.1;
						}
					}
					else
					{
						act.accelerate=0;
						action.brake = brakeOvertake(sm);
					}
				}
			}
			if ((opponents == 5) || (opponents == 7))
			{
				if (sm.getTrackPosition()<(width/2-7)/(width/2))
				{
					if (act.steering < 0.3)
						act.steering = 0.3;
				}
				else
				{
					act.accelerate=0;
					action.brake = brakeOvertake(sm);
				}
			}
			if (opponents == 6)
			{
				if (sm.getTrackPosition()<(width/2-5)/(width/2))
				{
					if (act.steering < 0.3)
						act.steering = 0.3;
				}
				else
				{
					if (inTurn && turn.getDirection().name().equals("LEFT"))
					{
						act.steering = 0;
					}
					else
					{
						act.steering = -0.2;
					}
				}
			}
			if ((opponents == 8) || (opponents == 12))
			{
				if (sm.getTrackPosition()>-(width/2-2)/(width/2))
				{
					if (detected < 10)
					{
						act.steering = -0.01*detected;
					}
					else
					{
						act.steering = -0.1;
					}
				}
				else
				{
					act.accelerate = 0;
					action.brake = brakeOvertake(sm);
				}
			}
			if (opponents == 9)
			{
				act.steering = 0;
			}
			if ((opponents == 10) || (opponents == 14))
			{
				if (sm.getTrackPosition()>-(width/2-5)/(width/2))
				{
					if (inTurn && turn.getDirection().name().equals("LEFT"))
					{
						act.steering = 0;
					}
					else
					{
						act.steering = -0.2;
					}
				}
				else
				{
					act.accelerate = 0;
					action.brake = brakeOvertake(sm);
				}
			}
			if ((opponents == 11) || (opponents == 13) || (opponents == 15))
			{
				act.accelerate=0;
				action.brake = brakeOvertake(sm);
			}
		}
		else
		{
			if (opponents == 8)
				;
			if ((opponents == 4) || (opponents == 12))
			{
				if ((act.steering > -0.1) && 
						(sm.getTrackPosition()>-(width/2-1.2)/(width/2)))
					act.steering = -0.1;			
			}
			if (opponents == 2)
			{
				if ((act.steering < -0.3) &&
						(sm.getTrackPosition()>-(width/2-7)/(width/2)))
					;
				else
				{
					if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
					{
						if (inTurn && turn.getDirection().name().equals("RIGHT"))
						{
							act.steering = 0;
						}
						else
						{
							act.steering = 0.1;
						}
					}
					else
					{
						act.accelerate=0;
						action.brake = brakeOvertake(sm);
					}
				}
			}
			if ((opponents == 10) || (opponents == 14))
			{
				if (sm.getTrackPosition()>-(width/2-7)/(width/2))
				{
					if (act.steering > -0.3)
						act.steering = -0.3;
				}
				else
				{
					act.accelerate=0;
					action.brake = brakeOvertake(sm);
				}
			}
			if (opponents == 6)
			{
				if (sm.getTrackPosition()>-(width/2-5)/(width/2))
				{
					if (act.steering > -0.3)
						act.steering = -0.3;
				}
				else
				{
					if (inTurn && turn.getDirection().name().equals("LEFT"))
					{
						act.steering = 0;
					}
					else
					{
						act.steering = 0.2;
					}
				}
			}
			if ((opponents == 1) || (opponents == 3))
			{
				if (sm.getTrackPosition()<(width/2-2)/(width/2))
				{
					if (detected < 10)
					{
						act.steering = 0.01*detected;
					}
					else
					{
						act.steering = 0.1;
					}
				}
				else
				{
					act.accelerate = 0;
					action.brake = brakeOvertake(sm);
				}
			}
			if (opponents == 9)
			{
				act.steering = 0;
			}
			if ((opponents == 5) || (opponents == 7))
			{
				if (sm.getTrackPosition()<(width/2-5)/(width/2))
				{
					if (inTurn && turn.getDirection().name().equals("LEFT"))
					{
						act.steering = 0;
					}
					else
					{
						act.steering = 0.2;
					}
				}
				else
				{
					act.accelerate = 0;
					action.brake = brakeOvertake(sm);
				}
			}
			if ((opponents == 11) || (opponents == 13) || (opponents == 15))
			{
				act.accelerate=0;
				action.brake = brakeOvertake(sm);
			}
		}
		act = dontCrash(sm, act, width);
		if (Math.abs(steerPrevious-act.steering)>0.08)
		{
			if (steerPrevious > act.steering)
				act.steering = steerPrevious-0.08;
			else
				act.steering = steerPrevious+0.08;
		}
		if (debugging)	debug(opponents, sm, act.steering);
		steerPrevious = act.steering;
		return act;
	}
	
	private void debug(byte opponents, SensorModel sm, double steering) {
		System.out.println("opponents debug: opponents "+opponents+", trackPos "+
				sm.getTrackPosition()+", steering "+steering);		
	}

	private Action dontCrash(SensorModel sm, Action action, double width)
	{
		Action act = action;
		if ((sm.getOpponentSensors()[2]<5.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[3]<4.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[4]<3.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[5]<3.2) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[6]<3) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[7]<3) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[8]<2.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[9]<2.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[10]<3) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[11]<3) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[12]<3.2) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[13]<3.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[14]<4.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[15]<5.5) && (act.steering>-0.05))
		{
			if (sm.getTrackPosition()>-(width/2-1.2)/(width/2))
				act.steering=-0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[20]<5.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[21]<4.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[22]<3.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[23]<3.2) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[24]<3) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[25]<3) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[26]<2.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
			{
				act.accelerate = 0;
				action.brake = brakeOvertake(sm);
			}
		}
		if ((sm.getOpponentSensors()[27]<2.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[28]<3) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[29]<3) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[30]<3.2) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[31]<3.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[32]<4.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		if ((sm.getOpponentSensors()[33]<5.5) && (act.steering<0.05))
		{
			if (sm.getTrackPosition()<(width/2-1.2)/(width/2))
				act.steering=0.05;
			else
				;
		}
		return act;
	}
	private double brakeOvertake(SensorModel sm)
	{
	//	return ESP.filterABS(sm, ESP.getBrakeActual());
		return 0.8;
	}
}
