package telemetry;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * A class for a storage of data necessary for graph generation of specific variables
 * chosen for offline analysis of the data received from the sensors and the data sent
 * by the effectors.
 */
public class DataStorage implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ArrayList<DataUnit> datas = new ArrayList<DataUnit>();
	//an array of integers representing data units of the first meters of the first laps
	private ArrayList<Integer> firstMeters = new ArrayList<Integer>();
	private double trackWidth;
	private double trackLength;
	private int laps;
	private Visualizer gui;
	
	public void store(DataUnit unit)
	{
		datas.add(unit);
	}
	
	public void addFirstMeters(int unit)
	{
		firstMeters.add(unit);
	}
	
	public void setWidth(double width)
	{
		trackWidth = width;
	}
	
	public double getWidth()
	{
		return trackWidth;
	}
	
	public void setLength(double length)
	{
		trackLength = length;
	}
	
	public double getLength()
	{
		return trackLength;
	}
	
	public DataUnit getLast()
	{
		return datas.get(datas.size()-1);
	}
	public int getCount()
	{
		return datas.size();
	}
	
	public void visualize()
	{
		gui = new Visualizer(datas, trackLength, firstMeters);
	}
}
