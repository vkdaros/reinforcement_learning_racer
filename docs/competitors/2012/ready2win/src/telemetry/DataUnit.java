package telemetry;

import java.io.Serializable;

/*
 * A class representing a data unit for a storage of values from the specific sector of the track.
 */
public class DataUnit implements Serializable
{
	private static final long serialVersionUID = 1L;
	private double accel;
	private double speed;
	private double rpm;
	private int gear;
	private double trackPos;
	private double time;
	private double distance;
	
	public DataUnit(double accel, double speed, double rpm, int gear, double trackPos, double time, double distance)
	{
		this.accel = accel;
		this.speed = speed;
		this.rpm = rpm;
		this.gear = gear;
		this.trackPos = trackPos;
		this.time = time;
		this.distance = distance;
	}
	
	public double getAccel()
	{
		return accel;
	}
	
	public double getSpeed()
	{
		return speed;
	}
	
	public double getRpm()
	{
		return rpm;
	}
	
	public int getGear()
	{
		return gear;
	}
	
	public double getTrackPos()
	{
		return trackPos;
	}
	
	public double getTime()
	{
		return time;
	}
	
	public double getDistance()
	{
		return distance;
	}
}
