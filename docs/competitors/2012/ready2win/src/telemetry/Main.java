package telemetry;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Main {
	static DataStorage dS;
	
	public static void main(String[] args) {
		File dir = new File ("./telemetry");
		JFileChooser fileopen = new JFileChooser(dir);
	    FileFilter filter = new FileNameExtensionFilter("telemetry analyzer data", "tad");
	    fileopen.addChoosableFileFilter(filter);

	    int ret = fileopen.showDialog(null, "Open file");

	    if (ret == JFileChooser.APPROVE_OPTION) {
	      File file = fileopen.getSelectedFile();
	      try {
			dS = readTelemetryData(file);
			dS.visualize();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      System.out.println(file);
	    }
	  }

	

	static DataStorage readTelemetryData(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
		ObjectInput input;
		if(file.exists()){
			input = new ObjectInputStream( new BufferedInputStream( new FileInputStream( file )) );
		
			return (DataStorage) input.readObject();
		}
		
		return null;

	}
}
