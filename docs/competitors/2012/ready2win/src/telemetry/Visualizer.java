package telemetry;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/*
 * A class visualizing data received from sensors and sent by effectors.
 */
public class Visualizer extends JFrame implements ActionListener
{
	private ArrayList<DataUnit> nodes;
	JComboBox lap;
	Renderer gr;
	ArrayList<JLabel[]> labels, labelsD;
	JCheckBox b1, b2, b3, b4, b5;
	int chosen=2;
	
	public Visualizer(ArrayList<DataUnit> datas, double length, ArrayList<Integer> firsts)
	{
		setTitle("Telemetry");
		setResizable(false);
		setBounds(100, 80, 800, 600);
		
		final Container con = getContentPane();
		con.setLayout(null);
				
		b1 = new JCheckBox("acceleration");
		b1.setMargin(new Insets(1,1,1,1));
		b1.setBounds(10, 10, 100, 20);
		b1.setBackground(new Color(255, 255, 255));
		b1.setActionCommand("acceleration");
        b1.addActionListener(this);
        b1.setSelected(true);
        con.add(b1);
        
        b2 = new JCheckBox("speed");
		b2.setMargin(new Insets(1,1,1,1));
		b2.setBounds(130, 10, 100, 20);
		b2.setBackground(new Color(255, 255, 255));
		b2.setActionCommand("speed");
        b2.addActionListener(this);
        b2.setSelected(true);
        con.add(b2);
        
        b3 = new JCheckBox("rpm");
		b3.setMargin(new Insets(1,1,1,1));
		b3.setBounds(250, 10, 100, 20);
		b3.setBackground(new Color(255, 255, 255));
		b3.setActionCommand("rpm");
        b3.addActionListener(this);
        b3.setEnabled(false);
        con.add(b3);
        
        b4 = new JCheckBox("gear");
		b4.setMargin(new Insets(1,1,1,1));
		b4.setBounds(370, 10, 100, 20);
		b4.setBackground(new Color(255, 255, 255));
		b4.setActionCommand("gear");
        b4.addActionListener(this);
        b4.setEnabled(false);
        con.add(b4);
        
        b5 = new JCheckBox("trackPos");
		b5.setMargin(new Insets(1,1,1,1));
		b5.setBounds(490, 10, 100, 20);
		b5.setBackground(new Color(255, 255, 255));
		b5.setActionCommand("trackPos");
        b5.addActionListener(this);
        b5.setEnabled(false);
        con.add(b5);        
        
        labels = new ArrayList<JLabel[]>();
        
        JLabel[] distinctLabels = new JLabel[5];
        
        JLabel label = new JLabel("0");
        label.setBounds(50, 160, 25, 20);
        distinctLabels[0] = label;
        con.add(label);
        
        JLabel l2 = new JLabel("0.5");
        l2.setBounds(50, 110, 25, 20);
        distinctLabels[1] = l2;
        con.add(l2);
        
        JLabel l3 = new JLabel("1");
        l3.setBounds(50, 60, 25, 20);
        distinctLabels[2] = l3;
        con.add(l3);
        
        JLabel l4 = new JLabel("-0.5");
        l4.setBounds(50, 210, 25, 20);
        distinctLabels[3] = l4;
        con.add(l4);
        
        JLabel l5 = new JLabel("-1");
        l5.setBounds(50, 260, 15, 20);
        distinctLabels[4] = l5;
        con.add(l5);
        
        labels.add(distinctLabels);
        
        distinctLabels = new JLabel[9];
        
        JLabel label_2 = new JLabel("100");
        label_2.setBounds(50, 160, 40, 20);
        distinctLabels[0] = label_2;
        con.add(label_2);
        
        JLabel l6_2 = new JLabel("150");
        l6_2.setBounds(50, 135, 40, 20);
        distinctLabels[1] = l6_2;
        con.add(l6_2);
        
        JLabel l2_2 = new JLabel("200");
        l2_2.setBounds(50, 110, 40, 20);
        distinctLabels[2] = l2_2;
        con.add(l2_2);
        
        JLabel l7_2 = new JLabel("250");
        l7_2.setBounds(50, 85, 40, 20);
        distinctLabels[3] = l7_2;
        con.add(l7_2);
        
        JLabel l3_2 = new JLabel("300");
        l3_2.setBounds(50, 60, 40, 20);
        distinctLabels[4] = l3_2;
        con.add(l3_2);
        
        JLabel l8_2 = new JLabel("50");
        l8_2.setBounds(50, 185, 40, 20);
        distinctLabels[5] = l8_2;
        con.add(l8_2);
        
        JLabel l4_2 = new JLabel("0");
        l4_2.setBounds(50, 210, 40, 20);
        distinctLabels[6] = l4_2;
        con.add(l4_2);
        
        JLabel l9_2 = new JLabel("-50");
        l9_2.setBounds(50, 235, 40, 20);
        distinctLabels[7] = l9_2;
        con.add(l9_2);
        
        JLabel l5_2 = new JLabel("-100");
        l5_2.setBounds(50, 260, 40, 20);
        distinctLabels[8] = l5_2;
        con.add(l5_2);
        
        labels.add(distinctLabels);
        
        distinctLabels = new JLabel[11];
        
        JLabel label_3 = new JLabel("5000");
        label_3.setBounds(50, 160, 40, 20);
        distinctLabels[0] = label_3;
        con.add(label_3);
        
        JLabel l6_3 = new JLabel("6000");
        l6_3.setBounds(50, 140, 40, 20);
        distinctLabels[1] = l6_3;
        con.add(l6_3);
        
        JLabel l2_3 = new JLabel("7000");
        l2_3.setBounds(50, 120, 40, 20);
        distinctLabels[2] = l2_3;
        con.add(l2_3);
        
        JLabel l7_3 = new JLabel("8000");
        l7_3.setBounds(50, 100, 40, 20);
        distinctLabels[3] = l7_3;
        con.add(l7_3);
        
        JLabel l3_3 = new JLabel("9000");
        l3_3.setBounds(50, 80, 40, 20);
        distinctLabels[4] = l3_3;
        con.add(l3_3);
        
        JLabel l8_3 = new JLabel("10000");
        l8_3.setBounds(50, 60, 40, 20);
        distinctLabels[5] = l8_3;
        con.add(l8_3);
        
        JLabel l4_3 = new JLabel("4000");
        l4_3.setBounds(50, 180, 40, 20);
        distinctLabels[6] = l4_3;
        con.add(l4_3);
        
        JLabel l9_3 = new JLabel("3000");
        l9_3.setBounds(50, 200, 40, 20);
        distinctLabels[7] = l9_3;
        con.add(l9_3);
        
        JLabel l5_3 = new JLabel("2000");
        l5_3.setBounds(50, 220, 40, 20);
        distinctLabels[8] = l5_3;
        con.add(l5_3);
        
        JLabel l10_3 = new JLabel("1000");
        l10_3.setBounds(50, 240, 40, 20);
        distinctLabels[9] = l10_3;
        con.add(l10_3);
        
        JLabel l11_3 = new JLabel("0");
        l11_3.setBounds(50, 260, 40, 20);
        distinctLabels[10] = l11_3;
        con.add(l11_3);
        
        labels.add(distinctLabels);
        
        distinctLabels = new JLabel[9];
        
        JLabel label_4 = new JLabel("3");
        label_4.setBounds(50, 160, 25, 20);
        distinctLabels[0] = label_4;
        con.add(label_4);
        
        JLabel l6_4 = new JLabel("4");
        l6_4.setBounds(50, 135, 25, 20);
        distinctLabels[1] = l6_4;
        con.add(l6_4);
        
        JLabel l2_4 = new JLabel("5");
        l2_4.setBounds(50, 110, 25, 20);
        distinctLabels[2] = l2_4;
        con.add(l2_4);
        
        JLabel l7_4 = new JLabel("6");
        l7_4.setBounds(50, 85, 25, 20);
        distinctLabels[3] = l7_4;
        con.add(l7_4);
        
        JLabel l3_4 = new JLabel("7");
        l3_4.setBounds(50, 60, 25, 20);
        distinctLabels[4] = l3_4;
        con.add(l3_4);
        
        JLabel l8_4 = new JLabel("2");
        l8_4.setBounds(50, 185, 25, 20);
        distinctLabels[5] = l8_4;
        con.add(l8_4);
        
        JLabel l4_4 = new JLabel("1");
        l4_4.setBounds(50, 210, 25, 20);
        distinctLabels[6] = l4_4;
        con.add(l4_4);
        
        JLabel l9_4 = new JLabel("0");
        l9_4.setBounds(50, 235, 25, 20);
        distinctLabels[7] = l9_4;
        con.add(l9_4);
        
        JLabel l5_4 = new JLabel("-1");
        l5_4.setBounds(50, 260, 15, 20);
        distinctLabels[8] = l5_4;
        con.add(l5_4);
        
        labels.add(distinctLabels);
        
        distinctLabels = new JLabel[5];
        
        JLabel label_5 = new JLabel("0");
        label_5.setBounds(50, 160, 25, 20);
        distinctLabels[0] = label_5;
        con.add(label_5);
        
        JLabel l2_5 = new JLabel("0.5");
        l2_5.setBounds(50, 110, 25, 20);
        distinctLabels[1] = l2_5;
        con.add(l2_5);
        
        JLabel l3_5 = new JLabel("1");
        l3_5.setBounds(50, 60, 25, 20);
        distinctLabels[2] = l3_5;
        con.add(l3_5);
        
        JLabel l4_5 = new JLabel("-0.5");
        l4_5.setBounds(50, 210, 25, 20);
        distinctLabels[3] = l4_5;
        con.add(l4_5);
        
        JLabel l5_5 = new JLabel("-1");
        l5_5.setBounds(50, 260, 15, 20);
        distinctLabels[4] = l5_5;
        con.add(l5_5);
        
        labels.add(distinctLabels);
        
        
        
        labelsD = new ArrayList<JLabel[]>();
        
        JLabel[] distinctLabelsD = new JLabel[5];
        
        JLabel labelD = new JLabel("0");
        labelD.setBounds(50, 160+250, 25, 20);
        distinctLabelsD[0] = labelD;
        con.add(labelD);
        
        JLabel l2D = new JLabel("0.5");
        l2D.setBounds(50, 110+250, 25, 20);
        distinctLabelsD[1] = l2D;
        con.add(l2D);
        
        JLabel l3D = new JLabel("1");
        l3D.setBounds(50, 60+250, 25, 20);
        distinctLabelsD[2] = l3D;
        con.add(l3D);
        
        JLabel l4D = new JLabel("-0.5");
        l4D.setBounds(50, 210+250, 25, 20);
        distinctLabelsD[3] = l4D;
        con.add(l4D);
        
        JLabel l5D = new JLabel("-1");
        l5D.setBounds(50, 260+250, 15, 20);
        distinctLabelsD[4] = l5D;
        con.add(l5D);
        
        labelsD.add(distinctLabelsD);
        
        distinctLabelsD = new JLabel[9];
        
        JLabel label_2D = new JLabel("100");
        label_2D.setBounds(50, 160+250, 40, 20);
        distinctLabelsD[0] = label_2D;
        con.add(label_2D);
        
        JLabel l6_2D = new JLabel("150");
        l6_2D.setBounds(50, 135+250, 40, 20);
        distinctLabelsD[1] = l6_2D;
        con.add(l6_2D);
        
        JLabel l2_2D = new JLabel("200");
        l2_2D.setBounds(50, 110+250, 40, 20);
        distinctLabelsD[2] = l2_2D;
        con.add(l2_2D);
        
        JLabel l7_2D = new JLabel("250");
        l7_2D.setBounds(50, 85+250, 40, 20);
        distinctLabelsD[3] = l7_2D;
        con.add(l7_2D);
        
        JLabel l3_2D = new JLabel("300");
        l3_2D.setBounds(50, 60+250, 40, 20);
        distinctLabelsD[4] = l3_2D;
        con.add(l3_2D);
        
        JLabel l8_2D = new JLabel("50");
        l8_2D.setBounds(50, 185+250, 40, 20);
        distinctLabelsD[5] = l8_2D;
        con.add(l8_2D);
        
        JLabel l4_2D = new JLabel("0");
        l4_2D.setBounds(50, 210+250, 40, 20);
        distinctLabelsD[6] = l4_2D;
        con.add(l4_2D);
        
        JLabel l9_2D = new JLabel("-50");
        l9_2D.setBounds(50, 235+250, 40, 20);
        distinctLabelsD[7] = l9_2D;
        con.add(l9_2D);
        
        JLabel l5_2D = new JLabel("-100");
        l5_2D.setBounds(50, 260+250, 40, 20);
        distinctLabelsD[8] = l5_2D;
        con.add(l5_2D);
        
        labelsD.add(distinctLabelsD);
        
        distinctLabelsD = new JLabel[11];
        
        JLabel label_3D = new JLabel("5000");
        label_3D.setBounds(50, 160+250, 40, 20);
        distinctLabelsD[0] = label_3D;
        con.add(label_3D);
        
        JLabel l6_3D = new JLabel("6000");
        l6_3D.setBounds(50, 140+250, 40, 20);
        distinctLabelsD[1] = l6_3D;
        con.add(l6_3D);
        
        JLabel l2_3D = new JLabel("7000");
        l2_3D.setBounds(50, 120+250, 40, 20);
        distinctLabelsD[2] = l2_3D;
        con.add(l2_3D);
        
        JLabel l7_3D = new JLabel("8000");
        l7_3D.setBounds(50, 100+250, 40, 20);
        distinctLabelsD[3] = l7_3D;
        con.add(l7_3D);
        
        JLabel l3_3D = new JLabel("9000");
        l3_3D.setBounds(50, 80+250, 40, 20);
        distinctLabelsD[4] = l3_3D;
        con.add(l3_3D);
        
        JLabel l8_3D = new JLabel("10000");
        l8_3D.setBounds(50, 60+250, 40, 20);
        distinctLabelsD[5] = l8_3D;
        con.add(l8_3D);
        
        JLabel l4_3D = new JLabel("4000");
        l4_3D.setBounds(50, 180+250, 40, 20);
        distinctLabelsD[6] = l4_3D;
        con.add(l4_3D);
        
        JLabel l9_3D = new JLabel("3000");
        l9_3D.setBounds(50, 200+250, 40, 20);
        distinctLabelsD[7] = l9_3D;
        con.add(l9_3D);
        
        JLabel l5_3D = new JLabel("2000");
        l5_3D.setBounds(50, 220+250, 40, 20);
        distinctLabelsD[8] = l5_3D;
        con.add(l5_3D);
        
        JLabel l10_3D = new JLabel("1000");
        l10_3D.setBounds(50, 240+250, 40, 20);
        distinctLabelsD[9] = l10_3D;
        con.add(l10_3D);
        
        JLabel l11_3D = new JLabel("0");
        l11_3D.setBounds(50, 260+250, 40, 20);
        distinctLabelsD[10] = l11_3D;
        con.add(l11_3D);
        
        labelsD.add(distinctLabelsD);
        
        distinctLabelsD = new JLabel[9];
        
        JLabel label_4D = new JLabel("3");
        label_4D.setBounds(50, 160+250, 25, 20);
        distinctLabelsD[0] = label_4D;
        con.add(label_4D);
        
        JLabel l6_4D = new JLabel("4");
        l6_4D.setBounds(50, 135+250, 25, 20);
        distinctLabelsD[1] = l6_4D;
        con.add(l6_4D);
        
        JLabel l2_4D = new JLabel("5");
        l2_4D.setBounds(50, 110+250, 25, 20);
        distinctLabelsD[2] = l2_4D;
        con.add(l2_4D);
        
        JLabel l7_4D = new JLabel("6");
        l7_4D.setBounds(50, 85+250, 25, 20);
        distinctLabelsD[3] = l7_4D;
        con.add(l7_4D);
        
        JLabel l3_4D = new JLabel("7");
        l3_4D.setBounds(50, 60+250, 25, 20);
        distinctLabelsD[4] = l3_4D;
        con.add(l3_4D);
        
        JLabel l8_4D = new JLabel("2");
        l8_4D.setBounds(50, 185+250, 25, 20);
        distinctLabelsD[5] = l8_4D;
        con.add(l8_4D);
        
        JLabel l4_4D = new JLabel("1");
        l4_4D.setBounds(50, 210+250, 25, 20);
        distinctLabelsD[6] = l4_4D;
        con.add(l4_4D);
        
        JLabel l9_4D = new JLabel("0");
        l9_4D.setBounds(50, 235+250, 25, 20);
        distinctLabelsD[7] = l9_4D;
        con.add(l9_4D);
        
        JLabel l5_4D = new JLabel("-1");
        l5_4D.setBounds(50, 260+250, 15, 20);
        distinctLabelsD[8] = l5_4D;
        con.add(l5_4D);
        
        labelsD.add(distinctLabelsD);
        
        distinctLabelsD = new JLabel[5];
        
        JLabel label_5D = new JLabel("0");
        label_5D.setBounds(50, 160+250, 25, 20);
        distinctLabelsD[0] = label_5D;
        con.add(label_5D);
        
        JLabel l2_5D = new JLabel("0.5");
        l2_5D.setBounds(50, 110+250, 25, 20);
        distinctLabelsD[1] = l2_5D;
        con.add(l2_5D);
        
        JLabel l3_5D = new JLabel("1");
        l3_5D.setBounds(50, 60+250, 25, 20);
        distinctLabelsD[2] = l3_5D;
        con.add(l3_5D);
        
        JLabel l4_5D = new JLabel("-0.5");
        l4_5D.setBounds(50, 210+250, 25, 20);
        distinctLabelsD[3] = l4_5D;
        con.add(l4_5D);
        
        JLabel l5_5D = new JLabel("-1");
        l5_5D.setBounds(50, 260+250, 15, 20);
        distinctLabelsD[4] = l5_5D;
        con.add(l5_5D);
        
        labelsD.add(distinctLabelsD);
        
        JLabel l6 = new JLabel("lap");
        l6.setBounds(630, 10, 30, 20);
        con.add(l6);
        
        lap = new JComboBox();
        lap.setBounds(670, 10, 80, 20);
        con.add(lap);
        
        double count = datas.get(datas.size()-1).getDistance()/length;
        for (int i=0; i<count; i++)
			lap.addItem(i+1);
        	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gr = new Renderer(datas, length, firsts, this);
		JScrollPane scrollpane = new JScrollPane(gr);
		scrollpane.setBounds(100, 50, 680, 510);
	//	gr.setPreferredSize(new Dimension(800, 400));
		con.add(scrollpane);
		
		setContentPane(con);
		setVisible(true);
	}

	
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("acceleration"))
		{
			if (b1.isSelected())
			{
				chosen++;
				if (chosen == 2)
				{
					setSelectable(false);
					if (b2.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 2);
					if (b3.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 3);
					if (b4.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 4);
					if (b5.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 5);
				}
			}
			else
			{
				chosen--;
				setSelectable(true);
			}
		}
		if (e.getActionCommand().equals("speed"))
		{
			if (b2.isSelected())
			{
				chosen++;
				if (chosen == 2)
				{
					setSelectable(false);
					if (b1.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 2);
					if (b3.isSelected())
						gr.render(lap.getSelectedIndex(), 2, 3);
					if (b4.isSelected())
						gr.render(lap.getSelectedIndex(), 2, 4);
					if (b5.isSelected())
						gr.render(lap.getSelectedIndex(), 2, 5);
				}
			}
			else
			{
				chosen--;
				setSelectable(true);
			}
		}
		if (e.getActionCommand().equals("rpm"))
		{
			if (b3.isSelected())
			{
				chosen++;
				if (chosen == 2)
				{
					setSelectable(false);
					if (b1.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 3);
					if (b2.isSelected())
						gr.render(lap.getSelectedIndex(), 2, 3);
					if (b4.isSelected())
						gr.render(lap.getSelectedIndex(), 3, 4);
					if (b5.isSelected())
						gr.render(lap.getSelectedIndex(), 3, 5);
				}
			}
			else
			{
				chosen--;
				setSelectable(true);
			}
		}
		if (e.getActionCommand().equals("gear"))
		{
			if (b4.isSelected())
			{
				chosen++;
				if (chosen == 2)
				{
					setSelectable(false);
					if (b1.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 4);
					if (b2.isSelected())
						gr.render(lap.getSelectedIndex(), 2, 4);
					if (b3.isSelected())
						gr.render(lap.getSelectedIndex(), 3, 4);
					if (b5.isSelected())
						gr.render(lap.getSelectedIndex(), 4, 5);
				}
			}
			else
			{
				chosen--;
				setSelectable(true);
			}
		}
		if (e.getActionCommand().equals("trackPos"))
		{
			if (b5.isSelected())
			{
				chosen++;
				if (chosen == 2)
				{
					setSelectable(false);
					if (b1.isSelected())
						gr.render(lap.getSelectedIndex(), 1, 5);
					if (b2.isSelected())
						gr.render(lap.getSelectedIndex(), 2, 5);
					if (b3.isSelected())
						gr.render(lap.getSelectedIndex(), 3, 5);
					if (b4.isSelected())
						gr.render(lap.getSelectedIndex(), 4, 5);
				}
			}
			else
			{
				chosen--;
				setSelectable(true);
			}
		}	
	}
	
	/*
	 * A method for activating and blocking unit selection for the graph generation.
	 */
	public void setSelectable(boolean option)
	{
		if (option)
		{
			if (!b1.isSelected())
				b1.setEnabled(true);
			if (!b2.isSelected())
				b2.setEnabled(true);
			if (!b3.isSelected())
				b3.setEnabled(true);
			if (!b4.isSelected())
				b4.setEnabled(true);
			if (!b5.isSelected())
				b5.setEnabled(true);
		}
		else
		{
			if (!b1.isSelected())
				b1.setEnabled(false);
			if (!b2.isSelected())
				b2.setEnabled(false);
			if (!b3.isSelected())
				b3.setEnabled(false);
			if (!b4.isSelected())
				b4.setEnabled(false);
			if (!b5.isSelected())
				b5.setEnabled(false);
		}
	}
	
	/*
	 * Method for setting correct labels of graphs.
	 */
	public void setLabels(int type)
	{
		 for (int i=0; i<5; i++)
		 {
			 JLabel[] distinctLabels = labels.get(i);
			 for (int j=0; j<distinctLabels.length; j++)
				 distinctLabels[j].setVisible(i==type);
		 }
	}
	
	public void setLabels2(int type)
	{
		 for (int i=0; i<5; i++)
		 {
			 JLabel[] distinctLabelsD = labelsD.get(i);
			 for (int j=0; j<distinctLabelsD.length; j++)
				 distinctLabelsD[j].setVisible(i==type);
		 }
	}
}

class Renderer extends JPanel{
	private static final long serialVersionUID = 1L;
	private Graphics2D g2d;
	private ArrayList<DataUnit> nodes;
	private ArrayList<Integer> firstMeters;
	private double trackLength;
	private int from=0;
	private int type=1;
	private int type2=2;
	private Visualizer window;
	
	public Renderer(ArrayList<DataUnit> datas, double length, ArrayList<Integer> firsts, Visualizer frame)
	{
		nodes = datas;
		trackLength = length;
		firstMeters = firsts;
		window = frame;
		setBackground(Color.WHITE);
		repaint();
	}

	/*
	 * A method determining a scope and a type of plotted graphs.
	 */
	public void render(int selectedIndex, int flag, int flag2) {
		from = selectedIndex;
		type = flag;
		type2 = flag2;
		repaint();
	}

	@Override
	public void paintComponent( Graphics g ){
		 super.paintComponent( g );
		 g2d = ( Graphics2D ) g;
		 
		 int endDatas;
		 if ((firstMeters.size() == 1) || (firstMeters.size() == 0))
			 endDatas = nodes.size()-1;
		 else
		 {
			 if (firstMeters.size()==(from+1))
				 endDatas = nodes.size()-1;
			 else
				 endDatas = firstMeters.get(from+1);
		 }
		 
		 //vertical line, distance
		 g2d.setColor(Color.GRAY);
		 for (int i=0; i<(trackLength); i=i+200)
		 {
			 g2d.drawString(Integer.toString(i), i, 485);
			 g2d.drawString(Integer.toString(i), i, 235);
			 g2d.drawLine(i, 20, i, 220);
			 g2d.drawLine(i, 270, i, 470);
		 }		 
		 g2d.drawLine((int)trackLength, 20, (int)trackLength, 220);
		 g2d.drawLine((int)trackLength, 270, (int)trackLength, 470);
	
		 
	/*	 //vertical lines, time
		 int diference=-1;
		 for (int i= firstMeters.get(from); i<endDatas; i++)
		 {
			 if(Math.floor(nodes.get(i).getTime())>diference)
			 {
				 g2d.drawString(Integer.toString((int)nodes.get(i).getTime()), (int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 490);
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 50, (int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 450);
				 diference+=5;
			 }
		 }		 
		 g2d.drawLine((int)trackLength, 50, (int)trackLength, 450);
	*/	 
		 
		 
	//	 Stroke strok = g2d.getStroke();
		 
		 if (type == 1)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 20, (int)trackLength, 20);
			 g2d.drawLine(0, 70, (int)trackLength, 70);
			 g2d.drawLine(0, 120, (int)trackLength, 120);
			 g2d.drawLine(0, 170, (int)trackLength, 170);
			 g2d.drawLine(0, 220, (int)trackLength, 220);	 		
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 		 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(),
						 (int) (nodes.get(i).getAccel()*(-100))+120, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getAccel()*(-100))+120);
			 window.setLabels(0);
		 }
		 if (type == 2)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 20, (int)trackLength, 20);
			 g2d.drawLine(0, 45, (int)trackLength, 45);
			 g2d.drawLine(0, 70, (int)trackLength, 70);
			 g2d.drawLine(0, 95, (int)trackLength, 95);
			 g2d.drawLine(0, 120, (int)trackLength, 120);
			 g2d.drawLine(0, 145, (int)trackLength, 145);
			 g2d.drawLine(0, 170, (int)trackLength, 170);
			 g2d.drawLine(0, 195, (int)trackLength, 195);
			 g2d.drawLine(0, 220, (int)trackLength, 220);
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i).getSpeed()*(-0.5))+170, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getSpeed()*(-0.5))+170);
			 window.setLabels(1);
		 }
		 if (type == 3)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 20, (int)trackLength, 20);
			 g2d.drawLine(0, 40, (int)trackLength, 40);
			 g2d.drawLine(0, 60, (int)trackLength, 60);
			 g2d.drawLine(0, 80, (int)trackLength, 80);
			 g2d.drawLine(0, 100, (int)trackLength, 100);
			 g2d.drawLine(0, 120, (int)trackLength, 120);
			 g2d.drawLine(0, 140, (int)trackLength, 140);
			 g2d.drawLine(0, 160, (int)trackLength, 160);
			 g2d.drawLine(0, 180, (int)trackLength, 180);
			 g2d.drawLine(0, 200, (int)trackLength, 200);
			 g2d.drawLine(0, 220, (int)trackLength, 220);
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i).getRpm()*(-0.02))+220, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getRpm()*(-0.02))+220);
			 window.setLabels(2);
		 }
		 if (type == 4)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 20, (int)trackLength, 20);
			 g2d.drawLine(0, 45, (int)trackLength, 45);
			 g2d.drawLine(0, 70, (int)trackLength, 70);
			 g2d.drawLine(0, 95, (int)trackLength, 95);
			 g2d.drawLine(0, 120, (int)trackLength, 120);
			 g2d.drawLine(0, 145, (int)trackLength, 145);
			 g2d.drawLine(0, 170, (int)trackLength, 170);
			 g2d.drawLine(0, 195, (int)trackLength, 195);
			 g2d.drawLine(0, 220, (int)trackLength, 220);
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i).getGear()*(-25))+195, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getGear()*(-25))+195);
			 window.setLabels(3);
		 }
		 if (type == 5)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 20, (int)trackLength, 20);
			 g2d.drawLine(0, 70, (int)trackLength, 70);
			 g2d.drawLine(0, 120, (int)trackLength, 120);
			 g2d.drawLine(0, 170, (int)trackLength, 170);
			 g2d.drawLine(0, 220, (int)trackLength, 220);	 		
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 		 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(),
						 (int) (nodes.get(i).getTrackPos()*(-100))+120, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getTrackPos()*(-100))+120);
			 window.setLabels(4);
		 }
		 
		 if (type2 == 1)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 270, (int)trackLength, 270);
			 g2d.drawLine(0, 320, (int)trackLength, 320);
			 g2d.drawLine(0, 370, (int)trackLength, 370);
			 g2d.drawLine(0, 420, (int)trackLength, 420);
			 g2d.drawLine(0, 470, (int)trackLength, 470);	 		
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 		 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(),
						 (int) (nodes.get(i).getAccel()*(-100))+370, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getAccel()*(-100))+370);
			 window.setLabels2(0);
		 }
		 if (type2 == 2)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 270, (int)trackLength, 270);
			 g2d.drawLine(0, 295, (int)trackLength, 295);
			 g2d.drawLine(0, 320, (int)trackLength, 320);
			 g2d.drawLine(0, 345, (int)trackLength, 345);
			 g2d.drawLine(0, 370, (int)trackLength, 370);
			 g2d.drawLine(0, 395, (int)trackLength, 395);
			 g2d.drawLine(0, 420, (int)trackLength, 420);
			 g2d.drawLine(0, 445, (int)trackLength, 445);
			 g2d.drawLine(0, 470, (int)trackLength, 470);
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i).getSpeed()*(-0.5))+420, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getSpeed()*(-0.5))+420);
			 window.setLabels2(1);
		 }
		 if (type2 == 3)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 270, (int)trackLength, 270);
			 g2d.drawLine(0, 290, (int)trackLength, 290);
			 g2d.drawLine(0, 310, (int)trackLength, 310);
			 g2d.drawLine(0, 330, (int)trackLength, 330);
			 g2d.drawLine(0, 350, (int)trackLength, 350);
			 g2d.drawLine(0, 370, (int)trackLength, 370);
			 g2d.drawLine(0, 390, (int)trackLength, 390);
			 g2d.drawLine(0, 410, (int)trackLength, 410);
			 g2d.drawLine(0, 430, (int)trackLength, 430);
			 g2d.drawLine(0, 450, (int)trackLength, 450);
			 g2d.drawLine(0, 470, (int)trackLength, 470);
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i).getRpm()*(-0.02))+470, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getRpm()*(-0.02))+470);
			 window.setLabels2(2);
		 }
		 if (type2 == 4)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 270, (int)trackLength, 270);
			 g2d.drawLine(0, 295, (int)trackLength, 295);
			 g2d.drawLine(0, 320, (int)trackLength, 320);
			 g2d.drawLine(0, 345, (int)trackLength, 345);
			 g2d.drawLine(0, 370, (int)trackLength, 370);
			 g2d.drawLine(0, 395, (int)trackLength, 395);
			 g2d.drawLine(0, 420, (int)trackLength, 420);
			 g2d.drawLine(0, 445, (int)trackLength, 445);
			 g2d.drawLine(0, 470, (int)trackLength, 470);
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i).getGear()*(-25))+445, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getGear()*(-25))+445);
			 window.setLabels2(3);
		 }
		 if (type2 == 5)
		 {
			 g2d.setColor(Color.GRAY);
			 g2d.setStroke(new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f, new float[] {2f}, 0f));
			 g2d.drawLine(0, 270, (int)trackLength, 270);
			 g2d.drawLine(0, 320, (int)trackLength, 320);
			 g2d.drawLine(0, 370, (int)trackLength, 370);
			 g2d.drawLine(0, 420, (int)trackLength, 420);
			 g2d.drawLine(0, 470, (int)trackLength, 470);	 		
			 
			 g2d.setColor(Color.BLACK);
			 g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1f));
			 		 
			 for (int i= firstMeters.get(from); i<endDatas; i++)
				 g2d.drawLine((int)nodes.get(i).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(),
						 (int) (nodes.get(i).getTrackPos()*(-100))+370, 
						 (int)nodes.get(i+1).getDistance()-(int)nodes.get(firstMeters.get(from)).getDistance(), 
						 (int) (nodes.get(i+1).getTrackPos()*(-100))+370);
			 window.setLabels2(4);
		 }
	 }
	
	public Dimension getPreferredSize() {
		if (nodes.size() < 2)
			return new Dimension(1,1);
		else
		{
			if ((int) nodes.get(nodes.size()-1).getDistance() < trackLength)
				return new Dimension((int) nodes.get(nodes.size()-1).getDistance(), 0);
			else
				return new Dimension((int) trackLength, 0);
		}
    }  
      
    public Dimension getSize() {  
        return getPreferredSize();  
    }
}
