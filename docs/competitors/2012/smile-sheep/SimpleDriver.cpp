/***************************************************************************

    file                 : SimpleDriver.cpp
    copyright            : (C) 2007 Daniele Loiacono

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "SimpleDriver.h"
#include "fuzzy_rule.h"


/* Gear Changing Constants*/
const int SimpleDriver::gearUp[6]=
    {
        5000,6000,6000,6500,7000,0
    };
const int SimpleDriver::gearDown[6]=
    {
        0,2500,3000,3000,3500,3500
    };

/* Stuck constants*/
const int SimpleDriver::stuckTime = 25;
const float SimpleDriver::stuckAngle = .523598775; //PI/6
#define PI 3.14159265
/* Accel and Brake Constants*/
const float SimpleDriver::maxSpeedDist=70;
const float SimpleDriver::maxSpeed=250;
const float SimpleDriver::sin5 = 0.08716;
const float SimpleDriver::cos5 = 0.99619;

/* Steering constants*/
const float SimpleDriver::steerLock=0.785398;
const float SimpleDriver::steerSensitivityOffset=80.0;
const float SimpleDriver::wheelSensitivityCoeff=1;

/* ABS Filter Constants */
const float SimpleDriver::wheelRadius[4]={0.3179,0.3179,0.3276,0.3276};
const float SimpleDriver::absSlip=2.0;
const float SimpleDriver::absRange=3.0;
const float SimpleDriver::absMinSpeed=3.0;

/* Clutch constants */
const float SimpleDriver::clutchMax=0.5;
const float SimpleDriver::clutchDelta=0.05;
const float SimpleDriver::clutchRange=0.82;
const float SimpleDriver::clutchDeltaTime=0.02;
const float SimpleDriver::clutchDeltaRaced=10;
const float SimpleDriver::clutchDec=0.01;
const float SimpleDriver::clutchMaxModifier=1.3;
const float SimpleDriver::clutchMaxTime=1.5;


int
SimpleDriver::getGear(CarState &cs)
{/*****************
    if (cs.getGear() <= 0) {
		return 1;
	}
	float gr_up = car->_gearRatio[car->_gear + car->_gearOffset];
	float omega = car->_enginerpmRedLine/gr_up;
	float wr = car->_wheelRadius(2);

	if (omega*wr*SHIFT < car->_speed_x) {
		return car->_gear + 1;
	} else {
		float gr_down = car->_gearRatio[car->_gear + car->_gearOffset - 1];
		omega = car->_enginerpmRedLine/gr_down;
		if (car->_gear > 1 && omega*wr*SHIFT > car->_speed_x + SHIFT_MARGIN) {
			return car->_gear - 1;
		}
	}
	return car->_gear;
***********************/

    int gear = cs.getGear();
    int rpm  = cs.getRpm();

    // if gear is 0 (N) or -1 (R) just return 1
    if (gear<1)
        return 1;
    // check if the RPM value of car is greater than the one suggested
    // to shift up the gear from the current one
    if (gear <6 && rpm >= gearUp[gear-1])
        return gear + 1;
    else
    	// check if the RPM value of car is lower than the one suggested
    	// to shift down the gear from the current one
        if (gear > 1 && rpm <= gearDown[gear-1])
            return gear - 1;
        else // otherwhise keep current gear
            return gear;
}
//////normalize the target angle
void NORM_PI_PI(float& targetAngle)
{
     while(targetAngle>=2*PI){
            targetAngle-=2*PI;
            }
}


float sensors[19];

float SimpleDriver::getSteer(CarState &cs)
{
    int j;

     float avg_SensorDist=0.0;
   float max_dist,sec_dist;
		for(int i=5;i<14;i++)
		{
			sensors[i] = cs.getTrack(i);
			if(sensors[i]>100) sensors[i]=100;			//distance all < 100
		}
	float targetAngle;
    max_dist=-1;
	int target_index=0;
    int ii;
	for( int i=5;i<14;i++)
	{
		if(sensors[i]>max_dist){
			target_index=i;
			//sec_dist=sensor[i+1];
			max_dist=sensors[i];
			ii=i;
		}
	}
	//sec_dist=(ii>9)?sensors[ii+1]:sensors[ii-1];

	targetAngle=(target_index-9)*(10.0f)*PI/180.0f;
    //////outside the track
    if(cs.getTrackPos()>1){
        targetAngle=10*PI/180.0f;
        /********************
            if(targetAngle<0){
                    targetAngle=0;
            }else{

            }
            *******/
    }//left
    if(cs.getTrackPos()<-1){
        targetAngle=-10*PI/180.0f;
        /***********************
            if(targetAngle>0){
                    targetAngle=0;
            } else{

            }
            **************/
    }//right
	NORM_PI_PI(targetAngle);

	return -targetAngle/steerLock;
//////////////////////////////////////////////
/***********************
	// steering angle is compute by correcting the actual car angle w.r.t. to track
	// axis [cs.getAngle()] and to adjust car position w.r.t to middle of track [cs.getTrackPos()*0.5]
    float targetAngle=(cs.getAngle()-cs.getTrackPos()*0.5);
    // at high speed reduce the steering command to avoid loosing the control
    if (cs.getSpeedX() > steerSensitivityOffset)
        return targetAngle/(steerLock*(cs.getSpeedX()-steerSensitivityOffset)*wheelSensitivityCoeff);
    else
        return (targetAngle)/steerLock;
******************************/
}

float
SimpleDriver::getAccel(CarState &cs)
{

             float front,max10=0.0,max20=0.0;		//**正前方 + max(左右10,20度Sensors)
             double control[3]={0.0};
             front = sensors[9];
	        max10 = max(sensors[8],sensors[10]);
            max20 = max(sensors[7],sensors[11]);

            fuzzy_rule(front,max10,max20,fabs(cs.getTrackPos()),control);

            float speed_TS =  cs.getSpeedX()- (control[fuzzy_TargetSpeed] * 0.8f) * (cs.getSpeedX()/cs.getWheelSpinVel(1)+cs.getSpeedX()/cs.getWheelSpinVel(3))/2.0f;

             float rxSensor=cs.getTrack(10),steer;
             steer = getSteer(cs);
        // reading of sensor parallel to car axis
            float cSensor=cs.getTrack(9);
        // reading of sensor at -5 degree w.r.t. car axis
            float sxSensor=cs.getTrack(8);

           float targetSpeed;

			if(front >= 99) //&& !(readyToOut(car,outMoment)))		//if賽道是直線，無條件設成300
            speed_TS = cs.getSpeedX() - 300 * (cs.getSpeedX()/cs.getWheelSpinVel(1)+cs.getSpeedX()/cs.getWheelSpinVel(3))/2.0f;

			float accel=1,brake=0;
            accel=1-fabs(cs.getTrackPos());

            //CarControl cc(accel,brake,gear,steer,clutch);

			 float Gas = -1 + 2 / (1+exp(speed_TS));
			 //float Gas = -1 + 2 / (1+cs.getSpeedX()/400);

		 //Gas = 0.1*Gas+0.1*accelCmd+0.7*(sensors[9]/200)+0.1*(2*PI/fabs(steer));
			// Gas=Gas*0.5+accelCmd*0.5;

			 //float angle;
            //Gas=-Gas;

			if( Gas > 0.0f)						//加速
			{
					//TCL機制
					//Gas = Gas - ((car->_wheelSpinVel(REAR_LFT)*car->_wheelRadius(REAR_LFT) + car->_wheelSpinVel(REAR_RGT)*car->_wheelRadius(REAR_RGT)) / 2.0 - car->_speed_x - 1.5f) / 5.0;
					if((sensors[9] < 55.0f || sensors[5]>sensors[9] || sensors[13]>sensors[9]) && fabs(cs.getDistRaced()) > 50 )  //偵測彎道時，降低加速值，起跑時不啟動
						//car->_accelCmd = Gas / 4.0f;
						//(car->_trkPos.toMiddle/((car->_trkPos.seg->width)/2))
					{
					//accelCmd=1-fabs(cs.getTrackPos());
						//car->_accelCmd = Gas / 4.0f;
						//if(car->_trkPos.toLeft - (car->_dimension_x/2) < 0.2 || car->_trkPos.toRight - (car->_dimension_x/2) < 0.2)  //退到邊線前
							//car->_accelCmd=car->_accelCmd*0.8	;

					 if(fabs(steer)>PI/8){
						  accel = accel*0.5;

						}
						else{

							accel = accel*0.8;
						}
					//if((car->_trkPos.toMiddle/((car->_trkPos.seg->width)/2))>0.9)car->_brakeCmd=0.5;
					}
					else
                      accel = Gas;

                    brake=0;
			}
			else								//減速
			{
					//ABS機制
					//Gas = Gas + ((car->_wheelSpinVel(REAR_LFT)*car->_wheelRadius(REAR_LFT) + car->_wheelSpinVel(REAR_RGT)*car->_wheelRadius(REAR_RGT)) / 2.0 - car->_speed_x - 1.5f) / 5.0;
                    brake=fabs(Gas);
                    accel = 0;
			}
			clutching(cs,clutch);
			CarControl cc(accel,brake,cs.getGear(),steer,clutch);
            return accel;

    //return accelCmd;

    // checks if car is out of track
    /********************************
    if (cs.getTrackPos() < 1 && cs.getTrackPos() > -1)
    {
        // reading of sensor at +5 degree w.r.t. car axis
        float rxSensor=cs.getTrack(10);
        // reading of sensor parallel to car axis
        float cSensor=cs.getTrack(9);
        // reading of sensor at -5 degree w.r.t. car axis
        float sxSensor=cs.getTrack(8);

        float targetSpeed;

        // track is straight and enough far from a turn so goes to max speed
        if (cSensor>maxSpeedDist || (cSensor>=rxSensor && cSensor >= sxSensor))
            targetSpeed = maxSpeed;
        else
        {
            // approaching a turn on right
            if(rxSensor>sxSensor)
            {
                // computing approximately the "angle" of turn
                float h = cSensor*sin5;
                float b = rxSensor - cSensor*cos5;
                float sinAngle = b*b/(h*h+b*b);
                // estimate the target speed depending on turn and on how close it is
                targetSpeed = maxSpeed*(cSensor*sinAngle/maxSpeedDist);
            }
            // approaching a turn on left
            else
            {
                // computing approximately the "angle" of turn
                float h = cSensor*sin5;
                float b = sxSensor - cSensor*cos5;
                float sinAngle = b*b/(h*h+b*b);
                // estimate the target speed depending on turn and on how close it is
                targetSpeed = maxSpeed*(cSensor*sinAngle/maxSpeedDist);
            }

        }
         float retValue=2/(1+exp(cs.getSpeedX() - targetSpeed)) - 1;
         if(fabs(steer)>PI/8) retValue/=2;
        // accel/brake command is expontially scaled w.r.t. the difference between target speed and current one
        return retValue;
    }
    else
        return 0.3; // when out of track returns a moderate acceleration command
   *************************************************/
}

CarControl
SimpleDriver::wDrive(CarState cs)
{

    float steer;
    int gear=-1;

	// check if car is currently stuck
	if ( fabs(cs.getAngle()) > PI/2 ||  cs.getSpeedX() < 7.0f)
    {
		// update stuck counter
        stuck++;
    }
    else
    {
    	// if not stuck reset stuck counter
        stuck = 0;
    }

	// after car is stuck for a while apply recovering policy
    if (stuck > stuckTime)
    {

        steer = - cs.getAngle() / steerLock;
		gear = -1;		// Reverse gear.
		clutch = 0.0f;	// Full clutch (gearbox connected with engine).

    	/* set gear and sterring command assuming car is
    	 * pointing in a direction out of track */

    	// to bring car parallel to track axis
        //float steer = - cs.getAngle() / steerLock;
        // gear R

        // if car is pointing in the correct direction revert gear and steer


        if (cs.getAngle()*cs.getTrackPos()>0)
        {
            gear = 1;
            steer = -steer;
        }

        // Calculate clutching
        clutching(cs,clutch);
        // build a CarControl variable and return it
        CarControl cc (1.0,0.0,gear,steer,clutch);
        return cc;
    }

    else // car is not stuck
    {
    	// compute accel/brake command
     //   float accel_and_brake = getAccel(cs);
        // compute gear
        int gear = getGear(cs);


        // normalize steering
        /********
        if (steer < -1)
            steer = -1;
        if (steer > 1)
            steer = 1;
********/
        // set accel and brake from the joint accel/brake command
      //  getAccel();
      //////////////////////////////////////////////////////////////modify accel and brake directly by sheep////////////////////////////////////////////////////

      float front,max10=0.0,max20=0.0;		//**正前方 + max(左右10,20度Sensors)
      double control[3]={0,0,0};
      front = sensors[9];
	  max10 = max(sensors[8],sensors[10]);
      max20 = max(sensors[7],sensors[11]);

      fuzzy_rule(front,max10,max20,fabs(cs.getTrackPos()),control);

      float speed_TS =  cs.getSpeedX()- (control[fuzzy_TargetSpeed] * 0.8f) * (cs.getSpeedX()/cs.getWheelSpinVel(2)+cs.getSpeedX()/cs.getWheelSpinVel(3))/2.0f;
      float rxSensor=cs.getTrack(10);
      steer = getSteer(cs);
        // reading of sensor parallel to car axis
      float cSensor=cs.getTrack(9);
        // reading of sensor at -5 degree w.r.t. car axis
      float sxSensor=cs.getTrack(8);

      float targetSpeed;

	  float accel,brake;
      //accel=1-fabs(cs.getTrackPos())*cs.getSpeedX()/200;

            //CarControl cc(accel,brake,gear,steer,clutch);
     if(steer==0)
     {
        if(sensors[5]>sensors[13])
        {
          steer+=PI/180;
	    }
	    else if(sensors[13]>sensors[5])
	    {
          steer-=PI/180;
	    }
     }

	  double Gas = -1.0f + 2.0f / (1.0f+exp(speed_TS));

	  //if(speed_TS==0.0f) Gas=1.0f;
			// float Gas = -1 + 2 / (1+cs.getSpeedX()/400);
		// Gas = 0.1*Gas+0.1*accelCmd+0.7*(sensors[9]/200)+0.1*(2*PI/fabs(steer));
            					/////////////////////////////緊急煞車機制
        /*****************************************************
					if(sensors[9]<10){
                            accel=0.3;
                            brake= cs.getSpeedX()/100;
					}else if (sensors[9]<20 ){
                            accel=0.3;
                            brake= cs.getSpeedX()/200;
					}else if(sensors[9]<30 ){
                            accel=0.3;
                            brake= cs.getSpeedX()/250;
					}
    by sheep
***********************************************************************/

            if(front >= 99||cs.getSpeedX()<=30)//&& !(readyToOut(car,outMoment)))		//if賽道是直線，無條件設成300
            {
               accel=1;//speed_TS = cs.getSpeedX() - 300 * (cs.getSpeedX()/cs.getWheelSpinVel(2)+cs.getSpeedX()/cs.getWheelSpinVel(3))/2.0f;
               brake=0;
            }
            else if( Gas > 0.0f)						//加速
			{
					//TCL機制
					//Gas = Gas - ((car->_wheelSpinVel(REAR_LFT)*car->_wheelRadius(REAR_LFT) + car->_wheelSpinVel(REAR_RGT)*car->_wheelRadius(REAR_RGT)) / 2.0 - car->_speed_x - 1.5f) / 5.0;
					if((sensors[9] < 55.0f || sensors[5]>sensors[9] || sensors[13]>sensors[9]) && fabs(cs.getDistRaced()) > 50 )  //偵測彎道時，降低加速值，起跑時不啟動
						//car->_accelCmd = Gas / 4.0f;
						//(car->_trkPos.toMiddle/((car->_trkPos.seg->width)/2))
					{
					//accelCmd=1-fabs(cs.getTrackPos());
						//car->_accelCmd = Gas / 4.0f;
						//if(car->_trkPos.toLeft - (car->_dimension_x/2) < 0.2 || car->_trkPos.toRight - (car->_dimension_x/2) < 0.2)  //退到邊線前
							//car->_accelCmd=car->_accelCmd*0.8	;

					 if(fabs(steer)>PI/8){
						  accel = Gas*0.6;
						  if(cs.getSpeedX()>100){
                            accel=Gas*0.56;
						  }
						}
						else{
							accel = Gas*0.9;
							if(cs.getSpeedX()>100){
                            accel=Gas*0.8;
						  }

						}
					//if((car->_trkPos.toMiddle/((car->_trkPos.seg->width)/2))>0.9)car->_brakeCmd=0.5;
					}
					else
                      accel = Gas;

                    brake=0;
			}
			else								//減速
			{
					//ABS機制
					//Gas = Gas + ((car->_wheelSpinVel(REAR_LFT)*car->_wheelRadius(REAR_LFT) + car->_wheelSpinVel(REAR_RGT)*car->_wheelRadius(REAR_RGT)) / 2.0 - car->_speed_x - 1.5f) / 5.0;
                    brake=fabs(Gas);
                    accel = 0;
			}
			clutching(cs,clutch);
			CarControl cc(accel,brake,gear,steer,clutch);
			/////////////////////////////////////////modify accel and brake directly
          /******************************
        //float accel,brake;
        if (accel_and_brake>0)
        {
            accel = accel_and_brake;
            brake = 0;
        }
        else
        {
            accel = 0;
            // apply ABS to brake
            brake = filterABS(cs,-accel_and_brake);
        }
**********************/

        return cc;
    }
}

float
SimpleDriver::filterABS(CarState &cs,float brake)
{
	// convert speed to m/s
	float speed = cs.getSpeedX() / 3.6;
	// when spedd lower than min speed for abs do nothing
    if (speed < absMinSpeed)
        return brake;

    // compute the speed of wheels in m/s
    float slip = 0.0f;
    for (int i = 0; i < 4; i++)
    {
        slip += cs.getWheelSpinVel(i) * wheelRadius[i];
    }
    // slip is the difference between actual speed of car and average speed of wheels
    slip = speed - slip/4.0f;
    // when slip too high applu ABS
    if (slip > absSlip)
    {
        brake = brake - (slip - absSlip)/absRange;
    }

    // check brake is not negative, otherwise set it to zero
    if (brake<0)
    	return 0;
    else
    	return brake;
}

void
SimpleDriver::onShutdown()
{
    cout << "Bye bye!" << endl;
}

void
SimpleDriver::onRestart()
{
    cout << "Restarting the race!" << endl;
}

void
SimpleDriver::clutching(CarState &cs, float &clutch)
{
  double maxClutch = clutchMax;

  // Check if the current situation is the race start
  if (cs.getCurLapTime()<clutchDeltaTime  && stage==RACE && cs.getDistRaced()<clutchDeltaRaced)
    clutch = maxClutch;

  // Adjust the current value of the clutch
  if(clutch > 0)
  {
    double delta = clutchDelta;
    if (cs.getGear() < 2)
	{
      // Apply a stronger clutch output when the gear is one and the race is just started
	  delta /= 2;
      maxClutch *= clutchMaxModifier;
      if (cs.getCurLapTime() < clutchMaxTime)
        clutch = maxClutch;
	}

    // check clutch is not bigger than maximum values
	clutch = min(maxClutch,double(clutch));

	// if clutch is not at max value decrease it quite quickly
	if (clutch!=maxClutch)
	{
	  clutch -= delta;
	  clutch = max(0.0,double(clutch));
	}
	// if clutch is at max value decrease it very slowly
	else
		clutch -= clutchDec;
  }
}

void
SimpleDriver::init(float *angles)
{

	// set angles as {-90,-75,-60,-45,-30,20,15,10,5,0,5,10,15,20,30,45,60,75,90}

	for (int i=0; i<5; i++)
	{
		angles[i]=-90+i*15;
		angles[18-i]=90-i*15;
	}

	for (int i=5; i<9; i++)
	{
			angles[i]=-20+(i-5)*5;
			angles[18-i]=20-(i-5)*5;
	}
	angles[9]=0;
}
