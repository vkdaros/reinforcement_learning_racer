#include <stdlib.h>
#include <math.h>
#include <iostream>

#define Rule_cnt 7
#define Positive_COG  0.613708
#define Negative_COG  0.608523

//For control parameter
#define fuzzy_TargetSpeed	0
#define fuzzy_AccelCmd		1
#define fuzzy_SteerCmd		2

using namespace std;

//****************	Membership functions	******************//

//Target Speed's membership fuctions

double front_low(double front)
{
	double alpha;
	if (front>=0.0 && front <= 20.0 )
		alpha = 1.0;
	else if(front>20.0 && front<=50.0)
		alpha = fabs(front-50.0) / 30.0;
	else 
		alpha = 0.0;

	return alpha;
}

double front_midium(double front)
{
	double alpha;
	if (front>=20.0 && front <= 50.0 )
		alpha = fabs(front - 20.0) / 30.0;
	else if(front>50.0 && front<=80.0)
		alpha = fabs(front - 80.0) / 30.0;
	else 
		alpha = 0.0;

	return alpha;
}

double front_high(double front)
{
	double alpha;
	if (front>=50.0 && front <= 80.0 )
		alpha = fabs(front - 50.0) / 30.0;
	else if(front>80.0)
		alpha = 1.0;
	else 
		alpha = 0.0;

	return alpha;
}

double max10_low(double max10)
{
	double alpha;
	if (max10<10.0 )
		alpha = 1.0;
	else if(max10>=10.0 && max10<=40.0)
		alpha = fabs(max10-40.0) / 30.0;
	else 
		alpha = 0.0;

	return alpha;
}

double max10_midium(double max10)
{
	double alpha;
	if (max10>=10.0 && max10 <= 40.0 )
		alpha = fabs(max10 - 10.0) / 30.0;
	else if(max10>40.0 && max10<=70.0)
		alpha = fabs(max10 - 70.0) / 30.0;
	else 
		alpha = 0.0;

	return alpha;
}

double max10_high(double max10)
{
	double alpha;
	if (max10>=40.0 && max10 <= 70.0 )
		alpha = fabs(max10 - 40.0) / 30.0;
	else if(max10>70.0)
		alpha = 1.0;
	else 
		alpha = 0.0;

	return alpha;
}

double max20_low(double max20)
{
	double alpha;
	if (max20<0 )
		alpha = 1.0;
	else if(max20>=0.0 && max20<=30.0)
		alpha = fabs(max20-30.0) / 30.0;
	else 
		alpha = 0.0;

	return alpha;
}

double max20_midium(double max20)
{
	double alpha;
	if (max20>=0.0 && max20 <= 30.0 )
		alpha = fabs(max20 - 0.0) / 30.0;
	else if(max20>30.0 && max20<=60.0)
		alpha = fabs(max20 - 60.0) / 30.0;
	else 
		alpha = 0.0;

	return alpha;
}

double max20_high(double max20)
{
	double alpha;
	if (max20>=30.0 && max20 <= 60.0 )
		alpha = fabs(max20 - 30.0) / 30.0;
	else if(max20 > 60.0)
		alpha = 1.0;
	else 
		alpha = 0.0;

	return alpha;
}


// Gas Control's membership fuctions
double Gas_brake(int x)
{
	double alpha;
	if (x>=50 &&  x <= 100 )
		alpha = fabs((double)x - 100.0) / 50.0;
	else if(x < 50)
		alpha = 1.0;
	else 
		alpha = 0.0;
	return alpha;
}

double Gas_maintain(int x)
{
		double alpha;
	if (x>=50 &&  x <= 100 )
		alpha = fabs((double)x - 50.0) / 50.0;
	else if(x > 100 && x <= 150)
		alpha = fabs((double)x - 150.0) / 50.0;
	else 
		alpha = 0.0;
	return alpha;
}

double Gas_accel(int x)
{
	double alpha;
	if (x>=100 &&  x <= 150 )
		alpha = fabs((double)x - 100.0) / 50.0;
	else if(x > 150)
		alpha = 1.0;
	else 
		alpha = 0.0;
	return alpha;
}

// Steer Control Fuction:
 //Position
double Pos_right(int x)
{
	double alpha;
	if (x>=50 &&  x <= 100 )
		alpha = fabs((double)x - 100.0) / 50.0;
	else if(x < 50)
		alpha = 1.0;
	else 
		alpha = 0.0;
	return alpha;
}

double Pos_middle(int x)
{
		double alpha;
	if (x>=50 &&  x <= 100 )
		alpha = fabs((double)x - 50.0) / 50.0;
	else if(x > 100 && x <= 150)
		alpha = fabs((double)x - 150.0) / 50.0;
	else 
		alpha = 0.0;
	return alpha;
}

double Pos_left(int x)
{
	double alpha;
	if (x>=100 &&  x <= 150 )
		alpha = fabs((double)x - 100.0) / 50.0;
	else if(x > 150)
		alpha = 1.0;
	else 
		alpha = 0.0;
	return alpha;
}
 //Steer
double Steer_right(int x)
{
	double alpha;
	if (x>=50 &&  x <= 100 )
		alpha = fabs((double)x - 100.0) / 50.0;
	else if(x < 50)
		alpha = 1.0;
	else 
		alpha = 0.0;
	return alpha;
}

double Steer_straight(int x)
{
		double alpha;
	if (x>=50 &&  x <= 100 )
		alpha = fabs((double)x - 50.0) / 50.0;
	else if(x > 100 && x <= 150)
		alpha = fabs((double)x - 150.0) / 50.0;
	else 
		alpha = 0.0;
	return alpha;
}

double Steer_left(int x)
{
	double alpha;
	if (x>=100 &&  x <= 150 )
		alpha = fabs((double)x - 100.0) / 50.0;
	else if(x > 150)
		alpha = 1.0;
	else 
		alpha = 0.0;
	return alpha;
}

// Defuzzification  (import each rules' alpha value)
double eval_COG(double *alpha)
{
	double Accel_threshold=0.0,
		   Maintain_threshold=0.0,
		   Brake_threshold=0.0;

	double COG[201]={0.0};

	for(int i=1;i<=7;i++)
	{
		if(i<=3)
			Accel_threshold = max(Accel_threshold,alpha[i]);
		else if(i==4 || i== 5)
			Maintain_threshold = max(Maintain_threshold,alpha[i]);
		else
			Brake_threshold = max(Brake_threshold,alpha[i]);
	}
	for(int i=1;i<=200;i++)
	{
		if(i<=100)  // 1~100 區域面積在 Brake 和 Maintain 中 取 MAX 
			COG[i] = max ( min( Brake_threshold , Gas_brake(i) ) , min( Maintain_threshold , Gas_maintain(i)) );
		else		// 101~200 區域面積在 Accel 和 Maintain 中 取 MAX 
			COG[i] = max ( min( Maintain_threshold , Gas_maintain(i) ) , min( Accel_threshold , Gas_accel(i) ) );
	}

	// sigma(x*y) / sigma(y) = crisp value
	double up=0.0, down=0.0 , value = 0.0;
	for(int k=1;k<=200;k++)
	{
		up += (double)(k - 100) * COG[k];         // Gas[-100,100] -> Gas[-1,1]
		down += COG[k];
	}
	value = (double) (up / down / 100);
	return value;
}

//Defuzzification for steerCmd   (import each rules' alpha value)

double eval_COG2(double *alpha)
{
	double left_threshold=0.0,
		   middle_threshold=0.0,
		   right_threshold=0.0;

	double COG[201]={0.0};

	for(int i=8;i<=10;i++)
	{
		if(i==8)
			left_threshold = alpha[8];
		else if(i==9)
			middle_threshold = alpha [9];
		else
			right_threshold = alpha [10];
	}
	for(int i=1;i<=200;i++)
	{
		if(i<=100)  // 1~100 區域面積在 right 和 striaght 中 取 MAX 
			COG[i] = max ( min( right_threshold , Steer_right(i) ) , min( middle_threshold , Steer_straight(i) ) );
		else		// 101~200 區域面積在 left 和 striaght 中 取 MAX
			COG[i] = max ( min( middle_threshold , Steer_straight(i) ) , min( left_threshold , Steer_left(i) ) );
	}

	// sigma(x*y) / sigma(y) = crisp value
	double up=0.0, down=0.0 , value = 0.0;
	for(int k=1;k<=200;k++)
	{
		up += (double)(k - 100) * COG[k];         // SteerCmd[-100,100] -> SteerCmd[-1,1]
		down += COG[k];
	}
	value = (double) (up / down / 100);
	return value;
}



//**************************  Fuzzy Rule bases  ************************//

double fuzzy_rule(double front,double max10,double max20,double position,double Control[])
{
	double TS[8]={300.0,
				  200.0,
				  175.0,
				  150.0,
				  125.0,
				  100.0,
				   75.0,
				   50.0};
	double alpha[11];
	double Gas_value;

	//Rule 1:
	alpha[1] = front_high(front);
	//Rule 2:
	alpha[2] = front_midium(front);
	//Rule 3:
	alpha[3] = min (front_low(front),max10_high(max10));
	//Rule 4:
	alpha[4] = min (front_low(front),max10_midium(max10));
	//Rule 5:
	alpha[5] = min (min (front_low(front),max10_low(max10)),max20_high(max20));
	//Rule 6:
	alpha[6] = min (min (front_low(front),max10_low(max10)),max20_midium(max20));
	//Rule 7:
	alpha[7] = min (min (front_low(front),max10_low(max10)),max20_low(max20));
	//Rule 8:
    alpha[8] = Pos_right(int(position*100 + 0.5 + 100.0)) ;
	//Rule 9:
	alpha[9] = Pos_middle(int(position*100 + 0.5 + 100.0)) ;
	//Rule 10:
	alpha[10] = Pos_left(int(position*100 + 0.5 + 100.0)) ;

	//Use COG to get a crisp value: (singleton)
	double outputTS=0.0,a=0.0,b=0.0;
	for(int i = 1;i<=Rule_cnt;i++)
	{
			a+=alpha[i]*TS[i];
			b+=alpha[i];
	}
	outputTS = (double) (a / b);
	Gas_value = eval_COG(alpha);
	Control[fuzzy_TargetSpeed] = outputTS;
	// 針對AccelCmd值進行 Scale -> [-1,1]
	if(Gas_value > 0.0)
	{
		Control[fuzzy_AccelCmd] =  Gas_value / Positive_COG;
	}
	else
	{
		Control[fuzzy_AccelCmd] =  Gas_value / Negative_COG;
	}

	// 針對SteerCmd值進行 Scale -> [-1,1]
	Control[fuzzy_SteerCmd] = eval_COG2(alpha);
	if(Control[fuzzy_SteerCmd] > 0.0)
	{
		Control[fuzzy_SteerCmd] =  Control[fuzzy_SteerCmd] / Positive_COG;
	}
	else
	{
		Control[fuzzy_SteerCmd] =  Control[fuzzy_SteerCmd] / Negative_COG;
	}

	return outputTS;
}