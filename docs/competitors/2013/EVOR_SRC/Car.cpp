/*
 * Car.cpp
 *
 *  Created on: 26/05/2013
 *      Author: sam
 */

#include "Car.h"

#include <assert.h>

Car::Car():
rotation(0.0)
{
}

Car::Car(const glm::vec2& position, double rotation):
			position(position), rotation(rotation)
{
}

Car::~Car()
{
}

const glm::vec2& Car::getPosition() const
{
	return position;
}

void Car::setPosition(const glm::vec2& position)
{
	this->position = position;
}

double Car::getRotation() const
{
	return rotation;
}

void Car::setRotation(double rotation)
{
	this->rotation = rotation;
}


double	Car::getSensorAngle(int sensor) const
{
	assert((sensor >= 0) && (sensor < TRACK_SENSOR_COUNT));
	return sensorAngles[sensor];
}

void Car::setSensorAngle(int sensor, double angle)
{
	assert((sensor >= 0) && (sensor < TRACK_SENSOR_COUNT));
	this->sensorAngles[sensor] = angle;
}

double Car::getDistanceFromStart() const
{
	return this->distanceFromStart;
}

void Car::setTrackPosition(double trackPosition)
{
	this->distanceFromStart = trackPosition;
}

void Car::update(CarState& cs, Track& track)
{
	float distance = cs.getDistFromStart();
	int segId = distance;
	float d1 = distance - segId;
	TrackSegment segment = track.getSegment(segId);

	float segmentRotation = segment.getRotation();
	float carAngleFromTrackAxis = -cs.getAngle();
	rotation = segmentRotation+carAngleFromTrackAxis;


	glm::vec2 segmentPosition = segment.getMiddle();
	float d2 = cs.getTrackPos()*(segment.getTrackWidth()/2);
	double sinAlpha = sin(rotation);
	double cosAlpha = cos(rotation);

//	float m = tan(segmentRotation);
//	glm::vec2 unitVectorLong(1,m);
//	float m2;
//	glm::vec2 unitVectorLat;
//	if(m != 0)
//	{
//		m2 = -1/m;
//		unitVectorLat = glm::vec2(1,-1/m);
//	}
//	else
//		unitVectorLat= glm::vec2(0,1);

	position = glm::vec2(segmentPosition.x+d1*cosAlpha-d2*sinAlpha, segmentPosition.y + d1*sinAlpha+d2*cosAlpha);

	distanceFromStart = cs.getDistFromStart();
}


std::ostream& operator<<(std::ostream& out, const Car& car)
{
	const glm::vec2& pos = car.getPosition();
	double rot = car.getRotation();

	out << pos.x << ", " << pos.y << ", " << rot;

	return out;
}



