/*
 * Car.h
 *
 *  Created on: 26/05/2013
 *      Author: sam
 */

#ifndef CAR_H_
#define CAR_H_

#include <glm/glm.hpp>

#include <iostream>

#include "CarState.h"
#include "Track.h"

#define	TRACK_SENSOR_COUNT		19

class Car
{
public:
	Car();
	Car(const glm::vec2& position, double rotation);
	virtual ~Car();

	const glm::vec2& 	getPosition() const;
	void 				setPosition(const glm::vec2& position);

	double 				getRotation() const;
	void 				setRotation(double rotation);

	double				getSensorAngle(int sensor) const;
	void				setSensorAngle(int sensor, double angle);

	double				getDistanceFromStart() const;
	void				setTrackPosition(double trackPosition);

	void				update(CarState& cs, Track& track);

private:
	double		sensorAngles[TRACK_SENSOR_COUNT];

	glm::vec2	position;
	double		rotation;
	double 		distanceFromStart;
};

std::ostream& operator<<(std::ostream& out, const Car& car);

#endif /* CAR_H_ */
