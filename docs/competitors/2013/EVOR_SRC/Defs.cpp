/*
 * Defs.cpp
 *
 *  Created on: 09/06/2013
 *      Author: sam
 */
#include <cmath>

#include "Defs.h"

double normalizeAngle(double angle)
{
	if(angle >= 0)
		return fmod(angle, 2 * PI);
	else
		return (2 * PI) + fmod(angle, 2 * PI);
}

double	getAngleDiff(double first, double second)
{
	double normFirst = normalizeAngle(first);
	double normSecond = normalizeAngle(second);

	double diff = (normFirst - normSecond);
	if(diff > PI)
		diff = -((2 * PI) - diff);
	else if(diff < -PI)
		diff = ((2 * PI) + diff);

	return diff;
}



