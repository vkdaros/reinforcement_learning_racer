/*
 * Defs.h
 *
 *  Created on: 28/04/2013
 *      Author: sam
 */

#ifndef DEFS_H_
#define DEFS_H_

#define PI 						3.14159265

#define TO_RADIANS(DEGREES) 	(PI/180*(DEGREES))
#define TO_DEGREES(RADIANS)		(180/PI*(RADIANS))



#define UPDATE_RATIO 0.9
#define MAX_SLIP TO_RADIANS(3)
#define TO_RADIANS(DEGREES) (PI/180*(DEGREES))
#define TO_DEGREES(RADIANS) (180/PI*(RADIANS))
#define STEER_TO_RADIANS(STEER) (0.366519*(STEER))
#define STEER_TO_DEGREES(STEER) ((0.366519*(STEER))*(180/PI))
#define TO_MS_1(KM_H) ((KM_H)*1000.0/3600.0)

#define MAX_ACCEL_DIST 190
#define MIN_ACCEL_DIST 20
#define MIN_PADDING_DIST 50
#define MIN_FITNESS -10000000

#define DEFAULT_FRICTION 20
#define DEFAULT_MAX_BRAKE -30

#define TICK_RECOVERY   10
#define TICK_REVERSE    45
#define TICK_POST_REVERSE 144
#define TICK_FULL_RECOVERY 65
#define TICK_POST_RECOVERY 200



double					normalizeAngle(double angle);
double					getAngleDiff(double first, double second);



#endif /* DEFS_H_ */
