/*
 * EAThread.cpp
 *
 *  Created on: 11/06/2013
 *      Author: sam
 */

#include "EAThread.h"

#include <unistd.h>
#include <iostream>

EAThread::EAThread(EvoDriver* driver):
	stopped(false), resetted(false), carState(NULL), driver(driver)
{
	pthread_mutex_init(&this->resetMutex, NULL);
}

EAThread::~EAThread()
{
	pthread_mutex_destroy(&this->resetMutex);
}

CarState* EAThread::getCarState() const
{
	return this->sharedCarState.get();
}

void EAThread::setCarState(CarState* cs)
{
	this->sharedCarState.set(cs);
}

CarControl*	EAThread::getCarControl() const
{
	return this->sharedCarControl.get();
}

void EAThread::setCarControl(CarControl* cc)
{
	this->sharedCarControl.set(cc);
}

bool EAThread::isReset() const
{
	bool ret = false;

	pthread_mutex_lock(&this->resetMutex);
		ret = this->resetted;
		this->resetted = false;
	pthread_mutex_unlock(&this->resetMutex);

	return ret;
}

void EAThread::reset()
{
	pthread_mutex_lock(&this->resetMutex);
		this->resetted = true;
	pthread_mutex_unlock(&this->resetMutex);
}



void EAThread::run()
{
//	time_t lastSecond = time(NULL);
	int	stepCount = 0;

	bool updateCalled = false;

	this->stopped = false;
	while(!this->stopped)
	{
		//
		// Check reset
		//
		bool resetted = this->isReset();
		if(resetted)
			this->driver->reset();


		//
		// Check new state
		//
		CarState* newCarState = getCarState();
		if((newCarState != NULL) && (this->carState != newCarState))
		{
			this->driver->update(*newCarState);
			updateCalled = true;

			delete this->carState;
			this->carState = newCarState;
		}

		if(updateCalled)
		{
			CarControl cc = this->driver->step();

			bool deleted = this->sharedCarControl.set(new CarControl(cc));
			if(!deleted)
				this->driver->postUpdate();		// Proxy has read the control, so we post update

			//
			// TODO: remove print steps per second
			//
//			time_t now = time(NULL);
//			if(now != lastSecond)
//			{
//				std::cout << stepCount << " steps per second" << std::endl;
//				stepCount = 0;
//				lastSecond = now;
//			}

			++stepCount;
		}
		else
		{
			usleep(10 * 1000);
		}
	}
}

void EAThread::stop()
{
	this->stopped = true;
}
