/*
 * EAThread.h
 *
 *  Created on: 11/06/2013
 *      Author: sam
 */

#ifndef EATHREAD_H_
#define EATHREAD_H_

#include "Thread.h"
#include "SharedPointer.h"
#include "EvoDriver.h"
#include "CarState.h"
#include "CarControl.h"

class EAThread: public Thread
{
public:
	EAThread(EvoDriver* driver);
	virtual ~EAThread();

	CarState*	getCarState() const;
	void		setCarState(CarState* cs);

	CarControl*	getCarControl() const;
	void		setCarControl(CarControl* cc);

	bool		isReset() const;
	void		reset();

	virtual void run();
	virtual void stop();

private:
	volatile bool						stopped;

	volatile mutable bool				resetted;
	mutable pthread_mutex_t				resetMutex;

	mutable SharedPointer<CarState>		sharedCarState;
	mutable SharedPointer<CarControl>	sharedCarControl;

	CarState*							carState;

	EvoDriver*							driver;
};

#endif /* EATHREAD_H_ */
