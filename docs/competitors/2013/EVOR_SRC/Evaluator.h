/*
 * Evaluator.h
 *
 *  Created on: 15/06/2013
 *      Author: sam
 */

#ifndef EVALUATOR_H_
#define EVALUATOR_H_

#include "Individual.h"

class EvoDriver;
class Evaluator {
public:
	Evaluator(EvoDriver* driver):driver(driver){};
	virtual ~Evaluator(){};

	virtual void	init() {};
	virtual void	upadate(Individual& controller, const CarState& prevState, const CarState& newSatate) {};
	virtual double 	evaluateFitness(Individual& indi, const CarState& state) = 0;

protected:
	EvoDriver* driver;
};

#endif /* EVALUATOR_H_ */
