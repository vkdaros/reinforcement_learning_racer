/*
 * EvoDriver.cpp
 *
 *  Created on: 13/04/2013
 *      Author: sam
 */

#include "EvoDriver.h"
#include "SimpleDriver.h"
#include "LinearEvaluator.h"
#include "SVG.h"


#include <fstream>
#include <deque>
#include <cmath>

#include <iterator>
#include <iostream>
#include <ctime>
#include <sys/time.h>
#include <iomanip>
#include <unistd.h>

#define WARMUP_TRACK_BUILD_LAP_COUNT	3
#define MAX_TRACK_BUILD_TIME			(20 * 60)


EvoDriver::EvoDriver():
fullTrack(&car), lap(0), mode(SIMPLE_MODE),
accelaration(201,8), debugCount(0)
{



	fitnessFunction = new LinearEvaluator(this);
}

EvoDriver::~EvoDriver()
{
	delete fitnessFunction;
}


string EvoDriver::drive(CarState& cs)
{
	update(cs);

	//run (1+1) EA
	int generationCount = 1000;

	//	double parentFitness = parent.getFitnessVal();
	//	int debugBestGen;

	int i = 0;
	debugGenCount = 0;
	//	debugBestGen = 0;

	while(i < generationCount)
	{
		step();

		//#ifdef DEBUGFILES
		//std::cout <<" iteration " << debugCount <<"generation " << i << "  fitness " << parentFitness << " and control" << parent.getControl().toString().c_str() << std::endl;
		//#endif
		++i;
		++debugGenCount;
	}

	//update member variables
	CarControl cc = prevParent.getControl();
	postUpdate();

	return cc.toString();
}

void EvoDriver::onShutdown()
{
}

void EvoDriver::onRestart()
{
}



glm::vec2 EvoDriver::getIntersectedPoint(const Line& line,
		const glm::vec2& firsPointt, const glm::vec2& secondPoint)
{
	if(firsPointt.x == secondPoint.x)
	{
#ifdef DEBUGFILES
		std::cout << " same x position " << std::endl;
#endif
	}

	/*static std::ofstream debugPrint2("output/debugFile.log");
	debugPrint2 << "  point 1 is x=" << firsPointt.x << ", y=" << firsPointt.y <<",  point 2 x=" << secondPoint.x << ", y=" << secondPoint.y << std::endl;
	 */



	float y1=0; float x1=0;
	if(firsPointt.x == secondPoint.x)
	{
		float lineM = line.getM();
		if(lineM == INFINITY || lineM == -INFINITY)
			return glm::vec2(x1,y1);
		float lineC = line.getC();
		x1 = firsPointt.x;
		y1 = lineM*x1 + lineC;
		return glm::vec2(x1,y1);
	}


	float m = (firsPointt.y - secondPoint.y) / (firsPointt.x - secondPoint.x);
	float c =  firsPointt.y - m*firsPointt.x;
	float lineM = line.getM();
	//	float lineC = line.getC();
	if(lineM == m)
		return glm::vec2(x1,y1);
	//intersection

	//m*x + c = line.getM()*x+line.getC();
	if(m == line.getM())
	{
		std::cout << " m is equal " << std::endl;
	}
	x1 = (line.getC() - c ) /(m-line.getM()) ;
	y1 = m*x1 + c;

	return glm::vec2(x1,y1);
}


void EvoDriver::updateAccelaration(const CarState& currentState)
{
	//and update the error
	std::pair<int, int>pair = getAccelarationTableIndexes(previousTickDesicion.getGear(),previousTickDesicion.getAccel());
	float lastTableVal = accelaration.getData(pair.first,pair.second);


	//V = u +at

	float actualAcc = (currentVelocity - previousVelocity)/tick;
	float newAcc = calculateLowPassFilter(UPDATE_RATIO,lastTableVal,actualAcc);
	accelaration.setData(pair.first,pair.second,newAcc);
#ifdef DEBUGFILES
	std::cout << "updateAcc previous acc " << lastTableVal << " addpedal " << previousTickDesicion.getAccel() << " gear " << previousTickDesicion.getGear() << " and acutal acceration " << actualAcc << " and updated acc " << newAcc << " for pedal " << pair.first << " and gear " << pair.second;
#endif
}


void EvoDriver::updateCentrepretalForce(CarState& currentState)
{
	//check what happened in the last game tick acutally Vs what predicted last game tick
	//and update the error


	double lastforce = previousTickDesicion.getCentripritalForce();


	double actualForce = calculateActualForce();
	double actualScaler = actualForce/lastforce;
	if(actualScaler >0 )
	{
		forceSlipScaler = calculateLowPassFilter(UPDATE_RATIO,forceSlipScaler,actualScaler);
		//std::cout << " actual force " << actualForce << " last force " << lastforce << " actual scaler " << actualScaler << "  slip scaler updated " << forceSlipScaler << " current x " << currentCarPosition.x << " current y " << currentCarPosition.y << " prev x " << carPositions[1].x << " prev y " << carPositions[1].y << " oldprev x " << carPositions[0].x << " old prev y " << carPositions[0].y<< std::endl;
		//std::cout << " centriprital force updated to " << newValue << " from " << lastTableVal << " for slip angle " << slipAngle << " and steeringangle " << steerAngle << std::endl;
	}
}



void EvoDriver::init(float* angles)
{




	strcpy(this->simpleDriver.trackName, this->trackName);
	this->simpleDriver.stage = this->stage;


	srand(time(NULL));//seeding the random number generator


	float anglesArr[] = {-90,-75,-60,-45,-30,-20,-15,-10,-5,0,5,10,15,20,30,45,60,75,90};	// simple driver sensor angles
	std::copy(anglesArr,anglesArr+19,angles);
	for(int i = 0; i < 19; i++)
	{
		float rad =TO_RADIANS(anglesArr[i]);
		car.setSensorAngle(i, -rad);
		trackSensorAngles[i] = -rad;
	}

	//	int rows = accelaration.getRows();
	int cols = accelaration.getCols();
	for(double i = 0; i < cols; ++i)
	{
		for(int j = 0; j < 100;  ++j)
			accelaration.setData(j,i, -j/10.0);


		accelaration.setData(100,i,0);

	}

	fillAccForGear(-1, -3.0, -8.0, -8.0,0.8,1.0);
	fillAccForGear(1, 3.0, 8.0, 8.0, 0.6, 1.0);
	fillAccForGear(2, 2.5, 6.0, 6.0, 0.75, 1.0);
	fillAccForGear(3, 2.0, 5.0, 5.0, 0.8, 1.0);
	fillAccForGear(4, 1.5, 4.0, 4.0, 0.825, 1.0);
	fillAccForGear(5, 1.0, 3.0, 3.0, 0.85, 1.0);
	fillAccForGear(6, 0.5, 1.0, 1.0, 0.9, 1.0);


	//	fillAccForGear(-1, -3.0, -8.0, -7.0,0.8,1.0);
	//	fillAccForGear(1, 3.0, 8.0, 5.0, 0.2, 0.5);
	//	fillAccForGear(2, 2.5, 6.0, 4.0, 0.3, 0.6);
	//	fillAccForGear(3, 2.0, 5.0, 3.0, 0.4, 0.75);
	//	fillAccForGear(4, 1.5, 4.0, 2.0, 0.5, 0.8);
	//	fillAccForGear(5, 1.0, 2.9, 0.75, 0.6, 0.9);
	//	fillAccForGear(6, 0.5, 1.0, 0.5, 0.7, 1.0);


	//	rows = centrepretalForce.getRows();
	//	cols = centrepretalForce.getCols();
	//	for(double i = 0; i < rows; ++i)
	//	{
	//		for(int j = 0; j < cols;  ++j)
	//		{
	//			centrepretalForce.setData(i,j,0.01);
	//		}
	//
	//	}

	//todo:update F
	static bool firstInit = true;
	if(firstInit)
	{
		fitnessFunction->init();

		fullTrack.init(this->trackName);
		firstInit = false;
	}

	initTime = time(NULL);

	reInit();

}

std::pair<int, int> EvoDriver::getCentrepretalForceIndexes(double alpha, double delta)
{
	int deltaDegrees = TO_DEGREES(delta);//todo:check the units of angles-input radians?
	int alphaDegrees =TO_DEGREES(alpha);
	return std::make_pair(alphaDegrees+180,deltaDegrees+22);

}



double EvoDriver::calculateSlideslipAngle(double Vx, double Vy)
{
	return atan2(Vy,Vx);

	//	glm::vec2 v(Vx, Vy);
	//	glm::vec2 car(1,0);
	//	return acos(glm::dot(v,car)/(v.length()*car.length()));
}


double EvoDriver::getRadius(double force, double velocity)
{
	//F/M = V2/R
	double r = INT_MAX*1.0;
	if(velocity != 0)
		r = (velocity*velocity)/force;
	return r;


}

double EvoDriver::getAccelaration(int gear, double accPedal)
{


	std::pair<int,int>  pair = getAccelarationTableIndexes(gear,accPedal);
	return accelaration.getData(pair.first,pair.second);

}


void EvoDriver::calculateVeclocityLongitudal(const CarState& state,
		Individual controller)
{
}

void EvoDriver::fitnessEvaluate(Individual& controller,  CarState& state)
{

	double fitness = fitnessFunction->evaluateFitness(controller,state);
	controller.setFitnessVal(fitness);


}

double EvoDriver::calculateActualForce()
{
	//calculate the circle for three points, old-old, old and current and derive the force by the R, prveous V
	if(carPositions[0] != glm::vec2(0,0))
	{
		double R = getCircleRadius(currentCarPosition, carPositions[0], carPositions[1]);
		return previousVelocity*previousVelocity/R;
	}
	return 0.0;
}

double EvoDriver::calculateLowPassFilter(double a, double old, double current)
{
	return old * a + current *  (1-a);

}

std::pair<int, int> EvoDriver::getAccelarationTableIndexes(int gear, double accPedal)
{
	int gearTable = gear+1;
	int accTable = accPedal*100+100;
	return std::make_pair(accTable,gearTable);
}



std::pair<glm::vec2, double> EvoDriver::isIntersetSegment(glm::vec2& c1, glm::vec2& c2,
		glm::vec2& center, double r, glm::vec2& middle)
{

	int flag = -1;
	std::pair<glm::vec2,glm::vec2> intersectPoints = getIntersectedPoint(center,r,c1,c2,flag);
	glm::vec2 p1 = intersectPoints.first;
	glm::vec2 p2 = intersectPoints.second;

	if(flag == 2)//both points intersect
	{
		double len1 = glm::length(middle - p1);
		double len2 = glm::length(middle - p2);
		if(len1 < len2)
		{
			return std::make_pair(p1,len1);
		}
		else
		{
			return std::make_pair(p2,len2);
		}
	}
	else if(flag == 1)
	{
		double len1 = glm::length(middle - p1);
		return std::make_pair(p1,len1);
	}
	else
	{
		return std::make_pair(p1,-1);
	}


	//	double m;
	//	double c;
	//	double A ;
	//	double B ;
	//	double C ;
	//	if(c2.x != c1.x)
	//	{
	//		m = (c2.y - c1.y)/(c2.x - c1.x);
	//		c = c1.y - m * c1.x;
	//		A = m*m +1;
	//		B = 2*(m*c - m*center.y - center.x);
	//		C = center.y*center.y - r*r + center.x*center.x - (2*c*center.y) + c*c;
	//	}
	//	else
	//	{
	//		double d = c1.x;
	//		A = 1;
	//		B = -2*center.y;
	//		C = d*d -2*d*center.x + center.x*center.x +center.y*center.y -r*r;
	//	}
	//	double intersect = B*B - 4*A*C;
	//	if(intersect > 0)
	//	{
	//		double sqrtVal = sqrt(intersect);
	//		double x1 = (-B + sqrtVal)/(2*A);
	//		double y1;
	//		if((x1 > c1.x && x1 < c2.x)||(x1 < c1.x && x1 > c2.x))
	//		{
	//			y1 = m *( x1) +c;
	//			if((y1 > c1.y && y1 < c2.y )||(y1 < c1.y && y1 > c2.y))
	//				return std::make_pair(glm::vec2(x1, y1), glm::length(glm::vec2(x1,y1)-middle));
	//		}
	//		else
	//		{
	//			x1 = (-B - sqrtVal)/(2*A);
	//			if((x1 > c1.x && x1 < c2.x)||(x1 < c1.x && x1 > c2.x))
	//			{
	//				y1 = m *(x1) + c;
	//				if((y1 > c1.y && y1 < c2.y )||(y1 < c1.y && y1 > c2.y))
	//					return std::make_pair(glm::vec2(x1, y1), glm::length(glm::vec2(x1,y1)-middle));
	//			}
	//
	//		}
	//		return std::make_pair(glm::vec2(x1,y1), -glm::length(glm::vec2(x1,y1)-middle));
	//	}
	//
	//	return std::make_pair(glm::vec2(), -100.0);



}

double EvoDriver::getCircleRadius(glm::vec2& c1, glm::vec2& c2, glm::vec2& c3)
{
	double p,q,r;
	//(c1.x - p)*(c1.x - p) + (c1.y - q)*(c1.y -q) = r*r;
	//c1.x*c1.x - 2*p*c1.x + p*p + c1.y*c1.y - 2*q*c1.y + q*q = r*r;
	//  c2.x*c2.x - 2*p*c2.x + p*p + c2.y*c2.y - 2*q*c2.y + q*q = r*r;

	//q  = (c1.x*c1.x - c2.x*c2.x -2*p*(c1.x + c2.x) + c1.y*c1.y - c2.y*c2.y)/2*(c1.y - c2.y);
	//q = (c1.x*c1.x - c3.x*c3.x -2*p*(c1.x + c3.x) + c1.y*c1.y - c3.y*c3.y)/2*(c1.y - c3.y);
	//(c1.x*c1.x - c2.x*c2.x -2*p*(c1.x + c2.x) + c1.y*c1.y - c2.y*c2.y) = 2*(c1.y - c2.y) *( (c1.x*c1.x - c3.x*c3.x -2*p*(c1.x + c3.x) + c1.y*c1.y - c3.y*c3.y)/2*(c1.y - c3.y));
	//-2*p*(c1.x + c2.x) = -c1.x*c1.x +c2.x*c2.x  - c1.y*c1.y + c2.y*c2.y + 2*(c1.y - c2.y) *( (c1.x*c1.x - c3.x*c3.x  + c1.y*c1.y - c3.y*c3.y)/2*(c1.y - c3.y));
	//double a = -c1.x*c1.x +c2.x*c2.x  - c1.y*c1.y + c2.y*c2.y + 2*(c1.y - c2.y) *( (c1.x*c1.x - c3.x*c3.x  + c1.y*c1.y - c3.y*c3.y)/2*(c1.y - c3.y));
	//	-2*p*(c1.x + c2.x) + (2*p*(c1.x + c3.x)*2*(c1.y - c2.y)/2*(c1.y - c3.y)) = a;
	//p = a/( 2*(-c1.x - c2.x +  (c1.x + c3.x)*2*(c1.y - c2.y)/2*(c1.y - c3.y)) );
	//-2*p*(c1.x + c3.x) + 2*p*(c1.x + c2.x)*(c1.y - c3.y)/(c1.y - c2.y) = -c1.x*c1.x + c3.x*c3.x c3.y*c3.y - c1.y*c1.y+((c1.y - c3.y)*(c1.x*c1.x - c2.x*c2.x  + c1.y*c1.y - c2.y*c2.y)/(c1.y - c2.y));
	//(-2*p(c1.x*c1.y - c1.x*c2.y + c3.x*c1.y - c3.x*c2.y) +  2*p*(c1.x + c2.x)*(c1.y - c3.y))/(c1.y - c2.y) = -c1.x*c1.x + c3.x*c3.x c3.y*c3.y - c1.y*c1.y+((c1.y - c3.y)*(c1.x*c1.x - c2.x*c2.x  + c1.y*c1.y - c2.y*c2.y)/(c1.y - c2.y));
	//2*p*(-c1.x*c1.y + c1.x*c2.y - c3.x*c1.y + c3.x*c2.y  +(c1.x + c2.x)*(c1.y - c3.y) )/(c1.y - c2.y) = -c1.x*c1.x + c3.x*c3.x c3.y*c3.y - c1.y*c1.y+((c1.y - c3.y)*(c1.x*c1.x - c2.x*c2.x  + c1.y*c1.y - c2.y*c2.y)/(c1.y - c2.y));
	//p =  a /(2*(-c1.x*c1.y + c1.x*c2.y - c3.x*c1.y + c3.x*c2.y  +(c1.x + c2.x)*(c1.y - c3.y) ))* (c1.y - c2.y) ;
	//q  = (c1.x*c1.x - c2.x*c2.x -2*p*(c1.x + c2.x) + c1.y*c1.y - c2.y*c2.y)/2*(c1.y - c2.y);
	double a = (c1.x - c3.x) * (c1.x*c1.x + c1.y*c1.y - c2.x*c2.x - c2.y*c2.y);
	double b = (c1.x - c2.x) * (c1.x*c1.x + c1.y*c1.y -c3.x*c3.x  -c3.y*c3.y);
	double c = (c1.y - c2.y) * (c1.x - c3.x);
	double d = (c1.y - c3.y) * (c1.x - c2.x);
	q = (a - b)/ (2* (c - d));
	p = ( c1.x*c1.x + c1.y*c1.y -2*q*(c1.y - c2.y) -c2.x*c2.x -c2.y*c2.y) / (2*(c1.x - c2.x));
	r= sqrt((c1.x - p)*(c1.x - p) + (c1.y - q)*(c1.y -q));
	return r;
}

glm::vec2 EvoDriver::getTrajectoryCenter(glm::vec2& carPos,double raduis,float direction)
{

	double angleOfCenter;
	if(direction > 0)
		angleOfCenter = angleOfCompoundVel+PI/2;
	else
		angleOfCenter = angleOfCompoundVel-PI/2;

	double m = tan(angleOfCenter);
	glm::vec2 unitVec = glm::normalize(glm::vec2(1,m));

	//
	//	double m = tan(angleOfCompoundVel);
	//	//double m = tan
	//	glm::vec2 unitVec;
	//	if(m != 0)
	//	{
	//		m = -1/m;
	//		unitVec = glm::normalize(glm::vec2(1,m));
	//	}
	//	else
	//	{
	//		m = INFINITY;
	//		unitVec = glm::normalize(glm::vec2(0,1));
	//	}

	//float direction = -1;//force is at opposite direction
	//	if(raduis < 0)
	//		direction  = 1;
	//	else
	//		direction = -1;



	//	else
	//float direction = -Vlat/abs(Vlat);
	//glm::vec2 center = carPos +(unitVec*(float)raduis);
	glm::vec2 center = carPos + (unitVec*(float)raduis);
	return center;


}

std::pair<glm::vec2, double> EvoDriver::isIntersetSegment(glm::vec2& c1, glm::vec2& c2, double m,
		double c,glm::vec2& middle)
{
	Line line(m,c);
	glm::vec2 intersectPoint = getIntersectedPoint(line,c1,c2);
	//	double minY = std::min(c1.y,c2.y);
	//	double maxY = std::max(c1.y,c2.y);
	//	double minX = std::min(c1.x,c2.x);
	//	double maxX = std::max(c1.x,c2.x);
	//
	//	if(intersectPoint.y < maxY && intersectPoint.y > minY && intersectPoint.x < maxX && intersectPoint.x > minX)
	//		return std::make_pair(intersectPoint, glm::length(intersectPoint-middle));

	return std::make_pair(intersectPoint, glm::length(intersectPoint-middle));
}



double EvoDriver::timevaldiff(struct timeval *starttime, struct timeval *finishtime)
{
	double sec;
	sec=(finishtime->tv_sec-starttime->tv_sec);
	sec+=(finishtime->tv_usec-starttime->tv_usec)/1000000.0;
	return sec;
}


void EvoDriver::fillAccForGear(int gear, float start, float peak, float end, float peakPoint, float endPoint)
{
	float step = (peak -start)/80.0;
	float acc = start;
	for(float i = 0.0 ; i < peakPoint; i = i+0.01 )
	{
		std::pair<int,int> indices = getAccelarationTableIndexes(gear,i);
		accelaration.setData(indices.first, indices.second, acc);
		acc +=step;
	}


	step = (end - peak)/20.0;
	peakPoint = peakPoint+0.01;
	for(float i = peakPoint; i < endPoint; i = i + 0.01)
	{
		std::pair<int,int> indices = getAccelarationTableIndexes(gear,i);
		accelaration.setData(indices.first, indices.second, acc);
		acc +=step;
	}

}

double EvoDriver::calculateSlipAngleForFront(double steerAngle,
		double Vlong, double Vlat)
{
	double b = 2.0;//todo
	return atan((Vlat + rotationVelocity*b)/abs(Vlong)) - steerAngle*(Vlong/abs(Vlong));
}

double EvoDriver::calculateSlipAngleForRear(
		double Vlong, double Vlat)
{
	double c = 2.0;//todo
	return atan((Vlat - rotationVelocity*c)/abs(Vlong));
}

double EvoDriver::calculateAnglurVelocity(double rotation)
{
	double diff = getAngleDiff(rotation,previousCarRotationAngle);
	return (diff)/tick;
}

double EvoDriver::calculateCentripritalForce(double steerAngle,
		double Vlong, double Vlat)
{
	double slipAnlgeFront = calculateSlipAngleForFront(steerAngle, Vlong, Vlat);
	double FF = getF(slipAnlgeFront);
	double FR = getF(slipAnlgeRear);
	return FR + cos(steerAngle)*FF;

}

double EvoDriver::getF(double slipAnlge)
{
	//double absAngle = abs(slipAnlge);
	/*if(absAngle < MAX_SLIP )
		return absAngle/MAX_SLIP;
	else
		return 1;*/

	double b = 0.714;
	double c = 1.40;
	double d = 1.00;
	double e = -0.20;


	return d*sin( c*atan( b*(1-e)*slipAnlge  +  e*atan(b*slipAnlge) ) );

}

double EvoDriver::getFScaler(double slipAngle)
{
	return getF(slipAngle)*forceSlipScaler;
}

void EvoDriver::testGetCircleRadius()
{
	glm::vec2 p1(10,4);
	glm::vec2 p2(5,-4);
	glm::vec2 p3(4,1);

#ifdef DEBUGFILES
	double r = getCircleRadius(p1,p2,p3);
	std::cout << " circle radias for point p1 x " << p1.x << " y " << p1.y << " and p2 x " << p2.x << " y " << p2.y << " and p3 x " << p3.x << " y " << p3.y << " is " << r << std::endl;
#endif
}

void EvoDriver::testCalculateTrajectory()
{
	float* angles= new float[18];
	init(angles);
	Individual controller;
	controller.setSteer(0.01);
	controller.setAccel(0.9);
	controller.setGear(3);
	CarState cs;
	cs.setSpeedX(40);
	cs.setSpeedY(20);
	currentRotationAngle = 0.3;
	currentCarPosition = glm::vec2(4,4);
	tick = 0.02;
	rotationVelocity = 20;

	//Trajectory t = calculateTrajectory(cs,controller);


	char s2[10];
	sprintf(s2,"Trajecotry.svg");
	std::ofstream file1(s2);
	file1 << SVG_HEADER;
	//file1<< t << std::endl;

	file1 << SVG_FOOTER;


}

void  EvoDriver::testIntersect()
{
	glm::vec2 c1(1,7);
	glm::vec2 c2(12,51);
	glm::vec2 center(5,3);
	glm::vec2 middle(11,23);
	double r = 5;
	std::pair<glm::vec2,double> res = isIntersetSegment(c1,c2,center,r,middle);
	std::cout << " intersect point is x " << res.first.x << " y " << res.first.y << " distance from center " << res.second << std::endl;

	//line
	/* double c =41;
    double m = 22;
    std::pair<glm::vec2,double> res = isIntersetSegment(c1,c2,m,c, middle);
           std::cout << " intersect point is x " << res.first.x << " y " << res.first.y << " distance from center " << res.second << std::endl;
	 */
}



void EvoDriver::testTrajecotryCenter()
{
	//	glm::vec2 pos(10,5);
	//	double angle = 0;
	//	double radius = 100;
	//	glm::vec2 center = getTrajectoryCenter(pos,angle,radius,-1);
	//	std::cout << " trajectory center for car pos x " << pos.x << " and y " << pos.y << " direction " << angle << " with radius " << radius << " is with x" << center.x  << " and y " << center.y << std::endl;

}

void EvoDriver::updateCommonVariablesForEvalutions(CarState& state)
{

	Vlat = TO_MS_1(state.getSpeedY());///todo:direction
	Vlong = TO_MS_1(state.getSpeedX());
	currentVelocity = glm::length(glm::vec2(state.getSpeedX(),state.getSpeedY()));
	currentVelocity = TO_MS_1(currentVelocity);
	slipAnlgeRear = calculateSlipAngleForRear(Vlong, Vlat);
	currentRotationAngle = car.getRotation();
	currentCarPosition = car.getPosition();
	rotationVelocity = calculateAnglurVelocity(currentRotationAngle);
	angleOfCompoundVel = atan2(Vlat,Vlong)+currentRotationAngle;
	currentState = state;


	//	double carHeadingDirection = -state.getAngle();//accoring to our system left is + right is -
	//	carHeadingDirection = abs(carHeadingDirection);
	//	double sideOfTrack = state.getTrackPos();
	//	sideOfTrack = abs(sideOfTrack);
	//	if(still)
	//	{
	//		if( (carHeadingDirection < PI/8)&&(sideOfTrack < 1) || ((sideOfTrack < 0.2)&&(carHeadingDirection < PI/3)) )//car is looking left at left side or right at right side need to revesr
	//		{
	//			still = false;//car is at the middle now heading front
	//			std::cout << " succesfully exiting from break mode carHeadingDir " << carHeadingDirection << " side of track " << sideOfTrack<<std::endl;
	//
	//		}
	//
	//	}
	//
	//	if(out)
	//	{
	//		if(sideOfTrack < 0.9)
	//		{
	//			out = false;
	//			std::cout << " successfully exiting from out mode side of track " << sideOfTrack << std::endl;
	//		}
	//
	//	}
	//	if(fullRecovery)
	//	{
	//		if(carHeadingDirection < PI/2)
	//			fullRecovery = false;//recovered looking forward now
	//	}


}

void EvoDriver::setGear(Individual& controller,CarState& cs)
{

	time_t now = time(NULL);
	double diff = difftime(now,lastGearChangeTime);
	if(diff < 1)
		return;
	else
		lastGearChangeTime = now;

	//if( (VlongBeforeTenTicks - Vlong < 0.5 && cs.getDistRaced() > 1) || (abs(-cs.getAngle()+STEER_TO_RADIANS(controller.getSteer()) )  > PI/2) )
	//	if(still)
	if(evaluateMethod == REVERSE_EVAL)
		controller.setGear(-1);
	else if(currentVelocity < 12)
		controller.setGear(1);
	else if(currentVelocity > 12 && currentVelocity < 30)
		controller.setGear(2);
	else if(currentVelocity > 30 && currentVelocity < 43)
		controller.setGear(3);
	else if(currentVelocity > 43 && currentVelocity < 58)
		controller.setGear(4);
	else if(currentVelocity > 58 && currentVelocity < 75)
		controller.setGear(5);
	else
		controller.setGear(6);
	/*	else if(currentVelocity < 15)
			controller.setGear(1);
		else if(currentVelocity > 15 && currentVelocity < 25)
			controller.setGear(2);
		else if(currentVelocity > 25 && currentVelocity < 40)
			controller.setGear(3);
		else if(currentVelocity > 40 && currentVelocity < 50)
			controller.setGear(4);
		else if(currentVelocity > 50 && currentVelocity < 70)
			controller.setGear(5);
		else
			controller.setGear(6);*/


}

void EvoDriver::updateMemberVariables(const Individual& parent)
{

	previousTickDesicion = parent;
	previousState = currentState;


	previousVelocity = currentVelocity;
	previousTrajectory = parent.getTrajectory();


	carPositions[0] = carPositions[1];
	carPositions[1] = currentCarPosition;


	previousCarRotationAngle = currentRotationAngle;

	//	double prevFitness = parent.getFitnessVal();
	//	++tickCounter;
	//	if(tickCounter%TICK_RECOVERY ==0)
	//	{
	//		double pos = abs(currentState.getTrackPos());
	//		if(prevFitness == 0||(pos > 1 && still == false))
	//		{
	//			std::cout << " out mode became true fitness " << prevFitness << std::endl;
	//			out = true;
	//
	//		}
	//	}
	//
	//
	//	if(tickCounter%TICK_REVERSE == 0)
	//	{
	//
	//		fitnessBeforeTenTicks = prevPrevFitness;
	//		prevPrevFitness = prevFitness;
	//
	//
	//		double carHeadingDirection = -currentState.getAngle();// + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
	//		double sideOfTrack = currentState.getTrackPos();
	//
	//
	//		if(checkStill(prevFitness,carHeadingDirection,sideOfTrack))
	//		{
	//			still = true;
	//			std::cout << " reverse mode became active prevFitness " << prevFitness << " car heading dir " << carHeadingDirection << " sideOfTrack " << sideOfTrack << std::endl;
	//		}
	//	}
	//
	//	if(tickCounter%TICK_FULL_RECOVERY ==0)
	//	{
	//		double carHeadingDirection = -currentState.getAngle();// + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
	//		double sideOfTrack = currentState.getTrackPos();
	//		if((carHeadingDirection < PI && carHeadingDirection > PI/2 && sideOfTrack < -1)|| (carHeadingDirection < -PI/3 && carHeadingDirection > -PI && sideOfTrack > 1))//to avoid the time gap at inti
	//		{
	//			fullRecovery = true;
	//			still = false;
	//		}
	//
	//
	//
	//
	//	}
	//
	//	if(tickCounter == TICK_POST_RECOVERY)
	//	{
	//		if((previousTrajectory.isIsRecovery() == false)&&(previousTrajectory.isIsReverse() == false )&&( previousTrajectory.isIsStraight() == false))
	//		{
	//			std::cout << " switched to post recovery " << std::endl;
	//			out = true;
	//			still = false;
	//		}
	//		else
	//		{
	//
	//		if(( (carPosBeforePostRecoverTicks.x - currentCarPosition.x < 0.5) && (carPosBeforePostRecoverTicks.y - currentCarPosition.y < 0.5))&& currentState.getDistRaced() > 10)
	//		{
	//				std::cout << " turn to reverse mode same car pos x " << currentCarPosition.x << " prev pos x " << carPosBefore500Ticks.x << " current pos y " << currentCarPosition.y << " prev y " << carPosBefore500Ticks.y << std::endl;
	//
	//					out = true;
	//					still = false;
	//
	//		}
	//		else if((currentVelocity < 0.5 )&& (currentVelocity - velocityBeforePostRecoverTicks < 0.3) && (currentState.getDistRaced() > 10))
	//		{
	//
	//					out = true;
	//					still = false;
	//
	//		}
	//		}
	//
	//		carPosBeforePostRecoverTicks = currentCarPosition;
	//		velocityBeforePostRecoverTicks = currentVelocity;
	//		tickCounter = 0;
	//
	//	}
	//	if(tickCounter%TICK_POST_REVERSE == 0)
	//	{
	//
	//		if((previousTrajectory.isIsRecovery() == false)&&(previousTrajectory.isIsReverse() == false )&&( previousTrajectory.isIsStraight() == false))
	//		{
	//			still = true;
	//			out = false;
	//			std::cout << " switched to post reverse " << std::endl;
	//		}
	//		else
	//		{
	////			double carHeadingDirection = -currentState.getAngle();// + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
	////			double sideOfTrack = currentState.getTrackPos();
	////			double check = checkStill(prevFitness,carHeadingDirection,sideOfTrack);
	//////			if(check == false)
	////			{
	////				if(still == true)
	////					still = false;
	////				std::cout << " exiting from reverse mode prevFitness " << prevFitness << " car Dir " << carHeadingDirection << " side " << sideOfTrack << std::endl;
	////			}
	//			if((carPosBefore500Ticks.x - currentCarPosition.x < 0.5) && (carPosBefore500Ticks.y - currentCarPosition.y < 0.5)&& currentState.getDistRaced() > 10)
	//			{
	//				std::cout << " turn to reverse mode same car pos x " << currentCarPosition.x << " prev pos x " << carPosBefore500Ticks.x << " current pos y " << currentCarPosition.y << " prev y " << carPosBefore500Ticks.y << std::endl;
	//				if(still == false)
	//					still = true;
	//			}
	//			else if((currentVelocity < 0.5 )&& (currentVelocity - velocityBeforeLong < 0.3) && (currentState.getDistRaced() > 10))
	//			{
	//				if(still == false)
	//				still = true;
	//			}
	//		}
	//		carPosBefore500Ticks = currentCarPosition;
	//		velocityBeforeLong = currentVelocity;
	//	}
}


double EvoDriver::evalSegment(double lenFromCenter, double accelPedal)
{
	double segFitness =  ( (trackWidth/2.0)-lenFromCenter);
	//segFitness = (segFitness/trackWidth) * 30;
	return segFitness +accelPedal;//*10;//+acc;
}

std::pair<glm::vec2,glm::vec2> EvoDriver::getIntersectedPoint(const glm::vec2& circleCenter,double r, const glm::vec2& firsPointt,
		const glm::vec2& secondPoint, int& flag)
{

	double A;
	double B;
	double C;
	double m;
	double c;
	double d;
	double p;
	double q;
	double delta;
	glm::vec2 intersect1(INT_MAX,INT_MAX);
	glm::vec2 intersect2(INT_MAX,INT_MAX);

	if(firsPointt.x != secondPoint.x)
	{
		m = (firsPointt.y - secondPoint.y)/(firsPointt.x - secondPoint.x);
		c = firsPointt.y - m*firsPointt.x;

		p = circleCenter.x;
		q = circleCenter.y;

		A = m*m +1;
		B = 2*(m*c - m*q - p);
		C = q*q - r*r + p*p -2*c*q +c*c;

		delta = B*B -4*A*C;
		if(delta > 0)
		{
			double deltaSQ = sqrt(delta);
			intersect1.x = (-B + deltaSQ)/(2*A);
			intersect1.y = m*intersect1.x +c;

			intersect2.x = (-B - deltaSQ)/(2*A);
			intersect2.y = m*intersect2.x +c;
			flag = 2;

		}
		else if(delta == 0)
		{
			intersect1.x = (-B)/(2*A);
			intersect1.y = m*intersect1.x +c;
			flag = 1;
		}
		else//no intersect
		{
			flag = 0;
		}
	}
	else
	{
		d = firsPointt.x;
		A = 1;
		B = -2*q;
		C = d*d - 2*d*p + p*p + q*q - r*r;

		delta = B*B - 4*A*C;
		if(delta > 0)
		{
			double deltaSQ = sqrt(delta);
			intersect1.y = (-B + deltaSQ)/(2*A);
			intersect1.x = d;

			intersect2.y = (-B - deltaSQ)/(2*A);
			intersect2.x = d;

			flag = 2;
		}
		else if(delta ==0)
		{
			intersect1.y = (-B)/(2*A);
			intersect1.x = d;

			flag = 1;
		}
		else//does not intersect
		{
			flag = 0;
		}

	}
	return std::make_pair(intersect1,intersect2);
}

bool EvoDriver::checkStill(double prevFitness, double carHeadingDirection,
		double sideOfTrack)
{
	if((prevFitness == MIN_FITNESS) && (fitnessBeforeTenTicks == prevFitness)&& (currentState.getDistRaced() > 10))
	{

		if( ((carHeadingDirection < PI && carHeadingDirection > 0 && sideOfTrack > 1)||(carHeadingDirection < 0 && carHeadingDirection > -PI && sideOfTrack < -1))
				||((abs(carHeadingDirection) >  2*PI/3)&& abs(sideOfTrack) < 0.5) )
			return true;
	}


	return false;
}

void EvoDriver::reInit()
{
	previousTickDesicion.setAccel(0.5);
	previousTickDesicion.setBrake(0);
	previousTickDesicion.setClutch(0);
	previousTickDesicion.setFocus(0);
	previousTickDesicion.setGear(1);
	previousTickDesicion.setMeta(0);
	previousTickDesicion.setSteer(0.0);


	carPositions[0] = glm::vec2(0,0);
	carPositions[1] = glm::vec2(0,0);

	forceSlipScaler = 12;//12;//F/M

	lastUpdatedTime.tv_sec = 0;
	lastUpdatedTime.tv_usec = 0;
	tick = 0.02;
	previousVelocity = 0;
	previousCarRotationAngle = 0;
	currentRotationAngle = 0.0;

	lastGearChangeTime = time(NULL);

	parent.setController(previousTickDesicion.getControl());
	parent.setFitnessVal(10.0);
	prevPrevFitness = 0;
	fitnessBeforeTenTicks = 0;
	out = false;
	still = false;
	tickCounter = 0;
	velocityBeforeLong = 0;
	velocityBeforePostRecoverTicks = 0;



}

void EvoDriver::testCentripritalForce()
{
	double steer = 0.03;
	double Vlat = 30;
	double Vlong = 70;
	rotationVelocity = 20;
	slipAnlgeRear = calculateSlipAngleForRear(Vlong,Vlat);
	double F = calculateCentripritalForce(steer, Vlong, Vlat);
	std::cout << " centriprital force for " << steer << " vlong " << Vlong << " vlat " << Vlat << " is " << F << std::endl;

}



string EvoDriver::drive(string sensors)
{
	CarState cs(sensors);
	return drive(cs);
}

//string EvoDriver::drive(CarState& cs)
//{
//	update(cs);
//
//	int generationCount = 100;
//	for(int i = 0; i < generationCount; ++i)
//	{
//		step();
//
//	}
//
//	postUpdate();
//
//	return prevParent.getControl().toString();
//}


void EvoDriver::update(CarState& cs)
{
	//std::cout  << "debugcount  "  << debugCount << " vlat " << Vlat << " vlong " << Vlong << " current velocity " << currentVelocity << std::endl;

	this->mode = this->selectMode();

	if(mode == SIMPLE_MODE)
	{
		simpleModeCarState = cs;

		this->fullTrack.update(cs);
		int newLap = this->fullTrack.getLap();
		if(newLap > this->lap)
		{
			this->lap = newLap;
		}
	}
	else if(mode == EVOLUTIONARY_MODE)
	{
		++debugCount;
		timeval tv;
		gettimeofday(&tv,0);
		if(lastUpdatedTime.tv_usec != 0 || lastUpdatedTime.tv_sec != 0)
			tick = timevaldiff(&lastUpdatedTime,&tv);

		lastUpdatedTime = tv;

		//feedback learning module
		setEvaluateMethod(cs);

		updateCommonVariablesForEvalutions(cs);

		fitnessFunction->upadate(prevParent, previousState, currentState);
		updateAccelaration(cs);
		//updateCentrepretalForce(cs);

		//std::cout  << "update debugcount  "  << debugCount << " vlat " << Vlat << " vlong " << Vlong << " current velocity " << currentVelocity << std::endl;
		setGear(parent,cs);
		fitnessEvaluate(parent,cs);
		stepCount = 0;
	}


	this->car.update(cs, this->fullTrack);
}


void EvoDriver::setEvaluateMethod(CarState& cs)
{
	static int tickCount = 0;
	++tickCount;

//	if(tickCount % 5 != 0)
//		return;

	static time_t reverseStartTime = 0;

	static int stuckTickCount = 0;
	static double stuckAcceleration = 0.0;
	static double stuckPrevVelocity = 0.0;

	stuckAcceleration = (this->currentVelocity - stuckPrevVelocity) / 0.02;

	if(this->currentVelocity < 1.0/* && fabs(stuckAcceleration) < 2.0*/)
		++stuckTickCount;
	else
		stuckTickCount = 0;

	stuckPrevVelocity = this->currentVelocity;


	if(stuckTickCount > 150)
	{
		//std::cout << "Stuck" << std::endl;

		if(this->evaluateMethod != REVERSE_EVAL)
			this->evaluateMethod = REVERSE_EVAL;
		else
			this->evaluateMethod = FULL_RECOVERY_EVAL;

		stuckTickCount = 0;
		return;
	}


	if(this->evaluateMethod == REVERSE_EVAL)
	{
//		if(difftime(time(NULL), reverseStartTime) < 3.0)
//			return;
//
		if((fabs(cs.getTrackPos()) > 1.0 || fabs(cs.getAngle()) > (PI / 16)))
			return;
	}



//	if(tickCount % 100 == 0)
//	{
//		if(isStuck())
//		{
//			if(this->evaluateMethod != REVERSE_EVAL)
//			{
//				this->evaluateMethod = REVERSE_EVAL;
//				reverseStartTime = time(NULL);
//			}
//			else
//			{
//				this->evaluateMethod = FULL_RECOVERY_EVAL;
//			}
//			velocityBeforeLong = currentVelocity;
//			carPosBefore500Ticks = currentCarPosition;
//			return;
//		}
//		else
//		{
//			velocityBeforeLong = currentVelocity;
//			carPosBefore500Ticks = currentCarPosition;
//		}
//	}


	bool carInside = fabs(cs.getTrackPos()) < 1.0;
	double trackPos = cs.getTrackPos();
	double angle = -cs.getAngle();
	double absAngle = fabs(angle);

	bool still = checkStill(0.0, angle, trackPos);


	if(carInside && absAngle < (PI / 3))
	{
		this->evaluateMethod = STRAIGHT_EVAL;
	}
//	else if (carInside && absAngle < (PI / 3))
//	{
//		this->evaluateMethod = RECOVERY_EVAL;
//	}
	else if (!carInside && absAngle < (PI / 8))
	{
		this->evaluateMethod = RECOVERY_EVAL;
	}
	else
	{
		this->evaluateMethod =  FULL_RECOVERY_EVAL;
	}

//	else if (carInside && absAngle > (PI / 3))
//	{
//		this->evaluateMethod = FULL_RECOVERY_EVAL;
//	}
//
//	else if ((trackPos > 1) && angle < PI && angle > PI / 8)
//	{
//		this->evaluateMethod = REVERSE_EVAL;
//		reverseStartTime = time(NULL);
//	}
//	else if ((trackPos < -1) && angle > -PI && angle < -PI / 8)
//	{
//		this->evaluateMethod = REVERSE_EVAL;
//		reverseStartTime = time(NULL);
//	}
//
//	else if ((trackPos < -1) && angle < PI / 2 && angle > 0)
//	{
//		this->evaluateMethod = RECOVERY_EVAL;
//	}
//	else if ((trackPos > 1) && angle > -PI / 2 && angle < 0)
//	{
//		this->evaluateMethod = RECOVERY_EVAL;
//	}
//
//	else if ((trackPos > 1) && angle < -PI / 2 && angle > -PI)
//	{
//		this->evaluateMethod = FULL_RECOVERY_EVAL;
//	}
//	else if ((trackPos < -1) && angle > PI / 2 && angle < PI)
//	{
//		this->evaluateMethod = FULL_RECOVERY_EVAL;
//	}

//	std::cout << "Evaluate method " << this->evaluateMethod << " track pos " << trackPos << " angle " << angle << std::endl;
}


bool EvoDriver::isStuck()
{
	if((abs(carPosBefore500Ticks.x - currentCarPosition.x) < 5) &&
			(abs(carPosBefore500Ticks.y - currentCarPosition.y) < 5) &&
			(currentState.getDistRaced() > 10))
	{
		return true;
	}
	else if((abs(currentVelocity) < 2.0 ) &&
			(abs(currentVelocity - velocityBeforeLong) < 1) &&
			(currentState.getDistRaced() > 10))
	{
		return true;
	}

	return false;
}

CarControl EvoDriver::step()
{
	if(mode == SIMPLE_MODE)
	{
		usleep(5 * 1000);
		return this->simpleDriver.wDrive(this->simpleModeCarState);
	}
	else if(mode == EVOLUTIONARY_MODE)
	{
		++stepCount;
		Individual offspring = parent;
		prevParent = parent;
		offspring.mutate();
		fitnessEvaluate(offspring, currentState);
		double parentFitness = parent.getFitnessVal();
		double offspringFitness = offspring.getFitnessVal();
		if(parentFitness < offspringFitness)
		{
			parent = offspring;
		}

		return parent.getControl();
	}
	else
	{
		return CarControl();
	}
}

void EvoDriver::postUpdate()
{
	if(this->mode == EVOLUTIONARY_MODE)
	{
		//if(prevParent.getTrajectory().isCircle())
		//std::cout << " debug count " << debugCount<<" fitness " << prevParent.getFitnessVal() <<" steer " << std::fixed << prevParent.getSteer()  << " center x " << prevParent.getTrajectory().getCenter().x << " y " << prevParent.getTrajectory().getCenter().y << " radiua " << prevParent.getTrajectory().getRadius()<< " accel " << prevParent.getAccelBrake() << " track pos " << currentState.getTrackPos() << " car angel " << -currentState.getAngle()<< " Vlat " << Vlat<< " Vlong" << Vlong<< " car position x " << currentCarPosition.x << " y "  << currentCarPosition.y << std::endl;
		//std::cout << " state " << currentState.toString() << std::endl;

//		std::cout << "post update debug count " << debugCount << " stepcount " << stepCount<<
//									" fitness " << prevParent.getFitnessVal() <<
//									" steer " << std::fixed << prevParent.getSteer()   << prevParent.getTrajectory().getC()<<
//									" accel " << prevParent.getAccelBrake() <<
//									" Vlat " << Vlat<<
//									" Vlong " << Vlong <<
//									" isStraight " << std::boolalpha <<prevParent.getTrajectory().isIsStraight() <<
//									" isRecovery " << prevParent.getTrajectory().isIsRecovery() <<
//									" isReverse " << prevParent.getTrajectory().isIsReverse()<<
//									" isFullRecovery " << prevParent.getTrajectory().isIsFullRecovery() <<
//									" angle " << -currentState.getAngle() <<
//									" track pos " << currentState.getTrackPos() <<std::endl;

		if(prevParent.getFitnessVal() == 0 )

		{
			// std::cout << "fitness became 0" << std::endl;

		}

		updateMemberVariables(prevParent);
	}

}


void EvoDriver::stop()
{
	this->fullTrack.dump();

}


void EvoDriver::reset()
{
	// TODO: implement reset

}


EvoDriver::Mode	EvoDriver::selectMode()
{
	switch(this->stage)
	{
	case WARMUP:
	{
		if(this->fullTrack.getLap() < 2)
		{
			return SIMPLE_MODE;
		}
		else if(this->fullTrack.getLap() < (WARMUP_TRACK_BUILD_LAP_COUNT + 1) &&
				(difftime(time(NULL), this->initTime) < MAX_TRACK_BUILD_TIME))
		{
			return SIMPLE_MODE;
		}
		else
		{
			return EVOLUTIONARY_MODE;
		}

		break;
	}
	case QUALIFYING:
	{
		if(this->fullTrack.isLoaded())
			return EVOLUTIONARY_MODE;
		else
			return SIMPLE_MODE;

		break;
	}
	case RACE:
	{
		if(this->fullTrack.isLoaded())
			return EVOLUTIONARY_MODE;
		else
			return SIMPLE_MODE;

		break;
	}
	case UNKNOWN:
	{
		if(this->fullTrack.isLoaded())
			return EVOLUTIONARY_MODE;
		else
			return SIMPLE_MODE;

		break;
	}
	}

	return SIMPLE_MODE;
}


