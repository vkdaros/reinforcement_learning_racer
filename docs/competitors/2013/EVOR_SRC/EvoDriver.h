/*
 * EvoDriver.h
 *
 *  Created on: 13/04/2013
 *      Author: sam
 */

#ifndef EVODRIVER_H_
#define EVODRIVER_H_

#include "BaseDriver.h"
#include "SimpleParser.h"
#include "CarState.h"
#include "Individual.h"
#include "VisibleTrack.h"
#include "Track.h"
#include "Trajectory.h"
#include "Table.h"
#include "SimpleDriver.h"

#include <glm/glm.hpp>
#include <utility>
#include <deque>
#include "Line.h"
#include <sys/time.h>
#include "Car.h"
#include "Evaluator.h"
#include "Defs.h"

using namespace std;


class EvoDriver: public BaseDriver {
public:
	EvoDriver();
	virtual ~EvoDriver();

	// the drive function wiht string input and output
	virtual string drive(string sensors);
	virtual string drive(CarState& cs);

	// Print a shutdown message
	virtual void onShutdown();

	// Print a restart message
	virtual void onRestart();

	// Initialization of the desired angles for the rangefinders
	virtual void init(float *angles);

	void 		update(CarState& cs);
	CarControl 	step();
	void 		postUpdate();
	void		stop();
	void		reset();

private:

	friend class LinearEvaluator;

	enum Mode
	{
		SIMPLE_MODE,
		EVOLUTIONARY_MODE
	};

	enum EvaluateMethod
	{
		STRAIGHT_EVAL,
		RECOVERY_EVAL,
		FULL_RECOVERY_EVAL,
		REVERSE_EVAL
	};


	Mode	selectMode();

	void fitnessEvaluate(Individual& controller,  CarState& state);

	void updateCommonVariablesForEvalutions(CarState& state);


	void updateCentrepretalForce(CarState& currentState);
	double calculateSlideslipAngle(double Vx, double Vy);
	double getCentripritalForce(double alpha, double delta);
	double getRadius(double force, double velocity);
	double getAccelaration(int gear, double accPedal);

	void updateAccelaration(const CarState& currentState);
	void calculateVeclocityLongitudal(const CarState& state, Individual controller);
	double calculateActualForce();
	double calculateLowPassFilter(double a, double old, double current);

	std::pair<int,int> getCentrepretalForceIndexes(double row, double col);
	std::pair<int,int> getAccelarationTableIndexes(int row, double col);

	glm::vec2 getIntersectedPoint(const Line& line, const glm::vec2& firsPointt, const glm::vec2& secondPoint);
	std::pair<glm::vec2,glm::vec2> getIntersectedPoint(const glm::vec2& circleCenter, double radius,  const glm::vec2& firsPointt, const glm::vec2& secondPoint,int& flag);


		std::pair<glm::vec2, double> isIntersetSegment(glm::vec2& c1, glm::vec2& c2, glm::vec2& center, double r, glm::vec2& middle);
	std::pair<glm::vec2, double> isIntersetSegment(glm::vec2& c1, glm::vec2& c2, double m, double c, glm::vec2& middle);


	double getCircleRadius(glm::vec2& p1, glm::vec2& p2, glm::vec2& p3);
	glm::vec2 getTrajectoryCenter(glm::vec2& carPos,double radius, float direction);
	double timevaldiff(struct timeval *starttime, struct timeval *finishtime);
    void fillAccForGear(int gear,float start, float peak, float end, float peakPoint, float endPoint);
    double calculateSlipAngleForFront(double steerAngle, double Vlong, double Vlat);
    double calculateSlipAngleForRear( double Vlong, double Vlat);
    double calculateAnglurVelocity(double rotation);
    double calculateCentripritalForce(double steerAngle, double Vlong, double Vlat);
    double getF(double slipAnlge);
    double getFScaler(double slipAngle);
    void setGear(Individual& controller, CarState& cs);
    void updateMemberVariables(const Individual& controller);
    double evalSegment(double lenFromCenter, double accelPedal);

   	bool checkStill(double prevFitness, double carHeadingDirection, double sideOfTrack);
   	void reInit();//to reinitialize the varialbes at a reset call

   	void	setEvaluateMethod(CarState& cs);
   	bool	isStuck();

    Car				car;
	Track 			fullTrack;
	int				lap;
	time_t			initTime;

	Mode			mode;

	SimpleDriver	simpleDriver;
	CarState		simpleModeCarState;


	//std::vector<float> centrepretalForce;
	Table<float> accelaration;
//	int lapCount;
	Individual previousTickDesicion;
	Trajectory previousTrajectory;

//	double slipAngle;
    glm::vec2 carPositions[2];
    double previousVelocity;
    double currentVelocity;
    glm::vec2 currentCarPosition;
    float trackSensorAngles[19];
    float trackWidth;
    int debugCount;
    int debugGenCount;
//    int trackLength;
//    double prevDistanceFromStart;
    double currentRotationAngle;
//    int lastSegmentIndex;
    double previousCarRotationAngle;

    timeval lastUpdatedTime;
    double tick;
    double slipAnlgeRear;
    double rotationVelocity;
    double forceSlipScaler;//to scale the lateral force based on slip angle

    //common varialbes
    double Vlat;
    double Vlong;
    double angleOfCompoundVel;
   time_t lastGearChangeTime;
   Individual prevParent;
   Individual parent;
   CarState currentState;
   CarState previousState;
   Evaluator* fitnessFunction;
   double stepCount;
   double fitnessBeforeTenTicks;
   	int tickCounter;
   	bool out;
   	bool fullRecovery;
   	bool still;
   	double prevPrevFitness;
   	glm::vec2 carPosBeforePostRecoverTicks;
   	double velocityBeforePostRecoverTicks;
   	glm::vec2 carPosBefore500Ticks;
   	double velocityBeforeLong;

   	EvaluateMethod	evaluateMethod;


   //use for warm up
  // SimpleDriver warmUpDriver;

public:
    //test functions
    void testIntersect();
    void testTrajecotryCenter();
    void testGetCircleRadius();
    void testCalculateTrajectory();
    void testCentripritalForce();



};

#endif /* EVODRIVER_H_ */
