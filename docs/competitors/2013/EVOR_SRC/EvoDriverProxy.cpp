/*
 * EvoDriverProxy.cpp
 *
 *  Created on: 11/06/2013
 *      Author: namal
 */

#include "EvoDriverProxy.h"

#include <unistd.h>

EvoDriverProxy::EvoDriverProxy():
	initCalled(false), driverThread(&driver), carControl(NULL)
{
}

EvoDriverProxy::~EvoDriverProxy()
{
}

void EvoDriverProxy::init(float *angles)
{
	strcpy(this->driver.trackName, this->trackName);
	this->driver.stage = this->stage;

	this->driver.init(angles);

	if(!initCalled)
	{
		driverThread.start();
		initCalled = true;
	}
}

string EvoDriverProxy::drive(string sensors)
{
	driverThread.setCarState(new CarState(sensors));

	// Wait until evolution of the car control
	while(1)
	{
		usleep(5 * 1000);

		CarControl* cc = driverThread.getCarControl();
		if(cc != NULL)
		{
			delete this->carControl;
			this->carControl = cc;
			break;
		}
	}

	std::string controlString = this->carControl->toString();
//	std::cout << controlString << std::endl;

	return controlString;
}

void EvoDriverProxy::onShutdown()
{
	std::cout << "Client Shutdown" << std::endl;

	this->driverThread.stop();
	this->driverThread.join();
}

void EvoDriverProxy::onRestart()
{
	std::cout << "Client Restart" << std::endl;
	this->driverThread.reset();
}

