/*
 * EvoDriverProxy.h
 *
 *  Created on: 11/06/2013
 *      Author: namal
 */

#ifndef EVODRIVERPROXY_H_
#define EVODRIVERPROXY_H_

#include "BaseDriver.h"
#include "EvoDriver.h"
#include "EAThread.h"
#include "CarControl.h"

class EvoDriverProxy: public BaseDriver
{
public:
	EvoDriverProxy();
	virtual ~EvoDriverProxy();

	virtual void init(float *angles);
	virtual string drive(string sensors);
	virtual void onShutdown();
	virtual void onRestart();

private:
	bool		initCalled;

	EvoDriver	driver;
	EAThread	driverThread;
	CarControl*	carControl;
};

#endif /* EVODRIVERPROXY_H_ */
