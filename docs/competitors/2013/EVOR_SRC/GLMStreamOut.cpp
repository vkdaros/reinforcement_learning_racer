/*
 * GLMStreamOut.cpp
 *
 *  Created on: 02/06/2013
 *      Author: namal
 */

#include "GLMStreamOut.h"

std::ostream& operator<<(std::ostream& out, const glm::vec2& vec)
{
	out << vec.x << ", " << vec.y;
	return out;
}

