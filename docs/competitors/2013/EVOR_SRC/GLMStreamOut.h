/*
 * GLMStreamOut.h
 *
 *  Created on: 02/06/2013
 *      Author: namal
 */

#ifndef GLMSTREAMOUT_H_
#define GLMSTREAMOUT_H_

#include <glm/glm.hpp>

#include <ostream>

std::ostream& operator<<(std::ostream& out, const glm::vec2& vec);

#endif /* GLMSTREAMOUT_H_ */
