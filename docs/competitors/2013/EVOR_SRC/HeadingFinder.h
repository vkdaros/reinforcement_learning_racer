/*
 * HeadingFinder.h
 *
 *  Created on: 22/06/2013
 *      Author: sam
 */

#ifndef HEADINGFINDER_H_
#define HEADINGFINDER_H_

class Track;
class CarState;

class HeadingFinder
{
public:
	HeadingFinder(Track* track): track(track) {};
	virtual ~HeadingFinder() {};

	virtual HeadingFinder*	clone() const = 0;

	virtual void 	update(const CarState& cs) = 0;
	virtual void	generateHeadings(int trackLength) {};

protected:
	Track*	track;
};


#endif /* HEADINGFINDER_H_ */
