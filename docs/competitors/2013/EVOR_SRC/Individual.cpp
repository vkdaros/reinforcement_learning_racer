/*
 * Individual.cpp
 *
 *  Created on: 13/04/2013
 *      Author: sam
 */

#include "Individual.h"
#include <math.h>

Individual::Individual()
{
}

Individual::~Individual()
{
}

float Individual::fitness(CarState& cs)
{
	return 0.0;
}

void Individual::mutate()
{
	double delta =  0.01;
	if( rand()%2 > 0)
	 delta = - 0.01;


	float accel = controller.getAccel();
	float brake = controller.getBrake();
	 accelBrake = extractAcelBrake(accel,brake);

	accelBrake = accelBrake +delta;
	//accelBrake = ((rand()*1.0)/(RAND_MAX))*2 -1;

	accel = extractAcel(accelBrake);
	brake = extractBrake(accelBrake);
	controller.setAccel(accel);
	controller.setBrake(brake);

//	float brake = controller.getBrake();
//	if(brake == 0)
//		brake +=change;
//	else
//	 brake *=currentPercentage;
//	controller.setBrake(brake);

	float clutch = controller.getClutch();
//	clutch = (clutch+change)/2;
//	if(clutch < 0)
//		clutch = 0;
	controller.setClutch(clutch);

	//int focus =  controller.getFocus();
	//controller.setFocus(focus);

////////////////////////////////////////////////////
	/*double gearChange = rand()%3 -1;
	int gear = controller.getGear();
	gear = gear+gearChange;
	if(gear > 6)
		gear = 6;
	else if(gear < -1)
		gear = -1;
	controller.setGear(gear);*////////////////////////////////////////////

	float steer = controller.getSteer();

	//double r = rand()*1.0/RAND_MAX;
	//steer = r*2 -1;
	int steerChange = (rand() % 3) - 1;
	steer += steerChange / 100.0;

	if(steer < -1)
		steer = -1;
	else if(steer > 1)
		steer = 1;
	controller.setSteer(steer);
#ifdef DEBUGFILES
std::cout << "at mutation accel " << accel << "  steer " << steer << std::endl;

#endif

}

void Individual::mutateFlip()
{
	double accel = (rand()/RAND_MAX);


	      controller.setAccel(accel);
	      if(accel > 0)
		  controller.setBrake(0);
	      else
	      {
	    	  double brake = (rand()/RAND_MAX);
	    	  controller.setBrake(brake);
	      }



		float clutch = (rand()/RAND_MAX);
		controller.setClutch(clutch);

		//int focus =  controller.getFocus();
		//controller.setFocus(focus);

        int gear = round(rand()%8 -1);
		controller.setGear(gear);

		float steer = (rand()/RAND_MAX)*2 -1;
		controller.setSteer(steer);

}

CarControl& Individual::getControl()
{
//	if(accel < 0)
//		controller.setBrake(-accel);
//	else
//		controller.setAccel(accel);
	return controller;
}

double Individual::extractAcelBrake(float acc, float brake)
{
	if(acc > 0)
		return acc;
	else return -brake;
}

double Individual::extractAcel(float accelBrake)
{
	if(accelBrake > 0)
	{
		if(accelBrake > 1)
			accelBrake = 1;
		return accelBrake;
	}
	else
		return 0;
}

float Individual::getBrake() const
{
	return controller.getBrake();
}

void Individual::setBrake(float brake)
{
	controller.setBrake(brake);
	accelBrake =  -brake;
}

int Individual::getGear() const
{
	return controller.getGear();
}

void Individual::setGear(int gear)
{
	controller.setGear(gear);
}

float Individual::getSteer() const
{
	return controller.getSteer();
}

void Individual::setSteer(float steer)
{
	controller.setSteer(steer);
}

int Individual::getMeta() const
{
	return controller.getMeta();
}

void Individual::setMeta(int meta)
{
	controller.setMeta(meta);
}

float Individual::getClutch() const
{
	return controller.getClutch();
}

void Individual::setClutch(float clutch)
{
	controller.setClutch(clutch);
}

int Individual::getFocus()
{
	return controller.getFocus();
}

void Individual::setFocus(int focus)
{
	controller.setFocus(focus);
}

float Individual::getAccel() const
{
	return controller.getAccel();
}

void Individual::setAccel(float accel)
{
	controller.setAccel(accel);
	accelBrake = accel;
}

double Individual::extractBrake(float accelBrake)
{
	if(accelBrake < 0)
	{
		if(accelBrake < -1)
			accelBrake = -1;
		return -accelBrake;
	}
	else
		return 0;
}
