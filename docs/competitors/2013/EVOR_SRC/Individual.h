/*
 * Individual.h
 *
 *  Created on: 13/04/2013
 *      Author: sam
 */

#ifndef INDIVIDUAL_H_
#define INDIVIDUAL_H_

#include "CarControl.h"
#include "CarState.h"
#include "Trajectory.h"
class Individual
{
public:
	Individual();
	virtual ~Individual();

	float fitness(CarState &cs);
	void mutate();
	void mutateFlip();
	CarControl& getControl();



	const CarControl& getController() const
	{
		return controller;
	}

	void setController(const CarControl& controller)
	{
		this->controller = controller;
	}

	float getFitnessVal() const
	{
		return fitnessVal;
	}

	void setFitnessVal(float fitnessVal)
	{
		this->fitnessVal = fitnessVal;
	}

	const Trajectory& getTrajectory() const
	{
		return trajectory;
	}

	void setTrajectory(const Trajectory& trajectory)
	{
		this->trajectory = trajectory;
	}





	 float getAccel() const;

	        void setAccel (float accel);

	        float getBrake() const;

	        void setBrake (float brake);

	        int getGear() const;

	        void setGear(int gear);

	        float getSteer() const;

	        void setSteer(float steer);

	        int getMeta() const;

	        void setMeta(int gear);

	        float getClutch() const;

	        void setClutch(float clutch);

			int getFocus();

			void setFocus(int focus);

	double getSlipAngleFront() const
	{
		return slipAngleFront;
	}

	void setSlipAngleFront(double slipAngleFront)
	{
		this->slipAngleFront = slipAngleFront;
	}

	double getCentripritalForce() const
	{
		return centripritalForce;
	}

	void setCentripritalForce(double centripritalForce)
	{
		this->centripritalForce = centripritalForce;
	}

	double getAccelBrake() const {
		return accelBrake;
	}

	void setAccelBrake(double accelBrake) {
		this->accelBrake = accelBrake;
	}

private:
	CarControl controller;
	float fitnessVal;
    double centripritalForce;
	Trajectory trajectory;
	double slipAngleFront;
	double accelBrake;
	double extractAcelBrake(float acc, float brake);
	double extractAcel(float accelBrake);
	double extractBrake(float accelBrake);


};

#endif /* INDIVIDUAL_H_ */
