/*
 * Line.cpp
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#include "Line.h"

Line::Line():
	m(0.0), c(0.0)
{
}

Line::Line(float m, float c):
	m(m),c(c)
{
}

Line::Line(double startX, double startY, double endX, double endY)
{
	m = (endY - startY) / (endX - startX);
	c = (-m * startX) + startY;
}

Line::Line(const glm::vec2& p1, const glm::vec2& p2)
{
	m = (p2.y - p1.y) / (p2.x - p1.x);
	c = (-m * p1.x) + p1.y;
}

Line::~Line()
{
}


float Line::getC() const
{
	return c;
}

void Line::setC(float c)
{
	this->c = c;
}

float Line::getM() const
{
	return m;
}

void Line::setM(float m)
{
	this->m = m;
}

const glm::vec2& Line::getPoint() const
{
	return point;
}

void Line::setPoint(const glm::vec2& point)
{
	this->point = point;
}

