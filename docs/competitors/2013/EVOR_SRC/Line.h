/*
 * Line.h
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#ifndef LINE_H_
#define LINE_H_

#include "glm/glm.hpp"

class Line
{
public:
	Line();
	Line(float m, float c);
	Line(double startX, double startY, double endX, double endY);
	Line(const glm::vec2& p1, const glm::vec2& p2);
	~Line();

	float 	getC() const;
	void 	setC(float c);

	float 	getM() const;
	void 	setM(float m);

	const glm::vec2& getPoint() const;
	void setPoint(const glm::vec2& point);

private:
	float m;
	float c;
	glm::vec2 point;//this point is on this line to be used when m and c are inf
};

#endif /* LINE_H_ */
