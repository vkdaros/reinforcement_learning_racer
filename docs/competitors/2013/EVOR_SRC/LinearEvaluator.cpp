/*
 * LinearEvaluator.cpp
 *
 *  Created on: 15/06/2013
 *      Author: sam
 */

#include "LinearEvaluator.h"
#include "Defs.h"
#include "EvoDriver.h"
#include "Trajectory.h"
#include "GLMStreamOut.h"
#include "SVG.h"
#include <fstream>


LinearEvaluator::LinearEvaluator(EvoDriver* driver):Evaluator(driver)
{
	// TODO Auto-generated constructor stub
	friction = DEFAULT_FRICTION;
	maxBrake = DEFAULT_MAX_BRAKE;

	prevBend = -1;
	slipCountInBend = 0;
	slippedInBend = false;
	slippedOutInBend = false;
	lastSlippedOutTime = 0;


}

LinearEvaluator::~LinearEvaluator() {
	// TODO Auto-generated destructor stub
}

void LinearEvaluator::init()
{
	this->driver->fullTrack.registerInitCallback(&this->bendInfoLoader);
	this->driver->fullTrack.registerDumpCallback(&this->bendInfoSaver);
}

void LinearEvaluator::upadate(Individual& controller, const CarState& prevState, const CarState& newSatate)
{

	double maxVisibleDist = newSatate.getTrack(9);
	//bendInfo = IsBendNearby(newSatate);
	double currentPosition = newSatate.getDistFromStart();
	immediateBendInfo = driver->fullTrack.getNextBend(currentPosition);
	nextBendInfo = driver->fullTrack.getNextBend(immediateBendInfo.getEnd()+1);

	if((immediateBendInfo.getStart() < currentPosition) &&
			(immediateBendInfo.getEnd() > currentPosition))
		isInsideImmediateBend = true;
	else
		isInsideImmediateBend = false;

	//	if(driver->currentCarPosition.x == driver->carPositions[0].x || driver->currentCarPosition.y == driver->carPositions[0].y)//in same positon for 2 ticks
	//	{
	//
	//	}
	//std::cout << " max visible dist " << maxVisibleDist << std::endl;

	this->updateBendInfo(newSatate);


	if((newSatate.getDistFromStart() < prevState.getDistFromStart()) &&
			(newSatate.getDistFromStart() < 10))
	{
		// This is a new lap
		// Dump available bend info
		this->bendInfoSaver.dump(&this->driver->fullTrack);
	}
}

double LinearEvaluator::evaluateFitness(Individual& controller, const CarState& state)
{
	CarControl cc = controller.getControl();
	steerAngle = STEER_TO_RADIANS(cc.getSteer());
	updateCentripritalForce(controller,state);

	Trajectory t;
	double fitness;
	t = calculateLineTrajectory(state,controller);

	//	if(controller.getGear() == -1)
	//		fitness = evaluateReverseTrajectory(controller, state, t);
	//	else if(driver->fullRecovery)
	//		fitness = evaluateFullRecoverTrajectory(controller, state, t);
	//	else if(driver->out)
	//		fitness = evaluateRecoverTrajectory(controller, state, t);
	//	else
	//		fitness = evaluateLineTrajectoryForCurve(controller, state, t);

	if(driver->evaluateMethod == EvoDriver::REVERSE_EVAL)
		fitness = evaluateReverseTrajectory(controller, state, t);
	else if(driver->evaluateMethod == EvoDriver::FULL_RECOVERY_EVAL)
		fitness = evaluateFullRecoverTrajectory(controller, state, t);
	else if(driver->evaluateMethod == EvoDriver::RECOVERY_EVAL)
		fitness = evaluateRecoverTrajectory(controller, state, t);
	else
		fitness = evaluateLineTrajectoryForCurve(controller, state, t);

	controller.setTrajectory(t);
	controller.setCentripritalForce(cForce);

	return fitness;

	//***************************************************************************************//

	//	CarControl cc = controller.getControl();
	//	steerAngle = STEER_TO_RADIANS(cc.getSteer());
	//	updateCentripritalForce(controller,state);
	//
	//		Trajectory t;
	//		double fitness;
	//		t = calculateLineTrajectory(state,controller);
	//		if(bendInfo.first)
	//		{
	//
	//		   fitness = evaluateLineTrajectoryForCurve(controller, state, t);
	//
	//		}
	//		else
	//		{
	//
	//			 fitness = evaluateLineTrajectory(controller, state, t);
	//			 //std::cout << " circular t with center x " << t.getCenter().x << " y " << t.getCenter().y << " radius " << t.getRadius() << " fitness " << fitness << std::endl;
	//		}
	//
	//		controller.setTrajectory(t);
	//		controller.setCentripritalForce(cForce);
	//		return fitness;


	/********************************************************************************************************/
	//	Trajectory t;
	//	double fitness;
	//if( driver->currentVelocity < 20)
	//{
	//		t = calculateLineTrajectory(state,controller);
	//	   fitness = evaluateLineTrajectory(controller, state, t);

	//}
	//	else
	//	{
	//		t = calculateCircularTrajectory(state,controller);
	//		 fitness = evaluateCircularTrajectory(controller, state, t);
	//		 //std::cout << " circular t with center x " << t.getCenter().x << " y " << t.getCenter().y << " radius " << t.getRadius() << " fitness " << fitness << std::endl;
	//	}

	//	controller.setTrajectory(t);
	//	controller.setCentripritalForce(cForce);
	//	return fitness;

	//***************************************************************************************************/
	/*	Trajectory tLine = calculateLineTrajectory(state,controller);
	Trajectory tCircle = calculateCircularTrajectory(state,controller);
	double fitnessLine = evaluateLineTrajectory(controller, state, tLine);
	double fitnessCircle = evaluateCircularTrajectory(controller,state,tCircle);
	if(fitnessLine > fitnessCircle)
	{
		controller.setTrajectory(tLine);
		return fitnessLine;
	}
	else
	{
		controller.setTrajectory(tCircle);
				return fitnessCircle;
	}*/

	//*******************************************************************************************************/

	//	Trajectory t = calculateTrajectory(state,controller);
	//
	//
	//	controller.setTrajectory(t);

	//
	//	if(t.isLine())
	//		return evaluateLineTrajectory(controller, state, t);
	//	else
	//		return evaluateCircularTrajectory(controller,state,t);

}





double LinearEvaluator::evaluateLineTrajectory(Individual& controller, const CarState& state,
		Trajectory& t)
{
	static int choosenSegments[] = {1,3,6,10,15,21,28,37,50,65,85,110,140,180,250};
	//static int choosenSegments[] = {1,3,7,12,19,30,45,60,80,110};
	static int choosenSegCount = sizeof(choosenSegments)/sizeof(choosenSegments[0]);
	bool valid = false;

	double carPos = state.getDistFromStart();
	int startSegment = (int)(carPos+1);
	double visibleDist = 250;//choosenSegments[choosenSegCount-1];
	int nextSegment = startSegment;
	glm::vec2 center = t.getCenter();

	int segmentIndex = 0;
	int damageFactor = 20;
	double fitness  = 0;


	CarControl& cc = controller.getControl();
	float accelPedal = cc.getAccel();
	double accelBrake = controller.getAccelBrake();



	double m = t.getM();
	double c = t.getC();
	while(segmentIndex < choosenSegCount)
	{
		TrackSegment segment = driver->fullTrack.getSegment(nextSegment);
		//	std::cout << " current segment " << segment << std::endl;
		if(segment.getIndex() == TrackSegment::UNSET_INDEX)
		{
			++segmentIndex;
			continue;
		}
#ifdef DEBUGSVG
		file1 << segment;

#endif
		glm::vec2 p1 = segment.getLeft();
		glm::vec2 p2 = segment.getRight();
		driver->trackWidth = segment.getTrackWidth();
		glm::vec2 middle = segment.getMiddle();

		double lenghtFromCenter = 0;
		bool parallel = isLinesParallel(p1,p2,m,c);
		if(!parallel)
		{
			std::pair<glm::vec2, double> intersectData = driver->isIntersetSegment(p1,p2,m,c,middle);

			glm::vec2 intersectionPoint = intersectData.first;

			lenghtFromCenter = intersectData.second;
			// std::cout << " intersect point " << intersectionPoint << " len " << lenghtFromCenter << std::endl;
		}
		if( lenghtFromCenter <= (driver->trackWidth/2))
		{

			fitness = fitness+((((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth)*30);


			//segFitness = (segFitness/trackWidth) * 30;
			//fitness = fitness +accelPedal*0.1;//*10;//+acc;


			//std::cout << "line trajectory with m "<< m << " and c " << c  << " intersected with segment with leftpont " << p1.x<< " right point " << p2.x << " distance from track center " << lenghtFromCenter << std::endl;
			//	std::cout << "fitness incremented at segment "<< segment.getIndex() << " new fitness " << fitness << "lenth from track center " << lenghtFromCenter << " trackwidth " << driver->trackWidth<< std::endl;

		}
		else
		{
			visibleDist = choosenSegments[segmentIndex];
			break;
		}

		nextSegment = startSegment + choosenSegments[segmentIndex];
		++segmentIndex;
	}

	//double accelFitness = evaluateAccel(visibleDist,controller.getAccelBrake());
	//fitness = fitness - accelFitness;
	//	//fitness = fitness - (calculateDamage(controller.getAccelBrake(),driver->currentVelocity,visibleDist));
	//	fitness = fitness - accelFitness;
	//std::cout << "visiuble distance " << visibleDist <<  " current position " << carPos << " accelbrake " << controller.getAccelBrake() <<" steer " << controller.getSteer()<< " accel fitnes " << accelFitness << " total fitness " << fitness << std::endl;

	return fitness;
}

double LinearEvaluator::calculateDamage(double a, double u,
		double S)
{
	double t;

	t = ( -u + sqrt(u*u + 2*a*S) ) / a;
	if(t < 0)
		t = ( -u - sqrt(u*u + 2*a*S) ) / a;
	double v = u + a*t;

	double damage = (v*v)/(100*t);
	//std::cout << " u " << u << " a " << a << " s " << S << " new v " << v << " in time " << t  << " damage " << damage<< std::endl;
	return damage;
}



double LinearEvaluator::evaluateAccel(double visibleDistance, double accel)
{
	double targetAccel = 0;
	if(visibleDistance > MAX_ACCEL_DIST)
		targetAccel = 1;
	else if(visibleDistance < MIN_ACCEL_DIST)
		targetAccel = -1;
	else
	{
		targetAccel = ( ( (visibleDistance-MIN_ACCEL_DIST)/(MAX_ACCEL_DIST-MIN_ACCEL_DIST) )*2 )-1;
		if(driver->Vlong < 28 && targetAccel <= 0)
			targetAccel = 1;
		else if(driver->Vlong < 35 && targetAccel < 0)
			targetAccel = 0;


	}
	//std::cout << " visible distance " << visibleDistance << " current accel " << accel << " target accel " << targetAccel << std::endl;
	return abs(targetAccel-accel);

}

double LinearEvaluator::updateCentripritalForce(Individual& controller,
		const CarState& state)
{

	cForce = driver->calculateCentripritalForce(steerAngle,driver->Vlong, driver->Vlat);
	controller.setCentripritalForce(abs(cForce));
	cForce = -cForce;
	cForce = cForce * driver->forceSlipScaler;
	//cForceDirection = (steerAngle/abs(steerAngle));
	cForceDirection = (cForce/abs(cForce));
	//cForceDirection = (-driver->Vlat/abs(driver->Vlat));
}

bool LinearEvaluator::isLinesParallel(const glm::vec2& firsPointt, const glm::vec2& secondPoint, double m,
		double c)
{

	float m1 = (firsPointt.y - secondPoint.y) / (firsPointt.x - secondPoint.x);
	if(m1 == m)
		return true;
	return false;

}

double LinearEvaluator::evaluateCircularTrajectory(Individual& controller, const CarState& state,
		Trajectory& t)
{
	static int choosenSegments[] = {1,3,6,10,15,21,28,37,50,65,85,110,140,180,230,300};
	static int choosenSegCount = sizeof(choosenSegments)/sizeof(choosenSegments[0]);
	bool valid = false;

	int startSegment = (int)(state.getDistFromStart()+1);
	double fullDist = 300;
	double lastIntsectedSegDistance = 300;
	int nextSegment = startSegment;
	glm::vec2 center = t.getCenter();

	int segmentIndex = 0;
	int lastIndex = 0;
	int damageFactor = 20;
	double fitness  = 0.0;


	CarControl& cc = controller.getControl();
	float accelPedal = cc.getAccel();
	int gear = cc.getGear();
	double acc = driver->getAccelaration(gear,accelPedal);
	double brk = cc.getBrake();
	double accelBrake = controller.getAccelBrake();
	//


#ifdef DEBUGSVGC
	std::stringstream fileName;
	fileName << "output/debug" << debugCount << "_" << debugGenCount<< ".svg";
	std::ofstream file1(fileName.str().c_str());
	file1 << SVG_HEADER;


	// Draw car
	file1 << SVG(car);
	file1 << SVG(fullTrack);

	//Draw trajectory
	file1 << SVG(t);
#endif

	double r = t.getRadius();

	while(segmentIndex < choosenSegCount)
	{

		TrackSegment segment = driver->fullTrack.getSegment(nextSegment);
		if(segment.getIndex() == TrackSegment::UNSET_INDEX)
		{
			++segmentIndex;
			continue;
		}
#ifdef DEBUGSVGC
		file1 << segment;
#endif

		glm::vec2 p1 = segment.getLeft();
		glm::vec2 p2 = segment.getRight();
		glm::vec2 middle = segment.getMiddle();
		driver->trackWidth = segment.getTrackWidth();

		std::pair<glm::vec2, double> intersectData = driver->isIntersetSegment(p1,p2,center,r,middle);

		glm::vec2 intersectionPoint = intersectData.first;
#ifdef DEBUGSVGC
		file1 << "<circle cx=\"" << intersectionPoint.x << "\" cy=\"" << -intersectionPoint.y << "\" r=\"0.25\" stroke=\"black\"  stroke-width=\"0.1\" fill=\"yellow\"/>";
#endif
		double lenghtFromCenter = intersectData.second;

		if(lenghtFromCenter >= 0 && lenghtFromCenter < (driver->trackWidth/2))
		{
			valid = true;
			//fitness = fitness + 10;
			//fitness = fitness + (choosenSegCount - segmentIndex);// + (trackWidth/2.0)-lenghtFromCenter;
			fitness = fitness + ( ( ((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth ) * 30 );
			//std::cout << " fitness " << fitness << "  at intesected segmetn " << nextSegment<< " lenthg from track center " << lenghtFromCenter<< std::endl;
			//std::cout << " fitness " << fitness <<  "circular trajectory with radiaus " << r << " and center x" << center.x << " y " << center.y << " intersected with segment with id " << segment.getIndex()<< std::endl;
			if((( driver->currentState.getTrackPos() > 1) && (center.y > driver->currentCarPosition.y ))||(( driver->currentState.getTrackPos() < -1) && (center.y < driver->currentCarPosition.y )) )
			{
				std::stringstream fileName;
				fileName << "output/debug" << driver->debugCount << "_" << driver->stepCount << "_" << segmentIndex<< ".svg";
				std::ofstream file1(fileName.str().c_str());
				file1 << SVG_HEADER;


				// Draw car
				file1 << SVG(driver->car);
				file1 << SVG(driver->fullTrack);

				//Draw trajectory
				file1 << SVG(t);
				file1 << SVG_FOOTER << std::endl;
				std::cout << "wrong interesection at track pos " << driver->currentState.getTrackPos() << " with center x " << center.x << " y " << center.y << " raduis " << r<< " carpos x " << driver->currentCarPosition.x << " y " << driver->currentCarPosition.y << " intersected at x " << intersectionPoint.x << " y " << intersectionPoint.y << " at track segment with left point x " << p1.x << "  y " << p1.y << " right point x "<<  p2.x << " y " << p2.y << " distance to track center " << lenghtFromCenter<< std::endl;
			}

#ifdef DEBUGEVALT
			//	std::cout << "circular trajectory with radiaus " << r << " and center " << center.x  << " intersected with segment with id " << segment.getIndex()<< " leftpont " << p1.x << " right point " << p2.x << " distance from track center " << lenghtFromCenter << std::endl;
			//std::cout << "fitness incremented at segment "<< segment.getIndex() << " new fitness " << fitness << std::endl;
#endif
		}
		else
		{
			//fitness = fitness + brk*1000;
			//fitness = fitness - calculateDamage(acc,driver->currentVelocity,nextSegment-startSegment);
			//	std::cout << "circular trajectory with radiaus " << r << " and center x " << center.x  << " y " << center.y << " distance from track center " << driver->currentState.getTrackPos()<< " steer " << controller.getSteer() << " acc " << acc << " fitness " << fitness << " Vlat " << driver->Vlat << " Vlong " << driver->Vlong<< " rotationVelociyt " << driver->rotationVelocity <<" car pos x " << driver->currentCarPosition.x << "  y " << driver->currentCarPosition.t<<" damage " << damageFactor << " intersected segment count " << segmentIndex<< std::endl;
			//std::cout <<" does not  intersected with segment with id " << segment.getIndex()<< " leftpont x" << p1.x << " y  " << p1.y << " right point " << p2.x << " y " << p2.y << " distance from track center " << lenghtFromCenter << std::endl;
			lastIntsectedSegDistance = choosenSegments[segmentIndex];

			lastIndex = segmentIndex;
			break;
			//fitness = fitness + (1.0/(segmentIndex+1))* lenghtFromCenter;
			//std::cout << "fitness decremented at segment "<< segment.getIndex() << " new fitness " << fitness << std::endl;

		}

		nextSegment = startSegment + choosenSegments[segmentIndex];
		++segmentIndex;

	}
#ifdef DEBUGSVGC
	file1 << SVG_FOOTER << std::endl;
#endif

	if(lastIndex == 0)
		;//fitness = -10000;
	else
	{
		double accelFitness = evaluateAccel(choosenSegments[segmentIndex],accelBrake);
		//fitness = fitness + accelFitness;
	}
	return fitness;


}

Trajectory LinearEvaluator::calculateLineTrajectory(const CarState& state, Individual& controller)
{
	//calulate trajectory curve based on current speed, accelaration, gear, rpm,steering angle, fuel level
	//find the circle with radius and center

	CarControl cc = controller.getControl();
	double steerAngle = STEER_TO_RADIANS(cc.getSteer());
	Trajectory t;

	double c;
	double m = tan(driver->currentRotationAngle+steerAngle);
	c = driver->currentCarPosition.y - m*driver->currentCarPosition.x;
	t.setLine(m,c);



	return t;
}

Trajectory LinearEvaluator::calculateCircularTrajectory(const CarState& state, Individual& controller)
{
	//calulate trajectory curve based on current speed, accelaration, gear, rpm,steering angle, fuel level
	//find the circle with radius and center
	CarControl cc = controller.getControl();
	double accelaration = driver->getAccelaration(cc.getGear(),cc.getAccel());

	Trajectory t;



	double targetV = driver->currentVelocity + accelaration;//*driver->tick;

	double slipAnlgeFront = driver->calculateSlipAngleForFront(steerAngle, driver->Vlong, driver->Vlat);
	controller.setSlipAngleFront(slipAnlgeFront);
	double radius = driver->getRadius(abs(cForce),targetV);



	glm::vec2 center = driver->getTrajectoryCenter(driver->currentCarPosition,radius,cForceDirection);
	t.setCircle(center,radius);


	//	std::cout << "calculateTrajectory  center x" << center.x << " y " << center.y << "  and radius " << radius  << " slipAngleFront " << slipAnlgeFront << " slipAngleRear " << driver->slipAnlgeRear << " centriprialForce " << cForce << " current velocity " << driver->currentVelocity << " rotation " << driver->currentRotationAngle<< std::endl;



	return t;
}

double LinearEvaluator::evaluateLineTrajectoryForCurve(Individual& controller,
		const CarState& state, Trajectory& t)
{

	//	double carHeadingDirection = -state.getAngle() + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
	//	if(abs(carHeadingDirection) >  2*PI/3)//going backwards
	//		return 0;
	static int choosenSegments[] = {1,3,6,10,15,21,28,37,50,65,85,110,140,180,200,300,350,400,600};
	// static int choosenSegments[] = {10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210,220,230,240,250,260,270,280,290,300,310,320,330,340,350,360,370,380,400};
	//	static int choosenSegments[] = {1,2,3,4,5,6,7,8,9,10,
	//			12,14,16,18,20,22,24,26,28,30,
	//			33,36,39,42,45,48,51,54,57,60,
	//			65,70,75,80,85,90,95,100,105,110,
	//			120,130,140,150,160,170,180,190,200,
	//			220,240,260,280,300,320,340,360,380,400,
	//			500,600,700,800};
	static int choosenSegCount = sizeof(choosenSegments)/sizeof(choosenSegments[0]);
	bool valid = false;

	double carPos = state.getDistFromStart();
	int startSegment = (int)(carPos+1);
	double visibleDist = 100;//choosenSegments[choosenSegCount-1];
	int nextSegment = startSegment;
	glm::vec2 center = t.getCenter();

	int segmentIndex = 0;
	int damageFactor = 20;
	double fitness  = 0;


	CarControl& cc = controller.getControl();
	float accelPedal = cc.getAccel();
	float brk = cc.getBrake();
	double accelBrake = controller.getAccelBrake();

	double targetAccel = 0;

	double accelFitness = evaluateAccel(immediateBendInfo, accelBrake,targetAccel);
	//	if(isInsideImmediateBend)
	{

		double nextTargetAccel = 0;
		double nextBendAccelFitness = evaluateAccel(nextBendInfo, accelBrake, nextTargetAccel);

		if(nextBendAccelFitness < accelFitness)
			accelFitness = nextBendAccelFitness;

		if(nextTargetAccel < targetAccel)
			targetAccel = nextTargetAccel;
	}
//	double steerBrakeFitness = 0;
//	if(targetAccel < 0)
//	{
//		steerBrakeFitness = 1-abs(controller.getSteer());
//	}



	double m = t.getM();
	double c = t.getC();
	while(segmentIndex < choosenSegCount)
	{
		TrackSegment segment = driver->fullTrack.getSegment(nextSegment);
		//	std::cout << " current segment " << segment << std::endl;
		if(segment.getIndex() == TrackSegment::UNSET_INDEX)
		{
			++segmentIndex;
			continue;
		}
#ifdef DEBUGSVG
		file1 << segment;

#endif
		glm::vec2 p1 = segment.getLeft();
		glm::vec2 p2 = segment.getRight();
		driver->trackWidth = segment.getTrackWidth();
		glm::vec2 middle = segment.getMiddle();

		double lenghtFromCenter = 0;
		bool parallel = isLinesParallel(p1,p2,m,c);
		if(!parallel)
		{
			std::pair<glm::vec2, double> intersectData = driver->isIntersetSegment(p1,p2,m,c,middle);

			glm::vec2 intersectionPoint = intersectData.first;

			lenghtFromCenter = intersectData.second;
			// std::cout << " intersect point " << intersectionPoint << " len " << lenghtFromCenter << std::endl;
		}
		if( lenghtFromCenter <= (driver->trackWidth/2))
		{

			fitness = fitness+((((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth)*30);
			//fitness = fitness+100;
			//fitness = fitness+((((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth)*10) + 20;


			fitness = fitness + accelFitness*0.1;
			//fitness = fitness + steerBrakeFitness;
			//segFitness = (segFitness/trackWidth) * 30;
			//fitness = fitness -accelBrake*0.1;//*10;//+acc;


			//std::cout << "line trajectory with m "<< m << " and c " << c  << " intersected with segment with leftpont " << p1.x<< " right point " << p2.x << " distance from track center " << lenghtFromCenter << std::endl;
			//	std::cout << "fitness incremented at segment "<< segment.getIndex() << " new fitness " << fitness << "lenth from track center " << lenghtFromCenter << " trackwidth " << driver->trackWidth<< std::endl;

		}
		else
		{
			visibleDist = choosenSegments[segmentIndex];
			break;
		}

		nextSegment = startSegment + choosenSegments[segmentIndex];
		++segmentIndex;
	}


	//	double accelFitness = evaluateAccel(visibleDist,accelBrake);



	//	//fitness = fitness - (calculateDamage(controller.getAccelBrake(),driver->currentVelocity,visibleDist));
	//	fitness = fitness - accelFitness;
	//std::cout << "visiuble distance " << visibleDist <<  " current position " << carPos << " accelbrake " << controller.getAccelBrake() <<" steer " << controller.getSteer()<< " accel fitnes " << accelFitness << " total fitness " << fitness << "selected segment count " << segmentIndex<< std::endl;
	t.setIsStraight(true);
	t.setIsRecovery(false);
	t.setIsReverse(false);
	t.setIsFullRecovery(false);
	return fitness;

}

double LinearEvaluator::evaluateAccelBrake(double visibleDistance,
		double accelBrake, double currentVelocity)
{


}

std::pair<bool, int> LinearEvaluator::IsBendNearby(const CarState& cs)
{
	double visibleDist = cs.getTrack(9);
	if(visibleDist < 80)
		return std::make_pair(true,1);
	else
		return std::make_pair(false,0);
}

double LinearEvaluator::calculateMaxBendVelocity(Track::Bend& bendInfo)
{
	Track::BendUserData* userData = bendInfo.getUserData();
	BendParams* params = dynamic_cast<BendParams*>(userData);
	if(params == NULL)
	{
		if(userData != NULL)
			delete userData;

		params = new BendParams();
		bendInfo.setUserData(params);
	}


	// F = muR, F = mV*V/R
	double r = bendInfo.getRadius();
	double friction = params->getFriction();

	return sqrt(friction*r);
}

double LinearEvaluator::calculatedTargetAccel(Track::Bend& bendInfo,
		double distFromStart,double accel)
{

	double bendStart = bendInfo.getStart();
	double bendEnd = bendInfo.getEnd();
	double distToBend;
	bool insideBend = false;

	if(bendStart > distFromStart)//facing a bend from front
		distToBend = bendStart - distFromStart;
	else if(bendStart < distFromStart && distFromStart < bendEnd) //inside the bend
	{
		distToBend = 0;
		insideBend = true;
	}
	else //facing the bend in start of track from the last part of track
		distToBend = driver->fullTrack.getLength()-distFromStart + bendStart;

	double v = calculateMaxBendVelocity(bendInfo);
	double u = driver->currentVelocity;

	if(abs(v-u) < 3 && insideBend )// need to retain the speed
		return 0.25;

	if(v > u)//can accelarate more
	{
		return calculateTurnAccel();
		//to consider the transition from reserve or recovery modes to straight mode
	}


	double t = (v-u)/maxBrake;


	double minDistReqToTargetVel = u*t + 1/2*maxBrake*t*t;
	minDistReqToTargetVel += MIN_PADDING_DIST;//padding


	if(minDistReqToTargetVel < distToBend)
		return accel+0.1;
	else
	{
		//std::cout << " bend radius " << bendInfo.getRadius() << " dist to bend " << distToBend << " min required distance " << minDistReqToTargetVel << " target velocity " << v << " current velocity " << u << std::endl;
		return -1;
	}
}

double LinearEvaluator::evaluateRecoverTrajectory(Individual& controller,
		const CarState& state,  Trajectory& t)
{
	double fitness  = 0;

//	if(driver->currentVelocity > 5)
//	{
		//	double carHeadingDirection = -state.getAngle() + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
		//	double sideOfTrack = state.getTrackPos();
		//	if((carHeadingDirection < PI && carHeadingDirection > PI/20 && sideOfTrack > 1)|| (carHeadingDirection < -PI/20 && carHeadingDirection > -PI && sideOfTrack < -1))//to avoid the time gap at inti
		//		return MIN_FITNESS;

		//	static int choosenSegments[] = {1,2,3,4,5,6,7,8,9,10,11,12,15,20,30,40,50,60,70,80,90,100, 120, 140, 160, 180, 200};
		//	static int choosenSegments[] = {1,2,3,4,5,6,7,8,9,10,11,12,15,20,30,40,50,60,70,80,90,100};
		static int choosenSegments[] = {1,2,3,4,5,6,7,8,9,10,11,12,15,20,30,40,50};

		static int choosenSegCount = sizeof(choosenSegments)/sizeof(choosenSegments[0]);
		bool valid = false;

		double carPos = state.getDistFromStart();
		int startSegment = (int)(carPos+1);
		int nextSegment = startSegment;
		glm::vec2 center = t.getCenter();

		int segmentIndex = 0;
		int damageFactor = 20;
		double accelPedal = controller.getAccel();
		controller.setAccel(0.3);
		controller.getControl().setBrake(0);

		CarControl& cc = controller.getControl();


		double m = t.getM();
		double c = t.getC();
		while(segmentIndex < choosenSegCount)
		{
			TrackSegment segment = driver->fullTrack.getSegment(nextSegment);
			//	std::cout << " current segment " << segment << std::endl;
			if(segment.getIndex() == TrackSegment::UNSET_INDEX)
			{
				++segmentIndex;
				continue;
			}
#ifdef DEBUGSVG
			file1 << segment;

#endif
			glm::vec2 p1 = segment.getLeft();
			glm::vec2 p2 = segment.getRight();
			driver->trackWidth = segment.getTrackWidth();
			glm::vec2 middle = segment.getMiddle();

			double lenghtFromCenter = 0;
			bool parallel = isLinesParallel(p1,p2,m,c);
			if(!parallel)
			{
				std::pair<glm::vec2, double> intersectData = driver->isIntersetSegment(p1,p2,m,c,middle);

				glm::vec2 intersectionPoint = intersectData.first;

				lenghtFromCenter = intersectData.second;
				// std::cout << " intersect point " << intersectionPoint << " len " << lenghtFromCenter << std::endl;
			}


			fitness = fitness+((((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth) * 30);
			//fitness = fitness + 1000/lenghtFromCenter;
			//	fitness = fitness - accelPedal*0.1;


			nextSegment = startSegment + choosenSegments[segmentIndex];
			++segmentIndex;
		}
//	}
//	else
//	{
//
//		double carHeadingDirection = -state.getAngle() + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
//		double side = state.getTrackPos();
//
//		double diff = 0;
//		if(abs(side) < 1)//inside road
//		{
//			diff = getAngleDiff(carHeadingDirection,0);
//			diff = abs(diff);
//			if(diff < PI/12)
//				diff = 0;
//		}
//		else
//		{
//			diff = getAngleDiff(carHeadingDirection,PI/2);
//			diff = abs(diff);
//			if(diff < PI/12)
//				diff = 0;
//		}
//
//		double fitness =  0 - diff;
//
//	}

	//give marks for car pos


	controller.setAccel(0.4);
	controller.getControl().setBrake(0);


	t.setIsRecovery(true);//for debug
	t.setIsStraight(false);
	t.setIsReverse(false);
	t.setIsFullRecovery(false);

	if(fitness == 0)
		fitness = MIN_FITNESS;
	return fitness;


}

double LinearEvaluator::evaluateReverseTrajectory(Individual& controller,
		const CarState& state, Trajectory& t)
{
	double carHeadingDirection = -state.getAngle() + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
	double side = state.getTrackPos();
	double diff = 0;
	if(abs(side) < 1)//inside road
	{
		diff = getAngleDiff(carHeadingDirection,PI);
		diff = abs(diff);
		if(diff < PI/3)
			diff = 0;
	}
	else
	{
		if(side < 1)
			diff = getAngleDiff(carHeadingDirection,PI/2);
		else
			diff = getAngleDiff(carHeadingDirection,-PI/2);

		diff = abs(diff);
		if(diff < PI/8)
			diff = 0;
	}

	double fitness =  0 - diff;

	//give marks for car pos


	controller.setAccel(0.6);
	controller.getControl().setBrake(0);
	//	static int choosenSegments[] = {1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,36,40,45,50,55,60,70,80,90,100,120,140,160,180,200};
	//	static int choosenSegCount = sizeof(choosenSegments)/sizeof(choosenSegments[0]);
	//	bool valid = false;
	//
	//	double carPos = state.getDistFromStart();
	//	int startSegment = (int)(carPos+1);
	//	int nextSegment = startSegment;
	//	glm::vec2 center = t.getCenter();
	//
	//	int segmentIndex = 0;
	//	int damageFactor = 20;
	//	double fitness  = 0;
	//	double accelPedal = controller.getAccel();
	//	controller.setAccel(0.3);
	//
	//
	//	CarControl& cc = controller.getControl();
	//
	//
	//	double m = t.getM();
	//	double c = t.getC();
	//	while(segmentIndex < choosenSegCount)
	//	{
	//		TrackSegment segment = driver->fullTrack.getSegment(nextSegment);
	//		//	std::cout << " current segment " << segment << std::endl;
	//		if(segment.getIndex() == TrackSegment::UNSET_INDEX)
	//		{
	//			++segmentIndex;
	//			continue;
	//		}
	//#ifdef DEBUGSVG
	//		file1 << segment;
	//
	//#endif
	//		glm::vec2 p1 = segment.getLeft();
	//		glm::vec2 p2 = segment.getRight();
	//		driver->trackWidth = segment.getTrackWidth();
	//		glm::vec2 middle = segment.getMiddle();
	//
	//		double lenghtFromCenter = 0;
	//		bool parallel = isLinesParallel(p1,p2,m,c);
	//		if(!parallel)
	//		{
	//			std::pair<glm::vec2, double> intersectData = driver->isIntersetSegment(p1,p2,m,c,middle);
	//
	//			glm::vec2 intersectionPoint = intersectData.first;
	//
	//			lenghtFromCenter = intersectData.second;
	//			 std::cout << " segment " << nextSegment<< " intersect point " << intersectionPoint << " len " << lenghtFromCenter << std::endl;
	//		}
	//
	//
	//		fitness = fitness+( (((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth)*0.1);
	//		//	fitness = fitness - accelPedal*0.1;
	//
	//
	//		nextSegment = startSegment - choosenSegments[segmentIndex];
	//		++segmentIndex;
	//	}
	//	std::cout << " angle " << -state.getAngle()<<   " track position " << state.getTrackPos() << " accelbrake " << controller.getAccelBrake() <<" steer " << controller.getSteer() << " heading direction " << carHeadingDirection<< " total fitness " << fitness << std::endl;

	t.setIsReverse(true);
	t.setIsRecovery(false);
	t.setIsStraight(false);
	t.setIsFullRecovery(false);

	return fitness;
}

/*
double LinearEvaluator::evaluateReverseTrajectory(Individual& controller,
		const CarState& state, Trajectory& t)
{

	static int choosenSegments[] = {1,2,3,4,5,6,7,8,9,10,12,14,16,18,20,24,28,32,36,40,45,50,55,60,70,80,90,100,120,140,160,180,200};
	static int choosenSegCount = sizeof(choosenSegments)/sizeof(choosenSegments[0]);
	bool valid = false;

	double carPos = state.getDistFromStart();
	int startSegment = (int)(carPos+1);
	int nextSegment = startSegment;
	glm::vec2 center = t.getCenter();

	int segmentIndex = 0;
	int damageFactor = 20;
	double fitness  = 0;
	double accelPedal = controller.getAccel();
	controller.setAccel(0.3);


	CarControl& cc = controller.getControl();


	double m = t.getM();
	double c = t.getC();
	while(segmentIndex < choosenSegCount)
	{
		TrackSegment segment = driver->fullTrack.getSegment(nextSegment);
		//	std::cout << " current segment " << segment << std::endl;
		if(segment.getIndex() == TrackSegment::UNSET_INDEX)
		{
			++segmentIndex;
			continue;
		}
#ifdef DEBUGSVG
		file1 << segment;

#endif
		glm::vec2 p1 = segment.getLeft();
		glm::vec2 p2 = segment.getRight();
		driver->trackWidth = segment.getTrackWidth();
		glm::vec2 middle = segment.getMiddle();

		double lenghtFromCenter = 0;
		bool parallel = isLinesParallel(p1,p2,m,c);
		if(!parallel)
		{
			std::pair<glm::vec2, double> intersectData = driver->isIntersetSegment(p1,p2,m,c,middle);

			glm::vec2 intersectionPoint = intersectData.first;

			lenghtFromCenter = intersectData.second;
			 std::cout << " segment " << nextSegment<< " intersect point " << intersectionPoint << " len " << lenghtFromCenter << std::endl;
		}


		fitness = fitness+( (((driver->trackWidth/2.0)-lenghtFromCenter)/driver->trackWidth)*0.1);
		//	fitness = fitness - accelPedal*0.1;


		nextSegment = startSegment - choosenSegments[segmentIndex];
		++segmentIndex;
	}
	std::cout <<  " current position " << carPos << " accelbrake " << controller.getAccelBrake() <<" steer " << controller.getSteer()<< " total fitness " << fitness << "selected segment count " << segmentIndex<< std::endl;

	t.setIsReverse(true);
	t.setIsRecovery(false);
	t.setIsStraight(false);
	return fitness;
}
 */

double LinearEvaluator::evaluateAccel(Track::Bend& bendInfo, double accel, double& target)
{
	double targetAccel = calculatedTargetAccel(bendInfo,driver->currentState.getDistFromStart(),accel);
	target = targetAccel;
	return 2-abs(accel - targetAccel);
}

double LinearEvaluator::calculateTurnAccel()
{
	double angle = abs(driver->currentState.getAngle());
	double maxAngle = PI/6;
	if(angle > maxAngle)
		return 0.5;
	else
		return (((0.5)/(maxAngle)) * (maxAngle -angle) )+ 0.5;

}



void LinearEvaluator::updateBendInfo(const CarState& cs)
{
	int immediateBend = this->immediateBendInfo.getStart();
	if(immediateBend != this->prevBend)
	{
		Track::Bend& prevBendInfo = this->driver->fullTrack.getNextBend(this->prevBend);

		if(this->slippedOutInBend || this->slippedInBend)
		{
			this->updateBendFriction(prevBendInfo, false);

			int consideredBend = prevBendInfo.getStart();
			//
			// Reduce the friction of one more bend before this if its near
			//
			while(1)
			{
				Track::Bend& prevPrevBendInfo = this->driver->fullTrack.getPrevBend(consideredBend - 1);
				double distanceToPrevPrevBend = this->getTrackDistance(prevPrevBendInfo.getEnd(), consideredBend);
				if(distanceToPrevPrevBend < 100)
					this->updateBendFriction(prevPrevBendInfo, false);
				else
					break;

				consideredBend = prevPrevBendInfo.getStart();
			}
		}
		else
		{
			this->updateBendFriction(prevBendInfo, true);
		}


		//
		// Clear member variables
		//
		this->prevBend = this->immediateBendInfo.getStart();
		this->slipCountInBend = 0;
		this->slippedInBend = false;
		this->slippedOutInBend = false;
	}


	if(this->isCarSliping(cs))
		++this->slipCountInBend;

	if(this->slipCountInBend > 100)
		this->slippedInBend = true;

	if(this->isCarOutside(cs))
	{
		this->slippedOutInBend = true;
		this->lastSlippedOutTime = time(NULL);
	}

}


void LinearEvaluator::updateBendFriction(Track::Bend& bend, bool increase)
{
	//
	// Get reference to bend params
	//

	Track::BendUserData* userData = bend.getUserData();
	BendParams* params = dynamic_cast<BendParams*>(userData);
	if(params == NULL)
	{
		if(userData != NULL)
			delete userData;

		params = new BendParams();
		bend.setUserData(params);
	}

	//
	// Calculate new params
	//
	double oldFriction = params->getFriction();
	double oldMinFriction = params->getMinFriction();
	double oldMaxFriction = params->getMaxFriction();

	double newFriction = oldFriction;
	double newMinFriction = oldMinFriction;
	double newMaxFriction = oldMaxFriction;

	// Has passed the previous bend
	// Update previous bend parameters
	if(!increase)
	{
		newMaxFriction = oldFriction;

		if(oldMinFriction >= newMaxFriction)
			newMinFriction = newMaxFriction - 5.0;
		else if(oldMinFriction > 0.0)
			newMinFriction = oldMinFriction;

		if(newMinFriction == 0.0)
			newFriction = newMaxFriction - 5.0;
		else
			newFriction = (newMinFriction + newMaxFriction) / 2;


		if(newFriction < 5.0)
		{
			// Friction has reached absolute min
			newFriction = newMinFriction = newMaxFriction = 5.0;
		}
	}
	else
	{
		double timeFromLastSlipOut = difftime(time(NULL), this->lastSlippedOutTime);
		if(timeFromLastSlipOut > 5)
		{
			newMinFriction = oldFriction;

			if((oldMaxFriction > 0) && (oldMaxFriction <= newMinFriction))
				newMaxFriction = newMinFriction + 2.0;

			if(newMaxFriction == 0.0)
				newFriction = newMinFriction + 5.0;
			else
				newFriction = (newMinFriction + newMaxFriction) / 2;
		}
	}

	//
	// Set new params
	//
	params->setFriction(newFriction);
	params->setMinFriction(newMinFriction);
	params->setMaxFriction(newMaxFriction);

	//std::cout << "Update friction of bend (" << bend.getStart() << " - " << bend.getEnd() << "): "
	//		"friction " << newFriction << ", min " << newMinFriction << ", max " << newMaxFriction << std::endl;
}


bool LinearEvaluator::isCarSliping(const CarState& cs)
{
	double movingDirection = atan2(cs.getSpeedY(), cs.getSpeedX());
	bool slips = (fabs(movingDirection) > (PI / 36.0));

	//	if(slips)
	//		std::cout << "friction slip" << std::endl;

	return slips;
}


bool LinearEvaluator::isCarOutside(const CarState& cs)
{
	return (fabs(cs.getTrackPos()) > 1.0);
}


double	LinearEvaluator::getTrackDistance(double from, double to)
{
	if(to >= from)
		return (to - from);
	else
		return ((this->driver->fullTrack.getLength() - from) + to);
}










/**************************************************************************************************
 * BendParams
 **************************************************************************************************/
LinearEvaluator::BendParams::BendParams():
						friction(DEFAULT_FRICTION), minFriction(0.0), maxFriction(0.0)
{
}

LinearEvaluator::BendParams::~BendParams()
{
}

Track::BendUserData* LinearEvaluator::BendParams::clone() const
{
	return new BendParams(*this);
}

double LinearEvaluator::BendParams::getFriction() const
{
	return this->friction;
}

void LinearEvaluator::BendParams::setFriction(double friction)
{
	this->friction = friction;
}

double LinearEvaluator::BendParams::getMinFriction() const
{
	return this->minFriction;
}

void LinearEvaluator::BendParams::setMinFriction(double minFriction)
{
	this->minFriction = minFriction;
}

double LinearEvaluator::BendParams::getMaxFriction() const
{
	return this->maxFriction;
}

void LinearEvaluator::BendParams::setMaxFriction(double maxFriction)
{
	this->maxFriction = maxFriction;
}


double LinearEvaluator::evaluateFullRecoverTrajectory(Individual& controller,
		const CarState& state, Trajectory& t)
{
//	double carHeadingDirection = -state.getAngle();// + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
//	double steer = controller.getSteer();
//	double trackPos = state.getTrackPos();
//	double fitness = 0;
//	int side = 0;
//	if(!driver->previousTrajectory.isIsFullRecovery())
//	{
//		if(trackPos > 0)//standing on left looking at lower right
//		{
//			if(carHeadingDirection < 3*PI/4)
//			{
//				fitness = fitness - steer;
//				side = -1;
//			}
//			else
//			{
//				fitness = fitness  +steer;//full left
//				side = 1;
//			}
//		}
//		else
//		{
//			if(carHeadingDirection > -3*PI/4)
//			{
//				fitness = fitness + steer;
//				side = 1;
//			}
//			else
//			{
//				fitness = fitness - steer;//full right
//				side = -1;
//			}
//		}
//	}
//	else
//	{
////		 carHeadingDirection = -state.getAngle()+ STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
////		double  diff = getAngleDiff(carHeadingDirection,0);
////		 diff = abs(diff);
//		side = driver->previousTrajectory.getSide();
//		if(side == -1)//right
//		{
//			fitness = fitness -steer;
//		}
//		else
//			fitness = fitness + steer;
//	}



//	double a = t.getM();
//	double b = 1;
//	double c = t.getC();
//
//	int segmentId = state.getDistFromStart() + 10;
//	const TrackSegment& seg = this->driver->fullTrack.getSegment(segmentId);
//	const glm::vec2& centerPoint = seg.getMiddle();
//
//	double x1 = centerPoint.x;
//	double y1 = centerPoint.y;
//
//	double distanceToCenter = fabs(a*x1 + b*y1 + c) / sqrt(a*a + b*b);
//
//
//	int segmentId2 = state.getDistFromStart() + 5;
//	const TrackSegment& seg2 = this->driver->fullTrack.getSegment(segmentId2);
//	const glm::vec2& centerPoint2 = seg2.getMiddle();
//
//	double x2 = centerPoint2.x;
//	double y2 = centerPoint2.y;
//
//	double distanceToCenter2 = fabs(a*x2 + b*y2 + c) / sqrt(a*a + b*b);
//
//
//	double fitness = -distanceToCenter - distanceToCenter2 * 0.1;


	double carHeadingDirection = -state.getAngle() + STEER_TO_RADIANS(controller.getSteer());//accoring to our system left is + right is -
	double side = state.getTrackPos();
	double diff = 0;
	if(abs(side) < 1)//inside road
	{
		diff = getAngleDiff(carHeadingDirection,0);
		diff = abs(diff);
		if(diff < PI/4)
			diff = 0;
	}
	else
	{
		if(side > 1)
			diff = getAngleDiff(carHeadingDirection,PI/2);
		else
			diff = getAngleDiff(carHeadingDirection,-PI/2);

		diff = abs(diff);
		if(diff < PI/8)
			diff = 0;

	}

	double fitness =  0 - diff;


	controller.setAccel(0.5);
	controller.setBrake(0.0);

	t.setIsReverse(false);
	t.setIsRecovery(false);
	t.setIsStraight(false);
	t.setIsFullRecovery(true);

	t.setSide(0);


	return fitness;
}


void LinearEvaluator::BendInfoLoader::init(Track* track)
{
	std::string trackMetaFileName = track->getName() + "_meta.csv";
	std::ifstream in(trackMetaFileName.c_str());

	while(in.good())
	{
		int bendStart;
		double	friction, minFriction, maxFriction;
		char bendComma, frictionComma, minFrictionComma;

		in >> bendStart >> bendComma >> friction >> frictionComma >> minFriction >> minFrictionComma >> maxFriction;

		Track::Bend& bend = track->getNextBend(bendStart);
		if((bendStart >= bend.getStart()) && (bendStart <= bend.getEnd()))
		{
			// Valid entry
			Track::BendUserData* userData = bend.getUserData();
			BendParams* params = dynamic_cast<BendParams*>(userData);
			if(params == NULL)
			{
				if(userData != NULL)
					delete userData;

				params = new BendParams();
				bend.setUserData(params);
			}

			params->setFriction(friction);
			params->setMinFriction(minFriction);
			params->setMaxFriction(maxFriction);

//			std::cout << "Load bend params " << bendStart <<
//					", friction " << friction <<
//					", minFriction " << minFriction <<
//					", maxFriction " << maxFriction << std::endl;

		}
	}


}


void LinearEvaluator::BendInfoSaver::dump(Track* track)
{
	std::string trackMetaFileName = track->getName() + "_meta.csv";
	std::ofstream out(trackMetaFileName.c_str());

	std::vector<Track::Bend>::iterator it = track->beginBends();
	std::vector<Track::Bend>::iterator end = track->endBends();
	for(; it != end; ++it)
	{
		Track::Bend& bend = *it;

		Track::BendUserData* userData = bend.getUserData();
		BendParams* params = dynamic_cast<BendParams*>(userData);
		if(params != NULL)
		{
			out << bend.getStart() << ", " <<
					params->getFriction() << ", " <<
					params->getMinFriction() << ", " <<
					params->getMaxFriction() << std::endl;

//			std::cout << "Dump bend params " << bend.getStart() <<
//					", friction " << params->getFriction() <<
//					", minFriction " << params->getMinFriction() <<
//					", maxFriction " << params->getMaxFriction() << std::endl;
		}
	}
}

