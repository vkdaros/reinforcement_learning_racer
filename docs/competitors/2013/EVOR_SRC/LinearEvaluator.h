/*
 * LinearEvaluator.h
 *
 *  Created on: 15/06/2013
 *      Author: sam
 */

#ifndef LINEAREVALUATOR_H_
#define LINEAREVALUATOR_H_

#include "Evaluator.h"
#include "Track.h"

class LinearEvaluator: public Evaluator {
public:
	LinearEvaluator(EvoDriver* driver);
	virtual ~LinearEvaluator();

	virtual void init();
	virtual double evaluateFitness(Individual& indi, const CarState& state);
	virtual void upadate(Individual& controller, const CarState& prevState, const CarState& newSatate);

private:

	class BendParams: public Track::BendUserData
	{
	public:
		BendParams();
		virtual ~BendParams();

		virtual Track::BendUserData*	clone() const;

		double	getFriction() const;
		void	setFriction(double friction);

		double	getMinFriction() const;
		void	setMinFriction(double maxFriction);

		double	getMaxFriction() const;
		void	setMaxFriction(double maxFriction);

	private:
		double	friction;
		double	minFriction;
		double	maxFriction;
	};


	class BendInfoLoader: public Track::InitCallback
	{
	public:
		virtual ~BendInfoLoader() {};
		virtual void init(Track* track);
	};


	class BendInfoSaver: public Track::DumpCallback
	{
	public:
		virtual ~BendInfoSaver() {};
		virtual void dump(Track* track);
	};


	double evaluateLineTrajectory(Individual& controller, const CarState& state,  Trajectory& t);
	double evaluateReverseTrajectory(Individual& controller, const CarState& state,  Trajectory& t);
	double evaluateRecoverTrajectory(Individual& controller, const CarState& state,  Trajectory& t);
	double evaluateLineTrajectoryForCurve(Individual& controller, const CarState& state,  Trajectory& t);
	double evaluateCircularTrajectory(Individual& controller, const CarState& state,  Trajectory& t);
	double evaluateFullRecoverTrajectory(Individual& controller, const CarState& state,  Trajectory& t);

	double updateCentripritalForce(Individual& controller, const CarState& state);

	Trajectory calculateLineTrajectory( const CarState& state,  Individual& controller);
	Trajectory calculateCircularTrajectory( const CarState& state,  Individual& controller);

	double calculateDamage(double a, double u,
	    		double S);
	double evaluateAccel(double visibleDistance, double accelaration);
	double evaluateAccelBrake(double visibleDistance, double accelBrake, double currentVelocity);
	bool isLinesParallel(const glm::vec2& c1, const glm::vec2& c2, double m, double c);
	std::pair<bool,int>IsBendNearby(const CarState& cs);
	double calculateMaxBendVelocity(Track::Bend&);
	double calculatedTargetAccel(Track::Bend&, double distFromStart, double acccel);
	double evaluateAccel(Track::Bend& bendInfo, double accel, double& target);
	double calculateTurnAccel();

	void	updateBendInfo(const CarState& cs);
	void	updateBendFriction(Track::Bend& bend, bool increase);
	bool	isCarSliping(const CarState& cs);
	bool	isCarOutside(const CarState& cs);
	double	getTrackDistance(double from, double to);

	double maxVisibleDist;
	double cForce;
	double cForceDirection;
	double steerAngle;
//	std::pair<bool,int>bendInfo;
	double friction;
	double maxBrake;
	Track::Bend immediateBendInfo;
	Track::Bend nextBendInfo;
	bool isInsideImmediateBend;

	int			prevBend;	// A bend is identified by its start
	int			slipCountInBend;
	bool		slippedInBend;
	bool		slippedOutInBend;
	time_t		lastSlippedOutTime;

	BendInfoLoader	bendInfoLoader;
	BendInfoSaver	bendInfoSaver;
};

#endif /* LINEAREVALUATOR_H_ */
