/*
 * NoisyHeadingFinder.cpp
 *
 *  Created on: 21/06/2013
 *      Author: sam
 */

#include "NoisyHeadingFinder.h"
#include "Track.h"

#include "Defs.h"


#define MAX_SENSOR_DISTANCE					100.0
#define MAX_BEND_SEGMENT_LENGTH				50
#define BEND_TO_STRAIGHT_THRESHOLD_RADIUS	10000
#define MIN_DETECT_COUNT					3



NoisyHeadingFinder::NoisyHeadingFinder(Track* track):
	HeadingFinder(track),
	trackWidth(0.0), trackWidthSampleCount(0)
{
	this->sensorArray.setLowPassCoefficient(0.99);

	this->reset();
}

NoisyHeadingFinder::~NoisyHeadingFinder()
{
}

HeadingFinder* NoisyHeadingFinder::clone() const
{
	return new NoisyHeadingFinder(*this);
}


void NoisyHeadingFinder::reset()
{
	inStraight = true;
	inLeftBend = false;
	inRightBend = false;

	straightDetectCount = 0;
	leftBendDetectCount = 0;
	rightBendDetectCount = 0;

	straightStart = 0;

	bendStart = 0;
	bendRadius = 0;
	bendRadiusSampleCount = 0;

	this->segments.clear();
}


void NoisyHeadingFinder::update(const CarState& cs)
{
	this->updateSensorArray(cs);
	this->updateTrackWidth(cs.getAngle());

	double leftSensorDistance = this->sensorArray.getValue(8);
	double centerSensorDistance = this->sensorArray.getValue(9);
	double rightSensorDistance = this->sensorArray.getValue(10);

	if(centerSensorDistance < MAX_SENSOR_DISTANCE)
	{
		//
		// Bend is visible
		//
		glm::vec2 leftPoint, centerPoint, rightPoint;
		this->findSensorPoints(leftSensorDistance, centerSensorDistance, rightSensorDistance,
								leftPoint, centerPoint, rightPoint);

		double radius = this->findBendRadius(leftPoint, centerPoint, rightPoint);
		if(radius <= 0)
			return;

		if(radius > BEND_TO_STRAIGHT_THRESHOLD_RADIUS)
		{
			// Straight detected
			this->processStraight(cs.getDistFromStart());
		}
		else
		{
			SegmentType type = leftSensorDistance > rightSensorDistance ? LEFT_BEND : RIGHT_BEND;

			if(type == LEFT_BEND)
			{
				if(inLeftBend)
				{
					this->bendRadius = ((this->bendRadius * this->bendRadiusSampleCount) + radius) / (this->bendRadiusSampleCount + 1);
					++this->bendRadiusSampleCount;
				}
			}
			else
			{
				if(inRightBend)
				{
					this->bendRadius = ((this->bendRadius * this->bendRadiusSampleCount) + radius) / (this->bendRadiusSampleCount + 1);
					++this->bendRadiusSampleCount;
				}
			}

			this->processBend(type, cs.getDistFromStart(), centerSensorDistance);
		}

	}
	else
	{
		this->processStraight(cs.getDistFromStart());
	}
}


void NoisyHeadingFinder::updateSensorArray(const CarState& cs)
{
	for(int i = 0; i < 19; ++i)
		sensorArray.setValue(i, cs.getTrack(i));
}


void NoisyHeadingFinder::updateTrackWidth(double carAngle)
{
	double distanceToLeft = sensorArray.getValue(0);
	double distanceToRight = sensorArray.getValue(18);

	double cosAngle = cos(carAngle);

	double leftPerp = distanceToLeft * cosAngle;
	double rightPerp = distanceToRight * cosAngle;

	double newWidth = leftPerp + rightPerp;

	this->trackWidth = ((this->trackWidth * this->trackWidthSampleCount) + newWidth) / (this->trackWidthSampleCount + 1);
	++this->trackWidthSampleCount;
}


void NoisyHeadingFinder::findSensorPoints(double leftSensor, double centerSensor, double rightSensor,
										glm::vec2& leftPoint, glm::vec2& centerPoint, glm::vec2& rightPoint) const
{
	static glm::vec2 leftUnitVec = glm::normalize(glm::vec2(1.0, tan(TO_RADIANS(-5.0))));
	static glm::vec2 centerUnitVec = glm::normalize(glm::vec2(1.0, 0.0));
	static glm::vec2 rightUnitVec = glm::normalize(glm::vec2(1.0, tan(TO_RADIANS(5.0))));

	leftPoint = leftUnitVec * glm::vec2::value_type(leftSensor);
	centerPoint = centerUnitVec * glm::vec2::value_type(centerSensor);
	rightPoint = rightUnitVec * glm::vec2::value_type(rightSensor);
}


double NoisyHeadingFinder::findBendRadius(const glm::vec2& leftPoint, const glm::vec2& centerPoint, const glm::vec2& rightPoint)
{
	double a = glm::length(leftPoint - centerPoint);
	double b = glm::length(centerPoint - rightPoint);
	double c = glm::length(rightPoint - leftPoint);

	if(((-a + b + c) <= 0) || ((a - b + c) <= 0) || ((a + b -c) <= 0))
	{
		//std::cout << "Points are in a straight line or error" << std::endl;
		return -1; // Points are in a straight line
	}

	double r = (a * b * c) / sqrt((a + b + c) * (-a + b + c) * (a - b + c) * (a + b - c));
	return r;
}


bool NoisyHeadingFinder::isBendStarted(double bendRadius, double centerSensorReading) const
{
	double theta = acos((bendRadius - this->trackWidth / 2) / bendRadius);
	double maxSensorReadingDistance = sin(theta) * bendRadius;

	bool bendStarted = centerSensorReading < maxSensorReadingDistance;
	return bendStarted;
}


double NoisyHeadingFinder::findVisibleDistanceInBend(double bendRadius, double centerSensorReading) const
{
	if(centerSensorReading > bendRadius)
		return 0.0;

	double theta = asin(centerSensorReading / bendRadius);
	double visibleDistance = (bendRadius - (this->trackWidth / 2)) * theta;
	return visibleDistance;
}


void NoisyHeadingFinder::generateStraight(int straightEnd)
{
	if(straightEnd < this->straightStart)
		return;

	double heading = this->adjustSegmentStart(this->straightStart);
//	std::cout << "Straight detected from " << this->straightStart << " to " << straightEnd << " with heading " << heading << std::endl;

	this->segments.push_back(Segment(this->straightStart, straightEnd, heading));
}


void NoisyHeadingFinder::generateBend(SegmentType type, int bendEnd)
{
	if(bendEnd < this->bendStart)
		return;

	double heading = this->adjustSegmentStart(this->bendStart);
//	std::cout << "Bend detected from " << this->bendStart << " to " << bendEnd << " with starting Heading " << heading << " and radius " << this->bendRadius << std::endl;

	this->segments.push_back(Segment(type, this->bendStart, bendEnd, heading, this->bendRadius));
}


void NoisyHeadingFinder::generateLastSegment(int trackLength)
{
	if(inStraight)
	{
		if(this->straightStart < trackLength)
		{
			double heading = this->adjustSegmentStart(this->straightStart);
//			std::cout << "Straight detected from " << this->straightStart << " to " << trackLength << " with heading " << heading << std::endl;

			this->segments.push_back(Segment(this->straightStart, trackLength, heading));
		}
	}
	else if(inLeftBend)
	{
		if(this->bendStart < trackLength)
		{
			double heading = this->adjustSegmentStart(this->bendStart);
//			std::cout << "Left bend detected from " << this->bendStart << " to " << trackLength << " with starting Heading " << heading << " and radius " << this->bendRadius << std::endl;

			this->segments.push_back(Segment(LEFT_BEND, this->bendStart, trackLength, heading, this->bendRadius));
		}
	}
	else if(inRightBend)
	{
		if(this->bendStart < trackLength)
		{
			double heading = this->adjustSegmentStart(this->bendStart);
//			std::cout << "Right bend detected from " << this->bendStart << " to " << trackLength << " with starting Heading " << heading << " and radius " << this->bendRadius << std::endl;

			this->segments.push_back(Segment(RIGHT_BEND, this->bendStart, trackLength, heading, this->bendRadius));
		}
	}
}



double	NoisyHeadingFinder::adjustSegmentStart(int newStart)
{
	if(this->segments.empty())
		return 0.0;

	double newHeading = 0.0;

	std::vector<Segment>::reverse_iterator it = this->segments.rbegin();
	std::vector<Segment>::reverse_iterator end = this->segments.rend();
	for(; it != end; ++it)
	{
		if(it->end < newStart)
		{
			newHeading = it->getEndHeading();
			break;
		}
		else
		{
			if(it->start < newStart)
			{
				it->end = newStart - 1;
				newHeading = it->getEndHeading();
				break;
			}
		}
	}

	this->segments.erase(it.base(), this->segments.end());

	if(this->segments.empty())
		newHeading = 0.0;

	return newHeading;
}



void NoisyHeadingFinder::generateHeadings(int trackLength)
{
	std::vector<Track::Heading>	headings;
	headings.reserve(trackLength);

	this->generateLastSegment(trackLength);

	std::vector<Segment>::iterator it = this->segments.begin();
	std::vector<Segment>::iterator end = this->segments.end();

	for(; it != end; ++it)
	{
		if(it->type == STRAIGHT)
		{
			for(int i = it->start; i <= it->end; ++i)
			{
				if(i > this->track->length)
					break;

				headings.push_back(Track::Heading(i, it->heading, this->trackWidth));
			}
		}
		else
		{
			double bendSide = (it->type == LEFT_BEND) ? 1.0 : -1.0;

			int bendLength = it->end - it->start;
			double headingChange = bendLength / it->bendRadius * bendSide;
			double headingDelta = headingChange / bendLength;

			double headingAngle = it->heading;
			for(int i = it->start; i <= it->end; ++i)
			{
				if(i > this->track->length)
					break;

				headings.push_back(Track::Heading(i, headingAngle, this->trackWidth));
				headingAngle += headingDelta;
			}
		}
	}

	this->mergeHeadings(headings);

	this->reset();
}


void NoisyHeadingFinder::mergeHeadings(std::vector<Track::Heading>& newHeadings)
{
	if(newHeadings.empty())
		return;

	if(this->track->headings.empty())
	{
		this->track->headings.swap(newHeadings);
		return;
	}

	int headingCount = std::min(this->track->headings.size(), newHeadings.size());

	double prevExistingHeadingDirection = this->track->headings.front().rotation;
	double prevNewHeadingDirection = newHeadings.front().rotation;

	double lastExistingHeadingDirction = prevExistingHeadingDirection;

	for(int i = 1; i < headingCount; ++i)
	{
		Track::Heading& existingHeading = this->track->headings[i];
		Track::Heading& newHeading = newHeadings[i];

		double existingHeadingChange = getAngleDiff(existingHeading.rotation, prevExistingHeadingDirection);
		double newHeadingChange = getAngleDiff(newHeading.rotation, prevNewHeadingDirection);

		double existingHeadingAdjusment = (newHeadingChange + existingHeadingChange) / 2.0;
		double newExistingHeadingDirction = lastExistingHeadingDirction + existingHeadingAdjusment;

		prevExistingHeadingDirection = existingHeading.rotation;
		prevNewHeadingDirection = newHeading.rotation;

		existingHeading.rotation = newExistingHeadingDirction;

		lastExistingHeadingDirction = newExistingHeadingDirction;
	}

	if((int)this->track->headings.size() > headingCount)
	{
		double headingCorrection = getAngleDiff(lastExistingHeadingDirction, prevExistingHeadingDirection);

		for(int i = headingCount; i < (int)this->track->headings.size(); ++i)
			this->track->headings[i].rotation += headingCorrection;
	}


	if((int)newHeadings.size() > headingCount)
	{
		double headingCorrection = getAngleDiff(this->track->headings.back().rotation, newHeadings[headingCount - 1].rotation);

		for(int i = headingCount; i < (int)newHeadings.size(); ++i)
		{
			Track::Heading newHeading = newHeadings[i];
			newHeading.rotation += headingCorrection;

			this->track->headings.push_back(newHeading);
		}
	}
}




void NoisyHeadingFinder::processStraight(double distanceFromStart)
{
	++this->straightDetectCount;

	this->leftBendDetectCount = 0;
	this->rightBendDetectCount = 0;

	if(this->straightDetectCount > MIN_DETECT_COUNT)
	{
		//
		// Straight section
		//
		if(this->inStraight)
		{
			// Nothing to do, go until straight is over
		}
		else
		{
			SegmentType type = this->inLeftBend ? LEFT_BEND : RIGHT_BEND;

			//std::cout << "generateBend " << type << __LINE__ << std::endl;

			this->generateBend(type, distanceFromStart);

			this->straightStart = distanceFromStart + 1;

			this->inLeftBend = false;
			this->inRightBend = false;
			this->inStraight = true;

			this->bendRadius = 0.0;
			this->bendRadiusSampleCount = 0;

			//std::cout << "Straight started" << std::endl;
		}
	}
}


void NoisyHeadingFinder::processBend(SegmentType type, double distanceFromStart, double centerSensorDistance)
{
	int bendDetectCount = 0;

	if(type == LEFT_BEND)
	{
		++this->leftBendDetectCount;
		this->rightBendDetectCount = 0;

		bendDetectCount = this->leftBendDetectCount;
	}
	else
	{
		++this->rightBendDetectCount;
		this->leftBendDetectCount = 0;

		bendDetectCount = this->rightBendDetectCount;
	}

	this->straightDetectCount = 0;

	SegmentType currentType = this->inLeftBend ? LEFT_BEND : RIGHT_BEND;

	if(bendDetectCount > MIN_DETECT_COUNT)
	{
		if(this->inStraight)
		{
			//
			// End current straight at half way the detected length
			//
			double straightEnd = distanceFromStart + centerSensorDistance / 2.0;

			this->generateStraight(straightEnd);

			this->bendStart = straightEnd + 1;

			if(type == LEFT_BEND)
			{
				this->inLeftBend = true;
				this->inRightBend = false;
			}
			else if(type == RIGHT_BEND)
			{
				this->inRightBend = true;
				this->inLeftBend = false;
			}

			this->inStraight = false;

			this->bendRadius = 0.0;
			this->bendRadiusSampleCount = 0; // Resatart calculating bend radius
		}
		else if(type == currentType)
		{
			int travelledDistanceInBend = distanceFromStart - this->bendStart;
			int visibleDistanceInBend = findVisibleDistanceInBend(this->bendRadius, centerSensorDistance);
			int totalDetectedBendLength = travelledDistanceInBend + visibleDistanceInBend;

			if(totalDetectedBendLength > MAX_BEND_SEGMENT_LENGTH)
			{
				// Max allowed single bend segment length reached
				// End bend here

				double bendEnd = distanceFromStart + visibleDistanceInBend;

				//std::cout << "generateBend " << type << __LINE__ << std::endl;

				this->generateBend(type, bendEnd);

				this->bendStart = bendEnd + 1;

				this->bendRadiusSampleCount = 1; // Resatart calculating bend radius
			}

		}
		else
		{
			//
			// End current bend at half way the detected length
			//
			double bendEnd = distanceFromStart + centerSensorDistance / 2;

			//std::cout << "generateBend " << type << __LINE__ << std::endl;

			this->generateBend(currentType, bendEnd);

			this->bendStart = bendEnd + 1;

			this->bendRadius = 0.0;
			this->bendRadiusSampleCount = 0; // Resatart calculating bend radius

			if(type == LEFT_BEND)
			{
				//std::cout << "Left bend started" << std::endl;

				this->inLeftBend = true;
				this->inRightBend = false;
			}
			else if(type == RIGHT_BEND)
			{
				//std::cout << "Right bend started" << std::endl;

				this->inRightBend = true;
				this->inLeftBend = false;
			}

			this->inStraight = false;

		}
	}
}






