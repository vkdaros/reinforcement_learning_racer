/*
 * HeadingFinder.h
 *
 *  Created on: 21/06/2013
 *      Author: sam
 */

#ifndef NOISYHEADINGFINDER_H_
#define NOISYHEADINGFINDER_H_

#include "HeadingFinder.h"
#include "SensorArray.h"
#include "CarState.h"
#include "Track.h"

#include <glm/glm.hpp>

#include <vector>


class NoisyHeadingFinder: public HeadingFinder
{
public:
	NoisyHeadingFinder(Track* track);
	virtual ~NoisyHeadingFinder();

	virtual HeadingFinder*	clone() const;

	virtual void 	update(const CarState& cs);
	virtual void	generateHeadings(int trackLength);

protected:

	enum SegmentType
	{
		STRAIGHT,
		LEFT_BEND,
		RIGHT_BEND,
	};

	struct Segment
	{
		Segment(int straightStart, int straightEnd, double straightHeading):
			type(STRAIGHT),
			start(straightStart), end(straightEnd), heading(straightHeading), bendRadius(0.0) {};

		Segment(SegmentType type, int bendStart, int bendEnd, double bendStartHeading, double bendRadius):
			type(type),
			start(bendStart), end(bendEnd), heading(bendStartHeading), bendRadius(bendRadius) {};

		double	getEndHeading()
		{
			if(type == STRAIGHT)
				return heading;
			else if(type == LEFT_BEND)
				return heading + ((end - start) / bendRadius);
			else if(type == RIGHT_BEND)
				return heading - ((end - start) / bendRadius);

			return heading;
		}

		SegmentType	type;

		int			start;
		int			end;
		double		heading;

		double		bendRadius;
	};

	virtual void	reset();

	virtual void 	updateSensorArray(const CarState& cs);
	virtual void	updateTrackWidth(double carAngle);

	virtual void 	findSensorPoints(double leftSensor, double centerSenosr, double rightSensor,
									glm::vec2& leftPoint, glm::vec2& centerPoint, glm::vec2& rightPoint) const;
	virtual	double	findBendRadius(const glm::vec2& leftPoint, const glm::vec2& centerPoint, const glm::vec2& rightPoint);

	virtual bool	isBendStarted(double bendRadius, double centerSensorReading) const;
	virtual double	findVisibleDistanceInBend(double bendRadius, double centerSensorReading) const;

	virtual	void	generateStraight(int straightEnd);
	virtual void	generateBend(SegmentType type, int bendEnd);
	virtual void	generateLastSegment(int trackLength);

	virtual void	processStraight(double distanceFromStart);
	virtual void	processBend(SegmentType type, double distanceFromStart, double centerSensorDistance);

	virtual double	adjustSegmentStart(int newStart);
	virtual void	mergeHeadings(std::vector<Track::Heading>& newHeadings);

	SensorArray				sensorArray;

	double					trackWidth;
	int						trackWidthSampleCount;

	bool					inStraight;
	bool					inLeftBend;
	bool					inRightBend;

	int						straightDetectCount;
	int						leftBendDetectCount;
	int						rightBendDetectCount;

	int						straightStart;

	int 					bendStart;
	double					bendRadius;
	double					bendRadiusSampleCount;

	std::vector<Segment>	segments;
};

#endif /* NOISYHEADINGFINDER_H_ */
