/*
 * NormalHeadingFinder.cpp
 *
 *  Created on: 22/06/2013
 *      Author: sam
 */

#include "NormalHeadingFinder.h"
#include "Car.h"
#include "SVG.h"

#include <fstream>

NormalHeadingFinder::NormalHeadingFinder(Track* track):
	HeadingFinder(track),
	lastDumpVisibleSegment(-1)
{
}

NormalHeadingFinder::~NormalHeadingFinder()
{
}

HeadingFinder*	NormalHeadingFinder::clone() const
{
	return new NormalHeadingFinder(*this);
}

void NormalHeadingFinder::update(const CarState& cs)
{
//	if(abs(cs.getAngle()) < PI / 3)
//	{
		VisibleTrack visibleTrack;


		static std::ofstream sensorRawStream("sensor_raw.csv");
		for(int i = 0; i < 19; ++i)
			sensorRawStream << cs.getTrack(i) << ", ";

		sensorRawStream << std::endl;


		updateSensorArray(cs);

		static std::ofstream sensorSmoothStream("sensor_smooth.csv");
		for(int i = 0; i < 19; ++i)
			sensorSmoothStream << this->sensorArray.getValue(i) << ", ";

		sensorSmoothStream << std::endl;


		double carAngleFromTrackAxis = -cs.getAngle();

		calculateVisibleTrack(this->sensorArray, carAngleFromTrackAxis, visibleTrack);
		//		this->dumpVisibleTrackEdges(visibleTrack, distanceFromStart);

		findVisibleCenterPoints(visibleTrack);

		//find visibleTrack segements from current distances , +1,+2 distances
		deque<TrackSegment> segments;
		findTrackSegments(segments, visibleTrack, cs.getDistFromStart());

		updateHeadings(segments);

//		dumpVisibleTrack(segments);

//	}
}




void NormalHeadingFinder::updateSensorArray(const CarState& cs)
{
	for(int i = 0; i < 19; i ++)
	{
		this->sensorArray.setValue(i, cs.getTrack(i));
	}
}


void NormalHeadingFinder::calculateVisibleTrack(const SensorArray& state, double carAngleFromTrackAxis, VisibleTrack& track)
{
	bool leftFound = false;
	for(int i = 0; i < 19; i ++)
	{
		double distanceFromCar = state.getValue(i);

		double angleFromCar = this->track->car->getSensorAngle(i);
		double combinedAngle = carAngleFromTrackAxis + angleFromCar;

		if((combinedAngle > (PI / 4)) && (combinedAngle < (3 * PI / 4)))//left
		{
			glm::vec2 sensorVec = glm::vec2(cos(combinedAngle), sin(combinedAngle)) * glm::vec2::value_type(distanceFromCar);
			track.addLeftTrackSegment(sensorVec);

			leftFound = true;
		}
		else
		{
			if(leftFound)
				break;
		}
	}


	bool rightFound = false;
	for(int i = 18; i >= 0; --i)
	{
		double distanceFromCar =state.getValue(i);

		double angleFromCar = this->track->car->getSensorAngle(i);
		double combinedAngle = carAngleFromTrackAxis+angleFromCar;

		if((combinedAngle < (-PI / 4)) && (combinedAngle > (-3 * PI / 4)))//right
		{
			glm::vec2 sensorVec = glm::vec2(cos(combinedAngle), sin(combinedAngle)) * glm::vec2::value_type(distanceFromCar);
			track.addRightTrackSegment(sensorVec);

			rightFound = true;
		}
		else
		{
			if(rightFound)
				break;
		}
	}
}



void NormalHeadingFinder::findVisibleCenterPoints(VisibleTrack& trackPart)
{
	//first find center relative to current reference point
	std::deque<glm::vec2>::const_iterator itL = trackPart.getLeftEdge().begin();
	std::deque<glm::vec2>::const_iterator itLEnd = trackPart.getLeftEdge().end();

	std::deque<glm::vec2>::const_iterator itR = trackPart.getRightEdge().begin();
	std::deque<glm::vec2>::const_iterator itREnd = trackPart.getRightEdge().end();

	//infer center axis of the road from the segments
	int startingIndex = 0;
	int currentIndex = 0;
	if(itL != itLEnd){ ++itL;}
	for(; itL != itLEnd; ++itL)
	{
		glm::vec2 firstL =  *(itL-1);
		glm::vec2 secondL = *itL;

		glm::vec2 trackCenterPoint = inferCenterPoint(firstL, secondL, trackPart.getRightEdge(), startingIndex);

		CenterPoint p;

		p.point = trackCenterPoint;
		p.rEdgeIndex = startingIndex;
		p.lEdgeIndex = currentIndex;

		trackPart.addCenterPoint(p);

		//std::cout << "lenght of track part  " << trackPart.getCenterAxis().size() << std::endl;
		++currentIndex;

	}



	startingIndex = 0;
	currentIndex = 0;
	if(itR != itREnd){ ++itR;}
	for(; itR != itREnd; ++itR)
	{
		glm::vec2 firstR = *(itR - 1);
		glm::vec2 secondR = *itR;
		glm::vec2 trackCenterPoint = inferCenterPoint(firstR, secondR, trackPart.getLeftEdge(), startingIndex);
		CenterPoint p;
		p.point = trackCenterPoint;
		p.lEdgeIndex = startingIndex;
		p.rEdgeIndex = currentIndex;

		trackPart.addCenterPoint(p);

		//std::cout << "lenght of track part  " << trackPart.getCenterAxis().size() << std::endl;
		++currentIndex;
	}
}



glm::vec2 NormalHeadingFinder::inferCenterPoint(	const glm::vec2& pointFirst,
									const glm::vec2& pointSecond,
									const std::deque<glm::vec2>& edge,
									int& startingIndex)
{
	//glm::vec2 lineL = pointSecond - pointFirst;
	std::pair<glm::vec2, glm::vec2> matchingEdge;
	startingIndex = findOppositeEdge(pointFirst, pointSecond, edge, startingIndex, matchingEdge);
	Line perpendicularLine = getPerpendicularLine(pointFirst, pointSecond);

//	static std::ofstream debugPrint("output/debugFileICP.log");
//	debugPrint << " point 1 is x=" << matchingEdge.first.x << ", y=" << matchingEdge.first.y <<", point 2 x=" << matchingEdge.second.x << ", y= " << matchingEdge.second.y << std::endl;

	glm::vec2 intersectPoint = getIntersectedPoint(perpendicularLine, matchingEdge.first, matchingEdge.second);
	glm::vec2 middleP = (pointFirst + pointSecond) / 2.0f;
	glm::vec2 trackCenterPoint = (intersectPoint + middleP) / 2.0f;

	return trackCenterPoint;
}


glm::vec2 NormalHeadingFinder::getIntersectedPoint(const Line& line, const glm::vec2& firstPoint, const glm::vec2& secondPoint)
{
//	if(firstPoint.x == secondPoint.x)
//		std::cout << "same x position " << std::endl;

//	static std::ofstream debugPrint2("output/debugFile.log");
//	debugPrint2 << "  point 1 is x=" << firstPoint.x << ", y=" << firstPoint.y <<",  point 2 x=" << secondPoint.x << ", y=" << secondPoint.y << std::endl;


	double m = (firstPoint.y - secondPoint.y) / (firstPoint.x - secondPoint.x);
	double c =  firstPoint.y - m*firstPoint.x;

	double y1,x1;

	double lineM = line.getM();
	double lineC = line.getC();
	if( ( (lineM != INFINITY) && (lineM != -INFINITY) ) ||( (lineC != INFINITY) && (lineC != -INFINITY) ) )
	{
		//
		// intersection
		//

		//m*x + c = line.getM()*x+line.getC();
		if(m == line.getM())
		{
			std::cout << " m is equal " << std::endl;
		}
		x1 = (line.getC() - c ) /(m-line.getM()) ;
		y1 = m*x1 + c;
	}
	else
	{

		glm::vec2 point = line.getPoint();
		x1 = point.x;
		y1 = m*point.x +c;
	}

	glm::vec2 interectPoint(x1,y1);


	return interectPoint;
}



Line NormalHeadingFinder::getPerpendicularLine(const glm::vec2& pointFirst, const glm::vec2& pointSecond)
{
	glm::vec2 middleP = (pointSecond + pointFirst) / 2.0f;
	return getPerpendicularLine(pointFirst,pointSecond,middleP);
}


Line NormalHeadingFinder::getPerpendicularLine(const glm::vec2& pointFirst, const glm::vec2& pointSecond, const glm::vec2& pointThird)
{
	double m1 = (pointFirst.y - pointSecond.y) / (pointFirst.x - pointSecond.x);
	double m2 = -1 / m1;
	double c =  pointThird.y - m2*pointThird.x;

	return Line(m2, c);
}


int NormalHeadingFinder::findOppositeEdge(const glm::vec2& pointFirst,
							const glm::vec2& pointSecond,
							const std::deque<glm::vec2>& edge,
							int startingIndex,
							std::pair<glm::vec2,glm::vec2>& toFind)
{

	std::deque<glm::vec2>::const_iterator it = edge.begin();
	std::deque<glm::vec2>::const_iterator itEnd = edge.end();
	double bestCosAngle = 0.80;

	int foundIndex = startingIndex;
	glm::vec2 line = pointSecond - pointFirst;

	//std::cout << " current vector x" << line.x << " vector y "<< line.y << std::endl;
	//<<" current point2 x " << pointSecond.x << " Y" << pointSecond.y << " and point 1 x " << pointFirst.x << " y " << pointFirst.y << std::endl;

	bool foundPrevious = false;
	if(startingIndex < (int)edge.size())
		it+=startingIndex; //search starts from the startingIndex

	if(it != itEnd)
		++it;

	for(; it != itEnd; ++it)
	{
		glm::vec2 first = *(it-1);
		glm::vec2 second = *(it);
		glm::vec2 line1 = second - first;
		//std::cout << "considered vector x " << line1.x << " vector y " << line1.y << std::endl;
		//<<" considered point2 x " << second.x << " Y" << second.y << " and point 1 x " << first.x << " y " << first.y << std::endl;


		double cosAngle = glm::dot(line, line1) / ( glm::length(line1) * glm::length(line) );

		if(cosAngle > bestCosAngle) //cos3
		{
			//found parallel
			bestCosAngle = cosAngle;
			toFind.first = first;
			toFind.second = second;
			startingIndex = foundIndex;
			foundPrevious = true;

		}
		else if(foundPrevious)
		{
			break;
		}

		++foundIndex;
	}

	return startingIndex;
}



void NormalHeadingFinder::findTrackSegments(std::deque<TrackSegment>& segments, VisibleTrack& trackPart, double distanceFromStart)
{
	std::deque<CenterPoint>::const_iterator it = trackPart.getCenterAxis().begin();
	std::deque<CenterPoint>::const_iterator itEnd = trackPart.getCenterAxis().end();

	if(it == itEnd)
		return;

	//calculate initial offset to traverse

	double currentCenterPointDistance = it->point.x + distanceFromStart;
	if(currentCenterPointDistance < 0)
		currentCenterPointDistance = this->track->length + currentCenterPointDistance;
	//todo:fix length
	int nextSegment = currentCenterPointDistance+1;



	++it;

	while(it != itEnd)
	{
		if(nextSegment < 0)
		{

			nextSegment = this->track->length + nextSegment;
			//std::cout << "wrong segment id" << std::endl;
			//return;
		}

		double distanceForNextSegment = nextSegment - currentCenterPointDistance;

		glm::vec2 currentCenterPoint = (it-1)->point;
		glm::vec2 nextCenterPoint = it->point;

		glm::vec2 centerVector = nextCenterPoint - currentCenterPoint;
		glm::vec2 unitCenterVector = glm::normalize(centerVector);


		//calculate curent track segment using left right edges parallel to current index point
		int leftPointIndex = it->lEdgeIndex;
		int rightPointIndex = it->rEdgeIndex;
		glm::vec2 lPoint1 = trackPart.getLeftTrackPart(leftPointIndex);
		glm::vec2 lPoint2 = trackPart.getLeftTrackPart(leftPointIndex+1);
		glm::vec2 rPoint1 = trackPart.getRightTrackPart(rightPointIndex);
		glm::vec2 rPoint2 = trackPart.getRightTrackPart(rightPointIndex+1);
		double rotation = atan2(centerVector.y,centerVector.x);
		if(rotation < 0)
			rotation = (2 * PI) + rotation;


		double length = glm::length(centerVector);
		while(length > distanceForNextSegment) //skip current center point until find the valid one for next segment
		{
			glm::vec2 nextSegmentCenter = unitCenterVector * glm::vec2::value_type(distanceForNextSegment) + currentCenterPoint;

			Line line = getPerpendicularLine(currentCenterPoint,nextCenterPoint,nextSegmentCenter);//get line perpendiculr to current center line
			line.setPoint(nextSegmentCenter);

//			static std::ofstream debugPrint1("output/debugFileFTS.log");
//			debugPrint1 << " left point 1 is x=" << lPoint1.x << ", y=" << lPoint1.y <<", left point 2 x=" << lPoint2.x << ", y=" << lPoint2.y <<  ", right point 1 is x=" << rPoint1.x << ", y=" << rPoint1.y <<", right point 2 x=" << rPoint2.x << ", y " << rPoint2.y << std::endl;
//

			glm::vec2 rightPoint = getIntersectedPoint(line,rPoint1,rPoint2);
			glm::vec2 leftPoint = getIntersectedPoint(line,lPoint1,lPoint2);

			//store current track segment
			TrackSegment segment(nextSegment, rightPoint,leftPoint,nextSegmentCenter,rotation);
			segments.push_back(segment);


			nextSegment+=1;
			distanceForNextSegment =  nextSegment - currentCenterPointDistance;



		}

		currentCenterPointDistance += length;
		++it;

	}
}


void NormalHeadingFinder::updateHeadings(const std::deque<TrackSegment>& segments)
{
	if(segments.empty())
		return;

	double headingChange = findHeadingChange(segments);

//	std::cout << "headingChange at " << segments.front().getIndex() << " = " << headingChange << std::endl;
//
//	std::cout << "At " << segments.front().getIndex() << " heading = " << segments.front().getRotation() << " headingChange = " << headingChange << std::endl;


	glm::vec2 currentCenter = this->track->getSegment(segments.front().getIndex()).getMiddle();

	std::deque<TrackSegment>::const_iterator it = segments.begin();
	std::deque<TrackSegment>::const_iterator end = segments.end();
	for(; it != end; ++it)
	{
		int index = it->getIndex();
		double heading = normalizeAngle(it->getRotation() + headingChange);
		double width = glm::length(it->getRight()- it->getLeft());

		int size = this->track->headings.size();
		if (size > index)
		{
			Track::Heading& existingHeading = this->track->headings[index];
			double headingDiff = getAngleDiff(existingHeading.rotation, heading);
			existingHeading = Track::Heading(index, (heading + headingDiff / 2), (width + existingHeading.width) / 2.0);
		}
		else
		{
			Track::Heading last = this->track->headings.empty() ? Track::Heading() : this->track->headings.back();
			last.index = -1;

			this->track->headings.insert(this->track->headings.end(), (index - size), last);
			this->track->headings.push_back(Track::Heading(index, heading, width));
		}


		//
		// Add track segment for current heading
		//
		if(this->track->getSegment(index).getIndex() == TrackSegment::UNSET_INDEX)
		{
			TrackSegment seg;
			seg.setIndex(index);

			currentCenter = this->track->generateNextSegment(currentCenter, heading, width, seg);

			this->track->addSegment(seg);
		}
	}
}


double NormalHeadingFinder::findHeadingChange(const std::deque<TrackSegment>& segments)
{
	if(segments.empty())
	{
		std::cout << "Couldn't find heading, segments empty" << std::endl;
		return 0.0;
	}

	bool	first = true;
	double	firstHeadingChange = 0.0;

	double 	cumulativeHeadingChange = 0.0;
	int		headingCount = 0;

	std::deque<TrackSegment>::const_iterator it = segments.begin();
	std::deque<TrackSegment>::const_iterator end = segments.end();
	for(; it != end; ++it)
	{
		int index = it->getIndex();
		if((int)this->track->headings.size() > index)
		{
			Track::Heading& heading = this->track->headings[index];
			if(heading.index != -1)
			{
				double angleDiff = getAngleDiff(heading.rotation , it->getRotation());

				if(first)
				{
					firstHeadingChange = angleDiff;
					first = false;
				}
				else
				{
					double angleDiffToFirst = getAngleDiff(angleDiff, firstHeadingChange);
					cumulativeHeadingChange += angleDiffToFirst;
					++headingCount;
				}

//				std::cout << "existing heading rotation = " << heading.rotation << " segment rotation = " << std::fixed << it->getRotation() << " angle diff = " << angleDiff << std::endl;
			}
		}
	}

	if(headingCount > 0)
	{
		return firstHeadingChange + (cumulativeHeadingChange / headingCount);
	}
	else if(!first)
	{
		return firstHeadingChange;
	}
	else if((int)this->track->headings.size() > this->track->segments.front().getIndex())
	{
		//
		// Find last heading
		//
		double lastHeading = 0.0;

		int index = segments.front().getIndex() < (int)this->track->headings.size() ? segments.front().getIndex() : this->track->headings.size() -1;
		for(int i = index; i >= 0; --i)
		{
			if(this->track->headings[i].index != -1)
			{
				lastHeading = this->track->headings[i].rotation;
				break;
			}
		}

		std::cout << "Found heading backtracking, heading = " << lastHeading << std::endl;

		return lastHeading;
	}
	else
	{
		std::cout << "Couldnt find heading" << std::endl;
		return 0.0;
	}
}


void NormalHeadingFinder::dumpVisibleTrack(const std::deque<TrackSegment>& seg) const
{
	if(seg.empty())
		return;

	if(seg.front().getIndex() == lastDumpVisibleSegment)
		return;
	else
		lastDumpVisibleSegment = seg.front().getIndex();

	std::stringstream trackFileNameStream;
	trackFileNameStream << "visible/VisibleTrack_" << seg.front().getIndex() << ".svg";
	std::ofstream trackFile(trackFileNameStream.str().c_str());

	trackFile << SVG_HEADER << std::endl;

	std::deque<TrackSegment>::const_iterator it = seg.begin();
	std::deque<TrackSegment>::const_iterator itEnd = seg.end();
	for(; it != itEnd; ++it)
	{
		trackFile << SVG(*it) << std::endl;
	}

	trackFile << SVG_FOOTER << std::endl;

}

void NormalHeadingFinder::dumpVisibleTrackEdges(const VisibleTrack& track, int seg) const
{
	std::stringstream trackFileNameStream;
	trackFileNameStream << "visible/VisibleTrackEdges_" << seg << ".svg";
	std::ofstream trackFile(trackFileNameStream.str().c_str());

	trackFile << SVG_HEADER << std::endl;

	glm::vec2 from;
	std::deque<glm::vec2>::const_iterator it = track.getLeftEdge().begin();
	std::deque<glm::vec2>::const_iterator end = track.getLeftEdge().end();
	if(it != end)
	{
		from = *it;
		++it;
	}

	for(; it != end; ++it)
	{
		glm::vec2 to = *it;
		trackFile <<  "<line x1=\"" << std::fixed << from.x << "\" y1=\"" << -from.y<<  "\" x2=\""  << to.x << "\" y2=\""  << -to.y << "\" " <<
					"style=\"stroke:rgb(0,0,0); stroke-width:1\"/>" << std::endl;

		from = to;
	}



	it = track.getRightEdge().begin();
	end = track.getRightEdge().end();
	if(it != end)
	{
		from = *it;
		++it;
	}

	for(; it != end; ++it)
	{
		glm::vec2 to = *it;
		trackFile <<  "<line x1=\"" << std::fixed << from.x << "\" y1=\"" << -from.y<<  "\" x2=\""  << to.x << "\" y2=\""  << -to.y << "\" " <<
					"style=\"stroke:rgb(0,0,0); stroke-width:1\"/>" << std::endl;

		from = to;
	}


	trackFile << SVG_FOOTER << std::endl;

}



