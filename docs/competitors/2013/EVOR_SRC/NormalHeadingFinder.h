/*
 * NormalHeadingFinder.h
 *
 *  Created on: 22/06/2013
 *      Author: sam
 */

#ifndef NORMALHEADINGFINDER_H_
#define NORMALHEADINGFINDER_H_

#include "HeadingFinder.h"
#include "Track.h"

class NormalHeadingFinder: public HeadingFinder
{
public:
	NormalHeadingFinder(Track* track);
	virtual ~NormalHeadingFinder();

	virtual HeadingFinder*	clone() const;

	virtual void 	update(const CarState& cs);

protected:

	void					updateSensorArray(const CarState& cs);
	void 					calculateVisibleTrack (const SensorArray& state, double carAngleFromTrackAxis, VisibleTrack& track);
	void					findVisibleCenterPoints(VisibleTrack& trackPart);

	glm::vec2 				inferCenterPoint(	const glm::vec2 & pointFirst,
												const glm::vec2 & pointSecond,
												const std::deque<glm::vec2>& edge,
												int& startingIndex);

	int 					findOppositeEdge(	const glm::vec2 & pointFirst,
												const glm::vec2 & pointSecond,
												const std::deque<glm::vec2>& edge,
												int startingIndex,
												std::pair<glm::vec2,glm::vec2>& toFind);

	Line 					getPerpendicularLine(const glm::vec2& pointFirst, const glm::vec2& pointSecond);
	Line 					getPerpendicularLine(const glm::vec2& pointFirst,const glm::vec2& pointSecond, const glm::vec2& pointThird);
	glm::vec2 				getIntersectedPoint(const Line& line, const glm::vec2& firsPointt, const glm::vec2& secondPoint);

	void 					findTrackSegments(std::deque<TrackSegment>& segments,VisibleTrack& trackPart, double distanceFromStart);

	void					updateHeadings(const std::deque<TrackSegment>& segments);
	double					findHeadingChange(const std::deque<TrackSegment>& segments);

	void					dumpVisibleTrack(const std::deque<TrackSegment>& seg) const;
	void 					dumpVisibleTrackEdges(const VisibleTrack& track, int seg) const;


	SensorArray		sensorArray;

	mutable int		lastDumpVisibleSegment;

};

#endif /* NORMALHEADINGFINDER_H_ */
