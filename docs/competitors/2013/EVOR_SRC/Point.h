/*
 * Point.h
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#ifndef POINT_H_
#define POINT_H_

class Point
{
public:
	Point(double X, double Y);
	virtual ~Point();

private:
	double X;
	double Y;
};

#endif /* POINT_H_ */
