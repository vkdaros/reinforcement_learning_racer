/*
 * SVG.cpp
 *
 *  Created on: 26/05/2013
 *      Author: sam
 */

#include "SVG.h"

template<>
std::ostream& operator<<(std::ostream& out, const SVGElement<TrackSegment>& seg)
{
	const TrackSegment& segment = seg.data;

	if(segment.getIndex() == TrackSegment::UNSET_INDEX)
		return out;

	const glm::vec2& rPoint = segment.getRight();
	const glm::vec2& lPoint = segment.getLeft();

	out <<  "<line x1=\"" << std::fixed << lPoint.x << "\" y1=\"" << -lPoint.y<<  "\" x2=\""  << rPoint.x << "\" y2=\""  << -rPoint.y << "\" style=\"stroke:rgb(0,0,0);stroke-width:0.2\"/>" << std::endl;

	return out;
}


template<>
std::ostream& operator<<(std::ostream& out, const SVGElement<Track>& track)
{
	const Track& trackData = track.data;
	Track::const_iterator it = trackData.begin();
	Track::const_iterator end = trackData.end();
	for(; it != end; ++it)
	{
		if(it->getIndex() == TrackSegment::UNSET_INDEX)
			continue;

		int index = it->getIndex();

		Track::Bend b = trackData.getNextBend(index);
		int bendStart = b.getStart();
		int bendEnd = b.getEnd();

		const char* stroke = "rgb(0, 0, 0)";
		double strokeWidth = 0.2;

		if((index >= bendStart) && (index < bendEnd))
		{
			if(index == bendStart)
			{
				strokeWidth = 1.0;

				const glm::vec2& centerPoint = it->getMiddle();

				out << "<text x=\"" << centerPoint.x + 20 << "\" y=\"" << -centerPoint.y + 20 << "\" "  <<
				        "font-family=\"Verdana\" font-size=\"10\" fill=\"black\" >" << it->getIndex() << "</text>" << std::endl;
			}

			double headingChange = b.getHeadingChange();
			if(headingChange > 0)
				stroke = "rgb(255, 255, 0)";
			else
				stroke = "rgb(0, 255, 255)";
		}

		const glm::vec2& rPoint = it->getRight();
		const glm::vec2& lPoint = it->getLeft();

		out <<  "<line x1=\"" << std::fixed << lPoint.x << "\" y1=\"" << -lPoint.y<<  "\" x2=\""  << rPoint.x << "\" y2=\""  << -rPoint.y << "\" " <<
					"style=\"stroke:" << stroke << ";stroke-width:" << std::fixed << strokeWidth << "\"/>" << std::endl;

	}

	std::copy(trackData.begin(), trackData.end(), std::ostream_iterator<SVGElement<TrackSegment> >(out, "\n"));
	return out;
}


template<>
std::ostream& operator<<(std::ostream& out, const SVGElement<Car>& car)
{
	const Car& carData = car.data;

	const glm::vec2& pos = carData.getPosition();
	double rot = carData.getRotation();

	out << "<rect x=\"" << pos.x - 2.5 << "\" y=\"" << -pos.y - 1 << "\" width=\"5\" height=\"2\" " <<
			"transform=\"rotate(" << TO_DEGREES(rot) << " " << 2.5 << " " << -1 << ") \" " <<
			"style=\"fill:rgb(0,0,255);stroke-width:0.1;stroke:rgb(0,0,0)\"></rect>";

	return out;
}


template<>
std::ostream& operator<<(std::ostream& out,const SVGElement<Trajectory>& t)
{
	const Trajectory trajectory = t.data;

	if(trajectory.isLine())
	{

		double m = trajectory.getM();
		double c = trajectory.getC();

		double fromX = 1000;
		double fromY = -(fromX * m + c);

		double toX = -1000;
		double toY = -(toX * m + c);

		 out <<  "<line x1=\"" << std::fixed << fromX << "\" y1=\"" << fromY <<  "\" x2=\""  << toX << "\" y2=\""  << toY << "\" style=\"stroke:rgb(0,0,0);stroke-width:0.2\"/>";

	}
	else if(trajectory.isCircle())
	{
		const glm::vec2& c = trajectory.getCenter();
		double r = trajectory.getRadius();

		out << "<circle cx=\"" << std::fixed << c.x <<  "\" cy=\"" << -c.y << "\" r=\"" << r << "\" style=\"fill-opacity:0.0;stroke-width:0.2;stroke:red\"/>";
	}
	else if(trajectory.isBoundedLine())
	{
		std::pair<glm::vec2, glm::vec2> line = t.data.getBoundedLine();

		double fromX = line.first.x;
		double fromY = -line.first.y;

		double toX = line.second.x;
		double toY = -line.second.y;

		 out <<  "<line x1=\"" << std::fixed << fromX << "\" y1=\"" << fromY <<  "\" x2=\""  << toX << "\" y2=\""  << toY << "\" style=\"stroke:rgb(0,0,0);stroke-width:0.2\"/>";
	}
	else if(trajectory.isLineStrip())
	{
		glm::vec2 from;
		std::vector<glm::vec2>::const_iterator it = trajectory.beginLineStrip();
		std::vector<glm::vec2>::const_iterator end = trajectory.endLineStrip();
		if(it != end)
		{
			from = *it;
			++it;
		}

		for(; it != end; ++it)
		{
			glm::vec2 to = *it;
			out <<  "<line x1=\"" << std::fixed << from.x << "\" y1=\"" << from.y <<  "\" x2=\""  << to.x << "\" y2=\""  << to.y << "\" style=\"stroke:rgb(0,0,0);stroke-width:0.2\"/>";

			from = to;
		}

	}

	return out;
}

