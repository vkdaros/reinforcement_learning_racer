/*
 * SVG.h
 *
 *  Created on: 26/05/2013
 *      Author: sam
 */

#ifndef SVG_H_
#define SVG_H_

#include "TrackSegment.h"
#include "Track.h"
#include "Trajectory.h"
#include "Car.h"

#include <iostream>
#include <iterator>

#define SVG_HEADER 	"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n"\
						"<rect width=\"100%\" height=\"100%\" style=\"fill:rgb(255,255,255);stroke-width:0;stroke:rgb(0,0,0)\"/>\n\n"
#define SVG_FOOTER  "</svg>"


#define SVG(element)	makeSVGElement(element)


template<typename T>
class SVGElement
{
public:
	SVGElement(const T& data): data(data) {};

private:

	template<typename U>
	friend std::ostream& operator<<(std::ostream& out, const SVGElement<U>& seg);

	const T&	data;
};

template<typename T>
SVGElement<T> makeSVGElement(const T& element) { return SVGElement<T>(element); };

template<typename T>
std::ostream& operator<<(std::ostream& out, const SVGElement<T>& element);

#endif /* SVG_H_ */
