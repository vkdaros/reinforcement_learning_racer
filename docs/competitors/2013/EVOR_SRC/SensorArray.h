/*
 * SensorArray.h
 *
 *  Created on: 21/06/2013
 *      Author: sam
 */

#ifndef SENSORARRAY_H_
#define SENSORARRAY_H_

class SensorData
{
public:
	SensorData(): value(0.0), a(0.0) {};
	~SensorData() {};

	double	getValue() const
	{
		return this->value;
	};

	void	addValue(double value)
	{
		this->value = (this->value * this->a) + ((1.0 - this->a) * value);
	}

	void	reset()
	{
		this->value = 0;
	}

	void	setLowPassCoefficient(double a)
	{
		this->a = a;
	}

private:
	double 	value;
	double 	a;
};

class SensorArray
{
public:
	SensorArray() {};
	~SensorArray() {};

	double	getValue(int index) const { return this->sensors[index].getValue(); };
	void	setValue(int index, double value) { this->sensors[index].addValue(value); };

	void	reset() { for(int i = 0; i < 19; ++i) this->sensors[i].reset(); };

	void	setLowPassCoefficient(double a) { for(int i = 0; i < 19; ++i) this->sensors[i].setLowPassCoefficient(a); };

private:
	SensorData	sensors[19];
};


#endif /* SENSORARRAY_H_ */
