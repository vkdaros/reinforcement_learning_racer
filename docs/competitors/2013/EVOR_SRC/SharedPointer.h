/*
 * SharedPointer.h
 *
 *  Created on: 11/06/2013
 *      Author: sam
 */

#ifndef SHAREDPOINTER_H_
#define SHAREDPOINTER_H_

#include <pthread.h>
#include <iostream>

template<typename T>
class SharedPointer
{
public:
	SharedPointer()
	{
		p = NULL;
		pthread_mutex_init(&this->mtx, NULL);
	}

	~SharedPointer()
	{
		delete p;
		pthread_mutex_destroy(&this->mtx);
	}

	T*	get()
	{
		T* ret = NULL;
		pthread_mutex_lock(&this->mtx);
		{
			ret = this->p;
			this->p = NULL;
		}
		pthread_mutex_unlock(&this->mtx);

		return ret;
	}

	bool set(T* p)
	{
		bool deleted = true;

		pthread_mutex_lock(&this->mtx);
		{
			deleted = (this->p != NULL);

			delete this->p;
			this->p = p;
		}
		pthread_mutex_unlock(&this->mtx);

		return deleted;
	}

private:
	T* volatile 	p;
	pthread_mutex_t	mtx;
};



#endif /* SHAREDPOINTER_H_ */
