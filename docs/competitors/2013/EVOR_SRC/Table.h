/*
 * Table.h
 *
 *  Created on: 18/04/2013
 *      Author: sam
 */

#ifndef TABLE_H_
#define TABLE_H_

template<typename T>
class Table
{
public:

	Table(int rows, int cols)
	{
		data = new T[rows*cols];
		this->rows = rows;
		this->cols = cols;
		std::fill(data,data+rows*cols, 0);
	}


	T getData(int row, int column) const
	{
		if(row > rows )
			std::cout << " row " <<  row << " greater than total rows " << rows << std::endl;
		else if(column > cols)
			std::cout << " col " <<  column << " greater than total cols " << cols << std::endl;

		assert((row < rows) && (column < cols));

		if(row >= rows || column >= cols)
			return T();

		int index = getIndex(row,column);
		T dataItem = data[index];
		return dataItem;
	}
	void setData(int row, int col,T value)
	{
		if(row > rows )
					std::cout << " row " <<  row << " greater than total rows " << rows << std::endl;
				else if(col > cols)
					std::cout << " col " <<  col << " greater than total cols " << cols << std::endl;

		assert((row < rows) && (col < cols));
		int index = getIndex(row,col);
		data[index] = value;


	}

	int getIndex(int row, int col) const
	{
		int index = row*cols+col;
		return index;
	}

	int getCols() const
	{
		return cols;
	}

	int getRows() const
	{
		return rows;
	}

private:
	T*	data;
	int rows;
	int cols;

};

#endif /* TABLE_H_ */
