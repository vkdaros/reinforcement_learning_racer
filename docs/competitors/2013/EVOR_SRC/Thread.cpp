/*
 * Thread.cpp
 *
 *  Created on: 11/06/2013
 *      Author: sam
 */

#include "Thread.h"

Thread::Thread(): t(0)
{
}

Thread::~Thread()
{
}

void* Thread::create(void* t)
{
	Thread* thread = (Thread*)t;
	thread->run();

	return NULL;
}

void Thread::start()
{
	pthread_create(&this->t, NULL, &Thread::create, this);
}


void Thread::join()
{
	pthread_join(this->t, NULL);
}
