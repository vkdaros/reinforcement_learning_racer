/*
 * Thread.h
 *
 *  Created on: 11/06/2013
 *      Author: sam
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>

class Thread
{
public:
	Thread();
	virtual ~Thread();

	void			start();
	void			join();

	virtual void 	run() = 0;
	virtual void	stop() = 0;

private:
	static	void* 	create(void* t);

	pthread_t	t;
};

#endif /* THREAD_H_ */
