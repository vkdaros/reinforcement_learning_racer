/*
 * Track.cpp
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#include "Track.h"
#include "SimpleDriver.h"
#include "SVG.h"
#include "GLMStreamOut.h"
#include "Car.h"
#include "NormalHeadingFinder.h"
#include "NoisyHeadingFinder.h"

#include "glm/glm.hpp"

#include <iostream>
#include <fstream>
#include <iterator>
#include <numeric>

#define DELTA 						0.1

#define MIN_BEND_LENGTH				10
#define MAX_BEND_LENGTH				50

#define MIN_BEND_END_DETECT_COUNT	3

#define MIN_HEADING_CHANGE_ON_BEND	0.005
#define HEADING_WINDOW_SIZE			20


Track::Track(Car* car):
	car(car),
	loaded(false),
	lap(0),
	prevDistanceFromStart(-1.0),
	headingFinder(new NoisyHeadingFinder(this))
{
	length = std::numeric_limits<int>::max();
}


Track::Track(const Track& other):
	car(other.car),
	loaded(other.loaded),
	length(other.length),
	segments(other.segments),
	headings(other.headings),
	bends(other.bends),
	lap(other.lap),
	prevDistanceFromStart(other.prevDistanceFromStart),
	headingFinder(other.headingFinder->clone())
{
}

Track& Track::operator=(const Track& other)
{
	this->car = other.car;
	this->loaded = other.loaded;
	this->length = other.length;
	this->segments = other.segments;
	this->headings = other.headings;
	this->bends = other.bends;
	this->lap = other.lap;
	this->prevDistanceFromStart = other.prevDistanceFromStart;

	delete this->headingFinder;
	headingFinder = other.headingFinder->clone();

	return *this;
}

Track::~Track()
{
	delete this->headingFinder;
}


bool Track::isLoaded() const
{
	return this->loaded;
}

std::string	Track::getName() const
{
	return this->name;
}


void Track::init(const std::string& trackName)
{
	if(!trackName.empty())
		this->name = trackName;
	else
		this->name = "Track";

	std::string trackFileName = this->name + ".csv";

	std::ifstream in(trackFileName.c_str());
	if(!in.good())
		return;
	else
		this->loaded = true;

	double heading = 0.0, width = 0.0;
	std::string comma;

	int index = 0;
	while(in.good())
	{
		in >> heading >> comma >> width;
		headings.push_back(Heading(index++, heading, width));
	}

	length = headings.size();

	buildTrackFromHeadings();
	findBends();

	//
	// Write track to a svg file
	//
	/*
	if(length > 1)
	{
		std::string trackSVGFileName = this->name + ".svg";

		std::ofstream out(trackSVGFileName.c_str());
		out << SVG_HEADER << std::endl;
		out << SVG(*this) << std::endl;
		out << SVG_FOOTER << std::endl;
	}
	*/

	std::set<InitCallback*>::iterator cbIt = this->initCallbacks.begin();
	std::set<InitCallback*>::iterator cbEnd = this->initCallbacks.end();
	for(; cbIt != cbEnd; ++cbIt)
	{
		(*cbIt)->init(this);
	}
}


void Track::dump()
{
	this->fixMisingTrackWidthAtBeginning();

	std::string trackFileName = this->name + ".csv";

	std::ofstream out(trackFileName.c_str());

	std::vector<Heading>::iterator it = headings.begin();
	std::vector<Heading>::iterator end = headings.end();
	for(; it != end; ++it)
	{
		out << it->rotation << " , " << it->width << std::endl;
	}


	std::set<DumpCallback*>::iterator cbIt = this->dumpCallbacks.begin();
	std::set<DumpCallback*>::iterator cbEnd = this->dumpCallbacks.end();
	for(; cbIt != cbEnd; ++cbIt)
	{
		(*cbIt)->dump(this);
	}
}


void Track::fixMisingTrackWidthAtBeginning()
{
	std::vector<Heading>::iterator it = headings.begin();
	std::vector<Heading>::iterator end = headings.end();
	for(; it != end; ++it)
	{
		if(it->width > 0.0)
			break;
	}

	if(it == end)
		return;

	double width = it->width;

	end = it;
	for(it = headings.begin(); it != end; ++it)
	{
		it->width = width;
	}
}


void Track::addSegment(const TrackSegment& seg)
{
	int index = seg.getIndex();
//	index = index % length;
	int size = segments.size();

	if (size > (index + MAX_VIEW_DISTANCE))
	{
		TrackSegment& existingSeg = segments[index + MAX_VIEW_DISTANCE];
//		if (existingSeg.getIndex() == -1)
//			existingSeg = seg;
//		else
//			merge(existingSeg, seg);

		existingSeg = seg;
	}
	else
	{
		int itemsToAdd = (index + MAX_VIEW_DISTANCE - size);
		if(itemsToAdd > 0)
			segments.insert(segments.end(), itemsToAdd, TrackSegment());

		segments.push_back(seg);
	}
}


void Track::addHeading(const Heading& h)
{
	int size = headings.size();
	if (size > h.index)
	{
		headings[h.index] = h;
	}
	else
	{
		Heading last = headings.empty() ? Heading() : Heading(-1, headings.back().rotation, headings.back().width);
		headings.insert(headings.end(), (h.index - size), last);
		headings.push_back(h);
	}
}


const TrackSegment& Track::getSegment(int index) const
{
//	index = index % length;
	int size = segments.size();

	if (size > (index + MAX_VIEW_DISTANCE))
	{
		return segments[index + MAX_VIEW_DISTANCE];
	}
	else
	{
		segments.insert(segments.end(), (index + MAX_VIEW_DISTANCE - size + 1), TrackSegment());
		return segments[index];
	}

}


int Track::getLength() const
{
	return this->length;
}

Track::iterator	Track::begin()
{
	return this->segments.begin();
}

Track::const_iterator Track::begin() const
{
	return this->segments.begin();
}

Track::iterator	Track::end()
{
	return this->segments.end();
}

Track::const_iterator Track::end() const
{
	return this->segments.end();
}


void Track::update(CarState& cs)
{
	this->headingFinder->update(cs);

	double distanceFromStart = cs.getDistFromStart();

	if( isNewLap(distanceFromStart) )
	{
		this->length = ceil(prevDistanceFromStart);

		this->headingFinder->generateHeadings(this->length);
		this->fixMisingTrackWidthAtBeginning();

//		correctSegmentsAtTrackEnd();
//		correctHeadingsAtTrackEnd();

		if((int)segments.size() > length)
			segments.erase((segments.begin() + length), segments.end());

		if((int)headings.size() > length)
			headings.erase((headings.begin() + length), headings.end());

//		recalculateHeadings();
		buildTrackFromHeadings();
		findBends();

		dump();
		//dumpTrack();


		++lap;
		if(lap < 2)
			this->headings.clear();
		else
			this->loaded = true;
	}

	this->prevDistanceFromStart = distanceFromStart;
}


Track::Bend& Track::getNextBend(int distaceFromStart)
{
	std::vector<Bend>::iterator it = bends.begin();
	std::vector<Bend>::iterator end = bends.end();
	for(; it != end; ++it)
	{
		if(it->getEnd() > distaceFromStart)
			return *it;
	}

	return bends.front();
}

const Track::Bend& Track::getNextBend(int distaceFromStart) const
{
	std::vector<Bend>::const_iterator it = bends.begin();
	std::vector<Bend>::const_iterator end = bends.end();
	for(; it != end; ++it)
	{
		if(it->getEnd() > distaceFromStart)
			return *it;
	}

	return bends.front();
}


Track::Bend& Track::getPrevBend(int distaceFromStart)
{
	std::vector<Bend>::reverse_iterator it = bends.rbegin();
	std::vector<Bend>::reverse_iterator end = bends.rend();
	for(; it != end; ++it)
	{
		if(it->getStart() < distaceFromStart)
			return *it;
	}

	return bends.back();
}


const Track::Bend& Track::getPrevBend(int distaceFromStart) const
{
	std::vector<Bend>::const_reverse_iterator it = bends.rbegin();
	std::vector<Bend>::const_reverse_iterator end = bends.rend();
	for(; it != end; ++it)
	{
		if(it->getStart() < distaceFromStart)
			return *it;
	}

	return bends.back();
}



int	Track::getLap() const
{
	return this->lap;
}

void Track::registerInitCallback(InitCallback* cb)
{
	this->initCallbacks.insert(cb);
}

void Track::registerDumpCallback(DumpCallback* cb)
{
	this->dumpCallbacks.insert(cb);
}


std::vector<Track::Bend>::iterator Track::beginBends()
{
	return this->bends.begin();
}

std::vector<Track::Bend>::iterator Track::endBends()
{
	return this->bends.end();
}





void Track::merge(TrackSegment& existingSeg, const TrackSegment& newSegment)
{
//std::cout << "merge existing seg with index " << existingSeg.getIndex() << " right point x " << existingSeg.getRight().x << " y " << existingSeg.getRight().y << " left point x " << existingSeg.getLeft().x << " y " << existingSeg.getLeft().y << " center x " << existingSeg.getMiddle().x << " y " << existingSeg.getMiddle().y<< std::endl;
//std::cout << "merge new seg with index "<< newSegment.getIndex()<< " right point x " << newSegment.getRight().x << " y " << newSegment.getRight().y << " left point x " << newSegment.getLeft().x << " y " << newSegment.getLeft().y << " center x " << newSegment.getMiddle().x << " y " << newSegment.getMiddle().y<< std::endl;

	glm::vec2 newPointR = (existingSeg.getRight() + newSegment.getRight()) / 2.0f;
	glm::vec2 newPointM = (existingSeg.getMiddle() + newSegment.getMiddle()) / 2.0f;
	glm::vec2 newPointL = (existingSeg.getLeft() + newSegment.getLeft()) / 2.0f;

	double rotationExisting = existingSeg.getRotation();
	double rotationNew = newSegment.getRotation();

	// correction for 2 PI near angles
	double correctionAngle = 0;
	if (abs(rotationExisting - rotationNew) > (2 * PI - DELTA))
		correctionAngle = 2 * PI;

	double rotation = (rotationExisting + rotationNew + correctionAngle) / 2.0;
	rotation = fmod(rotation, 2 * PI);

	existingSeg = TrackSegment(existingSeg.getIndex(), newPointR, newPointL, newPointM, rotation);
	//std::cout << "after merge  seg right point x " << existingSeg.getRight().x << " y " << existingSeg.getRight().y << " left point x " << existingSeg.getLeft().x << " y " << existingSeg.getLeft().y << " center x "<< newSegment.getMiddle().x << " y " << newSegment.getMiddle().y<< std::endl;
}


void Track::correctSegmentsAtTrackEnd()
{
	int last = this->segments.size();
	for(int i = length + MAX_VIEW_DISTANCE ; i < last; ++i )
	{
		int index = i - (length + MAX_VIEW_DISTANCE);

		if(segments[i].getIndex() != TrackSegment::UNSET_INDEX)
		{
			TrackSegment s = segments[i];
			s.setIndex(index);
			this->addSegment(s);
		}
	}
}

void Track::correctHeadingsAtTrackEnd()
{
	int last = this->headings.size();
	for(int i = length ; i < last; ++i )
	{
		int index = i - length;

		if(headings[i].index != -1)
		{
			Heading h = headings[i];
			h.index = index;
			headings[index] = h;
		}
	}
}

void Track::recalculateHeadings()
{
	double totalHeadingChange = getTotalHeadingChange();
	double correction = (2 * PI) / fabs(totalHeadingChange);

//	std::cout << "totalHeadingChange = " << totalHeadingChange << std::endl;
//	std::cout << "heading correction = " << correction << std::endl;

	double prevRotation = 0.0;

	std::vector<Heading>::iterator it = headings.begin();
	std::vector<Heading>::iterator end = headings.end();
	if(it != end)
	{
		prevRotation = it->rotation * correction;
		it->rotation = prevRotation;

		++it;
	}
	else
	{
		return;
	}


	for(; it != end; ++it)
	{
		double currentRotation = it->rotation;
		it->rotation = normalizeAngle((it - 1)->rotation + getAngleDiff(currentRotation, prevRotation) * correction);
		prevRotation = currentRotation;
	}
}

double Track::getTotalHeadingChange()
{
	double change = 0.0;

	std::vector<Heading>::iterator it = headings.begin();
	std::vector<Heading>::iterator end = headings.end();

	if(it != end)
		++it;
	else
		return change;

	for(; it != end; ++it)
	{
		if(it->index == -1)
			*it = Heading((it - 1)->index + 1, (it - 1)->rotation, (it - 1)->width);

		double diff = getAngleDiff(it->rotation, (it - 1)->rotation);
		change += diff;
	}

	return change;
}

void Track::buildTrackFromHeadings()
{
	this->segments.clear();

	if(headings.empty())
		return;

	glm::vec2 currentCenter(-1.0, 0.0);

	std::vector<Heading>::iterator beg = headings.begin();
	std::vector<Heading>::iterator it = headings.begin();
	std::vector<Heading>::iterator end = headings.end();

	for(; it != end; ++it)
	{
		int index = std::distance(beg, it);

		if(index > this->length)
			break;

		double heading = it->rotation;
		double width = it->width;

		TrackSegment seg;
		seg.setIndex(index);

		currentCenter = generateNextSegment(currentCenter, heading, width, seg);

//		std::cout << "Segment " << seg << std::endl;

		addSegment(seg);
	}


	// Append from first until max view distance
	double heading = 0.0;
	if((int)headings.size() > length)
		heading = headings[length - 1].rotation;
	else
		heading = headings.back().rotation;

	double prevHeading = headings.front().rotation;

	for(int i = 0; i < MAX_VIEW_DISTANCE; ++i)
	{
		if((int)headings.size() > i)
		{
			Heading& h = headings[i];
			double headingDiff = getAngleDiff(h.rotation, prevHeading);
			prevHeading = h.rotation;

			heading -= headingDiff;

			double width = h.width;

			TrackSegment seg;
			seg.setIndex(i + length);

			currentCenter = generateNextSegment(currentCenter, heading, width, seg);

//			std::cout << "Segment " << seg << std::endl;

			addSegment(seg);
		}
		else
		{
			break;
		}
	}

	// Add segments from first backwords
	heading = headings.front().rotation + PI;

	if((int)headings.size() > length)
		prevHeading = headings[length].rotation;
	else
		prevHeading = headings.back().rotation;

	currentCenter = getSegment(0).getMiddle();

	//std::cout << "Generating track backward: center " << currentCenter << ", heading " << heading << ", headings " << headings.size() << ", length " << length << std::endl;

	for(int i = 1; i < MAX_VIEW_DISTANCE; ++i)
	{
		if((int)headings.size() > i)
		{
			Heading& h = headings[length - i];
			double headingDiff = getAngleDiff(prevHeading, h.rotation);
			prevHeading = h.rotation;

			heading -= headingDiff;

			double width = h.width;

			TrackSegment seg;
			seg.setIndex(-i);

			currentCenter = generateNextSegment(currentCenter, heading, width, seg);

//			std::cout << "Segment " << seg << std::endl;

			addSegment(seg);
		}
		else
		{
			break;
		}
	}
}


glm::vec2 Track::generateNextSegment(const glm::vec2& currentCenter, double heading, double width, TrackSegment& seg)
{
	glm::vec2 headingUnitVec = glm::normalize(glm::vec2(cos(heading), sin(heading)));
	glm::vec2 nextCenter = headingUnitVec + currentCenter;

	double perpendicularAngle = heading + (PI / 2);
	glm::vec2 perpendicularUnitVec = glm::normalize(glm::vec2(cos(perpendicularAngle), sin(perpendicularAngle)));

	glm::vec2 nextLeft = (perpendicularUnitVec * (float)width / 2.0f) + nextCenter;
	glm::vec2 nextRight = (perpendicularUnitVec * -(float)width / 2.0f) + nextCenter;

	seg.setMiddle(nextCenter);
	seg.setLeft(nextLeft);
	seg.setRight(nextRight);
	seg.setRotation(heading);

	return nextCenter;
}



//void Track::findBends()
//{
//	bends.clear();
//
//	double headingWindowPrev[HEADING_WINDOW_SIZE];
//	double headingWindowNow[HEADING_WINDOW_SIZE];
//	for(int i = 0; i < HEADING_WINDOW_SIZE; ++i)
//	{
//		headingWindowPrev[i] = 0.0;
//		headingWindowNow[i] = 0.0;
//	}
//
//	int headingWindowIndex = 0;
//
//
//	bool inBend = false;
//
//	int	bendStart = 0;
//	double startHeading = 0.0;
//
//
//	std::vector<Heading>::iterator it = headings.begin();
//	std::vector<Heading>::iterator end = headings.end();
//	if(it == end)
//	{
//		bends.push_back(Bend(0, 0, 0.0));
//		return;
//	}
//
//	std::vector<Heading>::iterator prev = it;
//	++it;
//
//	for(; it != end; ++it)
//	{
//		double headingChange = getAngleDiff(prev->rotation, it->rotation);
//		std::cout << "heading change " << headingChange << std::endl;
//		prev = it;
//
//		int winIndex = headingWindowIndex % HEADING_WINDOW_SIZE;
//		++headingWindowIndex;
//
//		if(headingWindowIndex > HEADING_WINDOW_SIZE)
//		{
//			headingWindowPrev[winIndex] = headingWindowNow[winIndex];
//			headingWindowNow[winIndex] = headingChange;
//		}
//		else
//		{
//			headingWindowPrev[winIndex] = headingChange;
//			headingWindowNow[winIndex] = headingChange;
//		}
//
//
//		double headingAvgPrev = std::accumulate(headingWindowPrev, headingWindowPrev + HEADING_WINDOW_SIZE, headingAvgPrev) / HEADING_WINDOW_SIZE;
//		double headingAvgNow = std::accumulate(headingWindowNow, headingWindowNow + HEADING_WINDOW_SIZE, 0.0) / HEADING_WINDOW_SIZE;
//
//		double cumulativeHeadingChange = fabs(headingAvgNow - headingAvgPrev);
//		std::cout << "Cumulative heading change = " << std::fixed << cumulativeHeadingChange << std::endl;
//
//		if(cumulativeHeadingChange > MIN_HEADING_CHANGE_ON_BEND)
//		{
//			if(!inBend)
//			{
//				// Start of a bend
//				inBend = true;
//				bendStart = it->index > HEADING_WINDOW_SIZE ? it->index - HEADING_WINDOW_SIZE : 0;
//
//				int startWindowIndex = (winIndex + HEADING_WINDOW_SIZE) % HEADING_WINDOW_SIZE;
//				startHeading = headingWindowNow[startWindowIndex];
//			}
//			else
//			{
//				int bendEnd = it->index > HEADING_WINDOW_SIZE ? it->index - HEADING_WINDOW_SIZE : 0;
//
//				int endWindowIndex = (winIndex + HEADING_WINDOW_SIZE) % HEADING_WINDOW_SIZE;
//				double endHeading = headingWindowNow[endWindowIndex];
//
//				double headingChange = getAngleDiff(endHeading, startHeading);
//
//				this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
//
//
//
//				if(abs(headingAvgNow) < MIN_HEADING_CHANGE_ON_BEND)
//					inBend = false; // Straight line
//				else
//					inBend = true; // Another bend
//			}
//		}
//	}
//
//	//
//	// In a bend at the end
//	//
//	if(inBend)
//	{
//		int bendEnd = this->headings.back().index;
//		double endHeading = this->headings.back().rotation;
//
//		double headingChange = getAngleDiff(endHeading, startHeading);
//
//		this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
//	}
//
//	//
//	// Error no bends
//	// Insert full track as a bend
//	//
//	if(this->bends.empty())
//	{
//		int bendStart = 0;
//		int bendEnd = this->headings.size() - 1;
//		double headingChange = 2 * PI;
//
//		this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
//	}
//}


void Track::findBends()
{
	bends.clear();

	static double minHeadingChangeOnBend = 0.001;

	int bendDirection = +1;
	bool inBend = false;

	int	bendStart = 0;
	double startHeading = 0.0;

	int endDetectCount = 0.0;

	std::vector<Heading>::iterator it = headings.begin();
	std::vector<Heading>::iterator end = headings.end();
	if(it == end)
	{
		bends.push_back(Bend(0, 0, 0.0));
		return;
	}

	std::vector<Heading>::iterator prev = it;
	++it;
	for(; it != end; ++it)
	{
		double headingChange = getAngleDiff(prev->rotation, it->rotation);
		if(abs(headingChange) > minHeadingChangeOnBend)
		{
			// Bend
			if(!inBend)
			{
				bendDirection = headingChange / fabs(headingChange);
				inBend = true;

				bendStart = prev->index;
				startHeading = prev->rotation;

				endDetectCount = 0;
			}
			else
			{
				//
				// Detect consecutive other side bend
				//
				int newBendDirection = headingChange / fabs(headingChange);
				if(newBendDirection != bendDirection)
				{
					if(endDetectCount > MIN_BEND_END_DETECT_COUNT)
					{
						int bendEnd = it->index;
						if(bendEnd - bendStart > MIN_BEND_LENGTH)
						{
							double endHeading = it->rotation;

							double headingChange = getAngleDiff(endHeading, startHeading);

							this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
						}

						bendDirection = newBendDirection;

						bendStart = it->index;
						startHeading = it->rotation;

						endDetectCount = 0;
					}
					else
					{
						++endDetectCount;
					}
				}
				else
				{
					int bendLength = it->index - bendStart;
					if(bendLength >= MAX_BEND_LENGTH)	// Split bends in max bend length
					{
						int bendEnd = it->index;
						double endHeading = it->rotation;

						double headingChange = getAngleDiff(endHeading, startHeading);

						this->bends.push_back(Bend(bendStart, bendEnd, headingChange));


						bendStart = it->index;
						startHeading = it->rotation;

						endDetectCount = 0;
					}
				}
			}
		}
		else
		{
			// Not a bend
			if(inBend)
			{
				if(endDetectCount > MIN_BEND_END_DETECT_COUNT)
				{
					inBend = false;

					int bendEnd = it->index;
					if(bendEnd - bendStart > MIN_BEND_LENGTH)
					{
						double endHeading = it->rotation;

						double headingChange = getAngleDiff(endHeading, startHeading);

						this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
					}
				}
				else
				{
					++endDetectCount;
				}
			}
		}

		prev = it;
	}

	//
	// In a bend at the end
	//
	if(inBend)
	{
		int bendEnd = this->headings.back().index;
		double endHeading = this->headings.back().rotation;

		double headingChange = getAngleDiff(endHeading, startHeading);

		this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
	}

	//
	// Error no bends
	// Insert full track as a bend
	//
	if(this->bends.empty())
	{
		int bendStart = 0;
		int bendEnd = this->headings.size() - 1;
		double headingChange = 2 * PI;

		this->bends.push_back(Bend(bendStart, bendEnd, headingChange));
	}
}


bool Track::isNewLap(double newDistanceFromStart) const
{
	return ((abs(newDistanceFromStart - prevDistanceFromStart) > MIN_TRACK_LENGTH) &&
			(abs(newDistanceFromStart) < LAP_START_SEGMENT_LENGTH));
}









void Track::dumpTrack() const
{
	std::stringstream trackFileNameStream;
	trackFileNameStream << this->name << "_" << lap << ".svg";
	std::ofstream trackFile(trackFileNameStream.str().c_str());

	trackFile << SVG_HEADER << std::endl;
	trackFile << SVG(*this) << std::endl;
	trackFile << SVG_FOOTER << std::endl;
}




std::ostream& operator<<(std::ostream& out, const Track& track)
{
	std::copy(track.segments.begin(), track.segments.end(), std::ostream_iterator<TrackSegment>(out, "\n"));
	return out;
}







/**************************************************************************************************
 * Bend
 **************************************************************************************************/
Track::Bend::Bend():
		start(0), end(0), headingChange(0), userData(NULL)
{
}

Track::Bend::Bend(int start, int end, double headingChange) :
		start(start), end(end), headingChange(headingChange), userData(NULL)
{
}

Track::Bend::Bend(const Bend& other):
		start(other.start), end(other.end), headingChange(other.headingChange),
		userData(other.userData ? other.userData->clone() : NULL)
{
}


Track::Bend& Track::Bend::operator=(const Bend& other)
{
	this->start = other.start;
	this->end = other.end;
	this->headingChange = other.headingChange;
	this->userData = other.userData ? other.userData->clone() : NULL;

	return *this;
}


Track::Bend::~Bend()
{
	delete this->userData;
}

int	Track::Bend::getStart() const
{
	return this->start;
}

void Track::Bend::setStart(int start)
{
	this->start = start;
}

int	Track::Bend::getEnd() const
{
	return this->end;
}

void Track::Bend::setEnd(int end)
{
	this->end = end;
}

double Track::Bend::getHeadingChange() const
{
	return this->headingChange;
}

void Track::Bend::setHeadingChange(double headingChange)
{
	this->headingChange = headingChange;
}


double 	Track::Bend::getRadius() const
{
	if(this->start != this->end)
		return fabs((this->end - this->start) / this->headingChange);
	else
		return 0.0;
}

Track::BendUserData* Track::Bend::getUserData()
{
	return this->userData;
}

const Track::BendUserData* Track::Bend::getUserData() const
{
	return this->userData;
}

void Track::Bend::setUserData(Track::BendUserData* userData)
{
	this->userData = userData;
}





















