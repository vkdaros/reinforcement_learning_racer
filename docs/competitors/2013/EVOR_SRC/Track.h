/*
 * Track.h
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#ifndef TRACK_H_
#define TRACK_H_

#include "TrackSegment.h"

#include "CarState.h"
#include "VisibleTrack.h"
#include "Line.h"
#include "SensorArray.h"
#include "Defs.h"
#include "HeadingFinder.h"

#include <vector>
#include <set>

#define	MAX_VIEW_DISTANCE						200
#define MIN_TRACK_LENGTH						1000
#define LAP_START_SEGMENT_LENGTH				10


class Car;
class Track
{
public:
	typedef  	std::vector<TrackSegment>::iterator			iterator;
	typedef 	std::vector<TrackSegment>::const_iterator	const_iterator;

	class BendUserData
	{
	public:
		virtual ~BendUserData() {};
		virtual BendUserData*	clone() const = 0;
	};

	class Bend
	{
	public:
		Bend();
		Bend(const Bend& other);
		Bend& operator=(const Bend& other);
		Bend(int start, int end, double headingChange);
		~Bend();

		int		getStart() const;
		void	setStart(int start);

		int		getEnd() const;
		void	setEnd(int end);

		double	getHeadingChange() const;
		void	setHeadingChange(double headingChange);

		double 	getRadius() const;

		BendUserData*		getUserData();
		const BendUserData*	getUserData() const;
		void				setUserData(BendUserData* userData);

	private:
		int				start;
		int				end;
		double			headingChange;

		BendUserData*	userData;
	};


	class InitCallback
	{
	public:
		virtual ~InitCallback() {};
		virtual void init(Track* track) = 0;
	};


	class DumpCallback
	{
	public:
		virtual ~DumpCallback() {};
		virtual void dump(Track* track) = 0;
	};


	Track(Car* car);
	Track(const Track& other);
	Track& operator=(const Track& other);
	virtual ~Track();

	void					init(const std::string& name);
	void					dump();

	bool					isLoaded() const;

	std::string				getName() const;

	const TrackSegment& 	getSegment(int index) const;
	int 					getLength() const;

	iterator				begin();
	const_iterator			begin() const;

	iterator				end();
	const_iterator			end() const;

	void					update(CarState& cs);

	Bend&					getNextBend(int distaceFromStart);
	const Bend&				getNextBend(int distaceFromStart) const;

	Bend&					getPrevBend(int distaceFromStart);
	const Bend&				getPrevBend(int distaceFromStart) const;

	int						getLap() const;

	void					registerInitCallback(InitCallback* cb);
	void					registerDumpCallback(DumpCallback* cb);

	std::vector<Bend>::iterator		beginBends();
	std::vector<Bend>::iterator		endBends();

private:

	friend class			NormalHeadingFinder;
	friend class			NoisyHeadingFinder;

	friend std::ostream& 	operator<<(std::ostream& out,const Track& track);


	struct Heading
	{
		Heading(): index(-1), rotation(0.0), width(0.0) {};
		Heading(int index, double rotation, double width): index(index), rotation(rotation), width(width) {};

		int			index;
		double		rotation;
		double 		width;
	};


	void 					addSegment(const TrackSegment& t);
	void					addHeading(const Heading& h);

	bool					isNewLap(double newDistanceFromStart) const;
	void 					merge(TrackSegment& existingSeg, const TrackSegment& newSegment);
	void 					correctSegmentsAtTrackEnd();
	void 					correctHeadingsAtTrackEnd();
	void					recalculateHeadings();
	double					getTotalHeadingChange();
	void					buildTrackFromHeadings();
	glm::vec2				generateNextSegment(const glm::vec2& currentCenter, double heading, double width, TrackSegment& seg);

	void					findBends();
	void					fixMisingTrackWidthAtBeginning();


	void					dumpTrack() const;

	Car*									car;

	std::string								name;

	bool									loaded;

	int 									length;
	mutable std::vector<TrackSegment> 		segments;
	std::vector<Heading>					headings;
	std::vector<Bend>						bends;

	int										lap;
	double									prevDistanceFromStart;

	HeadingFinder*							headingFinder;

	std::set<InitCallback*>					initCallbacks;
	std::set<DumpCallback*>					dumpCallbacks;
};


std::ostream& operator<<(std::ostream& out,const Track& track);


#endif /* TRACK_H_ */
