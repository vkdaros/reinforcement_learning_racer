/*
 * TrackSegment.cpp
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#include "TrackSegment.h"

#include <limits>


int TrackSegment::UNSET_INDEX = std::numeric_limits<int>::max();

TrackSegment::TrackSegment():
		index(UNSET_INDEX), rotation(0)
{
}


TrackSegment::TrackSegment(int index, glm::vec2 right, glm::vec2 left, glm::vec2 middle, double rotation):
		index(index), right(right),left(left), middle(middle), rotation(rotation)
{
}

TrackSegment::~TrackSegment()
{
}

int TrackSegment::getIndex() const
{
	return index;
}

void TrackSegment::setIndex(int index)
{
	this->index = index;
}

const glm::vec2& TrackSegment::getLeft() const
{
	return left;
}

void TrackSegment::setLeft(const glm::vec2& left)
{
	this->left = left;
}

const glm::vec2& TrackSegment::getMiddle() const
{
	return middle;
}

void TrackSegment::setMiddle(const glm::vec2& middle)
{
	this->middle = middle;
}

const glm::vec2& TrackSegment::getRight() const
{
	return right;
}

void TrackSegment::setRight(const glm::vec2& right)
{
	this->right = right;
}

double TrackSegment::getRotation() const
{
	return rotation;
}

void TrackSegment::setRotation(double rotation)
{
	this->rotation = rotation;
}


std::ostream& operator<<(std::ostream& out,const TrackSegment& segment)
{
	if(segment.getIndex() == TrackSegment::UNSET_INDEX)
		return out;

	glm::vec2 rPoint = segment.getRight();
	glm::vec2 lPoint = segment.getLeft();
	glm::vec2 mPoint = segment.getMiddle();

	out << segment.getIndex() << ": " << rPoint.x << ", " << rPoint.y << " | " << mPoint.x << ", " << mPoint.y << " | " << lPoint.x << ", " << lPoint.y << " | " << segment.getRotation();

	return out;
}

float TrackSegment::getTrackWidth() const
{

				return glm::length(left-right);
}
