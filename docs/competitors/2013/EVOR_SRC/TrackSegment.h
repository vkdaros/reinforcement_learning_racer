/*
 * TrackSegment.h
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#ifndef TRACKSEGMENT_H_
#define TRACKSEGMENT_H_

#include "glm/glm.hpp"

#include <ostream>

class TrackSegment
{
public:
	static int	UNSET_INDEX;

	TrackSegment();
	TrackSegment(int index, glm::vec2 right, glm::vec2 left, glm::vec2 middle, double rotation);
	virtual ~TrackSegment();

	int 				getIndex() const;
	void 				setIndex(int index);

	const glm::vec2& 	getLeft() const;
	void 				setLeft(const glm::vec2& left);

	const glm::vec2& 	getMiddle() const;
	void 				setMiddle(const glm::vec2& middle);

	const glm::vec2& 	getRight() const;
	void 				setRight(const glm::vec2& right);

	double 				getRotation() const;
	void 				setRotation(double rotation);

	float 				getTrackWidth() const;

private:

	int 		index;
	glm::vec2 	right;
	glm::vec2 	left;
	glm::vec2 	middle;
	double 		rotation;
};

std::ostream& operator<<(std::ostream& out,const TrackSegment& segment);

#endif /* TRACKSEGMENT_H_ */
