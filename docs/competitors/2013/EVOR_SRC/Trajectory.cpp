/*
 * Trajectory.cpp
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#include "Trajectory.h"

Trajectory::Trajectory():
		type(NONE), radius(0.0), m(0.0), c(0.0),isStraight(false),isReverse(false),isRecovery(false)
{
}

Trajectory::Trajectory(const glm::vec2& center, double radius):
		type(CIRCLE), center(center), radius(radius), m(0.0), c(0.0),isStraight(false),isReverse(false),isRecovery(false)
{
}

Trajectory::Trajectory(double m, double c):
		type(LINE), radius(0.0), m(m), c(c),isStraight(false),isReverse(false),isRecovery(false)
{
}


Trajectory::Trajectory(const glm::vec2& from, const glm::vec2& to):
		type(BOUNDED_LINE), radius(0.0), m(0.0), c(0.0), from(from), to(to),isStraight(false),isReverse(false),isRecovery(false)
{
}

Trajectory::~Trajectory()
{
}


bool Trajectory::isLine() const
{
	return (this->type == LINE);
}

bool Trajectory::isCircle() const
{
	return (this->type == CIRCLE);
}

bool Trajectory::isBoundedLine() const
{
	return (this->type == BOUNDED_LINE);
}

bool Trajectory::isLineStrip() const
{
	return (this->type == LINE_STRIP);
}


const glm::vec2&  Trajectory::getCenter() const
{
	return this->center;
}

double 	Trajectory::getRadius() const
{
	return this->radius;
}

void Trajectory::setCircle(const glm::vec2& center, double radius)
{
	this->center = center;
	this->radius = radius;

	this->m = 0;
	this->c = 0;

	this->type = CIRCLE;
}



double Trajectory::getM() const
{
	return this->m;
}

double Trajectory::getC() const
{
	return this->c;
}


void Trajectory::setLine(double m, double c)
{
	this->center = glm::vec2();
	this->radius = 0.0;

	this->m = m;
	this->c = c;

	this->type = LINE;
}



std::pair<glm::vec2, glm::vec2>	Trajectory::getBoundedLine() const
{
	return std::make_pair(from, to);
}


void Trajectory::setBoundedLine(const glm::vec2& from, const glm::vec2& to)
{
	this->from = from;
	this->to = to;

	this->type = BOUNDED_LINE;
}



std::vector<glm::vec2>::iterator Trajectory::beginLineStrip()
{
	return this->lineStrip.begin();
}

std::vector<glm::vec2>::const_iterator Trajectory::beginLineStrip() const
{
	return this->lineStrip.begin();
}

std::vector<glm::vec2>::iterator Trajectory::endLineStrip()
{
	return this->lineStrip.end();
}

std::vector<glm::vec2>::const_iterator Trajectory::endLineStrip() const
{
	return this->lineStrip.end();
}

void Trajectory::appendLineStrip(const glm::vec2& point)
{
	this->lineStrip.push_back(point);
	this->type = LINE_STRIP;
}

void Trajectory::setLineStrip(const std::vector<glm::vec2>& strip)
{
	this->lineStrip = strip;
	this->type = LINE_STRIP;
}




std::ostream& operator<<(std::ostream& out,const Trajectory& trajectory)
{
	if(trajectory.isLine())
	{
		double m = trajectory.getM();
		double c = trajectory.getC();

		out << "1, " << m << ", " << c;
	}
	else if(trajectory.isCircle())
	{
		const glm::vec2& c = trajectory.getCenter();
		double r = trajectory.getRadius();

		out << "0, " << c.x << ", " << c.y << ", " << r;
	}
	else if(trajectory.isBoundedLine())
	{
		std::pair<glm::vec2, glm::vec2> line = trajectory.getBoundedLine();
		out << "12, " << line.first.x << ", " << line.first.y << ", " << line.second.x << ", " << line.second.y << std::endl;
	}
	else if(trajectory.isLineStrip())
	{
		out << "13";
		std::vector<glm::vec2>::const_iterator it = trajectory.beginLineStrip();
		std::vector<glm::vec2>::const_iterator end = trajectory.endLineStrip();
		for(; it != end; ++it)
			out << ", " << it->x << ", " << it->y;
	}

	return out;

}
