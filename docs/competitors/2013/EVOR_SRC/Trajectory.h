/*
 * Trajectory.h
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */


#ifndef TRAJECTORY_H_
#define TRAJECTORY_H_

#include "glm/glm.hpp"

#include <vector>
#include <iostream>

class Trajectory
{
public:
	Trajectory();
	Trajectory(const glm::vec2& center, double radius);
	Trajectory(double m, double c);
	Trajectory(const glm::vec2& from, const glm::vec2& to);
	virtual ~Trajectory();

	bool 				isLine() const;
	bool				isCircle() const;
	bool				isBoundedLine() const;
	bool				isLineStrip() const;


	const glm::vec2& 	getCenter() const;
	double 				getRadius() const;
	void 				setCircle(const glm::vec2& center, double radius);

	double				getM() const;
	double				getC() const;
	void				setLine(double m, double c);

	std::pair<glm::vec2, glm::vec2>	getBoundedLine() const;
	void							setBoundedLine(const glm::vec2& from, const glm::vec2& to);

	std::vector<glm::vec2>::iterator		beginLineStrip();
	std::vector<glm::vec2>::const_iterator	beginLineStrip() const;

	std::vector<glm::vec2>::iterator		endLineStrip();
	std::vector<glm::vec2>::const_iterator	endLineStrip() const;

	void									appendLineStrip(const glm::vec2& point);
	void									setLineStrip(const std::vector<glm::vec2>& strip);

	bool isIsReverse() const {
		return isReverse;
	}

	void setIsReverse(bool isReverse) {
		this->isReverse = isReverse;
	}

	bool isIsRecovery() const {
		return isRecovery;
	}

	void setIsRecovery(bool isRecovery) {
		this->isRecovery = isRecovery;
	}

	bool isIsStraight() const {
		return isStraight;
	}

	void setIsStraight(bool isStraight) {
		this->isStraight = isStraight;
	}

	bool isIsFullRecovery() const
	{
		return isFullRecovery;
	}

	void setIsFullRecovery(bool isFullRecovery)
	{
		this->isFullRecovery = isFullRecovery;
	}

	int getSide() const
	{
		return side;
	}

	void setSide(int side)
	{
		this->side = side;
	}

private:

	enum Type
	{
		NONE,
		LINE,
		CIRCLE,
		BOUNDED_LINE,
		LINE_STRIP,
	};

	Type		type;

	glm::vec2 	center;		// circle
	double 		radius;

	double 		m, c;		// line

	glm::vec2	from, to;	// bounded line

	std::vector<glm::vec2>	lineStrip;	// line strip

	bool isStraight;
	bool isReverse;
	bool isRecovery;
	bool isFullRecovery;
	int side;

};

std::ostream& operator<<(std::ostream& out,const Trajectory& trajectory);

#endif /* TRAJECTORY_H_ */
