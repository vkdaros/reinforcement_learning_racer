/*
 * VisibleTrack.cpp
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#include "VisibleTrack.h"

VisibleTrack::VisibleTrack()
{
}

VisibleTrack::~VisibleTrack()
{
}

std::ostream& operator<<(std::ostream& out,const VisibleTrack& track)
{
	std::deque<glm::vec2>::const_iterator it =  track.getLeftEdge().begin();
	std::deque<glm::vec2>::const_iterator itEnd =  track.getLeftEdge().end();
	for(; it != itEnd; ++it)
	{
		out << it->x << " , " << it->y << std::endl;
	}
	it =  track.getRightEdge().begin();
	itEnd =  track.getRightEdge().end();
	for(; it != itEnd; ++it)
	{
		out << it->x << " , " << it->y << std::endl;
	}

	std::deque<CenterPoint>::const_iterator itC =  track.getCenterAxis().begin();
	std::deque<CenterPoint>::const_iterator itCEnd =  track.getCenterAxis().end();

	for(; itC != itCEnd; ++itC)
	{
		out << itC->point.x << " , " << itC->point.y << std::endl;
	}

	return out;
}

void VisibleTrack::addCenterPoint(CenterPoint& p)
{
	std::deque<CenterPoint>::iterator it = centerAxis.begin();
	std::deque<CenterPoint>::iterator itEnd = centerAxis.end();
	for(; it != itEnd; ++it)
	{

		if(p.point.x <  it->point.x)
		{

			break;
		}
	}

	centerAxis.insert(it,p);

}
