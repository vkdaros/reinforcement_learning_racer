/*
 * VisibleTrack.h
 *
 *  Created on: 16/04/2013
 *      Author: sam
 */

#ifndef VISIBLETRACK_H_
#define VISIBLETRACK_H_
#include <deque>
#include <glm/glm.hpp>

#include <ostream>
 struct CenterPoint
   {
	   CenterPoint(const CenterPoint& point){this->point = point.point; this->rEdgeIndex = point.rEdgeIndex; this->lEdgeIndex = point.lEdgeIndex;};
	  CenterPoint(){this->lEdgeIndex = 0; this->rEdgeIndex = 0;};
	   glm::vec2 point;
	   int lEdgeIndex;
	   int rEdgeIndex;
   };


class VisibleTrack
{
public:
	VisibleTrack();
	virtual ~VisibleTrack();

//	float getDistanceFromCenterToLeft() const
//	{
//		return distanceFromCenterToLeft;
//	}
//
//	void setDistanceFromCenterToLeft(float distanceFromCenterToLeft)
//	{
//		this->distanceFromCenterToLeft = distanceFromCenterToLeft;
//	}
//
//	float getDistanceFromCenterToRight() const
//	{
//		return distanceFromCenterToRight;
//	}
//
//	void setDistanceFromCenterToRight(float distanceFromCenterToRight)
//	{
//		this->distanceFromCenterToRight = distanceFromCenterToRight;
//	}

	const std::deque<glm::vec2>& getLeftEdge() const
	{
		return leftEdge;
	}

	void setLeftEdge(const std::deque<glm::vec2>& leftEdge)
	{
		this->leftEdge = leftEdge;
	}

	const std::deque<glm::vec2>& getRightEdge() const
	{
		return rightEdge;
	}

	void setRightEdge(const std::deque<glm::vec2>& rightEdge)
	{
		this->rightEdge = rightEdge;
	}

	void addRightTrackSegment(const glm::vec2 vec){rightEdge.push_back(vec);};
	void addLeftTrackSegment(const glm::vec2 vec){leftEdge.push_back(vec);};
	void addCenterPoint(CenterPoint& p);//iterate and store in the sorted location
	glm::vec2& getRightTrackPart(int index){return rightEdge[index];};
	glm::vec2& getLeftTrackPart(int index){return leftEdge[index];};

	const std::deque<CenterPoint>& getCenterAxis() const
	{
		return centerAxis;
	}

	void setCenterAxis(const std::deque<CenterPoint>& centerAxis)
	{
		this->centerAxis = centerAxis;
	}

   std::deque<glm::vec2> leftEdge;
   std::deque<glm::vec2> rightEdge;
   std::deque<CenterPoint> centerAxis;

//   float distanceFromCenterToRight;
//   float distanceFromCenterToLeft;



};

std::ostream& operator<<(std::ostream& out,const VisibleTrack& track);



#endif /* VISIBLETRACK_H_ */
