#ifndef DRIVER_CONSTANTS_H
#define DRIVER_CONSTANTS_H

#include "BaseDriver.h"

// GA
#define NBIND 300
#define MUT_RATE  0.45
#define CROSS_RATE  0.65
#define NBCPU 3
#define CHANGING_POP 0.90
#define NB_COPY_CHANGING_POP 5
#define NB_MUT_CHANGING_POP 75

#define INIT_POP_FROM_GRNFILE 1
#define INIT_GRN_FILES  {"pop_19_max15.6625.grn"} //{"pop_20_max2.56988.grn"} // {"pop_29_max8.34968.grn"} //{"pop_15_max1.92503.grn"} //{"pop_24_max8.26017.grn"}//{"pop_2_max3056.25.grn"} //{"new_grn_16.grn"} //"pop_36_max2509.25.grn"
#define NBCOPY_INIT 5
#define NBMUT_INIT 200

#define ADD_INPUTS false
#define REMOVE_INPUTS false
#define SYMETRIZE_GRN false
#define MODIFY_GRN false

// GRN Size
#define MINGRNSIZE 4
#define MAXGRNSIZE 28 //20
#define NBOUTPUT 4
#define NBINPUT 11
#define DRIVING_NBOUTPUT 4
#define DRIVING_NBINPUT 11
#define GRN_STABILIZE_STEPS 25
#define GRN_STEPS 1
#define GRN_NORMALIZE_MODE 2 //0: Direct output, 1: Old normalization, 2: new normalization
static bool LIMITED_DYNAMIC = false;
static int ID_GAP = 2;

//#define RACE_DIST 14500
#define GAP 500

#define DUAL_GRN false
#define THROTTLE_REGULATION_FACTOR 5.0f
#define THROTTLE_REGULATION_SECTOR_LENGTH 50.0f

// Run mode

//#define IP_RUN "193.49.54.195"
#define IP_RUN "193.49.54.187"
//#define IP_RUN "192.168.0.17"
//#define IP_RUN "172.20.10.12"
//#define IP_RUN "192.168.0.8"

//#define IP_RUN "192.168.1.100"

// Servers
#define NB_SERVER_PER_TRACK 1
#define NB_TRACKS 3
//#define IP_ADDRESSES {"193.49.54.186","193.49.54.192","193.49.54.250","193.49.54.167","193.49.54.173","193.49.54.163","193.49.54.162","193.49.54.166","193.49.54.161","193.49.54.160","193.49.54.159","193.49.54.171","193.49.54.158","193.49.54.157","193.49.54.156"}
//#define IP_ADDRESSES {"193.49.54.195"}
#define IP_ADDRESSES {"193.49.54.123","193.49.54.183","193.49.54.181"}
//#define IP_ADDRESSES {"192.168.0.5","192.168.0.7","192.168.0.9"}

// Agressivity
//#define AGG_FILENAME "aggVector_33_149.772.agg" // Alpine 1
//static string AGG_FILENAME = "aggVector_28_39.872.agg"; //"aggVector_26_42.592.agg"; //"aggVector_13_40.33.agg" // CGSpeedway
//static string AGG_FILENAME = "aggVector_33_55.35.agg"; // CG Track 2

// Opponent avoidance

// Driving
#define STEERING_DIRECT_OUTPUT true
#define DIFFERENTIAL_LOCK 0.25f
#define STEERING_MAX_DELTA 2.0f
#define STEERING_JERKING_STEPS 50
#define STEERING_JERKING_DIST 25.0f
#define STEERING_JERKING_SPEED 0.0f

// Sensors Min Max
#define CONCENTRATION_MAX 0.20f
#define TRACK_MIN 0.0f
#define TRACK_MAX 200.0f
#define SPEEDX_MIN 0.0f
#define SPEEDX_MAX 400.0f
#define SPEEDY_MIN -150.0f
#define SPEEDY_MAX 150.0f
#define STEER_MIN -1.0f
#define STEER_MAX 1.0f

#define STUCK_TIMEOUT 100
#define STEERING_FACTOR 1.0f
#define UNSTUCK_DISTANCE 0.5
#define UNSTUCK_DELTA 0.1

#endif // CONSTANTS_H
