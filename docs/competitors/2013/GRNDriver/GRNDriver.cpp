/***************************************************************************

 file                 : GRNDriver.cpp
 copyright            : (C) 2007 Daniele Loiacono
 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "GRNDriver.h"

#include "DriverConstants.h"

/* Gear Changing Constants*/
const int GRNDriver::gearUp[6]=
{
    9500,9500,9500,9500,9000,0
};
const int GRNDriver::gearDown[6]=
{
    0,4000,6300,7000,7300,7300
};

/* Stuck constants*/
const int GRNDriver::stuckTime = 100000000;
const float GRNDriver::stuckAngle = .523598775; //PI/6

/* Accel and Brake Constants*/
const float GRNDriver::maxSpeedDist=70;
const float GRNDriver::maxSpeed=150;
const float GRNDriver::sin5 = 0.08716;
const float GRNDriver::cos5 = 0.99619;

/* Steering constants*/
const float GRNDriver::steerLock=0.785398;
const float GRNDriver::steerSensitivityOffset=80.0;
const float GRNDriver::wheelSensitivityCoeff=1;

/* ABS Filter Constants */
const float GRNDriver::wheelRadius[4]={0.3179,0.3179,0.3276,0.3276};
const float GRNDriver::absSlip=2.0;
const float GRNDriver::absRange=3.0;
const float GRNDriver::absMinSpeed=3.0;

/* Clutch constants */
const float GRNDriver::clutchMax=0.5;
const float GRNDriver::clutchDelta=0.05;
const float GRNDriver::clutchRange=0.82;
const float GRNDriver::clutchDeltaTime=0.02;
const float GRNDriver::clutchDeltaRaced=10;
const float GRNDriver::clutchDec=0.01;
const float GRNDriver::clutchMaxModifier=1.3;
const float GRNDriver::clutchMaxTime=1.5;

void GRNDriver::setGRN(GRNModel &newModel) {

    model = newModel;
    // initialization of the model
    model.evolve(GRN_STABILIZE_STEPS);
    drivingModel = model;

    distRaced=0;
    lastDistance = 0;
    distRacedOnTrackLine = 0;
    lastDistanceOnTrackLine = 0;
    estimatedLapLength = 0.;
    lastLapDist = 0.;
    
    currentAccel = 0;
    currentBrake = 0;
    currentSteer = 0;
    currentGear = 0;
    currentClutch = 0;

    lastAccel=0;
    lastBrake = 0;
    lastSteer = 0;
    lastGear = 0;
    lastClutch = 0;

    initialized=false;
    fitness=0;
    ontrack=true;
    goodDirection=true;
    reverseDriving = false;
}

void GRNDriver::setAgressivityVector(vector<float> &agg) {
    this->aggressivity=agg;
}


int GRNDriver::getGear(CarState &cs)
{
    /*
     int gear = cs.getGear();
    int rpm  = cs.getRpm();
    if (gear<1)
        return 1;

    if (rpm>9000 && gear <7) {
        return gear+1;
    }
    if (rpm<6000 && gear >2) {
        return gear -1;
    }
    return gear;
    */

    int gear = cs.getGear();
    int rpm  = cs.getRpm();

    // if gear is 0 (N) or -1 (R) just return 1
    if (gear<1)
        return 1;
    // check if the RPM value of car is greater than the one suggested
    // to shift up the gear from the current one
    if (gear <6 && rpm >= gearUp[gear-1])
        return gear + 1;
    else
        // check if the RPM value of car is lower than the one suggested
        // to shift down the gear from the current one
        if (gear > 1 && rpm <= gearDown[gear-1])
            return gear - 1;
        else // otherwhise keep current gear
            return gear;
}

float GRNDriver::getSteer(CarState &cs)
{
    // Only correct output from GRN
    float leftSteer = 0.;
    float rightSteer = 0.;
    float differentialSteer = 0.;
    
    leftSteer = ((Protein)(this->drivingModel.proteins.at(DRIVING_NBINPUT))).concentration;
    rightSteer = ((Protein)(this->drivingModel.proteins.at(DRIVING_NBINPUT+1))).concentration;

    float sum = leftSteer + rightSteer;
    
    if (sum !=0 ) {
            differentialSteer = (leftSteer - rightSteer)/sum;
        }
    else  {
        differentialSteer=0;
    }

    return differentialSteer;
}

float GRNDriver::getAccelAndBrake() {
    // >0 : Accel / <0 : Brake
    // Only correct output
    
    float brake = 0.;
    float accel = 0.;
    float differentialThrust = 0.;

    brake = ((Protein)(this->drivingModel.proteins.at(DRIVING_NBINPUT+2))).concentration;
    accel = ((Protein)(this->drivingModel.proteins.at(DRIVING_NBINPUT+3))).concentration;
    
    float sum = brake + accel;
    
    if (sum!=0) {
        differentialThrust = (accel - brake)/sum;
    } else {
        differentialThrust=0;
    }
    return differentialThrust;
}

CarControl GRNDriver::wDrive(CarState cs) {
    CarControl cc;


    if (!isInitialDistanceFromStartEvaluated) {
        initialDistanceFromStart=cs.getDistFromStart();
        isInitialDistanceFromStartEvaluated=true;
    }

    goodDirection=cs.getAngle()>=-M_PI/2 && cs.getAngle()<=M_PI/2;

    //update distRaced
    distRaced=cs.getDistRaced();
    damage=cs.getDamage();
    lastLapDist=cs.getDistFromStart();
    if (numLap==0 && cs.getDistFromStart()<100) {
        numLap=1;
    } else if (numLap>0 && lastLapTime!=cs.getLastLapTime()) {
        lastLapTime=cs.getLastLapTime();
        numLap++;
        if (lastLapTime<bestLapTime || bestLapTime<1) {
            bestLapTime=lastLapTime;
        }
    }
    
    if (distRaced - lastDistance >= 0.)
    {
        if (estimatedLapLength <= lastLapDist)
        {
            estimatedLapLength = lastLapDist;
        }
        if (numLap == 0)
        {
            distRacedOnTrackLine = distRaced;
        }
        else{
            distRacedOnTrackLine =  -initialDistanceFromStart + estimatedLapLength*numLap + lastLapDist;
        }
    }
    else
    {
        reverseDriving = true;
    }
    
    // Update last values
    lastAccel = currentAccel;
    lastBrake = currentBrake;
    lastSteer = currentSteer;
    lastGear = currentGear;
    lastClutch = currentClutch;
    lastSpeedX = cs.getSpeedX();

    if (RUN_MODE && TRACK_RECOVERY) {
        if ((cs.getTrackPos()<=-1. || cs.getTrackPos() >=1.) || outTrackRecovering || stuckRecovering) //Out of track
        {
            //switch to outTrack
            wrongwayRecovering = false;
            wrongway = 0;

            if (outTrack < STUCK_TIMEOUT)
            {
                ////cerr << "out track ----" << endl;

                cc = outTrackRecovery(cs);
            }
            else //if (outTrack > STUCK_TIMEOUT)
            {
                outTrackRecovering = false; //switch to stuck recovery
                cc = stuckRecovery(cs);
            }
        }
        else if ( !goodDirection|| wrongwayRecovering)
        {
            ////cerr << "worng way recovery ----" << endl;
            cc = wrongWayRecovery(cs);
        }

        if (!outTrackRecovering && !stuckRecovering && !wrongwayRecovering)
        {
            ////cerr << "On Track ----" << endl;

            outTrack = 0;
            stuck = 0;
            wrongway = 0;

            static int stuckontrack = 0;
            if (cs.getSpeedX() < 10.)
            {
                stuckontrack ++;
                if (stuckontrack > 100){
                    outTrack = STUCK_TIMEOUT + 1 ;
                    stuckRecovering = true;
                }
            }
            else{
                stuckontrack = 0;
            }

            cc = GRNDrive(cs);
        }
    }
    else {
        cc = GRNDrive(cs);
    }

    // update current values
    currentAccel = cc.getAccel();
    currentBrake = cc.getBrake();
    currentSteer = cc.getSteer();
    currentGear = cc.getGear();
    currentClutch = cc.getClutch();

    lastCarState = cs;
    steps++;

    //  cout << cc.toString() << endl;

    return cc;
}


CarControl GRNDriver::GRNDrive(CarState cs) {
    //  cout << cs.getDistFromStart() << "   " << distRaced << "   " << initialDistRaced << endl;
    ////cerr << distRaced << endl;

    throttle_regulator = 1.;
    mod_left = 1.; // 4 max left
    mod_right = 1.; //8 max right - 3 still turning

    if (AGGRESSIVE_DRIVE){
        lastSector = cs.getDistFromStart()/AGG_SECTOR_SIZE;

        if (lastSector>aggressivity.size()) {
            aggressivity.push_back(AGG_DEFAULT_VALUE);
        }

        ////cerr << currentSector << "   " << aggressivity[currentSector] << endl;
        if (cs.getSpeedX()-aggressivity[lastSector]<-10) {
            throttle_regulator=0.0;
        } else if (cs.getSpeedX()-aggressivity[lastSector]>10) {
            throttle_regulator=1.0+(cs.getSpeedX()-aggressivity[lastSector])/50;
        } else {
            throttle_regulator=1.0;
        }

        // throttle_regulator = aggressivity[lastSector];
    }

    if (AVOID_OPPONENTS){
        handleOpponents_beta(cs);
    }

    checkNextTurn(cs);

    ////cerr<<throttle_regulator<<"\t"<<mod_left<<"\t"<<mod_right<<endl;

    if (!this->mirror_sensors)
    {
        this->drivingModel.proteins.at(0).concentration = normalizeInput(cs.getTrack(3),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(1).concentration = normalizeInput(cs.getTrack(5),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(2).concentration = normalizeInput(cs.getTrack(7),TRACK_MIN, TRACK_MAX) * mod_left;
        this->drivingModel.proteins.at(3).concentration = normalizeInput(cs.getTrack(8),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(4).concentration = normalizeInput(cs.getTrack(9),TRACK_MIN, TRACK_MAX); //mid
        this->drivingModel.proteins.at(5).concentration = normalizeInput(cs.getTrack(10),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(6).concentration = normalizeInput(cs.getTrack(11),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(7).concentration = normalizeInput(cs.getTrack(13),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(8).concentration = normalizeInput(cs.getTrack(15),TRACK_MIN, TRACK_MAX)* mod_right;
    }
    else
    {
        this->drivingModel.proteins.at(0).concentration = normalizeInput(cs.getTrack(15),TRACK_MIN, TRACK_MAX) * mod_left;
        this->drivingModel.proteins.at(1).concentration = normalizeInput(cs.getTrack(13),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(2).concentration = normalizeInput(cs.getTrack(11),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(3).concentration = normalizeInput(cs.getTrack(10),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(4).concentration = normalizeInput(cs.getTrack(9),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(5).concentration = normalizeInput(cs.getTrack(8),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(6).concentration = normalizeInput(cs.getTrack(7),TRACK_MIN, TRACK_MAX) * mod_right;
        this->drivingModel.proteins.at(7).concentration = normalizeInput(cs.getTrack(5),TRACK_MIN, TRACK_MAX);
        this->drivingModel.proteins.at(8).concentration = normalizeInput(cs.getTrack(3),TRACK_MIN, TRACK_MAX);
    }

    this->drivingModel.proteins.at(9).concentration = normalizeInput(cs.getSpeedX(), SPEEDX_MIN, SPEEDX_MAX) * throttle_regulator;
    this->drivingModel.proteins.at(10).concentration = normalizeInput(cs.getSpeedY(), SPEEDY_MIN, SPEEDY_MAX);
    //this->model.proteins.at(11).concentration = normalizeInput(lastSteer, STEER_MIN, STEER_MAX);

    //cout<<cs.getSpeedX()<<"\t \t"<<cs.getSpeedY()<<endl;

    //model.getGenome().printOn(cout);
    ////cerr << "proteins: " << model.getGenome() << endl;

    /*---- Main run ----*/
    this->drivingModel.evolve(GRN_STEPS);


    //  //cerr << "evolved" << endl;
    
    // check if car is currently stuck
    // compute accel/brake command

    float accelbrake = getAccelAndBrake();

    accelbrake = filterTCL(cs, accelbrake);

    float accel=0.;
    float brake=0;

    if (accelbrake > 0)
    {
        accel = min(1.0f, accelbrake);
    }
    else
    {
        brake= filterABS(cs, min(1.0f, abs(accelbrake)));
    }
    
    // compute steering
    float steer = getSteer(cs);
    ////cerr << "steer:"<<steer<<endl;
    ////cerr << "steer" << endl;
    int gear = getGear(cs);
    
    // normalize steering
    //steer*=2;
    if (steer < -1)
        steer = -1;
    if (steer > 1)
        steer = 1;
    
    // Calculate clutching
    clutching(cs,clutch);
    
    // build a CarControl variable and return it
    CarControl cc(accel,brake,gear,steer,clutch);


    lastDistance=cs.getDistRaced();
    lastDistanceOnTrackLine = cs.getDistFromStart();
    lastDamage=cs.getDamage();
    ontrack=cs.getTrackPos()>=-IS_ON_TRACK_VALUE && cs.getTrackPos()<=IS_ON_TRACK_VALUE;
    isOnTrackPerso=cs.getTrackPos()>=-1.05 && cs.getTrackPos()<=1.05;
    //    //cerr << isOnTrackPerso << "  " << isOnTrack() << endl;
    
    return cc;
}

float  GRNDriver::filterTCL(CarState &cs,float accel_brake)
{
    float new_accel_brake=accel_brake;
    double s = ((3.6*cs.getWheelSpinVel(0) +
                 3.6*cs.getWheelSpinVel(1)) * wheelRadius[1] +
            (3.6*cs.getWheelSpinVel(2) +
             3.6*cs.getWheelSpinVel(3)) * wheelRadius[3]) / 4.0;

    float slipspeed;
    if (cs.getSpeedX()>=0) slipspeed = s-cs.getSpeedX();
    else slipspeed = s+cs.getSpeedX();

    float TCL_SLIP = 1.5;
    float TCL_RANGE = 5.0;
    if (slipspeed > TCL_SLIP && accel_brake>0) {
        new_accel_brake = accel_brake - min(accel_brake, (slipspeed - TCL_SLIP)/TCL_RANGE);
    }

    return new_accel_brake;

}


bool GRNDriver::opponentsAhead(CarState cs){
    return cs.getOpponents(16)<25.5f || cs.getOpponents(17)<40.f || cs.getOpponents(18)<40.f || cs.getOpponents(19)<25.5f;
    //return (cs.getOpponents(17)<45.f || cs.getOpponents(18)<45.f); //&& (cs.getOpponents(17)<cs.getTrack(9)||cs.getOpponents(18)<cs.getTrack(9));
}


bool GRNDriver::opponentsFrontLeft(CarState cs)
{
    bool opponent_nearby = false;
    for (int i=6; i<16; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<15.;
    }

    return opponent_nearby;
}

bool GRNDriver::opponentsFrontRight(CarState cs){
    bool opponent_nearby = false;
    for (int i=20; i<30; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<15.;
    }

    return opponent_nearby;
}


bool GRNDriver::opponentsOnLeft(CarState cs)
{
    bool opponent_nearby = false;
    for (int i=2; i<17; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<15.;
    }

    return opponent_nearby;
}

bool GRNDriver::opponentsOnRight(CarState cs){
    bool opponent_nearby = false;
    for (int i=19; i<34; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<15.;
    }

    return opponent_nearby;
}

bool GRNDriver::opponentsLeftSide(CarState cs)
{
    bool opponent_nearby = false;
    for (int i=3; i<15; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<5.;
    }

    return opponent_nearby;
}

bool GRNDriver::opponentsRightSide(CarState cs){
    bool opponent_nearby = false;
    for (int i=21; i<33; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<15.;
    }

    return opponent_nearby;
}

bool GRNDriver::opponentOvertakenLeft(CarState cs)
{
    bool opponent_nearby = false;
    for (int i=6; i<14; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<15.;
    }

    return opponent_nearby;
}

bool GRNDriver::opponentOvertakenRight(CarState cs){
    bool opponent_nearby = false;
    for (int i=22; i<30; i++){
        opponent_nearby = opponent_nearby || cs.getOpponents(i)<5.;
    }

    return opponent_nearby;
}

void GRNDriver::handleOpponents_beta(CarState cs)
{
    static float m_left = 1.;
    static float m_right = 1.;
    static float last_front_left = 200.;
    static float last_front_right = 200.;
    static float delta_d = 0.;
    static float delta_opponents_right = 0.;
    static float delta_opponents_left = 0.;
    static float last_opp_min = 200.;


    float dist_right = 0.;
    float dist_left = 0.;
    float dist_front = 0.;
    float track_width = cs.getTrack(0) + cs.getTrack(18);

    float opp_min = min(cs.getOpponents(17), cs.getOpponents(18));
    delta_opponents_left = cs.getOpponents(17) - last_front_left;
    delta_opponents_right = cs.getOpponents(18) - last_front_right;
    bool opponent_closer = delta_opponents_left < 0 || delta_opponents_right < 0;
    float delta_opp_min = min(delta_opponents_left, delta_opponents_right);


    if (!(opponentsAhead(cs) && opponent_closer)  && !opponentsOnLeft(cs) && !opponentsOnRight(cs) && overtaking > 0)
    {
        //cerr<<"------- RAZ ------- "<< overtaking <<endl;
        overtaking = 0;
        holding = 0;
        on_right = false;
        on_left = false;
        stay_behind = false;
        m_left = 1.;
        m_right = 1.;
        dist_right = 0.;
        dist_left = 0.;
        dist_front = 0.;
        last_front_left = 200.;
        last_front_right = 200.;
        last_opp_min = 200.;
    }

    else if ( (opponentsAhead(cs) && opponent_closer) || overtaking > 0)
    {
        //Overtaking
        if (overtaking == 0)
        {
            overtaking = 1;
        }

        dist_front = min(cs.getOpponents(16), min(cs.getOpponents(17), min(cs.getOpponents(18), cs.getOpponents(19))));
        dist_left = min(cs.getTrack(0), min(cs.getTrack(1), min(cs.getTrack(2), cs.getTrack(2))));
        dist_right = min(cs.getTrack(16), min(cs.getTrack(16), min(cs.getTrack(17), cs.getTrack(18))));

        if (overtaking == 1)
        { // choose direction
            if (min(cs.getOpponents(16),cs.getOpponents(17)) < min(cs.getOpponents(18),cs.getOpponents(19)))
            {
                if (!opponentsFrontRight(cs) && cs.getTrack(9)>50.) //&& dist_right > 5.)
                {
                    on_right = true;
                    on_left = false;
                    stay_behind = false;
                }
                else if (!opponentsFrontLeft(cs) && cs.getTrack(9)>50.) //&& dist_left > 5.)
                {
                    on_right = false;
                    on_left = true;
                    stay_behind = false;
                }
                else // if ((last_front_left - cs.getOpponents(17)) > 0. || ((last_front_right - cs.getOpponents(18))>0.))
                {
                    on_right = false;
                    on_left = false;
                    stay_behind = true;
                }
            }
            else
            {
                if (!opponentsFrontLeft(cs) && cs.getTrack(9)>50.) //&& dist_left> 5.)
                {
                    on_right = false;
                    on_left = true;
                    stay_behind = false;
                }
                else if (!opponentsFrontRight(cs) && cs.getTrack(9)>50.) //&& dist_right > 5.)
                {
                    on_right = true;
                    on_left = false;
                    stay_behind = false;
                }
                else //if ((last_front_left - cs.getOpponents(17)) > 0. || ((last_front_right - cs.getOpponents(18))>0.))
                {
                    on_right = false;
                    on_left = false;
                    stay_behind = true;
                }
            }
            overtaking = 2;
        }

        if (overtaking > 1) //active overtaking
        {
            float dist = min(cs.getOpponents(17), cs.getOpponents(18));
            if (stay_behind || (overtaking>2 && (dist <= 25. && opponent_closer))) //|| dist<5. || min(dist_right, dist_left) < 1.)//|| (cs.getTrack(9)<30 && (cs.getTrack(8)<30 || cs.getTrack(10)<30)))
            {
                //cerr<<"-- Stay behind --"<<endl;
                //cerr<<"-- Stay behind --"<<endl;
                if (cs.getRacePos()>1 && cs.getDamage()<6000)
                {
                    throttle_regulator = fmax(throttle_regulator, (-4/25.)*dist + 4.);
                }
                else
                {
                    throttle_regulator = fmax(throttle_regulator, (-3/25.)*dist + 4.);
                }

                if (dist<5.)
                {
                    throttle_regulator = 5.;
                }


                 if (on_right && overtaking > 2)
                {
                    //cerr<<"-- Stay behind -- Right Side ---"<<endl;

                     if (cs.getTrackPos()>0.){
                        m_right = 2.65 - (1.40*cs.getTrackPos()); //2.15;
                    }
                    else
                    {
                        m_right = 2.65;
                    }
                    m_left = 1.;
                }
                else if (on_left && overtaking > 2){
                     //cerr<<"-- Stay behind -- Left Side ---"<<endl;

                     if (cs.getTrackPos()<0.){
                        m_left = 1.65 + (0.25*cs.getTrackPos()); //2.15;
                    }
                    else
                    {
                       m_left = 1.65;
                    }
                    m_right = 1.;
                }
            }
            else if (on_right)
            {
                if (opponentsOnLeft(cs) || overtaking == 2)
                {
                    m_left = 1.;
                    if (holding > 0){
                        //cerr<<"-- Holding Right -- " << cs.getTrackPos() <<endl;
                        if (cs.getTrackPos()>0.){
                            m_right = 2.65 - (1.65*cs.getTrackPos()); //2.15;
                        }
                        else
                        {
                            m_right = 2.65;
                        }
                    }
                    else
                    {
                        //cerr<<"-- Overtaking Right -- "<< cs.getTrackPos() <<endl;
                        if (cs.getTrackPos()>0.)
                        {
                            m_right = 2.65 - (1.65*cs.getTrackPos()); //2.15;
                        }
                        else
                        {
                            m_right = 2.65;
                        }
                    }
                    if (opponentsOnLeft(cs)) {
                            overtaking = 3;
                    }
                }
                else if (overtaking > 2)
                {
                    overtaking = 0;
                    holding = 0;
                    on_right = false;
                    on_left = false;
                    stay_behind = false;
                    m_left = 1.;
                    m_right = 1.;
                }
            }
            else if (on_left)
            {
                if (opponentsOnRight(cs) || overtaking == 2)
                {
                    if (holding > 0){
                        //cerr<<"-- Holding Left --"<<endl;
                        if (cs.getTrackPos()<0.){
                            m_left = 1.65 + (0.65*cs.getTrackPos()); //2.15;
                        }
                        else
                        {
                           m_left = 1.65;
                        }
                    }
                    else{
                        //cerr<<"-- Overtaking Left --"<<endl;
                        if (cs.getTrackPos()<0.){
                            m_left = 1.65 + (0.65*cs.getTrackPos()); //2.15;
                        }
                        else
                        {
                           m_left = 1.65;
                        }
                    }
                    if (opponentsOnRight(cs)) {
                            overtaking = 3;
                    }
                    m_right = 1.;
                }
                else if (overtaking > 2)
                {
                    overtaking = 0;
                    holding = 0;
                    on_right = false;
                    on_left = false;
                    stay_behind = false;
                    m_left = 1.;
                    m_right = 1.;
                }
            }
        }
    }
    else if (opponentsLeftSide(cs))
    {
        if (!opponentsRightSide(cs))
        {
            //cerr<<"-- Going to Right Side --"<<endl;

            m_left = 1.;
            m_right = 2.45;
            on_right = true;
            on_left = false;
            stay_behind = false;
            overtaking = 2;
            holding = 1;
            //throttle_regulator = fmax(throttle_regulator, 1.0);
        }
        else
        {
            throttle_regulator = fmax(throttle_regulator, 1.0);
        }
    }
    else if (opponentsRightSide(cs)){
        if (!opponentsLeftSide(cs))
        {
            //cerr<<"-- Going to Left Side --"<<endl;

            m_left = 1.65;
            m_right = 1.;
            on_right = false;
            on_left = true;
            stay_behind = false;
            overtaking = 2;
            holding = 1;
            //throttle_regulator = fmax(throttle_regulator, 1.0);
        }
        else
        {
            throttle_regulator = fmax(throttle_regulator, 1.0);
        }
    }
    else
    {
        m_left = 1.;
        m_right = 1.;
    }

   last_front_left = cs.getOpponents(17);
   last_front_right = cs.getOpponents(18);
   last_opp_min = opp_min;

   mod_left = m_left;
   mod_right = m_right;
}

float GRNDriver::filterABS(CarState &cs,float brake)
{
    // convert speed to m/s
    float speed = cs.getSpeedX() / 3.6;
    // when spedd lower than min speed for abs do nothing
    if (speed < absMinSpeed)
        return brake;
    
    // compute the speed of wheels in m/s
    float slip = 0.0f;
    for (int i = 0; i < 4; i++)
    {
        slip += cs.getWheelSpinVel(i) * wheelRadius[i];
    }
    // slip is the difference between actual speed of car and average speed of wheels
    slip = speed - slip/4.0f;
    // when slip too high applu ABS
    if (slip > absSlip)
    {
        brake = brake - (slip - absSlip)/absRange;
    }
    
    // check brake is not negative, otherwise set it to zero
    if (brake<0)
        return 0;
    else
        return brake;
}

/* Antilocking filter for brakes */
float GRNDriver::filterABS_2(CarState &cs,float brake)
{
    if (cs.getSpeedX() < absMinSpeed) return brake;
    int i;
    float slip = 0.0;
    for (i = 0; i < 4; i++) {
        slip += cs.getWheelSpinVel(i) * wheelRadius[i] / cs.getSpeedX();
    }
    slip = slip/4.0;
    if (slip < 0.9) brake = brake*slip;
    return brake;
}

CarControl GRNDriver::outTrackRecovery(CarState cs){
    
    float accel = lastAccel * 0.66;
    float brake = 0.;
    float steer;
    int gear = cs.getGear();

    mod_left = 1.;
    mod_right = 1.;


    if (cs.getTrackPos()>=-1. && cs.getTrackPos()<=1.)
    {
        outTrackRecovering = false;
        outTrack = 0;
    }
    else
    {
        
        //cerr << "OUT Track --------------" << endl;
        
        
        outTrackRecovering = true;
        outTrack += 1;
        
        gear = cs.getGear();
        
        if (cs.getTrackPos() >= 1.) // Out on the Left of the track
        {
            accel = 0.66*lastAccel;
            
            if (cs.getAngle() <= 0 && cs.getAngle() >= -PI/2.){
                steer = -0.75; //-1.;//- PI/8.;
            }
            else if (cs.getAngle()>0. && cs.getAngle() < PI/4 - UNSTUCK_DELTA){
                steer = -0.75; //-1.; //- PI/8.;
            }
            else if (cs.getAngle() >= PI/4. - UNSTUCK_DELTA && cs.getAngle() <= PI/4. + UNSTUCK_DELTA){
                steer = 0.;
            }
            else if (cs.getAngle() > PI/4. + UNSTUCK_DELTA && cs.getAngle() <= PI/2.){
                steer =  0.75; //1.; //PI/8.;
            }
        }
        else if (cs.getTrackPos() <= -1.) // Out on the Right of the track
        {
            accel = 0.66*lastAccel;
            
            if (cs.getAngle() <= 0 && cs.getAngle() > -PI/4. + UNSTUCK_DELTA){
                steer = 0.75; //1.; //PI/8.;
            }
            else if (cs.getAngle()<= -PI/4. + UNSTUCK_DELTA && cs.getAngle() >= -PI/4. - UNSTUCK_DELTA){
                steer = 0.;
            }
            else if (cs.getAngle() <  -PI/4. - UNSTUCK_DELTA && cs.getAngle() >= -PI/2.){
                steer = -0.75; //-1.; //PI/8.;
            }
            else if (cs.getAngle() > 0. && cs.getAngle() <= PI/2.){
                steer = 0.75; //1.; //PI/8.;
            }
        }
        else
        {
            steer = 0;
            gear = 0;
            accel = 0.;
            brake = 1.;
        }
    }

    accel = filterTCL(cs, accel);
    
    // Calculate clutching
    clutching(cs,clutch);
    
    // build a CarControl variable and return it
    CarControl cc(accel,brake,gear,steer,clutch);
    
    return cc;
}


CarControl GRNDriver::stuckRecovery(CarState cs){
    mod_left = 1.;
    mod_right = 1.;
    float accel = 0.66;
    float brake = 0.;
    float steer;
    int gear = cs.getGear();

    if (cs.getTrackPos() >= -UNSTUCK_DISTANCE && cs.getTrackPos() <= UNSTUCK_DISTANCE && cs.getAngle() > -PI/2. && cs.getAngle() < PI/2.)
    {
        //cerr << "STUCK Recovery ----- OUT --------------" << endl;

        stuckRecovering = false;
        reverseRecovering = false;
        stuck = 0;
    }
    else
    {
        //cerr << "STUCK Recovery --------------" << endl;


        stuck += 1;
        stuckRecovering = true;

        if (stuck < 2 || (stuck > 50 && cs.getSpeedX() <1.) )
        {
            if ((cs.getTrackPos()>=0. && cs.getAngle()>-PI/4.)||(cs.getTrackPos()<=0. && cs.getAngle()<PI/4))
            {
                reverseRecovering = false;
            }
            else{
                reverseRecovering = true;
            }
        }

        if (cs.getSpeedX() <= 1.)
        {
            if ( stuck > 300 )
            {
                reverseRecovering = !reverseRecovering;
                stuck = 2;
            }
        }
        else
        {
            stuck = 2;
        }

        // //cerr << "RECOVERING "<< reverseRecovering <<" -----------------------------------" << endl;

        if (!reverseRecovering)
        {
            if (gear < 1){
                gear = 1;
            }
            else{
                gear = getGear(cs);
            }

            if (cs.getTrackPos() >= UNSTUCK_DISTANCE) // Out on the Left of the track
            {
                if (cs.getAngle() < 0. && cs.getAngle() >= -PI/2.){
                    steer = -1.; //PI/8.;
                }
                else if (cs.getAngle() < -PI/2 && cs.getAngle() >= -PI){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle()>=0. && cs.getAngle() < PI/4 - UNSTUCK_DELTA){
                    steer = -1.; //PI/8.;
                }
                else if (cs.getAngle() >= PI/4. - UNSTUCK_DELTA && cs.getAngle() <= PI/4. + UNSTUCK_DELTA){
                    steer = 0.;
                }
                else if (cs.getAngle() > PI/4. + UNSTUCK_DELTA && cs.getAngle() <= PI/2.){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle() > PI/2 && cs.getAngle() <= PI){
                    steer = 1.; //PI/8.;
                }
            }
            else if (cs.getTrackPos() <= -UNSTUCK_DISTANCE) // Out on the Right of the track
            {
                if (cs.getAngle() <= 0 && cs.getAngle() > -PI/4. + UNSTUCK_DELTA){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle()<= -PI/4. + UNSTUCK_DELTA && cs.getAngle() >= - PI/4. - UNSTUCK_DELTA){
                    steer = 0.;
                }
                else if (cs.getAngle() <  -PI/4. - UNSTUCK_DELTA && cs.getAngle() >= -PI){
                    steer = -1.; //PI/8.;
                }
                else if (cs.getAngle() > 0. && cs.getAngle() <= PI/2.){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle() > PI/2. && cs.getAngle() <= PI) {
                    steer = -1.; //-PI/8.;
                }
            }
        }
        else
        {
            gear = -1;

            if (cs.getTrackPos() >= UNSTUCK_DISTANCE) // Out on the Left of the track
            {
                if (cs.getAngle() <=0 && cs.getAngle() >= -PI/2.){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle() < -PI/2 && cs.getAngle() >= -PI){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle()>0. && cs.getAngle() < PI/4 - UNSTUCK_DELTA){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle() >= PI/4. - UNSTUCK_DELTA && cs.getAngle() <= PI/4. + UNSTUCK_DELTA){
                    ////cerr<<"good PI/4"<<endl;
                    steer = 0.;
                    gear = 0;
                    brake = 1.;
                    accel = 0.;
                    stuck = 0;
                }
                else if (cs.getAngle() > PI/4. + UNSTUCK_DELTA && cs.getAngle() <= PI/2.){
                    steer = -1.; //-PI/8.;
                }
                else if (cs.getAngle() > PI/2 && cs.getAngle() <= PI){
                    steer = -1.; //-PI/8.;
                }
            }
            else if (cs.getTrackPos() <= -UNSTUCK_DISTANCE) // Out on the Right of the track
            {
                if (cs.getAngle() <= 0 && cs.getAngle() > - PI/4. + UNSTUCK_DELTA){
                    steer = -1.; //-PI/8.;
                }
                else if (cs.getAngle()<= -PI/4. + UNSTUCK_DELTA && cs.getAngle() >= -PI/4. - UNSTUCK_DELTA){
                    ////cerr<<"good -PI/4"<<endl;
                    steer = 0.;
                    gear = 0;
                    brake = 1.;
                    accel = 0.;
                    stuck = 0;
                }
                else if (cs.getAngle() <  -PI/4. - UNSTUCK_DELTA && cs.getAngle() >= -PI){
                    steer = 1.; //PI/8.;
                }
                else if (cs.getAngle() > 0. && cs.getAngle() <= PI/2.){
                    steer = -1.; //-PI/8.;
                }
                else if (cs.getAngle()> PI/2. && cs.getAngle() <= PI) {
                    steer = -1.; //PI/8.;
                }
            }
        }
    }

    accel = filterTCL(cs, accel);

    // Calculate clutching
    clutching(cs,clutch);

    // build a CarControl variable and return it
    CarControl cc(accel,brake,gear,steer,clutch);

    return cc;
}

CarControl GRNDriver::wrongWayRecovery(CarState cs){
    mod_left = 1.;
    mod_right = 1.;
    float accel;
    float brake;
    float steer;
    int gear = cs.getGear();
    
    // //cerr << "WRONG WAY !!! -------"<<endl;
    
    if (cs.getAngle()>-PI/4. && cs.getAngle()<PI/4.)
    {
        wrongwayRecovering = false;
        wrongway = 0;
    }
    else
    {
        wrongway += 1;
        wrongwayRecovering = true;
        
        if (wrongway <2) {gear = -1;}
        
        if (wrongway > 100 && cs.getSpeedX() < 1.)
        {
            gear = gear * -1;
            wrongway = 15;
        }
        
        accel = 0.75;
        brake = 0.;
        if (cs.getAngle() < 0)
        {
            steer = -1. * gear;
        }
        else
        {
            steer = 1. * gear;
        }
    }
    // Calculate clutching
    clutching(cs,clutch);
    
    // build a CarControl variable and return it
    CarControl cc(accel,brake,gear,steer,clutch);
    
    return cc;
}

void
GRNDriver::onShutdown()
{
    //cout << "Bye bye!" << endl;
}

void
GRNDriver::onRestart()
{
    //cout << "Restarting the race!" << endl;
}

void GRNDriver::clutching(CarState &cs, float &clutch)
{
    double maxClutch = clutchMax;
    
    // Check if the current situation is the race start
    if (cs.getCurLapTime()<clutchDeltaTime  && stage==RACE && cs.getDistRaced()<clutchDeltaRaced)
        clutch = maxClutch;
    
    // Adjust the current value of the clutch
    if(clutch > 0)
    {
        double delta = clutchDelta;
        if (cs.getGear() < 2)
        {
            // Apply a stronger clutch output when the gear is one and the race is just started
            delta /= 2;
            maxClutch *= clutchMaxModifier;
            if (cs.getCurLapTime() < clutchMaxTime)
                clutch = maxClutch;
        }
        
        // check clutch is not bigger than maximum values
        clutch = min(maxClutch,double(clutch));
        
        // if clutch is not at max value decrease it quite quickly
        if (clutch!=maxClutch)
        {
            clutch -= delta;
            clutch = max(0.0,double(clutch));
        }
        // if clutch is at max value decrease it very slowly
        else
            clutch -= clutchDec;
    }
}

float GRNDriver::normalizeInput(float input, float minValue, float maxValue)
{
    return (input - minValue) / (maxValue - minValue);
}

bool GRNDriver::getJerkyDriving()
{
    return jerkingConfirmed;
}

int GRNDriver::checkNextTurn(CarState cs)
{
    if (cs.getTrack(7)-cs.getTrack(11)>7.5 && cs.getTrack(8)-cs.getTrack(10)>7.5 && cs.getAngle()>-PI/4. && cs.getAngle()<PI/4.)
    {
        ////cerr << "<--- Next turn left" << endl;
        return 0;
    }
    else if  (cs.getTrack(11)-cs.getTrack(7)>7.5 && cs.getTrack(10)-cs.getTrack(8)>7.5 && cs.getAngle()>-PI/4. && cs.getAngle()<PI/4.) {
        ////cerr << "Next turn right --->" << endl;
        return 1;
    }
    else if  (cs.getAngle()>-PI/4. && cs.getAngle()<PI/4.)
    {
        ////cerr << "--- Staight line ---" << endl;
        return 2;
    }
    return -1;
}
