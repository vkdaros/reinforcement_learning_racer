/***************************************************************************
 
 file                 : SimpleDriver.h
 copyright            : (C) 2007 Daniele Loiacono
 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef GRNDRIVER_H_
#define GRNDRIVER_H_

#include <iostream>
#include <cmath>
#include "BaseDriver.h"
#include "CarState.h"
#include "CarControl.h"
#include "SimpleParser.h"
#include "WrapperBaseDriver.h"
#include "GRNModel.h"

#define PI 3.14159265

using namespace std;

class GRNDriver : public WrapperBaseDriver
{
public:
	
    // Constructor
    GRNDriver(){
        stuck=0;clutch=0.0;
        lastDistance=0;
        lastDamage=0;
        bestLapTime=10000000000;
        numLap=0;
        currentSteer = 0.;
        estimatedLapLength = 0.;
        distRacedOnTrackLine = 0.;
        lastDistanceOnTrackLine=0.;
        reverseDriving = false;
        lastLapTime=0;
        isInitialDistanceFromStartEvaluated=false;
        stuck = 0;
        outTrack = 0;
        outTrackRecovering = false;
        stuckRecovering = false;
        reverseRecovering = false;
        wrongway = 0;
        wrongwayRecovering = 0;
        oneStepCarData ="";
        oneStepGRNData = "";
        jerkingCheck = 0;
        jerkingTimeSteps = 0;
        jerkingDelta = 0.f;
        jerkingConfirmed = false;
        overtaking = 0;
        on_left = false;
        on_right = false;
        stay_behind = false;
        throttle_regulator = 1.;
        mod_left = 1.;
        mod_right = 1.;
        steps = 0;
    };
    //{stuck=0;clutch=0.0;lastDistance=0;lastDamage=0;bestLapTime=10000000000; numLap=0;currentSteer = 0.;estimatedLapLength = 0.;distRacedOnTrackLine = 0.;lastDistanceOnTrackLine=0.;reverseDriving = false;lastLapTime=0; isInitialDistanceFromStartEvaluated=false;stuck = 0; outTrack = 0; outTrackRecovering = false; stuckRecovering = false; reverseRecovering = false; wrongway = 0;wrongwayRecovering = 0;oneStepCarData ="";oneStepGRNData = "";};
    
    void setGRN(GRNModel &newModel);
    void setAgressivityVector(vector<float> &agg);
    vector<float> getAggressivityVector() {return aggressivity;};
    
    // GRNDriver implements a simple and heuristic controller for driving
    virtual CarControl wDrive(CarState cs);
    
    // Print a shutdown message
    virtual void onShutdown();
	
    // Print a restart message
    virtual void onRestart();
    
	
    virtual float getDistRaced() {return distRaced;}
    virtual float getFitness() {return fitness;}
    virtual bool isOnTrack() {return ontrack;}
    virtual bool isOnGoodDirection() {return goodDirection;}
    virtual float getLastLapTime() {return lastLapTime;}
    virtual float getDamage() {return damage;}
    virtual float getLastLapDist() {return lastLapDist;}
    virtual float getBestLapTime() {return bestLapTime;}
    virtual int getLastSector() {return lastSector;};
    virtual int getNumLap() {return numLap;};
    virtual float getEstimatedLapLength() {return estimatedLapLength;};
    virtual float getDistRacedOnTrackLine() {return distRacedOnTrackLine;};
    virtual bool getReverseDriving() {return reverseDriving;};
    virtual bool getJerkyDriving();
    
    virtual float normalizeInput(float input, float minValue, float maxValue);

    
    float currentAccel;
    float currentBrake;
    float currentSteer;
    int currentGear;
    float currentClutch;

    float lastAccel;
    float lastBrake;
    float lastSteer;
    int lastGear;
    float lastClutch;
    float lastSpeedX;

    bool mirror_sensors;

    bool RUN_MODE;
    bool TRACK_RECOVERY;
    bool AVOID_OPPONENTS;
    bool AVOID_ALPHA;
    bool AGGRESSIVE_DRIVE;
    float AGG_SECTOR_SIZE;
    float AGG_DEFAULT_VALUE;
    float IS_ON_TRACK_VALUE;

    CarState lastCarState;
    int steps;

private:
	
    GRNModel model;
    GRNModel drivingModel;

    vector<float> aggressivity;
    float distRaced;
    float estimatedLapLength;
    float distRacedOnTrackLine;
    float fitness;
    float lastDistance;
    float lastDistanceOnTrackLine;
    float initialDistanceFromStart;
    bool isInitialDistanceFromStartEvaluated;

    float damage;
    float lastDamage;

    int jerkingCheck;
    int jerkingTimeSteps;
    float jerkingDelta;
    bool jerkingConfirmed;

    int lastSector;
    float initialDistRaced;
    float lastLapTime;
    float lastLapDist;
    float bestLapTime;
    int numLap;
    bool initialized;
    bool isOnTrackPerso;
    bool ontrack;
    bool goodDirection;
    bool reverseDriving;
    int wrongway;
    bool wrongwayRecovering;
    //indicates if the driver is performing out-track recovery
    bool reverseRecovering;
    bool stuckRecovering;
    bool outTrackRecovering;

    int overtaking;
    int holding;
    bool on_left;
    bool on_right;
    bool stay_behind;

    float throttle_regulator;
    float mod_left;
    float mod_right;

    /* Gear Changing Constants*/
	
    // RPM values to change gear
    static const int gearUp[6];
    static const int gearDown[6];
    
    /* Stuck constants*/
	
    // How many time steps the controller wait before recovering from a stuck position
    static const int stuckTime;
    // When car angle w.r.t. track axis is grather tan stuckAngle, the car is probably stuck
    static const float stuckAngle;
	
    /* Steering constants*/
	
    // Angle associated to a full steer command
    static const float steerLock;
    // Min speed to reduce steering command
    static const float steerSensitivityOffset;
    // Coefficient to reduce steering command at high speed (to avoid loosing the control)
    static const float wheelSensitivityCoeff;
	
    /* Accel and Brake Constants*/
	
    // max speed allowed
    static const float maxSpeed;
    // Min distance from track border to drive at  max speed
    static const float maxSpeedDist;
    // pre-computed sin5
    static const float sin5;
    // pre-computed cos5
    static const float cos5;
	
    /* ABS Filter Constants */
	
    // Radius of the 4 wheels of the car
    static const float wheelRadius[4];
    // min slip to prevent ABS
    static const float absSlip;
    // range to normalize the ABS effect on the brake
    static const float absRange;
    // min speed to activate ABS
    static const float absMinSpeed;
    
    /* Clutch constants */
    static const float clutchMax;
    static const float clutchDelta;
    static const float clutchRange;
    static const float clutchDeltaTime;
    static const float clutchDeltaRaced;
    static const float clutchDec;
    static const float clutchMaxModifier;
    static const float clutchMaxTime;
    
    // counter of stuck steps
    int stuck;
    int outTrack;
    
    //data string
    string oneStepCarData;
    string oneStepGRNData;

	
    // current clutch
    float clutch;
    
    // Solves the gear changing subproblems
    int getGear(CarState &cs);
    
    // Solves the steering subproblems
    float getSteer(CarState &cs);    
    
    // Solves the gear changing subproblems
    float getAccelAndBrake();
    
    // Apply an ABS filter to brake command
    float filterABS(CarState &cs,float brake);
    float filterABS_2(CarState &cs,float brake);

    
    // Solves the clucthing subproblems
    void clutching(CarState &cs, float &clutch);
    
    CarControl GRNDrive(CarState cs);
    CarControl outTrackRecovery(CarState cs);
    CarControl stuckRecovery(CarState cs);
    CarControl wrongWayRecovery(CarState cs);

    void handleOpponents_beta(CarState cs);

    bool opponentsAhead(CarState cs);
    bool opponentsOnLeft(CarState cs);
    bool opponentsOnRight(CarState cs);
    bool opponentsFrontLeft(CarState cs);
    bool opponentsFrontRight(CarState cs);
    bool opponentsLeftSide(CarState cs);
    bool opponentsRightSide(CarState cs);
    bool opponentOvertakenLeft(CarState cs);
    bool opponentOvertakenRight(CarState cs);

    int checkNextTurn(CarState cs);

    float filterTCL(CarState &cs,float accel_brake);
    
};

#endif /*GRNDriver_H_*/
