/*
 *  GRNModel.cpp
 *  GRN_perso
 *
 *  Created by Sylvain Cusat-Blanc on 19/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "GRNModel.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstdlib>
#include <time.h>

using namespace std;


GRNModel::GRNModel(vector<Protein> p, double b, double d) {
	proteins=p;
	beta=b;
	delta=d;
	
	currentStep=0;
	
	maxEnhance=0;
	maxInhibit=0;

	updateSignatures();
}

GRNModel::GRNModel() {
	proteins=vector<Protein>();
	beta=1;
	delta=1;

	currentStep=0;
}

GRNModel::GRNModel(const GRNModel& g) {
	proteins=vector<Protein>();
	for (unsigned int i=0; i<g.proteins.size(); i++) {
		proteins.push_back(Protein(g.proteins[i]));
	}
	
	beta=g.beta;
	delta=g.delta;
	currentStep=g.currentStep;
	
	maxEnhance=g.maxEnhance;
	maxInhibit=g.maxInhibit;

/*	enhanceMatching=vector < vector < unsigned int > >();
	for (unsigned int i=0;i<g.enhanceMatching.size(); i++) {
		vector<unsigned int> l = vector<unsigned int>();
		for (unsigned int j=0; j<g.enhanceMatching[i].size(); j++) {
			l.push_back(g.enhanceMatching[i][j]);
		}
		enhanceMatching.push_back(l);
	}
	inhibitMatching=vector < vector < unsigned int> >();
	for (unsigned int i=0;i<g.inhibitMatching.size(); i++) {
		vector<unsigned int> l = vector<unsigned int>();
		for (unsigned int j=0; j<g.inhibitMatching[i].size(); j++) {
			l.push_back(g.inhibitMatching[i][j]);
		}
		inhibitMatching.push_back(l);
	}*/
	updateSignatures();
}

void GRNModel::updateSignatures() {
	// calculating signatures

    enhanceMatching.clear();
    inhibitMatching.clear();

    for (unsigned int j=0; j<proteins.size(); j++) {
		vector<double> enh=vector<double>();
		vector<double> inh=vector<double>();

		for (unsigned int k=0; k<proteins.size(); k++) {

            enh.push_back(IDSIZE-abs(proteins[j].enhancer-proteins[k].id));
            maxEnhance=std::max(maxEnhance, enh[k]);

            inh.push_back(IDSIZE-abs(proteins[j].inhibiter-proteins[k].id));
            maxInhibit=std::max(maxInhibit, inh[k]);
        }
		enhanceMatching.push_back(enh);
		inhibitMatching.push_back(inh);
	}

	for (unsigned int j=0; j<proteins.size(); j++) {
		for (unsigned int k=0; k<proteins.size(); k++) {
    //		cerr << k << "  " << k << "  " << proteins.size() << endl;
                enhanceMatching[j][k]=exp(beta*enhanceMatching[j][k]-maxEnhance);
                inhibitMatching[j][k]=exp(beta*inhibitMatching[j][k]-maxInhibit);  
		}
	}
}


void GRNModel::evolve(unsigned int nbSteps) {
	double enhance, inhibit;
	unsigned int i,j,k,nbDiv;
	double sumConcentration;
//	for (i=0; i<proteins.size(); i++) {
//		cerr << proteins[i].concentration << "\t";
//	}
//	cerr << proteins.size() << endl;
	
	
	for (unsigned int step=0; step<nbSteps; step++) {
		vector<Protein> nextProteins=vector<Protein>();
		
		// Calculating next step protein concentrations        
		for (j=0; j<proteins.size(); j++) {
			// For an input protein, the concentration is not calculated
			if (proteins[j].type==Protein::PROTEIN_INPUT) {
				nextProteins.push_back(proteins[j]);
			} else {
				enhance=0;
				inhibit=0;
				// Calculating enhancing and inhibiting factor for the current protein
				for (k=0; k<proteins.size(); k++) {
					if (proteins[k].type!=Protein::PROTEIN_OUTPUT) {
	//cerr << j << "  " << k << "  ";
						enhance+=proteins[k].concentration*enhanceMatching[j][k];
						//cerr << j << " " << k << " " << 
						inhibit+=proteins[k].concentration*inhibitMatching[j][k];
	//cerr << "2" << endl;	
				}
				}
				// if (j=5) cout << enhance << "   " << inhibit << endl;
				// Calculating the next concentration of current protein 
				nextProteins.push_back(Protein(proteins[j].id, proteins[j].type, max(0.0,proteins[j].concentration+delta/proteins.size()*(enhance-inhibit)), proteins[j].enhancer, proteins[j].inhibiter));
			}
		}
				
		//		for (i=0; i<nextProteins.size(); i++) {
		//			cout << nextProteins[i].id << "=" << nextProteins[i].concentration << "\t";
		//		}
		//		cout << endl;
		// Reajusting proteins concentration so that their sum stay equal to 1
		sumConcentration = 0;
		nbDiv=0;
		for (i=0; i<proteins.size(); i++) {
			if (proteins[i].type!=Protein::PROTEIN_INPUT) {
				sumConcentration+=nextProteins[i].concentration;
				nbDiv++;
			}
		}
		//cout << sumConcentration << "\t" << nbDiv << "\t" << (sumConcentration-1)/nbDiv << "\t";
		for (i=0; i<proteins.size(); i++) {
			if (proteins[i].type!=Protein::PROTEIN_INPUT) {
				nextProteins[i].concentration/=sumConcentration;
			}
		}
				
		// Finalizing the step by switching the vector
		proteins.clear();
		proteins=nextProteins;
		
		//for (i=0; i<nextProteins.size(); i++) {
		//	cout << proteins[i].concentration << "\t";
		//}
		//cout << endl;
		currentStep++;
	}
}

void GRNModel::duplicateProteins(int nbDup, double mutProb) {
	//srand(time(NULL))
	int nbProteins=proteins.size();
	for (int dup=0; dup<nbDup; dup++) {
		for (int i=0; i<nbProteins; i++) {
				proteins.push_back(Protein(
									   (double)rand()/(double)RAND_MAX>mutProb?proteins[i].id:rand()/IDSIZE,
									   proteins[i].type,
									   proteins[i].concentration,
									   (double)rand()/(double)RAND_MAX>mutProb?proteins[i].enhancer:rand()/IDSIZE,
									   (double)rand()/(double)RAND_MAX>mutProb?proteins[i].inhibiter:rand()/IDSIZE));
		}
	}
	updateSignatures();
}

void GRNModel::duplicateRegulatoryProteins(int nbDup, double mutProb) {
	//srand(time(NULL))
	int nbProteins=proteins.size();
	for (int dup=0; dup<nbDup; dup++) {
		for (int i=0; i<nbProteins; i++) {
			if (proteins[i].type==Protein::PROTEIN_REGUL) {
				proteins.push_back(Protein(
										   (double)rand()/(double)RAND_MAX>mutProb?proteins[i].id:rand()/IDSIZE,
										   proteins[i].type,
										   proteins[i].concentration,
										   (double)rand()/(double)RAND_MAX>mutProb?proteins[i].enhancer:rand()/IDSIZE,
										   (double)rand()/(double)RAND_MAX>mutProb?proteins[i].inhibiter:rand()/IDSIZE));
			}
		}
	}
	updateSignatures();
}

GRNModel GRNModel::copy() {
	GRNModel res = GRNModel();
	res.proteins=vector<Protein>();
	for (unsigned int i=0; i<this->proteins.size(); i++) {
		res.proteins.push_back(Protein(this->proteins[i]));
	}
	
	res.beta=this->beta;
	res.delta=this->delta;
	res.currentStep=this->currentStep;
	
	res.maxEnhance=this->maxEnhance;
    res.maxInhibit=this->maxInhibit;
	
	res.enhanceMatching=vector < vector < double > >();
	for (unsigned int i=0;i<this->enhanceMatching.size(); i++) {
		vector<double> l = vector<double>();
		for (unsigned int j=0; j<this->enhanceMatching[i].size(); j++) {
			l.push_back(this->enhanceMatching[i][j]);
		}
		res.enhanceMatching.push_back(l);
	}
	res.inhibitMatching=vector < vector < double> >();
	for (unsigned int i=0;i<this->inhibitMatching.size(); i++) {
		vector<double> l = vector<double>();
		for (unsigned int j=0; j<this->inhibitMatching[i].size(); j++) {
			l.push_back(this->inhibitMatching[i][j]);
		}
		res.inhibitMatching.push_back(l);
	}
	
	return res;
}

void GRNModel::mutate(double mutRate) {
	cerr << "\nmutate " << mutRate << ": ";
	for (int i=0; i<proteins.size(); i++) {
		proteins[i].id=(double)rand()/RAND_MAX<mutRate?rand()%IDSIZE:proteins[i].id;
		proteins[i].enhancer=(double)rand()/RAND_MAX<mutRate?rand()%IDSIZE:proteins[i].enhancer;
		proteins[i].inhibiter=(double)rand()/RAND_MAX<mutRate?rand()%IDSIZE:proteins[i].inhibiter;
		cerr << "[" << proteins[i].id << "," << proteins[i].enhancer << "," << proteins[i].inhibiter << "] ";
	}
	beta=(double)rand()/RAND_MAX<mutRate?((double)rand()/RAND_MAX*1.5+0.5):beta;
	delta=(double)rand()/RAND_MAX<mutRate?((double)rand()/RAND_MAX*1.5+0.5):delta;
	cerr << "{" << beta << "," << delta << "}" <<  endl;
	updateSignatures();
}

void GRNModel::reset() {
	for (int i=0; i<proteins.size(); i++) {
		proteins[i].concentration=1.0/(double)proteins.size();
	}
	currentStep=0;
}

void GRNModel::saveToFile(string fileName) {
	ofstream file;
	file.open(fileName.c_str());
	
	file.write((char*)&beta, sizeof(double));
	file.write((char*)&delta, sizeof(double));
	file.write((char*)&currentStep, sizeof(int));
	int nbProt = proteins.size();
	file.write((char*)&nbProt, sizeof(int));

	for (int i=0; i<proteins.size(); i++) {
		file.write((char*)&proteins[i].id, sizeof(int));
		file.write((char*)&proteins[i].concentration, sizeof(double));
		file.write((char*)&proteins[i].type, sizeof(int));
		file.write((char*)&proteins[i].enhancer, sizeof(int));
		file.write((char*)&proteins[i].inhibiter, sizeof(int));
	}	
	
	file.close();
}

GRNModel::GRNModel(string fileName) {
	ifstream file;
	file.open(fileName.c_str());

	file.read((char*)&beta,sizeof(double));
	file.read((char*)&delta,sizeof(double));
	file.read((char*)&currentStep,sizeof(int));
	
	int nbProt;
	file.read((char*)&nbProt,sizeof(int));
	
	int protID;
	double protConc;
	int protType;
	int protEN;
	int protIN;
	for (int i=0; i<nbProt; i++) {
		file.read((char*)&protID,sizeof(int));
		file.read((char*)&protConc,sizeof(double));
		file.read((char*)&protType,sizeof(int));
		file.read((char*)&protEN,sizeof(int));
		file.read((char*)&protIN,sizeof(int));
		proteins.push_back(Protein(protID, protType, protConc, protEN, protIN));
	}
	file.close();
	maxEnhance=0;
    maxInhibit=0;

	updateSignatures();
}


string GRNModel::toString() {
	stringstream res;
	res << "GRN\tbeta=" << beta << " ; delta=" << delta << endl;
	res << "\tcurrent step=" << currentStep << " ; nbProt=" << proteins.size() << endl;
	res << "\tProteins:" << endl;
	for (int i=0; i<proteins.size(); i++) {
		res << "\t\tProtein " << i << ":\t[" << proteins[i].id << " ; " << proteins[i].concentration << " ; " << proteins[i].type << " ; " << proteins[i].enhancer << " ; " << proteins[i].inhibiter << "]" << endl;
	}
	return res.str();
}

