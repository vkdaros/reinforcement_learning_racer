/*
 *  GRNModel.h
 *  GRN_perso
 *
 *  Created by Sylvain Cusat-Blanc on 19/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef GRN_MODEL_H
#define GRN_MODEL_H

#include <vector>
#include <string>

#include "Protein.h"

using namespace std;

class GRNModel {
public:
	vector<Protein> proteins;
	double beta;
	double delta;
	
    GRNModel(vector<Protein> prot, double b, double d);
	GRNModel();
	GRNModel(const GRNModel& g);
	GRNModel(string fileName);
	
    inline vector<Protein> getProteins() {return proteins;}
//	inline unsigned int getNbProteins() {return nbProteins;};
	void evolve(unsigned int nbSteps);
	void duplicateProteins(int nbDup, double mutProb);
	void duplicateRegulatoryProteins(int nbDup, double mutProb);
	GRNModel copy();
	void mutate(double mutRate);
	void reset();
	void saveToFile(string fileName);
	string toString();

private:
	unsigned int currentStep;
    double maxEnhance;
    double maxInhibit;
    vector < vector < double > > enhanceMatching;
    vector < vector < double > > inhibitMatching;
	void updateSignatures();

};

#endif
