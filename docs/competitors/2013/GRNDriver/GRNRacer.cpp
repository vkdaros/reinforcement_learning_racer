/*
 *  GRNDriverGenome.cpp
 *  GRN_perso
 *
 *  Created by Sylvain Cusat-Blanc on 20/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifdef WIN32
#include <WinSock.h>
#else
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#endif

#include <iostream>
#include <cstdlib>
#include <cstdio>
//#include __DRIVER_INCLUDE__

/*** defines for UDP *****/
#define UDP_MSGLEN 1000
#define UDP_CLIENT_TIMEUOT 100000
//#define __UDP_CLIENT_VERBOSE__
/************************/

#ifdef WIN32
typedef sockaddr_in tSockAddrIn;
#define CLOSE(x) closesocket(x)
#define INVALID(x) x == INVALID_SOCKET
#else
typedef int SOCKET;
typedef struct sockaddr_in tSockAddrIn;
#define CLOSE(x) close(x)
#define INVALID(x) x < 0
#endif

#include<iostream>
#include<fstream>
#include<iomanip>
#include<cmath>
#include<climits>
#include<cfloat>// For FLT_MAX
#include<limits>// For numeric_limits
#include<semaphore.h>
#include<signal.h>
#include<cassert>
#include<sstream>
#include<stdio.h>
#include<stdlib.h>

#include <iostream>
#include "math.h"

#include "GRNRacer.h"

#include "Protein.h"
#include "GRNModel.h"
#include "GRNDriver.h"
#include "BaseDriver.h"

#include "DriverConstants.h"

using namespace std;


GRNRacer::GRNRacer(const GRNModel& pGRN):grn(pGRN)
{
}




bool GRNRacer::race(bool mirroring) {
    unsigned long currentStep=0;
    float gapDistance = GAP;

    double fit= 0.;
    float fitTicket = 0;
    float lastDamage=0;

    float cumulativeDist = 0.;
    int stepInSector = 0;
    float maxSpeed = 0.;

    try {

        GRNModel model = grn;

        SOCKET socketDescriptor;

        int numRead;

        //    bool noise;
        //    double noiseAVG;
        //    double noiseSTD;
        //    long seed;
        tSockAddrIn serverAddress;
        struct hostent *hostInfo;
        struct timeval timeVal;
        fd_set readSet;
        char buf[UDP_MSGLEN];

        // variable pour compter le nombre de pas ajouter
        int ticket=1;

#ifdef WIN32
        /* WinSock Startup */

        WSADATA wsaData={0};
        WORD wVer = MAKEWORD(2,2);
        int nRet = WSAStartup(wVer,&wsaData);

        if(nRet == SOCKET_ERROR)
        {
            std::cout << "Failed to init WinSock library" << std::endl;
            this->setNote(0);
            return false;
        }
#endif

        //    parse_args(argc,argv,hostName,serverPort,id,maxEpisodes,maxSteps,noise,noiseAVG,noiseSTD,seed,trackName,stage);

        //    if (seed>0)
        //    	srand(seed);
        //    else
        //    	srand(time(NULL));

        hostInfo = gethostbyname(hostName);
        if (hostInfo == NULL)
        {
            cout << "Error: problem interpreting host: " << hostName << "\n";
            //this->setNote(0);
            return false;
        }
        /*
        // Print command line option used
        cout << "***********************************" << endl;
        cout << "HOST: "   << hostName    << endl;
        cout << "PORT: " << serverPort  << endl;
        cout << "ID: "   << id     << endl;
        cout << "MAX_STEPS: " << maxSteps << endl;
        cout << "MAX_EPISODES: " << maxEpisodes << endl;

        //    if (seed>0)
        //    	cout << "SEED: " << seed << endl;

        cout << "TRACKNAME: " << trackName << endl;

        if (stage == BaseDriver::WARMUP)
            cout << "STAGE: WARMUP" << endl;
        else if (stage == BaseDriver::QUALIFYING)
            cout << "STAGE:QUALIFYING" << endl;
        else if (stage == BaseDriver::RACE)
            cout << "STAGE: RACE" << endl;
        else
            cout << "STAGE: UNKNOWN" << endl;

        cout << "***********************************" << endl;
*/
        // Create a socket (UDP on IPv4 protocol)
        socketDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
        if (INVALID(socketDescriptor))
        {
            cout << "cannot create socket\n";
            //            this->setNote(0);
            return false;
        }

        // Set some fields in the serverAddress structure.
        serverAddress.sin_family = hostInfo->h_addrtype;
        memcpy((char *) &serverAddress.sin_addr.s_addr,
               hostInfo->h_addr_list[0], hostInfo->h_length);
        serverAddress.sin_port = htons(serverPort);

        GRNDriver d = GRNDriver();
        d.mirror_sensors = mirroring;
        strcpy(d.trackName,trackName);
        d.stage = stage;
        d.setGRN(model);
        d.RUN_MODE=RUN_MODE;
        d.TRACK_RECOVERY=TRACK_RECOVERY;
        d.AVOID_OPPONENTS=AVOID_OPPONENTS;
        d.AGGRESSIVE_DRIVE=AGGRESSIVE_DRIVE;
        d.AGG_SECTOR_SIZE=AGG_SECTOR_SIZE;
        d.AGG_DEFAULT_VALUE=AGG_DEFAULT_VALUE;
        d.IS_ON_TRACK_VALUE=IS_ON_TRACK_VALUE;

        if (AGGRESSIVE_DRIVE){
            d.setAgressivityVector(this->aggVector);
        }

        bool shutdownClient=false;
        unsigned long curEpisode=0;
        do
        {
            /***********************************************************************************
             ************************* UDP client identification ********************************
             ***********************************************************************************/
            do
            {
                // Initialize the angles of rangefinders
                float angles[19];
                d.init(angles);
                string initString = SimpleParser::stringify(string("init"),angles,19);
                //cout << "Sending id to server: " << id << endl;
                initString.insert(0,id);
                //cout << "Sending init string to the server: " << initString << endl;
                if (sendto(socketDescriptor, initString.c_str(), initString.length(), 0,
                           (struct sockaddr *) &serverAddress,
                           sizeof(serverAddress)) < 0)
                {
                    cout << "cannot send data ";
                    CLOSE(socketDescriptor);
                    //                    this->setNote(0);
                    return false;
                }

                // wait until answer comes back, for up to UDP_CLIENT_TIMEUOT micro sec
                FD_ZERO(&readSet);
                FD_SET(socketDescriptor, &readSet);
                timeVal.tv_sec = 0;
                timeVal.tv_usec = UDP_CLIENT_TIMEUOT;

                if (select(socketDescriptor+1, &readSet, NULL, NULL, &timeVal))
                {
                    // Read data sent by the solorace server
                    memset(buf, 0x0, UDP_MSGLEN);  // Zero out the buffer.
                    numRead = recv(socketDescriptor, buf, UDP_MSGLEN, 0);
                    if (numRead < 0)
                    {
                        cout << "didn't get response from server...";
                    }
                    else
                    {
                        //cout << "Received: " << buf << endl;

                        if (strcmp(buf,"***identified***")==0)
                            break;
                    }
                }

            }  while(1);


            while(1)
            {
                // wait until answer comes back, for up to UDP_CLIENT_TIMEUOT micro sec
                FD_ZERO(&readSet);
                FD_SET(socketDescriptor, &readSet);
                timeVal.tv_sec = 0;
                timeVal.tv_usec = UDP_CLIENT_TIMEUOT;

                if (select(socketDescriptor+1, &readSet, NULL, NULL, &timeVal))
                {
                    // Read data sent by the solorace server
                    memset(buf, 0x0, UDP_MSGLEN);  // Zero out the buffer.
                    numRead = recv(socketDescriptor, buf, UDP_MSGLEN, 0);
                    if (numRead < 0)
                    {
                        cout << "didn't get response from server?";
                        CLOSE(socketDescriptor);
                        //                        this->setNote(0);
                        break;
                    }

#ifdef __UDP_CLIENT_VERBOSE__
                    //cout << "Received: " << buf << endl;
#endif
                    if (strcmp(buf,"***shutdown***")==0)
                    {
                        d.onShutdown();
                        shutdownClient = true;
                        cout << "Client Shutdown" << endl;
                        break;
                    }

                    if (strcmp(buf,"***restart***")==0)
                    {
                        d.onRestart();
                        //cout << "Client Restart" << endl;
                        break;
                    }
                    /**************************************************
                     * Compute The Action to send to the solorace sever
                     **************************************************/
                    // updating statistics
                    lastRaceDist = d.getDistRaced();
                    lastLapDist = d.getLastLapDist();
                    lastDamage = d.getDamage();
                    lastRaceNbSteps = currentStep;

                    if (AGGRESSIVE_DRIVE){
                        aggVector=d.getAggressivityVector();
                        lastSector=d.getLastSector();
                    }

                    bestLapTime = d.getBestLapTime();

                    currentStep++;

                    string action;

                    action = d.drive(string(buf));

                    memset(buf, 0x0, UDP_MSGLEN);
                    sprintf(buf,"%s",action.c_str());

                    numLap=d.getNumLap();

                    if (numLap> LAP_TO_RACE && d.getLastSector()>0){
                        cout<<"! End of race !"<<endl;
                        sprintf (buf, "(meta 1)");
                    }

                    if (CHK_ISONTRACK && (!d.isOnTrack() || !d.isOnGoodDirection()))
                    {
                        cout<<"-- Is On Track Fail --"<<endl;
                        sprintf (buf, "(meta 1)");
                    }

                    if (CHK_DAMAGE && d.getDamage()-lastDamage>DAMAGE_THRESHOLD)
                    {
                        cout<<"--- Damage Fail ---"<<endl;
                        sprintf(buf, "(meta 1)");
                    }
		    if (nbSimStep+currentStep>maxSteps) {
              cout << "--- End of the run ---" << endl;
		      sprintf(buf, "(meta 1)");
		    }

                    if (sendto(socketDescriptor, buf, strlen(buf)+1, 0,
                               (struct sockaddr *) &serverAddress,
                               sizeof(serverAddress)) < 0)
                    {
                        cout << "cannot send data ";
                        CLOSE(socketDescriptor);
                        //                        this->setNote(0);
                        return false;
                    }
#ifdef __UDP_CLIENT_VERBOSE__
                    else
                        //cout << "Sending " << buf << endl;
#endif
                }
                // 	    else
                // 	      {
                // 		cout << "** Server did not respond in 1 second.\n";
                // 	      }
		if (stage==BaseDriver::QUALIFYING && currentStep%1000==0) {
          cout << "Distance after " << currentStep << " steps: " << d.getDistRaced() << endl;
		}
            }
        } while(shutdownClient==false && ( (++curEpisode) != maxEpisodes));

        if (shutdownClient==false)
            d.onShutdown();
        CLOSE(socketDescriptor);
#ifdef WIN32
        WSACleanup();
#endif
        //    return 0;

        if (!DUAL_GRN && FITNESS_TYPE == 0){
            fit=d.getDistRaced();
            if (fit>RACE_DIST)
            {
                fit+=10000000/currentStep;
            }
        }

        //fit+=fitTicket;
        //cout << hostName << " : " << d.getDistRaced() << " / " << fit << " : " << currentStep << endl;

        //        this->setNote(fit);


        // updating statistics
        lastRaceDist=d.getDistRaced();
        lastLapDist=d.getLastLapDist();
        lastRaceNbSteps=currentStep;
        if (AGGRESSIVE_DRIVE){
            aggVector=d.getAggressivityVector();
            lastSector=d.getLastSector();
        }
        bestLapTime=d.getBestLapTime();

        return true;
    } catch (string e) {
        //        this->setNote(0);
        cout << "No fitness!!!" << endl;
        return false;
    }

    cout << "Raced distance after " << currentStep << ": " << lastRaceDist << endl;
}

void GRNRacer::setGRN(const GRNModel &newGRN) {
    grn=GRNModel(newGRN);
    grnInit=true;
}

void GRNRacer::setAggressivityVector(const vector<float> &agg) {
    aggVector=agg;
    aggVectorInit=true;
}

vector<float> GRNRacer::getAggressivityVector() {
    if (aggVectorInit) {
        return aggVector;
    } else {
        /*        aggVector=vector<float>();
         for (int i=0; i<AGG_NB_SECTORS; i++) {
         aggVector.push_back(1.0);
         }
         aggVectorInit=true;*/
        return aggVector;
    }
}

void GRNRacer::optimizeAgressivityVector_v7(int maxStep) {
    vector<float> bestAggEver=vector<float>(aggVector);
    float bestTimeEver=1000000.0;
    int stage=1;
    cout << " =====================================" <<endl;
    cout << "|              WARMING UP             |" << endl;
    cout << " =====================================" << endl;
    float variationValue = AGG_INIT_VARIATION;
    vector<int> sectorOptimState =vector<int>();
    int previousCrashSector=0;
    int ntry=0;
    int i=0;
    while (nbSimStep<maxStep) {
      // display and saving the current vector
      int nbSector = aggVector.size();
      for (int j=0; j<aggVector.size(); j++) {
    cout << j << ":" << aggVector[j] << "(" << sectorOptimState[j] << ")\t";
    if ((j+1)%10==0) cout << endl;
      }
      race(false);
      while (sectorOptimState.size()<aggVector.size()) {
	sectorOptimState.push_back(0);
      }
      ofstream file;
      stringstream filename;
      if (bestLapTime<100000000) {
    cout << "Best lap: " << bestLapTime << " (" << bestTimeEver << ")" << endl;
	filename << "aggVector_" << i << "_" << bestLapTime << ".agg";
      } else {
    cout << "Best lap: no time" << " (" << bestTimeEver << ")" << endl;
	filename << "aggVector_" << i << "_notime.agg";
      }
      nbSimStep+=lastRaceNbSteps;
      cout << "Sim steps: " << nbSimStep << " (" << lastRaceNbSteps << ")" << endl;
      if (AGG_WRITE_ALL_VECTORS) {
	file.open(filename.str().c_str());
	file.write((char*)&nbSector, sizeof(int));
	for (int j=0; j<aggVector.size(); j++) {
	  file.write((char*)&aggVector[j], sizeof(float));
	}
	file.close();
      }
      cout << "----------------------- " << i << " -----------------------" << endl;
      
      // saving best
      if (bestLapTime<bestTimeEver) {
	bestTimeEver=bestLapTime;
	bestAggEver=vector<float>(aggVector);
      }

      int initialSector=lastSector;
      if (lastSector<2 && numLap<=1) {
	// crashed in the very first sector or before, should be a server issue
    cout << "Race error, retrying" << endl;
      } else {
	// looking for an existing sector 1 or 2 sector before
    cout << "Crashed in sector " << lastSector << endl;
	if (lastSector-2>=0 && sectorOptimState[lastSector-2]>0) {
	  lastSector-=2;
	} else if (lastSector-1>=0 && sectorOptimState[lastSector-1]>0) {
	  lastSector-=1;
	}
    cout << "Updated sector " << lastSector << endl;
	if (numLap<3) {
	  if (sectorOptimState[lastSector]==0) {
        cout << "state 0 => downgrading 5 sectors: " ;
	    double desiredSpeed=fmax(50.0, aggVector[lastSector]-125.0);
	    for (int i=0; i<5; i++) {
	      aggVector[lastSector]=fmax(50.0, aggVector[lastSector]-125.0);
	      sectorOptimState[lastSector]=1;
          cout << lastSector << "(" << aggVector[lastSector] << ") ";
	      if (--lastSector<0) lastSector=aggVector.size()-1;
	    }
        cout << endl;
	  } else if (sectorOptimState[lastSector]==1) {
        cout << "state 1 => downgrading this sector... again: " << endl;
	    //looking forward the end of this sector
	    while (sectorOptimState[(lastSector+1)%aggVector.size()]==1) {
	      if (++lastSector>=aggVector.size()) lastSector=0;
	    }
	    double endOfSectorSpeed=fmax(50.0, aggVector[lastSector]-125.0);
	    while (sectorOptimState[lastSector]==1) {
	      aggVector[lastSector]=fmax(50.0, aggVector[lastSector]-125.0);
	      if (--lastSector<0) lastSector=aggVector.size()-1;
	    }
	    // the sector is at its lower limit, prepare the previous sector for next time
	    if (endOfSectorSpeed<75) {
	      for (int i=0; i<2; i++) {
		sectorOptimState[lastSector]=1;
		if (--lastSector<0) lastSector=aggVector.size()-1;
	      }
	    }
	  } else if (sectorOptimState[lastSector]==2) {
        cout << "Backtracking on sector " << lastSector << ": ";
	    // crashed while I was increasin the sector speed => backtrack
	    // looking forward the end of this sector
	    while (sectorOptimState[(lastSector+1)%aggVector.size()]==2) {
	      if (++lastSector>=aggVector.size()) lastSector=0;
	    }
	    // downgrading this sector
	    while (sectorOptimState[lastSector]==2) {
	      aggVector[lastSector]=fmax(50.0, aggVector[lastSector]-20.0);
          cout << lastSector << "=>" << aggVector[lastSector] << " ";
	      sectorOptimState[lastSector]=3;
	      if (--lastSector<0) lastSector=aggVector.size()-1;
	    }
        cout << endl;
	  } else if (sectorOptimState[lastSector]==3) {
        cout << "Reducing braking zone " << lastSector << " : ";
	    // crashed while I was reducing the braking zone => backtrack
	    // looking forward the end of this sector
	    while (sectorOptimState[(lastSector+1)%aggVector.size()]==3) {
	      if (++lastSector>=aggVector.size()) lastSector=0;
	    }
	    // looking for the first sector with 277km and marking the zone
	    while (aggVector[lastSector]<276 || aggVector[lastSector]>278) {
	      sectorOptimState[lastSector]=4;
	      if (--lastSector<0) lastSector=aggVector.size()-1;
	    }
	    // restoring previous value
	    aggVector[lastSector]=(aggVector[lastSector]-277.0)*1000.0;
        cout << lastSector << "=>" << aggVector[lastSector] << endl;
	    // keep marking the zone till the end of the sector
	    while (sectorOptimState[lastSector]==3) {
	      sectorOptimState[lastSector]=4;
	      if (--lastSector<0) lastSector=aggVector.size()-1;
	    }
	  } else if (sectorOptimState[lastSector]==4) {
        cout << "Safety procedure on sector " << lastSector << " : "<< endl;
	    // looking forward the end of this sector
	    while (sectorOptimState[(lastSector+1)%aggVector.size()]==4) {
	      if (++lastSector>=aggVector.size()) lastSector=0;
	    }
	    if (numLap==1) {
	      
	      // safety procedure, slighly reducing this sector of 10km/h
	      while (sectorOptimState[lastSector]==4) {
		aggVector[lastSector]=fmax(50.0, aggVector[lastSector]-10.0);
		if (--lastSector<0) lastSector=aggVector.size()-1;
	      }
	    } else if (numLap==2) {
	      while ((aggVector[lastSector]<276 || aggVector[lastSector]>278) && sectorOptimState[lastSector]==4) {
		if (--lastSector<0) lastSector=aggVector.size()-1;
	      }
	      if (sectorOptimState[lastSector]==4) {
		// restoring braking value
		aggVector[lastSector]=(aggVector[lastSector]-277.0)*1000.0;
	      } else {
		// increasing braking zone for this sector
		aggVector[lastSector]=aggVector[(lastSector+1)%aggVector.size()];
		sectorOptimState[lastSector]=4;
		aggVector[(lastSector+1)%aggVector.size()]=aggVector[(lastSector+2)%aggVector.size()];
	      }
	    }
	  }
	} else {
	  // if I finished the raced, I didn't crashed in sector 1 like they say
	  lastSector=aggVector.size();
	}
	lastSector-=1;
    cout << "Improving sectors before " << lastSector << endl;
	while (lastSector>0) {
	  if (sectorOptimState[lastSector]==0) {
	    // do nothing
	  } else if (sectorOptimState[lastSector]==1) {
	      // I can go to state 2
	      sectorOptimState[lastSector]=2;
	  } else if (sectorOptimState[lastSector]==2) {
	    // upgrading this sector
	    aggVector[lastSector]=fmin(275.0, aggVector[lastSector]+20);
	  } else if (sectorOptimState[lastSector]==3) {
	    // reducing the braking zone if it's the last sector
	    if (sectorOptimState[lastSector-1]!=3 || (aggVector[lastSector-1]>276.0 && aggVector[lastSector-1]<278.0)) {
	      aggVector[lastSector]=277.0+aggVector[lastSector]/1000.0;
	    }
	  } else if (sectorOptimState[lastSector]==4) {
	    // do nothing
	  }
	  lastSector--;
	}
	if (numLap==2) {
	  // in the second lap, I also passed the post crash sectors
	  int endBound=0;
	  endBound=initialSector;
	  int currentState=sectorOptimState[endBound];
	  // looking for the end of the sector
	  while (sectorOptimState[endBound]==currentState && endBound<aggVector.size()) {
	    endBound++;
	  }
	  lastSector=aggVector.size()-1;
	  while (lastSector>endBound) {
	    if (sectorOptimState[lastSector]==0) {
	      // do nothing
	    } else if (sectorOptimState[lastSector]==1) {
	      // I can go to state 2
	      sectorOptimState[lastSector]=2;
	    } else if (sectorOptimState[lastSector]==2) {
	      // upgrading this sector
	      aggVector[lastSector]=fmin(275.0, aggVector[lastSector]+20);
	    } else if (sectorOptimState[lastSector]==3) {
	      // reducing the braking zone if it's the last sector
	      if (sectorOptimState[lastSector-1]!=3 || (aggVector[lastSector-1]>276.0 && aggVector[lastSector-1]<278.0)) {
		aggVector[lastSector]=277.0+aggVector[lastSector]/1000.0;
	      }
	    } else if (sectorOptimState[lastSector]==4) {
	      // do nothing
	    }
	    lastSector--;
	  }
	}
      }
    }
    stringstream filename;
    ofstream file;
    filename << "aggVector_" << trackName << ".agg";
    file.open(filename.str().c_str());
    int nbSector=bestAggEver.size();
    file.write((char*)&nbSector, sizeof(int));
    for (int j=0; j<bestAggEver.size(); j++) {
        file.write((char*)&bestAggEver[j], sizeof(float));
    }
    file.close();

    cout << " ===============================================" << endl;
    cout << "| END OF OPTIMIZATION\t\t\t\t|" << endl;
    cout << "| Best time: " << bestTimeEver << "\t\t\t\t|" << endl;
    cout << "| Best aggVector saved in file: aggVector_" << trackName << ".agg\t|" << endl;
    cout << " ===============================================" << endl;
    exit(0);
}
