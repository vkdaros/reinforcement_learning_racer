/*
 *  GRNDriverGenome.h
 *  GRN_perso
 *
 *  Created by Sylvain Cusat-Blanc on 20/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef GRNRACER_H
#define GRNRACER_H

#include <vector>

#include "BaseDriver.h"

#include "GRNModel.h"

class GRNRacer {
public:
    GRNRacer() {};
    GRNRacer(const GRNModel& pGRN);

    bool race(bool mirroring);

    GRNModel getGRN();
    void setGRN(const GRNModel& newGRN);

    GRNModel grn;
    bool grnInit;

    vector<float> getAggressivityVector();
    void setAggressivityVector (const vector<float> &agg);

    void optimizeAgressivityVector_v7(int nbSteps);

    vector<float> aggVector;
    bool aggVectorInit;

    float lastRaceDist;
    float lastLapDist;
    float bestLapTime;
    int lastRaceNbSteps;
    int lastSector;
    int numLap;
    int nbSimStep;

    int LAP_TO_RACE;// = 2;
    int RACE_DIST;// = 9000;
    bool RUN_MODE;// = true;
    bool RUN_MIRROR;// = false;
    int FITNESS_TYPE;// = 1; //0: Distance + Ticket fitness ; 1: Sector + steps + ticket
    int TICKET_STEPS;// = 200;
    float FITNESS_SECTOR_LENGTH;// = 25.0f;
    bool CHK_TICKET;// = false;
    bool CHK_ISONTRACK;// = true;
    bool CHK_DAMAGE;// = true;
    bool CHK_JERKING;// = false;
    bool TRACK_RECOVERY;// = false;
    int DAMAGE_THRESHOLD;// = 10;
    string GRN_FILE_RUN;// = "exp16_11in_28p_norm2_a.grn"; //"pop_6_max4179.19.grn"; //"pop_24_max5943.52.grn"; //"pop_20_max2.56988.grn"; //"pop_19_max15.6625.grn" //"exp16_original.grn" //"pop_5_max1.54749.grn" //"new_grn_16.grn" //"exp16_11_inputs_noSteer.grn"  //"pop_6_max4179.19.grn" //"pop_10_max5127.04.grn"  //pop_34_max8.43137.grn" //"pop_17_max2.56323.grn" //"pop_15_max1.92503.grn" //"pop_24_max8.26017.grn" //"pop_6_max3.40678.grn" // "pop_29_max5.66278.grn" //"exp_16.grn" //"pop_78_max9570.6.grn"
    string GRN_DRIVING_FILE;// = "exp16_11_inputs_noSteer.grn";
    bool AGGRESSIVE_DRIVE;// = true;
    bool AGG_OPTIMIZE;// = true;
    float AGG_SECTOR_SIZE;// = 25.0;
    float AGG_DEFAULT_VALUE;// = 300.0f; //0.0f; //1.5f;
    float AGG_MAX_VALUE;// = 300.0f; //2.f;
    float AGG_INIT_VARIATION;// = 125; //87.5; //1.0f;
    bool AGG_WRITE_ALL_VECTORS;
    bool AGG_LOADFILE;// = false;
    string AGG_FILENAME;// = "aggVector_49_127.486.agg"; // Olethros
    bool AVOID_OPPONENTS;// = false;
    float IS_ON_TRACK_VALUE;

    char* hostName;
    unsigned int serverPort;
    char* id;
    char* trackName;
    BaseDriver::tstage stage;
    int maxSteps;
    int maxEpisodes;
};

#endif
