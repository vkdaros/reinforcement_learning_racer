/*
 *  Protein.cpp
 *  GRN_perso
 *
 *  Created by Sylvain Cusat-Blanc on 19/10/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "Protein.h"

Protein::Protein(int ID, int t, double conc, int enh, int inh) {
	id=ID;
	concentration=conc;
	enhancer=enh;
	inhibiter=inh;
	type=t;
}

Protein::Protein(){
	id=0;
	concentration=0;
	enhancer=0;
	inhibiter=0;
	type=0;
}

Protein::Protein(const Protein& p) {
	id=p.id;
	concentration=p.concentration;
	enhancer=p.enhancer;
	inhibiter=p.inhibiter;
	type=p.type;
}

Protein Protein::copy() {
	return Protein(id, type, concentration, enhancer, inhibiter);
}