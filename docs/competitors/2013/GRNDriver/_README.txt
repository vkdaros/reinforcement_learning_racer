   ___________________________
 /                             \
|    GRN Driver controller      |
|                               |
|       St�phane Sanchez        |
|     Sylvain Cussat-Blanc      |
|                               |
| IRIT - University of Toulouse |
 \_____________________________/

================
Compile command:
================
make

================
Warm up command:
================
./client id:SCR host:SERVER_IP port:PORT maxSteps:100000 maxEpisodes:1 track:TRACK_NAME stage:0
The server should restart multiple times but it's normal: the practice is restarted each time the car is crashing. A step counter makes sure the simulation step doesn't go over the maxSteps limit. We are not sure the maxEpisodes command is working properly with a value higher than 1. Please use it carefully :)

===================
Qualifying command:
===================
./client id:SCR host:SERVER_IP port:PORT maxSteps:100000 maxEpisodes:1 track:TRACK_NAME stage:1
the maxSteps limit is also respected here. We are not sure the maxEpisodes command is working properly with a value higher than 1. Please use it carefully :) 

=============
Race command:
=============
./client id:SCR host:SERVER_IP port:PORT maxSteps:100000 maxEpisodes:1 track:TRACK_NAME stage:2
the maxSteps limit is also respected here. We are not sure the maxEpisodes command is working properly with a value higher than 1. Please use it carefully :) 
