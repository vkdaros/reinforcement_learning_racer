#ifdef WIN32
#include <WinSock.h>
#else
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#endif

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cstdio>

#include<iostream>
#include<fstream>
#include<iomanip>
#include<cmath>
#include<climits>
#include<cfloat>// For FLT_MAX
#include<limits>// For numeric_limits
#include<semaphore.h>
#include<signal.h>
#include<cassert>
#include<sstream>
#include<stdio.h>
#include<stdlib.h>

#include "GRNDriver.h"
#include "GRNModel.h"
#include "GRNRacer.h"

#include "DriverConstants.h"

using namespace std;

void evolutionRun();
void testRun(string pFilename);

void warmUpMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName);
void qualifMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName);
void raceMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName);
void testMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName);

void parse_args(int argc, char *argv[], char *hostName, unsigned int &serverPort, char *id, unsigned int &maxEpisodes,
                unsigned int &maxSteps, char *trackName, BaseDriver::tstage &stage, char* grnFileName)
{
    int		i;

    // Set default values
    maxEpisodes=0;
    maxSteps=0;
    serverPort=3001;
    strcpy(hostName,"localhost");
    strcpy(id,"SCR");
    strcpy(grnFileName, "exp16_11in_28p_norm2_a.grn");
    //    noise=false;
    //    noiseAVG=0;
    //    noiseSTD=0.05;
    //    seed=0;
    strcpy(trackName,"unknown");
    stage=BaseDriver::UNKNOWN;


    i = 1;
    while (i < argc)
    {
        if (strncmp(argv[i], "host:", 5) == 0)
        {
            sprintf(hostName, "%s", argv[i]+5);
            i++;
        }
        else if (strncmp(argv[i], "port:", 5) == 0)
        {
            sscanf(argv[i],"port:%d",&serverPort);
            i++;
        }
        else if (strncmp(argv[i], "id:", 3) == 0)
        {
            sprintf(id, "%s", argv[i]+3);
            i++;
        }
        else if (strncmp(argv[i], "maxEpisodes:", 12) == 0)
        {
            sscanf(argv[i],"maxEpisodes:%ud",&maxEpisodes);
            i++;
        }
        else if (strncmp(argv[i], "maxSteps:", 9) == 0)
        {
            sscanf(argv[i],"maxSteps:%ud",&maxSteps);
            i++;
        }
        else if (strncmp(argv[i], "track:", 6) == 0)
        {
            sscanf(argv[i],"track:%s",trackName);
            i++;
        }
        else if (strncmp(argv[i], "stage:", 6) == 0)
        {
            int temp;
            sscanf(argv[i],"stage:%d",&temp);
            stage = (BaseDriver::tstage) temp;
            i++;
            if (stage<BaseDriver::WARMUP || stage > BaseDriver::RACE)
                stage = BaseDriver::UNKNOWN;
        } else if (strncmp(argv[i], "grn:", 4) == 0)
        {
            sscanf(argv[i],"grn:%s",grnFileName);
            //cerr << grnFileName << endl;
            i++;
        }
        else {
            i++;		/* ignore bad args */
        }
    }
}


int main(int argc, char **argv)
{
    char hostName[1000];
    unsigned int serverPort;
    char id[1000];
    unsigned int maxEpisodes;
    unsigned int maxSteps;
    char trackName[1000];
    char grnFileName[1000];
    BaseDriver::tstage stage;

    parse_args(argc,argv,hostName,serverPort,id,maxEpisodes,maxSteps,trackName,stage, grnFileName);

    if (stage==BaseDriver::WARMUP) {
        warmUpMode(hostName,id, serverPort,maxEpisodes,maxSteps,trackName,stage, grnFileName);
    } else if (stage==BaseDriver::QUALIFYING) {
        qualifMode(hostName,id, serverPort,maxEpisodes,maxSteps,trackName,stage, grnFileName);
    } else if (stage==BaseDriver::RACE) {
        raceMode(hostName,id, serverPort,maxEpisodes,maxSteps,trackName,stage, grnFileName);
    } else if (stage==BaseDriver::UNKNOWN) {
        testMode(hostName,id, serverPort,maxEpisodes,maxSteps,trackName,stage, grnFileName);
    }
}



void warmUpMode(char *hostName, char *id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char *trackName, BaseDriver::tstage stage, char* grnFileName) {
    GRNRacer* racer = new GRNRacer();
    ifstream file;
    file.open(grnFileName);
    if (!file.is_open()) {
        cout << "GRN model file " << grnFileName << " not found" << endl;
        exit(-1);
    }
    file.close();

    GRNModel grn = GRNModel(string(grnFileName));
    vector<float> aggVector=vector<float>();

    racer->setGRN(grn);
    racer->setAggressivityVector(aggVector);
    racer->AGGRESSIVE_DRIVE=true;
    racer->AGG_DEFAULT_VALUE=300.0;
    racer->AGG_INIT_VARIATION=125.0;
    racer->AGG_LOADFILE=false;
    racer->AGG_MAX_VALUE=300.0;
    racer->AGG_OPTIMIZE=true;
    racer->AGG_SECTOR_SIZE=25.0;
    racer->AGG_WRITE_ALL_VECTORS=false;
    racer->AVOID_OPPONENTS=false;
    racer->CHK_DAMAGE=false;
    racer->CHK_ISONTRACK=true;
    racer->CHK_JERKING=false;
    racer->CHK_TICKET=false;
    racer->DAMAGE_THRESHOLD=10.0;
    racer->GRN_FILE_RUN=string(grnFileName);
    racer->LAP_TO_RACE=2;
    racer->RACE_DIST=9000;
    racer->RUN_MIRROR=false;
    racer->RUN_MODE=true;
    racer->TRACK_RECOVERY=false;
    racer->IS_ON_TRACK_VALUE=0.8;

    racer->hostName=hostName;
    racer->id=id;
    racer->serverPort=serverPort;
    racer->trackName=trackName;
    racer->stage=stage;
    racer->maxEpisodes=maxEpisodes;
    racer->maxSteps=maxSteps;

    racer->optimizeAgressivityVector_v7(maxSteps);

}

void qualifMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName) {
    GRNRacer* racer = new GRNRacer();
    cout << "Loading GRN " << grnFileName << endl;
    GRNModel grn = GRNModel(string(grnFileName));
    cout << grn.toString() << endl;

    vector<float> aggVector=vector<float>();
    ifstream file;
    stringstream AGG_FILENAME;
    AGG_FILENAME << "aggVector_" << trackName << ".agg";
    file.open(AGG_FILENAME.str().c_str());
    if (file.is_open()) {
        cout << "Loading aggressivity vector " << AGG_FILENAME.str() << endl;
        int nbAgg;
        file.read((char*)&nbAgg, sizeof(int));
        //  //cerr << nbAgg << endl;
        for (int i=0; i<nbAgg; i++) {
            float val;
            file.read((char*)&val, sizeof(float));
            aggVector.push_back(val);//(double)rand()/RAND_MAX*2.0-1.0);
            cout << aggVector.back() << "  ";
        }
        cout << endl;
        racer->AGGRESSIVE_DRIVE=true;
    } else {
        cout << "No aggressivity vector found!" << endl;
        cout << "Runing in safe mode" << endl;
        racer->AGGRESSIVE_DRIVE=false;
    }


    racer->setGRN(grn);
    racer->setAggressivityVector(aggVector);
    racer->AGG_DEFAULT_VALUE=100.0;
    racer->AGG_INIT_VARIATION=125.0;
    racer->AGG_LOADFILE=false;
    racer->AGG_MAX_VALUE=100.0;
    racer->AGG_OPTIMIZE=false;
    racer->AGG_SECTOR_SIZE=25.0;
    racer->AVOID_OPPONENTS=false;
    racer->CHK_DAMAGE=false;
    racer->CHK_ISONTRACK=false;
    racer->CHK_JERKING=false;
    racer->CHK_TICKET=false;
    racer->DAMAGE_THRESHOLD=1000000.0;
    racer->GRN_FILE_RUN=string(grnFileName);
    racer->LAP_TO_RACE=1000000;
    racer->RACE_DIST=90000000;
    racer->RUN_MIRROR=false;
    racer->RUN_MODE=true;
    racer->TRACK_RECOVERY=true;
    racer->IS_ON_TRACK_VALUE=1.0;

    racer->hostName=hostName;
    racer->id=id;
    racer->serverPort=serverPort;
    racer->trackName=trackName;
    racer->stage=stage;
    racer->maxEpisodes=maxEpisodes;
    racer->maxSteps=maxSteps;

    racer->race(false);
}

void raceMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName) {
    GRNRacer* racer = new GRNRacer();
    cout << "Loading GRN " << grnFileName << endl;
    GRNModel grn = GRNModel(string(grnFileName));
    cout << grn.toString() << endl;

    vector<float> aggVector=vector<float>();
    ifstream file;
    stringstream AGG_FILENAME;
    AGG_FILENAME << "aggVector_" << trackName << ".agg";
    file.open(AGG_FILENAME.str().c_str());
    if (file.is_open()) {
        cout << "Loading aggressivity vector " << AGG_FILENAME.str() << endl;
        int nbAgg;
        file.read((char*)&nbAgg, sizeof(int));
        //  //cerr << nbAgg << endl;
        for (int i=0; i<nbAgg; i++) {
            float val;
            file.read((char*)&val, sizeof(float));
            aggVector.push_back(val);//(double)rand()/RAND_MAX*2.0-1.0);
            cout << aggVector.back() << "  ";
        }
        racer->AGGRESSIVE_DRIVE=true;
        cout << endl;
    } else {
        cout << "No aggressivity vector found!" << endl;
        cout << "Runing in safe mode" << endl;
        racer->AGGRESSIVE_DRIVE=false;
    }


    racer->setGRN(grn);
    racer->setAggressivityVector(aggVector);
    racer->AGG_DEFAULT_VALUE=100.0;
    racer->AGG_INIT_VARIATION=125.0;
    racer->AGG_LOADFILE=false;
    racer->AGG_MAX_VALUE=100.0;
    racer->AGG_OPTIMIZE=false;
    racer->AGG_SECTOR_SIZE=25.0;
    racer->AVOID_OPPONENTS=true;
    racer->CHK_DAMAGE=false;
    racer->CHK_ISONTRACK=false;
    racer->CHK_JERKING=false;
    racer->CHK_TICKET=false;
    racer->DAMAGE_THRESHOLD=1000000.0;
    racer->GRN_FILE_RUN=string(grnFileName);
    racer->LAP_TO_RACE=1000000;
    racer->RACE_DIST=90000000;
    racer->RUN_MIRROR=false;
    racer->RUN_MODE=true;
    racer->TRACK_RECOVERY=true;
    racer->IS_ON_TRACK_VALUE=1.0;

    racer->hostName=hostName;
    racer->id=id;
    racer->serverPort=serverPort;
    racer->trackName=trackName;
    racer->stage=stage;
    racer->maxEpisodes=maxEpisodes;
    racer->maxSteps=maxSteps;

    racer->race(false);
}


void testMode(char* hostName, char* id, unsigned int serverPort, unsigned int maxEpisodes, unsigned int maxSteps, char* trackName, BaseDriver::tstage stage, char* grnFileName) {
    GRNRacer* racer = new GRNRacer();
    cout << "Loading GRN " << grnFileName << endl;
    GRNModel grn = GRNModel(string(grnFileName));
    cout << grn.toString() << endl;

    vector<float> aggVector=vector<float>();
    ifstream file;
    stringstream AGG_FILENAME;
    AGG_FILENAME << "aggVector_" << trackName << ".agg";
    file.open(AGG_FILENAME.str().c_str());
    if (file.is_open()) {
        cout << "Loading aggressivity vector " << AGG_FILENAME.str() << endl;
        int nbAgg;
        file.read((char*)&nbAgg, sizeof(int));
        //  //cerr << nbAgg << endl;
        for (int i=0; i<nbAgg; i++) {
            float val;
            file.read((char*)&val, sizeof(float));
            aggVector.push_back(val);//(double)rand()/RAND_MAX*2.0-1.0);
            cout << aggVector.back() << "  ";
        }
        cout << endl;
    } else {
        cout << "No aggressivity vector found!" << endl;
        cout << "Runing in safe mode" << endl;
    }


    racer->setGRN(grn);
    racer->setAggressivityVector(aggVector);
    racer->AGGRESSIVE_DRIVE=false;
    racer->AGG_DEFAULT_VALUE=100.0;
    racer->AGG_INIT_VARIATION=125.0;
    racer->AGG_LOADFILE=false;
    racer->AGG_MAX_VALUE=100.0;
    racer->AGG_OPTIMIZE=false;
    racer->AGG_SECTOR_SIZE=25.0;
    racer->AVOID_OPPONENTS=true;
    racer->CHK_DAMAGE=false;
    racer->CHK_ISONTRACK=false;
    racer->CHK_JERKING=false;
    racer->CHK_TICKET=false;
    racer->DAMAGE_THRESHOLD=1000000.0;
    racer->GRN_FILE_RUN=string(grnFileName);
    racer->LAP_TO_RACE=1000000;
    racer->RACE_DIST=90000000;
    racer->RUN_MIRROR=false;
    racer->RUN_MODE=true;
    racer->TRACK_RECOVERY=true;
    racer->IS_ON_TRACK_VALUE=1.0;

    racer->hostName=hostName;
    racer->id=id;
    racer->serverPort=serverPort;
    racer->trackName=trackName;
    racer->stage=stage;
    racer->maxEpisodes=maxEpisodes;
    racer->maxSteps=maxSteps;

    racer->race(racer->RUN_MIRROR);
}
