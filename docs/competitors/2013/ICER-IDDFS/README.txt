How to run our controller ICER-IDDFS:

Our controller can be executed by
java -jar ICER_IDDFS.jar host:localhost port:3001 id:SCR maxEpisodes:1 maxSteps:100000 trackName:<trackName> stage:<stage>
The track name is specified at <trackName>, and three argument types are available for <stage>
0   for the warm-up stage
1   for the qualifying stage
2   for the race stage

Assume that the jar file is in C:\Users\shirakawa\Desktop\ICER, and, for example, for a warm-up at track1, you should enter

C:\Users\shirakawa\Desktop\ICER>java -jar ICER_IDDFS.jar host:localhost port:3001 id:SCR maxEpisodes:1 maxSteps:100000 trackName:track1 stage:0

After the warm-up, five track-data files will be generated in the same folder as the jar file. These files will be used in the qualifying and race stage at the same track. 

Next, for the qualifying stage, you should enter 

C:\Users\shirakawa\Desktop\ICER>java -jar ICER_IDDFS.jar host:localhost port:3001 id:SCR maxEpisodes:1 maxSteps:100000 trackName:track1 stage:1

Finally, for the race stage,  you should enter 

C:\Users\shirakawa\Desktop\ICER>java -jar ICER_IDDFS.jar host:localhost port:3001 id:SCR maxEpisodes:1 maxSteps:100000 trackName:track1 stage:2

Thank You!
