/***************************************************************************
 
    file                 : SimpleDriver.cpp
    copyright            : (C) 2007 Daniele Loiacono
 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "SimpleDriver.h"



/* Stuck constants*/
const int SimpleDriver::stuckTime = 150;
const float SimpleDriver::stuckAngle = .523598775; //PI/6

/* Accel and Brake Constants*/
const float SimpleDriver::maxSpeedDist=70;
const float SimpleDriver::maxSpeed=150;
const float SimpleDriver::sin5 = 0.08716;
const float SimpleDriver::cos5 = 0.99619;

/* Steering constants*/
const float SimpleDriver::steerLock=0.785398;
const float SimpleDriver::steerSensitivityOffset=80.0;
const float SimpleDriver::wheelSensitivityCoeff=1;

/* ABS Filter Constants */
const float SimpleDriver::wheelRadius[4]={0.3179,0.3179,0.3276,0.3276};
const float SimpleDriver::absSlip=2.0;
const float SimpleDriver::absRange=3.0;
const float SimpleDriver::absMinSpeed=3.0;

/* Clutch constants */
const float SimpleDriver::clutchMax=0.5;
const float SimpleDriver::clutchDelta=0.05;
const float SimpleDriver::clutchRange=0.82;
const float SimpleDriver::clutchDeltaTime=0.02;
const float SimpleDriver::clutchDeltaRaced=10;
const float SimpleDriver::clutchDec=0.01;
const float SimpleDriver::clutchMaxModifier=1.3;
const float SimpleDriver::clutchMaxTime=1.5;

float steer_1 = 0;
float accelbrake_1 = 0;
float clutch_1 = 0;
int ciclos = 0;
float Tangles[19];
float target_Speed=0;

float tiempo=0;
float tiempo_1=0;

float pos=0;
float pos_1=0;

float damage=0;
float damage_1=0;

int vuelta=0;

float width=0;

double TolOvertake[11]={20,20,18,16,14,12,10,10,10,10,10};
double TolOvertake1[11]={1,1,0.75,0.75,0.5,0.5,0.3,0.3,0.3,0.3,0.3};
double IncOvertake[11]={0.15,0.15,0.14,0.13,0.12,0.12,0.10,0.10,0.10,0.10,0.10};

double WSteer[4]={1,0.1,0.1,1};
double Steer[19]={1,1,1,1,1,1.7,1.5,1.0,0.5,
                                     0,
                                     -0.5,-1.0,-1.5,-1.7,-1,-1,-1,-1,-1};


double ST1=0.75;
double ST2=0.75;
double ST3=0;
double ST4=0;
double ST5=1.5;

double TS1=2;
double TS2=2;
double TS3=0;
double TS4=0;
double TS5=0;

int GE1[6]={9500,9500,9500,9500,9000,0};
int GE2[6]={0,4000,6300,7000,7300,7300}; 

double o0[36]; 
double o1[36]; 

double TIEMPO = 0.0;
double PENALIZACION = 0.0;

    
double OV1 = 100;
double OV2 = 100;
double OV3 = 100;
double OV4 = 50;
double OV5 = 30;


double margen = 10;

double **MAPA;
int T_ajuste=0;
int tramo = 0;
int tramo1 = 0;

int
SimpleDriver::getGear(CarState &cs)
{

    int gear = cs.getGear();
    int rpm  = cs.getRpm();

    if (gear<1)
        return 1;   
    if (gear <6 && rpm >= GE1[gear-1] && ciclos%50==0)
        return gear + 1;
    else
        if (gear > 1 && rpm <= GE2[gear-1] && ciclos%50==0)
            return gear - 1;
        else
            return gear;
}

bool flag = false;

float SimpleDriver::getSteer(CarState &cs) {
    float devolver;
    float speed = cs.getSpeedX() / 3.6;  
    if (abs(cs.getTrackPos())<1 && abs(cs.getAngle())<0.5){  
        
         
       double mayor = 0;
       int ind = 0;
       
       for (int i=4;i<=14;i++){
           if (mayor==cs.getTrack(i) && fabs(i-9)<fabs(ind-9)){  
              mayor = cs.getTrack(i);
              ind = i;                 
           } 
           if (mayor<cs.getTrack(i)){
              mayor = cs.getTrack(i);
              ind = i;                          
           } 
       }
       
       double angulo = -(Tangles[ind])*PI/180;
       double angulod = -(Tangles[ind+1])*PI/180;
       double anguloi = -(Tangles[ind-1])*PI/180;   
       double angulod1 = -(Tangles[ind+2])*PI/180;
       double anguloi1 = -(Tangles[ind-2])*PI/180;    
       double angulod2 = -(Tangles[ind+3])*PI/180;
       double anguloi2 = -(Tangles[ind-3])*PI/180;
       
       double t = cs.getTrack(ind);
       double td = cs.getTrack(ind+1);
       double ti = cs.getTrack(ind-1);    
       double td1 = cs.getTrack(ind+2);
       double ti1 = cs.getTrack(ind-2);   
       double td2 = cs.getTrack(ind+3);
       double ti2 = cs.getTrack(ind-3);
       
       devolver =  ST1*angulo;
       
       devolver+=  ST2*(((angulod*td)+(anguloi*ti))/(0.1+ti+td)); 
       devolver+=  ST3*(((angulod1*td1)+(anguloi1*ti1))/(0.1+ti1+td1)); 
       devolver+=  ST4*(((angulod2*td2)+(anguloi2*ti2))/(0.1+ti2+td2)); 
        
       if (fabs(cs.getTrackPos())<0.75 && (ind==9 || cs.getTrack(9)>ST5*cs.getSpeedX()/3.6 || cs.getTrack(9)>190)){
            devolver = 0.5*cs.getAngle();
       }
    }else{   
        float targetAngle=(3*cs.getAngle()-(cs.getTrackPos())*0.5);    
        devolver = (targetAngle)/steerLock;
    }
    
        if (devolver < -1)
            devolver = -1;
        if (devolver > 1)
            devolver = 1;
            
    steer_1=devolver; 
    
    int lado = 0;       
    for (int i=8;i>2;i-=1){ lado-=min((float)cs.getOpponents(i+8),(float)cs.getTrack(i)); }    
    for (int i=10;i<16;i+=1){ lado+=min((float)cs.getOpponents(i+8),(float)cs.getTrack(i)); }
    
    double ov = 0.35;
    if (fabs(lado)<100){ lado = 0; }
    if (lado>0){ lado = 1; }
    if (lado<0){ lado = -1; } 
   
   
    
    
    if((o0[17]<50  || o0[18]<margen/sin(10*PI/180)  || o0[16]<margen/sin(10*PI/180) || o0[15]<margen/sin(20*PI/180) || o0[19]<margen/sin(20*PI/180))){devolver+=lado*width/OV1;}   
        
    if ((o0[13]<1*margen/sin(40*PI/180) || o0[14]<1*margen/sin(30*PI/180) || o0[15]<1*margen/sin(20*PI/180) || o0[16]<1*margen/sin(20*PI/180))){devolver-=2*width/OV2;}
    else if ((o0[18]<1*margen/sin(20*PI/180) || o0[19]<1*margen/sin(20*PI/180) || o0[20]<1*margen/sin(30*PI/180) || o0[21]<1*margen/sin(40*PI/180))){devolver+=2*width/OV2;}
    
    if ((o0[10]<15 || o0[11]<15 || o0[12]<15)){devolver-=width/OV3;}
    if ((o0[22]<15 || o0[23]<15 || o0[24]<15)){devolver+=width/OV3;}

    if ((o0[ 3]<15 || o0[ 4]<15 || o0[ 5]<15 || o0[ 6]<15 || o0[ 7]<15 || o0[ 8]<15 || o0[ 9]<15)){devolver-=width/OV4;}
    if ((o0[25]<15 || o0[26]<15 || o0[27]<15 || o0[28]<15 || o0[29]<15 || o0[30]<15 || o0[31]<15)){devolver+=width/OV4;}     
    
    
    if (devolver>steer_1+ov)
       devolver=steer_1+ov+0.01;
    if (devolver<steer_1-ov)
       devolver=steer_1-ov-0.01;
       
    return devolver;
    

}
float SimpleDriver::getAccel(CarState &cs) {
    float devolver=1;
    if (fabs(cs.getTrackPos()) < 1){    
        target_Speed  = TS1*cs.getTrack(9);
        target_Speed += TS2*max(cs.getTrack(7),cs.getTrack(11));        
        target_Speed += TS3*min(cs.getTrack(7),cs.getTrack(11));  
        target_Speed += TS4*max(cs.getTrack(5),cs.getTrack(13));        
        target_Speed += TS5*min(cs.getTrack(5),cs.getTrack(13));     
        
       if ( (cs.getTrack(9)>=150 || max(cs.getTrack(8),cs.getTrack(10))>=150))
          target_Speed=300;
       
          
          
       if (cs.getDamage()>1000 && cs.getDamage()<=2000 && stage != BaseDriver::WARMUP)
          target_Speed*=0.98;
       if (cs.getDamage()>2000 && cs.getDamage()<=3000 && stage != BaseDriver::WARMUP)
          target_Speed*=0.96;
       if (cs.getDamage()>3000 && cs.getDamage()<=4000 && stage != BaseDriver::WARMUP)
          target_Speed*=0.94;
       if (cs.getDamage()>4000 && cs.getDamage()<=5000 && stage != BaseDriver::WARMUP)
          target_Speed*=0.92;
       if (cs.getDamage()>5000 && cs.getDamage()<=6000 && stage != BaseDriver::WARMUP)
          target_Speed*=0.90;
       if (cs.getDamage()>6000 && stage != BaseDriver::WARMUP)
          target_Speed*=0.88;
                
        if (target_Speed<40) target_Speed=40;
        if (target_Speed>300) target_Speed=300;
        if (target_Speed>200 && MAPA[0][tramo]<1.0) target_Speed=200;
        
        if (MAPA[0][tramo]<0.2){
           MAPA[0][tramo]=0.2;
        }
        
        target_Speed*=MAPA[0][tramo];
        
                
        if (target_Speed<40) target_Speed=40;
        if (target_Speed>300) target_Speed=300;
        if (target_Speed>200 && MAPA[0][tramo]<1.0) target_Speed=200;
                
        if (MAPA[0][tramo]<=0.2){
           target_Speed=20;
        }
        
        devolver = 2/(1+exp(cs.getSpeedX() - target_Speed)) - 1;        
        
        if (target_Speed > 290)
            devolver=1;       
    } 
    
    float de1=devolver;    
        
    if (cs.getSpeedX()>100 && o0[17]-o1[17]<-0.5 && o0[17]<20){devolver=-0.1;}
    
    if (cs.getSpeedX()>100 && o0[16]-o1[16]<-0.5 && o0[16]<4/sin(10*PI/180)){devolver=-0.1;}
    if (cs.getSpeedX()>100 && o0[18]-o1[18]<-0.5 && o0[18]<4/sin(10*PI/180)){devolver=-0.1;}
    
    if (cs.getSpeedX()>100 && o0[15]-o1[15]<-0.5 && o0[15]<4/sin(20*PI/180)){devolver=-0.1;}
    if (cs.getSpeedX()>100 && o0[19]-o1[19]<-0.5 && o0[19]<4/sin(20*PI/180)){devolver=-0.1;}
    /*
    if (cs.getSpeedX()>100 && o0[14]-o1[14]<-0.5 && o0[14]<4/sin(30*PI/180)){devolver=-0.1;}
    if (cs.getSpeedX()>100 && o0[20]-o1[20]<-0.5 && o0[20]<4/sin(30*PI/180)){devolver=-0.1;}
        
    if (cs.getSpeedX()>100 && o0[13]-o1[13]<-0.5 && o0[13]<4/sin(40*PI/180)){devolver=-0.1;}
    if (cs.getSpeedX()>100 && o0[21]-o1[21]<-0.5 && o0[21]<4/sin(40*PI/180)){devolver=-0.1;}*/
    
    if (o0[17]<15 && cs.getSpeedX()>50){devolver=-1;}
    if (o0[16]<2/sin(10*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    if (o0[18]<2/sin(10*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    /*if (o0[15]<2/sin(20*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    if (o0[19]<2/sin(20*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    if (o0[14]<2/sin(30*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    if (o0[20]<2/sin(30*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    if (o0[13]<2/sin(40*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}
    if (o0[21]<2/sin(40*PI/180) && cs.getSpeedX()>50){devolver=-0.1;}*/
          
    if (de1!=devolver){
       flag = true;
    }
    else{
       if (ciclos%(2+3*(int)ceil(cs.getDamage()/1000))==0)
          flag = false;
    }
    
    if (flag == true && stage != BaseDriver::WARMUP)
       devolver=-1;
                
    if (de1!=devolver){
  //     cout<<"ACC   "<<cs.getCurLapTime()<<endl;
    }
    if (fabs(cs.getTrackPos()) > 1 && cs.getSpeedX()>75){devolver*=0.3;}
    if (fabs(cs.getTrackPos()) > 1 && cs.getSpeedX()<=75){devolver*=0.6;}
    
    if ((cs.getSpeedZ())>12){
//       devolver=-1;
       }
    
    return devolver;
}



CarControl SimpleDriver::wDrive(CarState cs)
{
    FILE * fo;
//Control de Vueltas
    pos_1 = pos;     
    pos = cs.getTrackPos();
    tiempo_1 = tiempo;     
    tiempo = cs.getCurLapTime();
    damage_1 = damage;     
    damage = cs.getDamage();
    
    if (ciclos%100==0){    
    cout<<cs.getDamage()<<endl;          
    }
    
          
    if (tiempo_1>tiempo){ 
       vuelta++; 
       TIEMPO+=cs.getLastLapTime();
    }   
    tramo = (int)ceil(cs.getDistFromStart());
    if (((fabs(pos)>0.9 && fabs(pos_1)<=0.9) || damage_1+10<damage || stuck == stuckTime) && stage == BaseDriver::WARMUP){ 
                    //    cout<<damage<<endl;
       for (int i=tramo;i>max(0,tramo-150);i--){
              if (MAPA[1][i]!=vuelta){
                  if (MAPA[0][i]>1)
                     MAPA[0][i]=1;
                  else
                     if (1800-TIEMPO > 5*cs.getLastLapTime())
                        MAPA[0][i]*=0.9;
                     else
                        MAPA[0][i]*=0.8;
              }
              MAPA[1][i]=vuelta;
       }
       fo = fopen(BaseDriver::trackName,"w");
         fprintf (fo,"%d\n",T_ajuste);
       for (int i=0;i<T_ajuste;i++){
            fprintf (fo,"%.2f\n",MAPA[0][i]);
       }  
       fclose(fo);      
    }  
    else{ 
        if ((cs.getZ()>0.5) && stage == BaseDriver::WARMUP){ 
           for (int i=max(0,tramo-10);i>max(0,tramo-25);i--){
                  if (MAPA[1][i]!=vuelta){
                        MAPA[0][i]*=0.5;
                  }
                  MAPA[1][i]=vuelta;
           }
           fo = fopen(BaseDriver::trackName,"w");
             fprintf (fo,"%d\n",T_ajuste);
           for (int i=0;i<T_ajuste;i++){
                fprintf (fo,"%.2f\n",MAPA[0][i]);
           }  
           fclose(fo);      
        } 
    }
     if (tiempo_1>tiempo && vuelta > 2 && stage == BaseDriver::WARMUP && 1800-TIEMPO > 5*cs.getLastLapTime()){
      for (int i=0;i<T_ajuste;i++){
           if (MAPA[1][i]==0 && MAPA[0][i]>=1.0 && MAPA[0][i]<=2.0){
              MAPA[0][i]*=1.25;                        
              MAPA[1][i]=vuelta-1;                  
           }
       }
       fo = fopen(BaseDriver::trackName,"w");
         fprintf (fo,"%d\n",T_ajuste);
       for (int i=0;i<T_ajuste;i++){
            fprintf (fo,"%.2f\n",MAPA[0][i]);
       }  
       fclose(fo);               
     }   
    
    
    
    if (cs.getDistRaced()<0.5){
       width = cs.getTrack(0)+cs.getTrack(17);   
       T_ajuste = (int)cs.getDistFromStart()+520;                   
    }
    else{    
        MAPA[2][tramo]=cs.getSpeedX();
        MAPA[3][tramo]=target_Speed;  
        MAPA[2][max(0,tramo-1)]=cs.getSpeedX();
        MAPA[3][max(0,tramo-1)]=target_Speed;   
        MAPA[2][max(0,tramo-2)]=cs.getSpeedX();
        MAPA[3][max(0,tramo-3)]=target_Speed;   
        MAPA[2][max(0,tramo-4)]=cs.getSpeedX();
        MAPA[3][max(0,tramo-4)]=target_Speed;   
    }
    
     if (stage == BaseDriver::WARMUP && cs.getDistRaced()<0.5){
         T_ajuste = (int)cs.getDistFromStart()+520;     
         fo = fopen(BaseDriver::trackName,"w");
         MAPA = new double*[4]; 
         MAPA[0] = new double[T_ajuste];
         MAPA[1] = new double[T_ajuste];
         MAPA[2] = new double[T_ajuste];
         MAPA[3] = new double[T_ajuste];
         fprintf (fo,"%d\n",T_ajuste);
         for (int i=0;i<T_ajuste;i++){
            MAPA[0][i]=1;
            MAPA[1][i]=0;
            MAPA[2][i]=1;
            MAPA[3][i]=1;
            fprintf (fo,"%.2f\n",MAPA[0][i]);
        }      
        fclose(fo); 
     }
     
     if (stage != BaseDriver::WARMUP && cs.getDistRaced()<0.5){
        if (fo = fopen(BaseDriver::trackName,"r")){
            fscanf (fo, "%d", &T_ajuste);
            MAPA = new double*[4]; 
            MAPA[0] = new double[T_ajuste];
            MAPA[1] = new double[T_ajuste];
            MAPA[2] = new double[T_ajuste];
            MAPA[3] = new double[T_ajuste];
            
            for (int i=0;i<T_ajuste;i++){
                fscanf (fo,"%lf",&MAPA[0][i]);
            } 
            
            
            fclose(fo);
        }
        else{
             T_ajuste = (int)cs.getDistFromStart()+520;     
             fo = fopen(BaseDriver::trackName,"w");
             MAPA = new double*[4]; 
             MAPA[0] = new double[T_ajuste];
             MAPA[1] = new double[T_ajuste];
             MAPA[2] = new double[T_ajuste];
             MAPA[3] = new double[T_ajuste];
             fprintf (fo,"%d\n",T_ajuste);
             for (int i=0;i<T_ajuste;i++){
                MAPA[0][i]=1;
                MAPA[1][i]=0;
                fprintf (fo,"%.2f\n",MAPA[0][i]);
            }      
            fclose(fo);              
        }
     }
    
    for (int i=1;i<35;i++){
       o1[i]=o0[i];
       o0[i]=cs.getOpponents(i+1);          
    }
    
    if (( fabs(cs.getAngle()) > stuckAngle || (cs.getSpeedX()<10 && cs.getGear()>0))){stuck++;}
    else{stuck = 0;}
      
    
    if (stuck > stuckTime)
    {
    	float steer = - cs.getAngle() / steerLock; 
        int gear=-1;
        
        if (cs.getSpeedX()>15){
           CarControl cc (0.0,0.5,gear,-steer,clutch);
           return cc;
        }
        
        
        if ((cs.getAngle()*cs.getTrackPos()>0 && fabs(cs.getTrackPos())>0.5) || cs.getTrack(9)>15)
        {
            if (cs.getSpeedX()<-15){
               CarControl cc (0.0,0.5,gear,-steer,clutch);
               return cc;
            }
            stuck = 0;
            gear = 1;
            steer = -steer;
            CarControl cc (0.0,1.0,gear,steer,clutch);
            return cc;
        }

        clutching(cs,clutch);
        clutch_1 = clutch;
        CarControl cc (1.0,0.0,gear,steer,clutch);
        return cc;
    }

    else // car is not stuck
    {
        float accel_and_brake = getAccel(cs);
        int gear = getGear(cs);
        float steer = getSteer(cs);
        
        // normalize steering
        if (steer < -1)
            steer = -1;
        if (steer > 1)
            steer = 1;
        
        // set accel and brake from the joint accel/brake command 
        float accel,brake;
        if (accel_and_brake>0)
        {
            accel = filterTCL(cs,accel_and_brake);
            brake = 0;
            accelbrake_1=accel;
        }
        else
        {
            accel = 0;
            if (accel_and_brake<-0.75) brake=0.75;
            else                       brake=-accel_and_brake;
            
            brake = filterABS(cs,brake);
            accelbrake_1=-brake;
        }

        clutching(cs,clutch);
        
        ciclos++;
        if (ciclos%1000==0){
            ciclos = 0; 
        }
        
        //steer_1 = steer;
        clutch_1 = clutch;
        CarControl cc(accel,brake,gear,steer,clutch);
        return cc;
    }
}


float  SimpleDriver::filterTCL(CarState &cs,float accel_brake)
{
  float salida=accel_brake;
  double s = ((3.6*cs.getWheelSpinVel(0) +
               3.6*cs.getWheelSpinVel(1)) * wheelRadius[1] +
			  (3.6*cs.getWheelSpinVel(2) +
			  3.6*cs.getWheelSpinVel(3)) * wheelRadius[3]) / 4.0;
				  
  float slipspeed;
  if (cs.getSpeedX()>=0) slipspeed = s-cs.getSpeedX();
  else slipspeed = s+cs.getSpeedX();
  
  float TCL_SLIP = 1.5;
  float TCL_RANGE = 5.0;
  if (slipspeed > TCL_SLIP && accel_brake>0) {
	salida = accel_brake - min(accel_brake, (slipspeed - TCL_SLIP)/TCL_RANGE);
  }
    
  return salida;
  
} 

float SimpleDriver::filterABS(CarState &cs,float brake)
{
	float speed = cs.getSpeedX() / 3.6;
	
    if (speed < absMinSpeed)
        return brake;
    
    float slip = 0.0f;
    
    for (int i = 0; i < 4; i++){
        slip += cs.getWheelSpinVel(i) * wheelRadius[i];}
        
    slip = speed - slip/4.0f;
    
    if (slip > absSlip){
        brake = brake - (slip - absSlip)/absRange; }
    
    if (brake<0)
    	return 0;
    else
    	return brake;
}


void
SimpleDriver::onShutdown()
{
   // cout << "Bye bye!" << endl;
}

void
SimpleDriver::onRestart()
{
    //cout << "Restarting the race!" << endl;
}

void
SimpleDriver::clutching(CarState &cs, float &clutch)
{
  double maxClutch = clutchMax;

  // Check if the current situation is the race start
  if (cs.getCurLapTime()<clutchDeltaTime  && stage==RACE && cs.getDistRaced()<clutchDeltaRaced)
    clutch = maxClutch;

  // Adjust the current value of the clutch
  if(clutch > 0)
  {
    double delta = clutchDelta;
    if (cs.getGear() < 2)
	{
      // Apply a stronger clutch output when the gear is one and the race is just started
	  delta /= 2;
      maxClutch *= clutchMaxModifier;
      if (cs.getCurLapTime() < clutchMaxTime)
        clutch = maxClutch;
	}

    // check clutch is not bigger than maximum values
	clutch = min(maxClutch,double(clutch));

	// if clutch is not at max value decrease it quite quickly
	if (clutch!=maxClutch)
	{
	  clutch -= delta;
	  clutch = max(0.0,double(clutch));
	}
	// if clutch is at max value decrease it very slowly
	else
		clutch -= clutchDec;
  }
}

void
SimpleDriver::init(float *angles)
{

	// set angles as {-90,-75,-60,-45,-30,20,15,10,5,0,5,10,15,20,30,45,60,75,90}

	for (int i=0; i<5; i++)
	{
		angles[i]=-90+i*15;
		angles[18-i]=90-i*15;
		Tangles[i]=-90+i*15;
		Tangles[18-i]=90-i*15;
	}

	for (int i=5; i<9; i++)
	{
			angles[i]=-20+(i-5)*5;
			angles[18-i]=20-(i-5)*5;
			Tangles[i]=-20+(i-5)*5;
			Tangles[18-i]=20-(i-5)*5;
	}
		
	angles[9]=0;
	Tangles[9]=0;
	
	
	 FILE * fo;
     fo = fopen("salida.txt","w");
     fprintf (fo,"\n");
     fclose(fo);
     if (stage == BaseDriver::WARMUP){
    	 FILE * fo;
         fo = fopen(BaseDriver::trackName,"w");
         fprintf (fo,"\n");
         fclose(fo);
     }
     ifstream CONF("CONFIGURACION.txt",fstream::in);
         CONF>>ST1;CONF>>ST2;CONF>>ST3;CONF>>ST4;CONF>>ST5; 
         CONF>>GE1[0];CONF>>GE1[1];CONF>>GE1[2];CONF>>GE1[3];CONF>>GE1[4];CONF>>GE1[5];
         CONF>>GE2[0];CONF>>GE2[1];CONF>>GE2[2];CONF>>GE2[3];CONF>>GE2[4];CONF>>GE2[5];    
         CONF>>TS1;CONF>>TS2;CONF>>TS3;CONF>>TS4;CONF>>TS5;
         CONF>>OV1;CONF>>OV2;CONF>>OV3;CONF>>OV4;CONF>>OV5;
     CONF.close();
		
}
