/* 
Gazelle car racing driver
Dana Vrajitoru
Kholah Albelihi
*/
#include <cmath>
#include <iostream>
using namespace std;

#include "Gazelle.h"

int i=0;
//Constant constants
const float PI=3.1415926536;
const float PI12 = PI/2;
const float deg10 = PI/18.0;
const float deg5 = PI/36.0;
const float maxTurn=0.785398;
const float ourSteerSensitivityOffset=80.0;
const float ourWheelSensitivityCoeff=1;

/* Stuck constants*/
const int ourStuckTime = 25;
const float ourStuckAngle = .523598775; //PI/6

/* Constants copied from SimpleDriver */
/* Stuck constants*/
const int stuckTime = 25;
const float stuckAngle = .523598775; //PI/6

/* Accel and Brake Constants*/
const float maxSpeedDist=70;
const float maxSpeed=120;
const float sin10 = 0.17365;
const float cos10 = 0.98481;

/* Steering constants*/
const float steerLock=0.785398;
const float steerSensitivityOffset=80.0;
const float wheelSensitivityCoeff=1;

/* ABS Filter Constants */
const float wheelRadius[4]={0.3179,0.3179,0.3276,0.3276};
const float absSlip=2.0;
const float absRange=3.0;
const float absMinSpeed=3.0;

// Variable constants
const float FastCurve = deg10;//0.05236; // approximately 3 degrees.
const float MedCurve=1.5*deg10;
const float SlowCurve=3*deg10; 
const float pedalToTheMetal = 5000;
const float floorItDist = 80; 
const float floorItAngle = deg10;
const float safelyInsideTrack = 0.9;
const float maxSensor = 100;
const float steerOutsideCurve = 0.5*deg10;
const float steerCoef = 0.2;
const int ourGearUp[6]= {8000,7500,7000,7000,7000,0};
const int ourGearDown[6]= {0,2500,3000,3000,3500,3500};
const float tooClose = 10; // meters
const int minSafeSpeed = 30;
const float steerAmp = 1.2;
const float safeSharpTurn = 0.4;
const float distMargin = 0.9;

float TrackSurfaceMode=0;
int OvalTrack=2;
int DirtTrack=4;
int RoadTrack=4;
int CurrentSurfaceMode=0;
int JummpingCount=0;


//Opponent Modifier
int OpponentFlag=0;
int OpponentBrakeFlag=0;
int OpponentAccelFlag=0;
int MinAllowedSpace=20;
int SafeDisS[6]={20,18,16,14,12,10};				//steering
float SafeDisB[5]={8,7.5,7,6.5,6};					//Breake
float SafeDisA[5]={10,9.5,9,8.5,8};					//Accelerate
float AvoidCollision=-0.4;
float CollAvoidExteraSpeed=5;
float OpponentSpeed=0;

/* Clutch constants */
const float Gazelle::clutchMax=0.5;
const float Gazelle::clutchDelta=0.05;
const float Gazelle::clutchRange=0.82;
const float Gazelle::clutchDeltaTime=0.02;
const float Gazelle::clutchDeltaRaced=10;
const float Gazelle::clutchDec=0.01;
const float Gazelle::clutchMaxModifier=1.3;
const float Gazelle::clutchMaxTime=1.5;

float spotDist = 200; // distance to the trouble spot
float lastSpot = 0;

Gazelle::Gazelle()
    : WrapperBaseDriver() // SimpleDriver()
{
    onRestart();
}

// Print a restart message and reset class variables.
void Gazelle::onRestart()
{
    buffAngle = 0;
    prevDamage = 0;
    prevLapseTime=-1;
    prevLapseDamage = 0;
    safeSpeed = 100;
    medSpeed = 200;
    sundayDriver = 350;
}

// This is basically the same function as in SimpleDriver, except that we redefined the 
// RPM values for changing the gear up. We're waiting longer to be able to accelerate faster.s
int Gazelle::getGear(CarState &cs)
{
    int gear = cs.getGear();
    int rpm  = cs.getRpm();

    // if gear is 0 (N) or -1 (R) just return 1 
    if (gear<1)
        return 1;
    // check if the RPM value of car is greater than the one suggested 
    // to shift up the gear from the current one     
    if (gear <6 && rpm >= ourGearUp[gear-1])
        return gear + 1;
    else
        // check if the RPM value of car is lower than the one suggested 
        // to shift down the gear from the current one
        if (gear > 1 && rpm <= ourGearDown[gear-1])
            return gear - 1;
        else // otherwhise keep current gear
            return gear;
}

// Converts an angle from degrees to radians.
float deg2rad(float angle)
{
    return angle*PI/180.0;
}

// Converts an angle from radians to degrees.
float rad2deg(float angle)
{
    return angle*180.0/PI;
}

// Finds out which index in the trackPos array of the sensors corresponds
// to a given angle. The angle is relative to the car orientation.
int indexTrack(float angle)
{
    return int((angle + PI12)*18/PI);
}

// Finds out which index in the opponents array of the sensors corresponds
// to a given angle. The angle is relative to the car orientation.
int indexOpponent(float angle)
{
    return int((angle + PI)*35/(2*PI));
}


// Find the minimal distance between adjacent sensors as an indication of
// a sharp turn coming up ahead. The angle is the target angle by which
// we expect to be turning.
void Gazelle::computeSharpTurn(CarState &cs, float angle)
{
    float localSharpTurn, dist;
    sharpTurn = 0;
    for (float i=-2*deg10 + angle; i< 2*deg10 + angle; i+= deg10)
    {
        float first, second;
        first = distanceAhead(cs, i);
        second = distanceAhead(cs, i+deg10);
        if (first != -1 && second != -1)
        {
            // Find the absolute value of the difference between the distance ahead in two directions.
            // The coefficient of sharp turn is 1/(1+dist). We're looking for the minimal such
            // difference indicating that the road goes in a new direction. So we're finding the
            // maximum of this coefficient and storing it.
            dist = fabs(first - second);
            if (first < 90 && second < 90) 
            {
                localSharpTurn = 1.0/(1.0+dist);
                if (localSharpTurn > sharpTurn)
                    sharpTurn = localSharpTurn;
            }
        }
    }
    //cout << "sharp turn" << sharpTurn << endl;
}

// Gives us the available distance ahead in the direction given by this angle.
float Gazelle::distanceAhead(CarState &cs, float angle)
{
    float currentAngle = cs.getAngle();
    float relAngle = currentAngle-angle;
    if (fabs(relAngle) > PI12) // outside of the track sensors, don't know
        return -1;
    float low, high, distTrack, distOpponent, ratio;
    // Approximate the distance ahead in the direction of the angle as given by the track array.
    low = indexTrack(relAngle);
    if (low == 18)
        high = low;
    else 
        high = low+1;
    ratio = (relAngle + PI12 - deg10*low)/deg10;
    distTrack = (1-ratio) * cs.getTrack(low) + ratio * cs.getTrack(high);
    return distTrack;

    // Compute the same thing in the opponent array.
    /*
    low = indexOpponent(relAngle);
    if (low == 35)
    high = 0;
    else 
    high = low+1;
    ratio = (relAngle + PI - deg10*low)/deg10;
    distOpponent = (1-ratio) * cs.getOpponents(low) + ratio * cs.getOpponents(high);
    return distTrack <= distOpponent ? distTrack : distOpponent;
    */
}

// checks if the car is out of the track
bool Gazelle::outOfTrack(CarState &cs)
{
    return (fabs(cs.getTrackPos()) > 1);
}

// Deciding on the speed based on the direction in which we want the car to go. 
// That is given as the parameter targetAngle, and is decided on elsewhere.
float Gazelle::getAccel(CarState &cs, float targetAngle)
{
    // checks if car is out of track
    if (cs.getTrackPos() < 1 && cs.getTrackPos() > -1)
    {
        float targetSpeed=0, currentAngle;

        //currentAngle = cs.getAngle();

        // track is straight and enough far from a turn so goes to max speed

        if (outOfTrack(cs)) // is it out of the track?
            targetSpeed = safeSpeed/2; // play it safe
        else if( (sharpTurn < safeSharpTurn)&& (fabs(targetAngle) <= FastCurve ))
            targetSpeed = sundayDriver; //pedalToTheMetal;
        else //if (fabs(targetAngle) <= MedCurve && sharpTurn < safeSharpTurn)
        {
            // computing approximately the "angle" of turn
            float sinAngle = sin(fabs(targetAngle));
            float freeSpace = distanceAhead(cs, targetAngle);
            //float freeStraight = distanceAhead(cs, 0); // free space straight ahead of us
            //float aveSpace = (freeSpace+freeStraight)/2.0;
            float aveSpace = freeSpace;
            // estimate the target speed depending on turn and on how close it is
            targetSpeed = safeSpeed + (sundayDriver-safeSpeed)*sinAngle* (25+aveSpace)/(25+maxSensor);
            // Mitigate the target speed by the sharp turn factor
            targetSpeed = targetSpeed * (1 - sharpTurn);
            //cout << "speed " << cs.getSpeedX() << " ts " << targetSpeed << endl;
        }

        // Added to take the trouble spots into account
        float spotClose = spotAdjust(cs);
        if (spotClose > 0)
        {
            float newSpeed = spotClose*safeSpeed + (1-spotClose)*medSpeed;
            if (targetSpeed > newSpeed)
                targetSpeed = newSpeed;
        }

        //accelerate a liitle bit to avoid a coming opponent  from the back
        //&&fabs(targetAngle)<=StrightValue
        if (OpponentAccelFlag=1)
        {
            if (OpponentSpeed>cs.getSpeedX())
                targetSpeed=OpponentSpeed+CollAvoidExteraSpeed;
            else
                targetSpeed+=CollAvoidExteraSpeed;

            OpponentSpeed=0;
        }
        /* else if (fabs(targetAngle) <= SlowCurve && sharpTurn < safeSharpTurn)
        targetSpeed =minSafeSpeed;

        cout << "speed: " << cs.getSpeedX() << " ts " << targetSpeed <<" , Curve Angle="<<targetAngle<< endl;
        //return targetSpeed;
        // accel/brake command is expontially scaled w.r.t. the difference between target speed and current one
        */
        
        // to test the steering: set the speed as constant
        //targetSpeed = 90;

        return 2.0/(1+exp(cs.getSpeedX() - targetSpeed)) - 1;
    }
    else
        return 0.3; // when out of track returns a moderate acceleration command
}

// This function checks if a trouble spot is close to the car and 
// returns a value between 0 and 1 telling us how close we are to it.
// 1 means that we are at this spot, 0 if we're far enough from it.
float Gazelle::spotAdjust(CarState &cs)
{
    Iter it = troubleSpots.begin();
    float distFromStart = cs.getDistFromStart();

    // skip all the spots that are behind us
    while (it != troubleSpots.end() && distFromStart > *it)
        ++it;

    // find a trouble spot that we are close enough to
    while (it != troubleSpots.end() && distFromStart <= *it)
    {
        if (*it - distFromStart <= spotDist) // we're close enough
            // return something between 0 and 1, 1 being the closest
        {
            float spotWeight = 1-(*it - distFromStart)/spotDist;
            //cout << "spot: " <<  *it << " distance: " << spotDist 
            //    << " weight: " << spotWeight << endl;
            return spotWeight;
        }
        ++it;
    }
    return 0;
}

// This function will adjust the target angle based on the desired position with respect to the 
// center of the track.

void Gazelle::positionTrack(CarState &cs, float trackPos, float &targetAngle)
{
    float centerAngle;
    // if the car is too far to the left
    if (trackPos >= safelyInsideTrack) 
    {
        centerAngle = -5*(trackPos-safelyInsideTrack) * deg5;
        //cout << "cr " << centerAngle << endl;
        if (centerAngle < targetAngle)
            targetAngle = centerAngle;
    }
    // if the car is too far to the right
    else if (trackPos <= -safelyInsideTrack) 
    {
        centerAngle = -5*(trackPos+safelyInsideTrack) * deg5;
        //cout << "cl " << centerAngle << endl;
        if (centerAngle > targetAngle)
            targetAngle = centerAngle;
    }
    // The next code was supposed to 
    //else if (targetAngle < 0) 
    //{
    //	if (targetAngle > -steerOutsideCurve && trackPos < safelyInsideTrack) // steer the car towards the outside of the curve
    //	{
    //		centerAngle = steerCoef*(safelyInsideTrack - trackPos)* deg10;
    //		targetAngle += centerAngle;
    //	}
    //	else if (targetAngle < -steerOutsideCurve && trackPos > -safelyInsideTrack)
    //	{
    //		centerAngle = steerCoef*(trackPos-safelyInsideTrack)* deg10;
    //		targetAngle += centerAngle;
    //	}
    //}
    //else // targetAngle > 0
    //{
    //	if (targetAngle < steerOutsideCurve && trackPos > -safelyInsideTrack) // steer the car towards the outside of the curve
    //	{
    //		centerAngle = steerCoef*(trackPos - safelyInsideTrack)* deg10;
    //		targetAngle += centerAngle;
    //	}
    //	else if (targetAngle < -steerOutsideCurve && trackPos < safelyInsideTrack)
    //	{
    //		centerAngle = steerCoef*(safelyInsideTrack - trackPos)* deg10;
    //		targetAngle += centerAngle;
    //	}
    //}
}

float Gazelle::getTargetAngle(CarState &cs)
{
    register float targetAngle, centerAngle;
    static float lastTargetAngle = 0;
    float dist, dist1, dist2, dist3;
    float currentAngle = cs.getAngle();	
    float trackPos = cs.getTrackPos();
    if (outOfTrack(cs)) // if we are out of the tarck, try to get back in
    {
        targetAngle = - trackPos*0.5*deg10;
        return steerAmp*targetAngle;
    }
    if (fabs(currentAngle) <= floorItAngle && 
        distanceAhead(cs, 0)>= floorItDist && 
        fabs(trackPos) < safelyInsideTrack)
    {
        //cout << "current angle " << currentAngle << " track pos " << trackPos << endl;
        if (fabs(currentAngle) < 0.5*deg10 && fabs(trackPos) > 0.5)
        {
            targetAngle = currentAngle ;
            //cout << "current angle " << currentAngle << " track pos " << trackPos 
            //     << " traget angle " << targetAngle << endl;
            lastTargetAngle = targetAngle;
            return steerAmp*targetAngle;
        }
        lastTargetAngle = 0;
        return 0;
    }
    targetAngle = currentAngle;
    if (targetAngle > maxTurn)
        targetAngle = maxTurn;
    else if (targetAngle < -maxTurn)
        targetAngle = -maxTurn;
    dist = distanceAhead(cs, targetAngle);
    dist1 = distanceAhead(cs, targetAngle + deg10);
    dist2 = distanceAhead(cs, targetAngle - deg10);
    dist3 = distanceAhead(cs, lastTargetAngle);

    // keep the same angle as long as it gives us a good distance
    if (dist3 >= dist && dist1 < distMargin*dist3 && dist2 < distMargin*dist3)
        return steerAmp*targetAngle; 

    if (dist >= dist1 && dist >= dist2)
        ;
    else if (dist1 > dist2) 
        while (targetAngle+deg10 <= maxTurn && dist1 >= dist2) 
        {
            targetAngle += deg10;
            dist2 = dist1;
            dist1 = distanceAhead(cs, targetAngle+deg10);
        }
    else 
        while (targetAngle-deg10 >= -maxTurn && dist2 >= dist1) 
        {
            targetAngle -= deg10;
            dist1 = dist2;
            dist2 = distanceAhead(cs, targetAngle - deg10);
        }
        //cout << "ta " << rad2deg(targetAngle) << " ca " << rad2deg(currentAngle) << endl;
        //cout << "ta " << rad2deg(targetAngle) << " tp " << trackPos << endl;
        // If the car is too close to the border of the road then make it move to the inside.
        positionTrack(cs, trackPos, targetAngle);
        computeSharpTurn(cs, targetAngle);
        lastTargetAngle = targetAngle; // store the last angle

        return steerAmp*targetAngle;
        //targetAngle += buffAngle;
        //if (fabs(targetAngle) > 0.8*deg10) {
        //	buffAngle = 0;
        //	return targetAngle;
        //}
        //else {
        //	buffAngle = targetAngle;
        //	return 0;
        //}
}

// Gives us the available distance ahead in the direction given by this angle.
float Gazelle::CloserOppnent(CarState &cs, float angle)
{	
    float currentAngle = cs.getAngle();
    float relAngle = currentAngle-angle;
    if (fabs(relAngle) > PI12) // outside of the track sensors, don't know
        return -1;
    float low, high, distOpponent, ratio;


    low = indexOpponent(relAngle);
    if (low == 35)
        high = 0;
    else 
        high = low+1;
    ratio = (relAngle + PI - deg10*low)/deg10;
    distOpponent = (1-ratio) * cs.getOpponents(low) + ratio * cs.getOpponents(high);
    return  distOpponent;
}

// This function predict the steering angle needed to avoid the opponent 
//if there is no danger it returns 0;

float Gazelle::OpponentDetector(CarState &cs)
{
    float LocalOppAngle, dist;
    LocalOppAngle = 0;
    OpponentFlag=0;
    for (float i=-2*deg10 + LocalOppAngle; i< 2*deg10 + LocalOppAngle; i+= deg10)
    {
        float first, second;
        first = CloserOppnent(cs, i);
        second = CloserOppnent(cs, i+deg10);
        float ShortestDis=200;
        if (first != -1)
        { if ((first<MinAllowedSpace)&& (first<ShortestDis))
        {LocalOppAngle=i;
        ShortestDis=first;
        cout<<"There is an opponent I="<<i<<endl;
        OpponentFlag=1;

        float TestedAngle= abs(i);
        OpponentBrakeFlag=0;

        if (OpponentFlag==1)
        {	if (((TestedAngle==0) &&  (first<SafeDisB[0]) ) ||
        ((TestedAngle==deg10) &&  (first<SafeDisB[1]) ) ||
        ((TestedAngle==2*deg10) &&  (first<SafeDisB[2]) ) ||
        ((TestedAngle==3*deg10) &&  (first<SafeDisB[3]) ) ||
        ((TestedAngle==4*deg10) &&  (first<SafeDisB[4]) ) )
        OpponentBrakeFlag=1;

        //float TrackWidth=cs.getTrack(0)+cs.getTrack(18);
        //float TrackWidth=3;

        //if ((ABSAngle=0) && (Distance<50) ) 
        //	 Steering=TrackWidth/100;
        /*
        if (cs.getTrackPos() < 1 && cs.getTrackPos() > -1)
        {
        if ((ABSAngle>0) && (ABSAngle<=deg10) && (Distance<10) ) 
        {if (OppositDistance>Distance)
        Steering=Dir*(TrackWidth/100);	
        else 
        Steering=Dir*(TrackWidth/50);
        }

        else if ((ABSAngle<=14*deg10) && (Distance<15))
        Steering=Dir*(TrackWidth/50);
        }
        */
        }


        }
        /*else if ((first>second)&(second<MinAllowedSpace))
        {LocalOppAngle=i+deg10;
        cout<<"There is an opponent J="<<i+deg10<<endl;
        }
        */
        }
    }

    float Dir=0, Steering=0;

    if (OpponentFlag==1)
    {	float Distance=CloserOppnent(cs, LocalOppAngle);

    //float OppositDistance=CloserOppnent(cs, -LocalOppAngle);

    //inverse the signal 
    if (LocalOppAngle<=0)
        Dir=1;
    else
        Dir=-1;
    float ABSAngle=abs(LocalOppAngle);

    if ((ABSAngle==0) &&  (Distance<SafeDisS[0]) ) 
        Steering=SafeDisS[0]/100;
    else if ((ABSAngle==deg10) &&  (Distance<SafeDisS[0]) ) 
        Steering=SafeDisS[0]/100;
    else if ((ABSAngle==2*deg10) &&  (Distance<SafeDisS[1]) ) 
        Steering=SafeDisS[1]/100;
    else if ((ABSAngle==3*deg10) &&  (Distance<SafeDisS[2]) ) 
        Steering=SafeDisS[2]/100;
    else if ((ABSAngle==4*deg10) &&  (Distance<SafeDisS[3]) ) 
        Steering=SafeDisS[3]/100;
    else if ((ABSAngle==5*deg10) &&  (Distance<SafeDisS[4]) ) 
        Steering=SafeDisS[4]/100;
    else if ((ABSAngle>5*deg10) &&  (Distance<SafeDisS[5]) ) 
        Steering=SafeDisS[5]/100;

    }
    else
        Steering=0;

    return Dir*Steering;
}

float Gazelle::getSteer(CarState &cs, float targetAngle)
{ 
    float OD=OpponentDetector(cs);
    // at high speed reduce the steering command to avoid loosing the control
    //return targetAngle;
    if (cs.getSpeedX() > ourSteerSensitivityOffset) 

        return (targetAngle)/maxTurn;

    else if (OpponentFlag==1)
        return  OD+(targetAngle/maxTurn);
    else
    {//cout<<"No opponent if "<<endl;
        return targetAngle/maxTurn;

    }
}

// Adapts the reference speeds of the car to the situation on the road so far. 
// 1. If we notice that the car was dammaged and there is no other car in the
// close vicinity, it means we bumped against the shoulder and we're going too fast.
// 2. If we managed to complete an entire lapse without new dammage, maybe we can increase 
// the speed a little bit.
// 3. If the car is stuck and there is no other car close, maybe we're going too fast again.
// We might need to check this because we might skid without hitting a wall and that can 
// also mean that we're going too fast.
void Gazelle::learnSpeed(CarState cs)
{
    static bool damageLast = false;
    float damage = cs.getDamage();
    float lapseTime = cs.getCurLapTime();

    if (damage > prevDamage || stuck == 1) 
    {
        bool carClose = false;
        for (int i=0; i<36 && !carClose; i++)
        {
            //cout << "c" << i << " " << cs.getOpponents(i) << endl;
            if (fabs(cs.getOpponents(i)) < tooClose)
                carClose = true;
        }
        if (!carClose)
        {
            if (!damageLast) 
                safeSpeed -= 10;
            damageLast = true;
            //cout << "ss " << safeSpeed << endl;
        }
    }
    else if (prevLapseTime > lapseTime) 
    {
        if (prevLapseDamage == damage)
            safeSpeed += 5;
    }
    else
        damageLast = false;
    // Update the globals after we're done dealing with them.
    if (prevLapseTime > lapseTime) 
        prevLapseDamage = damage;
    prevDamage = damage;
    prevLapseTime = lapseTime;

    if (safeSpeed < minSafeSpeed)
        safeSpeed = minSafeSpeed;
}

float Gazelle::filterABS(CarState &cs,float brake)
{
    // convert speed to m/s
    float speed = cs.getSpeedX() / 3.6;
    // when spedd lower than min speed for abs do nothing
    if (speed < absMinSpeed)
        return brake;

    // compute the speed of wheels in m/s
    float slip = 0.0f;
    for (int i = 0; i < 4; i++)
    {
        slip += cs.getWheelSpinVel(i) * wheelRadius[i];
    }
    // slip is the difference between actual speed of car and average speed of wheels
    slip = speed - slip/4.0f;
    // when slip too high applu ABS
    if (slip > absSlip)
    {
        brake = brake - (slip - absSlip)/absRange;
    }

    // check brake is not negative, otherwise set it to zero
    if (brake<0)
        return 0;
    else
        return brake;
}

CarControl Gazelle::wDrive(CarState cs)
{
    static int starting = 0;
    //learnSpeed(cs);
    sharpTurn = 0;
    //computeSharpTurn(cs);
    // check if car is currently stuck
    if ( fabs(cs.getAngle()) > ourStuckAngle )
    {
        // update stuck counter
        stuck++;
    }
    else
    {
        // if not stuck reset stuck counter
        stuck = 0;
    }

    // after car is stuck for a while apply recovering policy
    if (stuck > ourStuckTime)
    {
        /* set gear and sterring command assuming car is 
        * pointing in a direction out of track */
        // to bring car parallel to track axis
        float steer = - cs.getAngle() / steerLock; 
        int gear=-1; // gear R

        // if car is pointing in the correct direction revert gear and steer  
        if (cs.getAngle()*cs.getTrackPos()>0)
        {
            gear = 1;
            steer = -steer;
        }
        // build a CarControl variable and return it
        CarControl cc(1.0,0.0,gear,steer,0.0,0,0);
        return cc;
    }

    else // car is not stuck
    {	
        int gear;
        float targetAngle;
        float accel_and_brake;

        if (fabs(cs.getTrackPos()) > 1) // car out of the track
        {
            gear = getGear(cs);
            targetAngle = getTargetAngle(cs);
            accel_and_brake = getAccel(cs, targetAngle);
   
            if (lastSpot != 0) // record it then set it to 0
            {
                insertSpot(lastSpot);
                lastSpot = 0;
            }
        }
        else 
        {
            lastSpot = cs.getDistFromStart(); // record the last spot where were on the track


            if (starting < 10) 
            {
                //cout << "starting" << endl;
                gear = 1;
                targetAngle = 0;
                accel_and_brake = 1;
                starting++;
            }
            else {
                gear = getGear(cs);
                targetAngle = getTargetAngle(cs);

                //cout << "ta " << targetAngle << endl;

                // Brake if there is an closed opponent can't be avoid and the speed is higher 
                // than the minimum safe speed
                if (cs.getSpeedX()>minSafeSpeed && OpponentBrakeFlag==1)	
                    accel_and_brake=-0.5;
                else
                    accel_and_brake = getAccel(cs, targetAngle);
            }
        }
        //cout << "acc " << accel_and_brake << endl;
        //int gear = getGear(cs);

        /*//DirtTrack Detector
        int NewSurfaceMode=cs.getSpeedZ();
        //cout<<"LocalSurfaceMode= "<<NewSurfaceMode<<endl;
        if ((NewSurfaceMode>(CurrentSurfaceMode+3))||(NewSurfaceMode<(CurrentSurfaceMode-3)))
        {if (JummpingCount<=10 ) 
        JummpingCount++;
        else
        {i++;
        cout<<"Jummping Count: "<<i<<endl;
        JummpingCount=0;
        float trackPos=cs.getTrackPos();
        positionTrack(cs,  trackPos, targetAngle);
        //float CorrectPosition = getSteer(cs, targetAngle);
        }

        }
        */


        // compute steering

        float steer = getSteer(cs, targetAngle);

        // normalize steering
        if (steer < -1)
            steer = -1;
        if (steer > 1)
            steer = 1;

        // set accel and brake from the joint accel/brake command 
        float accel,brake;
        if (accel_and_brake>0)
        {
            accel = accel_and_brake;
            brake = 0;
        }


        else
        {
            accel = 0;
            // apply ABS to brake
            brake = filterABS(cs,-accel_and_brake);
        }
        if ((steer == 1 || steer == -1) && brake == 0)
        {
            brake = .1515;
        }
        //cout << "steer " << steer << " brake " << brake << endl;
        // build a CarControl variable and return it
        
        // Calculate clutching
        clutching(cs,clutch);

        //CarControl cc(1.0,0.0,gear,steer,0.0,0,0);
        CarControl cc(accel,brake,gear,steer,clutch);
        return cc;
    }
}

// insert a trouble spot, keeping the list in ascending order)
void Gazelle::insertSpot(float spot)
{
    //cout << "Inserting spot: " << spot << endl;

    Iter it = troubleSpots.begin();

    while (it != troubleSpots.end() && *it < lastSpot)
        ++it;

    if (it == troubleSpots.end()) // the spot was after all those already recorded
        troubleSpots.push_back(spot);
    else // we insert it before the current iterator
        troubleSpots.insert(it, spot);
}

void Gazelle::clutching(CarState &cs, float &clutch)
{
  double maxClutch = clutchMax;

  // Check if the current situation is the race start
  if (cs.getCurLapTime()<clutchDeltaTime  && stage==RACE && cs.getDistRaced()<clutchDeltaRaced)
    clutch = maxClutch;

  // Adjust the current value of the clutch
  if(clutch > 0)
  {
    double delta = clutchDelta;
    if (cs.getGear() < 2)
	{
      // Apply a stronger clutch output when the gear is one and the race is just started
	  delta /= 2;
      maxClutch *= clutchMaxModifier;
      if (cs.getCurLapTime() < clutchMaxTime)
        clutch = maxClutch;
	}

    // check clutch is not bigger than maximum values
	clutch = min(maxClutch,double(clutch));

	// if clutch is not at max value decrease it quite quickly
	if (clutch!=maxClutch)
	{
	  clutch -= delta;
	  clutch = max(0.0,double(clutch));
	}
	// if clutch is at max value decrease it very slowly
	else
		clutch -= clutchDec;
  }
}

/*Gazelle::DetectTrackSurface(CarState cs)
nedded:
smooth movement
Opponent 

Deg10		time					maxSpeed
1			1:07:26					251			(the FastCurve's optimal angle)
1.5			1:07:33					251
2			1:07:31					252

condition: if the other side is free& check the track position

*/