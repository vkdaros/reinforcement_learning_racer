/* This is a very fast mount.

Written by Shogoth and Xylae.

*/

#ifndef	GAZELLE_H
#define GAZELLE_H

#include "WrapperBaseDriver.h"
#include "CarState.h"
#include <list>

typedef list<float> ListFloat;
typedef ListFloat::iterator Iter;

class Gazelle: public WrapperBaseDriver // SimpleDriver 
{
public:
	// Information about the previous steps and constants that the program might change.
	int buffAngle;
	float prevDamage, prevLapseTime, prevLapseDamage;
	float safeSpeed;
	float sundayDriver;
    float medSpeed;
	float sharpTurn;
    
    ListFloat troubleSpots;
	
	// counter of stuck steps
	int stuck;

	// current clutch
	float clutch;

	Gazelle();

	float distanceAhead(CarState &cs, float angle);
	int getGear(CarState &cs);
	float getAccel(CarState &cs, float targetAngle);
	float getTargetAngle(CarState &cs);
	float getSteer(CarState &cs, float targetAngle);
	void positionTrack(CarState &cs, float trackPos, float &targetAngle);
	void learnSpeed(CarState cs);
    float spotAdjust(CarState &cs);
    void insertSpot(float spot);
    bool outOfTrack(CarState &cs);
	void computeSharpTurn(CarState &cs, float angle);
	float CloserOppnent(CarState &cs, float angle);
	float OpponentDetector(CarState &cs);
	CarControl wDrive(CarState cs);
	void clutching(CarState &cs, float &clutch);

	// Apply an ABS filter to brake command
	float filterABS(CarState &cs,float brake);
	
	// Print a restart message and reset class variables.
	virtual void onRestart();

private:

	/* Clutch constants */
	static const float clutchMax;
	static const float clutchDelta;
	static const float clutchRange;
	static const float clutchDeltaTime;
	static const float clutchDeltaRaced;
	static const float clutchDec;
	static const float clutchMaxModifier;
	static const float clutchMaxTime;
};

#endif
