Gazelle Car Driver

C++ code. It uses the Standard Template Library list class, which should
be compatible with any compiler, as well as the standard libaries and
files provided in the Client package.

Definitions for the file client.cpp:
#define __DRIVER_CLASS__ Gazelle  
#define __DRIVER_INCLUDE__ "Gazelle.h" 

Nothing else is special about the compilation.