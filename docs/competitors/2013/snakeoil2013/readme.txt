SnakeOil
========
Chris X. Edwards <http://xed.ch>

== Organization
There are two important parts to this entry. First is the SnakeOil
module which is a general purpose utility for making SCR entries in
Python. You can read about this at:

    http://xed.ch/lwm/programs/snakeoil/

== Running The Client
The library parses options and you can execute either `./snakeoil.py -h` or
`./client -h` to see the list of options.

The options are handled like normal Posix options. There are long and
short option forms.

To run an entry start the server and do something like the following.
First the warm-up stage:

    $ ./client.py --stage 0 --track ovalB --steps 100000 --port 3001 --host localhost
    
This will write a file called `ovalB.trackinfo` into the current
directory. This file is then used in the subsequent stages.
The qualifying stage:

    $ ./client.py --stage 1 --track ovalB  --port 3001 --host localhost 

Then to race:

    $ ./client.py --stage 2 --track ovalB  --port 3001 --host localhost 

If the race ends before the specified number of steps (or the 100000
default) then the client will finish counting those at the end, so
just wait a couple of seconds for it to finish.

== Highlights

* The snakeoil.py library was written to encourage Python programmers
  to enter the SCR. It's quite easy to use to develop your own bot.

* I basically did all the strategies and features. Badly. I did not
  have time to properly debug this client. It performs terribly.

* Mapped a complete turn by turn course description.

* Created a route plan for where to be at every point on the
  course... but that didn't work and probably can't without a heroic
  effort. It's harder than it first seems.

* Used the track feature map to mark trouble spots and show more
  caution there while racing. 

* I set things up prepared to optimize but I did not have time yet.
  This application is definitely ideal for evolutionary algorithms.

* My best innovation was using a very different set of track sensor
  angles. IMO it is useless to know what is happening to the track
  on either side. I use a +/-45deg sensor on the extreme positions
  and a roughly logrithmic concentration toward the middle. 

* My client maps out curves accurately over 180m away. With noise, a
  bit less.

* I also added a fancy output feature to the library that uses
  ASCII art to graphically represent the state of the simulator and
  car. (Use '-d' for "debug".)
