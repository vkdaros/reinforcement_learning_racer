% ---------------------------------------------------------------------------- %
\chapter{Conclusão}
\label{chap:conclusao}
% ---------------------------------------------------------------------------- %
    Nesta dissertação, apresentamos uma breve introdução aos principais
    trabalhos no campo da condução autônoma de veículos, dando ênfase àqueles
    elaborados sobre o simulador de corridas \emph{TORCS}. Expomos como
    competições acadêmicas adotando simuladores realistas encorajam pesquisas
    que podem beneficiar tanto o desenvolvimento de personagens comandados por
    IA em jogos de \emph{videogame} quanto técnicas de controle de carros
    autônomos e robôs móveis.

    Mostramos também o funcionamento e implementação de controladores do tipo
    PID, os quais podem ser usados como componentes que gerenciam atuadores com
    funções diversas. Destacamos situações delicadas onde esses controladores
    podem apresentar comportamento indesejado, mas abordamos formas de contornar
    tais adversidades. Além disso, comentamos sobre os processos de ajuste de
    parâmetros e um procedimento eficiente para o ajuste manual.

    Neste trabalho propomos um método para criação de modelos de pistas usando
    apenas uma pequena quantidade de sensores. Por esse método, é possível
    determinar aproximadamente o formato do circuito, o qual pode ser
    segmentado em trechos. Esses trechos, por sua vez, podem ser classificados
    formando um conjunto de informações concisas e expressivas para representar
    a pista.

    Expusemos os conceitos fundamentais de Aprendizagem por Reforço, bem como
    sua adaptação na construção de um piloto virtual. O piloto emprega RL para
    descobrir como controlar o acelerador e freio visando completar voltas na
    pista gastando o menor tempo possível. Propusemos também uma heurística
    baseada em controladores PID, a qual considera a sequência de tipos dos
    trechos para manejar o volante de modo satisfatório.

    Por fim, conduzimos experimentos na maioria das pistas disponíveis no
    \emph{TORCS} que são semelhantes aos autódromos convencionais do mundo real.
    Confrontamos os resultados de nosso piloto virtual com o desempenho de dois
    dos melhores projetos submetidos à competição SCRC, chamados de AUTOPIA e
    Mr. Racer. Em cada experimento, os controladores foram isoladamente para a
    pista, correndo sozinhos por $300$ voltas. A medida de desempenho foi o
    tempo médio das últimas voltas. Nosso controlador se mostrou mais lento que
    seus concorrentes, mas surpreendentemente foi mais rápido que Mr. Racer em
    uma das $10$ pistas testadas.

    Embora não tenhamos conseguido superar os concorrentes, as diferenças de
    tempo na maioria das pistas testadas ficou entre $10$ e $15s$. Considerando
    as limitações de nosso agente, tais como a completa discretização das ações
    e estados, além da utilização de heurística para guiar o carro, na verdade
    essas marcas se mostram encorajadoras. Concluímos que o emprego de
    Aprendizagem por Reforço nesse domínio de aplicação é promissor e
    acreditamos que a substituição do \emph{$Q$-Learning} tabular por um método
    capaz de lidar melhor espaços de ações e estados contínuos pode trazer
    melhorias significativas nos tempos de volta.

    O principal ponto fraco da discretização em nossa implementação é o
    componente \emph{velocidade} usado como parte da definição dos estados.
    Existem apenas $5$ faixas de velocidade, sendo sua amplitude média
    aproximadamente $45km/h$. Essa alta granularidade é um dos fatores que
    contribuem para o piloto não conseguir conduzir o carro próximo ao limite da
    aderência dos pneus.

    Os experimentos também nos mostraram que o uso da estrutura de histórico
    análoga a um traço de elegibilidade duplo atenua a sensibilidade do RL aos
    parâmetros de aprendizagem $\alpha$ e $\gamma$. Entretanto utilizar
    corretamente esse traço adaptado se mostrou um dos grandes desafios do
    projeto. Uma vez que o mecanismo de identificação das ações responsáveis
    pelos acidentes está altamente atrelado ao histórico, existem duas situações
    que podem comprometer o aprendizado do agente:

    \begin{itemize}
        \item Quando a ação (ou as ações) responsável por fazer o carro sair da
            pista não está no histórico no momento do acidente, seu valor
            estimado não recebe a punição. Se isso acontece sistematicamente, o
            piloto deixa de conseguir fazer voltas limpas e o aprendizado não
            converge;

        \item No caso inverso, quando ações ``inocentes'' ficam no histórico
            errado ou não são removidas dos históricos, o piloto se torna
            excessivamente cauteloso. Assim, o carro nunca é levado ao seu
            limite e os tempos de volta ficam altos.
    \end{itemize}

    Outro ponto delicado que merece atenção está relacionado ao processo de
    segmentação. O método que propomos gera resultados satisfatórios para a
    maioria dos tipos de trecho. Entretanto, ele não se mostrou consistente em
    trechos com curvatura inconstante, por exemplo, curvas espiraladas. Essa é
    uma questão difícil, pois o \textfunc{filtroDePatamar} fundamentalmente
    tenta transformar o sinal da variação angular em um conjunto de ``saltos'' e
    ``patamares''.  Uma solução para a identificação desses trechos peculiares
    seria flexibilizar o filtro para buscar também tendências uniformemente
    ascendentes ou descentendes, porém essa é uma questão que exige maior
    reflexão.

    %Valeu a pena estudar RL para controle de carros mesmo que tenha sido no
    %TORCS por que o ambiente é complexo, simula bem a realidade e é claro que
    %grande parte do conhecimento adquirido pode ser transferido para o ambiente
    %real. Mas o simulador tira vários problemas (equipamentos, perigos, custos,
    %etc)

% ---------------------------------------------------------------------------- %
\section{Trabalhos futuros}
    A elaboração deste trabalho nos mostrou que construir um piloto virtual
    completo demanda muito mais do que apenas implementar algoritmos de
    Inteligência Artificial. A atividade de dirigir um veículo é na verdade uma
    composição de tarefas de controle, cada qual com diferentes
    particularidades. Embora este projeto tange várias dessas tarefas,
    entendemos claramente que ainda há muito a ser explorado.  Assim, listamos a
    seguir algumas direções que apresentam oportunidades de expansão deste
    trabalho.

    % ------------------------------------------------------------------------ %
    \subsection*{Aumento de robustez}
        Dois pontos mencionados anteriormente que podem ser aprimorados são:
        (\emph{i}) identificação de curvas em espiral pelo filtro de
        segmentação; e
        (\emph{ii}) a substituição da matriz $Q$ por outro método mais adequado
        ao uso de espaços contínuos. O primeiro tópico não apresenta alta
        complexidade técnica, porém a alteração deve ser conduzida com cuidado
        para evitar que picos no sinal passem a ser considerados trechos de
        curva.

        Já a substituição do método tabular é uma questão que pode ser aborda de
        diferentes formas de acordo com os objetivo da nova pesquisa. Uma
        alternativa é usar redes neurais artificiais para atuarem como
        aproximador de função valor. Outra opção, que exigiria uma
        reestruturação maior, porém se mostra muito promissora, é adotar um
        técnica de aprendizado em lote, como o \emph{Fitted $Q$-Iteration}.

        Com um método mais abrangente no mecanismo de aprendizagem, também seria
        interessante estudar formas de incluir o controle do volante no espaço
        de ações. Assim, seria possível dispensar o uso de heurística no piloto
        virtual, potencialmente tornando-o mais flexível.

    % ------------------------------------------------------------------------ %
    \subsection*{Inicialização com conhecimento prévio}
        Neste projeto, o controlador não transfere nenhum tipo de informação
        entre sessões de teste. Ou seja, a matriz $Q$ sempre é inicializada com
        valor zero em todas as posições e a política é determinada sem qualquer
        influência de resultados prévios. Esse método evita a possibilidade do
        processo de aprendizado ser enviesado por uma inicialização tendenciosa.
        Em contrapartida, sempre que o piloto virtual é colocado para correr em
        uma pista, é necessário que ele complete várias voltas até atingir um
        comportamento estável.

        Assim, seria interessante investigar os impactos que a inicialização dos
        valores de $Q$ tem sobre o aprendizado e o desempenho geral do agente.
        Caso os efeitos fossem positivos, o estudo poderia seguir no sentido de
        elaborar uma forma de mesclar as políticas decorrente do treinamento em
        diferentes pistas. Acreditamos que, com essa política resultante, o
        controlador seria capaz de ter bom desempenho em novas pistas sem a
        necessidade de completar várias voltas no circuito.

    % ------------------------------------------------------------------------ %
    \subsection*{Condições adversas}
        Nossos experimentos foram realizados estritamente em pistas de asfalto.
        Porém o \emph{TORCS} também possui circuitos de terra, onde a aderência
        dos pneus ao solo é muito inferior e controlar o carro passa a ser uma
        tarefa ainda mais difícil. Portanto, outra alternativa de trabalho
        futuro é adaptar o piloto virtual para esse desafio adicional, ou até
        mesmo desenvolver uma solução flexível quanto às condições da pista.

        Além disso, o \emph{software} adaptado para a competição SCRC
        possibilita introduzir ruídos nas medições realizadas pelos sensores
        virtuais. Com isso, a analogia entre a simulação e o mundo real fica
        ainda mais forte já que, na prática, as leituras de sensores estão
        sujeitas a imprecisões e erros. Dessa forma, testar o desempenho do
        segmentador quando se tem dados ruidosos e elaborar eventuais
        modificações no método também é um caminho a ser explorado.

    % ------------------------------------------------------------------------ %
    \subsection*{Oponentes}
        Em uma corrida com a presença de adversários, as habilidades de efetuar 
        ultrapassagens e de bloquear os outros carros podem ser tão ou mais
        decisivas do que a capacidade de realizar voltas rápidas em pista livre.
        Em razão disso, mesmo não sendo o mais rápido, um piloto pode conseguir
        vencer provas coletivas. Isso é uma motivação para que esforços sejam
        direcionados em adaptar o comportamento do controlador em função dos
        oponentes próximos e eventualmente submeter o agente a uma edição do
        SCRC.
