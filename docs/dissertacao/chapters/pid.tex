% ---------------------------------------------------------------------------- %
\chapter{Controlador PID} \label{chap:pid}
% ---------------------------------------------------------------------------- %
%* TODO: Ramping
Para algumas das atividades que o piloto virtual precisa realizar durante as
sessões de testes, o exato comportamento desejado já é conhecido. Nessa
categoria estão, por exemplo, posicionar o carro ao centro da pista, manter uma
velocidade constante, atacar uma curva, entre outras.  Nesses contextos,
torna-se mais interessante aplicar métodos relacionados à Teoria de Controle do
que a  Aprendizado de Máquina. Em particular para essas atividades,
implementamos controladores do tipo PID
(\textbf{P}roporcional-\textbf{I}ntegrativo-\textbf{D}erivativo), que são o tema
deste capítulo.

O capítulo se inicia com uma breve introdução sobre sistemas de controle e os
princípios do PID. Adiante analisamos algumas situações nas quais o controlador
apresenta comportamentos indesejados e como contorná-las. Por fim, é abordado o
processo de ajuste de parâmetros necessário para personalizar o PID para cada
caso de uso.

% ---------------------------------------------------------------------------- %
\section{PID - Proporcional Integrativo Derivativo}
    Na teoria de controle, um sistema é visto como uma combinação de componentes
    usada para cumprir determinado objetivo. Quando um sistema toma a medição de
    uma grandeza, a compara com um valor de referência desejado e usa essa
    diferença como meio de controle, usa-se a denominação \textbf{sistema de
    controle com retroação} \cite{ogata}. Sistemas desse tipo são agrupados em
    duas categorias:

    \begin{description}
        \item[Sistemas de controle de malha aberta:]\hfill\\
            Os sistemas com retroação que produzem saída usando um sinal de
            entrada que se refere exclusivamente à medição de determinada
            grandeza são chamados de sistemas de malha aberta. A
            \mbox{Figura \ref{fig:malha_aberta}} mostra como é o esquema geral
            de um sistema desse tipo.

            \begin{minipage}{\linewidth}
                \centering
                \includegraphics[width=0.7\textwidth]{malha_aberta}
                \captionof{figure}{Representação em blocos de um sistema de
                                   malha aberta.}
                \label{fig:malha_aberta}
            \end{minipage}
            \vspace{0.1em}

        \item[Sistemas de controle de malha fechada:]\hfill\\
            Sistemas de malha fechada, por sua vez, além de considerarem o valor
            da medição, também tomam o próprio sinal de saída como parte da
            entrada. Com isso, tem-se realimentação do sistema, permitindo
            monitorar a diferença entre o valor medido e o resultado da saída
            devolvida, como ilustrado pela Figura \ref{fig:malha_fechada}. Essa
            diferença é chamada de \textbf{erro} e geralmente os sistemas de
            malha fechada buscam minimizar esse valor.

            \begin{minipage}{\linewidth}
                \centering
                \includegraphics[width=0.7\textwidth]{malha_fechada}
                \captionof{figure}{Representação em blocos de um sistema de
                                   malha fechada.}
                \label{fig:malha_fechada}
            \end{minipage}
            \vspace{0.1em}
    \end{description}

    \noindent
    \textit{Observação: Muitas vezes os sistemas de controle são chamados apenas
    de \emph{controladores}. Por praticidade e para seguir a convenção,
    adotaremos essa nomenclatura até o fim deste capítulo. Porém, no restante da
    dissertação o termo \emph{controlador} é usado ao nos referirmos aos pilotos
    virtuais.}
    \vspace{1em}

    O controlador \textbf{P}roporcional-\textbf{I}ntegrativo-\textbf{D}erivativo,
    que é um sistema de malha fechada, é notoriamente o algoritmo mais utilizado
    em aplicações de controle \cite{karl}. O princípio por trás desse
    controlador se resume a monitorar o valor de uma variável de interesse
    (denominado por \emph{process value}, ou simplesmente PV), avaliar a
    diferença entre PV e um determinado valor de referência (\emph{setpoint}, ou
    apenas SP) e produzir saídas que ajustam o processo externo ao controlador
    a fim de minimizar o erro entre PV e SP.

    A sigla PID se refere aos três termos que compõe a Equação \eqref{eq:pid},
    a qual rege o cálculo da resposta do controlador.

    \begin{equation}
        u(t) = K_p e(t) + K_i \int_{0}^{t} e(\tau) d\tau + K_d \dfrac{d}{dt} e(t)
        \label{eq:pid}
    \end{equation}

    \noindent
    Onde,
    \vspace{0.5em}

    \begin{tabular}{r l}
        PV:     & \emph{Process Value} - variável cujo valor é monitorado e
                  serve de entrada para sistema de controle;\\
        SP:     & \emph{Setpoint} - valor de referência até onde se deseja levar
                  PV;\\
        $u(t)$: & Sinal de saída do PID no instante $t$;\\
        $e(t)$: & Erro no instante $t$, sendo que $erro = SP - PV$;\\
        %$\tau$: & Process time constante (estimativa do tempo de resposta a
        %          alterações no sistema)\\
        $K_p$:  & Constante de ganho do termo proporcional;\\
        $K_i$:  & Constante de ganho do termo integrativo;\\
        $K_d$:  & Constante de ganho do termo derivativo;\\
    \end{tabular}
    \\

    Pode-se observar que o PID depende basicamente de três termos, sendo que a
    magnitude da contribuição de cada um deles é controlada pelas constantes de
    ganho $K_p$, $K_i$ e $K_d$. Os termos exercem influências distintas sobre o
    comportamento do sinal de resposta. Assim sendo, para conseguir ajustar o
    controlador de modo a resolver determinado problema atendendo às
    restrições impostas pelo contexto de uso, é preciso primeiro compreender
    as relações entre os termos e as características do sinal de saída.

    % ------------------------------------------------------------------------ %
    \subsection*{Termo Proporcional}
        O termo Proporcional está diretamente ligado à diferença entre o valor
        medido PV e o patamar de referência SP desejado. Em outras palavras, o
        componente Proporcional responde francamente ao erro do tempo presente.
        Essa característica está intimamente vinculada à ``responsividade'' (ou
        ``sensibilidade'') do controlador, isto é, à rapidez com que o sistema
        responde às mudanças da variável externa ou do \emph{setpoint}.

        Em certos cenários, quando o processo externo ao controlador é
        consideravelmente estável, um controlador puramente Proporcional (com
        $K_i = K_d = 0$) pode ser suficiente para garantir que PV alcance o
        \emph{setpoint} de maneira satisfatória. Entretanto, o desempenho dos
        controladores Proporcionais é limitado, principalmente em lidar com
        erros de pequena amplitude ao se atingir o estado estacionário. Também
        vale observar que altos parâmetros de ganho proporcional refletem em
        grandes variações na saída, criando oscilações e podendo até tornar o
        sistema instável.

    % ------------------------------------------------------------------------ %
    \subsection*{Termo Integrativo}
        O termo Integrativo é calculado com base na área sob a curva de erro, ou
        seja, o erro acumulado. Consequentemente, além da magnitude, a duração
        do período em que PV esteve longe do valor desejado também é relevante.
        Por essa razão, frequentemente interpreta-se o componente Integrativo
        como relacionado ao ``passado'' ou ao ``histórico'' de erro.

        O uso do fator integrativo ajuda a superar a deficiência do componente
        Proporcional em anular o erro residual no estado estacionário, pois
        mesmo para erros pequenos o valor da integral continua aumentando.
        Assim, eventualmente a saída do controlador continua levando o sistema
        em direção ao \emph{setpoint} até que o erro torne-se nulo de fato.

        Contudo, acompanhando o raciocínio anterior, é possível perceber que em
        algumas situações o fator Integrativo continuará empurrando a saída do
        controlador em um sentido mesmo depois de PV ultrapassar o
        \emph{setpoint}. Esse acontecimento recebe o nome de \emph{overshoot}
        (ou exagero sobre o alvo) e é um dos efeitos colaterais que o uso do
        Integrativo pode ocasionar. Longos períodos passados em erro faz com que
        o termo Integrativo induza permanecer em erro oposto.

        Além disso, dada sua característica acumulativa, é fácil notar que esse
        termo possui a tendência de tornar-se excessivamente grande. Por conta
        disso, geralmente $K_i$ é definida com valores consideravelmente
        inferiores às demais constantes. Portanto o componente Integrativo é
        usado como um fator de resposta lenta, embora ele contribua para
        acelerar a escalada de PV em direção à SP desde o início.

    % ------------------------------------------------------------------------ %
    \subsection*{Termo Derivativo}
        O termo Derivativo atua em resposta à taxa de variação do erro em
        relação ao tempo. Por conta disso, sua reação a mudanças tanto em PV
        quanto em SP são imediatas, característica que torna esse componente
        consideravelmente sensível a alterações ruidosas no erro. Em
        contrapartida, o Derivativo se anula na presença de erros constantes,
        mesmo quando eles apresentam alto valor.

        Devido a esse aspecto de responder à tendência do erro, o termo
        derivativo é interpretado como uma previsão do erro futuro. Tal
        propriedade se contrasta com os efeitos produzidos pelo Proporcional e
        Integrativo, pois faz com que o Derivativo seja o único termo capaz de
        combater o \emph{overshoot}. Isso acontece porque, à medida que PV se
        aproxima do \emph{setpoint}, o Proporcional vai perdendo força até que
        em certo momento (antes de PV ultrapassar SP) o Derivativo se torna o
        termo dominante, fazendo com que o PID passe a responder de modo a
        tentar reduzir a derivada, ou seja, tornar o erro constante. Vale notar
        que, assim como o Proporcional, o termo Derivativo pode contribuir para
        que o sistema atinja o estado estacionário sem que o erro seja
        completamente anulado (seja com PV abaixo do \emph{setpoint}, ou mesmo
        acima do \emph{setpoint}), ressaltando assim o papel do componente
        Integrativo.

    % ------------------------------------------------------------------------ %
    \subsection*{Algoritmo inicial}
        Tendo como base os conceitos apresentados nas seções anteriores,
        principalmente a Equação \eqref{eq:pid}, é possível construir o
        Algoritmo \ref{alg:simplePID} do sistema de controle PID. É preciso
        ressaltar que a abordagem apresentada omite qualquer tipo de controle de
        tempo, simplesmente por pressupor que as medições da variável de
        processo PV e as atualizações da saída são realizadas em intervalos
        discretos e regulares, caracterizando assim um \emph{controlador
        discreto com retroação} \cite{corripio}.
        
        Acompanhando o algoritmo é possível entender mais dois fatores atrativos
        desse tipo de controlador: o PID consegue ter robustez e flexibilidade
        quanto aos cenários de aplicação ao mesmo tempo em que $(i)$ possui
        implementação simples e $(ii)$ demanda baixo poder computacional. Pela
        junção dessas características o PID se mostra um algoritmo de controle
        muito favorável ao uso em sistemas embarcados e em microcontroladores.

        Apesar de já ser funcional para a maioria das situações, há casos nos
        quais o Algoritmo \ref{alg:simplePID} apresenta certos comportamentos
        indesejados. Por isso, essa pode ser entendida como uma versão inicial
        do controlador. Logo adiante são mostrados mais detalhes sobre os casos
        em que essa primeira implementação não é satisfatória, bem como uma
        versão adaptada do algoritmo.

        \begin{algorithm}[]
            \SetAlgoNoLine
            \SetKwFor{Para}{para}{faça}{fim}
            \SetKwProg{Fn}{}{}{fim}
            \SetKw{Devolva}{devolva}

            \SetCommentSty{textit}
            \SetKwComment{comment}{$\triangleright$ }{}

            \SetKwData{gets}{$\leftarrow$}

            \SetKwData{input}{input}
            \SetKwData{output}{output}
            \SetKwData{setpoint}{setpoint}
            \SetKwData{error}{error}
            \SetKwData{errorSum}{errorSum}
            \SetKwData{errorDelta}{dError}
            \SetKwData{lastError}{lastError}
            \SetKwData{pTerm}{P}
            \SetKwData{iTerm}{I}
            \SetKwData{dTerm}{D}
            \SetKwData{Kp}{$K_p$}
            \SetKwData{Ki}{$K_i$}
            \SetKwData{Kd}{$K_d$}

            \SetKwFunction{PID}{PID}

            \Fn{\PID{\input, \setpoint}}{
                \BlankLine
                \comment{Termo proporcional.}
                \error \gets $\setpoint - \input$\\
                \pTerm \gets $\Kp \times \error$\\

                \BlankLine
                \comment{Termo integrativo.}
                \errorSum \gets $\errorSum + \error$\\
                \iTerm \gets $\Ki \times \errorSum$\\

                \BlankLine
                \comment{Termo derivativo.}
                \errorDelta \gets $\error - \lastError$\\
                \lastError \gets \error\\
                \dTerm \gets $\Kd \times \errorDelta$\\

                \BlankLine
                \comment{Resultado.}
                \output \gets $\pTerm + \iTerm + \dTerm$\\
                \Devolva \output\\
            }
            \caption{Algoritmo simples de PID assumindo que o intervalo entre as
                     chamadas é constante.}
            \label{alg:simplePID}
        \end{algorithm}

% ---------------------------------------------------------------------------- %
\section{Problemas e soluções}
    Como dito anteriormente, apesar da formulação apresentada pelo Algoritmo
    \ref{alg:simplePID} ser funcional, ela está sujeita a produzir algumas
    anomalias na saída. Em muitos contextos, tais anomalias causam pouca ou
    nenhuma alteração indesejada no processo externo ao controlador, passando
    completamente desapercebidas. Entretanto, quando a saída é usada em
    controles delicados ou sensíveis, como o volante, essas anormalidade podem
    se tornar muito problemáticas. Para exemplificar, basta imaginar o que
    aconteceria se, durante uma curva, a saída do PID produzisse um pico e o
    piloto girasse abruptamente o volante - provavelmente um acidente seria
    inevitável.
        
    A seguir veremos duas questões que proporcionam a ocorrência de picos ou
    atrasos na resposta do PID e também como resolvê-las. Essas correções foram
    adicionadas ao algoritmo inicial, resultando na versão descrita pelo
    Algoritmo \ref{alg:finalPID}, a qual foi usada na implementação do piloto
    virtual.


    % ------------------------------------------------------------------------ %
    \subsection*{\emph{Derivative kick}}
        Considerando que um dos objetivos do PID é diminuir a diferença entre os
        valores da variável PV e o patamar de referência SP, é natural
        considerar que durante o uso do controlador SP 
        sofrerá mudanças. Porém quando o valor de SP muda significativamente de
        uma só vez, o erro é alterado instantaneamente e a derivada dessa
        alteração acaba sendo bem alta - a rigor, seria infinita uma vez que
        $dt$ tenderia a zero.

        Entretanto, em implementações digitais do PID, os intervalos de operação
        são regidos por um relógio (\emph{clock}) e, em especial nos
        controladores discretos, são regulares. Portanto $dt$ na prática não
        fica nulo de fato e, por sua vez, o Derivativo nunca chega a ser
        computado como infinito. Embora esse caso extremo nunca aconteça, ainda
        assim o Derivativo assume um valor desproporcionalmente alto no instante
        em que há o ``salto'' de SP. Esse pico de resposta é chamado de
        \emph{derivative kick} (ou coice derivativo, em tradução livre) e está
        ilustrado na Figura \ref{fig:derivative_kick}. É possível contornar esse
        problema atacando apenas o instante quando \emph{setpoint} é alterado.

        \begin{figure}[!htb]
            \centering
            \begin{subfigure}[!htb]{0.35\textwidth}
                \includegraphics[width=\textwidth]{derivative_kick}
                \caption{Termo derivativo considerando SP.}
                \label{fig:derivative_kick}
            \end{subfigure}
            \hspace{6em}
            \begin{subfigure}[!htb]{0.35\textwidth}
                \centering
                \includegraphics[width=\textwidth]{derivative_kick_solved}
                \caption{Termo derivativo calculado por PV.}
                \label{fig:derivative_kick_solved}
            \end{subfigure}
            \caption{Exemplificação de \emph{derivative kick} e sua solução.}
            \label{fig:derivative_kick_fix}
        \end{figure}

        \begin{align}
            \shortintertext{Sabemos que:}
            \dfrac{d\textrm{Erro}}{dt} &= \dfrac{d\textrm{SP}}{dt} - \dfrac{d\textrm{PV}}{dt} \nonumber \\
            \shortintertext{Portanto, quando SP é constante, temos:}
            \dfrac{d\textrm{Erro}}{dt} &= - \dfrac{d\textrm{PV}}{dt}
            \label{eq:kick}
        \end{align}

        Pela Equação \eqref{eq:kick}, vemos que enquanto o \emph{setpoint}
        permanecer inalterado podemos usar o negativo da derivada de PV no
        componente Derivativo sem alterar o comportamento do controlador.  Por
        sua vez, nos instantes nos quais SP varia, o Derivativo não é afetado e
        a saída não mais apresenta picos. Esse cenário está ilustrado na Figura
        \ref{fig:derivative_kick_solved}.  Portanto, para eliminar os picos
        indesejados basta calcular o termo Derivativo em função da variável de
        processo PV, não em função do erro.  Essa adaptação se reflete nas
        operações das linhas $6$ a $9$ do Algoritmo \ref{alg:finalPID}.

    % ------------------------------------------------------------------------ %
    \subsection*{Saturação do componente integrativo (\emph{Integrator windup})}
        A saída do PID normalmente é ligada a algum dispositivo capaz de
        realizar alterações no processo e consequentemente influenciar o valor
        de PV. Esses dispositivos de atuação praticamente sempre possuem algum
        tipo de limite ou restrição de suas capacidades.  Por exemplo, cada
        motor tem um limite máximo de quanta força consegue gerar.

        A formulação de PID vista no Algoritmo \ref{alg:simplePID} ignora
        completamente quaisquer limites para a saída. Portanto, num cenário
        hipotético, onde a saída controlasse diretamente a porcentagem da
        potência que um motor deveria usar, eventualmente o PID poderia devolver
        um valor maior que $100\%$. Mas certamente o motor não conseguiria
        realizar um trabalho usando mais que $100\%$ de sua potência. Nessa
        situação, há uma disparidade entre o valor que o PID ``pensa'' que está
        sendo usado no atuador e o valor que de fato está sendo aplicado, afinal
        o atuador permanecerá no seu limite independentemente da saída do
        controlador (como visto na Figura \ref{fig:windup}).

        \begin{figure}[htb]
            \centering
            \begin{subfigure}[!htb]{0.32\textwidth}
                \includegraphics[width=\textwidth]{windup}
                \vspace{0.5em}
                \caption{Saída do PID maior que o limite máximo do atuador.}
                \label{fig:windup}
            \end{subfigure}
            \hfill
            \begin{subfigure}[!htb]{0.32\textwidth}
                \centering
                \includegraphics[width=\textwidth]{windup_fix1}
                \vspace{0.5em}
                \caption{Termo integrativo limitado.}
                \label{fig:windup_fix1}
            \end{subfigure}
            \hfill
            \begin{subfigure}[!htb]{0.32\textwidth}
                \centering
                \includegraphics[width=\textwidth]{windup_fix2}
                \vspace{0.5em}
                \caption{Integrativo e saída do PID limitados.}
                \label{fig:windup_fix2}
            \end{subfigure}
            \caption{Problema de \emph{integrator windup} e as duas etapas de
                     solução.}
            \label{fig:windup_fix}
        \end{figure}

        No caso de controladores com o componente Integrativo, o erro acumulado
        continuará crescendo independentemente da saída devolvida ao processo.
        Com isso, esse termo pode eventualmente tornando-se exageradamente
        grande. Essa situação implica que o processo não se estabilizará
        enquanto o erro não permanecer por um longo período com sinal oposto ao
        acumulado.

        A solução desse problema é feita em duas etapas simples. A primeira
        consiste em apenas repassar para o PID o intervalo dos valores máximo e
        mínimo do atuador. Com essa informação, quando o termo integrativo
        atinge esses limites de saturação, interrompe-se a soma (ou subtração)
        do acumulador mantendo o valor do termo dentro do intervalo operacional
        do atuador. Como consequência disso, assim que o erro assume um valor
        com sinal oposto ao acumulado, o acumulador passa a diminuir e como
        resultado elimina-se o atraso na reação do controlador.
        
        Acompanhando a Figura \ref{fig:windup_fix1}, vemos que embora a técnica
        anterior já faça o PID responder prontamente à alterações em SP, a saída
        ainda continua apresentando valores maiores que o limite do atuador.
        Isso se deve ao fato de que, embora o Integrativo esteja controlado, o
        Proporcional e o Derivativo ainda podem contribuir no resultado final.
        Portanto a segunda etapa da solução consiste em simplesmente filtrar a
        saída de modo a mantê-la dentro das restrições, resultando no
        comportamento ilustrado pela \mbox{Figura \ref{fig:windup_fix2}}.

        Uma observação a ser feita é que, se apenas aplicássemos a segunda etapa
        da solução, a saída do PID se manteria controlada de fato, porém o termo
        Integrativo continuaria crescendo indiscriminadamente. Dessa forma, se
        manteriam o atraso na reação e a exigência de um longo período com erro
        oposto ao acumulado para o controlador atingir o estado estacionário.

    % ------------------------------------------------------------------------ %
    \subsection*{Algoritmo corrigido}
        Incorporando as melhorias apresentadas à implementação inicial do PID,
        chegamos ao Algoritmo \ref{alg:finalPID}. Essa foi a versão integrada ao
        piloto virtual. Porém não basta ter em mãos o algoritmo do controlador,
        ainda é preciso definir as constantes $K_p$, $K_i$, $K_d$ a fim de que o
        PID se comporte como o desejado para cada contexto. Mostramos na próxima
        seção como determinar os valores para cada constante.

        \begin{algorithm}[]
            \SetAlgoNoLine
            \SetKwFor{Para}{para}{faça}{fim}
            \SetKwProg{Fn}{}{}{fim}
            \SetKw{Devolva}{devolva}

            \SetCommentSty{textit}
            \SetKwComment{comment}{$\triangleright$ }{}

            \SetKwData{gets}{$\leftarrow$}

            \SetKwData{input}{input}
            \SetKwData{output}{output}
            \SetKwData{setpoint}{setpoint}
            \SetKwData{error}{error}
            \SetKwData{inputDelta}{dInput}
            \SetKwData{lastInput}{lastInput}
            \SetKwData{pTerm}{P}
            \SetKwData{iTerm}{I}
            \SetKwData{dTerm}{D}
            \SetKwData{Kp}{$K_p$}
            \SetKwData{Ki}{$K_i$}
            \SetKwData{Kd}{$K_d$}
            \SetKwData{upperBound}{$LIMITE_{max}$}
            \SetKwData{lowerBound}{$LIMITE_{min}$}

            \SetKwFunction{PID}{PID}
            \SetKwFunction{min}{Min}
            \SetKwFunction{max}{Max}

            \Fn{\PID{\input, \setpoint}}{
                \BlankLine
                \comment{Termo proporcional.}
                \error \gets $\setpoint - \input$\\
                \pTerm \gets $\Kp \times \error$\\

                \BlankLine
                \comment{Termo integrativo. (Obs: \iTerm deve ser inicializado com $0$.)}
                \iTerm \gets $\iTerm + \Ki \times \error$\\
                \iTerm \gets \max{\min{\iTerm, \upperBound}, \lowerBound}\\

                \BlankLine
                \comment{Termo derivativo.}
                \inputDelta \gets $\input - \lastInput$\\
                \lastInput \gets \input\\
                \dTerm \gets $\Kd \times \inputDelta$\\

                \BlankLine
                \comment{Resultado.}
                \output \gets $\pTerm + \iTerm - \dTerm$\\
                \output \gets \max{\min{\output, \upperBound}, \lowerBound}\\

                \BlankLine
                \Devolva \output\\
            }

            \caption{Algoritmo final de PID com as modificações citadas.}
            \label{alg:finalPID}
        \end{algorithm}

% ---------------------------------------------------------------------------- %
\section{Processo de ajuste - \emph{Tuning}}

    Pela Equação \eqref{eq:pid} vemos que a saída do PID é fundamentalmente
    dependente das constantes $K_p$, $K_i$ e $K_d$. Portanto, definir esses
    valores basicamente molda o comportamento do controlador perante às
    alterações na variável de processo e no \emph{setpoint}. O procedimento
    feito para determinar o ganho em cada um dos termos é chamado de
    \emph{tuning} (ou processo de ajuste).

    Na literatura da área de controle, encontra-se métodos formais para se
    fazer o ajuste do PID de modo a atender rigorosamente às especificações de
    cada caso de uso. Por exemplo, o método de Ziegler-Nichols \cite{ziegler}
    se apoia na análise dos parâmetros de resposta do sistema para testes
    específicos, permitindo assim calcular as constantes de modo que a resposta
    do controlador sempre gere efeitos dentro de limites determinados como
    aceitáveis.

    Cada processo de ajuste demanda algum tipo específico de informação sobre a
    dinâmica do processo onde deseja-se aplicar o PID. Dessa forma, a escolha do
    método depende de quão prático é conseguir as informações necessárias
    através de ensaios com o sistema e do quão rigorosamente é preciso seguir as
    exigências sobre a saída.

    Embora o rigor e a exatidão desse tipo de método sejam grandes pontos
    positivos, em contrapartida é necessário preparar ensaios e medições para 
    conseguir aplicá-los. Muitas vezes essas preparações demandam tão trabalho
    e os requerimentos de resposta não são muito rígidos. Nesses casos, métodos
    de ajuste complexos se mostram pouco práticos.

    Na Tabela \ref{tab:kchange} vemos de forma simplificada quais são as
    implicações de se aumentar cada uma das constantes de ganho. Baseando-se
    apenas nesse conhecimento, é possível fazer ciclos iterativos alternando
    entre alterar os parâmetros e verificar o comportamento resultante do
    sistema. Apesar desse processo de ajuste manual consistir basicamente em
    tentativa e erro, ele é frequentemente usado em cenários simplificados, pois
    é possível rapidamente chegar a configurações que produzam resultados
    satisfatórios.

    Para os controladores PID presentes no piloto virtual, ajustamos manualmente
    as constantes. A seguir mostraremos um procedimento para realizar o processo
    de ajuste de forma eficiente.

    \newcolumntype{R}{>{\raggedleft\arraybackslash}X}%
    \newcolumntype{C}{>{\centering\arraybackslash}X}%
    \begin{table}[hbt]
        \centering
        \caption{Efeitos ao se alterar as constantes de ganho do PID \cite{li}.}
        \label{tab:kchange}
        \small
        \begin{tabularx}{\linewidth}{lCCCCc}
            \toprule
                                       & \textbf{Tempo de ascensão}
                                       & \textbf{Sobrepassagem (overshoot)}
                                       & \textbf{Tempo de estabilização}
                                       & \textbf{Erro no estado estacionário}
                                       & \textbf{Estabilidade} \\
            \midrule
            \addlinespace
            \textbf{Incrementar $K_p$} & Diminui       & Aumenta
                                       & Aumenta pouco & Diminui
                                       & Piora   \\[0.5em]

            \textbf{Incrementar $K_i$} & Diminui pouco & Aumenta & Aumenta
                                       & Diminui muito & Piora   \\[0.5em]

            \textbf{Incrementar $K_d$} & Diminui pouco & Diminui & Diminui
                                       & Pouco muda    & Melhora \\
            \addlinespace
            \bottomrule
        \end{tabularx}
    \end{table}

    % ------------------------------------------------------------------------ %
    \subsection*{Método de ajuste manual}
        Com o auxílio da Tabela \ref{tab:kchange}, cada alteração nas constantes
        de ganho pode ser feita de forma mais consciente do que por meros
        palpites. Além disso, basta seguir uma metodologia simples de como fazer
        essas alterações para ajudar a minimizar o número de testes necessários
        até que o controlador produza o comportamento desejado.  Primeiramente
        deve-se deixar todas as constantes com valor zero. Depois, os passos a
        serem seguidos em cada iteração de ajuste são:

        \begin{enumerate}
          \item Aumentar $K_p$ aos poucos até que a saída do controlador se
                torne oscilatória (deve-se ter cuidado para não tornar o sistema
                instável ao se aumentar demasiadamente o valor de $K_p$);
          \item Aumentar $K_d$ até reduzir a oscilação (ou até não haver mais
                \emph{overshoot}, eliminando completamente a oscilação);
          \item Aumentar $K_i$ até que o erro no estado estacionário seja
                anulado tão rápido quanto se desejar.
        \end{enumerate}

        Uma vez terminado um ciclo de ajuste, é perfeitamente válido reajustar
        os valores das constantes, independentemente de ordem. Afinal, cada
        processo tem suas peculiaridades e muitas vezes será preciso fazer
        alterações sutis a fim encontrar o melhor ajuste fino.

