% ---------------------------------------------------------------------------- %
\chapter{Modelagem da pista de corrida}
\label{chap:pista}
% ---------------------------------------------------------------------------- %
    Apesar de nosso carro virtual estar munido com uma ampla gama de sensores,
    nenhum deles fornece de imediato dados sobre a pista de corrida como um
    todo. Embora essa informação não esteja disponível a princípio, ela é de
    grande valia caso se deseje elaborar um controlador que não seja
    simplesmente reativo aos sensores. Construir um modelo da pista possibilita
    a identificação de trechos com características similares, o que pode ser
    aproveitado no processo de aprendizagem, além de permitir eventualmente a
    exploração de técnicas de planejamento.

    Neste capítulo, apresentamos um método de como construir uma representação
    da pista a partir da consulta a apenas um pequeno subconjunto dos sensores
    disponíveis no carro virtual. Além disso, propomos uma técnica para
    segmentar esse modelo da pista em trechos e classificá-los de acordo com
    suas particularidades.  Esses tópicos compõem um trabalho apresentado no
    simpósio SBGAMES $2014$ \cite{daros}, porém o presente capítulo contém
    refinamentos na abordagem do tema, produzindo melhores resultados.


% ---------------------------------------------------------------------------- %
\section{Construção de um modelo de pista}
    Durante a primeira volta, nosso piloto virtual coleta dados da pista para
    montar um modelo do circuito, isto é, uma representação do formato da pista
    como um todo. Esse modelo é usado posteriormente nas etapas de segmentação e
    classificação a fim de facilitar o processo de aprendizagem. A seguir,
    descrevemos como nosso agente usa seus sensores para construir tal modelo.


    % ------------------------------------------------------------------------ %
    \subsection{Piloto coletor de dados}
        De forma semelhante a um robô móvel em um ambiente desconhecido a ser
        explorado, nosso piloto virtual percorre a primeira volta em cada
        circuito coletando dados por meio de seus sensores, listados
        anteriormente na Tabela \ref{tab:sensors}. Esses dados são processados
        de modo a se produzir um modelo do formato da pista. Assim, durante a
        primeira volta, o agente realiza praticamente apenas três atividades:
        \begin{itemize}
            \item Manter o carro centralizado sobre o eixo da pista;
            \item Manter a velocidade baixa e constante (em torno de $40 km/h$);
            \item Coletar e armazenar os dados dos sensores a cada $5$ metros
                  percorridos.
        \end{itemize}

        \begin{figure}[!htb]
            \centering
            \begin{subfigure}[t]{0.49\textwidth}
                \centering
                \includegraphics[width=\textwidth]{hairpin_open}
                \caption{Controle do volante com base na Equação
                         \eqref{eq:steer}.}
                \label{fig:hairpin_open}
            \end{subfigure}
            \hfill
            \begin{subfigure}[t]{0.49\textwidth}
                \centering
                \includegraphics[width=\textwidth]{hairpin_close}
                \caption{Controle do volante com PID.}
                \label{fig:hairpin_close}
            \end{subfigure}
            \caption{Diferença de posicionamento ao se passar pelo
                     \emph{hairpin} da pista \emph{street-1}.}
            \label{fig:hairpin}
        \end{figure}

        Para manter o carro ao centro da pista, o piloto virtual pode se basear
        nas leituras dos sensores \emph{angle} e \emph{trackPos}. Retomando a
        Figura \ref{fig:angle_sensor}, vemos que o sensor \emph{angle} mede o
        ângulo formado entre a direção do carro e o eixo da pista. Entretanto,
        mesmo que esse ângulo seja nulo, ainda assim o carro pode não estar
        posicionado como o desejado. Um exemplo dessa situação está ilustrado na
        Figura \ref{fig:trackpos_sensor}, onde vemos que o carro está alinhado,
        porém afastado do trajeto central. Na mesma imagem, também vemos que a
        distância desse afastamento é medida pelo sensor \emph{trackPos}.

        Quando o carro está na posição ideal, as medições de ambos sensores
        estarão em zero. Portanto, o valor \emph{steer} de quanto se precisa
        esterçar o volante deve ser determinado de modo a se contrapor a
        medições não nulas. Uma forma simples de calcular \emph{steer} é através
        da Equação \eqref{eq:steer}, onde \emph{steerLock} é uma constante do
        carro a qual indica o ângulo máximo de esterção do volante.

        \begin{equation}
            \label{eq:steer}
            steer = \frac{angle - trackPos \times steerLock}{steerLock}
        \end{equation}

        Segundo a Tabela \ref{tab:effectors}, o atuador do volante só pode
        receber valores de entrada no intervalo $[-1, 1]$. Embora o termo
        \emph{trackPos} seja limitado pelo mesmo intervalo, \emph{angle}, por
        sua vez, pode assumir valores com módulo maior que \emph{steerLock}. Por
        conta disso, depois do cálculo de \emph{steer}, é preciso ter o cuidado
        de saturar o valor a ser repassado para o atuador.

        Embora a Equação \eqref{eq:steer} seja simples e funcional, ela não leva
        em conta o fato de carro não ser capaz de se reposicionar
        instantaneamente de acordo com o pretendido. Como resultado, é possível
        verificar durante as simulações que o carro não se mantém
        satisfatoriamente ao centro da pista. Um flagrante desse efeito pode ser
        visto na Figura \ref{fig:hairpin_open}, a qual mostra o piloto virtual
        conduzindo o carro por um \emph{hairpin} (curva do tipo grampo) da pista
        \emph{street1}. Como essa pista apresenta uma faixa tracejada ao
        centro, nota-se claramente o quanto o posicionamento do carro fica
        defasado em relação ao ideal.

        Esse comportamento é indesejado, pois a análise dos dados para a criação
        do modelo de cada pista (processo explicado na próxima seção) é feita
        assumindo que as medições foram realizadas a partir de pontos
        perfeitamente centralizados. Por essa razão, faz-se necessário um método
        de controle mais robusto para gerenciar o atuador do volante de modo a
        manter o carro alinhado ao centro da pista o mais precisamente possível.

        Dada essa necessidade, os comandos do volante de nosso agente são
        regidos por um controlador PID que recebe o valor do sensor
        \emph{trackPos} e tenta posicionar o volante de modo a minimizar essa
        medida. Isso produz um resultado nitidamente melhor em comparação ao uso
        da Equação \eqref{eq:steer}, como pode-se verificar na Figura
        \ref{fig:hairpin_close}.

        Quanto ao controle da velocidade, ingenuamente, pode-se pensar que
        manter o acelerador com pressão constante seja suficiente para também
        manter a velocidade em um mesmo patamar. Entretanto, o simples ato de
        contornar as curvas já faz o carro ir mais devagar caso não haja uma
        compensação no acelerador. Além disso, as pistas possuem aclives e
        declives, os quais também influenciam diretamente a velocidade do carro.
        Em virtude disso, usamos outro controlador PID para gerenciar o
        acelerador e o freio durante a volta de leitura de dados. Por esse
        método, o carro é mantido com velocidade praticamente constante,
        variando entre $39$ e $40 km/h$.

        Já a gravação dos dados em intervalos regulares é a atividade mais
        simples. O monitoramento da distância é feito pelo acompanhamento das
        leituras do sensor \emph{distFromStart}, o qual informa o quanto se
        percorreu na pista a partir da linha de largada. Assim que se inicia a
        primeira volta, o piloto virtual registra os dados dos sensores
        disponíveis. A partir daí, verifica-se a cada instante a diferença entre
        \emph{distFromStart} e o ponto do último registro. Quando a diferença é
        maior ou igual a $5$ metros, faz-se então uma nova gravação. Ao término
        da volta, os dados armazenados são usados nas etapas seguintes de
        análise, além de serem exportados em arquivos para eventual verificação
        posterior.


    % ------------------------------------------------------------------------ %
    \subsection{Curvaturas}
        Coletados todos os dados referentes a uma volta completa pela pista,
        podemos usá-los para montar uma representação do percurso. Cada elemento
        desses dados em questão é o conjunto dos valores dos sensores no momento
        da medição, que ocorre aproximadamente a cada $5$ metros. Portanto,
        tomando-os aos pares, é possível extrair informações para descrever
        pequenas fatias da pista, as quais chamaremos de \textbf{segmentos}.
        Assim, temos que a diferença de valores em \emph{distFromStart} revela o
        comprimento exato do segmento, mas também é preciso descobrir se esse
        segmento faz parte de uma reta ou se o carro mudou de direção entre as
        duas medições em foco.

        Para conseguir fazer essa distinção, pode-se consultar \emph{steer} e
        verificar o quanto o volante estava esterçado nas leituras inicial e
        final. Se ao menos um dos valores não for nulo, então o carro virou no
        intervalo entre as medições. Nesse caso, é importante descobrir o quanto
        o carro virou, ou seja, a \textbf{curvatura} do segmento. Essa
        informação não está contida diretamente em nenhum dos sensores
        disponíveis e para consegui-la é preciso realizar uma série de
        operações.

        \begin{figure}[!htb]
            \centering
            \includegraphics[width=0.58\textwidth]{bicycle_model}
            \caption{Modelo de bicicleta usado para simplificar o carro.}
            \label{fig:bicycle_model}
        \end{figure}

        Uma simplificação que adotamos foi interpretar o carro seguindo o
        \emph{modelo da bicicleta} \cite{gillespie} ilustrado na Figura
        \ref{fig:bicycle_model}. Na ilustração, \emph{WB (wheelbase)} é a
        distância entre os eixos das rodas, $\delta$ é o ângulo em que o volante
        está esterçado e \emph{R} é o raio da curva, o qual queremos calcular.
        Por esse modelo, quando $\delta$ assume valores pequenos, o raio da
        curva pode ser aproximado pela Equação \eqref{eq:radius}.

        \begin{equation}
            \label{eq:radius}
            R = \frac{WB}{tg(\delta)} \simeq \frac{WB}{sen(\delta)}
        \end{equation}

        Tendo o raio da curva, pela Equação \eqref{eq:alpha} encontramos o
        ângulo $\alpha$ do arco percorrido pelo carro ao andar por esse
        segmento. O termo $r$ é a constante do raio das rodas dianteiras,
        $\omega$ é o valor do sensor \emph{wheelSpinVel} de velocidade angular
        da roda externa à curva, $\Delta_t$ é o intervalo entre as medições do
        início e fim do segmento e $T$ é a constante \emph{TRACK} representando
        a distância entre as rodas do mesmo eixo.

        \begin{equation}
            \label{eq:alpha}
            \alpha = \frac{r \times \omega \times \Delta_t}{R + \frac{T}{2}}
        \end{equation}

        Repetindo esse processo para todos os pares de dados adjacentes, cada
        fatia da pista passa a ser descrita por um segmento composto por:
        (\emph{i}) um \emph{comprimento}, próximo a $5$ metros; e
        (\emph{ii})uma \emph{curvatura}, referente ao ângulo coberto pelo arco
        que o carro fez. Contudo, ao longo das etapas descritas há
        arredondamentos e imprecisões tanto em decorrência do intervalo entre
        medições, quanto pelo posicionamento não ideal do carro no instante de
        cada coleta. Por esse motivo, a concatenação desses segmentos não forma
        um circuito fechado.  Esse efeito já foi apresentado em outros trabalhos
        \cite{munoz2}, porém não caracteriza um problema, pois o modelo da pista
        não é usado pelo controlador para determinar seu posicionamento.

        Apesar disso, considerando pistas que não apresentem cruzamentos,
        sabemos que a soma de todos os ângulos do circuito deveria resultar em
        $2\pi$. Com essa suposição, podemos calcular a proporção entre $2\pi$ e
        a soma dos ângulos definindo assim um \emph{fator de correção}.
        Multiplicando todas as curvaturas por esse fator, conseguimos um modelo
        cujas extremidades tenham a mesma direção - embora não necessariamente
        estejam alinhadas, como visto na Figura \ref{fig:track_grids}. Nas
        mesmas ilustrações estão representadas as divisões dos segmentos.

        \begin{figure}[!hbt]
            \centering
            \begin{subfigure}[!htb]{0.79\textwidth}
                \centering
                \includegraphics[width=\textwidth]{e-track-3_grid}
                \caption{Pista e-track-3.}
                \label{fig:e-track-3_grid}
            \end{subfigure}
            \hspace{-12em}
            \begin{subfigure}[!htb]{0.45\textwidth}
                \centering
                \includegraphics[width=\textwidth]{g-track-1_grid}
                \caption{Pista g-track-1.}
                \label{fig:g-track-1_grid}
            \end{subfigure}
            \vspace{5em}

            \begin{subfigure}[!htb]{0.42\textwidth}
                \centering
                \includegraphics[width=\textwidth]{a-speedway_grid}
                \caption{Pista a-speedway.}
                \label{fig:a-speedway_grid}
            \end{subfigure}
            \hfill
            \begin{subfigure}[!htb]{0.55\textwidth}
                \centering
                \includegraphics[width=\textwidth]{e-track-2_grid}
                \caption{Pista e-track-2.}
                \label{fig:e-track-2_grid}
            \end{subfigure}
            \caption{Modelos de pista com divisórias dos segmentos.}
            \label{fig:track_grids}
        \end{figure}

% ---------------------------------------------------------------------------- %
\section{Segmentação}
%Conseguir fazer o aprendizado ser por trecho e não por pista (Com isso, o
%treinamento fica mais genérico e o agente pode encarar qualquer pista depois
%reaproveitando o que ele descobriu nos trecho das pistas de treinamento)
    Como vimos na seção anterior, o modelo formado pelos segmentos de $5$ metros
    é capaz de remontar as pistas de modo aceitável. Entretanto a informação
    contida em cada um desses segmentos é pouco expressiva e não há vantagem
    alguma em lidar com uma representação tão fragmentada. Ao observar o desenho
    de um circuito, é fácil para uma pessoa distinguir onde começa e termina
    cada reta, quão fechada é cada curva e quando um trecho é longo ou curto.
    Para um piloto, esse entendimento é muito importante, pois a informação do
    tipo de trecho em que se está e do(s) trecho(s) seguinte(s) tem influência
    direta na decisão de qual ação executar. Dessa forma, aglutinar os segmentos
    em trechos relativamente uniformes traz as seguintes vantagens:

    \begin{itemize}%[noitemsep,topsep=0pt,parsep=0pt]
        \item Simplifica-se o modelo. Por
              exemplo, a pista g-track-1 vista na Figura
              \ref{fig:g-track-1_grid} pode ser representada por apenas $12$
              trechos em vez de 400 segmentos;

        \item A descrição por trechos é mais eficiente, transmitindo informações
              de forma concisa e significativa. Por exemplo, o fato dos
              primeiros $70$ segmentos de g-track-1 formarem uma reta de $350$
              metros é uma informação simples e valiosa;

        \item Facilita-se a leitura, compreensão e interpretação dos dados.
    \end{itemize}

    Para realizar a segmentação, isto é, determinar os pontos nos quais um
    trecho termina e outro começa, propomos um método que avalia as variações
    das curvaturas dos segmentos como oscilações de um sinal. Na Figura
    \ref{fig:track_raw_plots} estão representados os ângulos de curvatura dos
    segmentos das pistas g-track-1 e a-speedway, cujos modelos já foram
    mostrados na Figuras \ref{fig:g-track-1_grid} e \ref{fig:a-speedway_grid}
    respectivamente. Nos gráficos, o eixo das abscissas indica a distância do
    segmento até a linha de largada e o eixo das ordenadas se refere aos ângulos
    de curvatura. Vale notar que ângulos negativos indicam curvas para a direita
    e curvas para a esquerda tem ângulos positivos. Comparando-se com as
    figuras, é fácil observar que os trechos de curva nas pistas tem paralelo
    com patamares nos gráficos. Porém, também é possível perceber que há regiões
    aparentemente homogêneas na pista que possuem oscilações consideráveis no
    sinal.

    \begin{figure}[]
        \centering
        \begin{subfigure}[!htb]{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{g-track-1_plot_raw}
        \end{subfigure}
        \vspace{1em}

        \begin{subfigure}[!htb]{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{a-speedway_plot_raw}
        \end{subfigure}
        \caption{Representação dos ângulos de curvatura das pistas g-track-1 e
                 a-speedway representados como sinais ao longo da distância
                 percorrida.}
        \label{fig:track_raw_plots}
    \end{figure}

    Nosso objetivo com o segmentador é filtrar o sinal original de modo a
    encontrar os \textbf{patamares} que representam trechos homogêneos na pista
    mesmo com a existência de oscilações. O método que desenvolvemos para tratar
    esses sinais é descrito pelo Algoritmo \ref{alg:segmenter}, o qual recebe os
    vetores \textvar{distâncias} e \textvar{ângulos} de tamanho $n$ (que
    representam o sinal da pista) e devolve duas novas listas com as distâncias
    e ângulos representando o sinal contendo apenas os pontos referentes aos
    patamares encontrados. Dessa forma, tomando dois pontos consecutivos desse
    novo sinal, ou eles estão alinhados (indicando um patamar) ou há uma
    diferença significativa de ângulo entre eles (indicando um  ``salto'').

    O primeiro passo realizado pelo algoritmo é passar o vetor \textvar{ângulos}
    por um filtro de média ponderada para atenuar os picos locais do sinal
    original. Assim, para cada ângulo $a_i$, \textfunc{filtroDeMédia()}
    \mbox{devolve $a'_i$} de acordo com a regra \eqref{eq:mean}.

    \begin{equation}
        \label{eq:mean}
        %\beta \ \leftarrow \ \dfrac{\alpha_{i-1} + 2 \alpha_i + \alpha_{i+1}}{4}
        a'_i = \begin{cases}
            \dfrac{a_{i-1} + 2a_i + a_{i+1}}{4} &, i \in [1, n-2]\\
            a_i &, \text{\it caso contrário.}
        \end{cases}
    \end{equation}

    \begin{algorithm}[]
        \SetAlgoNoLine
        \SetKwFor{Para}{para}{faça}{fim}
        \SetKwFor{ParaCada}{para cada}{faça}{fim}
        \SetKwProg{Fn}{}{}{fim}
        \SetKw{Devolva}{devolva}
        \SetKw{E}{e}
        \SetKw{Ou}{ou}
        \SetKw{Nao}{não}

        \SetCommentSty{textit}
        \SetKwComment{comment}{$\triangleright$ }{}

        \SetKwData{gets}{$\leftarrow$}

        \SetKwData{pontos}{ângulos}
        \SetKwData{dfs}{distâncias}
        \SetKwData{limiar}{limiar}
        \SetKwData{scale}{ESCALA}
        \SetKwData{maxStraight}{MAX\_RETA}
        \SetKwData{base}{base}
        \SetKwData{flatD}{distFiltrada}
        \SetKwData{flatA}{angFiltrado}
        \SetKwData{bd}{distInício}
        \SetKwData{ba}{angInício}
        \SetKwData{fd}{distFim}
        \SetKwData{fa}{angFim}

        \SetKwFunction{Filtro}{filtroDePatamar}
        \SetKwFunction{FiltroMedia}{filtroDeMédia}
        \SetKwFunction{desvio}{desvioPadrão}
        \SetKwFunction{push}{adiciona}
        \SetKwFunction{isGap}{éSalto}
        \SetKwFunction{isBaseline}{éPatamar}

        \Fn{\Filtro{\dfs, \pontos, $n$}}{
            \pontos \gets \FiltroMedia{\pontos}\\
            \limiar \gets \scale $\times$ \desvio{\pontos}\\
            \base \gets $0$\\
            \flatD.\push{$0$}\\
            \flatA.\push{\base}\\
            \Para{$i$ \gets $1$ \Ate $n - 2$}{
                \Se{\isGap{$\pontos[i]$, \base, \limiar}}{
                    \bd \gets $\dfs[i-1]$\\
                    \fd \gets $\dfs[i]$\\
                    \ba \gets \base\\

                    \Enqto{$i < n - 3$ \E \Nao \isBaseline{\pontos, $i$}}{
                        \comment{Se houve mudança de sinal, ajusta a distância.}
                        \Se{$\pontos[i] \times \pontos[i-1] \leq 0$}{
                            \bd \gets $\dfs[i-1]$\\
                            \fd \gets $\dfs[i]$\\
                        }
                        $i$ \gets $i + 1$\\
                    }

                    \fa \gets $\pontos[i]$\\
                    \Se{$|\fa| < \maxStraight$}{\fa \gets $0$}

                    \BlankLine
                    \comment{Ponto do início do salto.}
                    \flatD.\push{\bd}\\
                    \flatA.\push{\ba}\\

                    \BlankLine
                    \comment{Ponto do fim do salto.}
                    \flatD.\push{\fd}\\
                    \flatA.\push{\fa}\\
                    \base \gets \fa\\
                }
            }
            \flatD.\push{$\dfs[n]$}\\
            \flatA.\push{\base}\\
            \Devolva{\flatD, \flatA}\\
        }
        \caption{Método de segmentação, onde $n$ é o tamanho dos vetores
                 \textvar{distâncias} e \textvar{ângulos}.}
        \label{alg:segmenter}
    \end{algorithm}

    Na sequência, é definido o \textvar{limiar} acima do qual a diferença entre
    dois ângulos é considerada significativa, um ``salto''. Esse limiar é
    proporcional ao desvio padrão das diferenças entre ângulos consecutivos.
    Para nossos testes, o fator \textvar{ESCALA} usado foi $4$. Em seguida, o
    ponto inicial $(0, 0)$ é adicionado ao novo sinal e inicia-se a iteração
    sobre os pontos do sinal.
    
    A função \textfunc{éSalto()} identifica se: ou  a diferença entre o ângulo
    atual e o ângulo \textvar{base} é suficientemente grande; ou esses dois
    ângulos estão em curvas de direções opostas; ou um dos ângulos faz parte de
    uma reta e o outro de uma curva. Nesses casos, \textfunc{éSalto()} devolve
    \emph{verdade}. Quando um salto é encontrado, \textvar{distInício} e
    \textvar{angInício} indicam o ponto do início do salto. Já \textvar{distFim}
    e \textvar{angFim} marcarão o fim do salto e início de um patamar.

    A função \textfunc{éPatamar()} devolve \emph{verdade} quando os ângulos
    imediatamente à frente de $i$ tem valores próximos. Durante a busca ao fim
    do salto, há a possibilidade de dois ângulos consecutivos terem sinais
    diferentes, indicando duas curvas para direções opostas em sequência.
    Quando isso acontece, as distâncias de início e fim do salto são reajustadas
    para que o salto cruze o eixo das abscissas no ponto onde há a inversão das
    curvas. Caso o valor absoluto de \textvar{angInício} seja menor que
    \textvar{MAX\_RETA}, o arredondamos para zero, indicando que o patamar
    encontrado é o início de uma reta. A constante \textvar{MAX\_RETA}
    representa a curvatura máxima que um trecho pode ter e ainda ser considerado
    uma reta. Em nossos experimentos, seu valor foi definido como $0.008$.

    \begin{figure}[]
        \centering
        \begin{subfigure}[!htb]{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{g-track-1_plot}
        \end{subfigure}
        \vspace{2em}

        \begin{subfigure}[!htb]{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{a-speedway_plot}
        \end{subfigure}
        \vspace{2em}

        \begin{subfigure}[!htb]{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{e-track-3_plot}
        \end{subfigure}
        \vspace{2em}

        \begin{subfigure}[!htb]{\textwidth}
            \centering
            \includegraphics[width=\textwidth]{e-track-2_plot}
        \end{subfigure}
        \caption{Exemplos de comparação entre os sinais de algumas pistas.}
        \label{fig:track_plots}
    \end{figure}

    Ao término da execução do algoritmo, cada salto precede um patamar, o qual é
    uma região uniforme da pista. Nos gráficos da Figura \ref{fig:track_plots}
    estão mostradas as comparações entre os sinais originais, com filtro de
    média ponderada e com filtro de patamar das pistas apresentadas na Figura
    \ref{fig:track_grids}. Como resultado, a segmentação dessas pistas está
    indicada na Figura \ref{fig:track_edges}, onde as marcações apontam as
    divisões dos trechos em decorrência dos saltos encontrados.
    \begin{figure}[]
        \centering
        \begin{subfigure}[!htb]{0.79\textwidth}
            \centering
            \includegraphics[width=\textwidth]{e-track-3_edge}
            \caption{e-track-3}
            \label{fig:e-track-3_edge}
        \end{subfigure}
        \hspace{-12em}
        \begin{subfigure}[!htb]{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{g-track-1_edge}
            \caption{g-track-1}
            \label{fig:g-track-1_edge}
        \end{subfigure}
        \vspace{2em}

        \begin{subfigure}[!htb]{0.42\textwidth}
            \centering
            \includegraphics[width=\textwidth]{a-speedway_edge}
            \caption{a-speedway}
            \label{fig:a-speedway_edge}
        \end{subfigure}
        \hfill
        \begin{subfigure}[!htb]{0.55\textwidth}
            \centering
            \includegraphics[width=\textwidth]{e-track-2_edge}
            \caption{e-track-2}
            \label{fig:e-track-2_edge}
        \end{subfigure}
        \caption{Marcação da segmentação das pistas.}
        \label{fig:track_edges}
    \end{figure}

    Vale notar que a pista g-track-1 termina coincidentemente com o fim do
    último segmento. Por essa razão, há uma marcação no final da última curva,
    na linha de chegada da Figura \ref{fig:g-track-1_edge}. Nas demais pistas,
    não há tal marcação, pois a linha de chegada está posicionada no meio de
    retas, de modo que o piloto virtual interpreta que as porções antes e depois
    desse ponto fazem parte do mesmo trecho.


% ---------------------------------------------------------------------------- %
\section{Classificação}
    Depois da segmentação, cada trecho passa a ser descrito por três atributos:
    (\emph{i}) \textvar{comprimento};
    (\emph{ii}) \textvar{raio}; e
    (\emph{iii}) ângulo $\alpha$. O \textvar{comprimento} é simplesmente a
    distância entre o início e o fim do trecho. O \textvar{raio} indica quanto
    uma curva é fechada, lembrando que é possível considerar as retas como tendo
    raio infinito. Por fim, $\alpha$ indica o ângulo coberto pelo carro ao
    percorrer uma curva.

    Esses três atributos são suficientes para descrever as características
    principais dos trechos e, por conta disso, podem ser usados como parâmetros
    de distinção. Assim, após a etapa de segmentação, o agente analisa cada
    trecho da pista e o classifica seguindo a árvore de decisão descrita no
    Algoritmo \ref{alg:classify}.

    Nosso classificador agrupa trechos em 11 classes distintas. Embora não
    haja necessidade de rotular as categorias, atribuímos a cada uma delas um
    nome de modo a facilitar a compreensão da classificação. As retas foram
    dividias em \emph{curta}, \emph{média} e \emph{longa}. As curvas, da mais
    fechada para a mais suave, são \emph{hairpin (grampo)}, \emph{cotovelo},
    \emph{acentuada}, \emph{moderada} e \emph{leve}, sendo que as três últimas
    categorias ainda foram subdivididas em \emph{curta} e \emph{longa}.

    \begin{algorithm}[h]
        \SetAlgoNoLine
        \SetKwProg{Fn}{}{}{fim}
        \SetKw{Devolva}{devolva}
        \SetKw{E}{e}
        \SetKw{Ou}{ou}
        \SetKw{Nao}{não}

        \SetCommentSty{textit}
        \SetKwComment{comment}{$\triangleright$ }{}

        \SetKwData{gets}{$\leftarrow$}

        \SetKwData{Angle}{ângulo}
        \SetKwData{radius}{raio}
        \SetKwData{length}{comprimento}
        \SetKwData{type}{tipo}
        \SetKwData{angle}{$\alpha$}
        \SetKwData{shortStraight}{reta curta}
        \SetKwData{mediumStraight}{reta média}
        \SetKwData{longStraight}{reta longa}
        \SetKwData{shortEasy}{curva leve curta}
        \SetKwData{shortMedium}{curva moderada curta}
        \SetKwData{shortHard}{curva acentuada curta}
        \SetKwData{longEasy}{curva leve longa}
        \SetKwData{longMedium}{curva moderada longa}
        \SetKwData{longHard}{curva acentuada longa}
        \SetKwData{hairpin}{curva \emph{hairpin} (grampo)}
        \SetKwData{elbow}{curva cotovelo}
        \SetKwData{undef}{$\varnothing$}

        \SetKwFunction{classify}{classificaSegmento}

        \Fn{\classify{\Angle, \radius, \length}}{
            \type \gets \undef\\
            \angle \gets $|\Angle|$\\

            \BlankLine
            \uSe{\radius $=$ $\infty$}{
                \comment{Retas.}
                \uSe{\length $\leq$ $55\ metros$}{
                    \type \gets \shortStraight\\
                }
                \uSenaoSe{\length $\leq$ $200\ metros$}{
                    \type \gets \mediumStraight\\
                }
                \Senao{
                    \type \gets \longStraight\\
                }
            }
            \Senao{
                \comment{Curvas.}
                \uSe(\hspace{1em} \comment*[h]{Curvas curtas.}){\length $\leq$ $91\ metros$}{
                    \uSe{\length $\leq$ $60\ metros$ \E \angle $\leq$ $45^\circ$}{
                        \type \gets \shortEasy\\
                    }
                    \uSenaoSe{\angle $\leq$ $80^\circ$}{
                        \type \gets \shortMedium\\
                    }
                    \Senao{
                        \type \gets \shortHard\\
                    }
                }
                \Senao(\hspace{1em} \comment*[h]{Curvas longas.}){
                    \uSe{\radius $\leq$ $112\ metros$ \E \angle $>$ $80^\circ$}{
                        \type \gets \longHard\\
                    }
                    \uSenaoSe{\radius $\leq$ $210\ metros$ \E \angle $>$ $50^\circ$}{
                        \type \gets \longMedium\\
                    }
                    \Senao{
                        \type \gets \longEasy\\
                    }
                }

                \BlankLine
                \comment{Se a curva for muito forte e curta, recebem
                         classificação especial.}
                \Se{\radius $\leq$ $57\ metros$}{
                    \uSe{\angle $\geq$ $110^\circ$}{
                        \type \gets \hairpin\\
                    }
                    \SenaoSe{\angle $\geq$ $50^\circ$}{
                        \type \gets \elbow\\
                    }
                }
            }
            \Devolva \type\\
        }

        \caption{Método de classificação.}
        \label{alg:classify}
    \end{algorithm}


% ---------------------------------------------------------------------------- %
\section{Resultados}
    Usando o \emph{TORCS} como ambiente de testes, realizamos experimentos nas
    $16$ pistas de asfalto que acompanham o simulador. Os resultados das etapas
    de segmentação e classificação foram satisfatórios, como exemplificado pela
    Figura \ref{fig:track_clusters}. Os resultados provenientes das demais
    pistas estão apresentados no \mbox{Apêndice \ref{app:segmentacao}}.

    \begin{figure}[h]
        \begin{subfigure}[!htb]{0.79\textwidth}
            \centering
            \includegraphics[width=\textwidth]{e-track-3_clusters}
            \caption{e-track-3.}
        \end{subfigure}
        \hspace{-12em}
        \begin{subfigure}[!htb]{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{g-track-1_clusters}
            \caption{g-track-1.}
        \end{subfigure}
        \vspace{2em}

        \begin{minipage}{0.55\textwidth}
            \begin{subfigure}[!htb]{0.76\textwidth}
                \centering
                \includegraphics[width=\textwidth]{a-speedway_clusters}
                \caption{a-speedway.}
            \end{subfigure}
            \vspace{2em}

            \begin{subfigure}[!htb]{\textwidth}
                \centering
                \includegraphics[width=\textwidth]{e-track-2_clusters}
                \caption{e-track-2.}
            \end{subfigure}
        \end{minipage}
        \hfill
        \begin{minipage}{0.49\textwidth}
            \centering
            \begin{subfigure}[!htb]{0.85\textwidth}
                \includegraphics[width=\textwidth]{legenda}
            \end{subfigure}
        \end{minipage}
        \caption{Exemplos de resultados do processo de coleta de dados,
                 segmentação e classificação.}
        \label{fig:track_clusters}
    \end{figure}
