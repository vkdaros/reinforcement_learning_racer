% ---------------------------------------------------------------------------- %
\chapter{Introdução}
\label{chap:prefacio}
% ---------------------------------------------------------------------------- %
    O desenvolvimento de tecnologias aplicadas a automóveis autônomos é uma área
    que tem despertado o interesse de pesquisadores tanto no meio acadêmico
    quanto na indústria. Em particular, pilotar carros de corrida é uma tarefa
    reconhecidamente difícil e pessoas especialistas nessa atividade precisam
    realizar complexas sequências de ações para levar o carro aos limites de seu
    desempenho. Esse cenário desafiador é um campo fértil para o emprego de
    métodos de controle baseados em Inteligência Artificial (IA).

    Entretanto, a construção de veículos é um processo demorado e oneroso,
    fazendo com que os procedimentos de testes tenham alto risco. Em paralelo a
    essa condição, muitos jogos de corrida modernos se mostram capazes de
    simular detalhadamente o mundo real e a dinâmica entre carros e pista.
    Essa conjunção de fatores apresenta a utilização de jogos eletrônicos para
    teste e desenvolvimento técnicas de aprendizado de máquina como um domínio
    promissor a ser explorado.

    Em meio aos jogos existentes atualmente, o \emph{TORCS}\footnote{
        \url{http://www.torcs.org}
    } (\emph{The Open Racing Car Simulator}) se mostra um ambiente virtual
    particularmente interessante para a pesquisa científica. Esse simulador
    preza pela implementação realista de conceitos físicos e sua arquitetura
    modular permite facilmente construir e integrar ao jogo controladores
    responsáveis pela condução dos carros. Por conta disso, o \emph{TORCS} tem
    sido amplamente adotado como plataforma de testes por trabalhos encontrados
    na literatura e por competições realizadas em conferências internacionais
    sobre IA.

    Dentre esses campeonatos, um dos mais renomados é o SCRC (\emph{Simulated
    Car Racing Championship}) \cite{loiacono}, onde os projetos submetidos
    competem entre si em séries de corridas no \emph{TORCS}. Os trabalhos dos
    competidores têm se baseado em métodos variados, tais como redes neurais
    artificiais, algoritmos genéticos e lógica \emph{fuzzy}.  Contudo, a
    quantidade de publicações envolvendo Aprendizagem por Reforço
    (\emph{Reinforcement Learning} ou apenas RL) é notoriamente limitada,
    valendo mencionar a implementação de um método simples voltado
    exclusivamente para ultrapassagens \cite{loiacono2}. Também existe a
    proposta de um aproximador de função valor específico para o contexto de
    carros de corrida autônomos \cite{abdullahi} e, embora a motivação desse
    artigo seja a aplicação em cenários complexos como os oferecidos pelo
    \emph{TORCS}, os autores se limitaram a usar um simulador muito mais simples
    de implementação própria.

    Consequentemente, não há relatos significativos sobre o emprego de RL no
    domínio de corridas simuladas. Algumas suposições a respeito da escassez de
    experimentos com pilotos virtuais baseados nesse importante ramo da
    aprendizagem de máquina já foram discutidas \cite{abdullahi}, tais como:
    \begin{itemize}
        \item Comparando-se com técnicas evolutivas, RL é mais sensível às
            muitas escolhas de parâmetros, tais como taxa de aprendizagem, fator
            de desconto e funções de recompensa;
        \item Falta de um modelo preditivo capaz de auxiliar o agente a estimar
            as mudanças de estado em função da tomada de ações;
        \item Necessidade de se considerar um grande número de características
            para a formulação do espaço de estados, possivelmente sendo algumas
            delas contínuas;
        \item A própria falta de resultados anteriores sobre o desempenho da
            abordagem nesse domínio.
    \end{itemize}

    Dessa forma, um dos objetivos desta dissertação é descrever todas as etapas
    do desenvolvimento de um controlador baseado em Aprendizagem por Reforço
    capaz de pilotar um carro virtual em um ambiente complexo que simula a
    realidade. O objetivo do controlador é correr em diferentes pistas gastando
    o menor tempo possível em cada volta. Nenhuma informação prévia é
    disponibilizada e o piloto virtual deve, por si só, descobrir o
    comportamento que melhor atende à sua meta. A avaliação de desempenho é
    feita pela comparação entre o tempo médio de volta e os resultados obtidos
    por competidores do SCRC quando expostos a condições de testes idênticas.

    Tipicamente, o comportamento de personagens não jogáveis (NPCs,
    \emph{non-player characters}) em jogos de corrida comerciais é projetado com
    o auxílio de especialistas de domínio \cite{lecchi}. As poucas exceções
    notáveis às abordagens manuais incluem o \emph{Colin McRace Rally}\footnote{
        \url{http://en.wikipedia.org/wiki/Colin_McRae_Rally}
    } (da empresa Codemasters), onde o comportamento dos NPCs foi definido pelo
    resultado do treinamento de redes neurais \cite{hannan}, e a série
    \emph{Forza Motorsport}\footnote{
        \url{http://en.wikipedia.org/wiki/Forza_Motorsport}
    } (da Microsoft), onde técnicas de aprendizado supervisionado foram
    exploradas para treinar a IA do jogo e computação evolutiva foi aplicada na
    otimização do traçado a ser seguido \cite{stern}. Assim, acreditamos que
    este trabalho também pode beneficiar a criação de NPCs em jogos comerciais
    ao reduzir a necessidade de peritos de domínio no acompanhamento do projeto.
%Nosso controlador enfrenta muitas restrições que os NPCs não teriam.
%Ou seja, seria mais fácil.

    Além disso, carros autônomos capazes de navegar por ruas e estradas sem a
    necessidade de motoristas humanos já são uma realidade \cite{googlecar}.
    Entretanto, projetar e efetuar testes com carros reais envolve alta demanda
    de tempo, pessoal e materiais, sendo portanto um processo de alto custo.
    Simuladores de robôs dessa categoria seriam então alternativas mais simples,
    rápidas e baratas para a condução de experimentos. Porém, frequentemente
    esses simuladores não são bons o suficiente a ponto de ser possível empregar
    com sucesso no mundo real os controladores treinados nos ambientes virtuais
    \cite{togelius}.
    
    Paralelamente a esse fato, alguns jogos atuais proveem implementações de
    física elaboradas o bastante para atender a essa necessidade, podendo ser o
    campo de provas ideal para o treinamento de robôs autônomos \cite{togelius}.
    Com isso, outro objetivo deste trabalho é contribuir com essa pluralidade de
    aplicações dos \emph{video games}, apresentando soluções que eventualmente
    possam ser exploradas também no domínio de robótica móvel.


% ---------------------------------------------------------------------------- %
\section{Visão geral do piloto virtual}
    A fim de transmitir ao leitor uma perspectiva global do controlador
    desenvolvido em nosso trabalho, apresentamos brevemente nesta seção as
    principais características do projeto, indicando os capítulos dedicados ao
    detalhamento dos respectivos tópicos. Posto isso, listamos a seguir os
    principais aspectos que guiaram nossa proposta de piloto virtual: 
    \begin{itemize}
        \item A plataforma de testes escolhida é o \emph{TORCS} e a
            implementação do controlador usa a interface padronizada do SCRC;
        \item O carro sempre corre sozinho na pista, disputando apenas pelo
            tempo de volta;
        \item Na primeira volta, o piloto deve coletar dados sobre a pista por
            meio dos sensores virtuais presentes no carro. Exclusivamente a
            partir dessas informações, cria-se então um modelo da pista, o qual
            é segmentado em trechos que recebem classificações individuais;
        \item O piloto deve aprender a controlar o acelerador e o freio por meio
            de técnicas de Aprendizado por Reforço de modo a ser capaz de
            adaptar-se a novas pistas;
        \item O manejo do volante é gerenciado em função do posicionamento do
            carro por meio de heurística;
        \item Os efeitos de desgaste de pneus, consumo de combustível e danos ao
            carro não são ativados durante as simulações.
    \end{itemize}

    As vantagens fundamentais que nos motivaram a adotar o ferramental de
    \emph{software} disponibilizado pela organização do SCRC para a comunicação
    com o \emph{TORCS} foram:
    (\emph{i}) a possibilidade de expor alguns competidores do campeonato aos
    mesmos testes enfrentados por nosso controlador, nos permitindo assim
    comparar resultados;
    (\emph{ii}) a interface disponível ao controlador para acessar as
    informações do ambiente e comandar o carro é análoga aos sensores e
    atuadores que um robô móvel poderia ter.
    %
    Em contrapartida, essa escolha também implica em algumas restrições, como
    por exemplo:
    (\emph{i}) cada iteração de processamento do controlador precisa ser rápida
    o suficiente para se manter sincronizada com a execução da simulação de
    física, obrigando o aprendizado a funcionar praticamente em tempo real;
    (\emph{ii}) as características do ambiente que não podem ser medidas pelos
    sensores virtuais também não podem ser usadas pelo controlador.

    Na próxima seção, mostramos alguns dos controladores submetidos ao SCRC, bem
    como outros trabalhos relacionados ao desenvolvimento de carros autônomos
    reais. Já a última seção deste capítulo é dedicada a apresentar as
    especificações do \emph{TORCS} e a interface de sensores e atuadores
    virtuais.

%\ref{chap:pid}
    Como já mencionado, ao longo da primeira volta na pista, nosso piloto
    virtual apenas coleta dados para reconhecer o circuito. Durante essa
    atividade, deseja-se manter a velocidade baixa e constante em
    aproximadamente $40km/h$. O posicionamento do carro, por sua vez, deve ser
    mantido ao centro da pista tanto quanto o possível. Assim, nessa tarefa, o
    volante, acelerador e freio são gerenciados por um mecanismo de controle do
    tipo PID (\textbf{P}roporcional-\textbf{I}ntegrativo-\textbf{D}erivativo),
    cujo funcionamento está explicado no Capítulo \ref{chap:pid}.

%\ref{chap:pista}
    Quando a volta de reconhecimento termina, os dados são processados e o
    piloto virtual então cria um modelo do formato da pista. Depois de sua
    construção, esse modelo é segmentado em trechos, os quais são classificados
    em \emph{reta longa}, \emph{curva leve}, \emph{curva acentuada}, entre
    outros. Descrevemos no Capítulo \ref{chap:pista} os detalhes do método que
    propomos para efetuar a segmentação e classificação da pista.

%\ref{chap:aprendizagem}
%\ref{chap:piloto}
    A partir da segunda volta, nosso controlador passa a experimentar várias
    ações distintas ao longo dos trechos da pista, empregando técnicas de
    Aprendizagem por Reforço na busca pelo comportamento mais veloz. A
    aprendizagem atua sobre o controle dos pedais do acelerador e freio, sendo
    que a fundamentação teórica dos conceitos de RL está apresentada no Capítulo
    \ref{chap:aprendizagem}. Já o Capítulo \ref{chap:piloto} aborda as
    adaptações necessárias à implementação do piloto virtual, bem como as
    particularidades da heurística que propomos para o manejo do volante usando
    uma série de controladores PID.
    %Apresentamos uma técnica inspirada no conceito de traços de elegibilidade,
    %mas é uma adaptação para tentar diferenciar no histórico completo de ações
    %aquelas que potencialmente são as responsáveis por eventos indesejados
    %(acidentes)

%\ref{chap:resultados}
    Após a exposição dos elementos conceituais e a descrição da construção do
    controlador, relatamos no Capítulo \ref{chap:resultados} os procedimentos
    aplicados para a avaliação de desempenho. Retratamos dois dos mais notáveis
    participantes do SCRC e comparamos seus resultados com os de nosso piloto.
    Ainda nesse capítulo, apontamos reflexões sobre algumas questões que se
    revelaram durante o processo de testes.

%\ref{chap:conclusao}
    Por fim, no Capítulo \ref{chap:conclusao} apresentamos nossas conclusões e
    últimas considerações sobre este projeto. Entendemos claramente que este
    texto apresenta apenas os primeiros passos dados em um amplo campo de
    pesquisa, onde há muito a ser explorado. Assim, encerramos a dissertação
    indicando algumas oportunidades para trabalhos futuros.

% JUSTIFICATIVA DE ESCOLHAS:
%* Tabular: é legível e fácil de entender as escolhas do agente.
%    A política pode ser mostrada para uma pessoa que queira aprender a pilotar.


% ---------------------------------------------------------------------------- %
\section{Trabalhos relacionados}
    Criar carros que não dependam de motoristas não é uma proposta recente.  A
    literatura sobre o tema é abundante e diversas publicações relatam casos de
    sucesso.  Embora exista uma polarização entre trabalhos relacionados a
    automóveis reais e aos simuladores, estudos em ambas frentes podem
    contribuir entre si. Por conta disso, apresentamos brevemente a seguir os
    projetos mais emblemáticos da história da condução autônoma de veículos.
    Posteriormente, expomos o levantamento de alguns trabalhos relacionados ao
    emprego de IA em simuladores de corrida.

    % ------------------------------------------------------------------------ %
    \subsection*{Ambientes reais}
        As primeiras pesquisas buscando transformar carros convencionais em
        veículos autônomos datam da década de $80$. A Universidade de Carnegie
        Mellon foi a pioneira nesse segmento, iniciando dois projetos em $1984$:
        \emph{Navlab1} \cite{jochem} e \emph{ALV} \cite{wallace, kanade}. O
        \emph{Navlab1} foi o primeiro de uma série de automóveis desenvolvidos
        na universidade e consistia em uma van adaptada carregando $5$
        prateleiras de computadores. Porém seu \emph{software} era limitado e o
        carro só se tornou completamente funcional ao final da década, quando
        conseguiu atingir sua velocidade máxima de $32km/h$ \cite{jochem}.

        A primeira versão do \emph{ALV} usava como único sensor apenas uma
        simples câmera de televisão em preto e branco \cite{wallace}. O trabalho
        se concentrou em produzir algoritmos para a detecção das faixas central
        e laterais da pista, além de um método de controle do volante baseado na
        imagem capturada. A velocidade era mantida constante e baixa. A evolução
        do projeto incorporou uma gama de sensores, tais como câmeras estéreo
        coloridas e sonares \cite{kanade}. O carro ainda se orientava pelas
        faixas na pista, mas também pela presença de calçadas. Além disso,
        obstáculos e pedestres eram monitorados e evitados.

        Outro precursor da área foi Ernst Dickmanns, da Universidade do
        Bundeswehr de Munique, quem desenvolveu um sistema computadorizado para
        controlar a direção, acelerador e freios de uma van da Mercedes-Benz
        também usando processamento de imagens \cite{dickmanns}. Em $1987$, o
        aparato foi capaz de percorrer aproximadamente $20km$, atingindo
        velocidade máxima de $96km/h$.  Mais tarde, em $1994$, os modelos
        aprimorados \emph{VaMP} e \emph{VITA-2}, preparados pelo \emph{EUREKA
        Prometheus Project (PROgraMme for a European Traffic of Highest
        Efficiency and Unprecedented Safety)} \cite{williams}, conseguiram andar
        mais de $1000km$ no trânsito normal em uma rodovia de Paris, França.

        Em $1995$, o \emph{VaMP} percorreu $1758km$ em uma viagem entre Munique
        e Odense, Dinamarca. Aproximadamente $95\%$ do trajeto foi cumprido de
        forma totalmente autônoma, incluindo trechos da Autobahn alemã, onde o
        carro alcançou velocidades superiores a $175km/h$ \cite{maurer}.

        A primeira grande competição entre projetos de veículos dessa categoria
        foi organizada em $2004$ pela \emph{Defense Advanced Research Projects
        Agency} dos Estados Unidos. O chamado \emph{DARPA Grand Challenge}
        rapidamente ganhou notoriedade devido ao prêmio de um milhão de dólares
        à equipe vencedora.

        No desafio de $2004$\footnote{
            \url{http://archive.darpa.mil/grandchallenge04/}
        }, os carros dirigidos por computador deveriam atravessar um trecho de
        aproximadamente $240km$ do Deserto de Mojave (Califórnia, Estados
        Unidos), porém nenhum dos competidores conseguiu realizar a façanha.  No
        ano seguinte, as especificações e objetivos do desafio não se alteraram
        e $5$ carros foram capazes de cumpri-lo\footnote{
            \url{http://archive.darpa.mil/grandchallenge05/}
        }. Já em $2007$\footnote{
            \url{http://archive.darpa.mil/grandchallenge/}
        }, o cenário passou a ser um percurso em um ambiente urbano controlado e
        os automóveis precisaram seguir as leis de trânsito além de percorrer o
        circuito no menor tempo possível. Das $11$ equipes na disputa, $6$
        completaram a tarefa com sucesso.

    % ------------------------------------------------------------------------ %
    \subsection*{Ambientes simulados}
        Jogos de corrida tem sido considerados um domínio de testes interessante
        para a construção e avaliação de técnicas de condução autônoma de
        veículos baseadas em IA \cite{autopia}. Vários autores tem usado o
        simulador \emph{TORCS} para elaborar controladores capazes de descobrir
        sozinhos como correr em pistas variadas.

        Um dos candidatos do SCRC bem sucedido nessa tarefa é denominado AUTOPIA
        \cite{onieva, autopia}. A arquitetura desse competidor é modular e
        empregou-se lógica \emph{fuzzy} em conjunto a algoritmos genéticos para
        determinar a velocidade mais apropriada em cada trecho da pista.  Outro
        controlador muito competitivo é o chamado Mr. Racer \cite{quadflieg2},
        fundamentado fortemente em planejamento. Usando exaustivamente todos os
        sensores de distâncias disponíveis, cria-se um modelo acurado da pista
        para o planejamento das ações. Em virtude do desempenho notável desses
        dois controladores, descrevemos mais detalhadamente seu funcionamento no
        Capítulo \ref{chap:resultados}, onde também comparamos seus resultados
        com os de nosso piloto virtual.

        Outro trabalho baseado em lógica \emph{fuzzy} mostra uma tentativa de
        representar o mundo do ambiente simulado por meio dessa técnica
        \cite{perez}. Os sensores e atuadores do carro são agrupados em
        conjuntos \emph{fuzzy} e as regras usadas em cada um dos conjuntos foram
        definidas manualmente. A evolução dos parâmetros é feita por um
        algoritmo genético com o objetivo de otimizar o tempo de volta, o dano
        sofrido e o tempo gasto fora da pista. O desempenho desse controlador
        foi menos expressivo e os autores mostram uma discussão sobre como a
        construção dos conjuntos \emph{fuzzy} limitou a capacidade de se
        acelerar e frear totalmente.

        Uma abordagem mais direta é adotada no projeto COBOSTAR \cite{butz},
        onde os dados provenientes dos sensores são mapeados para o controle dos
        atuadores sem a existência de representações internas. Pela técnica
        \emph{sensory-to-motor}, a informação sensorial é usada como entrada
        para uma função a qual devolve os sinais de comando aos atuadores. Os
        parâmetros dessa função são otimizados usando a estratégia evolutiva
        CMA (Adaptação de Matrizes de Covariância). Por essa característica
        imediatista, o controlador precisa ser treinado em várias pistas
        distintas a fim de determinar parâmetros genéricos para a função de
        mapeamento e, com isso, evitar o sobre-ajuste. Em razão dessa
        característica, o desempenho do carro se limita a ser mediano na maioria
        dos circuitos.

        Entretanto, a competitividade e a busca pela maior eficiência não são os
        únicos fatores motivando pesquisas nessa área. Existem também
        estudos com o objetivo de criar NPCs que se comportem de forma
        semelhante aos jogadores humanos guiando seus carros \cite{munoz2,
        munoz1}. Para isso, uma série de redes neurais artificiais é treinada
        com dados recebidos de partidas de jogadores humanos. A partir desse
        treinamento, as redes neurais respondem às diferentes situações de uma
        corrida fazendo previsões de como seriam as escolhas de um jogador em
        relação ao traçado e à velocidade a serem seguidos.

        Também existem estudos sobre formalizações das capacidades e limitações
        relacionadas a condução de carros em pistas de corrida \cite{braghin}. O
        modelo matemático proposto é abrangente e aborda questões como o cálculo
        das velocidades máximas em cada ponto da pista, a busca pelo melhor
        traçado e até mesmo um método para estimar o tempo mínimo necessário
        para se completar uma volta. Apesar de muito poderosa, essa abordagem
        exige informações muito detalhadas para a construção do modelo, tais
        como as diversas relações dinâmicas de interação entre o carro e o
        ambiente. Assim, há poucos trabalhos aproveitando esse formalismo.

        Tentando aplicar algumas das conclusões do modelo citado acima, porém
        reduzindo a quantidade de informações prévias exigidas, vale destacar
        uma proposta de método evolutivo para encontrar o traçado ideal a ser
        seguido \cite{cardamone1}. Dada apenas a geometria precisa de uma pista,
        um algoritmo genético usado em conjunto de simulações no \emph{TORCS} é
        capaz de determinar a melhor ponderação entre o caminho de menor
        curvatura e o caminho mais curto em um circuito.

% ---------------------------------------------------------------------------- %
\section{\emph{TORCS}}
    O ambiente de simulação escolhido como plataforma de testes para este
    trabalho é o \emph{TORCS (The Open Racing Car Simulator)}, um simulador de
    corridas automobilísticas de código aberto (licença GPL) desenvolvido com o
    intuito de permitir aos usuários a criação de módulos capazes de controlar o
    comportamento dos carros virtuais. O emprego do \emph{TORCS} em projetos
    acadêmicos apresenta uma série de vantagens em comparação a outros
    simuladores \cite{autopia}. Algumas delas são:

    \begin{itemize}
        \item Ele possui qualidades de um simulador avançado, semelhante a
            jogos comerciais recentes, porém possibilita grande variedade de
            personalizações;

        \item Há um sofisticado mecanismo de física, no qual a aderência dos
            pneus à pista, efeitos aerodinâmicos, consumo de combustível, dano
            sofrido e diversos outros fatores influenciam o comportamento e
            desempenho do carro;

        \item Há representação gráfica em 3D da corrida, permitindo o
            acompanhamento visual da simulação;

        \item A arquitetura do \emph{software} é altamente modularizada,
            facilitando a tarefa de implementar e integrar controladores para o
            jogo.
    \end{itemize}

    Embora o \emph{TORCS} não tenha a mesma qualidade visual com gráficos de
    última geração dos jogos comerciais, o visualizador 3D permite acompanhar o
    comportamento do controlador na pista por de vários ângulos diferentes, como
    exemplificado na Figura \ref{fig:screenshot}. Ademais, também é possível
    usar apenas o simulador, sem o componente gráfico, acelerando a execução de
    testes. Essa característica é fundamental para a realização de baterias de
    experimentos em várias pistas com centenas de voltas em cada.

    \begin{figure}[!htb]
        \centering
        \begin{subfigure}[b]{\textwidth}
            \centering
            \includegraphics[width=0.6\textwidth]{images/torcs_1}
        \end{subfigure}
        \vspace{-0.5em}

        \begin{subfigure}[b]{\textwidth}
            \centering
            \includegraphics[width=0.6\textwidth]{images/torcs_2}
        \end{subfigure}
        \caption{Imagens de uma corrida no \emph{TORCS}.}
        \label{fig:screenshot}
    \end{figure}

    Por padrão, o \emph{TORCS} permite aos controladores acessar uma enorme
    quantidade de informações da simulação, pois elas são compartilhadas entre
    todos os módulos acoplados ao jogo. Assim, qualquer piloto virtual poderia
    analisar diretamente as estruturas de dados da pista, por exemplo. Além
    disso, a atualização da simulação de física fica bloqueada enquanto os
    controladores não respondem quais ações farão, podendo impedir o sistema de
    trabalhar em tempo real.

    Essas duas questões afastam a condição de jogo do cenário que um robô móvel
    enfrentaria no mundo real, onde as informações disponíveis ao robô são
    restritas e eventos acontecem em seu próprio ritmo. Ademais, controladores
    com longas atividades de processamento a cada iteração poderiam tornar as
    partidas de disputa direta inviavelmente prolongadas.

    A fim de lidar com essas adversidades e tornar o \emph{TORCS} uma ferramenta
    funcional e justa para competições, os autores do SCRC \emph{(Simulated Car
    Racing Championship)} desenvolveram uma adaptação para o jogo seguindo o
    modelo cliente-servidor \cite{scrc2012}. O servidor está acoplado
    diretamente ao \emph{TORCS} e os controladores devem implementar a interface
    de cliente. A comunicação é feita por troca de mensagens em intervalos
    regulares de $20ms$, dos quais apenas $10ms$ são reservados para os
    controladores efetuarem seus processamentos.

    Com isso, o controlador não tem acesso indiscriminado às informações da
    partida e a observação do ambiente é feita por meio de sensores virtuais
    presentes no carro. Esses sensores são capazes de realizar medições
    variadas, sendo atualizados pelo servidor de acordo com o estado do ambiente
    a cada passo da simulação. Na Figura \ref{fig:sensors} mostramos uma
    representação visual de alguns desses sensores e, na Tabela
    \ref{tab:sensors}, apresentamos sua listagem completa, juntamente com
    descrições individuais.

    Depois da atualização dos sensores, o controlador possui um intervalo de
    $10ms$ para determinar qual será sua ação e ajustar os parâmetros passados
    aos atuadores do carro. Encerrado o período de processamento, o servidor
    consulta o valor atribuído a cada atuador e retransmite ao \emph{TORCS} os
    comandos associados. Caso o cliente não termine seus cálculos há tempo, o
    servidor dá continuidade à simulação usando a última ação definida. Na
    Tabela \ref{tab:effectors} há a descrição dos atuadores disponíveis.

    O campeonato SCRC tem sido realizado em conferências internacionais, tais
    como EVO* \cite{evo2012}, ACM GECCO \cite{gecco2012} e IEEE CIG
    \cite{cig2012}. Em cada etapa, são feitas baterias de experimentos compostas
    por três estágios:
    (\emph{i}) aquecimento;
    (\emph{ii}) qualificação; e
    (\emph{iii}) corridas.
    Durante o aquecimento, cada piloto virtual é colocado sozinho na pista para
    explorar o circuito a fim de coletar informações que poderão ser usadas nos
    próximos estágios. Em seguida, os pilotos, ainda sozinhos, correm contra o
    relógio com o objetivo de percorrer a maior distância dentro do limite de
    tempo. Por fim, os melhores qualificados disputam corridas juntos, onde o
    vencedor é o primeiro que conseguir cruzar a linha de chegada. Ao término de
    cada corrida, os controladores recebem pontos de acordo com sua
    classificação final, seguindo os moldes da Fórmula $1$. Aquele que acumular
    mais pontos ao encerramento da última conferência ganha o campeonato.

    Vale salientar que as pistas usadas em todas as corridas do torneio não são
    distribuídas com o \emph{TORCS}. Ou seja, o primeiro contato entre os
    pilotos virtuais e os circuitos da competição acontece no aquecimento. Com
    isso, fica evidente que os desenvolvedores não têm possibilidade de ajustar
    previamente parâmetros específicos para as pistas do campeonato e, portanto,
    os controladores precisam ter a capacidade de se adaptar a circuitos
    desconhecidos.

    %Nós estamos usando o programa deles, mas não temos carro para competir.

    \begin{table}
        \caption{Sensores disponíveis}
        \label{tab:sensors}
        %\small
        \newcolumntype{R}{>{\raggedleft\arraybackslash}X}%
        \newcolumntype{C}{>{\centering\arraybackslash}X}%
        \begin{tabularx}{\textwidth}{rX}
            \toprule
            \textbf{Nome} & \textbf{Descrição} \\
            \midrule
            \emph{angle}         & Ângulo entre a direção do carro e a direção do eixo
                            da pista. Valores negativos indicam que o carro está
                            apontando para a direita do eixo da pista e valores
                            positivos para a esquerda.\\
            \addlinespace
            \emph{curLapTime}    & Tempo decorrido desde o início da volta atual.
                            \\
            \addlinespace
            \emph{damage}        & Quantidade de dano sofrido pelo carro. \\
            \addlinespace
            \emph{distFromStart} & Distância entre o carro e a linha de largada ao
                            longo do eixo da pista. \\
            \addlinespace
            \emph{distRaced}     & Distância percorrida pelo carro desde o início da
                            corrida. \\
            \addlinespace
            \emph{fuel}          & Indica o nível de combustível. \\
            \addlinespace
            \emph{gear}          & Indica qual marcha está engatada. \\
            \addlinespace
            \emph{lastLapTime}   & Tempo gasto para completar a volta anterior. \\
            \addlinespace
            \emph{opponents}     & Vetor de $36$ sensores que detecta a distância (de
                            $0$ a $100$ metros) até um oponente. Cada sensor cobre
                            um setor de $10^\circ$, de $-\pi$ a $\pi$, em torno
                            do carro.\\
            \addlinespace
            \emph{racePos}       & Posição na corrida com relação aos outros carros.\\
            \addlinespace
            \emph{rpm}           & Número de rotações por minuto do motor. \\
            \addlinespace
            \emph{speedX}        & Velocidade do carro ao longo do eixo longitudinal do
                            carro. \\
            \addlinespace
            \emph{speedY}        & Velocidade do carro ao longo do eixo transversal ao
                            carro. \\
            \addlinespace
            \emph{track}         & Vetor de $19$ sensores de proximidade posicionados na
                            frente: cada sensor indica a distância entre a
                            frente do carro e a extremidade da pista. Cada
                            sensor tem inclinação própria, indo de $-\pi/2$ a
                            $+\pi/2$, definida na configuração inicial do carro.
                            A distância é dada em metros e o alcance dos
                            sensores é de $200$ metros. Quando o carro está fora
                            da pista (ou seja, quando trackPos é menor que $-1$ ou
                            maior que $1$), esses sensores não são confiáveis.\\
            \addlinespace
            \emph{trackPos}      & Distância entre o carro e o eixo da pista. O valor
                            é normalizado com respeito à largura da pista: $0$
                            quando o carro está sobre o eixo, $-1$ quando estiver
                            sobre a borda direita da pista e $1$ quando estiver
                            sobre a borda esquerda. Valores menores que $-1$ e
                            maiores que $1$ indicam que o carro está fora da
                            pista. \\
            \addlinespace
            \emph{wheelSpinVel}  & Vetor de $4$ sensores representando a rotação de cada
                            roda. \\
            \bottomrule
        \end{tabularx}
    \end{table}
    \begin{table}
        \caption{Atuadores disponíveis}
        \label{tab:effectors}
        \begin{tabularx}{\textwidth}{rX}
            \toprule
            \textbf{Nome} & \textbf{Descrição} \\
            \midrule
            \emph{accel}    & Pedal virtual do acelerador ($0$ significa sem aceleração
                       e $1$ é aceleração total)\\
            \addlinespace
            \emph{brake}    & Pedal virtual do freio ($0$ significa sem freio e $1$ é
                       frenagem total).\\
            \addlinespace
            \emph{gear}     & Engata a marcha especificada: $-1$ para ré, $0$ para neutro
                       (ponto morto) e de $1$ a $6$ para as demais. \\
            \addlinespace
            \emph{steer} & Valor de $-1$ a $+1$ indicando o quanto o volante está
                       virado (completamente para esquerda e direita
                       respectivamente). Inclinação total corresponde a
                       $0,785398$ radianos.\\
            \addlinespace
            \emph{meta}     & Para uso de controle da corrida: $0$ não faz nada e $1$
                       solicita ao servidor o reinício da corrida. \\
            \bottomrule
        \end{tabularx}
    \end{table}

    \begin{figure}[]
        \centering
        \begin{subfigure}[t]{0.85\textwidth}
            \centering
            \includegraphics[width=\textwidth]{track_angle_sensors}
            \caption{Representação do sensor de inclinação em relação ao eixo da
                     pista (\emph{angle}) e dos sensores de distância até as
                     bordas da pista (\emph{track}).}
            \label{fig:angle_sensor}
        \end{subfigure}
        \vspace{4em}

        \begin{subfigure}[t]{0.85\textwidth}
            \centering
            \includegraphics[width=\textwidth]{trackPos_sensor}
            \caption{Representação do sensor de distância em relação ao eixo
                     da pista (\emph{trackPos}).}
            \label{fig:trackpos_sensor}
        \end{subfigure}
        \caption{Alguns dos sensores mais usados pelos controladores. A lista
                 completa de todos os sensores disponíveis, bem como seus
                 detalhes, encontra-se na Tabela \ref{tab:sensors}.}
        \label{fig:sensors}
    \end{figure}
