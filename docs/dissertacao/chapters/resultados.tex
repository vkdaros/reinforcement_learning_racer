% ---------------------------------------------------------------------------- %
\chapter{Experimentos e análises}
\label{chap:resultados}
% ---------------------------------------------------------------------------- %
    Neste capítulo, expomos os
    resultados dos testes práticos realizados com o agente construído. Na
    primeira seção, descrevemos o ambiente e as pistas usados nos experimentos.
    Na segunda seção, retratamos os experimentos para determinar os valores
    adequados para os parâmetros de aprendizagem. Na seção seguinte, damos
    detalhes de dois controladores concorrentes também desenvolvidos para o
    \emph{TORCS} e comparamos seu desempenho com os resultados de nosso piloto
    virtual. Por fim, na quarta seção apresentamos uma reflexão sobre as
    características de nosso agente.


% ---------------------------------------------------------------------------- %
\section{Ambiente de testes}
    No Capítulo \ref{chap:prefacio}, listamos algumas características do
    simulador de corridas \emph{TORCS} que são interessantes para propósitos
    acadêmicos. Por conta dessas qualidades, o jogo tem sido amplamente usado
    como plataforma de testes para algoritmos de inteligência artificial
    \cite{autopia} e, em decorrência disso, torneios têm sido realizados várias
    conferências internacionais, tais como
    WCCI\footnote{\url{http://www2.mae.cuhk.edu.hk/wcci2008}},
    CEC\footnote{\url{http://www.cec-2009.org}},
    EVO*\footnote{\url{http://www.evostar.dei.uc.pt}},
    GECCO\footnote{\url{http://www.sigevo.org/gecco-2012/competitions.html\#scrc}} e
    CIG\footnote{\url{http://geneura.ugr.es/cig2012/competitions.html}}.

    As competições permitem aos pesquisadores divulgar seus trabalhos e por à
    prova os controladores desenvolvidos. O primeiro campeonato acadêmico de
    corridas de carros simuladas usando o \emph{TORCS} aconteceu em 2008 no
    \emph{IEEE World Congress on Computational Intelligence} (WCCI) \cite{wcci}.
    No ano seguinte, o torneio recebeu o nome \emph{Simulated Car Racing
    Championship} (SCRC) \cite{loiacono}, o qual passou a ser usado nas edições
    posteriores. Nesse novo formato, os organizadores proveem uma interface
    padrão entre o jogo e os controladores definida por sensores e atuadores
    (apresentados nas Tabelas \ref{tab:sensors} e \ref{tab:effectors}) fazendo
    analogia aos dispositivos encontrados em carros autônomos do mundo real.
    %Essa padronização permite a realização de comparações e avaliações mais
    %objetivas.

    Trabalhos com abordagens variadas são submetidos às competições, indo de
    algoritmos de controle baseados puramente em heurísticas até a agentes com
    sistemas de IA muito elaborados. A fim de comparar o desempenho de nosso
    piloto virtual com os controladores desenvolvidos por outros
    pesquisadores, nosso projeto foi construído com base no \emph{software}
    disponibilizado para o SCRC \cite{scrc2012}.

    As pistas usadas nos experimentos estão mostradas na Figura
    \ref{fig:test_tracks}. Todas acompanham a instalação padrão do \emph{TORCS}
    e são do grupo de circuitos asfaltados, com características similares às de
    autódromos reais. O modelo de carro usado foi \emph{car1-trb1}, com $4,52m$ de
    comprimento, $1,9m$ de largura, massa de $1150kg$ (além de $94kg$ de
    combustível), motor com potência de $405kW$ (aproximadamente $543hp$) e
    tração traseira.

    \begin{figure}[!htb]
        \centering
        \begin{subfigure}[!htb]{0.16\textwidth}
            \includegraphics[width=\textwidth]{alpine-2}
            \caption{alpine-2}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.27\textwidth}
            \includegraphics[width=\textwidth, height=7em]{e-track-2}
            \caption{e-track-2}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.23\textwidth}
            \includegraphics[width=\textwidth]{e-track-3}
            \caption{e-track-3}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.23\textwidth}
            \includegraphics[width=\textwidth]{e-track-4}
            \caption{e-track-4}
        \end{subfigure}
        \vspace{3em}

        \begin{subfigure}[!htb]{0.23\textwidth}
            \includegraphics[width=\textwidth]{e-track-6}
            \caption{e-track-6}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.23\textwidth}
            \includegraphics[width=\textwidth, height=9em]{g-track-1}
            \caption{g-track-1}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.23\textwidth}
            \includegraphics[width=\textwidth]{g-track-2}
            \caption{g-track-2}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.17\textwidth}
            \includegraphics[width=\textwidth]{g-track-3}
            \caption{g-track-3}
        \end{subfigure}
        \vspace{3em}

        \begin{subfigure}[!htb]{0.3\textwidth}
            \includegraphics[width=\textwidth]{street-1}
            \caption{street-1}
        \end{subfigure}
        \hspace{1em}
        \begin{subfigure}[!htb]{0.25\textwidth}
            \includegraphics[width=\textwidth]{wheel-1}
            \caption{wheel-1}
        \end{subfigure}

        \caption{Silhuetas das pistas usadas nos experimentos.}
        \label{fig:test_tracks}
    \end{figure}

    Todos os testes foram executados em um computador com sistema operacional
    Linux, processador com dois núcleos de $2,27GHz$ e $4GB$ de RAM.
    Além disso, o simulador recebeu dois parâmetros modificadores:
    \textfunc{-nofuel} e \textfunc{-nodamage}. A primeira opção desativa o
    consumo de combustível, fazendo com que o tanque do carro permaneça sempre
    cheio. A segunda opção desliga a simulação de danos e avarias causados por
    batidas, bem como o desgaste dos pneus. A motivação para tais escolhas foram
    principalmente:

    \begin{itemize}
        \item Possibilitar que o carro permaneça correndo na pista por um número
              arbitrariamente grande de voltas sem a necessidade de entrar nos
              boxes para reabastecer;

        \item Manter o desempenho do carro constante durante o processo de
              aprendizagem de modo que as mudanças de comportamento sejam o
              único elemento variável influenciando as tomadas de tempo em cada
              volta.
    \end{itemize}

    %Além disso, não usei a opção de ruídos.
    Com essa estruturação, as baterias de testes consistem em deixar o agente
    correr por $300$ voltas em cada uma das dez pistas citadas. Para esse
    procedimento, desativamos a interface visual do \emph{TORCS} a fim de
    reduzir o tempo dos experimentos. Entretanto, ainda assim a execução de cada
    sequência de testes consumiu aproximadamente $4$ horas.


% ---------------------------------------------------------------------------- %
\section{Experimentos}
% Aumentar o alpha para 0,12 melhorou só e-track-4, g-track-3. Resto pior.
% Diminuir o alpha para 0,01 melhorou só e-track-4, g-track-3. Resto pior.
% Aumentar o gamma para 0,99 piorou tudo.
% Aumentar o gamma para 0,95 piorou tudo, menos e-track-4.
% Diminuir o alpha para 0,8 melhorou só e-track-4, g-track-3. Resto pior.
% Diminuir lambda para 0,3 estragou conv de alpine2, street1 e wheel1,
%     melhorou: e-track-3 (1s), e-track-4 (7), g-track-3 (1,5s)
% Aumentar lambda para 0,5 piorou tudo.
% Aumentar lambda para 0,6 estragou conv de e-track-3, street-1 e wheel-1
%     melhorou: e-track-4 (5s), g-track-2 (1s), g-track-3 (4.5s)
    No Capítulo \ref{chap:piloto}, descrevemos o cerne do processo de
    aprendizagem de nosso piloto virtual, onde é possível observar a presença
    dos parâmetros $\alpha$, $\gamma$ e $\lambda$. Esses componentes são usados
    para ajustar as regras de atualização das estimativas de recompensa total
    esperada por cada ação na matriz $Q$. Abaixo listamos a interpretação dada
    a esses termos.

    \begin{description}[labelindent=\parindent, leftmargin=2.85\parindent,%
                        labelwidth=2.3em]
        \item[$\alpha$ :] Representa a \textbf{taxa de aprendizado}. Quanto
            maior for essa taxa, mais enfase se dá às experiências recém
            adquiridas. Para valores pequenos, as recompensas recentes pouco
            alteram as estimativas em $Q$. Quando $\alpha = 0$, o aprendizado
            se interrompe;

        \item[$\gamma$ :] É o \textbf{fator de desconto}. Ele indica a
            atenuação que o valor de recompensas futuras recebe por ser
            avaliado no tempo presente;

        \item[$\lambda$ :] É o fator de \textbf{decaimento do traço}. Quanto
            mais distante no histórico estiver uma ação, menor será a fração
            $\lambda$ que indica seu impacto no presente.
    \end{description}

    Nota-se claramente que $\lambda$ é um componente variável em relação ao quão
    longe se retrocede no traço de ações. O termo constante é na verdade o valor
    de inicialização e decaimento \textvar{LAMBDA}, presente no Algoritmo
    \ref{alg:applyBadTrace}. Por simplicidade de notação, nesta seção usaremos o
    símbolo $\lambda$ quando estivermos nos referindo à constante
    \textvar{LAMBDA}.

    A fim de determinar os valores para esses parâmetros, investigamos uma série
    de combinações e analisamos as alterações no comportamento do agente. As
    avaliações se deram principalmente pela leitura dos registros de tempo de
    volta e recompensa acumulada por volta. Na Figura \ref{fig:e-track-2_times}
    apresentamos um exemplo de gráfico composto gerado a partir da junção desses
    registros. A sequência superior de pontos indica, em segundos, o tempo gasto
    para completar cada volta durante a aprendizagem. Os pontos da camada
    inferior representam as somas das recompensas que alteraram as estimativas
    em $Q$, também a cada volta.

    Vale salientar que, por conta de nossa estratégia das recompensas terem um
    caráter quase sempre punitivo, os pontos desse gráfico tentem a ser
    predominantemente negativos. Assim, quando o aprendizado se estabiliza, as
    recompensas por volta completada tendem a ser nulas, ou muito próximas a
    zero. Em casos como esse, dizemos que o aprendizado convergiu e a política
    resultante é simplesmente a ação de maior valor esperado em cada estado na
    matriz $Q$ ao final do teste.

    \begin{figure}[!htb]                                                        
        \centering                                                              
        \includegraphics[width=\textwidth]{e-track-2_rewards_laptime}           
        \caption{Tempo e recompensa acumulada de cada volta na pista e-track-2.}
        \label{fig:e-track-2_times}
    \end{figure} 

    Tendo definido uma ferramenta prática para a análise das seções de
    aprendizado do piloto virtual, conduzimos experimentos com diversas
    combinações de valores para $\alpha$, $\gamma$ e $\lambda$. O objetivo foi
    determinar qual o conjunto de valores mais adequado ao nosso agente. Os
    critérios para a avaliação dos resultados em cada pista foram:

    \begin{itemize}
        \item O aprendizado deve convergir em todas as pistas da bateria de
              testes. Isto é, ao final de cada experimento, o piloto virtual
              deve estar dirigindo sem sair da pista e sem grande variação nos 
              tempos das últimas voltas;

        \item O desempenho do piloto nas últimas voltas deve ser próximo ao de
              sua melhor volta. Ou seja, o tempo das últimas voltas deve estar
              estável e próximo ao de sua volta mais rápida;
    \end{itemize}

    Pela investigação dos registros de cada seção de testes, observamos que a
    variação de valores dos parâmetros apresenta pouca influência no desempenho
    geral do agente. Ou seja, mesmo com diferentes valores em cada termo, não
    constatamos diferenças significativas no tempo médio das últimas voltas
    quando o aprendizado convergia. Apesar disso, percebemos que a convergência
    de nosso método é sensível a mudanças em $\lambda$.

    Um caso no qual o aprendizado não conseguiu convergir pode ser visto na
    Figura \ref{fig:wheel-1_bad}. Os parâmetros desse teste foram
    $\alpha = 0,1$, $\gamma = 0,9$ e $\lambda = 0,3$. Acreditamos que a causa
    desse fenômeno esteja relacionada ao valor de $\lambda$ ser inferior ao
    ideal. Com isso, o mecanismo de recompensas aplica punições muito atenuadas
    às ações relacionadas ao carro parar ou sair da pista. Assim, há pouca
    distinção entre as atitudes que levam a acidentes e aquelas que são punidas
    simplesmente por não contribuírem para a diminuição no tempo de volta.
    Consequentemente, o piloto virtual acaba insistentemente escolhendo ações
    problemáticas.

    \begin{figure}[!htb]                                                        
        \centering                                                              
        \includegraphics[width=\textwidth]{wheel-1_rewards_laptime_bad}           
        \caption{Resultado do teste com $\alpha = 0,1$, $\gamma = 0,9$ e
                 $\lambda = 0,3$ na pista wheel-1, no qual o aprendizado não
                 convergiu a uma política estável.}
        \label{fig:wheel-1_bad}
    \end{figure} 

    Quando $\lambda$ é configurado com valores maiores que o ideal, observamos
    efeito semelhante de falta de convergência, porém a causa é o inverso do
    caso anterior. Os acidentes estão associados a várias ações, entretanto nem
    todas elas são diretamente responsáveis pelo ocorrido. Dessa forma, altos
    valores de $\lambda$ fazem com que a punição decaia pouco à medida que é
    aplicada às ações ao longo do histórico. Assim, novamente tem-se um cenário
    onde muitas ações são erroneamente avaliadas como igualmente ruins.

    Como fruto dessas verificações, concluímos que $\lambda = 0,4$ é a
    configuração que torna o aprendizado mais consistente em todas as pistas
    testadas. Embora impacto das variações nos termos $\alpha$ e $\gamma$ tenha
    se mostrado muito menos expressivo, os valores que resultaram em tempos
    médios de volta marginalmente inferiores foram: $\alpha = 0,1$ e
    $\gamma = 0,9$.

    Por conta do grande volume de registros de tempo e recompensas proveniente
    da bateria de testes, julgamos não ser interessante sobrecarregar o leitor
    com toda a massa de dados adquiridos. Porém, apresentamos no Apêndice
    \ref{app:laptimes} todos os gráficos obtidos pelos experimentos realizados
    com a combinação final de $\alpha = 0,1$, $\gamma = 0,9$ e $\lambda = 0,4$
    em cada uma das dez pistas da Figura \ref{fig:test_tracks}.


% ---------------------------------------------------------------------------- %
\section{Pilotos concorrentes}
    Uma das metas deste trabalho é fazer nosso piloto virtual completar voltas o
    mais rápido possível. Mas quão baixo precisa ser o tempo de uma volta
    para ela ser considerada ``rápida''? Uma forma de responder a essa questão é
    convocar especialistas e coletar dados sobre seu desempenho no simulador.
    Entretanto existem desvantagens inerentes a essa solução, dentre elas:

    \begin{itemize}
        \item A dificuldade em se encontrar pessoas que sejam especialistas em
              pilotar os carros do \emph{TORCS}, um jogo sem o grande público
              dos títulos comerciais;
        \item Os especialistas não estariam sujeitos às mesmas restrições que o
              piloto virtual, como ``enxergar'' a pista apenas pelas informações
              dadas pelos sensores;
        \item Se as limitações do piloto virtual fossem artificialmente impostas
              aos especialistas, a situação de uso do simulador seria tão
              diferente do convencional que provavelmente o desempenho das
              pessoas seria muito inferior ao esperado de um perito.
    \end{itemize}

    Uma alternativa é avaliar outros agentes construídos especificamente para o
    \emph{TORCS}, tais como os submetidos para o SCRC. Essa opção contorna as
    dificuldades listadas acima e foi a escolha tomada para esse projeto. Dentre
    os concorrentes enviados ao SCRC, dois se destacaram por seu
    desempenho formidável: AUTOPIA e Mr. Racer. Por essa razão, nós sujeitamos
    esses competidores aos mesmos testes realizados com nosso piloto virtual e
    usamos seus resultados como base de comparação. A seguir, apresentamos mais
    detalhes desses dois controladores.
    \vspace{1em}

    % ------------------------------------------------------------------------ %
    \subsection{AUTOPIA}
        A primeira participação do controlador que posteriormente
        recebeu o nome AUTOPIA deu-se no ano de 2009 \cite{loiacono}. A
        proposta do trabalho é apresentar uma arquitetura modular onde cada um
        dos módulos é independente e se encarrega de gerenciar um tipo básico de
        ação necessária para guiar o carro \cite{onieva}. Os componentes da
        arquitetura são:
        \begin{description}
            \item[Controle de marchas:]
                Responsável por monitorar o número de rotações por minuto (RPM)
                do motor e efetuar a troca de marchas. As trocas são feitas com
                base em um mapa de valores máximos e mínimos de RPM, de forma
                semelhante ao método usado em nosso piloto virtual, como
                apresentado no Capítulo \ref{chap:piloto};

            \item[Velocidade alvo:]
                Este módulo calcula a velocidade desejada em cada trecho da
                pista usando um sistema de regras baseadas em lógica
                \emph{fuzzy};

            \item[Acelerador e freio:]
                Os pedais do acelerador e freio são acionados em função da
                diferença entre a velocidade alvo e a velocidade instantânea do
                carro.

            \item[Volante:]
                O módulo que guia a direção do carro identifica três situações
                distintas:
                \emph{(i)} andando com marcha ré;
                \emph{(ii)} carro do lado de fora da pista; e
                \emph{(iii)} carro estando dentro dos limites da pista.
                No caso mais frequente, item \emph{(iii)}, o volante é regido
                puramente por uma ponderação entre as leituras das distâncias
                provenientes dos sensores \emph{track};

            \item[Gestão de oponentes:]
                O último componente da arquitetura atua como modificador dos
                controles de acelerador, freio e volante de modo a evitar
                colisões e efetuar ultrapassagens.
        \end{description}

        Além disso, esse controlador também conta com um sistema para se adaptar
        às particularidades de cada pista: os locais de ocorrência de eventuais
        acidentes são registrados e tem-se mais cautela ao passar por esses
        pontos através de uma redução arbitrária da velocidade alvo. Esses
        fatores em conjunto resultaram na classificação final em segundo lugar
        na competição de 2009. O projeto então passou a ser continuamente
        aprimorado com o emprego de algoritmos genéticos para otimizar
        simultaneamente os parâmetros de controle do volante e velocidades alvo
        \cite{autopia}. No ano de 2010, AUTOPIA conquistou o primeiro lugar na
        disputa e passou a ser considerado o estado da arte, deixando de
        concorrer no torneio mas ainda sim participando a título de servir como
        parâmetro de comparação.

    % ------------------------------------------------------------------------ %
    \subsection{Mr. Racer}
        Outro projeto de destaque é o controlador batizado de Mr. Racer, o qual
        foi apresentado pela primeira vez em um trabalho focado no uso de
        técnicas de planejamento para pilotar carros no simulador \emph{TORCS}
        \cite{quadflieg1}. Um requisito forte da abordagem escolhida é ter
        um modelo acurado da pista. Para atender tal demanda, usa-se
        intensamente todos os $19$ sensores \emph{track} disponíveis de modo a
        definir uma série de vetores que descrevem as bordas da pista. Esse
        modelo é convertido em uma representação abstrata dos trechos da pista.
        Cada trecho é classificado de acordo com o parâmetro de curvatura,
        sendo \mbox{rotulados por:}
        \begin{itemize}[noitemsep]
            \item Reta;
            \item Reta próxima a uma curva;
            \item Curva de alta velocidade;
            \item Curva de média velocidade;
            \item Curva de baixa velocidade;
            \item Curva do tipo grampo(\emph{hairpin}).
        \end{itemize}

        O volante é controlada pela heurística ingênua de seguir a direção do
        sensor \emph{track} que devolve o maior valor. O controle do acelerador
        e freio segue uma função linear para cada um dos seis cenários listados
        acima. Os parâmetros dessas funções são determinados por uma estratégia
        evolutiva que precisa ser processada previamente à corrida.

        Em sua primeira participação no SCRC, Mr. Racer conquistou a terceira
        posição da disputa de 2010, competindo diretamente contra AUTOPIA. Nos
        três anos seguintes, o controlador AUTOPIA participou apenas como
        elemento comparativo e Mr. Racer recebeu aperfeiçoamentos
        \cite{quadflieg2}. A estratégia de evolução do gerenciamento do
        acelerador e freio passou a ser a Adaptação de Matrizes de Covariância
        (CMA-ES) e houve melhorias na forma de se lidar com carros adversários.
        Além disso, o agente também recebeu um novo módulo de aprendizado,
        responsável por adaptar o comportamento planejado às particularidades de
        cada pista. Dessa forma, Mr. Racer venceu as competições de 2011, 2012 e
        2013.

    % ------------------------------------------------------------------------ %
    \subsection{Comparações}
        Para comparar o desempenho de nosso piloto virtual em relação a AUTOPIA
        e Mr. Racer, realizamos para cada controlador uma bateria de testes com
        as dez pistas apresentadas na Figura \ref{fig:test_tracks}. Nosso agente
        correu por $300$ voltas em cada pista. Já AUTOPIA e Mr. Racer passaram
        por uma etapa de aquecimento antes de cada prova, pois esse é o
        procedimento realizado no SCRC. As médias dos tempos de volta foram
        calculadas depois da estabilização do comportamento dos controladores e
        esses resultados estão condensados na Figura \ref{fig:times}.

        No geral, o desempenho de nosso piloto virtual foi inferior ao de seus
        concorrentes. Os piores resultados se deram nas pistas e-track-4 e
        street-1, com diferenças de aproximadamente $60s$ e $40s$
        respectivamente. Surpreendentemente, em g-track-3 a diferença foi de
        apenas $2s$ e em e-track-2 fomos $5s$ mais rápidos que Mr. Racer.
        Nas demais pistas, a diferença média de tempo foi de aproximadamente
        $13s$.

        \begin{figure}
            \centering
            \includegraphics[width=\paperwidth, angle=270]{times}
            \caption{Comparação entre os tempos médios de volta dos pilotos
                     analisados. Os resultados referem-se à média de $40$ voltas
                     \textbf{após} o aprendizado dos pilotos ter convergido,
                     sendo que ``RL Driver'' é o nosso agente.}
            \label{fig:times}
        \end{figure}


% ---------------------------------------------------------------------------- %
\section{Discussão}
        Analisar a evolução do agente durante o processo de aprendizagem, tanto
        através dos registros de tempo e recompensas quanto pela interface
        visual do \emph{TORCS}, nos instigou algumas reflexões. A seguir
        discutimos sobre algumas questões relacionadas ao comportamento do
        piloto virtual.

    % ------------------------------------------------------------------------ %
    \subsection{Aprendizado humano vs. aprendizado de máquina}
        Um dos instintos básicos do ser humano é a autopreservação. Por conta
        disso não é surpreendente notar que pessoas inexperientes na atividade
        de pilotar carros tendem a ser cautelosas durante suas primeiras
        experiências em pistas de corrida. Seja conduzindo carros convencionais
        ou \emph{karts}, a maior prioridade do aspirante a piloto é manter o
        carro na pista, evitando assim se machucar ou colocar a própria vida em
        risco. Fazer voltas velozes é uma etapa secundária em seu aprendizado.

        Entretanto, os simuladores eliminam o perigo de ferimentos reais e o
        temor pela própria segurança deixa de existir. Dessa forma, em seções de
        corrida simulada, a tendência se inverte e frequentemente observa-se
        iniciantes acelerando displicentemente e protagonizando grandes
        colisões. Mas logo as pessoas entendem melhor como controlar o carro
        virtual e passam para um estado de cautela muito semelhante ao da
        situação real descrita anteriormente.

        Em ambas as condições, uma vez que as pessoas dominam o controle do
        carro, virtual ou real, elas passam então a tentar diminuir o tempo de
        suas voltas. Nessa fase, o aprendiz praticamente não se envolve mais em
        acidentes e tenta ser o mais contante possível em todas as voltas,
        experimentando mudar de comportamento em poucas curvas, ou em apenas
        uma. Assim, usa-se a medição de tempo para avaliar os comportamentos e
        buscar o melhor.
%Caso haja tomadas de tempos parciais por setores
%da pista, otimiza-se um pouco esse processo, mas limitadamente.
        Resumidamente, podemos então dizer que o aprendizado das pessoas passa
        por três etapas:
        \textbf{displicência} e envolvimento em vários acidentes;
        \textbf{cautela} e tentativa de controlar o carro para dirigir com
        segurança;
        \textbf{aprimoramento} pela busca gradual do melhor tempo.
        %Salientando que a primeira etapa é mais frequente nos simuladores do que
        %em situações reais. E os acidentes são praticamente apenas sair da
        %pista. Dificilmente as pessoas param completamente o carro.

        O piloto virtual, por sua vez, mostrou um processo de aprendizagem
        completamente diferente. Em vez de passar pelas etapas listadas acima,
        os registros dos experimentos nos mostram que o agente repetidamente sai
        da pista ou para o carro até um dado momento quando os acidentes cessam.
        A partir desse ponto, o piloto virtual já está fazendo suas melhores
        voltas, não havendo uma melhora gradual nos tempos tomados como
        inicialmente esperávamos. A Figura \ref{fig:e-track-2_times} mostra esse
        fato: picos nos tempos de volta alinhados a punições acentuadas
        (associados a ocorrência de acidentes) são frequentes até
        aproximadamente a volta $220$, quando o piloto estabiliza seu
        desempenho.

        Acreditamos que a causa desse fenômeno esteja relacionada aos múltiplos 
        estímulos de \emph{feedback} que o agente recebe durante toda a volta,
        além de apenas o tempo total acumulado. Assim, o piloto virtual consegue
        ponderar sobre seu comportamento sem depender exclusivamente do
        cronômetro. De certo modo, pode-se entender que em vez de tentar dominar
        o circuito curva a curva como um humano faria, o agente aprende sobre
        todos os trechos simultaneamente. Em decorrência disso, quando ações
        que levam a acidentes param de ser tomadas, a política já está próxima
        ao melhor conjunto de escolhas que o controlador consegue determinar.

    % ------------------------------------------------------------------------ %
    \subsection{Topologia das pistas}
        Uma questão com a qual nosso piloto virtual não consegue lidar é a
        eventual ocorrência de elevações e declives nas pistas. Uma vez que
        nenhum dos sensores disponíveis no carro é capaz de indicar a presença
        de subidas e descidas, nossa abordagem sempre produz modelos de pista
        planificados.

        Sem informações sobre esse tipo de característica, o agente não consegue
        diferenciar descidas e retas planas, por exemplo.  Isso se mostra um
        problema, pois esses dois trechos podem receber a mesma classificação,
        porém evidentemente exigem comportamentos distintos. Em uma descida, o
        piloto precisa chegar ao final da reta com velocidade inferior ao normal
        a fim de evitar que se saia da pista.

        Um desafio especialmente difícil para nosso agente se dá em pistas com
        uma ocorrência de uma (ou poucas) descida(s) em um tipo de trecho que se
        repete várias vezes no circuíto, porém com altura constante. Nesses
        cenários, o mecanismo de aprendizagem se contradiz ao punir ações que
        levam a acidentes (na descida), como acelerar, e também punir ações que
        fazem o carro andar muito devagar (nos trechos normais). Dessa forma,
        todas as alternativas de ação se mostram opções de baixo valor e o
        agente tente alternar indefinidamente em sua escolha.

        Isso faz com que o aprendizado não convirja, resultando em um histórico
        de tempos de volta e recompensas similar ao apresentado na Figura
        \ref{fig:wheel-1_bad}. Por conta desse fator, nossa bateria de
        experimentos com $300$ voltas não incluiu algumas pistas usadas nos
        testes de segmentação e classificação mostrados no Apêndice
        \ref{app:segmentacao}.

        A princípio, não há formas simples de se identificar elevações e
        declives em virtude da falta de sensores preparados para essa tarefa.
        Portanto, uma forma de se lidar com essa questão é seguir a abordagem
        implementada no AUTOPIA e Mr. Racer: adicionar ao controlador um módulo
        que monitora a frequência de acidentes em cada trecho e torna as ações
        mais cautelosas nessa região. Porém isso já é uma solução personalizada
        para cada pista. Ou seja, mesmo que o agente seja inicializado com uma
        boa política, ainda sim será preciso passar por acidentes até que essa
        solução tenha efeito.

    % ------------------------------------------------------------------------ %
    \subsection{$Q$-\emph{Learning} tabular e discretizações}
        Uma das restrições impostas pelo uso do $Q$-\emph{Learning} tabular
        baseado na \emph{equação de Bellman} é a necessidade de se trabalhar com
        espaços discretos de estados e ações. Neste projeto, os dois principais
        atributos contínuos que precisaram ser interpretados discretamente
        foram: a \textbf{velocidade} instantânea do carro, usada para determinar
        em qual estado se está; e o sinal composto da \textbf{intensidade} de
        acionamento dos pedais do acelerador e freio. A medição da velocidade
        foi dividida em $5$ intervalos e o sinal para os atuadores dos pedais
        também foi fragmentado em $5$ patamares, como mencionado no Capítulo
        \ref{chap:piloto}.

        Esse tipo de limitação certamente impacta no desempenho do agente,
        embora essa influência negativa tenha se mostrado menos severa do que
        estimávamos. No caso do controle do acelerador e freio, acreditamos que
        o emprego dos \emph{sistemas auxiliares} descritos no Capítulo
        \ref{chap:piloto} contribuiu para mitigar o efeito da discretização.
        Embora as respostas do mecanismo de aprendizado se restrinjam a $5$
        alternativas, esses sistemas repassam aos atuadores um sinal filtrado
        com transições menos abruptas.

        Em contrapartida, a avaliação do estado no qual o carro se encontra
        precisa indicar alguma linha na matriz $Q$. Portanto, a granularidade
        dos intervalos de velocidade está diretamente relacionada a
        \emph{maldição da dimensionalidade}. Uma forma de atacar essa questão é
        substituir a matriz $Q$ por um \emph{aproximador de função valor}. Essa
        abordagem aproveita técnicas de aprendizado supervisionado, tais como
        redes neurais artificiais, para ajustar os parâmetros do aproximador.
        Muitos trabalhos têm adotado soluções dessa categoria, inclusive
        apresentando conclusões positivas sobre a robustez do emprego de
        aproximadores CMAC\footnote{\emph{Cerebellar Model Articulation
        Controller}} no Aprendizado por Reforço \cite{sutton2}.

        Outra opção ainda seria seguir os trabalhos relacionados a Aprendizagem
        por Reforço em lote, como o \emph{Fitted $Q$-Iteration} \cite{ernst}.
        Algoritmos dessa categoria reformulam os problemas de Aprendizagem por
        Reforço como sequências de problemas convencionas de aprendizado
        supervisiondado. Sua principal característica é o alto aproveitamento
        dos dados coletados durante o aprendizado, pois todas as transições de
        estados são armazenadas e a atualização das estimativas de valor é
        realizada simultaneamente em todo o lote de transições.

        %loop de traço pior que dimensão da matriz
%
        %Discretização de maior impacto: velocidades para formar os estados e ações.
%
        %RL é viável, mesmo na forma tabular.
        %Não tivemos muito problema com a maldição da dimensionalidade e geralmente o
        %agente convergia antes de $200$ voltas, o que é muito menos do que esperávamos.
        %Hipótese: aumentar um pouco a granularidade das ações. Nos estados, quanto mais
        %fino for a leitura de velocidade, melhor.
        %Usar técnicas de coarsing code, estimadores de função valor em vez da matriz e
        %até mesmo X-fit que lidam com espaços contínuos.
%
        %Precisamos deixar mais robusto o método de traço duplo e a identificação das
        %causas de acidentes.
