#set terminal wxt size 350,262 enhanced font 'Verdana,10' persist;
set terminal pdfcairo
set output "slip.pdf"

set xrange[0:1];
set yrange[0:0.9];
set samples 200

set xtics 0.2
set ytics 0.2
set grid xtics ytics

set xlabel "Escorregamento (slip)"
set ylabel "Coeficiente de frenagem"

#plot "data" smooth cspline lw 5 title""
plot "-" smooth cspline lt 7 lw 6 title "" with lines
0 0
0.15 0.83
0.19 0.85
0.25 0.83
0.6 0.65
1 0.53
e

