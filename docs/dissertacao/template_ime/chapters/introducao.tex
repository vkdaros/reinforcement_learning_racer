% ---------------------------------------------------------------------------- %
\chapter{Introdução}
\label{cap:introducao}
% ---------------------------------------------------------------------------- %

Escrever bem é uma arte que exige muita técnica e dedicação. Há vários bons
livros sobre como escrever uma boa dissertação ou tese. Um dos trabalhos
pioneiros e mais conhecidos nesse sentido é o livro de Umberto Eco \cite{eco:09}
intitulado \emph{Como se faz uma tese}; é uma leitura bem interessante mas, como
foi escrito em 1977 e é voltado para teses de graduação na Itália, não se aplica
tanto a nós.

Para a escrita de textos em Ciência da Computação, o livro de Justin Zobel,
\emph{Writing for Computer Science} \cite{zobel:04} é uma leitura obrigatória.
O livro \emph{Metodologia de Pesquisa para Ciência da Computação} de Raul Sidnei
Wazlawick \cite{waz:09} também merece uma boa lida. Já para a área de
Matemática, dois livros recomendados são o de Nicholas Higham, \emph{Handbook of
Writing for Mathematical Sciences} \cite{Higham:98} e o do criador do \TeX,
Donald Knuth, juntamente com Tracy Larrabee e Paul Roberts, \emph{Mathematical
Writing} \cite{Knuth:96}.

O uso desnecessário de termos em lingua estrangeira deve ser evitado. No
entanto, quando isso for necessário, os termos devem aparecer \emph{em itálico}.

\begin{small}
    \begin{verbatim}
        Modos de citação:
        indesejável: [AF83] introduziu o algoritmo ótimo.
        indesejável: (Andrew e Foster, 1983) introduziram o algoritmo ótimo.
        certo : Andrew e Foster introduziram o algoritmo ótimo [AF83].
        certo : Andrew e Foster introduziram o algoritmo ótimo (Andrew e Foster, 1983).
        certo : Andrew e Foster (1983) introduziram o algoritmo ótimo.
    \end{verbatim}
\end{small}

Uma prática recomendável na escrita de textos é descrever as legendas das
figuras e tabelas em forma auto-contida: as legendas devem ser razoavelmente
completas, de modo que o leitor possa entender a figura sem ler o texto onde a
figura ou tabela é citada.

\begin{figure}[!htb]
    \centering
    \begin{subfigure}[t]{0.85\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/track_angle_sensors}
        \caption{Representação do sensor de inclinação em relação ao
                 eixo da pista (angle) e, em vermelho, dos sensores de
                 distância até as bordas da pista (\emph{track}).}
        \label{fig:angle_sensor}
    \end{subfigure}
    \vskip \baselineskip

    \begin{subfigure}[t]{0.85\textwidth}
        \centering
        \includegraphics[width=\textwidth]{images/trackPos_sensor}
        \caption{Representação do sensor de distância em relação ao eixo
                 da pista (trackPos).}
        \label{fig:trackpos_sensor}
    \end{subfigure}
    \caption{Alguns dos sensores mais usados aos quais se tem acesso.
             Imagens de \cite{loiacono}.}
    \label{fig:sensors}
\end{figure}

Apresentar os resultados de forma simples, clara e completa é uma tarefa que
requer inspiração. Nesse sentido, o livro de Edward Tufte
\cite{tufte01:visualDisplay}, \emph{The Visual Display of Quantitative
Information}, serve de ajuda na criação de figuras que permitam entender e
interpretar dados/resultados de forma eficiente.

% ---------------------------------------------------------------------------- %
\section{Considerações Preliminares}
\label{sec:consideracoes_preliminares}

    Considerações preliminares\footnote{Nota de rodapé (não abuse).}
    \index{genoma!projetos}.
    % index permite acrescentar um item no indice remissivo
    Texto texto texto texto texto texto texto texto texto texto texto texto.

    \begin{table}
        \caption{Sensores disponíveis}
        \label{tab:sensors}
        \small
        \begin{tabular}{| c | p{11.73cm} |}
            \hline
            \textbf{Nome} & \textbf{Descrição} \\ \hline
            angle         & Ângulo entre a direção do carro e a direção do eixo
                            da pista. Valores negativos indicam que o carro está
                            apontando para a direita do eixo da pista e valores
                            negativos para a esquerda.\\ \hline
            curLapTime    & Tempo decorrido desde o início da volta atual.
                            \\ \hline
            damage        & Quantidade de dano sofrido pelo carro. \\ \hline
            distFromStart & Distância entre o carro e a linha de largada ao
                            longo do eixo da pista. \\ \hline
            distRaced     & Distância percorrida pelo carro desde o início da
                            corrida. \\ \hline
            fuel          & Indica o nível de combustível. \\ \hline
            gear          & Indica qual marcha está engatada. \\ \hline
            lastLapTime   & Tempo gasto para completar a volta anterior. \\
                            \hline
            opponents     & Vetor de 36 sensores que detecta a distância (de
                            0 a 100 metros) até um oponente. Cada sensor cobre
                            um setor de $10^\circ$, de $-\pi$ a $\pi$, em torno
                            do carro.\\ \hline
            racePos       & Posição na corrida com relação aos outros carros.
                            \\ \hline
            rpm           & Número de rotações por minuto do motor. \\ \hline
            speedX        & Velocidade do carro ao longo do eixo longitudinal do
                            carro. \\ \hline
            speedY        & Velocidade do carro ao longo do eixo transversal ao
                            carro. \\ \hline
            track         & Vetor de 19 sensores de proximidade posicionados na
                            frente: cada sensor indica a distância entre a
                            frente do carro e a extremidade da pista. Cada
                            sensor tem inclinação de $10^\circ$ em relação ao
                            anterior, indo de $-\pi/2$ a $+\pi/2$. A distância
                            é dada em metros e o alcance dos sensores é de 100
                            metros. Quando o carro está fora da pista (ou seja,
                            quando trackPos é menor que -1 ou maior que 1),
                            esses sensores não são confiáveis.\\ \hline
            trackPos      & Distância entre o carro e o eixo da pista. O valor
                            é normalizado com respeito à largura da pista: 0
                            quando o carro está sobre o eixo, -1 quando estiver
                            sobre a borda direita da pista e 1 quando estiver
                            sobre a borda esquerda. Valores menores que -1 e
                            maiores que 1 indicam que o carro está fora da
                            pista. \\ \hline
            wheelSpinVel  & Vetor de 4 sensores representando a rotação de cada
                            roda. \\ \hline
        \end{tabular}
    \end{table}
    \begin{table}
        \caption{Atuadores disponíveis}
        \label{tab:effectors}
        \small
        \begin{tabular}{| c | p{12.65cm} |}
            \hline
            \textbf{Nome} & \textbf{Descrição} \\ \hline
            accel    & Pedal virtual do acelerador (0 significa sem aceleração
                       e 1 é aceleração total)\\ \hline
            brake    & Pedal virtual do freio (0 significa sem freio e 1 é
                       frenagem total).\\ \hline
            gear     & Engata a marcha especificada: -1 para ré, 0 para neutro
                       (ponto morto) e de 1 a 6 para as demais. \\ \hline
            steering & Valor de -1 a +1 indicando o quanto o volante está
                       virado (completamente para esquerda e direita
                       respectivamente). Inclinação total corresponde a
                       0,785398 radianos.\\ \hline
            meta     & Para uso de controle da corrida: 0 não faz nada e 1
                       solicita ao servidor o reinício da corrida. \\ \hline
        \end{tabular}
    \end{table}

    Texto texto texto texto texto texto texto texto texto texto texto texto
    texto texto texto texto texto texto texto texto texto texto texto texto
    texto texto texto texto texto texto texto.

% ---------------------------------------------------------------------------- %
\section{Objetivos}
\label{sec:objetivo}

    Texto texto texto texto texto texto texto texto texto texto texto texto
    texto texto texto texto texto texto texto texto texto texto texto texto
    texto texto texto texto texto texto texto.

    \begin{algorithm}[]
        \SetAlgoNoLine
        \SetKwFor{Para}{para}{faça}{fim}

        \SetKwData{ma}{$\mu_{antes}$}
        \SetKwData{md}{$\mu_{depois}$}
        \SetKwData{da}{$\sigma_{antes}$}
        \SetKwData{dd}{$\sigma_{depois}$}
        \SetKwData{dm}{$\Delta_\mu$}
        \SetKwData{pontos}{ângulos}

        \SetKwFunction{Segmentador}{Segmentador}
        \SetKwFunction{media}{Média}
        \SetKwFunction{desvio}{Desvio}
        \SetKwFunction{abs}{Abs}
        \SetKwFunction{max}{Max}

        \Segmentador{\pontos, n}

        \Indp
            \Para{$i \in [5, n - 5]$}{
                \ma $\leftarrow$ \media{\pontos$[i - 5,$ $i - 1]$}\\
                \da $\leftarrow$ \desvio{\pontos$[i - 5,$ $i - 1]$}\\
                \md $\leftarrow$ \media{\pontos$[i$, $i + 4]$}\\
                \dd $\leftarrow$ \desvio{\pontos$[i$, $i + 4]$}\\
                \dm $\leftarrow$ \abs{$\ma - \md$}\\

                \Se{\dm $>$ 3 * \max{\da, \dd}}{
                    $\triangleright$ $i$ é início de um novo trecho.
                }
            }
        \caption{Método de segmentação, onde $n$ é o tamanho do vetor
                 \textbf{ângulos}, no qual estão os valores das curvaturas de
                 cada segmento.}
        \label{alg:segmenter}
    \end{algorithm}

    Texto texto texto texto texto texto texto texto texto texto texto texto
    texto texto texto texto texto texto texto texto texto texto texto texto.

% ---------------------------------------------------------------------------- %
\section{Contribuições}
\label{sec:contribucoes}

    As principais contribuições deste trabalho são as seguintes:

    \begin{itemize}
        \item Item 1. Texto texto texto texto texto texto texto texto texto
              texto texto texto texto texto texto texto texto texto texto.

        \item Item 2. Texto texto texto texto texto texto texto texto texto
              texto texto texto texto texto texto texto texto texto texto.
    \end{itemize}

% ---------------------------------------------------------------------------- %
\section{Organização do Trabalho}
\label{sec:organizacao_trabalho}

    No primeiro capítulo, apresentaríamos os conceitos ... Finalmente, no
    Capítulo~\ref{cap:conclusoes} discutimos algumas conclusões obtidas neste
    trabalho. Analisamos as vantagens e desvantagens do método proposto ...

    As sequências testadas no trabalho \index{trabalho!grad} estão disponíveis
    no Apêndice \ref{ape:sequencias}.
