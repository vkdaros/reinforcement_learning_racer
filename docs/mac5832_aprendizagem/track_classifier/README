#########################################
# MAC 5832 - Aprendizagem Computacional #
# ------------------------------------- #
# Projeto: Track Classifier             #
# Vínicius Kiwi Daros    N.USP: 5893980 #
#########################################

==========
Diretórios
==========

documents
---------
    Nessa pasta estão os arquivos .pdf do relatório e artigo do projeto. Bem
    como uma pasta images, onde estão figuras de várias etapas do processo de
    classifição.

    images
    ------
        signals
        -------
            Nessa pasta estão as imagens das variações de curvatura de cada
            pista em formato de "sinal".

        segmentation
        ------------
            Nessa pasta estão as figuras resultantes do processo de segmentação
            das pistas. As cores não representam nada, são apenas usadas para
            diferenciar os segmentos.

        methods_distributions
        ---------------------
            Nessa pasta foram colocados os gráficos da nuvem de pontos
            referente aos segmentos. Cada figura corresponde a um método de
            agrupamento e os pontos estão pintados com corres correspondentes
            às usadas nas pastas *_clusters.

        *_clusters
        __________
            Nessas pastas estão as figuras dos resultados de classificação por
            cada um dos seguintes métodos: Decision Tree (dt); Expectation
            Maximization com 13 classes (em13); K-Means com 13 classes (km13).

data
____
    Nessa pasta foram colocados todos os dados usados em todas as etapas da
    classificação de modo a ser possível reproduzir os resultados apresentados
    nos documentos escritos.

    raw_data
    --------
        Dados gerados pelo piloto virtual coletor de dados ao correr nas pistas
        do TORCS.

    segments
    ________
        Dados de comprimento e curvatura referentes aos segmentos de cada
        pista. Esses arquivos são gerados pré-processando os arquivos .raw.

    stretches
    _________
        Dados de comprimento e curvatura dos trechos de cada pista. Arquivos
        gerados ao segmentar os arquivos .segments.

    clusters
    ________
        Dados classificados e prontos para serem visualizados em forma de
        desenhos dos trechos coloridos sobre as pistas. Cada subpasta dentro
        desse diretório corresponde a um método de classificação.

    arff_files
    __________
        Arquivos de dados compatíveis com o Weka. O arquivo all.rff contem as
        informações de todos os trechos de todas as pistas e está pronto para
        ser usado no Weka para testar os diversos algoritmos de agrupamento.

    signals
    _______
        Arquivos análogos aos de stretches, porém com o campo "comprimento"
        carregando a distância acumulada, assim podem ser usados com o GNUPlot
        para desenhar as imagens dos "sinais".

scripts
_______
    Pasta contendo os scripts usado no processo de classificação. A forma de
    uso de cada um deles está detalhada a seguir.


============================
Reproduzindo os experimentos
============================

1. Aquisição de dados brutos
----------------------------
    Ao executar o TORCS e colocar o piloto coletor de dados para correr,
    gera-se o arquivo .raw com os dados da pista.

2. Extraindo os segmentos
_________________________
    Para transformar os dados brutos em uma lista de segmentos, usa-se o script
    preprocessor.py da seguinte forma:
        $ ./preprocessor.py trackX.raw

    Isso gerará o arquivo trackX.segments contendo uma lista de comprimento e
    curvatura dos segmentos da pista. Para gerar um arquivo .signal com a
    lista contendo a distância acumulada, usa-se:
        $ ./preprocessor.py -a trackX.raw

    Também é possível executar essa operção para todos os arquivos brutos de
    uma só vez. Nesse casa vários arquivos .segments serão criados, cada um com
    o nome do .raw corresondente:
        $ ./preprocessor.py *.raw

3. Segmentando
______________
    Como explicado no relatório, há duas formas possíveis de segmentar as
    pistas: usando o método automático ou o método manual (pelo visualizador).

    3.1 Segmentação automática
    __________________________
        Para executar a segmentação automática e gerar os arquivos .stretches,
        usa-se o seguinte script:
            $ ./segmenter.py trackX.segments

        Ou para realizar a tarefa para mais de um arquivo:
            $ ./segmenter.py *.segments

    3.2 Segmentação manual
    ______________________
        A segmentação manual é feita pelo visualizador (OBS: o 0.5 é um fator
        de escala opcional, podendo ser qualquer float, para fazer a figura
        caber na tela):
            $ ./track_visualizer.py trackX.segments 0.5

        As teclas de interação com a ferramenta são impressas no console ao
        iniciar o programa, mas a operação básica para a segmentação é:
        - Apertar a tecla "p" para desenhar os trechos na pista;
        - Aumentar ou diminuir o ângulo de curvatura máxima entre segmentos do
          mesmo trecho apertando "q" e "a";
        - Aumentar ou diminuir o comprimento mínimo dos trechos apertando "w"
          e "s";
        - Apertar "c" para criar o arquivo trackX.stretches com os trechos do
          jeito que estão sendo visualizados;
        - ESC para sair.

        Funcionalidades bônus do visualizador: a tecla "v" exporta um png com
        a imagem que está sendo mostrada na janela e a tecla "x" exporta um
        arquivo .xy com o posicionamento dos pontos em um plano cartesiano,
        permitindo plotar as pistas com o GNUPlot.

4. Classificação
________________
    A classificação dos trechos pode ser feita pela árvore de decisão mostrada
    no artigo ou pelos algoritmos do Weka.

    4.1 Usando árvore de decisão
    ____________________________
        Para gerar um arquivo trackX.cluster com a classificação dos trechos,
        basta executar:
            $ ./stretches2clustersDT.py trackX.stretches

        Ou:
            $ ./stretches2clustersDT.py *.stretches

    4.2 Usando o Weka
    _________________
        Para usar os algoritmos de agrupamento do Weka, é preciso reunir todos
        os trechos de todas as pistas em um único arquivo all.arff para o
        clustering ter o maior volume de pontos disponível. Isso é feito com:
            $ ./stretches2all.py *.stretches

        Após isso, é preciso abrir o Explorer do Weka, escolher importar o
        arquivo all.arff, executar a classificação e exportar o arquivo com o
        resultado (os arquivos da pasta arff_files são frutos desse processo).
        Isto feito, é preciso "quebrar" o arquivo com a classificação gerando
        os .cluster correspondentes a cada .stretch usado:
            $ ./all2clusters.py all_classified.arff *.stretches

        É importante notar que não se deve alterar, remover ou adicionar os
        arquivos .stretches entre essas duas etapas. Caso contrário, a
        correspondência entre trecho e classificação em cada pista será
        perdida, resultando em erro.

5. Conferindo resultados
________________________
    Independentemente do processo usado, a etapa final consiste em abrir o
    arquivo de segmentos juntamente com o de classificação e visualizar o
    resultado:
        $ ./track_visualizer_clusters.py trackX.segments trackX.cluster

    Os comandos desse visualizador são os mesmos do track_visualizer.py.


==================
Executando o TORCS
==================

    Para testar o TORCS e o piloto coletor de dados, é preciso gastar um pouco
    de energia em configurar o ambiente de modo a se adequar ao esquema da
    SCRC. Por essa razão, é altamente recomendável baixar o TORCS diretamente
    do repositório deste projeto:

    $ git clone https://bitbucket.org/vkdaros/reinforcement_learning_racer.git

    Para detalhes de como realizar corretamente a instalação, consulte o
    arquivo scr-manual/manual.pdf, mas siga as instruções que escrevi em no
    README dentro dessa mesma pasta.
