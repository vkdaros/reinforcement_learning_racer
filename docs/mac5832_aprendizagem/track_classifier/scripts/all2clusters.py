#!/usr/bin/python

import sys
import os

error_message = """
ERROR: no input file provided. A file with clusters and at least one segment file must be given.
    Usage: all2clusters.py <CLUSTERED_FILE> [FILE1 FILE2 ...]
"""

def get_cluster_file_name():
    if len(sys.argv) <= 2:
        print(error_message)
        sys.exit()
    return sys.argv[1]


def get_file_names():
    if len(sys.argv) <= 2:
        print(error_message)
        sys.exit()
    return sys.argv[2:]


def get_first_data_line(data):
    i = 0
    while data[i] != "@data\n":
        i += 1
    i += 1

    while data[i][0] == "\n" or data[i][0] == " " or data[i][0] == "%":
        i += 1

    return i


def fix_cluster_counter(data, i):
    # cluster{i} turns to cluster{i - 1}
    while i < len(data):
        data[i] = data[i][0:-2] + str(int(data[i][-2]) - 1) + "\n"
        i += 1
    return data


def main():
    cluster_file_name = get_cluster_file_name()
    file_names = get_file_names()

    cluster_file = open(cluster_file_name, "r")
    data = cluster_file.readlines()
    cluster_file.close()
    
    i = get_first_data_line(data)
    #data = fix_cluster_counter(data, i)

    for name in file_names:
        file = open(name, "r")
        length = len(file.readlines())
        file.close()

        file = open(os.path.splitext(name)[0] + ".cluster", "w")
        for line in data[i:(i + length)]:
            line = line.strip().split(',')[1:]
            file.write("%s,%s,%s\n" % (line[0], line[1], line[2]))
        file.close()
        i += length


main()
