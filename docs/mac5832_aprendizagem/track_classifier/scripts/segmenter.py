#!/usr/bin/python

import os
import sys
import math
import numpy

error_message = """
ERROR: no input file provided.
    Usage: segmenter.py <FILE1> [FILE2, FILE3, ..]
"""

WINDOW_SIZE = 5
FILTER_WINDOW_SIZE = 3


def get_file_names():
    if len(sys.argv) == 1:
        print(error_message)
        sys.exit()

    return sys.argv[1:]


def get_data(file_name):
    file = open(file_name, "r")
    data = [[float(val) for val in line.split()] for line in file.readlines()]
    file.close()

    # data = [[dist, angle], [dist, angle], ... ]
    return data


def filter_peaks(data):
    for i in range(FILTER_WINDOW_SIZE, len(data) - FILTER_WINDOW_SIZE):
        mean = numpy.average(data[i - FILTER_WINDOW_SIZE:i + 1 + FILTER_WINDOW_SIZE])
        std = numpy.std(data[i - FILTER_WINDOW_SIZE:i + 1 + FILTER_WINDOW_SIZE])

        if math.fabs(mean - data[i][1]) > 3 * std and \
           math.fabs(mean - data[i - 1][1]) <= 2 * std and \
           math.fabs(mean - data[i + 1][1]) <= 2 * std:

            data[i][1] = mean


def get_segments(data):
    segments = []
    recently_appended = False
    dist_log = 0

    accumulated_distance = 0
    accumulated_angle = 0
    for i in range(WINDOW_SIZE):
        accumulated_distance += data[i][0]
        accumulated_angle += data[i][1]
        dist_log += data[i][0]

    for i in range(WINDOW_SIZE, len(data) - WINDOW_SIZE):
        angles = [d[1] for d in data[i - WINDOW_SIZE:i]]
        mean_back = numpy.average(angles)
        std_back = numpy.std(angles)

        angles = [d[1] for d in data[i:i + WINDOW_SIZE]]
        mean_forward = numpy.average(angles)
        std_forward = numpy.std(angles)

        if math.fabs(mean_back - mean_forward) > 3 * max(std_back, std_forward):
            if recently_appended:
                accumulated_distance += data[i][0]
                accumulated_angle += data[i][1]
                dist_log += data[i][0]
            else:
                segments.append([accumulated_distance, accumulated_angle])
                accumulated_distance = data[i][0]
                accumulated_angle = data[i][1]
                recently_appended = True
        else:
            accumulated_distance += data[i][0]
            accumulated_angle += data[i][1]
            recently_appended = False
            dist_log += data[i][0]

    for i in range(len(data) - WINDOW_SIZE, len(data)):
        accumulated_distance += data[i][0]
        accumulated_angle += data[i][1]
        dist_log += data[i][0]

    # Add the last segment.
    segments.append([accumulated_distance, accumulated_angle])

    return segments


def create_segments_file(file_name, segments):
    file = open(os.path.splitext(file_name)[0] + ".stretches", "w")
    for segment in segments:
        file.write("%f, %f\n" % (segment[0], segment[1]))
    file.close()


def main():
    file_names = get_file_names()
    for file_name in file_names:
        data = get_data(file_name)
        #filter_peaks(data)
        segments = get_segments(data)
        create_segments_file(file_name, segments)


main()
