\documentclass[a4paper]{sbgames}

\usepackage[portuguese]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{graphicx}

%% use this for zero \parindent and non-zero \parskip, intelligently.
\usepackage{parskip}

%% the 'caption' package provides a nicer-looking replacement
\usepackage[labelfont=bf,textfont=it]{caption}

% Habilita subfigure.
\usepackage{subcaption}

% Habilita o uso de matrix.
\usepackage{amsmath}

% Habilita ambiente de algoritmos.
\usepackage[linesnumbered,portuguese]{algorithm2e}

% Permite ajusta o espaçamento entre os itens de itemize
\usepackage{enumitem}

% Divide igualmente o conteúdo da última página entre as duas colunas.
\usepackage{flushend}

\usepackage{url}

%% Paper title.
\title{Identificando e Classificando Trechos de Pistas no Simulador de Corridas
       TORCS}

%% Author and Affiliation (multiple authors). Use: and between authors

\author{Vinícius K. Daros, Flávio S. C. da Silva\\
        Instituto de Matemática e Estatística - IME\\
        Universidade de São Paulo - USP}
\contactinfo{\{vkdaros, fcs\}@ime.usp.br}

\keywords{Car racing, TORCS, videogame, segmentation, classification}

\begin{document}
\maketitle

\begin{abstract}
    A criação de agentes capazes de navegar por ambientes sem o auxílio humano
    com eficiência tem sido foco de diversas pesquisas na área da Inteligência
    Artificial. Quando o ambiente em particular são pistas de corrida, existem
    aplicações diretas no desenvolvimento de jogos e simuladores
    automobilísticos. Um dos desafios envolvidos nessa tarefa é fazer bom uso
    dos dados disponíveis ao agente, transformando-os em informações mais
    condensadas e significativas. Tendo como plataforma de testes o simulador de
    corridas TORCS, mostramos neste trabalho um método capaz de construir um
    modelo de pista baseado apenas em leituras de um número limitado de sensores
    no carro de corrida. Além disso também apresentamos uma técnica de
    segmentação do circuito indicando o início e fim de trechos distintos,
    classificando-os por suas características - como reta, curva suave, curva
    fechada, entre outros. O resultado da segmentação e classificação dos
    trechos de cada pista pode ser um componente valioso na aplicação de
    técnicas de aprendizagem de máquina e/ou planejamento tanto para pilotos
    virtuais quanto para robôs autônomos. Nosso método se mostrou eficaz em
    segmentar e classificar as 18 pistas inclusas no TORCS.
\end{abstract}

\keywordlist
\contactlist

\section{Introdução}
    Um dos desafios atuais na área de Inteligência Artificial é desenvolver
    controladores capazes de atingir uma boa performance no ambiente de
    simulação virtual de corridas automobilísticas sem o auxílio humano. Isto é,
    fazer com que um piloto virtual seja capaz de ``aprender'' sozinho quando
    acelerar, frear, fazer curvas corretamente, efetuar ultrapassagens dentre
    outras ações. Um primeiro passo para conseguir moldar o comportamento de um
    controlador é identificar os diferentes tipos de trechos das pistas, tais
    como reta longa, reta curta, curva fechada, entre outros, a fim de associar
    a cada um deles uma ação desejada. Para realizar essa tarefa, o piloto
    virtual deve ser capaz de montar um modelo do circuito como um todo,
    segmentá-lo em uma sequência de trechos e classificar cada uma dessas áreas
    de modo a conseguir reconhecer curvas ou retas semelhantes como sendo de uma
    mesma categoria. Nesse trabalho, construímos um piloto virtual para o
    simulador de corridas de código aberto
    \emph{TORCS}\footnote{\url{http://torcs.sourceforge.net}}. Para se manter na
    pista, nosso controlador necessita monitorar apenas 2 sensores. Além disso,
    fazendo medições a cada 5 metros e armazenando dados referentes a apenas 4
    outros sensores, é possível construir um modelo do circuito, segmentá-lo em
    trechos e classificar cada trecho.

\section{Trabalhos Relacionados}
\label{sec:related-work}
    Os jogos de corrida são de um gênero muito popular no mercado e também
    atraem o interesse de muitos pesquisadores. Vários autores tem usado o
    \emph{TORCS} como base para explorar diferentes questões relacionadas a
    corridas, como formas  eficientes de se encontrar o traçado ideal em uma
    pista \cite{braghin,cardamone1}. Há também tentativas de elaborar
    controladores capazes de aprender sozinhos como correr e alguns autores tem
    obtido êxito nessa tarefa \cite{onieva,autopia,butz,munoz2,perez,quadflieg2}.
    Apesar disso, o desafio de se conseguir um piloto virtual que dirija um
    carro de corrida tão bem quanto um bom jogador humano ainda é uma questão
    sem solução fechada.
    
    As abordagens adotadas no desenvolvimento de controladores autônomos são
    variadas, envolvendo redes neurais, algoritmos genéticos e lógica
    \emph{fuzzy} por exemplo. Entretanto as estratégias mais recorrentes nas
    pesquisas geralmente envolvem algoritmos que buscam comportamentos ideais
    para responder a situações imediatas descritas pelos sensores do carro.
    Poucos são os estudos com foco em criar um modelo da pista e aproveitá-lo
    como parte do aprendizado do agente. Por exemplo, o trabalho de
    \cite{munoz2} faz um modelo da pista para que o controlador tenha ações
    parecidas com as que um humano teria em cada situação. Já em
    \cite{quadflieg1} é apresentada uma técnica que usa vários sensores para
    construir um modelo do percurso com precisão considerável, essa informação é
    usada para planejar a velocidade e comportamento do agente em cada trecho.

\section{\emph{TORCS}}
    O ambiente de simulação usado para esse trabalho foi o \emph{TORCS (The Open
    Racing Car Simulator)}, um simulador de corridas automobilísticas de código
    aberto desenvolvido com o intuito de permitir aos usuários a criação de
    módulos capazes de controlar o comportamento dos carros. Outras vantagens da
    escolha do \emph{TORCS} como plataforma são: o simulador é também um jogo,
    permitindo o acompanhamento por um visualizador 3D e com possibilidade de
    um jogador humano participar das corridas; há um sofisticado mecanismo de
    física no qual a aderência dos pneus à pista, consumo de combustível, dano
    sofrido e diversos outros fatores influenciam o comportamento e desempenho
    do carro; o sistema é multiplataforma; e há grande variedade de conteúdo
    disponível, tal como pistas e modelos de carros. Por conta dos pontos já
    citados e muitos outros, o \emph{TORCS} vem sendo usado em competições de
    inteligência artificial no meio acadêmico. Em particular, este trabalho
    segue os moldes do \emph{SCRC (Simulated Car Racing
    Championship)}\footnote{\url{http://games.ws.dei.polimi.it/competitions/scr/}}.

    \begin{table}
        \caption{Sensores disponíveis}
        \label{tab:sensors}

        \small
        \begin{tabular}{| c | p{6.2cm} |}
            \hline
            \textbf{Nome} & \textbf{Descrição} \\ \hline
            angle         & Ângulo entre a direção do carro e a direção do eixo
                            da pista. Valores negativos indicam que o carro
                            está apontando para a direita do eixo da pista e
                            valores negativos para a esquerda.\\ \hline
            curLapTime    & Tempo decorrido desde o início da volta atual.
                            \\ \hline
            damage        & Quantidade de dano sofrido pelo carro. \\ \hline
            distFromStart & Distância entre o carro e a linha de largada ao
                            longo do eixo da pista. \\ \hline
            distRaced     & Distância percorrida pelo carro desde o início da
                            corrida. \\ \hline
            fuel          & Indica o nível de combustível. \\ \hline
            gear          & Indica qual marcha está engatada. \\ \hline
            lastLapTime   & Tempo gasto para completar a volta anterior. \\ \hline
            opponents     & Vetor de 36 sensores que detecta a distância (de
                            0 a 100 metros) até um oponente. Cada sensor
                            cobre um setor de $10^\circ$, de $-\pi$ a $\pi$,
                            em torno do carro.\\ \hline
            racePos       & Posição na corrida com relação aos outros carros.
                            \\ \hline
            rpm           & Número de rotações por minuto do motor. \\ \hline
            speedX        & Velocidade do carro ao longo do eixo longitudinal
                            do carro. \\ \hline
            speedY        & Velocidade do carro ao longo do eixo transversal ao
                            carro. \\ \hline
            track         & Vetor de 19 sensores de proximidade posicionados na
                            frente: cada sensor indica a distância entre a
                            frente do carro e a extremidade da pista. Cada
                            sensor tem inclinação de $10^\circ$ em relação ao
                            anterior, indo de $-\pi/2$ a $+\pi/2$. A distância
                            é dada em metros e o alcance dos sensores é de 100
                            metros. Quando o carro está fora da pista (ou seja,
                            quando trackPos é menor que $-1$ ou maior que $1$),
                            esses sensores não são confiáveis.\\ \hline
            trackPos      & Distância entre o carro e o eixo da pista. O valor
                            é normalizado com respeito à largura da pista: 0
                            quando o carro está sobre o eixo, $-1$ quando estiver
                            sobre a borda esquerda da pista e $1$ quando estiver
                            sobre a borda direita. Valores menores que $-1$ e
                            maiores que 1 indicam que o carro está fora da
                            pista. \\ \hline
            wheelSpinVel  & Vetor de 4 sensores representando a rotação de cada
                            roda. \\ \hline
        \end{tabular}
    \end{table}

\section{Construindo modelo da pista}
    O objetivo do piloto virtual desenvolvido nesse trabalho não é completar o
    percurso no menor tempo, mas sim coletar dados da pista para montar um
    modelo do circuito que possa ser usado para as etapas de segmentação e
    classificação. A seguir, descrevemos os passos que nosso controlador executa
    para criar modelo das pistas.

    \subsection{Piloto coletor de dados}
        Durante a volta de reconhecimento, o piloto virtual conta apenas com os
        dados fornecidos pelos sensores (Tabela \ref{tab:sensors}) para guiar o
        carro e também adquirir informações sobre o traçado do circuito. Durante
        essa tarefa, o controlador executa três ações principais: \emph{(i)}
        tentar manter o carro sempre sobre o eixo da pista (minimizando o valor
        de \emph{trackPos}); \emph{(ii)} tentar manter a velocidade constante
        por volta de 60 Km/h em trechos sem curvas (quando o sensor \emph{track}
        frontal é maior que 50 metros) e 20 Km/h caso contrário; \emph{(iii)} a
        cada 5 metros aproximadamente, armazenar os valores de todos os sensores.
        Quando se completa a volta, todos os dados armazenados são exportados
        para analise posterior e encerra-se o trabalho do piloto virtual.

        A atividade de manter o carro sobre o eixo da pista demanda que se vire
        o volante proporcionalmente à defasagem de alinhamento fornecida pelo
        sensor \emph{angle}, levando em conta também o sensor \emph{trackPos},
        pois, mesmo com o carro alinhado ($angle = 0$), deve-se mudar de direção
        caso não se esteja sobre centro da pista. Pela Equação (\ref{eq:steer1})
        é possível determinar o valor $steer \in [-1,1]$ de quanto se deve virar
        o volante, sendo \emph{steerLock} a constante que define o ângulo máximo
        ao qual o volante pode chegar.
        \begin{align}
            steer &= \frac{angle - trackPos * steerLock}{steerLock}\label{eq:steer1}\\
            steer' &= max(min(3 * steer, 1), -1)\label{eq:steer2}
        \end{align}

        Com essa fórmula, o controlador é capaz de posicionar o carro ao centro
        da pista. Entretanto notamos pelos testes que, em curvas mais fechadas,
        o carro tendia a se afastar do caminho ideal. Uma vez que toda a análise
        dos dados se baseia na crença de que eles foram coletados com o carro
        sobre o eixo da pista (embora saibamos que essa é uma situação ideal e
        não reflete a realidade em vários momentos), buscamos adaptações que
        possibilitassem um melhor alinhamento em curvas mais fechadas. Dessa
        forma, adotamos a Equação (\ref{eq:steer2}), com a qual o piloto vira o
        carro menos suavemente mas se afasta menos do eixo da pista sem correr
        riscos de derrapar estando em baixas velocidades.

    \subsection{Curvaturas e segmentos}
        Coletados todos os dados referentes a uma volta completa pela pista,
        podemos usá-los para montar uma representação do percurso. Cada um dos
        dados em questão é o conjunto dos valores de todos os sensores no
        momento da medição, que ocorre aproximadamente a cada 5 metros.
        Portanto, tomando-os aos pares, é possível extrair informações para
        descrever pequenas fatias da pista, as quais chamaremos de
        \textbf{segmentos}. Primeiramente, temos que a diferença de valores em
        \emph{distFromStart} revela o comprimento exato do segmento. Em seguida,
        é preciso descobrir se esse segmento é parte de uma reta ou se o carro
        mudou de direção entre as duas medições.

        Para fazer essa distinção, pode-se verificar o quanto o volante estava
        virado na leitura inicial e final consultando o campo \emph{steer}. Se
        ao menos um dos valores não for nulo, então o carro realizou uma curva
        no intervalo entre as medições. Nesse caso, é importante descobrir o
        quanto o carro virou, ou seja, a \textbf{curvatura} do segmento. Essa
        informação não está contida diretamente em nenhum dos sensores
        disponíveis e para consegui-la é preciso realizar uma série de
        operações.
        \begin{figure}[!htb]
            \centering
            \includegraphics[width=0.2\textwidth]{images/bicycle_model}
            \caption{Modelo de bicicleta usado para simplificar o carro.}
            \label{fig:bicycle_model}
        \end{figure}

        Simplificamos o carro pelo modelo da bicicleta \cite{gillespie}
        ilustrado na Figura \ref{fig:bicycle_model}, onde \emph{WB (wheelbase)}
        é a distância entre os eixos das rodas, $\delta$ é o ângulo do volante
        e \emph{R} é o raio da curva. Por esse modelo, considerando-se pequenos
        valores para $\delta$, é possível aproximar o raio da curva pela Equação
        (\ref{eq:radius}).
        \begin{equation}
            \label{eq:radius}
            R = \frac{WB}{tg(\delta)} \simeq \frac{WB}{sen(\delta)}
        \end{equation}

        Tendo o raio da curva, a Equação (\ref{eq:alpha}) nos fornece o ângulo
        $\alpha$ do arco coberto pelo carro ao andar por esse segmento, sendo
        $r$ a constante do raio das rodas dianteiras, $w$ o valor do sensor
        \emph{wheelSpinVel} de velocidade angular da roda externa à curva,
        $\Delta_t$ o intervalo entre as medições do início e fim do segmento e
        $T$ a constante \emph{TRACK} com o valor da distância entre as rodas do
        mesmo eixo.
        \begin{equation}
            \label{eq:alpha}
            \alpha = \frac{r * w * \Delta_t}{R + \frac{T}{2}}
        \end{equation}
        
        Assim, esse processo é repetido para todos os pares de dados adjacentes
        e cada fatia da pista passa a ser descrita por um segmento composto por:
        \emph{comprimento}, próximo a 5 metros; e \emph{curvatura}, referente ao
        ângulo coberto pelo arco feito pelo carro. Contudo, ao longo das etapas
        descritas há arredondamentos e imprecisões tanto em decorrência do
        intervalo entre medições quanto pelo mal posicionamento do carro no
        instante da coleta. Por esse motivo, a concatenação desses segmentos não
        forma um circuito fechado e é preciso aplicar um fator de correção em
        todas as curvaturas. Para isso, verificamos a proporção entre a soma
        $S_c$ das curvaturas encontradas e $2\pi$. Em seguida, definimos o fator
        de correção $f = \tfrac{S_c}{2\pi}$. Ao multiplicarmos por $f$ o valor
        da curvatura de cada segmento, garantimos que a representação da pista
        tenha uma volta completa e conseguimos um modelo que se aproxima da
        pista real. É possível ver na Figura \ref{fig:segments} os segmentos que
        compõem a pista CG1 e como a parte final nem sempre se encontra
        exatamente com a inicial. Porém, para os objetivos deste trabalho, essa
        disparidade é aceitável.
        \begin{figure}[!htb]
            \centering
            \includegraphics[width=0.38\textwidth]{images/g-track-1_grid_edge}
            \caption{Divisão da pista CG1 em segmentos de aproximadamente 5
                     metros. Em vermelho estão indicações dos pontos que o
                     segmentador identificou a divisão de trechos.}
            \label{fig:segments}
        \end{figure}

\section{Segmentação das pistas}
    \begin{figure*}[!htb]
        \centering
        \includegraphics[width=\textwidth]{images/g-track-1_plot}
        \caption{Variação de ângulos da pista CG1 (Figura \ref{fig:segments})
                 vista como sinal.}
        \label{fig:g-track-1_signal}
    \end{figure*}

    O modelo formado pelos segmentos de 5 metros remonta razoavelmente bem a
    pista, entretanto a informação contida em cada um desses segmentos é pouco
    expressiva e não há vantagem alguma em lidar com uma representação tão
    fragmentada. Ao ver o desenho de um circuito, é fácil para uma pessoa
    distinguir onde começa e termina cada reta, quão fechada é cada curva e
    quando um trecho é longo ou curto. Para um piloto, esse entendimento é
    importante, pois a informação do tipo de trecho em que se está e do(s)
    trecho(s) seguinte(s) tem influência direta na decisão de qual ação
    executar. Dessa forma, aglutinar os segmentos em trechos relativamente
    uniformes traz as seguintes vantagens:
    \begin{itemize}[noitemsep,topsep=0pt,parsep=0pt]
        \item Simplifica-se o modelo. Por
              exemplo, a pista CG1 vista na Figura \ref{fig:segments} pode ser
              representada por apenas 14 trechos em vez de 400 segmentos;

        \item A descrição por trechos é mais eficiente, transmitindo informações
              de forma concisa e significativa. Por exemplo, o fato dos
              primeiros 40 segmentos formarem uma reta com 200 metros é uma
              informação simples e valiosa;

        \item Facilita-se a leitura, compreensão e interpretação dos dados.
    \end{itemize}

    \begin{algorithm}[]
        \small
        \SetInd{0.5em}{0.6em}
        \SetKwFor{Para}{para}{faça}{fim}
        \SetKw{Return}{devolva}
        \SetAlgoNoLine

        \SetKwData{pontos}{ângulos}
        \SetKwData{dfs}{distâncias}
        \SetKwData{limiar}{limiar}
        \SetKwData{scale}{ESCALA}
        \SetKwData{base}{base}
        \SetKwData{flatD}{distFiltrada}
        \SetKwData{flatA}{angFiltrada}
        \SetKwData{bd}{distTrás}
        \SetKwData{ba}{angTrás}
        \SetKwData{fd}{distFrente}
        \SetKwData{fa}{angFrente}

        \SetKwFunction{Filtro}{FiltroDePatamar}
        \SetKwFunction{FiltroMedia}{FiltroDeMédia}
        \SetKwFunction{desvio}{DesvioPadrão}
        \SetKwFunction{push}{adiciona}
        \SetKwFunction{isGap}{éSalto}
        \SetKwFunction{isBaseline}{éPatamar}

        \Filtro{\dfs, \pontos, n}

        \Indp
            $\pontos \leftarrow \FiltroMedia{\pontos}$\\
            $\limiar \leftarrow \scale * \desvio{\pontos}$\\
            $\base \leftarrow 0$\\
            \flatD.\push{$0$}\\
            \flatA.\push{\base}\\
            \Para{$i \leftarrow 1$ \Ate $n - 2$}{
                \Se{\isGap{\pontos$[i]$, \base, \limiar}}{
                    $\bd \leftarrow \dfs[i-1]$\\
                    $\fd \leftarrow \dfs[i]$\\
                    $\ba \leftarrow \base$\\

                    \Enqto{$i < n - 3$ \textbf{\emph e não} \isBaseline{\pontos, $i$}}{
                        $\triangleright$ \emph{Se houve mudança de sinal, ajusta a distância.}\\
                        \Se{$\pontos[i] * \pontos[i-1] <= 0$}{
                            $\bd \leftarrow \dfs[i-1]$\\
                            $\fd \leftarrow \dfs[i]$\\
                        }
                        $i \leftarrow i + 1$
                    }

                    $\fa \leftarrow \pontos[i]$\\
                    \Se{$|\fa| < MAX\_RETA$}{$\fa \leftarrow 0$}
                    $\triangleright$ \emph{Ponto do início do salto.}\\
                    \flatD.\push{\bd}\\
                    \flatA.\push{\ba}\\
                    $\triangleright$ \emph{Ponto do fim do salto.}\\
                    \flatD.\push{\fd}\\
                    \flatA.\push{\fa}\\
                    $\base \leftarrow \fa$\\
                }
            }
            \flatD.\push{$\dfs[n]$}\\
            \flatA.\push{\base}\\
            \Return{\flatD, \flatA}

        \caption{Método de segmentação, onde $n$ é o tamanho dos vetores
                 \emph{distâncias} e \emph{ângulos}.}
        \label{alg:segmenter}
    \end{algorithm}

    Para realizar a segmentação, isto é, determinar os pontos nos quais um
    trecho termina e outro começa, propomos um método que avalia as variações
    das curvaturas dos segmentos como oscilações de um sinal. Na Figura
    \ref{fig:g-track-1_signal}, a linha em vermelho ilustra o ``sinal''
    referente à pista da Figura \ref{fig:segments}. No gráfico, o eixo das
    abscissas indica a distância do segmento até a linha de largada e o eixo das
    ordenadas se refere aos ângulos de curvatura. Vale notar que ângulos
    negativos indicam curvas para a direita e curvas para a esquerda tem ângulos
    positivos. Comparando as duas figuras, é fácil observar que os trechos de
    curva na pista tem paralelo com patamares no gráfico. Porém também é
    possível perceber que há regiões aparentemente homogêneas na pista que
    possuem oscilações consideráveis no sinal - como na primeira grande curva à
    direita por volta da distância de $600m$.

    Nosso objetivo é filtrar o sinal original de modo a encontrar os
    \textbf{patamares} que representam trechos homogêneos na pista mesmo com a
    existência de oscilações. O método que desenvolvemos para tratar esses
    sinais é descrito pelo Algoritmo \ref{alg:segmenter}, o qual recebe os
    vetores \emph{distâncias} e \emph{ângulos} de tamanho $n$ (que representam o
    sinal da pista) e devolve duas novas listas com as distâncias e ângulos
    representando o sinal contendo apenas os pontos referentes aos patamares
    encontrados. Ou seja, tomando dois pontos consecutivos desse novo sinal, ou
    eles estão alinhados (indicando um patamar) ou há uma diferença
    significativa de ângulo entre eles (indicando um  ``salto''). Na Figura
    \ref{fig:g-track-1_signal} vemos em preto os pontos referentes ao sinal já
    filtrado.

    O primeiro passo realizado pelo algoritmo é passar o vetor \emph{ângulos}
    por um filtro de média para atenuar os picos do sinal original. Assim, os
    novos valores $a'_i$ do vetor são tais que:

    \[
        a'_i = \begin{cases}
            \dfrac{a_{i-1} + 2a_i + a_{i+1}}{4} &, i \in [1, n-2]\\
            a_i &, cc.
        \end{cases}
    \]

    Na sequência, é definido o \emph{limiar} acima do qual a diferença entre
    dois ângulos é considerada significativa, um ``salto''. Esse limiar é
    proporcional ao desvio padrão das diferenças entre ângulos consecutivos.
    Para nossos testes, o fator \emph{ESCALA} usado foi $4$. Em seguida, o ponto
    inicial $(0, 0)$ é adicionado ao novo sinal e inicia-se a iteração sobre os
    pontos do sinal original. A função \emph{éSalto} identifica se a diferença
    entre o ângulo atual e o ângulo \emph{base} é suficientemente grande, se
    esses dois ângulos estão em curvas de direções opostas ou se um dos ângulos
    faz parte de uma reta e o outro de uma curva. Nesses casos, \emph{éSalto}
    devolve \emph{verdade}. Quando um salto é encontrado, \emph{distTrás} e
    \emph{angTrás} indicam o ponto do início do salto. Já \emph{distFrente} e
    \emph{angFrente} marcarão o fim do salto e início de um patamar. A função
    \emph{éPatamar} devolve \emph{verdade} quando os ângulos imediatamente à
    frente de $i$ tem valores próximos. Durante a busca ao fim do salto, há a
    possibilidade de dois ângulos consecutivos terem sinais diferentes
    (indicando duas curvas para direções opostas em sequência). Quando isso
    acontece, as distâncias de início e fim do salto são reajustadas para que o
    salto cruze o eixo das abscissas no ponto onde há a inversão das curvas.
    Caso o valor absoluto de \emph{angFrente} seja muito pequeno o arredondamos
    para zero, indicando que o patamar encontrado é o início de uma reta. O
    valor de \emph{MAX\_RETA} usado em nossos experimentos foi $0.008$.

    Ao término da execução do algoritmo, cada salto precede um patamar, o qual é
    uma região uniforme da pista e, portanto, pode ser considerado como um
    trecho. Assim, temos a segmentação da pista em trechos, como exemplificado
    pela Figura \ref{fig:segments}, onde as marcações em vermelho mostram os
    saltos encontrados.

\section{Classificação}
    Depois da segmentação, cada trecho pode ser descrito por dois atributos:
    \emph(i) comprimento; \emph(ii) e raio, que determina o quanto uma curva é
    fechada. Como o comprimento de um trecho de curva está diretamente
    relacionado ao ângulo da mudança de direção do trajeto, optamos por usar
    esse ângulo para a classificação. Além disso, em trechos de reta, o raio
    deveria ser infinito, porém usaremos valores negativos para representar esse
    caso.

    Nosso classificador agrupa trechos em 11 classes distintas. Embora não
    haja necessidade de rotular cada categoria, atribuímos a cada uma delas um
    nome de modo a facilitar a compreensão da classificação. As retas foram
    dividias em \emph{curta}, \emph{média} e \emph{longa}. As curvas, da mais
    fechada para a mais suave, são \emph{hairpin (grampo)}, \emph{cotovelo},
    \emph{acentuada}, \emph{moderada} e \emph{leve}, sendo que as três últimas
    categorias ainda foram subdivididas em \emph{curta} ou \emph{longa}. As
    regras para a classificação dos trechos estão descritas no Algoritmo
    \ref{alg:classifier}.

    \vskip 1em
    \begin{algorithm}[]
        \small
        \SetInd{0.5em}{0.6em}
        \SetKw{Return}{devolva}
        \SetAlgoNoLine

        \SetKwData{angle}{ângulo}
        \SetKwData{radius}{raio}
        \SetKwData{len}{comprimento}

        \SetKwFunction{foo}{Classificador}

        \foo{\angle, \radius, \len}

        \Indp
            $\triangleright$ \emph{Retas tem raio infinito.}\\
            \uSe{$\radius < 0$}{
                \uSe{$\len <= 50$} {
                    \Return{Reta curta}
                }
                \uSe{$\len <= 150$} {
                    \Return{Reta média}
                }
                \Return{Reta longa}
            }
            \uSenao{
                \uSe{$\radius <= 60$} {
                    \uSe{$\angle >= 120^\circ$}{
                        \Return{Hairpin}
                    }
                    \uSe{$\angle >= 50^\circ$}{
                        \Return{Cotovelo}
                    }
                }
                \uSe{$\radius <= 127$} {
                    \uSe{$\angle <= 50^\circ$}{
                        \Return{Curva acentuada curta}
                    }
                    \Return{Curva acentuada longa}
                }
                \uSe{$\radius <= 185$} {
                    \uSe{$\angle <= 50^\circ$}{
                        \Return{Curva moderada curta}
                    }
                    \Return{Curva moderada longa}
                }
                \uSenao{
                    \uSe{$\angle <= 50^\circ$}{
                        \Return{Curva leve curta}
                    }
                    \Return{Curva leve longa}
                }
            }

        \caption{Classificador de segmentos.}
        \label{alg:classifier}
    \end{algorithm}
    \vskip 1em

\section{Resultados}
\label{sec:results}
    Usando o \emph{TORCS} como ambiente de testes, executamos baterias de
    experimentos nas 18 pistas de asfalto que acompanham o simulador. Em todas
    elas o resultado da segmentação e classificação foram satisfatórios, como
    exemplificado pela Figura \ref{fig:clusters}. Embora algumas pistas tenham
    apresentado encaixe perfeito entre o início e fim do circuito como visto na
    Figura \ref{fig:e-track-3_cluster}, isso não ocorreu em todos os casos.
    Entretanto essa não é uma condição essencial para os contextos de uso que
    este trabalho visa atender.
    \vskip 2em

\section{Conclusão}
\label{sec:conclusion}
    Nesse trabalho, mostramos como construímos um piloto virtual para coletar
    dados das pistas do \emph{TORCS} e como calcular a curvatura dos segmentos.
    Além disso, apresentamos um método de segmentação de pistas baseado na
    interpretação da variação das curvaturas como oscilações em um sinal. Por
    fim, propusemos um conjunto de regras para identificar e classificar trechos
    com características semelhantes. O ferramental aqui apresentado se mostrou
    eficaz e pode ser utilizado no desenvolvimento e aplicação de técnicas de
    aprendizagem de máquina em ambientes de simulação virtual e jogos de
    corrida. Também acreditamos que há potencial de uso em sistemas de robôs
    móveis ou qualquer outro método de tomada de decisão e planejamento que se
    baseie na interpretação dos tipos de trechos de um circuito fechado.

    \begin{figure}[!htb]
        \centering
        \begin{subfigure}[!htb]{0.24\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/g-track-1_cluster_edge}
            \caption{CG1.}
            \label{fig:g-track-1_cluster}
        \end{subfigure}
        \begin{subfigure}[!htb]{0.23\textwidth}
            \hfill
            \includegraphics[width=0.8\textwidth]{images/legenda}
        \end{subfigure}
%
%        \begin{subfigure}[!htb]{0.48\textwidth}
%            \centering
%            \includegraphics[width=0.9\textwidth]{images/e-track-2_cluster_edge}
%            \caption{E-Track 2.}
%            \label{fig:e-track-2_cluster}
%        \end{subfigure}

        \begin{subfigure}[!htb]{0.48\textwidth}
            \centering
            \includegraphics[width=0.77\textwidth]{images/e-track-3_cluster_edge}
            \caption{E-Track 3.}
            \label{fig:e-track-3_cluster}
        \end{subfigure}
        \caption{Exemplos de resultado da segmentação e classificação de trechos
                 de pistas do TORCS.}
        \label{fig:clusters}
    \end{figure}

\bibliographystyle{sbgames}
\bibliography{template}
\end{document}
